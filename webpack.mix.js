const mix = require('laravel-mix');

mix.setPublicPath('public/js');
if (mix.inProduction()) {
    mix.version();
}

mix.js('resources/assets/js/app.js', 'public/js');
mix.react('resources/assets/js/react.js', 'public/js');

mix.webpackConfig({
  output: {
    publicPath: '/js/',
    chunkFilename:  '[name].js?id=[chunkhash]',
  },
});