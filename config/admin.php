<?php
return [

    'admin_pin' => env('ADMIN_PIN',null),
    'can_admin_login' => env('CAN_ADMIN_LOGIN',false)
];