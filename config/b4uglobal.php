<?php

## general configurations  for b4uGlobal.com

return [
    'onlyShowVWithdrawalPage' => [
        131476,
    ],
    'onlyShowVWithdrawalPageAllowedRoutes' => [
        '/dashboard/vwithdrawals',
        '/dashboard/vwithdrawals/json',
        '/dashboard/logout2',
        '/dashboard/accountdetails',
        '/dashboard/updateAlbmdetails',
        '/dashboard/updateacct',
        '/updateacct/valid',
        '/dashboard/updatephoto',
        '/dashboard/updatepass',
        '/dashboard/logout',
        '/dashboard/viewWithdrawDetailsPost',
        '/articles/meezan_IBT/0',
        '/articles/meezan_IBFT/0',
        '/articles/meezan_IBT/1',
        '/articles/meezan_IBFT/1',
        '/articles/askari_IBFT',
        '/articles/faysal_IBFT',
        '/articles/bankIslami_IBFT',
        '/articles/soneri_IBT',
        '/articles/soneri_IBFT',
        '/articles/MeezanIBT_GT2',
        '/articles/MeezanIBFT_GT2',
        '/articles/FincaExportCSV',

    ],
    'onlyShowVWithdrawalPageFallbackUrl' => [
        'dashboard/vwithdrawals'
    ],


    ## aws details.
    'AWS_REGION' => env('AWS_REGION', 'ap-southeast-1'),
    'AWS_VERSION' => env('AWS_VERSION', 'latest'),
    'AWS_KEY' => env('AWS_KEY', 'AKIARKSNHGNWRF27R2IQ'),
    'AWS_SECRET' => env('AWS_SECRET', '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'),

    ## price in dollar
    'PER_SMS_CHARGES_TO_USER_BY_B4UGLOBAL' => 0.07,

    'RETRY_CASH_BOX_TRANS' => 5,
    'USER_PER_CB_WORKER' => env('USER_PER_CB_WORKER', 20000),
    'WORKER_LIMIT' => env('WORKER_LIMIT', 500000),
    'CASH_BOX_FAILED_ATTEMPTS' => env('CASH_BOX_FAILED_ATTEMPTS', 10),
    'TEMP_API_KEY' => '12HasAqAbceseMBCbcop2n25AHCDqw1sf',
    'B4WalletPublicKeyPath' => storage_path('b4uwallet_public_key/pb_key.txt'),
    'B4UWalletCashBoxTransactionIntimateURL' => env('B4UWalletCashBoxTransactionIntimateURL', 'https://www.coinee.cf/api/v2/peatio/account/rsc_withdraws'),
    'B4UWalletCashBoxTransactionIntimateApiKey' => env('B4UWalletCashBoxTransactionIntimateApiKey', 'a7a977331d7d7b12'),
    'B4UWalletCashBoxTransactionIntimateApiSecret' => env('B4UWalletCashBoxTransactionIntimateApiSecret', '39b939cc45b696c9d68af38f5d9ee0ac'),

    'CallCenterService' => env('CallCenterServiceURL', 'https://blessed-fog-3vt8se7k1fop.vapor-farm-d1.com/api/'),
    'CallCenterServiceHeaders' => [
        'Accept' => 'application/json',
        'Authorization' => "Bearer {{_fcptoken}}"
    ],
];
