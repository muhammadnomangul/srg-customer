<?php

return [
    'mplansCache' => 'mplansCache',
    'withdrawals' => 'withdrawals',
    'depositCache' => 'depositCache',
    'soldCache' => 'soldCache',
    'imageGalleryCache' => 'imageGalleryCache',
    'albumGalleryCache' => 'albumGalleryCache',
    'albumsCache' => 'albumsCache',
    'albumsCacheFirst' => 'albumsCacheFirst',
    'albumsIdCache' => 'albumsIdCache',
    'albumsIdFirstCache' => 'albumsIdFirstCache',
    'eventsAllCache' => 'eventsAllCache',
];