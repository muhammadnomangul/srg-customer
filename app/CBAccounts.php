<?php

namespace App;

use App\Jobs\CashBox\CreateCreditFundTransfer;
use App\Jobs\CashboxGenerateReport;
use App\Mail\CashboxReportMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CBAccounts extends Model
{
    protected $table = 'cb_accounts';
    static $tableName = 'cb_accounts';

    ## 0 to 500 CashOut fee will charge 10
    const cashOutB4UFeeMinRange = 40;
    const cashOutB4UFeeMaxRange = 500;
    const cashOutB4UFee = 10;

    ## More than 500 CashOut fee will charge 20
    const cashOutB4UFeeOnBigAmount = 20;
    ## Minimum cash out transfer
    const minimumCashOutTransferInDollars = 40;
    const minimumCashOutTransferErrorMessage = 'Minimum CashOut request should be equivalent to 40$';
    const minimumCashOutForPakistaniUserTransferErrorMessage = 'Minimum CashOut request is 40$for USD Accounts';


    protected $fillable = ['current_balance'];


    static $tableNameCurrenciesColumnRef = 'cb_accounts.currencies_id';

    public static function approveCbTransferBySender(int $cbDebitId)
    {
        try {
            $cbDebit = CBDebits::find($cbDebitId);
            if ($cbDebit instanceof CBDebits) {
                $response = CBDebits::approveDebit($cbDebit);
                $cbCredit = CBCredits::where('cb_debit_id', $cbDebit->id)->first();

                if ($response && $cbCredit instanceof CBCredits) {
                    $cbCreditResponse = CBCredits::approveCreditFundTransfer($cbCredit);
                    if ($cbCreditResponse) {
                        return true;
                    }
                }else{
                    return false;
                }
            }
        }catch(\Exception $exception){
            Log::error('error while doing Approve Cb transfer', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
            return $exception->getMessage();
        }
    }

    private static function CashboxGenerateReport($userId, $cbAccountId, $debitId)
    {
        try {
            $balanceCalculation = CBAccounts::balanceCalculation($cbAccountId);
            $cbAccount = CBAccounts::whereId($cbAccountId)->first();
            $cbDebit = CBDebits::whereId($debitId)->first();
             $supportEmail = "hafiznaeemkhan0306@gmail.com";
          //  $supportEmail = "mubixhb4u@gmail.com";
            if ((round($balanceCalculation->expected_current,0) == round($cbAccount->current_balance,0)) &&
                 (round($balanceCalculation->expected_locked,0)   == round($cbAccount->locked_balance,0) )
            ) {
                $cbDebit->status = 'pending';
                $cbDebit->save();
            } else {
                Mail::to($supportEmail)->send(new CashboxReportMail($userId, $cbAccountId, $debitId));
            }
        }catch (\Exception $exception){
            Log::error('error while generation cb Report', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
        }
    }

    public function user()
    {
        return $this->belongsTo(users::class);
    }

    public function currencies()
    {
        return $this->belongsTo(Currencies::class, 'currencies_id')->first();
    }

    /**
     * @param int $currencyId
     * @param User $userId
     * @return CBAccounts|string
     * @purpose Make Cash Box account of user.
     */
    public static function makeCBAccount(int $currencyId, int $userId)
    {
        try {

            ## get Currency
            $Currency = Currencies::find($currencyId);
            $User = User::find($userId);

            if ($Currency instanceof Currencies && $User instanceof User) {
                $cbAccount = new CBAccounts();
                $cbAccount->name = $Currency->code . ' Account';
                $cbAccount->currencies_id = $Currency->id;
                $cbAccount->user_id = $User->id;
                $cbAccount->current_balance = floatval(0);
                $cbAccount->locked_balance = floatval(0);
                $cbAccount->save();
                return $cbAccount;
            } else {
                return 'Invalid Details';
            }

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

    /**
     * @param int $currencyId
     * @param int $userId
     * @return mixed
     * @Purpose Get CashBox account on Base of Currency and User
     */
    public static function getCBAccountByCurrencyId(int $currencyId, int $userId)
    {
        return CBAccounts::where('currencies_id', $currencyId)->where('user_id', $userId)->first();
    }

    /**
     * @param int $currencyId
     * @param int $userId
     * @return mixed
     * @Purpose Get CashBox account on Base of Currency and User and If CashBox account of that user not found then system will create a new Cash box account.
     */
    public static function getOrCreateIfCBAccountByCurrencyId(int $currencyId, int $userId)
    {
        $CBAccount = self::getCBAccountByCurrencyId($currencyId, $userId);
        if ($CBAccount instanceof CBAccounts) {
            return $CBAccount;
        } else {
            return self::makeCBAccount($currencyId, $userId);
        }
    }

    /**
     * @Purpose Get Current of Specific Currency
     * @param $currencyId
     * @return mixed
     */
    public static function getCashBoxCurrentBalanceByCurrencyId($currencyId)
    {
        return CBAccounts::where('currencies_id', $currencyId)->where('user_id', Auth::user()->id)->first()->current_balance;
    }

    /**
     * @Purpose Get Current of Specific Currency
     * @param $currencyCode
     * @return mixed
     */
    public static function getCashBoxCurrentBalanceByCurrencyCode($currencyCode)
    {
        $Currencies = Currencies::where('code', $currencyCode)->first();
        if ($Currencies instanceof Currencies) {
            return CBAccounts::where('currencies_id', $Currencies->id)->where('user_id', Auth::user()->id)->first()->current_balance;
        } else {
            return 0;
        }
    }


    /**
     * @Purpose Get Locked Balance of Specific Currency
     * @param $currencyCode
     * @return mixed
     */
    public static function getCashBoxLockedBalanceByCurrencyCode($currencyCode)
    {
        $Currencies = Currencies::where('code', $currencyCode)->first();
        if ($Currencies instanceof Currencies) {
            return CBAccounts::where('currencies_id', $Currencies->id)->where('user_id', Auth::user()->id)->first()->locked_balance;
        } else {
            return 0;
        }

    }
    public static function getCashBoxLockedBalanceByCurrencyCodeUser($currencyCode,$user_id)
    {
        $Currencies = Currencies::where('code', $currencyCode)->first();
        if ($Currencies instanceof Currencies) {
            return CBAccounts::where('currencies_id', $Currencies->id)->where('user_id', $user_id)->first()->locked_balance;
        } else {
            return 0;
        }

    }


    /**
     * @Purpose Get Minimum amount to invest in B4U Global.
     * @param string $currency
     * @return float|int
     */
    public static function getMinimumAmountToInvestFromCashBox(string $currency)
    {
        $currencyRate = '';
      //  $ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();

        if (isset($currency) && isset($ratesQuery)) {
            $curr = strtolower($currency);
            $rateVal = 'rate_' . $curr;
            $currencyRate = $ratesQuery->$rateVal;
        }
        return floatval(1) / floatval($currencyRate);
    }

    /**
     * @Purpose Convert CashBox Amount in USD Currency
     * @param string $currencyCode
     * @param int $amount
     * @return array
     */
    public static function ConvertCBAmountInUSD(string $currencyCode, $amount)
    {
        ## get the Currency Rate.
        $convertedAmount = 0;
       // $ratesQuery = currecy_rates::orderBy('created_at', 'desc')->first();
        $ratesQuery = current_rate::first();
        $rateVal = "rate_" . $currencyCode;
        $currencyRate = $ratesQuery->$rateVal;

        if ($currencyRate) {
            $convertedAmount = ($amount * $currencyRate);
        }
        return [
            'actual_amount' => $amount,
            'usd_amount' => $convertedAmount,
            'currency_rate' => $currencyRate,
            'currency' => $currencyCode
        ];

    }

    /**
     * @param string $currencyCode
     * @param int $userId
     * @return mixed
     * @purpose Get CashBoxAccount by Currency Code.
     */
    public static function getCBAccountByCurrencyCode(string $currencyCode, int $userId)
    {
        $Currencies = Currencies::where('code', $currencyCode)->first();
        return CBAccounts::where('currencies_id', $Currencies->id)->where('user_id', $userId)->first();
    }


    /**
     * @param User $TransferFromUser
     * @param User $TransferToUser
     * @param string $Currency
     * @param $amount
     * @return mixed
     * @throws \Throwable
     * @Purpose Fund-transfer to different account.
     * @deprecated on 20-05-21 by dev-Mubashar as we have to set the fundtransfer on pending state at first
     */
    public static function fundTransfer(User $TransferFromUser, User $TransferToUser, string $Currency, $amount)
    {
        if($Currency != 'USD' && $Currency != 'RSC'){
            return back()->with('errormsg', 'Crypto transfers not allowed.');
        }
        return DB::transaction(function () use ($TransferFromUser, $TransferToUser, $Currency, $amount) {
            if ($TransferFromUser->id == $TransferToUser->id) {
                return back()->with('errormsg', 'CashBox is not allowing to transfer an amount to your own account.');
            }

            ## Check Cash box account is exits of users, who is transferring and whom to transferring.
            $TransferFromCashBoxAccount = $TransferFromUser->getCashBoxAccountOfSpecificCurrency($Currency);
            $TransferToCashBoxAccount = $TransferToUser->getCashBoxAccountOfSpecificCurrency($Currency);
            $CBAccounts = self::getCashBoxCurrentBalanceByCurrencyCode($Currency);
            if ($CBAccounts <= 0 || $amount > $CBAccounts) {
                return back()->with('errormsg', 'Current Balance is not enough to transfer');
            }
            if (($TransferFromCashBoxAccount instanceof CBAccounts) && ($TransferToCashBoxAccount instanceof CBAccounts)) {
                ## Create debit.
                $CBDebit = CBDebits::createDebit($TransferFromCashBoxAccount->id, $amount, 0, 'balance', 'transfer', 'Internal Transfer To B4U User #' . $TransferToUser->u_id,
                    'approved', false, null, $TransferToCashBoxAccount->id, $TransferToUser->id);
                if ($CBDebit instanceof CBDebits && $CBDebit->id) {
                    CreateCreditFundTransfer::dispatch($CBDebit->id, $TransferFromUser->id);
                } else {
                    Log::error('In-Transfer, CashBox is trying to create CashBox Debit but failed to Create Debit.');
                }
                return redirect()->route('cash-box.show', [$TransferFromCashBoxAccount->id])->with('successmsg', $amount . ' ' . $Currency . ' has been successfully transfer to ' . $TransferToUser->u_id);
            } else {
                throw new \Exception('Receiver Cash Box Account is not created');
            }
        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));
    }

    public static function balanceCalculation(int $cbAccountId)
    {
        $balanceCalculation = DB::select(DB::raw("SELECT id as cb_account_id,current_balance,locked_balance, COALESCE((SELECT sum(amount + fee) as total from cb_debits where cb_debits.cb_account_id = cb_accounts.id and cb_debits.status != 'approved' and cb_debits.status != 'failed' and cb_debits.status != 'deposited' and cb_debits.status != 'cancelled'),0) as expected_locked,
(COALESCE((SELECT sum(amount + fee) as total from cb_credits where cb_credits.cb_account_id = cb_accounts.id and cb_credits.status = 'approved'),0) - COALESCE((SELECT sum(amount + fee) as total from cb_debits where cb_debits.cb_account_id = cb_accounts.id and cb_debits.status != 'failed'  and cb_debits.status != 'cancelled'),0) ) as expected_current
from cb_accounts WHERE cb_accounts.id ='" . $cbAccountId . "'"));
        return $balanceCalculation[0];
    }

    /**
     * @param User $TransferFromUser
     * @param User $TransferToUser
     * @param string $Currency
     * @param $amount
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public static function newFundTransfer(User $TransferFromUser, User $TransferToUser, string $Currency, $amount,string $status = 'pending')
    {
        if($Currency != 'USD' && $Currency != 'RSC' && $Currency != 'USDT' ){
            return back()->with('errormsg', 'Crypto transfers not allowed.');
        }
        return DB::transaction(function () use ($TransferFromUser, $TransferToUser, $Currency, $amount,$status) {
            if ($TransferFromUser->id == $TransferToUser->id) {
                return back()->with('errormsg', 'CashBox is not allowing to transfer an amount to your own account.');
            }

            ## Check Cash box account is exits of users, who is transferring and whom to transferring.
            $TransferFromCashBoxAccount = $TransferFromUser->getCashBoxAccountOfSpecificCurrency($Currency);
            $TransferToCashBoxAccount = $TransferToUser->getCashBoxAccountOfSpecificCurrency($Currency);
            $CBAccounts = self::getCashBoxCurrentBalanceByCurrencyCode($Currency);
            if ($CBAccounts <= 0 || $amount > $CBAccounts) {
                return back()->with('errormsg', 'Current Balance is not enough to transfer');
            }
            if (($TransferFromCashBoxAccount instanceof CBAccounts) && ($TransferToCashBoxAccount instanceof CBAccounts)) {
                ## Create debit.
                $CBDebit = CBDebits::createDebit($TransferFromCashBoxAccount->id, $amount, 0, 'balance', 'transfer', 'Internal Transfer To B4U User #' . $TransferToUser->u_id,
                    $status, true, null, $TransferToCashBoxAccount->id, $TransferToUser->id);
                if ($CBDebit instanceof CBDebits && $CBDebit->id) {
                    CashboxGenerateReport::dispatch($TransferFromUser->u_id,$TransferFromCashBoxAccount->id,$CBDebit->id,$TransferFromUser->id);
                  //  self::CashboxGenerateReport($TransferFromUser->u_id,$TransferFromCashBoxAccount->id,$CBDebit->id);
                    CreateCreditFundTransfer::dispatch($CBDebit->id, $TransferFromUser->id,$status);
                } else {
                    Log::error('In-Transfer, CashBox is trying to create CashBox Debit but failed to Create Debit.');
                }
                return redirect()->route('cash-box.show', [$TransferFromCashBoxAccount->id])->with('successmsg', $amount . ' ' . $Currency . ' has been successfully transfer to ' . $TransferToUser->u_id);
            } else {
                throw new \Exception('Receiver Cash Box Account is not created');
            }
        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));
    }


}


