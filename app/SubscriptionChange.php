<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SubscriptionChange extends Model
{
    //


    public function subscribeUser($plan_id = 1, $user_id, $payment_mood = null, $package_type = 'call', $start_at, $end_at, $amount = 0, $currency, $remarks = '')
    {
        $new_subscriber = new self();
        $new_subscriber->plan_id = $plan_id;
        $new_subscriber->user_id = $user_id;
        $new_subscriber->payment_mode = $payment_mood;
        $new_subscriber->package_type = $package_type;
        $mytime = Carbon::now();
        $mytime->toDateTimeString();
        $new_subscriber->start_at = $mytime;
        $new_subscriber->end_at = $mytime;
        $new_subscriber->amount = $amount;
        $new_subscriber->currency = $currency;
        $new_subscriber->remarks = $remarks;
        $new_subscriber->is_exp = 0;
        $new_subscriber->save();
        if ($new_subscriber) {
            return $new_subscriber;
        } else {
            return false;
        }


    }
}
