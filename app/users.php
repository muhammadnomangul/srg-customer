<?php

namespace App;

use App\Http\Controllers\UsersController;
use App\WebHook\IntegrationWithCallCenterService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\ParameterBag;

class users extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password', 'u_id', 'parent_id', 'plan', 'bank_name', 'account_name', 'account_no', 'acc_hold_No', 'phone_no', 'photo',
        'Country', 'kin_bank_info', 'btc_address', 'eth_address', 'bch_address',
        'ltc_address', 'xrp_address', 'dash_address', 'zec_address', 'rsc_address','usdt_address','usa_account_holder_name','date_of_birth','full_address','full_bank_name','routing_number','bank_account_type','institute_bank_number','transit_number','iban_number'
    ];

    public static function findOrUpdate($id, $data)
    {
        $Model = new users;
        $Model->find($id)->update($data);
    }

    public static function InsertUserToCallCenterService($userId, string $u_id, $phone_no)
    {
        $endPoint = "insertUser";
        $payLoad = [
          'user_id' =>  $userId,
          'u_id' =>  $u_id,
          'phone_no' =>  $phone_no,
            'api_token' => "ksand23jkr4o#$@sdn#nasd"
        ];
     return  IntegrationWithCallCenterService::send($endPoint,$payLoad);
    }

    public static function updateCallPackageOnCallCenterService(int $userId, bool  $isCallPackage)
    {
        $endPoint = "updateCallPackage";
        $payLoad = [
            'user_id' =>  $userId,
            'is_call_sub' =>  $isCallPackage,
            'api_token' => "ksand23jkr4o#$@sdn#nasd"
        ];
          IntegrationWithCallCenterService::send($endPoint,$payLoad);
    }

    public static function updatePhoneNoOnCallCenterService(int $userId, string  $phoneNo)
    {
        $endPoint = "updatePhoneNo";
        $payLoad = [
            'user_id' =>  $userId,
            'phone_no' =>  $phoneNo,
            'api_token' => "ksand23jkr4o#$@sdn#nasd"
        ];
        IntegrationWithCallCenterService::send($endPoint,$payLoad);
    }

    private static function blockChange($user)
    {
        if (!empty($user->getOriginal('name'))) {
            $user->setAttribute('name', $user->getOriginal('name'));
        }

//        if (!empty($user->getOriginal('email'))) {
//            $user->setAttribute('email', $user->getOriginal('email'));
//        }

        if (!empty($user->getOriginal('u_id'))) {
            $user->setAttribute('u_id', $user->getOriginal('u_id'));
        }
        if (!empty($user->getOriginal('parent_id'))) {
            $user->setAttribute('parent_id', $user->getOriginal('parent_id'));
        }
        if (!empty($user->getOriginal('bank_name'))) {
            $user->setAttribute('bank_name', $user->getOriginal('bank_name'));
        }
        if (!empty($user->getOriginal('account_name'))) {
            $user->setAttribute('account_name', $user->getOriginal('account_name'));
        }
        if (!empty($user->getOriginal('account_no'))) {
            $user->setAttribute('account_no', $user->getOriginal('account_no'));
        }
        if (!empty($user->getOriginal('acc_hold_No'))) {
            $user->setAttribute('acc_hold_No', $user->getOriginal('acc_hold_No'));
        }
        if (!empty($user->getOriginal('phone_no'))) {
            $user->setAttribute('phone_no', $user->getOriginal('phone_no'));
        }
        if (!empty($user->getOriginal('Country'))) {
            $user->setAttribute('Country', $user->getOriginal('Country'));
        }

        //CryptoAddresses
        if (!empty($user->getOriginal('btc_address'))) {
            $user->setAttribute('btc_address', $user->getOriginal('btc_address'));
        }
        if (!empty($user->getOriginal('eth_address'))) {
            $user->setAttribute('eth_address', $user->getOriginal('eth_address'));
        }
        if (!empty($user->getOriginal('bch_address'))) {
            $user->setAttribute('bch_address', $user->getOriginal('bch_address'));
        }
        if (!empty($user->getOriginal('ltc_address'))) {
            $user->setAttribute('ltc_address', $user->getOriginal('ltc_address'));
        }
        if (!empty($user->getOriginal('dash_address'))) {
            $user->setAttribute('dash_address', $user->getOriginal('dash_address'));
        }
        if (!empty($user->getOriginal('xrp_address'))) {
            $user->setAttribute('xrp_address', $user->getOriginal('xrp_address'));
        }
        if (!empty($user->getOriginal('zec_address'))) {
            $user->setAttribute('zec_address', $user->getOriginal('zec_address'));
        }
        if (!empty($user->getOriginal('rsc_address'))) {
            $user->setAttribute('rsc_address', $user->getOriginal('rsc_address'));
        }
    }

    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        self::updating(function ($model) {
//            self::blockChange($model);
        });
        self::saving(function ($model) {
//            self::blockChange($model);
        });

    }

    public function gh()
    {
        return $this->hasMany('App\gh', 'donation_from');
    }

    public function ruser()
    {
        return $this->hasMany('App\gh', 'donation_to');
    }

    public function dp()
    {
        return $this->hasMany('App\deposits', 'user');
    }

    public function wd()
    {
        return $this->hasMany('App\withdrawals', 'user');
    }

    public function dplan()
    {
        return $this->belongsTo('App\plans', 'plan');
    }

    public function sd()
    {
        return $this->belongsTo('App\solds', 'user');
    }

    public function BonusHistory()
    {
        return $this->hasMany('App\bonus_history', 'created_by');
    }

    public function verifiedBy()
    {
        return $this->hasOne("App\admin_logs", 'user_id');
    }


    /**
     * Get the user's phone number.
     *
     * @param string $value
     * @return string
     */
//    public function getPhoneNoAttribute($value)
//    {
//        $value = str_replace('+920', '+923', $value);
//        $value = str_replace('03', '+923', $value);
//        $value = str_replace(' ', '', $value);
//        $value = str_replace('+920092', '+92', $value);
//
//        return $value;
//    }

    /**
     * set the user's phone number.
     *
     * @param string $value
     * @return string
     */
    public function setPhoneNoAttribute($value)
    {
        $value = str_replace('+920', '+923', $value);
        $value = str_replace('03', '+923', $value);
        $value = str_replace(' ', '', $value);
        $value = str_replace('+920092', '+92', $value);
        $this->attributes['phone_no'] = $value;
    }

    public static function getUserDetails($userId)
    {
        return User::find($userId);
    }

}
