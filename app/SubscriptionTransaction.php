<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTransaction extends Model
{
    //
    protected $table = 'subscription_charges_transaction';


    public function storeTransaction($sub_id,$trans_id,$trans_type){
        $transaction = new self();
        $transaction->subscription_id = $sub_id;
        $transaction->transaction_media_id = $trans_id;
        $transaction->transaction_media_type = $trans_type;
        $transaction->save();
        if($transaction){
            return true;
        }else{
            return false;
        }


    }
}
