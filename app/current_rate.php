<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class current_rate extends Model
{
    //current Rate
    /**
     * @var int[]
     */
    private static $weekDaysInMonth2021 = [21,20,23,22,21,22,22,22,22,21,22,23];
    public $table = "current_rate";
 //   protected $connection = 'mysql3';


    public static function profitMonthlyLimit(){
        $todayBtcRate = self::first()->rate_btc;
        $prevBtcRate = currency_rates::orderBy('id','DESC')->first()->rate_btc;

        if($todayBtcRate >= $prevBtcRate){
            $percentage = (($todayBtcRate - $prevBtcRate)/$prevBtcRate) * 100;
            $result =   self::mappingPercentage($percentage,true);
        }else{
            $percentage = (($prevBtcRate - $todayBtcRate)/$prevBtcRate) * 100;
            $result =   self::mappingPercentage($percentage);
        }
         current_rate::whereId(1)->update(['today_profit' => $result]);
       return $result;
    }

    private static function mappingPercentage($percentage,bool $increment = false)
    {
        $settings = settings::first();
       $maxMonthlyProfitLimit = $settings->max_profit_limit;
       $minMonthlyProfitLimit =  $settings->min_profit_limit;

        $avgMonthlyProfitLimit = ($maxMonthlyProfitLimit + $minMonthlyProfitLimit)/2;
        $newProfitMappingRange = $maxMonthlyProfitLimit - $avgMonthlyProfitLimit;
        $newProfitMappingValue = ($newProfitMappingRange/100) * $percentage;
        $newProfitMappingValue1 = $newProfitMappingValue;

        if($newProfitMappingValue > $newProfitMappingRange){
            $newProfitMappingValue = $newProfitMappingRange ;
        }

        $daysInCurrMonth = now()->month - 1;
        $daysInCurrMonth = self::$weekDaysInMonth2021[$daysInCurrMonth];
        if($increment){
            $todayProfit = ($avgMonthlyProfitLimit + $newProfitMappingValue)/$daysInCurrMonth ;
        }else{
            $todayProfit = ($avgMonthlyProfitLimit - $newProfitMappingValue)/$daysInCurrMonth ;
        }
        $todayProfitSet = round($todayProfit,2);
        app('App\Http\Controllers\Controller')->adminLogs('', '', '', 'TodayProfitAdded_' . $todayProfitSet. '_PercentageIncrease_' .$percentage. '_ExpectedMapValue_' .$newProfitMappingValue1.
            '_LimitMapValue_' .$newProfitMappingValue. '_todayProfit_' .$todayProfit );
        return $todayProfitSet;
    }

}