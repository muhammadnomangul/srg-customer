<?php

use App\Banks;
use App\Currencies;
use App\FixDepositHistory;
use App\FixDepositTenure;
use App\User;
use App\daily_investment_bonus;
use App\deposits;
use App\settings;
use App\users;
use App\withdrawals;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

defined('SITE_NORMAL_USER') or define('SITE_NORMAL_USER', 0);
defined('SITE_ADMIN') or define('SITE_ADMIN', 1);
defined('SITE_MANAGER') or define('SITE_MANAGER', 2);
defined('SITE_AGENT') or define('SITE_AGENT', 3);
defined('SITE_COMMITTEE') or define('SITE_COMMITTEE', 4);
defined('SITE_COUNTRY_MANAGER') or define('SITE_COUNTRY_MANAGER', 5);
defined('SITE_SUPER_ADMIN') or define('SITE_SUPER_ADMIN', 100);

// site_settings added - Xee
if (!function_exists('site_settings')) {
    function site_settings()
    {
        return settings::getSettings();
    }
}


if (!function_exists('is_user')) {
    // this funtion get the u_id of user and return the user object
    function is_user($type, $user)
    {
        try {
            if ($user instanceof User) {
                switch ($type) {
                    case 'normal_user':
                        return $user == SITE_NORMAL_USER;
                    case 'admin':
                        return $user == SITE_ADMIN;
                    case 'manager':
                        return $user == SITE_MANAGER;
                    case 'agent':
                        return $user == SITE_AGENT;
                    case 'committee':
                        return $user == SITE_COMMITTEE;
                    case 'country_manager':
                        return $user == SITE_COUNTRY_MANAGER;
                    default:
                        return false;
                }
            }
        } catch (\Exception $e) {
            \Log::error('helper function is_user error ' . $e->getMessage());
            return false;
        }
    }
}
//  use Exception,Log;
if (!function_exists('getUserDetails')) {
    // this funtion get the u_id of user and return the user object
    function getUserDetails($id)
    {
        try {
            return User::where('u_id', $id)->first();
        } catch (\Exception $e) {
            \Log::error('helper function getUserDetails error ' . $e->getMessage());

            return null;
        }
    }
}
if (!function_exists('getUserDetailsById')) {
    // this funtion get the u_id of user and return the user object
    function getUserDetailsById($id)
    {
        try {
            return User::where('id', $id)->first();
        } catch (\Exception $e) {
            \Log::error('helper function getUserDetails error ' . $e->getMessage());

            return null;
        }
    }
}

if (!function_exists('getBankDetails')) {
    // this funtion get the u_id of user and return the user object
    function getBankDetails($id)
    {
        try {
            return Banks::where('id', $id)->first();
        } catch (\Exception $e) {
            \Log::error('helper function getBankDetails error ' . $e->getMessage());

            return null;
        }
    }
}

if (!function_exists('getFixedDepositPlans')) {
    // this funtion return all the plan for fixed deposit
    function getFixedDepositPlans()
    {
        try {
            return FixDepositTenure::get();
        } catch (\Exception $e) {
            \Log::error("helper function getFixedDepositPlans error " . $e->getMessage());
            return null;
        }
    }
}

if (!function_exists('notFixedDeposit')) {
    // this funtion return the deposit that is within fixed deposit tenure
    function notFixedDeposit($deposit_id)
    {
        try {
            $deposit = FixDepositHistory::where("deposit_id", $deposit_id)->orderBy("id", "DESC")->first();
            if ($deposit) {
                $created_date = Carbon::parse($deposit->created_at)->format('Y-m-d');
                $enddate = Carbon::parse($deposit->end_date)->format('Y-m-d');
                if ($enddate > $created_date) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (\Exception $e) {
            \Log::error("helper function notFixedDeposit error " . $e->getMessage());
            return true;
        }
    }
}

//deduct fee if same day deposits are more then the limit set in the fees
if (!function_exists('feeDeductOnDailyDeposit')) {
    function feeDeductOnDailyDeposit($total_amount, $UserId = null)
    {
        try {
            $amount = 0;
            $deposits = deposits::whereDate('created_at', Carbon::today())
                ->where('status', 'NOT LIKE', 'Cancelled')->whereUserId($UserId)->get();
            if ($deposits) {
                $amount = $deposits->sum('total_amount');
            }
            $amount = $amount + $total_amount;
            return $amount;
//            return $total_amount;
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Log::error("helper function feeDeductOnDailyDeposit error " . $e->getMessage());
            return 0;
        }
    }
}

//deduct fee if same day widhtdraw fund are more then the limit set in the fees
if (!function_exists('feeDeductOnDailyFundTransfer')) {
    function feeDeductOnDailyFundTransfer($total_amount)
    {
        try {
            $amount = 0;
            $withdrawals = withdrawals::whereDate('created_at', Carbon::today())->whereUser(Auth::user()->id)->where("withdrawal_fee", 0)->get();

            if ($withdrawals) {
                $amount = $withdrawals->sum('amount');
            }

            $amount = $amount + $total_amount;
            return $amount;
        } catch (\Exception $e) {
            \Log::error("helper function feeDeductOnDailyFundTransfer error " . $e->getMessage());
            return 0;
        }
    }
}

// this funtion return totalInvestmentBonus of specific parent using u_id
if (!function_exists('totalInvestmentBonus')) {
    function totalInvestmentBonus($user_id)
    {
        try {
            return daily_investment_bonus::where('parent_id', '=', $user_id)->sum('bonus');
        } catch (\Exception $e) {
            \Log::error("helper function totalInvestmentBonus error " . $e->getMessage());
            return null;
        }
    }
}

// this funtion return profitBonus of specific parent using u_id

if (!function_exists('profitBonus')) {
    function profitBonus($user_id)
    {
        try {
            return daily_investment_bonus::where('parent_id', '=', $user_id)->where('details', 'LIKE', "Profit Bonus")->sum('bonus');
        } catch (\Exception $e) {
            \Log::error("helper function profitBonus error " . $e->getMessage());
            return null;
        }
    }
}


// this funtion return investmentBonus of specific parent using u_id
if (!function_exists('investmentBonus')) {
    function investmentBonus($user_id)
    {
        try {
            return daily_investment_bonus::where('parent_id', '=', $user_id)->where('details', 'NOT LIKE', "Profit Bonus")->sum('bonus');
        } catch (\Exception $e) {
            \Log::error("helper function investmentBonus error " . $e->getMessage());
            return null;
        }
    }
}

// this funtion return all currencies code
if (!function_exists('currencies')) {
    function currencies()
    {
        try {
            return Currencies::where('status', 'active')->get();
        } catch (\Exception $e) {
            \Log::error("helper function currencies error " . $e->getMessage());
            return null;
        }
    }
}

///get trans types list
if (!function_exists('getTransTypes')) {
    function getTransTypes()
    {
        try {
            $types = array("1" => "NewInvestment", "2" => "Reinvestment");
            return $types;
        } catch (\Exception $e) {
            \Log::error("helper function getTransTypes error " . $e->getMessage());
            return null;
        }
    }
}


///get re invest types list
if (!function_exists('getReinvestTypes')) {
    function getReinvestTypes()
    {
        try {
            $types = array("2" => "Sold", "3" => "Bonus", "4" => "Profit", "5" => "FundTransfer");
            return $types;
        } catch (\Exception $e) {
            \Log::error("helper function getReinvestTypes error " . $e->getMessage());
            return null;
        }
    }
}


///get status types list
if (!function_exists('getDepositStatus')) {
    function getDepositStatus()
    {
        try {
            $types = array("1" => 'Approved', "2" => "Pending", "3" => "Sold", "4" => "Cancelled", "5" => "Deleted");
            return $types;
        } catch (\Exception $e) {
            \Log::error("helper function getDepositStatus error " . $e->getMessage());
            return null;
        }
    }
}


///get pre status types list
if (!function_exists('getDepositPreStatus')) {
    function getDepositPreStatus()
    {
        try {
            $types = array("1" => 'Approved', "2" => "Pending", "3" => "Sold", "4" => "Cancelled", "5" => "New", "6" => "Partial", "7" => "FundReceived");
            return $types;
        } catch (\Exception $e) {
            \Log::error("helper function getDepositPreStatus error " . $e->getMessage());
            return null;
        }
    }
}

///get payment mode list
if (!function_exists('getPaymentModeList')) {
    function getPaymentModeList()
    {
        try {
            $types = array("1" => 'Bitcoin', "2" => "Ethereum", "3" => "Bank transfer", "4" => "Etherum", "5" => "Credit card", "6" => "Paypal", "7" => "Bitcash", "8" => "Litecoin", "9" => "Ripple", "10" => "Dash", "11" => "USD", "12" => "Zeecash", "13" => "FundTransfer");
            return $types;
        } catch (\Exception $e) {
            \Log::error("helper function getPaymentModeList error " . $e->getMessage());
            return null;
        }
    }
}
if (!function_exists('getMailFromAddress')) {
    // this funtion get the u_id of user and return the user object
    function getMailFromAddress()
    {
        return config('mail.from.address');
    }
}
if (!function_exists('getSupportMailFromAddress')) {
    // this funtion get the u_id of user and return the user object
    function getSupportMailFromAddress()
    {
        return config('mail.from.support_address');
    }
}
if (!function_exists('getDevelopersMailAddresses')) {
    // this funtion get the u_id of user and return the user object
    function getDevelopersMailAddresses()
    {
        return config('mail.developers.mail');
    }
}
if (!function_exists('getMailFromName')) {
    // this funtion get the u_id of user and return the user object
    function getMailFromName()
    {
        return config('mail.from.name');
    }
}
if (!function_exists('sendEmailToDeveloper')) {
    // this funtion get the u_id of user and return the user object
    function sendEmailToDeveloper($user_email, $percentage, $date, $startEnd, $subject = "Profit")
    {
        // Send Emails to users
        if (isset($user_email) && isset($percentage) && isset($date)) {
            //$email_to         = $user_email;
            //$userName     = $user_name;

            $from_Name = getMailFromName();
            $from_email = getSupportMailFromAddress();
            $subject = "B4U Global " . $subject;
            if ($startEnd == "Successful") {
                $subject .= " Cron Job successfully Runs for Date ($date) and profit percentage $percentage %";
            } elseif ($startEnd == "Started") {
                $subject .= " Cron Job Started successfully! from B4U Global";
            } else {
                $subject .= " Cron Not Successful!";
            }
            $message =
                "<html>
    								 <body align=\"left\" style=\"height: 100%;\">
    									<div>
    										
    										<div>
    											<table style=\"width: 100%;\">
    												<tr>
    													<td style=\"text-align:left; padding:10px 0;\">
    														<h1>Dear Member B4U Global</h1>
    													</td>
    												</tr>";
            if ($startEnd == "Started") {
                $message .= "<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job Runs successfully and date is " . $date . ".</h1>
														</td>
													</tr>";
            } elseif ($startEnd == "Successful") {
                $message .= "<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job Completed successfully, and today Profit Percentage is " . $percentage . " % and date is " . $date . ".</h1>
														</td>
												</tr>";
            } else {
                $message .= "<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job not successfull, </h1>
															<p> Due to haveing Error : " . $startEnd . "</p>
														</td>
												</tr>";
            }

            $message .= "<tr>
														<td style=\"text-align:left; padding:10px 0;\">
    														Thanks
														</td>
													</tr>
													<tr>
    													<td style=\"padding:10px 0; text-align:left;\">
    														Your Sincerely,
    													</td>
    												</tr>
    												<tr>
    													<td style=\"padding:10px 0; text-align:left;\">
    														Team B4U GLOBAL
    														
    													</td>
    												</tr>
    												
    												
    											</table>
    										</div>
    									</div>
    								</body>
    							</html>";
            //echo $message;
            //exit;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:B4U Global Support <' . $from_email . '>' . "\r\n";
            // More headers info
            //$headers .= 'From: Noreply <noreply@b4uinvestors.cf>' . "\r\n";
            $headers .= 'Cc: mubixhb4u@gmail.com' . "\r\n";
            $headers .= 'Bcc: mkamranb4u@gmail.com' . "\r\n";

            // Send Emails to users
            $success = @mail($user_email, $subject, $message, $headers);
        }
    }
}
if (!function_exists('site_settings')) {
    function site_settings()
    {
        return settings::getSettings();
    }
}
## Send OTP to Non-Pakistan Users
## These function already exits in User Model at the time of Queue User is not logged in so, session is not created.
if (!function_exists('sendOtpToUserByCall')) {
    // this funtion get the u_id of user and return the user object
    function sendOtpToUserByCall($phoneNo, $randomCode, int $userId)
    {
        // http://18.188.99.93:88/call?&phoneNumber=${phone.code}${phone.number}&otp=${newsmsotp.getPin()}

        $RemoteEndPoint = 'http://18.188.99.93:88/call';
        $ClientRequest = new \GuzzleHttp\Client();
        $ClientResponse = $ClientRequest->request('GET', $RemoteEndPoint, [
            'query' => [
                'phoneNumber' => $phoneNo,
                'otp' => $randomCode,
            ]
        ]);

//        $RemoteEndPoint = 'http://bsms.its.com.pk/apiotp.php';
//        $ClientRequest = new \GuzzleHttp\Client();
//        $ClientResponse = $ClientRequest->request('GET', $RemoteEndPoint, [
//            'query' => [
//                'key' => '753bfbf780eec18ed5ac7fbd0e2cbdec',
//                'mobile' => $phoneNo,
//                'otp' => $randomCode,
//            ]
//        ]);

        if ($ClientResponse->getStatusCode() === 200) {
            ## Call Response..
            $responseBody = json_decode($ClientResponse->getBody());

            if (strtolower($responseBody) == 'ok') {
                increase_user_sms_credit($userId, 1);
            } else {
                Log::error('ErrorWhileCallingAtOtpVerification', [
                    'responseMessage' => json_encode($responseBody)
                ]);
            }
//            foreach ($responseBody as $body) {
//                if ($body->errorno == 0 and strtolower($body->status) == 'success') {
//
//                } else {
//
//                }
//            }
            return 'success';
        } else {
            Log::error('ErrorWhileCallingAtOtpVerification', [
                'responseMessage' => 'It seems Call link Api response is down and not responding to ' . $ClientResponse->getStatusCode()
            ]);
            return 'failed';
        }
    }


}
if (!function_exists('sendOtpToUserByCall2')) {
    // this funtion get the u_id of user and return the user object
    function sendOtpToUserByCall2($phoneNo, $randomCode, int $userId)
    {
        // http://18.188.99.93:88/call?&phoneNumber=${phone.code}${phone.number}&otp=${newsmsotp.getPin()}

        $RemoteEndPoint = 'http://18.188.99.93:88/call';
        $ClientRequest = new \GuzzleHttp\Client();
        $ClientResponse = $ClientRequest->request('GET', $RemoteEndPoint, [
            'query' => [
                'phoneNumber' => $phoneNo,
                'otp' => $randomCode,
            ]
        ]);

//        $RemoteEndPoint = 'http://bsms.its.com.pk/apiotp.php';
//        $ClientRequest = new \GuzzleHttp\Client();
//        $ClientResponse = $ClientRequest->request('GET', $RemoteEndPoint, [
//            'query' => [
//                'key' => '753bfbf780eec18ed5ac7fbd0e2cbdec',
//                'mobile' => $phoneNo,
//                'otp' => $randomCode,
//            ]
//        ]);

        if ($ClientResponse->getStatusCode() === 200) {
            ## Call Response..
            $responseBody = json_decode($ClientResponse->getBody());

            if (strtolower($responseBody) == 'ok') {
//                increase_user_sms_credit($userId, 1);
            } else {
                Log::error('ErrorWhileCallingAtOtpVerification', [
                    'responseMessage' => json_encode($responseBody)
                ]);
            }
//            foreach ($responseBody as $body) {
//                if ($body->errorno == 0 and strtolower($body->status) == 'success') {
//
//                } else {
//
//                }
//            }
            return 'success';
        } else {
            Log::error('ErrorWhileCallingAtOtpVerification', [
                'responseMessage' => 'It seems Call link Api response is down and not responding to ' . $ClientResponse->getStatusCode()
            ]);
            return 'failed';
        }
    }


}
##
if (!function_exists('increase_user_sms_credit')) {
    function increase_user_sms_credit($userId = null, $noOfSMS = 1)
    {
        ## get the user.
        $loggedInUser = users::find($userId);
        if ($loggedInUser instanceof User) {
            ## given valid user
            $loggedInUser->sms_payable_amount = ($loggedInUser->sms_payable_amount) + (config('b4uglobal.PER_SMS_CHARGES_TO_USER_BY_B4UGLOBAL') * $noOfSMS);
            $loggedInUser->save();
            return true;
        } else {
            return false;
        }
    }
}
##
if (!function_exists('is_valid_phone_no')) {
    ## this functions return valid phone no.
    function is_valid_phone_no($phoneNo, $countryCode)
    {
        try {
            $countryCode = strtoupper($countryCode);
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $swissNumberProto = $phoneUtil->parse($phoneNo, $countryCode);
            $internationalPhoneNoFormat = $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
            $phoneNo = str_replace(['-', '(', ')', ' ', '  '], '', $internationalPhoneNoFormat);
            return [
                'is_valid' => $phoneUtil->isValidNumberForRegion($swissNumberProto, $countryCode),
                'formatted_phone_no' => $phoneNo,
            ];
        } catch (Exception $exception) {
            Log::error('ErrorWhileValidatingPhoneNo', [
                'phone_no' => $phoneNo,
                'country_code' => $countryCode
            ]);
            return $exception->getMessage();
        }
    }
}
## get user group
if (!function_exists('getUserGroup')) {
    function getUserGroup(int $userId)
    {
        if($userId > config('b4uglobal.WORKER_LIMIT')) return  0;
        return ((int)($userId / config('b4uglobal.USER_PER_CB_WORKER')))+1;
    }
}
