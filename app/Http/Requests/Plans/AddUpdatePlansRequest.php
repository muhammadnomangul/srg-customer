<?php

namespace App\Http\Requests\Plans;

use App\Http\Requests\RequestConstants;
use App\User;
use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddUpdatePlansRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User) && (Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable|' . RequestConstants::PlansIdValidationRequired,
            'name' => RequestConstants::AlphaNumericWhiteSpaceWithoutCharRegex,
            'price' => RequestConstants::AmountRegexRequired,
            'personel_investment_limit' => RequestConstants::AmountRegexRequired,
            'structural_investment_limit' => RequestConstants::AmountRegexRequired,
            'return' => RequestConstants::AmountRegexRequired,
            'type' => 'required|in:Main',
            'increment_type' => 'required|in:0',
            'increment_interval' => 'required|in:0',
            'increment_amount' => RequestConstants::AmountRegexRequired,
            'expiration' => 'required|in:expiration',

            'first_line' => RequestConstants::AmountRegexRequired,
            'second_line' => RequestConstants::AmountRegexRequired,
            'third_line' => RequestConstants::AmountRegexRequired,
            'fourth_line' => RequestConstants::AmountRegexRequired,
            'fifth_line' => RequestConstants::AmountRegexRequired,
            'first_pline' => RequestConstants::AmountRegexRequired,
            'second_pline' => RequestConstants::AmountRegexRequired,
            'fourth_pline' => RequestConstants::AmountRegexRequired,
            'third_pline' => RequestConstants::AmountRegexRequired,
            'fifth_pline' => RequestConstants::AmountRegexRequired,
        ];
    }
}
