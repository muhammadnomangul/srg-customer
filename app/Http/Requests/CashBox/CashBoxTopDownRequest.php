<?php

namespace App\Http\Requests\CashBox;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CashBoxTopDownRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => RequestConstants::cashBoxAmountValidation(null),
            'currency' => RequestConstants::cashBoxAccountIdValidation()
        ];
    }

    public function messages()
    {
        return [
            'amount.min' => 'B4U Global allow, Minimum $1 equivalent transaction.',
            'amount.max' => 'You have insufficient Cash Box balance to make this transaction. ',
        ];
    }
}
