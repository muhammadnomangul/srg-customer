<?php

namespace App\Http\Requests\CashBox;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CbRatingUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::onlyNumberValidation,
            'transType' => 'required|in:cr,db|regex:/^[\w-]*$/u',
            'rate' => RequestConstants::ratingValidation,
            'comment' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex
        ];
    }
}
