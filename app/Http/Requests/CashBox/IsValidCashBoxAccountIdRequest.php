<?php

namespace App\Http\Requests\CashBox;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IsValidCashBoxAccountIdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'cash_box' => $this->cash_box,
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cash_box' => [
                'required',
                'numeric',
                'regex:/\d+/',
                Rule::exists('cb_accounts', 'id')->where('user_id', Auth::user()->id)->where('id', $this->cash_box)
            ],
        ];
    }


    public function messages()
    {
        return [
            'cashbox.required' => 'Invalid parameter has been passed.',
            'cashbox.numeric' => 'Invalid parameter has been passed.',
            'cashbox.regex' => 'Invalid parameter has been passed.',
            'cashbox.exists' => 'Invalid parameter has been passed',
        ];
    }
}
