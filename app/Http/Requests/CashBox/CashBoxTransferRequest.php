<?php

namespace App\Http\Requests\CashBox;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CashBoxTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validationsRule = [
            ## valid currency that we supports.
            'cb_account' => RequestConstants::CurrencyValidationRequired,
            'amount' => RequestConstants::cashBoxAmountValidation($this->cb_account),
        ];

        ## if fund_receivers_id have valid B4U user id
        if ($this->request->get('fund_receivers_id') !== 'notexist') {

            ## if fund receive id is equal to notexist then fund_receiver_id should have valid B4U user's valid u_id
            $validationsRule = array_merge($validationsRule, [
                'fund_receivers_id' => [
                    'required',
                    RequestConstants::B4UUIDRegex,
                    Rule::exists('fund_beneficiary', 'beneficiary_uid')->where('user_id', Auth::user()->id)->where('status', 0)
                ]
            ]);

        } else {

            ## if fund receiver id has null and user wants to add another user's u_id
            $validationsRule = array_merge($validationsRule, [
                'fund_receivers_id2' => RequestConstants::B4UUIDValidation
            ]);
        }

        return $validationsRule;
    }

    public function messages()
    {
        return [
            'cb_account.regex' => 'Invalid Cash box account passed',
            'cb_account.required' => 'Invalid Cash box account passed',
            'cb_account.exits' => 'Invalid Cash box account passed',
            'fund_receivers_id.required' => 'You are transferring to wrong, such User is not exits in the system',
            'fund_receivers_id.regex' => 'You are transferring to wrong, such User is not exits in the system',
            'fund_receivers_id2.regex' => 'You are transferring to wrong, such User is not exits in the system',
            'fund_receivers_id2.required' => 'You are transferring to wrong, such User is not exits in the system',
            'fund_receivers_id2.exits' => 'You are transferring to wrong, such User is not exits in the system',
            'fund_receivers_id.exits' => 'You are transferring to wrong, such User is not exits in the system',
            'amount.required' => 'Invalid Deposit amount',
            'amount.regex' => 'Invalid Deposit amount',
            'amount.between' => 'You have insufficient Cash Box balance to make this transaction. ',
            'amount.min' => 'B4U Global allow, Minimum $1 equivalent transaction.',
            'amount.max' => 'You have insufficient Cash Box balance to make this transaction. ',
        ];
    }
}
