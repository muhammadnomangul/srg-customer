<?php

namespace App\Http\Requests\CashBox;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CashBoxDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ## valid currency that we supports.
            'cb_account' => RequestConstants::CurrencyValidationRequired,
            'cb_amount' => RequestConstants::cashBoxAmountValidation($this->cb_account)
        ];
    }

    public function messages()
    {
        $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';

        return [
            'cb_account.regex' => 'Invalid Cash box account passed',
            'cb_account.required' => 'Invalid Cash box account passed',
            'cb_account.exits' => 'Invalid Cash box account passed',
            'cb_amount.required' => 'Invalid Deposit amount',
            'cb_amount.regex' => 'Invalid Deposit amount',
//            'cb_amount.min' => $msg,
            'cb_amount.max' => 'You have insufficient Cash Box balance to make this transaction. ',
        ];
    }
}
