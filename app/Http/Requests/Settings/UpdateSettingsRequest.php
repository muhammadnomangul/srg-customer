<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && (Auth::user()->is_super_admin == 1));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_name' => RequestConstants::AlphabetsRegex,
            'description' => RequestConstants::AlphaNumericWhiteSpaceWithoutCharRegex,
            'keywords' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'site_title' => RequestConstants::AlphabetsRegex,
            'rate_pkr' => RequestConstants::AmountRegexRequired,
            'currency' => RequestConstants::CurrencyValidationRequired,
            's_currency' => RequestConstants::CurrencyValidationRequired,
            'payment_mode1' => 'in:1',
            'payment_mode2' => 'in:2',
            'payment_mode3' => 'in:3',
            'site_address' => RequestConstants::URLRegex,
            'eth_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'btc_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'bch_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'ltc_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'xrp_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'dash_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'zec_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            's_s_k' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            's_p_k' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'pay_key' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'pay_key2' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'bank_name' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'account_name' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'account_number' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'update' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'ref_commission' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'deposit_limit' => RequestConstants::AmountRegex,
            'withdraw_limit' => RequestConstants::AmountRegex,
            'reinvest_limit' => RequestConstants::AmountRegex,
        ];
    }
}
