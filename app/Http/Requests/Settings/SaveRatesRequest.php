<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveRatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'today_profit' => RequestConstants::AmountRegexRequired,
            'rate_btc' => RequestConstants::AmountRegexRequired,
            'rate_bch' => RequestConstants::AmountRegexRequired,
            'rate_ltc' => RequestConstants::AmountRegexRequired,
            'rate_xrp' => RequestConstants::AmountRegexRequired,
            'rate_dash' => RequestConstants::AmountRegexRequired,
            'rate_zec' => RequestConstants::AmountRegexRequired,
        ];
    }
}
