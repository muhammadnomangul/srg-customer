<?php

namespace App\Http\Requests\UserPromotions;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveUserPromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => RequestConstants::B4UIDValidation,
            'user_u_id' => RequestConstants::B4UUIDValidation,
            'name' => RequestConstants::UserNameValidationRequire,
            'country' => RequestConstants::B4UCountryValidationRequire,
            'plan' => RequestConstants::AlphaNumericWhiteSpaceWithoutCharRegex,
            'level_1' => RequestConstants::AmountRegexRequired,
            'level_2' => RequestConstants::AmountRegexRequired,
            'level_3' => RequestConstants::AmountRegexRequired,
            'level_4' => RequestConstants::AmountRegexRequired,
            'total_investment' => RequestConstants::AmountRegexRequired,
            'from' => RequestConstants::DateValidationRequire,
            'to' => RequestConstants::DateValidationRequire,
            'price' => RequestConstants::AmountRegexRequired,
            'prize_date' => RequestConstants::DateValidationRequire,
            'remarks' => RequestConstants::AlphaNumericWhiteSpaceRegex,
        ];
    }
}
