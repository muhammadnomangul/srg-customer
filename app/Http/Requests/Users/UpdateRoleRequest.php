<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => RequestConstants::B4UIDValidation,
            'user_type' => 'required|in:manager,normal,post_check,committe_acc,dummy,awarded,update_parent',
            'parentid' => RequestConstants::B4UUIDValidation,
            'email' => RequestConstants::B4UEmailValidationRequired,
            'bank_acc_info' => RequestConstants::AlphaNumericWhiteSpaceWithoutCharRegex,

        ];
    }
}
