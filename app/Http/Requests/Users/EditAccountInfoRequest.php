<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditAccountInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => RequestConstants::UserNameValidationRequire,
//            'email' => 'required|email|max:255',
//            'phone_no' => RequestConstants::PhoneNoValidationRequire,
            'Country' => RequestConstants::B4UCountryValidationRequire,
//            'bank_name' => RequestConstants::BanksNameValidationRequired,
            'acc_hold_No' => RequestConstants::PhoneNoValidation,
            'account_name' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'account_no' => RequestConstants::AlphaNumericBlankRegex,
            'kin_bank_info' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            // 'id' => RequestConstants::B4UIDValidation,
        ];
    }
}
