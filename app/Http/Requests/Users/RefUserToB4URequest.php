<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RefUserToB4URequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => RequestConstants::UserNameValidationRequire,
            'email' => RequestConstants::EmailValidationRequire,
            'password' => RequestConstants::PasswordValidation,
            'password_confirmation' => 'required',
            'phone_no' => RequestConstants::PhoneNoValidationRequire,
        ];
    }

    public function messages()
    {
        return [
            'name.*' => 'Full name is invalid.',
            'email.*' => 'Email is invalid.',
            'password.*' => 'Password format is invalid.',
            'password_confirmation.*' => 'Confirm Password format is invalid.',
            'phone_no.*' => 'Phone number is invalid',
        ];
    }
}
