<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LoginToUserAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->is_super_admin === 1) {
            return true;
        } else {
            $logged_user = User::find($this->user_id);
            return ((Auth::user()->is_user(SITE_ADMIN) || Auth::user()->is_user(SITE_MANAGER)) && ($logged_user->is_user(SITE_NORMAL_USER) || $logged_user->type > 2));
        }
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'user_id' => $this->user_id
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => RequestConstants::B4UIDValidation
        ];
    }

}
