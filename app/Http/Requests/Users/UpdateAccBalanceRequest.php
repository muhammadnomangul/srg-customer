<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateAccBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => RequestConstants::B4UUIDValidation,
            'reference_bonus' => RequestConstants::AmountRegexRequired,
            'total_deduct' => RequestConstants::AmountRegexRequired,
            'balance_usd' => RequestConstants::AmountRegexRequired,
            'sold_bal_usd' => RequestConstants::AmountRegexRequired,
            'profit_usd' => RequestConstants::AmountRegexRequired,
            'waiting_profit_usd' => RequestConstants::AmountRegexRequired,
            'balance_btc' => RequestConstants::AmountRegexRequired,
            'sold_bal_btc' => RequestConstants::AmountRegexRequired,
            'profit_btc' => RequestConstants::AmountRegexRequired,
            'waiting_profit_btc' => RequestConstants::AmountRegexRequired,
            'balance_eth' => RequestConstants::AmountRegexRequired,
            'sold_bal_eth' => RequestConstants::AmountRegexRequired,
            'profit_eth' => RequestConstants::AmountRegexRequired,
            'waiting_profit_eth' => RequestConstants::AmountRegexRequired,
            'balance_bch' => RequestConstants::AmountRegexRequired,
            'sold_bal_bch' => RequestConstants::AmountRegexRequired,
            'profit_bch' => RequestConstants::AmountRegexRequired,
            'waiting_profit_bch' => RequestConstants::AmountRegexRequired,
            'balance_ltc' => RequestConstants::AmountRegexRequired,
            'sold_bal_ltc' => RequestConstants::AmountRegexRequired,
            'profit_ltc' => RequestConstants::AmountRegexRequired,
            'waiting_profit_ltc' => RequestConstants::AmountRegexRequired,
            'balance_xrp' => RequestConstants::AmountRegexRequired,
            'sold_bal_xrp' => RequestConstants::AmountRegexRequired,
            'profit_xrp' => RequestConstants::AmountRegexRequired,
            'balance_dash' => RequestConstants::AmountRegexRequired,
            'sold_bal_dash' => RequestConstants::AmountRegexRequired,
            'profit_dash' => RequestConstants::AmountRegexRequired,
            'waiting_profit_dash' => RequestConstants::AmountRegexRequired,
            'balance_zec' => RequestConstants::AmountRegexRequired,
            'sold_bal_zec' => RequestConstants::AmountRegexRequired,
            'profit_zec' => RequestConstants::AmountRegexRequired,
            'waiting_profit_zec' => RequestConstants::AmountRegexRequired,
        ];
    }
}
