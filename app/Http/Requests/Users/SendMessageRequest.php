<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User &&
         (Auth::user()->is_user(SITE_ADMIN) || Auth::user()->is_user(SITE_MANAGER) ));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uid' => RequestConstants::B4UIDValidation,
            'did'=> RequestConstants::DepositPrimaryIdValidation,
            'newstatus'=> RequestConstants::DepositStatusValidation,
        ];
    }
}
