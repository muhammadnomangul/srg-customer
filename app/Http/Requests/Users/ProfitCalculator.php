<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfitCalculator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ((Auth::user() instanceof User));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'approvedAt' => RequestConstants::DateValidationRequire,
            'soldAt' => RequestConstants::DateValidationRequire,
            'amount' => RequestConstants::AmountRegexRequired,
        ];
    }
}
