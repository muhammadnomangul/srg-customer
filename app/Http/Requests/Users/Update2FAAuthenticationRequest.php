<?php

namespace App\Http\Requests\Users;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Update2FAAuthenticationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $Rules = [
            'auth_type' => 'required|in:0,1,2,5,3'
        ];

        if ($this->auth_type == 4) {
            $Rules = array_merge($Rules, [
                'fP' => 'required|min:1|max:1',
                'sP' => 'required|min:1|max:1',
                'tP' => 'required|min:1|max:1',
                'fuP' => 'required|min:1|max:1',
                'fvP' => 'required|min:1|max:1',
                'sxP' => 'required|min:1|max:1',
            ]);
        }

        return $Rules;

    }

    public function messages()
    {
        return [
            'sP.required' => 'Secure Pin is incomplete',
            'fP.required' => 'Secure Pin is incomplete',
            'tP.required' => 'Secure Pin is incomplete',
            'fuP.required' => 'Secure Pin is incomplete',
            'fvP.required' => 'Secure Pin is incomplete',
            'sxP.required' => 'Secure Pin is incomplete',
        ];
    }

}
