<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditWalletAccountInformation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eth_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'btc_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
//            'btc_address' =>'nullable|regex:/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/u',
            'bch_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'ltc_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'xrp_address1' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'xrp_address2' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'dash_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'zec_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'rsc_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
            'usdt_address' => RequestConstants::AlphaNumericWhiteSpaceBlankRegex,
        ];
    }
}
