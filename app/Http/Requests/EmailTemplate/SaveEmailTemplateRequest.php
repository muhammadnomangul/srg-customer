<?php

namespace App\Http\Requests\EmailTemplate;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveEmailTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => RequestConstants::AlphaNumericWhiteSpaceRegex . '|required|unique:email_templates',
            'message' => RequestConstants::AlphaNumericWhiteSpaceRegex . '|required',
            'body' => RequestConstants::AlphaNumericWhiteSpaceRegex,
            'from_email' => 'required|email',
        ];
    }
}
