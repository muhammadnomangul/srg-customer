<?php

namespace App\Http\Requests\Investors;

use App\Http\Requests\RequestConstants;
use App\User;
use App\users;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TopMemberCalculationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => RequestConstants::DateValidationRequire,
            'to' => RequestConstants::DateValidationRequire,
        ];
    }
}
