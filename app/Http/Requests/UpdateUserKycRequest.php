<?php

namespace App\Http\Requests;

use App\Http\Controllers\SignedUrlUploadController;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UpdateUserKycRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->has('form')) {
            if ($this->get('form') == 'kyc') {
                $rules = [
//                    'mother_name' => 'required|regex:/^[\w\s-]*$/u|min:3|max:50',
                    'mother_name' => 'required|regex:/^[a-zA-Z\s]+$/|min:3|max:50',
                    'dob' => 'required|before:today|date',
                    'passport_fileurl' => $this->passport_fileurl ? RequestConstants::URLValidation : [],
                    'cnic_fileurl' => $this->cnic_fileurl ? RequestConstants::URLValidation : [],
                ];
            } elseif ($this->get('form') == 'nok') {
                $rules = [
//                    'nok_name' => 'required|regex:/^[\w\s-]*$/u|min:3|max:50',
//                    'nok_name' => 'required|alpha|regex:/^[\w\s-]*$/s|min:3|max:50',
                    'nok_name' => 'required|regex:/^[a-zA-Z\s]+$/|min:3|max:50',
//                    'nok_relation' => 'required|regex:/^[\w\s-]*$/u|min:3|max:50',
                    'nok_relation' => 'required',
                    'nok_contact_num' => 'required|regex:/^[0-9]+$/|min:10|max:20',
                ];
            }
        }
        return $rules;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nok_name.required' => 'A Name is required',
            'nok_relation.required' => 'A Relation is required',
            'nok_contact_num.required' => 'A Contact is required',
            'nok_contact_num.min' => 'Please provide a valid contact number'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function getValidRequest()
    {
        $user = Auth::user();
        $data = [];


        if ($this->get('form') == 'kyc') {
            $userUploadsPath = 'public/' . $user->id;
            $data['mother_name'] = $this->mother_name;
            $data['dob'] = date('Y-m-d', strtotime($this->dob));

            if ($this->has('passport_fileurl')) {
                $uploadedFile = SignedUrlUploadController::uploadImageToGoogleCloud($this->passport_fileurl, $userUploadsPath);
                if ($uploadedFile['status']) {
                    $data['passport'] = !empty($uploadedFile['result']['imageName']) ? $uploadedFile['result']['imageName'] : null;
                } else {
                    \Illuminate\Support\Facades\Log::error('Error while uploading file at KYC Passport upload.',
                        [
                            'aws-file-upload-url' => $this->passport_fileurl
                        ]);
                }
            }
            if ($this->has('cnic_fileurl')) {
                $uploadedFile = SignedUrlUploadController::uploadImageToGoogleCloud($this->cnic_fileurl, $userUploadsPath);
                if ($uploadedFile['status']) {
                    $data['cnic'] = !empty($uploadedFile['result']['imageName']) ? $uploadedFile['result']['imageName'] : null;
                } else {
                    \Illuminate\Support\Facades\Log::error('Error while uploading file at KYC CNIC upload.',
                        [
                            'aws-file-upload-url' => $this->cnic_fileurl
                        ]);
                }
            }

//            if ($this->has('cnic')) {
//                if (Storage::disk('local')->exists(@$user->kyc->cnic)) {
//                    Storage::disk('local')->delete($user->kyc->cnic);
//                }
//                $cnicImg = $this->file('cnic')->store($userUploadsPath);
//                $data['cnic'] = $cnicImg;
//            }
//            if ($this->has('passport')) {
//                if (Storage::disk('local')->exists(@$user->kyc->passport)) {
//                    Storage::disk('local')->delete($user->kyc->passport);
//                }
//                $passportImg = $this->file('passport')->store($userUploadsPath);
//                $data['passport'] = $passportImg;
//            }


        } elseif ($this->get('form') == 'nok') {
            $data['nok_name'] = $this->get('nok_name');
            $data['nok_relation'] = $this->get('nok_relation');
            $data['nok_contact_num'] = $this->get('nok_contact_num');
        }

        return $data;
    }
}
