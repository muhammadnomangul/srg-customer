<?php

namespace App\Http\Requests\Report;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }


    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'b4uid' => $this->b4uid,
            'iframe' => $this->iframe
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'b4uid' => RequestConstants::B4UUIDValidation,
            'iframe' => RequestConstants::AlphaNumericWhiteSpaceRegex
        ];
    }
}
