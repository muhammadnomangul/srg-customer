<?php

namespace App\Http\Requests\Deposits;

use App\Http\Controllers\UsersController;
use App\Http\Requests\RequestConstants;
use App\User;
use App\users;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MakeDepositViewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => RequestConstants::AmountRegexRequired,
            'deposit_mode' => RequestConstants::DepositModeRequired,
//            'payment_mode' => RequestConstants::DepositPaymentModeRequired(),
            'currency' => RequestConstants::CurrencyValidationRequired,
//            'plan_id' => RequestConstants::PlansIdValidationRequired,

        ];
    }
}
