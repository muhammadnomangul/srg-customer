<?php

namespace App\Http\Requests\Deposits;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ApproveDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && (Auth::user()->is_user(SITE_ADMIN) || Auth::user()->is_user(SITE_SUPER_ADMIN) || Auth::user()->is_user(SITE_MANAGER) || Auth::user()->is_user(SITE_COUNTRY_MANAGER)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::DepositPrimaryIdValidationRequired,
        ];
    }
}
