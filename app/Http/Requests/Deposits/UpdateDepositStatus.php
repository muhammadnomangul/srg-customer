<?php

namespace App\Http\Requests\Deposits;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateDepositStatus extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get data to be validated from the request.
     * @return array
     */
    public function validationData()
    {
        return [
            'id' => $this->id,
            'newstatus' => $this->newstatus,
            'type' => $this->type
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::DepositPrimaryIdValidationRequired,
            'newstatus' => RequestConstants::DepositStatusValidationRequired,
            'type' => 'required|in:deposit'
        ];
    }

    public function messages()
    {
        return [
            'id.exists' => 'No such withdrawal exits',
            'newstatus.regex' => 'Status should be Alphanumeric and between 0 to 1'
        ];
    }
}
