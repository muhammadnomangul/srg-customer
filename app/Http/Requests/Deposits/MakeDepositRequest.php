<?php

namespace App\Http\Requests\Deposits;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MakeDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationRules = [
            'payment_mode' => RequestConstants::DepositPaymentModeRequired(),
            'amount' => RequestConstants::AmountRegexRequired,
            'plan_id' => RequestConstants::PlansIdValidationRequired,
            'deposit_mode' => RequestConstants::DepositModeRequired,
            'currency' => RequestConstants::CurrencyValidationRequired,
        ];

        ## on reinvest validation will be different
        if ($this->deposit_mode == 'new') {
            $newDepositValidation = [
                'fileurl' => array_merge(RequestConstants::URLValidation, ['required']),
                'trans_id' => 'required|' . RequestConstants::AlphaNumericRegex,
            ];

            if ($this->currency == 'USD' && strtolower(Auth::user()->Country) == 'pakistan') {
                $newDepositValidation = array_merge($newDepositValidation, [
                    'bank_id' => RequestConstants::BanksValidationRequired,
                ]);
            }

            $validationRules = array_merge($validationRules, $newDepositValidation);
        } else {
            $validationRules = array_merge($validationRules, [
                'reinvest_type' => 'required|in:Bonus,Profit,Sold|regex:/^[\w-]*$/u'
            ]);
        }

        return $validationRules;

    }

    public function messages()
    {
        return [
            'fileurl.required' => 'Please select a deposit proof'
        ];
    }
}
