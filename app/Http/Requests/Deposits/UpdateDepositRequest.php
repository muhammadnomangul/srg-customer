<?php

namespace App\Http\Requests\Deposits;

use App\Http\Controllers\SignedUrlUploadController;
use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        ## allow only type 1 users.
        return (Auth::user() instanceof User && Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::DepositPrimaryIdValidationRequired,
            'amount' => RequestConstants::AmountRegexRequired,
            'currency' => RequestConstants::CurrencyValidationRequired,
            'total_amount' => RequestConstants::AmountRegexRequired,
            'profit_take' => RequestConstants::AmountRegexRequired,
            'approved_at' => 'date',
            'fee_deducted' => RequestConstants::AmountRegexRequired,
            'notes' => RequestConstants::AlphaNumericWhiteSpaceRegex,
        ];
    }


    public function getValidRequest()
    {
        $data['id'] = $this->id;
        $data['amount'] = $this->amount;
        $data['currency'] = $this->currency;
        $data['total_amount'] = $this->total_amount;
        $data['profit_take'] = $this->profit_take;
        $data['approved_at'] = $this->approved_at;
        $data['fee_deducted'] = $this->fee_deducted;
        $data['notes'] = $this->notes;
        return $data;
    }

}
