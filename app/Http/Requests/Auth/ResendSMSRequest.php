<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class ResendSMSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function validationData()
    {
        return [
            'id' => $this->id
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::B4UIDValidation,
        ];
    }
}
