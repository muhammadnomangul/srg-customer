<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordFormViewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'token' => $this->token,
            'email' => $this->email
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => RequestConstants::B4UEmailValidationRequired,
            'token' => 'required',
        ];
    }
}
