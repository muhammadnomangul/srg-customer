<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $captcha = 'required|captcha';
        if (config('app.env') == 'local' || env('is_allowed_for_long_ip', '0') == 1) {
            $captcha = '';
        }

        return [
           'g-recaptcha-response' => $captcha,
            'login' => 'required',
            'password' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'g-recaptcha-response.required' => 'Invalid Google Captcha'
        ];
    }

}
