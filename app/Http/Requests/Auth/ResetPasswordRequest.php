<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => RequestConstants::B4UEmailValidationRequired,
            'password' => 'required|regex:/(^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,}))/u|min:8|max:20|not_in:Password@@0123|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'password.not_in' => "Password@@0123 is an example password, please use another password which one is most similar to example password",
        ];
    }
}
