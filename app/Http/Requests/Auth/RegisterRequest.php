<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $auth = true;

        if (env('site_enable_for_admin', 0) == 1) {
            $auth = false;
        }

        return $auth;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $captcha = 'required|captcha';
        if (config('app.env') == 'local') {
            $captcha = '';
        }

        ## replace phone number
        $this->request->set('phone_no', str_replace(" ", "", trim($this->get('phone_no'))));

        ## rules
        return [
            'name' => RequestConstants::UserNameValidationRequire,
            'email' => RequestConstants::EmailValidationRequire,
            'phone_no' => RequestConstants::PhoneNoValidationRequire,
            'password' => RequestConstants::PasswordValidation,
            'sign' => 'regex:/^on/u|required',
            'g-recaptcha-response' => $captcha,
            'parent_id' => RequestConstants::B4UUIDValidation,
            'country_coe' => RequestConstants::CountryCodeNonRequireRegex,
            'country' => RequestConstants::B4UCountryValidationRequire,
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.regex' => 'Name should be Alphabets words',
            'g-recaptcha-response.required' => 'Invalid Google Captcha',
            'password.regex' => "Password should contain 8 character longer 1 UpperCase, 1 LowerCase, 1 Number & Special Character ",
            'sign.regex' => 'Please accept terms and condition',
            'sign.require' => 'Please accept terms and condition',
            'parent_id.regex' => "Invalid Parent ID, If Don't Know Any Use Our Default 'B4U0001'",
            'password.not_in' => "Password@@0123 is an example password, please use another password which one is most similar to example password",

        ];
    }
}
