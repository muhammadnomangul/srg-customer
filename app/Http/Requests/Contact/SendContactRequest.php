<?php

namespace App\Http\Requests\Contact;

use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class SendContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|' . RequestConstants::AlphaNumericWhiteSpaceRegex,
            'name' => 'required|' . RequestConstants::AlphaNumericWhiteSpaceWithoutCharRegex,
            'email' => 'required|email',
        ];
    }
}
