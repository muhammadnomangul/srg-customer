<?php

namespace App\Http\Requests\Albums;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveAlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && (Auth::user()->is_user(SITE_ADMIN)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => RequestConstants::AlbumNameValidationRequire,
            'description' => RequestConstants::AlphaNumericWhiteSpaceRequired,
            'photo' => NULL,
        ];
    }
}
