<?php

namespace App\Http\Requests;

use App\CBAccounts;
use App\Currencies;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RequestConstants
{

    ## URL Validations
    const URLRegex = 'regex:/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)/u';
    const URLValidation = [
        RequestConstants::URLRegex,
        'url',
        'active_url'
    ];

    const onlyNumberValidation = 'required|regex:/\d+/u|numeric';

    ## Currency validation.
    const CurrencyRegex = 'regex:/^[a-zA-Z]{3,4}$/u';
    const CurrencyValidationRequired = 'required|' . self::CurrencyRegex . '|exists:\App\Currencies,code';
    const CashBoxIDValidationRequired = 'required|' . self::onlyNumberValidation . '|exists:\App\CBAccounts,id';

    ## B4U user uid validation.
    const B4UUIDRegex = 'regex:/^B4U.\d*/u';
    const B4UUIDValidation = 'required|' . self::B4UUIDRegex . '|exists:\App\User,u_id';
    const B4UIDValidation = 'required|regex:/\d/u|exists:\App\User,id';
    const B4UEmailValidationRequired = 'required|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|exists:\App\User,email';
    const EmailValidationRequire = 'required|max:255|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users';


    ## Validation WithDrawl Record Id Rule
    const WithDrawlPrimaryIdValidationRequired = 'required|regex:/\d+/u|numeric|exists:withdrawals,id';
    const WithDrawlStatusValidationRequired = 'required|in:Dummy0,Dummy1,New,new,Cancelled,onHold,showWithdrawal,hideWithdrawal,Approved,approved,Pending,pending,Deleted,deleted,dummy1,dummy0,cancelled,onhold,showwithdrawal,hidewithdrawal|regex:/[a-zA-Z0-1]/u';


    ## Validation Deposit Record Id Rule
    const DepositPrimaryIdValidationRequired = 'required|regex:/\d+/u|numeric|exists:deposits,id';
    const DepositPrimaryIdValidation = 'nullable|regex:/\d+/u|numeric|exists:deposits,id';
    const DepositStatusValidationRequired = 'required|in:Dummy0,dummy0,Dummy1,dummy1,Cancelled,cancelled,Pending,pending|regex:/^[\w-]*$/u';
    const DepositStatusValidation = 'nullable|in:Dummy0,dummy0,Dummy1,dummy1,Cancelled,cancelled,Pending,pending|regex:/^[\w-]*$/u';
    const DepositModeRequired = 'required|in:new,reinvest,re-invest|regex:/^[\w-]*$/u';

    const DepositPaymentModeRequired = 'required|in:Bank transfer,Credit card,Paypal,|regex:/^[\w-]*$/u';
    const DepositReinvestRequired = 'required|in:Bonus,bonus,Profit,profit,Sold,sold|regex:/^[\w-]*$/u';

    ## Banks Validation Request
    const BanksValidationRequired = 'required|regex:/^[\d-]*$/u|exists:banks,id';
    const BanksNameValidationRequired = 'required|regex:/^[\w\s-]*$/u|exists:banks,bank_name';


    ## Other Regex
    const AmountRegex = 'regex:/^[+]?\d+([.]\d+)?$/u';
    const AmountRegexRequired = 'required|numeric|regex:/^[+]?\d+([.]\d+)?$/u';
    const CountryCodeNonRequireRegex = 'regex:/[a-zA-Z ]{2,}/u';


    const AlphabetsRegex = 'required|regex:/^[a-zA-Z]$/u';
    const AlphabetsNonRequireRegex = 'regex:/^[a-zA-Z]$/u';

    const AlphaNumericRegex = 'regex:/^[\w-]*$/u';
    const AlphaNumericBlankRegex = 'nullable|regex:/^[\w-]*$/u';
    const PhoneRegex = 'regex:/[\+\d]/u';
    const AlphaNumericWhiteSpaceBlankRegex = 'nullable|regex:/^[\.\,\@\w\s-]*$/u';
    const AlphaNumericWhiteSpaceRegex = 'regex:/^[\.\,\@\w\s-]*$/u';
    const AlphaNumericWhiteSpaceWithoutCharRegex = 'regex:/^[\w\s-]*$/u';

    ## Plans validation
    const PlansIdValidationRequired = 'required|regex:/\d+/u|numeric|exists:\App\plans,id';

    const PasswordValidation = 'required|regex:/(^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,}))/u|not_in:Password@@0123|min:8|max:20|confirmed';
    const UserNameValidationRequire = 'regex:/^[\p{L}\p{P}\p{Zs}]+$/u|required|max:255';
    const PhoneNoValidationRequire = 'required|regex:/[+0-9]/u';
    const PhoneNoValidation = 'nullable|regex:/[+0-9]/u';
    const B4UCountryValidationRequire = 'required|regex:/[a-zA-Z ]{2,}/u';
    const DonationPaymentModeValidation = 'required|in:Bonus,bonus,Profit,profit,Sold,sold';
    const DonationTypeValidation = 'required|in:0,1,2,3,4';

    ## Logs
    const LogsIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\Logs,id';

    ## Albums
    const AlbumsIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\Albums,id';
    const AlbumNameValidationRequire = 'required|regex:/^[\w\s-]*$/u';
    const AlphaNumericWhiteSpaceRequired = 'required|regex:/^[\.\,\@\w\s-]*$/u';

    ## Referral
    const ReferralsChildIdValidationRequired = 'required|regex:/\d/u|numeric|exists:referrals,child_id';

    ## Email Template
    const EmailTemplateIdValidationRequired = 'required|regex:/\d/u|numeric|exists:email_templates,id';

    ## Currency
    const   CurrencyIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\Currencies,id';

    ## OTP
    const OtpIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\OTPToken,id';
    const OtpTypeValidationRequired = 'required|in:withdrawal,account,re-invest,register,login,personal_info';

    ## Promotions
    const PromotionsIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\Promotion,id';
    const UserPromotionsIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\UserPromotion,id';

    const BonusHistoryIdValidationRequired = 'required|regex:/\d/u|numeric|exists:\App\bonus_history,id';


    const DateValidationRequire = 'required|date';

    const ratingValidation = 'required|regex:/\d+/u|numeric|in:0,1,2,3,4,5,6';

    public static function cashBoxAccountIdValidation()
    {
        return [
            'required',
            'numeric',
            'regex:/\d+/u',
            Rule::exists('cb_accounts', 'id')->where('user_id', Auth::user()->id)
        ];
    }

    ##Cashbbox Debit
    public static function cbDebitValidation()
    {
        return [
            'required',
            'numeric',
            'regex:/\d+/u',
            Rule::exists('cb_debits', 'id')->where('user_id', Auth::user()->id)
        ];
    }

    public static function DepositPaymentModeRequired()
    {
        $currencies = Currencies::getCurrencies('name');
        return 'required|in:Bank transfer,Credit card,Paypal' . implode(',', $currencies->toArray());
    }

    /**
     * @Purpose get CashBox Current balance.
     * @param $currencyCode
     * @return string
     */
    public static function cashBoxAmountValidation($currencyCode): string
    {
        $currentBalance = CBAccounts::getCashBoxCurrentBalanceByCurrencyCode($currencyCode);
        $minAmount = CBAccounts::getMinimumAmountToInvestFromCashBox($currencyCode);
//        return RequestConstants::AmountRegexRequired . '|min:' . $minAmount . '|max:' . $currentBalance;
        return RequestConstants::AmountRegexRequired . "|min:{$minAmount}|max:{$currentBalance}";
    }

}
