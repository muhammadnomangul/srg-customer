<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class GoogleAuthiSetupOneTimePasswordRequest extends FormRequest
{
    protected $redirect = "/dashboard/accountdetails";

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'one' => RequestConstants::onlyNumberValidation,
            'two' => RequestConstants::onlyNumberValidation,
            'three' => RequestConstants::onlyNumberValidation,
            'four' => RequestConstants::onlyNumberValidation,
            'five' => RequestConstants::onlyNumberValidation,
            'six' => RequestConstants::onlyNumberValidation,
            'secret' => RequestConstants::AlphaNumericRegex,
        ];
    }


    public function messages()
    {
        return [
            'one.required' => 'Invalid One Time Password',
            'one.regex' => 'Invalid One Time Password',
            'two.required' => 'Invalid One Time Password',
            'two.regex' => 'Invalid One Time Password',
            'three.required' => 'Invalid One Time Password',
            'three.regex' => 'Invalid One Time Password',
            'four.required' => 'Invalid One Time Password',
            'four.regex' => 'Invalid One Time Password',
            'five.required' => 'Invalid One Time Password',
            'five.regex' => 'Invalid One Time Password',
            'six.required' => 'Invalid One Time Password',
            'six.regex' => 'Invalid One Time Password',
        ];
    }
}
