<?php

namespace App\Http\Requests\Solds;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PartialSoldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'withdrawalAmount' => RequestConstants::AmountRegexRequired,
            'soldDeduct' => 'required|in:1',
            'deposit_id' => RequestConstants::DepositPrimaryIdValidationRequired,
        ];
    }
}
