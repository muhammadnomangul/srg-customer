<?php

namespace App\Http\Requests\Solds;

use App\Http\Requests\RequestConstants;
use App\Model\Deposit;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MakeSoldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $User = Auth::user();
        if ($User instanceof User && $User->type <> 3) {
            $Deposit = Deposit::find($this->val);
            if ($Deposit->user_id <> $User->id) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'val' => RequestConstants::DepositPrimaryIdValidationRequired
        ];
    }
}
