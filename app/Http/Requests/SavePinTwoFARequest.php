<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SavePinTwoFARequest extends FormRequest
{

    protected $redirect = "/dashboard/accountdetails";

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() instanceof User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'one' => RequestConstants::onlyNumberValidation,
            'two' => RequestConstants::onlyNumberValidation,
            'three' => RequestConstants::onlyNumberValidation,
            'four' => RequestConstants::onlyNumberValidation,
            'five' => RequestConstants::onlyNumberValidation,
            'six' => RequestConstants::onlyNumberValidation,
        ];
    }

    public function messages()
    {
        return [
            '*' => 'Invalid One Time Password',
//            'two.*' => 'Invalid One Time Password',
//            'three.*' => 'Invalid One Time Password',
//            'four.*' => 'Invalid One Time Password',
//            'five.*' => 'Invalid One Time Password',
//            'six.*' => 'Invalid One Time Password',
        ];
    }
}
