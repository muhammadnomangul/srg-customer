<?php

namespace App\Http\Requests\API\BFWallet\Users;

use App\Http\Requests\ApiFormRequest;
use App\Http\Requests\RequestConstants;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class GetUserInfoRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->headers->get('apisecret') == config('b4uglobal.TEMP_API_KEY'));
    }

    public function validationData()
    {
        return [
            'value' => $this->value
        ];
    }

    public function rules()
    {

        if (filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
            $validationRules = RequestConstants::B4UEmailValidationRequired;
        } else {
            $validationRules = RequestConstants::B4UIDValidation;
        }
        return [
            'value' => $validationRules
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'value.*' => 'Invalid user reference'
        ];
    }
}
