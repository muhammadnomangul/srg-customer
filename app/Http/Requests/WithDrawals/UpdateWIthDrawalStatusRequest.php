<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateWIthDrawalStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }


    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'id' => $this->id,
            'newstatus' => $this->newstatus,
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::WithDrawlPrimaryIdValidationRequired,
            'newstatus' => RequestConstants::WithDrawlStatusValidationRequired
        ];
    }

    public function messages()
    {
        return [
            'id.exists' => 'No such withdrawal exits',
            'newstatus.regex' => 'Status should be Alphanumeric and between 0 to 1'
        ];

    }

}
