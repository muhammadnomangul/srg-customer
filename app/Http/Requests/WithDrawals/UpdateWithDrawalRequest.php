<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateWithDrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && app('request')->session()->get('back_to_admin'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|' . RequestConstants::AmountRegex,
            'withdrawal_fee' => 'required|' . RequestConstants::AmountRegex,
            'status' => 'required|in:Pending,pending,Cancelled,cancelled',
            'id' => RequestConstants::WithDrawlPrimaryIdValidationRequired
        ];
    }
}
