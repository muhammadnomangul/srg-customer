<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BigAmountWithDrawlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return (Auth::user() instanceof User);
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'id' => $this->id,
            'Ids' => $this->request->get('Ids')
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('Ids')) {
            ## plural validation
            $validationRule = [
                'Ids' => 'required|array|min:1',
                'Ids.*' => RequestConstants::WithDrawlPrimaryIdValidationRequired
            ];
        } else {
            ## singular validation
            $validationRule = [
                'id' => RequestConstants::WithDrawlPrimaryIdValidationRequired
            ];
        }

        return $validationRule;
    }
}
