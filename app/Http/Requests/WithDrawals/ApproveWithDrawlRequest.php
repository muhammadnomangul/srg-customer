<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Twilio\Security\RequestValidator;

class ApproveWithDrawlRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && (Auth::user()->is_user(SITE_ADMIN) || Auth::user()->is_user(SITE_SUPER_ADMIN) || Auth::user()->is_user(SITE_MANAGER) || Auth::user()->is_user(SITE_COUNTRY_MANAGER)));
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return [
            'id' => $this->id
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::WithDrawlPrimaryIdValidationRequired
        ];
    }

    public function messages()
    {
        return [
            'id.exists' => 'No such withdrawal exits'
        ];

    }
}
