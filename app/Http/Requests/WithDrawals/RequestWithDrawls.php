<?php

namespace App\Http\Requests\WithDrawals;

use App\Currencies;
use App\Http\Requests\RequestConstants;
use App\User;
use App\users;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RequestWithDrawls extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        ## withdrawals mode can be.
        $withdrawalModeCanBe = ['Bonus', 'Profit'];

        if (Auth::user()->u_id <> 'B4U0720') {
            array_push($withdrawalModeCanBe, 'Sold');
        }

        $validationsRule = [
            "withdraw_mode" => 'required|regex:/([A-Za-z])/u|in:' . implode(',', $withdrawalModeCanBe),
            "currency" => RequestConstants::CurrencyValidationRequired,
            "fund_type" => 'in:fundtransfer,cashbox,""',
            "amount" => 'required|' . RequestConstants::AmountRegex,
            "donation" => RequestConstants::AmountRegex,
            "donation2" => RequestConstants::AmountRegex,
        ];

        ## check for benifiectory if fund_type is fundtransfer
        if ($this->fund_type && $this->fund_type <> 'cashbox') {
            ## if fund_receivers_id have valid B4U user id
            if ($this->request->get('fund_receivers_id') !== 'notexist') {

                ## if fund receive id is equal to notexist then fund_receiver_id should have valid B4U user's valid u_id
                $validationsRule = array_merge($validationsRule, [
                    'fund_receivers_id' => [
                        'required',
                        RequestConstants::B4UUIDRegex,
                        Rule::exists('fund_beneficiary', 'beneficiary_uid')->where('user_id', Auth::user()->id)->where('status', 0)
                    ]
                ]);

            } else {

                ## if fund receiver id has null and user wants to add another user's u_id
                $validationsRule = array_merge($validationsRule, [
                    'fund_receivers_id2' => RequestConstants::B4UUIDValidation
                ]);

            }
        }
        return $validationsRule;
    }

    public function messages()
    {
        return [

            'withdraw_mode.in' => 'Withdrawal mode should be Bonus, Profit or Sold',
            'withdraw_mode.required' => 'Withdrawal mode should be Bonus, Profit or Sold',
            'withdraw_mode.regex' => 'Invalid Character, Withdrawal mode should be Bonus, Profit or Sold',

            'amount.required' => 'Amount should be a number',
            'amount.regex' => 'Amount should be only in numeric',

            'donation.regex' => 'Donation should be only in numeric',
            'donation2.regex' => 'Donation should be only in numeric',

            'fund_receivers_id2.exists' => 'The mentioned Beneficiary B4U-ID is not exits in the system',
            'fund_receivers_id2.regex' => 'The mentioned Beneficiary B4U-ID must start from B4U(Capital letters)'
        ];
    }


}
