<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DepositProofRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|regex:/\d+/u|numeric|exists:\App\withdrawals,id',
            'fileurl' => array_merge(RequestConstants::URLValidation, [
                'required'
            ])
        ];
    }

    public function messages()
    {

        return [
            'fileurl.required' => 'Please upload file',
            'fileurl.regex' => 'Uploaded file is not valid',
            'fileurl.url' => 'Uploaded file is not valid',
            'fileurl.active_url' => 'Uploaded file is not valid',
        ];
    }
}
