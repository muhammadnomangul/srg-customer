<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BeneficiaryDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'benficiaryid' => RequestConstants::B4UUIDValidation
        ];
    }

    public function messages()
    {
       return [
           'benficiaryid.required' => 'Please type B4U Id',
           'benficiaryid.regex' => 'Invalid B4U Id format',
           'benficiaryid.exists' => 'B4U Id is not exits',
       ];
    }
}
