<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/* @deprecated 02-Sep-2020 */
class DepositProofHTMLRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => RequestConstants::WithDrawlPrimaryIdValidationRequired
        ];
    }

    public function messages()
    {
        return [
            'id.exists' => 'No such withdrawal exits'
        ];
    }
}
