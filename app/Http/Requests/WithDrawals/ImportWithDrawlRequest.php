<?php

namespace App\Http\Requests\WithDrawals;

use App\Http\Requests\RequestConstants;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ImportWithDrawlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() instanceof User && Auth::user()->is_user(SITE_ADMIN));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileurl' => array_merge(RequestConstants::URLValidation, ['required'])
        ];
    }

    public function messages()
    {
        return [
            'fileurl.required' => 'Please select CSV file',
            'fileurl.regex' => 'Please select CSV file',
            'fileurl.active_url' => 'Please select CSV file',
            'fileurl.url' => 'Please select CSV file',
        ];
    }
}
