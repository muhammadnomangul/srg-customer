<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TwoFAWrongAttemptsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $isAccountBlocked = User::isAccountBlocked();
        if ($isAccountBlocked) {
            return redirect()->route('account_blocked')->with('warning', "Your account has been suspended due to Wrong Attempts with password,
             Please contact to Support Admin to make your account enable");
        }

        return $next($request);
    }
}
