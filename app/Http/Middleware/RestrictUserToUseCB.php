<?php

namespace App\Http\Middleware;

use App\CBAccounts;
use App\Currencies;
use App\Model\Deposit;
use Closure;
use Illuminate\Support\Facades\Auth;

class RestrictUserToUseCB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      /*  $sumDeposits = Deposit::whereUserId(Auth::user()->id)->where('status','LIKE','Approved')->sum('total_amount');
        if (round($sumDeposits,2) < 200){
            return redirect('/dashboard')->with('errormsg','You personal minimum investment should be of $200 to use cashbox');
        }*/
        foreach (Currencies::getCurrencies() as $currency){
            $cbAccount = CBAccounts::where('user_id',Auth::user()->id)->where('currencies_id',$currency->id)->first();
            if (isset($cbAccount) && ($cbAccount->current_balance < 0 || $cbAccount->locked_balance < 0)){
                return back()->with('errormsg','You cannot perform any transaction due to negative balance deducted in your cashbox. Please wait for the system to validate your account');
            }
        }
        return $next($request);
    }
}
