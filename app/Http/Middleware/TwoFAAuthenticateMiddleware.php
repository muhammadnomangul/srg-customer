<?php

namespace App\Http\Middleware;

use App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PragmaRX\Google2FALaravel\Google2FA;

class TwoFAAuthenticateMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if ($request->user()->two_fa_auth_type <> 3) {
                /** @var User $loggedInUser */
                $loggedInUser = Auth::user();
                ## Authenticate OTP
                $isAuthenticate = TwoFAAuthenticateFacade::authenticate($loggedInUser->two_fa_auth_type, $loggedInUser, $request);
                ## return back if OTP is not authenticate.
                if (!$isAuthenticate) {
                    return back()->with('errormsg', 'Invalid Otp');
                }
            }
            return $next($request);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . " " . $exception->getFile() . " " . $exception->getLine() . " " . $exception->getCode());
            return back()->with('errormsg', 'Invalid Otp Exception');
        }

    }
}
