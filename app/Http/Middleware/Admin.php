<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Http\Controllers\Admin\IpController;
use Illuminate\Http\Request;
use Session;

class Admin
{

    /**
     * The authentication factory instance.
     *
     * @var Auth
     */
    protected $auth;
    /**
     * Create a new middleware instance.
     *
     * @param Auth $auth
     *
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);
        $user = \Auth::user();

        $headers = apache_request_headers();
        $headers = $headers['Host'];

        if ( ($headers == 'b4uglobal.com' || $headers == 'beta.b4uglobal.com')  && ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_COUNTRY_MANAGER)) ){
            ## if admin is not logged in then redirect user to the admin portal
            session()->flash('message', 'Please contact to Developer');
                auth()->logout();

            return redirect()->intended('login')->with('errormsg', 'Not Allowed ! Please contact to developer');
            
        } else {
            ## system is considering user is logged into the so checking, Is this user allowed to checkout the portal without admin...
            if (env('site_enable_for_admin', 0) == 0) {
                return redirect(url(env('admin_web_url', 'https://admin.b4uglobal.com/') . 'dashboard'));
            }
        }


        if ($user->type != 0 && !Session::has('login_info') && env('APP_ENV') == "production") {
            {
                $otp = new OTPToken();
                $random_string = $otp->generateCode();
                if (PinController::sendOtpSms(\Auth::user()->phone_no, $random_string)) {
                    ///save otp into database
                    $otp->code = $random_string;
                    $otp->user_id = \Auth::user()->id;
                    $otp->otp_type = 'login';
                    $otp->save();
                    Session::put('login_info', 1);
                    return redirect()->route('getOtp', [$otp->id]);
                }
            }
        }
        // if(!IpController::ip_security($user->type)){
        //     return redirect()->to('/ip');
        // }

        return $next($request);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate(array $guards)
    {
        if (empty($guards)) {
            return $this->auth->authenticate();
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards);
    }
}
