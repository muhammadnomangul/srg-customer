<?php

namespace App\Http\Middleware;

use Closure;

class RequestDecrypt
{
    const IgnoreFields = [
        '_token'
    ];

    const encryptValuesOf = [
        'id',
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        foreach ($request->all() as $key => $value) {
            if (!in_array($key, self::IgnoreFields)) {
                $request->request->set(decrypt($key), in_array(decrypt($key), self::encryptValuesOf) ? decrypt($value) : $value);
                $request->request->remove($key);
            }
        }
        return $next($request);
    }
}
