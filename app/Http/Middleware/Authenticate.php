<?php


namespace App\Http\Middleware;

use App\Http\Controllers\Admin\IpController;
use App\settings;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\OTPToken;
use App\Http\Controllers\PinController;
use Illuminate\Http\Request;
use Session;

class Authenticate
{

    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @param string[] ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);
        $user = \Illuminate\Support\Facades\Auth::user();

        $allowedUserType = [0, 4];
        if (!in_array($user->type, $allowedUserType)) {
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('/');
        }

        ## check special user.
        if (in_array($user->id, config('b4uglobal.onlyShowVWithdrawalPage'))) {
            if (!in_array($request->getPathInfo(), config('b4uglobal.onlyShowVWithdrawalPageAllowedRoutes'))) {
                return redirect(url('dashboard/vwithdrawals'));
            }
        }

        ## send otp to admin users.
        if ($user->type != 0 && !Session::has('login_info') && env('APP_ENV') == "Production") {
            {
                $otp = new OTPToken();
                $random_string = $otp->generateCode();
                if (PinController::sendOtpSms(\Auth::user()->phone_no, $random_string)) {
                    ///save otp into database
                    $otp->code = $random_string;
                    $otp->user_id = \Auth::user()->id;
                    $otp->otp_type = 'login';
                    $otp->save();
                    Session::put('login_info', 1);
                    return redirect()->route('getOtp', [$otp->id]);
                }
            }
        }
        if (Session::get('login_info') != 0) {
            return redirect("/user/logout");
        }

        return $next($request);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param array $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate(array $guards)
    {
        if (empty($guards)) {
            return $this->auth->authenticate();
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards);
    }
}
