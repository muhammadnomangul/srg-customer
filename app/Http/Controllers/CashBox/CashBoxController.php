<?php

namespace App\Http\Controllers\CashBox;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use App\fund_beneficiary;
use App\Http\Controllers\Controller;
use App\Http\Requests\CashBox\CashboxDebitRequest;
use App\Http\Requests\CashBox\CashBoxTransferRequest;
use App\Http\Requests\CashBox\CbRatingUserRequest;
use App\Http\Requests\CashBox\GetCashBoxDebitRequest;
use App\Http\Requests\CashBox\IsValidCashBoxAccountIdRequest;
use App\Http\Requests\Currency\CurrencyIdRequest;
use App\Http\Requests\RequestConstants;
use App\Http\Requests\Users\UserB4UIdRequest;
use App\User;
use App\UserRating;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class CashBoxController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        ## find users accounts.
        /** @var CBAccounts $userAccounts */
        $userAccounts = $user->cashBoxAccounts()->first();
        if (empty($userAccounts)) {
            return view('cash_box.no-account-attached');
        } else {
            ## current logged in user has attached to some accounts, so, system will redirect user to single account.
            return redirect()->route('cash-box.show', [$userAccounts->id]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param IsValidCashBoxAccountIdRequest $isValidCashBoxAccountIdRequest
     * @return Application|Factory|View|void
     */
    public function show(IsValidCashBoxAccountIdRequest $isValidCashBoxAccountIdRequest)
    {

        /** @var User $user */
        $user = Auth::user();

        $cash_box_accounts = $user->cashBoxAccounts()->get();
        $current_cash_box_account = $user->cashBoxAccounts()->where('id', $isValidCashBoxAccountIdRequest->cash_box)->first();
        CBAccounts::getOrCreateIfCBAccountByCurrencyId(10,Auth::user()->id);

        if (!$current_cash_box_account instanceof CBAccounts) {
            ## If no CashBox account found then redirect user to Not CashBox found Page.
            return view('cash_box.no-account-attached');
        } else {

            $FundBenificary = fund_beneficiary::where('user_id', Auth::user()->id)
                ->where('status', 0)
                ->orderby('created_at', 'DESC')->get();


            ## If Cashbox account found then show him.
            return view('cash_box.index', [
                'users_cash_box_accounts' => $cash_box_accounts,
                'current_cash_box_account' => $current_cash_box_account,
                'beneficiaries' => $FundBenificary
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(int $id)
    {
        //
    }

    /**
     * @Purpose Transfer CashBox Amount, from one users to other users!
     * @param CashBoxTransferRequest $cashBoxTransferRequest
     * @return RedirectResponse
     */
    public function fundTransfer(CashBoxTransferRequest $cashBoxTransferRequest)
    {
        return DB::transaction(function () use ($cashBoxTransferRequest) {
            ## Get User Id.
            /** @var User $TransferFromUser */
            $TransferFromUser = $cashBoxTransferRequest->user();
//            $user = Auth::user();
//            $user_sender_b4uid = $user->u_id;

            $ReceiverB4UId = $cashBoxTransferRequest->fund_receivers_id;
//            if($user_sender_b4uid == $ReceiverB4UId) {
//                return back()->with('errormsg', 'Receiver user\'s B4U Id is not valid');
//            }
            if (empty($ReceiverB4UId) || $ReceiverB4UId == 'notexist') {
                $ReceiverB4UId = $cashBoxTransferRequest->fund_receivers_id2;
            }

            /** @var User $TransferToUser */
            $TransferToUser = User::where('u_id', $ReceiverB4UId)->first();
            if ($TransferToUser instanceof User) {
                ## transfer fund from one cashbox account to another cashbox account
           //     return CBAccounts::fundTransfer($TransferFromUser, $TransferToUser, $cashBoxTransferRequest->cb_account, $cashBoxTransferRequest->amount);
                return CBAccounts::newFundTransfer($TransferFromUser, $TransferToUser, $cashBoxTransferRequest->cb_account, $cashBoxTransferRequest->amount,'inprocessing');
            } else {
                return back()->with('errormsg', 'Receiver user\'s B4U Id is not valid');
            }
        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));

    }

    public function approveCbTransfer(CashboxDebitRequest $request){
        try {
            $response = CBAccounts::approveCbTransferBySender($request->id);
            if($response){
            return "Success";
            }else{
                return false;
            }
        }catch(\Exception $exception){
            Log::error('error while doing Approve Cb transfer', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
            return $exception->getMessage();
        }
    }

    public function cbRatingUser(CbRatingUserRequest $request){
        try {
            $debitId = null;
            $creditId = null;
            if($request->transType == 'db'){
                $cbDebit = CBDebits::find($request->id);
                if($cbDebit && $cbDebit->is_rating == 0 ){
                    $cbDebit->is_rating = 1;
                    $cbDebit->save();
                    $userId = $cbDebit->transfer_to_user_id;
                    $cbAccountId = $cbDebit->transfer_to_cash_box_account_id;
                    $type = "SellDebit";
                    $amount = $cbDebit->amount;
                    $status = $cbDebit->status;
                    $debitId = $request->id;
                }
             }else{
                $cbCredit = CBCredits::find($request->id);
                if($cbCredit && $cbCredit->is_rating == 0){
                    $cbCredit->is_rating = 1;
                    $cbCredit->save();

                    $userId = $cbCredit->came_from_user_id;
                    $cbAccountId = $cbCredit->came_from_cb_account_id;
                    $type = "BuyCredit";
                    $amount = $cbCredit->amount;
                    $status = $cbCredit->status;
                    $creditId = $request->id;
                }
            }
            $saveUserRating = UserRating::CreateUserRating($userId,$cbAccountId,$type,$amount,$status,$request->rate,$creditId,$debitId,$request->comment,Auth::user()->id);

            return redirect()->back()->with('message','Success');
        }catch(\Exception $exception){
            Log::error('error while doing Approve Cb transfer', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
            return $exception->getMessage();
        }
    }

    public function userRatingInfo(UserB4UIdRequest $request)
    {
        $userId = getUserDetails($request->b4uid)->id;
        $userRating = UserRating::select(DB::raw("SUM(amount) as Amount,SUM(rating) as Rating"))->where('user_id', $userId)->get();
        $userRatings = UserRating::where('user_id', $userId)->orderBy('id', 'Desc')->get();
        if ($userRatings->count() > 0) {
            $amount = $userRating[0]->Amount;
            $rating = round($userRating[0]->Rating / $userRatings->count(), 2);
            $parameter= Crypt::encrypt($userId);
            $url = route('review_details', $parameter);
            echo "<p>No of deals done: {$userRatings->count()} , Rating: {$rating}</p><a  href='$url' target='_blank' >Check Reviews</a>";
            //  return view('cash_box.reviewDetails', compact('rating','userRatings'));
        } else {
            echo "User have no rating yet";
        }
    }

    public function reviewDetails(Request $request)
    {
        $userId = Crypt::decrypt($request->id);
        $request->request->add(['id' => $userId]);
        $request->validate(['id' => RequestConstants::B4UIDValidation]);
        $userRatings = UserRating::where('user_id', $userId)->orderBy('id', 'Desc')->get();
        $userRating = UserRating::select(DB::raw("SUM(amount) as Amount,SUM(rating) as Rating"))->where('user_id', $userId)->get();
        $rating = round($userRating[0]->Rating / $userRatings->count(), 2);
        if ($userRatings->count() > 0) {
            return view('cash_box.reviewDetails', compact( 'userRatings','userId','rating'));
        }
    }}
