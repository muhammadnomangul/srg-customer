<?php

namespace App\Http\Controllers\CashBox;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use App\currency_rates;
use App\current_rate;
use App\deposits;
use App\Http\Controllers\Controller;
use App\Http\Requests\CashBox\CashBoxAccountIdUserBasedRequest;
use App\Http\Requests\CashBox\CashBoxDepositRequest;
use App\Http\Requests\CashBox\CashBoxTopUpRequest;
use App\Http\Requests\CashBox\GetCashBoxDebitRequest;
use App\Http\Requests\IsAllowedTopDownRequest;
use App\Jobs\CashBox\CreateCBDebitOnDepositJob;
use App\Jobs\CashBox\CreateCreditFundTransfer;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CashBoxDebitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetCashBoxDebitRequest $request
     * @return array
     * @throws \Exception
     */
    public function index(CashBoxAccountIdUserBasedRequest $request)
    {
        try {
            return datatables()->of(Auth::user()->cashBoxDebits($request->ac))->toJson();
        } catch (\Exception $exception) {
            Log::error('getCashBoxCreditsRecord', [
                'errorMessage' => $exception->getMessage(),
            ]);
            return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @Purpose: Create new deposit from Cash Box.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function store(CashBoxDepositRequest $request)
    {

        return DB::transaction(function () use ($request) {

            if (env('APP_DEBUG', false) === false && deposits::where('user_id', \Auth::user()->id)->where('created_at', '>', \Carbon\Carbon::now()->subMinutes(5))->count()) {
                return redirect()->intended('dashboard/deposits')->with('errormsg', 'You can make only 1 deposit every 5 mints. please wait 5 mins to make new deposit');
            }

            ## Check is currency user having cash box Account of specific currency or not.
            $TransferFromUser = $request->user();
            $TransferFromCashBoxAccount = $TransferFromUser->getCashBoxAccountOfSpecificCurrency($request->cb_account);
            if (!$TransferFromCashBoxAccount instanceof CBAccounts) {
                return back()->with('errormsg', "You are not having Valid Cash Box Account to do a Top Up Request");
            }

            $invested_amount = $request['cb_amount'];
            $payment_mode = 'cashbox';
            $currency = $request['cb_account'];
            $deposit_mode = 'new';
            $logeduser_id = Auth::user()->id;
            $logeduser_uid = Auth::user()->u_id;
            $image = null;
            $trade_profit = 0;
            $currencyRate = '';

            // Total Amount in USD
            //$ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
            $ratesQuery = current_rate::first();

            if (isset($currency) && isset($ratesQuery)) {
                $curr = strtolower($currency);
                $rateVal = 'rate_' . $curr;
                $currencyRate = $ratesQuery->$rateVal;
            }

            if (is_null($currencyRate)) {
                return back()->with('errormsg', 'Currency rate is not defined');
            }

            $fee_deduct = explode(',', site_settings()->deposit_fee);
            $fee = 0;
            //deduct fee from the deposit if depost amount less then equal to 700 usd then 10 usd else 30 usd
            $total_amount = floatval($invested_amount) * floatval($currencyRate);

            if (strtolower($currency) <> 'rsc') {
                if (feeDeductOnDailyDeposit($total_amount, $logeduser_id) <= 300) {
                    $total_amount = ($total_amount - $fee_deduct[0]);
                    $fee = $fee_deduct[0];
                    $invested_amount = floatval($total_amount) / floatval($currencyRate);
                } elseif (feeDeductOnDailyDeposit($total_amount, $logeduser_id) > 300 && feeDeductOnDailyDeposit($total_amount, $logeduser_id) <= 1000) {
                    $total_amount = ($total_amount - $fee_deduct[1]);
                    $fee = $fee_deduct[1];
                    $invested_amount = floatval($total_amount) / floatval($currencyRate);
                } elseif (feeDeductOnDailyDeposit($total_amount, $logeduser_id) > 1000) {
                    $total_amount = ($total_amount - $fee_deduct[2]);
                    $fee = $fee_deduct[2];
                    $invested_amount = floatval($total_amount) / floatval($currencyRate);
                }
            }

            if($currency != 'USD' && $currency != 'RSC' && $currency != 'USDT'){
                return redirect()->back()->with('errormsg','Not allowed!');
            }
            ### CashBox Calculation ###
            $cashBoxBalance = CBAccounts::getCashBoxCurrentBalanceByCurrencyCode($currency);
            if ($invested_amount <= $cashBoxBalance && $cashBoxBalance > 0) {

                if ($total_amount >= site_settings()->deposit_limit) {
                    $CBDebit = CBDebits::createDebit($TransferFromCashBoxAccount->id, ($invested_amount + ($fee/$currencyRate)), 0, 'deposit', 'newdeposit', "Create Deposit in B4U Global", 'pending', true);
                    // trade save
                    $dp = new deposits();
                    $dp->amount = $invested_amount;
                    $dp->payment_mode = $payment_mode;
                    $dp->currency = $currency;
                    $dp->rate = $currencyRate;
                    $dp->total_amount = $total_amount;
                    $dp->plan = Auth::user()->plan;
                    $dp->user_id = $logeduser_id;
                    $dp->unique_id = $logeduser_uid;
                    $dp->reinvest_type = '';
                    $dp->pre_status = 'New';
                    $dp->flag_dummy = 0;
                    $dp->profit_take = 0;
                    $dp->status = 'Pending';
                    $dp->trade_profit = $trade_profit;
                    if (isset($image)) {
                        $dp->proof = $image;
                    }
                    if (isset($trans_id)) {
                        $dp->trans_id = $trans_id;
                    }
                    $dp->trans_type = 'NewInvestment';
                    //fee detucted
                    $dp->fee_deducted = $fee;
                    $dp->save();

                    ##
                    if (!$CBDebit instanceof CBDebits && $dp instanceof deposits) {
                        $dp->payment_mode = 'cashbox-failed';
                        $dp->save();
                        DB::select("CALL reverse_approved_deposits({$dp->id}, 1)");
                        return "m";
                     //   return back()->with('errormsg', 'Deposit Failed from Cash Box, It seems, system is under maintenance, It will be up in some hours.');
                    } else {

                        ## save debit id in deposit.
                        $dp->trans_id = "CB-DB-{$CBDebit->id}";
                        $dp->cb_debit_id = $CBDebit->id;
                        $dp->save();

                        ## save deposit id in debit.
                        $CBDebit->deposit_id = $dp->id;
                        $CBDebit->reason = "Create Deposit in B4U Global D#{$dp->id}";
                        $CBDebit->save();
                    }


                } else {
                    $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';
                    return back()->with('errormsg', $msg);
                }

            } else {
                return back()->with('errormsg', "You don't have enough balance in cash box " . $request->currency . ' account, so you are not able to make deposit . ');
            }

            $title = 'new CashBox Investment';
            $details = $logeduser_uid . ' has created a deposit from CashBox(' . $currency . ') of ' . $invested_amount;
            $pre_amt = 0;
            $curr_amt = 0;
            $approvedby = '';
            // Save Logs
            $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $invested_amount, $pre_amt, $curr_amt, $approvedby);
            return redirect()->route('cash-box.show', [$TransferFromCashBoxAccount->id])->with('message', 'Deposit Successfully created from CashBox!!');
        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));

    }


    /**
     * @purpose Top up from CashBox Account
     * @param CashBoxTopUpRequest $cashBoxTopUpRequest
     */
    public function topdown(CashBoxTopUpRequest $cashBoxTopUpRequest)
    {
        return DB::transaction(function () use ($cashBoxTopUpRequest) {
            ## secure check if we have enough balance.
            //  $CBAccounts = CBAccounts::getCashBoxCurrentBalanceByCurrencyCode($cashBoxTopUpRequest->cb_account);
            $isAllowedCashOut = CBDebits::isAllowedToCashOut($cashBoxTopUpRequest->cb_account, $cashBoxTopUpRequest->amount);
            if (!empty($isAllowedCashOut['should_transfer']) && $isAllowedCashOut['should_transfer'] && isset($isAllowedCashOut['deduction_fee'])) {
                ## system allow to transfer amount from CashBox.

                $TransferFromUser = $cashBoxTopUpRequest->user();
                $TransferFromCashBoxAccount = $TransferFromUser->getCashBoxAccountOfSpecificCurrency($cashBoxTopUpRequest->cb_account);
                if ($TransferFromCashBoxAccount instanceof CBAccounts) {
                    $CBDebit = CBDebits::createDebit($TransferFromCashBoxAccount->id, $cashBoxTopUpRequest->amount, $isAllowedCashOut['deduction_fee'], 'b4uwallet', 'topdown', 'Cash Out Request', 'inprocessing', true);
                    if ($CBDebit instanceof CBDebits) {
                        return back()->with('successmsg', "You have successfully top {$cashBoxTopUpRequest->amount}  {$cashBoxTopUpRequest->cb_account}");
                    } else {
                        return back()->with('errormsg', (string)$CBDebit);
                    }
                } else {
                    return back()->with('errormsg', "You are not having Valid Cash Box Account to do a Top Up Request");
                }
            } else {
                return back()->with('errormsg', isset($isAllowedCashOut['message']) ? $isAllowedCashOut['message'] : "You don't have enough balance in CashBox Account");
            }
        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));
    }

    /**
     * @Purpose Is Allowed top down.
     * @param IsAllowedTopDownRequest $cashBoxTopUpRequest
     * @return Application|Factory|View|JsonResponse
     */
    public function isAllowedTopDown(IsAllowedTopDownRequest $cashBoxTopUpRequest)
    {
        $isAllowedResponse = CBDebits::isAllowedToCashOut($cashBoxTopUpRequest->cb_account, $cashBoxTopUpRequest->amount);
        return view('cash_box.cashoutamounthave', $isAllowedResponse);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
