<?php

namespace App\Http\Controllers\Admin\Withdrawal;

use App\Http\Requests\WithDrawals\ImportWithDrawlRequest;
use App\Http\Requests\WithDrawals\ImportWithDrawls;
use App\withdrawals;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

}
