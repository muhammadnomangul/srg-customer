<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserPromotions\SaveUserPromotionRequest;
use App\Http\Requests\UserPromotions\UpdateUserPromotionRequest;
use App\Http\Requests\UserPromotions\UserPromotionsIdRequest;
use App\Http\Requests\UserPromotions\UserPromotionsRequest;
use App\Http\Requests\UserPromotions\UserPromotionsRequest as UserPromotionsRequestAlias;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserPromotion;
use DB;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Log;
use Exception;
use DataTables;
use Illuminate\Http\Response;

class UserPromotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = UserPromotion::latest()->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn(
                        'action',
                        function ($row) {
                            $edit = '<a href="' . route("promotion_history.edit", $row['id']) . '" class="edit btn btn-info btn-sm ">Edit</a><br />';

                            $delete = '' . $edit . '  <form action="' . route("promotion_history.destroy", $row['id']) . '" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="' . csrf_token() . '">
                    <button class="btn btn-primary">Delete User</button>
                </form>';
                            return $delete;
                        }
                    )
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } catch (Exception $e) {
            \Illuminate\Support\Facades\Log::error("Something went wrong. Please try again later. " . $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function store(SaveUserPromotionRequest $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserPromotionsIdRequest $userPromotionsRequest
     * @return Application|Factory|RedirectResponse|Response|View
     */
    public function edit(UserPromotionsIdRequest $userPromotionsRequest)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveUserPromotionRequest $request
     * @param UserPromotionsIdRequest $userPromotionsIdRequest
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function update(UpdateUserPromotionRequest $request, UserPromotionsIdRequest  $userPromotionsIdRequest)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param UserPromotionsIdRequest $userPromotionsIdRequest
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy(UserPromotionsIdRequest $userPromotionsIdRequest)
    {
    }
}
