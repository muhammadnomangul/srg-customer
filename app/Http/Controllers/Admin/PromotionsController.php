<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Promotions\PromotionsIdRequest;
use App\Http\Requests\Users\UserIdRequest;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Response;

class PromotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|Response|\Illuminate\View\View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {

        $this->authorize('viewAny', Promotion::class);
        try {
            if ($request->ajax()) {
                $data = Promotion::latest()->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn(
                        'action',
                        function ($row) {
                            $edit = '<a href="' . route("promotions.edit", $row['id']) . '" class="edit btn btn-info btn-sm ">Edit</a><br />';
                            if ($row['status'] == 'active') {
                                $btn = '' . $edit . ' <a href="' . route("promotion.disable", $row['id']) . '" class="edit btn btn-danger btn-sm ">Disable</a>';
                            } else {
                                $btn = '' . $edit . ' <a href="' . route("promotion.active", $row['id']) . '" class="edit btn btn-success btn-sm ">Activate</a>';
                            }

                            return $btn;
                        }
                    )
                    ->rawColumns(['action'])
                    ->make(true);
            }
            return view('admin.promotions.index');
        } catch (Exception $e) {
            \Illuminate\Support\Facades\Log::error("Something went wrong. Please try again later. " . $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|Response|\Illuminate\View\View
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the status of the promotion from disable to active.
     *
     * @param PromotionsIdRequest $promotionsIdRequest
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    public function activate(PromotionsIdRequest $promotionsIdRequest)
    {
    }

    /**
     * Update the status of the promotion from active to disable.
     *
     * @param PromotionsIdRequest $promotionsIdRequest
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    public function disable(PromotionsIdRequest $promotionsIdRequest)
    {
    }
}
