<?php

namespace App\Http\Controllers;

use App\Http\Requests\Albums\AlbumsIdRequest;
use App\Http\Requests\Albums\DeleteAlbumRequest;
use App\Http\Requests\Albums\SaveAlbumRequest;
use App\Http\Requests\Albums\UpdateAlbumsRequest;
use App\settings;
use Illuminate\Http\Request;
use App\Albums;
use App\AlbumImages;
use DB;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Cache;

class AlbumsController extends Controller
{
    public function gallery(AlbumsIdRequest $albumsIdRequest)
    {
        //dd($id);
        $images = AlbumImages::where('albums_id', $albumsIdRequest->id)->get();
        return view('gallery')->with(array('images' => $images, 'albumid' => $albumsIdRequest->id));
        //return view('gallery',compact('images'));
    }

    public function gallery2()
    {
        $AlbumImages = Albums::OrderBy('id', 'DESC')->get();
        return view('home/gallery')->with(array('AlbumImages' => $AlbumImages));
        //return view('gallery',compact('images'));
    }

    public function albumsAll()
    {
        $AlbumImages = Albums::OrderBy('id', 'DESC')->get();
        return view('home/albumsAll')->with(array('AlbumImages' => $AlbumImages));
    }

    public function events_gallery1(AlbumsIdRequest $albumsIdRequest)
    {

        $images1 = AlbumImages::where('albums_id', $albumsIdRequest->id)->get();
        $images2 = AlbumImages::where('albums_id', $albumsIdRequest->id)->first();
        $images3 = Albums::where('id', $albumsIdRequest->id)->first();
        return view('home/events_gallery1')->with(array('images1' => $images1, 'images2' => $images2, 'images3' => $images3));
    }

    public function promotions()
    {
        //dd($id);
        $id = 13;
        $images1 = Cache::remember(config('cachevalue.albumsIdCache'), settings::TopCacheTimeOut, function () use ($id) {
            return AlbumImages::where('albums_id', $id)->orderBy('id', 'desc')->get();
        });

        $images2 = Cache::remember(config('cachevalue.albumsIdFirstCache'), settings::TopCacheTimeOut, function () use ($id) {
            $record = AlbumImages::where('albums_id', $id)->first();
            if (empty($record)) {
                return [];
            } else {
                return $record;
            }
        });

        $images3 = Cache::remember(config('cachevalue.albumsCacheFirst'), settings::TopCacheTimeOut, function () use ($id) {
            $record = Albums::where('id', $id)->first();
            if (empty($record)) {
                return [];
            } else {
                return $record;
            }
        });

        return view('home/events_gallery1')->with(array('images1' => $images1, 'images2' => $images2, 'images3' => $images3));
    }


    public function events_gallery()
    {
        $images = Albums::get();
        return view('home/events_gallery')->with(array('images' => $images));
    }
}
