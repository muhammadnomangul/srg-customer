<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;
use App\Device;
use Validators;

class DeviceController extends Controller
{
  function list($id=null){
    return $id?users::find($id):users::all();
  }

  function search($name){
  return users::where("name","like","%".$name."%")->first();
  }
  function testData(Request $req){
      $rules=array(
          "name"=>"required"
      );
      $validators=Validators::make($req->all(),$rules);
      if($validators->fails()){
          return $validators->errors();
      }
      else
      {
          return["a"=>"x"];
      }

  }
}
