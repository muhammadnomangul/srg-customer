<?php

namespace App\Http\Controllers;

use App\current_rate;
use App\Http\Requests\Users\B4UUserSearchRequest;
use App\Http\Requests\WithDrawals\ApproveWithDrawlRequest;
use App\Http\Requests\WithDrawals\BigAmountWithDrawlRequest;
use App\Http\Requests\WithDrawals\DepositProofHTMLRequest;
use App\Http\Requests\WithDrawals\DepositProofRequest;
use App\Http\Requests\WithDrawals\HoldWithDrawalRequest;
use App\Http\Requests\WithDrawals\RequestWithDrawls;
use App\Http\Requests\WithDrawals\UpdateWithDrawalRequest;
use App\Http\Requests\WithDrawals\UpdateWIthDrawalStatusRequest;
use App\Http\Requests\WithDrawals\VerifiedWithDrawlRequest;
use App\Http\Requests\WithDrawals\ViewWithdrawalDetailRequest;
use App\Kyc;
use App\plans;
use App\settings;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\fund_beneficiary;
use App\daily_investment_bonus;
use App\users;
use App\ph;
use App\gh;
use App\withdrawals;
use App\deposits;
use App\currency_rates;
use App\deposit_investment_bonus;
use App\UserAccounts;
use App\Currencies;
use App\OTPToken;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Session;

/**
 * Class WithdrawalController
 * @package App\Http\Controllers
 */
class WithdrawalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
//        parent::__construct();
        $this->middleware('auth');
        $this->settings = settings::getSettings();
        // GET EMAIL TEMPLATE DATA SENT IN EMAILS
//        function getEmailTemplate($event)
//        {
//            $emailDetails = email_templates::where('title', 'Like', $event)->first();
//
//            return $emailDetails;
//        }
    }

    /**
     * Show the application dashboard.
     * @return Response
     */

    /*  $rules = [
         'trans_id' => 'required|string|max:255',
          'proof' => 'mimes:jpg,jpeg,png|max:4000',
          //|digits_between:10,10
        ];

        $customMessages = [
                'trans_id.required' => 'Transaction ID field is required.'
            ];

        $validator =  $this->validate($request, $rules, $customMessages);
    */

    //Return withdrawals route
    public function withdrawals()
    {
        $title = 'withdrawals';
        $widrawalQry = withdrawals::where('user', Auth::user()->id)
            ->where('status', '!=', 'Cancelled')
            ->where('status', '!=', 'Deleted')
            ->where('is_manual_cancel', '!=', 2)
            ->orderby('created_at', 'DESC')->get();

        $fund_beneficiaries = fund_beneficiary::where('user_id', Auth::user()->id)
            ->where('status', 0)
            ->orderby('created_at', 'DESC')->get();

        $UserAccountsQry = UserAccounts::where('user_id', Auth::user()->id)->first();
        $currency_RatesQry = currency_rates::orderby('created_at', 'DESC')->first();
        //DB::enableQueryLog();
        //dd($UserAccountsQry);

        $uid = Auth::user()->u_id;
        $parentid = Auth::user()->parent_id;
        $userdonation = $this->userdonations();

        $related_users = users::where('users.parent_id', '=', $uid)->orwhere('users.u_id', '=', $parentid)
            ->select('users.id', 'users.u_id', 'users.parent_id', 'users.name', 'users.email')->get();

        return view('withdrawals')->with(array('title' => $title, 'withdrawals' => $widrawalQry, 'usersList' => $related_users, 'accountsInfo' => $UserAccountsQry, 'ratesQuery' => $currency_RatesQry, 'currencies' => Currencies::getCurrencies(), 'userdonation' => $userdonation, 'beneficiaries' => $fund_beneficiaries));
    }

    public function withdrawals_json()
    {
        $widrawalQry = withdrawals::where('user', Auth::user()->id)
            ->where('status', '!=', 'Cancelled')
            ->where('status', '!=', 'Deleted')
            ->where('is_manual_cancel', '!=', 2)
            ->orderby('created_at', 'DESC')->get();

        return datatables()->of($widrawalQry)->toJson();
        //return response()->json(['msg' => "Something went wrong. Please try again later."], 500);
    }

    public function userdonations()
    {
        $exrate = current_rate::where('id', '=', 1)->first();
        $user_id = Auth::user()->id;

        $DonationUSD = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'USD')->sum('donation');

        $DonationBTC = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'BTC')->sum('donation');
        $BTCrate = $exrate->rate_btc;
        $DonationBTC = $DonationBTC * $BTCrate;

        $DonationRSC = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'RSC')->sum('donation');
        $RSCrate = $exrate->rate_rsc;
        $DonationRSC = $DonationRSC * $RSCrate;

        $DonationETH = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'ETH')->sum('donation');
        $ETHrate = $exrate->rate_eth;
        $DonationETH = $DonationETH * $ETHrate;

        $DonationBCH = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'BCH')->sum('donation');
        $BCHrate = $exrate->rate_bch;
        $DonationBCH = $DonationBCH * $BCHrate;

        $DonationLTC = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'LTC')->sum('donation');
        $LTCrate = $exrate->rate_ltc;
        $DonationLTC = $DonationLTC * $LTCrate;

        $DonationDASH = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'DASH')->sum('donation');
        $DASHrate = $exrate->rate_dash;
        $DonationDASH = $DonationDASH * $DASHrate;

        $DonationZEC = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'ZEC')->sum('donation');
        $ZECrate = $exrate->rate_zec;
        $DonationZEC = $DonationZEC * $ZECrate;

        $DonationXRP = withdrawals::where('user', $user_id)->where(
            function ($q) {
                $q->where('status', 'Approved')
                    ->orwhere('status', 'Pending');
            }
        )->where('currency', 'XRP')->sum('donation');
        $XRPrate = $exrate->rate_xrp;
        $DonationXRP = $DonationXRP * $XRPrate;

        $Donation = $DonationUSD + $DonationBTC + $DonationRSC + $DonationETH + $DonationBCH + $DonationLTC + $DonationDASH + $DonationZEC + $DonationXRP;

        return $Donation;
    }

    // send EMail
    public function sendWithdrawalSuccessMail($wid, $amount, $finalAmount, $currency)
    {
        if (Auth::user()->email != "") {
            //$withdrawal_id     = $wid;

            $email_to = Auth::user()->email;
            $userName = Auth::user()->name;
            $from_Name = $this->mail_from_name;
            $from_email = $this->mail_from_address;
            $subject = "Your Widrawal Request is Successful";
            $message =
                "<html>
								<body align=\"left\" style=\"height: 100%;\">
									<div>
										<div>
											<table style=\"width: 100%;\">
												<tr>
													<td style=\"text-align:left; padding:10px 0;\">
														Dear " . $userName . ",
													</td>
												</tr>
												<tr>
													<td style=\"text-align:left; padding:10px 0;\">
														Your requested withdrawal is successful, to " . $from_Name . ", your withdrawal id is W-" . $wid . ".
															
													</td>
												</tr>";
            /*    <tr>
                    <td style=\"text-align:left; padding:10px 0;\">
                            Your withdrawal amount is ".$amount."(".$currency."),after deduction of withdrawal fee you will recieve ".$finalAmount." (".$currency.").
                    </td></tr> */


            $message .= "<tr>
													<td style=\"text-align:left; padding:10px 0;\">
															Note: (if withdrawal amount <= 500 than $6 fee will be charged. and if withdrawal amount greater than $500 than $17 fee will be charged.)
													</td>
												</tr>
												<tr>
													<td style=\"text-align:left; padding:10px 0;\">
														You will get your withdraw amount, as soon as Admin approved your request.
													</td>
												</tr>
													
												<tr>
													<td style=\"text-align:left; padding:10px 0;\">
														Thanks for using  " . $from_Name . ".
													</td>
												</tr>
												<tr>
													<td style=\"padding:10px 0; text-align:left;\">
														Your Sincerely,
													</td>
												</tr>
												<tr>
													<td style=\"padding:10px 0; text-align:left;\">
														Team " . $from_Name . "
													</td>
												</tr>
													
											</table>
										</div>
									</div>
								</body>
							</html>";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:B4U Global Withdrawals<' . $from_email . '>' . "\r\n";
            // More headers info
            //$headers .= 'Cc: myboss@example.com' . "\r\n";

            $success = @mail($email_to, $subject, $message, $headers);

            return "Email send successfully";
        }
    }

    public function calculateDate($created_at)
    {
        /*
        $carbonCreatedAt = \Carbon\Carbon::parse($created_at);
        $carbonNow = \Carbon\Carbon::now();
        if($carbonNow->greaterThan($carbonCreatedAt) && $carbonNow->diffInMonths($created_at) > 1){
            return 1;
        }else{
            return 0;
        }
        */
        // Update lastest Profit for users
        $todayDate = date("Y-m-d");
        $creationDate = date("Y-m-d", strtotime($created_at));
        $date1MonthAfterCreate = date("Y-m-d", strtotime($creationDate . "+1 Month")); //Date After 1 month of Approved
        $date1 = date_create($creationDate);
        $date = date_create($todayDate);

        // Calculate Dates Differance1 in days
        $diff = date_diff($date1, $date);
        $diff_in_days = $diff->days;
        $diff_in_days2 = $diff->format("%R%a Days");
        $dateDifferance = date("Y-m-d", strtotime($created_at . $diff_in_days2));

        if ($dateDifferance >= $date1MonthAfterCreate) {
            return 1;
        } else {
            return 0;
        }
    }

    // Add new Withdrawal Process
    public function withdrawal(RequestWithDrawls $request)
    {
//        dd($request);
        if (isset($request['withdraw_mode'])) {
            $withdraw_mode = $request['withdraw_mode'];
        } else {
            $withdraw_mode = "";
        }
        if (isset($request['amount'])) {
            $amount = $request['amount'];
        } else {
            $amount = 0;
        }

        if (isset($request['donation'])) {
            $donation = $request['donation'];
        } else {
            $donation = 0;
        }

        if (isset($request['donation2'])) {
            $donation2 = $request['donation2'];
        } else {
            $donation2 = 0;
        }

        if (isset($request['currency'])) {
            $currency = $request['currency'];
        } else {
            $currency = "";
        }

       /* if($currency != 'USD' && $currency != 'RSC'){
            return redirect()->back()->with('errormsg', 'We are facing some technical issues in crypto withdrawals. We apologise for inconvenience .We are working on it. Please wait till 22-May-2021 for further update.');
        }*/
        if ($request['fund_type'] != "fundtransfer" && $request['fund_type'] != "cashbox" && $withdraw_mode == "Bonus"){
            return redirect()->back()->with('errormsg', 'Bonus cash withdrawal is not allowed!');
        }
        if ($request['fund_type'] != "fundtransfer" && $withdraw_mode == "Sold"){
            return redirect()->back()->with('errormsg', 'Something went wrong!');
        }

        $loggedInUser = Auth::user();

//        if (strtolower($loggedInUser->Country) == 'pakistan' && strtolower($currency) == 'usd' && $request->amount < 710 && empty($request->fund_type)) {
//            return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Pakistani user can only withdraw amount of equal or more than $710');
//        }


        $user_id = Auth::user()->id;
        $user_uid = Auth::user()->u_id;

        if (strtolower($loggedInUser->Country) != 'pakistan'

        ) {
            $user_id = Auth::user()->id;
            $user_kyc = Kyc::where('user_id',$user_id)->first();
            if($user_kyc){
                $mother_name = $user_kyc['mother_name'] ;
                $dob = $user_kyc['dob'] ;
                $cnic = $user_kyc['cnic'] ;
                $passport = $user_kyc['passport'] ;
	            $nok_name = $user_kyc['nok_name'] ;
                $nok_relation = $user_kyc['nok_relation'] ;
	            $nok_contact_num = $user_kyc['nok_contact_num'] ;
	            if(isset($mother_name)
                    && isset($dob)
                    && isset($cnic)
                    && isset($passport)
                    && isset($nok_name)
                    && isset($nok_relation)
                    && isset($nok_contact_num)
                ){
	                $two = 2;
                }else{
                    return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Please Complete your kyc information to proceed with withdrawal ');
                }

            }


        }else{

        }
        // Accounts Info
        $userAccInfo = UserAccounts::where('user_id', $user_id)->first();

        // Currency Rates Query
        $ratesQuery = current_rate::first();
        $activeDeposits = deposits::where('user_id', Auth::user()->id)->where('status', 'Approved')->get();
        $totalActiveDeposits = count($activeDeposits);
        $createdAt = "";
        $previousWithdrawal = "";
        $dateDifferece = 0;
      //  if ($withdraw_mode == "Sold") { return redirect()->back()->with('errormsg',"Some Error Occur"); }
        if ($withdraw_mode == "Profit") {

            //Only one withdrawal allowed with in 1 month against a specific currency
            $previousWithdrawal = withdrawals::where('user', Auth::user()->id)
                ->where('payment_mode', 'Profit')
                ->where('currency', $currency)
                ->where('amount', '>', 0)
                ->where(function ($q) {
                    $q->where('status', 'Approved')
                        ->orwhere('status', 'Pending');
                })
                ->orderBy('created_at', 'DESC')
                ->first();
            $thismonthWithdrawalcountwithcall = withdrawals::where('user', Auth::user()->id)

                ->where('currency', $currency)
                ->where('amount', '>', 0)
                ->where('fund_type', '=', 'callcharges')
                ->whereMonth('created_at', Carbon::now()->month)
                ->where(function ($q) {
                    $q->where('status', 'Approved')
                        ->orwhere('status', 'Pending');
                })
                ->orderBy('created_at', 'DESC')
                ->get();
            $thismonthWithdrawalcount = withdrawals::where('user', Auth::user()->id)

                ->where('currency', $currency)
                ->where('amount', '>', 0)
                ->whereMonth('created_at', Carbon::now()->month)
                ->where('fund_type', '!=', 'callcharges')
                ->where(function ($q) {
                    $q->where('status', 'Approved')
                        ->orwhere('status', 'Pending');
                })
                ->orderBy('created_at', 'DESC')
                ->get();
            $thismonthWithdrawalcancel = withdrawals::where('user', Auth::user()->id)

                ->where('currency', $currency)
                ->where('amount', '>', 0)
                ->whereMonth('created_at', Carbon::now()->month)
                ->where('fund_type', '!=', 'callcharges')
                ->where(function ($q) {
                    $q->where('status', 'Cancelled');
                })
                ->orderBy('created_at', 'DESC')
                ->get();
            $thismonthWithdrawalcountwithcall = $thismonthWithdrawalcountwithcall->count();
            $thismonthWithdrawalcount = $thismonthWithdrawalcount->count();
            $thismonthWithdrawalcancel = $thismonthWithdrawalcancel->count();

//            if()
//            dd($previousWithdrawaldcount);

            if (isset($previousWithdrawal) && isset($previousWithdrawal->created_at)) {
                $createdAt = $previousWithdrawal->created_at;
                $dateDifferece = $this->calculateDate($createdAt);
//                if($previousWithdrawal->fund_type = 'callcharges'  && $dateDifferece == 1){
//                    $dateDifferece = 1;
                //                    echo "withdrawal " .$thismonthWithdrawalcount ." call sub ". $thismonthWithdrawalcountwithcall;
//                    exit();
                 if($thismonthWithdrawalcount > 0){

                 }else{
                     if($thismonthWithdrawalcancel > 0 ){
                         $dateDifferece = 1;

                     }else{
                         if( $thismonthWithdrawalcountwithcall > 0){
                             $dateDifferece = 1;
                         }
                     }
                 }
            } else {
                $dateDifferece = 1;
            }
        }


        if ((app('request')->session()->get('back_to_admin')) || ($withdraw_mode != "Profit" || $dateDifferece == 1)) {
            if ($withdraw_mode != "" && isset($userAccInfo) && isset($ratesQuery)) {
                $reference_bonus = $userAccInfo->reference_bonus;
                $reference_bonus2 = $userAccInfo->reference_bonus;
                //exit;
                if (isset($currency)) {
                    $curr = strtolower($currency);

                    $rateVal = "rate_" . $curr;
                    $currencyRate = $ratesQuery->$rateVal;

                    $accProfit = "profit_" . $curr;
                    $userProfit = $userAccInfo->$accProfit;
                    $userProfit2 = $userAccInfo->$accProfit;

                    $accBalSold = "sold_bal_" . $curr;
                    $userbalanceSold = $userAccInfo->$accBalSold;
                    $userbalanceSold2 = $userAccInfo->$accBalSold;

                    $accBal = "balance_" . $curr;
                    $userbalance = $userAccInfo->$accBal;
                    $userbalance2 = $userAccInfo->$accBal;
                    $totalUsd = $amount * $currencyRate;
                    //B4U Foundation Donation
                    if ($donation > 0) {
                        $donationUsd = $donation * $currencyRate;
                    } else {
                        $donationUsd = 0;
                    }

                    //B4U Foundation Donation
                    if ($donation2 > 0) {
                        $donationUsd = $donation2 * $currencyRate;
                    } else {
                        $donationUsd = 0;
                    }

                    $finalAmount = $amount;
                    $withdrawal_fee = 0;

                    if (strtolower($currency) <> 'rsc') {
                        ## don't deduct fees on rsc currency.
                        // Set Withdrawal Deduct Fee
                        if ($request['fund_type'] == 'cashbox') {
                            if ($totalUsd <= 500) {
                                if ($currency != "USD") {
                                    $withdrawal_fee = 6 / $currencyRate;
                                } else {
                                    $withdrawal_fee = 6;
                                }
                                $finalAmount = $amount - $withdrawal_fee;
                            } elseif ($totalUsd > 500) {
                                if ($currency != "USD") {
                                    $withdrawal_fee = 17 / $currencyRate;
                                } else {
                                    $withdrawal_fee = 17;
                                }
                                $finalAmount = $amount - $withdrawal_fee;
                            }
                        } elseif ($request['fund_type'] != "fundtransfer") {
                            if ($totalUsd <= 500) {
                                if ($currency != "USD") {
                                    $withdrawal_fee = 10 / $currencyRate;
                                } else {
                                    $withdrawal_fee = 10;
                                }
                                $finalAmount = $amount - $withdrawal_fee;
                            } elseif ($totalUsd > 500) {
                                if ($currency != "USD") {
                                    $withdrawal_fee = 20 / $currencyRate;
                                } else {
                                    $withdrawal_fee = 20;
                                }
                                $finalAmount = $amount - $withdrawal_fee;
                            }
                        }
                    }


                    if ($currency == "USD" && $withdraw_mode == "Bonus") {
                        if ($totalUsd < site_settings()->withdraw_limit || $amount > $reference_bonus) {
                            return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $' . site_settings()->withdraw_limit . '! Or You have insufficient balance for this request!');
                        } elseif ($totalActiveDeposits == 0) {
                            return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Bonus Withdrawal Not Allowed ! You have no approved deposits in your deposits account.');
                        }
                    } elseif ($currency != "USD" && $withdraw_mode == "Bonus") {
                        return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Invalid request! selected currency not allowed for reinvest Bonus');
                    } elseif ($withdraw_mode == "Profit" && ($totalUsd < site_settings()->withdraw_limit || $amount > $userProfit)) {
                        return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $' . site_settings()->withdraw_limit . '! Or You have insufficient balance for this request!');
                    } elseif ($withdraw_mode == "Sold" && ($totalUsd < site_settings()->withdraw_limit || $amount > $userbalanceSold)) {
                        return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $' . site_settings()->withdraw_limit . '! Or You have insufficient balance for this request!');
                    }

                    $fundRecieverID = "";

                    if (isset($request['fund_type']) && isset($request['fund_receivers_id'])) {
                        if (isset($request['fund_receivers_id2']) && $request['fund_receivers_id'] != "notexist") {
                            $fundRecieverID = strtoupper($request['fund_receivers_id']);
                        } elseif (isset($request['fund_receivers_id2']) && $request['fund_receivers_id2'] != "") {
                            $fundRecieverID = strtoupper($request['fund_receivers_id2']);
                        }

                        if ($fundRecieverID != "") {
                            if ($request['fund_type'] != "fundtransfer"){
                                return redirect()->back()->with('errormsg', 'Invalid Request. Fund Receiver id is not allowed when you request cashbox transfer. ');
                            }
                            $fundBeneficiary = fund_beneficiary::where('user_id', $user_id)->where('beneficiary_uid', $fundRecieverID)->first();
                            $validuser = users::where('u_id', $fundRecieverID)->where('status', 'LIKE', 'active')->first();

                            if (!isset($fundBeneficiary) && $fundRecieverID !== Auth::user()->u_id && isset($validuser)) {
                                $bene_details = users::where('u_id', $fundRecieverID)->first();

                                $nb = new fund_beneficiary();
                                $nb->user_id = $user_id;
                                $nb->user_uid = $user_uid;
                                $nb->beneficiary_id = $bene_details->id;
                                $nb->beneficiary_uid = $fundRecieverID;
                                $nb->save();
                            }

                            $fundUserAcc = users::where('u_id', $fundRecieverID)->where('status', 'LIKE', 'active')->first();


                            if (!isset($fundUserAcc) || $fundRecieverID == Auth::user()->u_id) {
                                return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Invalid beneficiary, Please add valid beneficiary user id for fundtransfer!');
                                //return redirect()->back()->with('errormsg', 'Invalid beneficiary, Please add valid beneficiary user id for fundtransfer.');
                            }
                        }
                    }
                    if (($withdraw_mode != "FundTransfer") && $totalUsd >= site_settings()->withdraw_limit) {
                        $balance = 0;
                        //$last_withdrawal_id =  "W-".$wd->id;
                        $dateTime = date("Y-m-d H:i:s");
                        $title = $withdraw_mode . " Withdraw by " . $user_uid;
                        $details = "New " . $withdraw_mode . " Withdrawal added by" . $user_id;
                        $approvedby = "";
                        if ($withdraw_mode == "Bonus") {
                            $balance = $reference_bonus;
                            $reference_bonus = $reference_bonus - $amount;

                            // B4U Foundation Donations
                            if ($donation > 0 && $reference_bonus >= $donation) {
                                $reference_bonus = $reference_bonus - $donation;
                            } else {
                                $donation = 0;
                            }

                            // Crona Donations
                            if ($donation2 > 0 && $reference_bonus >= $donation2) {
                                $reference_bonus = $reference_bonus - $donation2;
                            } else {
                                $donation2 = 0;
                            }

                            UserAccounts::findOrUpdate($user_id, ['reference_bonus' => $reference_bonus,]);
                            $pre_amt = $reference_bonus2;
                            $curr_amt = $reference_bonus;
                            // Save Logs
                            $this->saveLogs($title, $details, $user_id, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
                        } elseif ($withdraw_mode == "Profit") {
                            $balance = $userProfit;
                            $userProfit = $userProfit - $amount;
                            // B4U Foundation Donation
                            if ($donation > 0 && $userProfit >= $donation) {
                                $userProfit = $userProfit - $donation;
                            } else {
                                $donation = 0;
                            }

                            // Crona Donation
                            if ($donation2 > 0 && $userProfit >= $donation2) {
                                $userProfit = $userProfit - $donation2;
                            } else {
                                $donation2 = 0;
                            }

                            UserAccounts::findOrUpdate($user_id, [$accProfit => $userProfit]);

                            $pre_amt = $userProfit2;
                            $curr_amt = $userProfit;
                            // Save Logs
                            $this->saveLogs($title, $details, $user_id, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
                        } elseif ($withdraw_mode == "Sold") {
                            if (Auth::user()->u_id == 'B4U0720') {
                                return redirect()->back()->with('errormsg', 'You are not allowed!');
                            } else {
                                $balance = $userbalanceSold;
                                $userbalanceSold = $userbalanceSold - $amount;
                                //B4U Foundation Donation
                                if ($donation > 0 && $userbalanceSold >= $donation) {
                                    $userbalanceSold = $userbalanceSold - $donation;
                                } else {
                                    $donation = 0;
                                }

                                //Crona Donation
                                if ($donation2 > 0 && $userbalanceSold >= $donation2) {
                                    $userbalanceSold = $userbalanceSold - $donation2;
                                } else {
                                    $donation2 = 0;
                                }

                                UserAccounts::findOrUpdate($user_id, [$accBalSold => $userbalanceSold]);

                                $pre_amt = $userbalanceSold2;
                                $curr_amt = $userbalanceSold;
                                // Save Logs
                                $this->saveLogs($title, $details, $user_id, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
                            }
                        }
                        // save New Withdrawal
                        $wd = new withdrawals();
                        $wd->user = $user_id;

                        $wd->amount = $finalAmount;  // withdrawal Amount
                        $wd->pre_amount = $balance; // Previous Acc Balance
                        if ($currency != "USD") {
                            $wd->crypto_amount = $amount;
                        }
                        $wd->usd_amount = $totalUsd;
                        $wd->donation = $donation + $donation2;
                        $wd->currency = $currency;
                        $wd->unique_id = $user_uid;
                        $wd->payment_mode = $withdraw_mode;
                        $wd->status = 'Pending';
                        $wd->pre_status = 'New';
                        if (Auth::user()->awarded_flag == 2 || Auth::user()->awarded_flag == '2') {
                            $wd->flag_dummy = 1;
                        }

                        $wd->withdrawal_fee = $withdrawal_fee; // Previous Acc Balance

                        if (isset($request['fund_type'])) {
                            $wd->fund_type = $request['fund_type'];

                            $wd->fund_receivers_id = $fundRecieverID;
                        }

                        // save the created by id for withdrawls
                        $createdby = (app('request')->session()->get("Admin_Id")) ? app('request')->session()->get("Admin_Id") : Auth::user()->id;
                        $wd->created_by = $createdby;

                        // dd($wd);
                        $wd->save();

                        $last_withdrawal_id = $wd->id;

                        // send email functions
                        $this->sendWithdrawalSuccessMail($last_withdrawal_id, $amount, $finalAmount, $currency);
                        if(Auth::user()->Country == "Canada"){
                            users::findOrUpdate(Auth::user()->id,
                                [
                                    'usa_account_holder_name' => $request->usa_account_holder_name,
                                    'date_of_birth' => $request->date_of_birth,
                                    'full_address' => $request->full_address,
                                    'full_bank_name' => $request->full_bank_name,
                                    'institute_bank_number' => $request->institute_bank_number,
                                    'transit_number' => $request->transit_number,
                                    'bank_account_type' => $request->bank_account_type

                                ]
                            );
                        } if(Auth::user()->Country == "United States"
                            || Auth::user()->Country == "United States minor outlying islands"){
//            dd($request->usa_account_holder_name);
                            users::findOrUpdate(Auth::user()->id,
                                [
                                    'usa_account_holder_name' => $request->usa_account_holder_name,
                                    'date_of_birth' => $request->date_of_birth,
                                    'full_address' => $request->full_address,
                                    'full_bank_name' => $request->full_bank_name,
                                    'routing_number' => $request->routing_number,
                                    'bank_account_type' => $request->bank_account_type

                                ]
                            );
                        } if(Auth::user()->Country == "United Kingdom"){
//            dd($request->usa_account_holder_name);
                            users::findOrUpdate(Auth::user()->id,
                                [
                                    'usa_account_holder_name' => $request->usa_account_holder_name,
                                    'date_of_birth' => $request->date_of_birth,
                                    'full_address' => $request->full_address,
                                    'full_bank_name' => $request->full_bank_name,
                                    'routing_number' => $request->routing_number,
                                    'bank_account_type' => $request->bank_account_type,
                                    'iban_number' => $request->iban_number

                                ]
                            );
                        }
                        if(Auth::user()->Country == "Europe"
                            || Auth::user()->Country == "France"
                            || Auth::user()->Country == "France, Metropolitan"
                            || Auth::user()->Country == "French Guiana"
                            || Auth::user()->Country == "French Polynesia"
                            || Auth::user()->Country == "French Southern Territories"
                            || Auth::user()->Country == "Georgia"
                            || Auth::user()->Country == "Germany"
                            || Auth::user()->Country == "Greece"
                            || Auth::user()->Country == "Greenland"
                            || Auth::user()->Country == "Italy"){
//            dd($request->usa_account_holder_name);
                            users::findOrUpdate(Auth::user()->id,
                                [
                                    'usa_account_holder_name' => $request->usa_account_holder_name,
                                    'iban_number' => $request->iban_number
//                                    'date_of_birth' => $request->date_of_birth,
//                                    'full_address' => $request->full_address,
//                                    'full_bank_name' => $request->full_bank_name,
//                    'routing_number' => $request->routing_number,
//                                    'bank_account_type' => $request->bank_account_type

                                ]
                            );
                        }
                        $successmsg = 'Action Successful! Please wait for system to approve your withdrawal request.';
                        return redirect("dashboard/withdrawals")->with('successmsg', $successmsg);
                    }
                } // end currenc if
            } // end mode if

            // end of top if
        } elseif ($dateDifferece != 1 && $withdraw_mode == "Profit") {
            return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Withdrawal Not Allowed ! You can create only 1 profit-withdrawal against a same currency within 1 Month');
        }


    }// end of function

    public function viewWithdrawDetailsPost(ViewWithdrawalDetailRequest $request)
    {
        $withdrawal = withdrawals::where('id', $request->id)->where('user', Auth::user()->id)->first();
        if ($withdrawal instanceof withdrawals) {
            $user_id = $withdrawal->user;
            $unique_id = "W-" . $withdrawal->id;
            $amount = $withdrawal->amount;

            $pre_status = $withdrawal->pre_status;

            $status = $withdrawal->status;

            $payment_mode = $withdrawal->payment_mode;

            $pre_amount = $withdrawal->pre_amount;
            $new_amount = $withdrawal->new_amount;

            $currency = $withdrawal->currency;

            $withdrawal_fee = $withdrawal->withdrawal_fee;

            $bank_reference_id = $withdrawal->bank_reference_id;

            $new_type = $withdrawal->new_type;
            $fund_type = $withdrawal->fund_type;
            $fund_receivers_id = $withdrawal->fund_receivers_id;

            $paid_flag = $withdrawal->paid_flag;

            $flag_dummy = $withdrawal->flag_dummy;

            $created_at = $withdrawal->created_at;

            $updated_at = $withdrawal->updated_at;

            $createdby = ($withdrawal->createdBy) ? $withdrawal->createdBy->name : "NA";

            $userinfo = users::where('id', $user_id)->first();

            if ($withdrawal->is_verify == 1) {
                $userverifyname = users::where('u_id', $withdrawal->verified_by)->first();
                if (isset($userverifyname->name)) {
                    $verifyName = $userverifyname->name;
                } else {
                    $verifyName = $withdrawal->verified_by;
                }
            }
            if (isset($withdrawal->approved_by)) {
                $userApprovename = users::where('u_id', $withdrawal->approved_by)->first();
                if (isset($userApprovename->name)) {
                    $ApproveName = $userApprovename->name;
                } else {
                    $ApproveName = $withdrawal->approved_by;
                }
            }
            if ($withdrawal->is_paid == 1) {
                $userPaidname = users::where('u_id', $withdrawal->paid_by)->first();
                if (isset($userPaidname->name)) {
                    $PaidName = $userPaidname->name;
                } else {
                    $PaidName = $withdrawal->paid_by;
                }
            }
            //print_r($userinffsdo);
            //exit;
            $userUID = $userinfo->u_id;
            $totalAmount = $amount + $withdrawal_fee;

            $USERTYPE = Auth::user()->type;
            $USERID = Auth::user()->id;

            if (($USERTYPE == 0 && $USERID == $user_id) || $USERTYPE == 1 || $USERTYPE == 2) {
                $details = '<div class="modal-header">

			        <button type="button" class="close" data-dismiss="modal">&times;</button>

			        <h4 class="modal-title" style="text-align:center;">Withdraw Details</h4>

			      </div>

			      <div class="modal-body">

					<table width="90%" align="center" style="padding-bottom: 15px !important;">

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Withdrawal Info: </h4></td></tr>

						<tr><td width="25%"> <strong>Withdraw ID : </strong></td><td width="25%">' . $unique_id . ' </td><td width="25%"> <strong>User ID : </strong></td><td width="25%">' . $userUID . '</td> </tr>

						<tr><td width="25%"><strong>Current Status : </strong></td><td width="20%">' . $status . '</td><td width="35%">	<strong>Pre Status : </strong></td><td width="20%">' . $pre_status . ' </td></tr>

						<tr><td width="25%"><strong>Payment Mode : </strong></td><td width="25%">' . $payment_mode . '</td>';
                if ($USERTYPE == 1 || $USERTYPE == 2) {
                    if (isset($bank_reference_id)) {
                        $details .= '<td width="15%"><strong>Bank Ref.Id : </strong></td><td width="15%">' .
                            $bank_reference_id . '</td></tr>';
                    } else {
                        $details .= '<td></td></tr>';
                    }

                    if ($withdrawal->is_verify == 1 && isset($verifyName)) {
                        $verified_at = date("Y-m-d", strtotime($withdrawal->verified_at));

                        $details .= '<tr><td width="25%"><strong>Verify By : </strong></td><td width="25%">' .
                            $verifyName . '</td>
							<td width="25%"><strong>Verified at : </strong></td><td width="25%">' . $verified_at . '</td></tr>';
                    }

                    if (isset($withdrawal->approved_by) && isset($ApproveName)) {
                        $approved_at = date("Y-m-d", strtotime($withdrawal->approved_at));
                        $details .= '<tr><td width="25%"><strong>Approved By : </strong></td><td width="25%">' .
                            $ApproveName . '</td>
							<td width="25%"><strong>Approved at : </strong></td><td width="25%">' . $approved_at . '</td></tr>';
                    }
                    //withdrawls created by
                    $details .= '<tr><td width="25%"><strong>Created By: </strong></td><td>' . $createdby . '</td></tr>';

                    if ($withdrawal->is_paid == 1 && isset($PaidName)) {
                        $paid_at = date("Y-m-d", strtotime($withdrawal->paid_at));

                        $details .= '<tr><td width="25%"><strong>Paid By : </strong></td><td width="25%">' . $PaidName . '</td>
							<td width="25%"><strong>Paid at : </strong></td><td width="25%">' . $paid_at . '</td></tr>';
                    }
                }
                if ($payment_mode == "FundTransfer") {
                    $details .= '<tr><td width="25%"><strong>Total Amount: </strong></td><td>' . number_format($totalAmount, 2) . '</td><td width="25%"><strong>Fund Reciever ID : </strong></td><td width="25%">' . $fund_receivers_id . '</td width="25%"></tr>';
                }

                if ($currency == "USD") {
                    $details .= '<tr><td colspan="4"></td></tr>

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Amount Info: </h4></td></tr>

						<tr><td colspan="2"><strong>User Recieved Amount: </strong></td><td colspan="2">' . number_format($amount, 2) . ' (' . $currency . ')</td></tr>
						<tr><td colspan="2"> <strong>Deduction Fee: </strong></td><td colspan="2">' . number_format($withdrawal_fee, 2) . ' (' . $currency . ')</td></tr>

						<tr><td colspan="4"></td></tr>';
                } else {
                    $details .= '<tr><td colspan="4"></td></tr>

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Amount Info: </h4></td></tr>

						<tr><td colspan="2"><strong>User Recieved Amount: </strong></td><td colspan="2">' . number_format($amount, 5) . ' (' . $currency . ')</td></tr>
						<tr><td colspan="2"> <strong>Deduction Fee: </strong></td><td colspan="2">' . number_format($withdrawal_fee, 5) . ' (' . $currency . ')</td></tr>

						<tr><td colspan="4"></td></tr>';
                }

                $details .= '</table></div>';

                echo $details;
            } else {
                echo "You are not allowed to view the details";
            }
        } else {
            return 'Not allowed to view withdrawals';
        }
        //exit;
    }

    /**
     * @param ViewWithdrawalDetailRequest $request
     * @return string
     * @Purpose:: View Withdrawal deposit proof.
     */
    public function viewProofPost1(ViewWithdrawalDetailRequest $request)
    {

        $withdrawal = withdrawals::where('id', $request->id)->where('user', Auth::user()->id)->first();

        if ($withdrawal instanceof withdrawals) {
            if ($withdrawal->paid_img) {

                ## upload Image proof, before aws file upload, these deposit proofs are uploading in local system storage
                ## so make it functional, I mean to view old proof of deposits, we have these codes, for the old deposits we will
                ## take deposit proof from the local storage and for the new deposit proof we will have from google cloud,
                $previousFileUpload = $image = "uploads/paiduploads/" . $withdrawal->paid_img;
                $fileUrl = null;
                if (is_file($previousFileUpload)) {

                    if (strtolower(pathinfo($withdrawal->paid_img, PATHINFO_EXTENSION)) == 'pdf') {
                        $details = '<a  target="_blank" href="' . url($image) . '">Download Proof Pdf</a>';
                    } else {
                        $details = '<img width="80%" src="' . url($image) . '">';
                    }
                } else {
                    $details = '<img width="80%" src="' . Storage::disk('gcs')->url($withdrawal->paid_img) . '">';
                }

                echo '<p style="text-align:center;">' . $details . '</p> <br/>';
            } else {
                echo '<p style="text-align:center;">Proof Not Found! </p> <br/>';
            }
            exit();
        } else {
            echo 'access denied';
            exit();
        }
    }

    public function fundTransfer($withdrawID)
    {
        try {
            $wid = trim($withdrawID);
            $withdrawal = withdrawals::where('id', $wid)->where('user', Auth::user()->id)->first();
            if (isset($withdrawal) && $withdrawal->fund_receivers_id != "" && $withdrawal instanceof withdrawals) {
                $user_id = $withdrawal->user;
                $unique_id = $withdrawal->unique_id;
                $amount = $withdrawal->amount;
                $currency = $withdrawal->currency;
                $withdrawal_fee = $withdrawal->withdrawal_fee;
                $fund_receivers_id = $withdrawal->fund_receivers_id;
                $fund_type = $withdrawal->fund_type;
                $flag_dummy = $withdrawal->flag_dummy;

                // trade save
                $trans_id = "Fund tranfer by : $unique_id , and withdrawal id : w-$wid";
                $trans_type = "NewInvestment";

                $userinfo = users::where('u_id', $fund_receivers_id)->first();
                $fundUserId = $userinfo->id;
                $planid = $userinfo->plan;

                //$ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();
                $ratesQuery = current_rate::first();
                $curr = strtolower($currency);
                $rateVal = "rate_" . $curr;
                $accBal = "balance_" . $curr;
                $rate = $ratesQuery->$rateVal;

                // get settings table data
                $fee_deduct = explode(",", site_settings()->deposit_fee);
                $fee = 0;

                $totalAmount = floatval($amount) * floatval($rate);

                if (feeDeductOnDailyDeposit($totalAmount, $fundUserId) <= 300) {
                    $totalAmount = ($totalAmount - $fee_deduct[0]);
                    $fee = $fee_deduct[0];
                    $amount = floatval($totalAmount) / floatval($rate);
                } elseif (feeDeductOnDailyDeposit($totalAmount, $fundUserId) > 300 && feeDeductOnDailyDeposit($totalAmount, $fundUserId) <= 1000) {
                    $totalAmount = ($totalAmount - $fee_deduct[1]);
                    $fee = $fee_deduct[1];
                    $amount = floatval($totalAmount) / floatval($rate);
                } elseif (feeDeductOnDailyDeposit($totalAmount, $fundUserId) > 1000) {
                    $totalAmount = ($totalAmount - $fee_deduct[2]);
                    $fee = $fee_deduct[2];
                    $amount = floatval($totalAmount) / floatval($rate);
                }

                // if($currency != "USD")
                // {
                //     $totalAmount     = $amount * $rate;
                // }else{
                //     $totalAmount     = $amount;
                // }
                $dp = new deposits();
                $dp->amount = $amount;
                $dp->payment_mode = "FundTransfer";
                $dp->currency = $currency;
                $dp->rate = $rate;
                $dp->total_amount = $totalAmount;
                $dp->reinvest_type = "FundTransfer";
                $dp->plan = $planid;
                $dp->user_id = $fundUserId;
                $dp->unique_id = $fund_receivers_id;
                $dp->pre_status = 'Pending';
                $dp->flag_dummy = 0;
                $dp->profit_take = 0;
                $dp->status = 'FundReceived';
                $dp->approved_at = date('Y-m-d');
                $dp->trans_id = $trans_id;
                $dp->trans_type = $trans_type;
                $dp->withdrawal_id = $withdrawal->id;

                $dp->fee_deducted = $fee;

                $dp->save();

                ////update user balance
                // $userAccInfo         = UserAccounts::where('user_id', $userinfo->id)->first();
                // $userbalance        = $userAccInfo->$accBal;
                // $userbalance        = $userbalance + $amount;
                // UserAccounts::where('user_id', $fundUserId)->update([$accBal=>$userbalance]);
                // $trade_id = deposits::insertGetId(['user_id' => 'first']);
                $trade_id = DB::getPdo()->lastInsertId();

                $response = DB::select('CALL approve_deposit(' . $dp->id . ',' . Auth::user()->id . ',0,' . date('Y-m-d') . ')');
                /*$trade_id =    response()->json(array('success' => true, 'last_insert_id' => $dp->id), 200);
                $trade_id1 = $trade_id->last_insert_id;*/
                //exit($trade_id);

                // Changes made by basit ####################
                ///store procedure is already doing all the things mention below
                // $deposit_user            =    users::where('id', $fundUserId)->first();
                // $parent_id                 =     $deposit_user->parent_id;

                // $deposit_user_parent    =    users::where('u_id', $parent_id)->first();
                // if (isset($deposit_user_parent)) {
                //     $sendBonusToParents =  $this->calculateParentBonus($trade_id);
                // }

                // Referral::sync($fundUserId);
                // Changes made by basit ####################
                $result = "FundTranffered successfull";
                return $result;
            } else {
                $result = "Fund Transfer unsuccessful";
                return $result;
            }
        } catch (\Exception $exception) {
            Log::error('Funds-Transfer unsuccessful', [
                'error message' => $exception->getMessage(),
                'withdrawal id' => $withdrawID
            ]);
            return 'Funds Transfer unsuccessful ' . $exception->getMessage();
        }

    }

    public function calculateParentBonus($deposit_id)
    {
        $deposit = deposits::where('id', $deposit_id)->where('user_id', Auth::user()->id)->first();
        if ($deposit instanceof deposits) {
            $total_amount = $deposit->total_amount;
            $userid = $deposit->user_id;
            $flag_dummy = $deposit->flag_dummy;
            $profit_take = $deposit->profit_take;
            $trans_Type = $deposit->trans_type;
            $currency = $deposit->currency;
            $amount = $deposit->amount;

            $deposit_user = users::where('id', $userid)->first();
            $plan_id = $deposit_user->plan;
            $user_uid = $deposit_user->u_id;
            $parent_id = $deposit_user->parent_id;
            $user_awarded_flag = $deposit_user->awarded_flag;

            if (($profit_take == 0 || $profit_take == "0") && ($flag_dummy == 0 && $user_awarded_flag == 0 && $trans_Type != "Reinvestment")) {
                $exitsBonuses = daily_investment_bonus::where('trade_id', $deposit_id)->first();
                $exitsBonuses1 = daily_investment_bonus::where('trade_id', $deposit_id)->where('details', 'NOT LIKE', 'Profit Bonus')->first();
                // Now Get latest Accounts Balances and rates and update Plans of Users
                if ((!isset($exitsBonuses) || !isset($exitsBonuses1)) && ($deposit_id != "" && $parent_id != "0" && $parent_id != "B4U0001")) {
                    $count = 1;
                    $calculatedBonus = 0;
                    for ($i = 0; $i < 5; $i++) {
                        $parentDetails = users::select('id', 'u_id', 'parent_id', 'plan')->where('u_id', $parent_id)->first();

                        $Parent_userID = $parentDetails->id;
                        $parentPlanid = $parentDetails->plan;
                        $parentNewId = $parentDetails->parent_id;
                        $parent_uid = $parentDetails->u_id;

                        //$parent_uid     = $parentDetails->u_id;
                        $plansDetailsQuery = plans::join('referal_investment_bonus_rules AS refinvbonus', 'plans.id', '=', 'refinvbonus.plan_id')
                            ->select('refinvbonus.first_line', 'refinvbonus.second_line', 'refinvbonus.third_line', 'refinvbonus.fourth_line', 'refinvbonus.fifth_line')
                            ->where('plans.id', $parentPlanid)->first();

                        $investment_bonus_line1 = $plansDetailsQuery->first_line;
                        $investment_bonus_line2 = $plansDetailsQuery->second_line;
                        $investment_bonus_line3 = $plansDetailsQuery->third_line;
                        $investment_bonus_line4 = $plansDetailsQuery->fourth_line;
                        $investment_bonus_line5 = $plansDetailsQuery->fifth_line;

                        if (floatval($investment_bonus_line1) > 0 && $count == 1) {
                            $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line1)) / 100;
                            $percentage = $investment_bonus_line1;
                        } elseif (floatval($investment_bonus_line2) > 0 && $count == 2) {
                            $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line2)) / 100;
                            $percentage = $investment_bonus_line2;
                        } elseif (floatval($investment_bonus_line3) > 0 && $count == 3) {
                            $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line3)) / 100;
                            $percentage = $investment_bonus_line3;
                        } elseif (floatval($investment_bonus_line4) > 0 && $count == 4) {
                            $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line4)) / 100;
                            $percentage = $investment_bonus_line4;
                        } elseif (floatval($investment_bonus_line5) > 0 && $count == 5) {
                            $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line5)) / 100;
                            $percentage = $investment_bonus_line5;
                        }
                        //$bonus = $calculatedBonus;

                        if ($calculatedBonus > 0 && $user_uid != "B4U0001" && $parent_id != "B4U0001") {
                            $daily_ibonus = new daily_investment_bonus();

                            $daily_ibonus->trade_id = $deposit_id;

                            $daily_ibonus->user_id = $user_uid;

                            $daily_ibonus->parent_id = $parent_id;
                            //    $daily_ibonus->parent_plan    =     $parentPlanid;
                            $daily_ibonus->parent_user_id = $Parent_userID;
                            $daily_ibonus->bonus = $calculatedBonus;
                            //$daily_ibonus->bonus        =     $bonus;
                            $daily_ibonus->details = "Investment Bonus with percentage of " . $percentage;

                            $daily_ibonus->save();

                            $savedb = $daily_ibonus->id;

                            $new_user_id = $daily_ibonus->user_id;
                        }
                        // Update Account will be on Update status

                        $parent_id = $parentNewId;
                        //echo "save data".$count." ids =".$savedid;
                        if ($parent_id == '0' || $parent_uid == "B4U0001") {
                            //echo $parent_id;
                            break;
                        }
                        $calculatedBonus = 0;
                        $count++;
                    } // end of loop
                }
                $daily_investment_bonus = daily_investment_bonus::where('trade_id', $deposit_id)->get();

                foreach ($daily_investment_bonus as $investmentbonus) {
                    $current_bonus = $investmentbonus->bonus;
                    $user_uid = $investmentbonus->parent_id;
                    if ($user_uid != '') {
                        $parent_user = users::where('u_id', $user_uid)->first();
                        $ref_user_id = $parent_user->id;
                        $ref_user_uid = $parent_user->u_id;

                        $userAccDetails = UserAccounts::where('user_id', $ref_user_id)->first();
                        $reference_bonus = $userAccDetails->reference_bonus;
                        $reference_bonus2 = $userAccDetails->reference_bonus;
                        if ($current_bonus > 0) {
                            $reference_bonus = $reference_bonus + $current_bonus;
                            UserAccounts::where('user_id', $ref_user_id)->update(['reference_bonus' => $reference_bonus, 'latest_bonus' => $current_bonus]);
                            $title = "Reference Bonus Updated";
                            $details = $ref_user_id . " has updated bonus to " . $reference_bonus;
                            $pre_amt = $reference_bonus2;
                            $curr_amt = $reference_bonus;
                            $currency = "";
                            $approvedby = "";
                            // Save Logs
                            $this->saveLogs($title, $details, $ref_user_id, $user_uid, $currency, $current_bonus, $pre_amt, $curr_amt, $approvedby);
                            $event = "User Deposit Approved";
                            $admin_id = Auth::user()->u_id;
                            $user_id = $user_uid;
                            $this->adminLogs($user_id, $admin_id, $deposit_id, $event);
                        }
                    }
                }// end foreach loop
                deposits::where('id', $deposit_id)->update(['profit_take' => 1]);
                return "successful";
            }
            return "unsuccessful";
        } else {
            return "denied";
        }
    }

    public function beneficiaryDetails(\App\Http\Requests\WithDrawals\BeneficiaryDetailRequest $request)
    {
        $benficiaryuid = strtoupper(trim($request['benficiaryid']));
        $userid = Auth::user()->id;
        $useruid = Auth::user()->u_id;

        if (isset($benficiaryuid)) {
            $bene_details = users::where('u_id', $benficiaryuid)->where('status', 'LIKE', 'active')->first();
            if ($bene_details instanceof users) {
                $fundBeneficiary = fund_beneficiary::where('user_id', $userid)->where('beneficiary_uid', $benficiaryuid)->first();
                if (!$fundBeneficiary instanceof fund_beneficiary) {
                    $nb = new fund_beneficiary();
                    $nb->user_id = $userid;
                    $nb->user_uid = $useruid;
                    $nb->beneficiary_id = $bene_details->id;
                    $nb->beneficiary_uid = $benficiaryuid;
                    $nb->save();
                    if (isset($bene_details->name) && $bene_details->name != "") {
                        $name = $bene_details->name;
                    } else {
                        $name = $benficiaryuid;
                    }

                    if ($name != "") {
                        $name = $name;
                    }
                    echo '<strong class="has-success-in-beni" style="color:green;">UserName: ' . $name . '</strong><br>Alert: Be careful Fund Transfer cannot be reverse.';
                } else {
                    echo '<strong style="color:red;">Beneficiary already exists in list!</strong>';
                }
            } else {
                echo '<strong style="color:red;">Beneficiary not valid or blocked. Please add correct Beneficiary !(Note: You cannot enter your own User_id) </strong>';
            }
        } else {
            echo "nodata";
        }
        exit();
    }
}
