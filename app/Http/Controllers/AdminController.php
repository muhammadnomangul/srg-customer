<?php

namespace App\Http\Controllers;

use App\current_rate;
use App\daily_investment_bonus;
use App\Http\Requests\Currency\EditCurrencyRequest;
use App\Http\Requests\Currency\SaveCurrencyRequest;
use App\Http\Requests\Currency\CurrencyIdRequest;
use App\Http\Requests\Users\MakeUserDefaulterRequest;
use App\Http\Requests\Users\ProfitCalculator;
use App\Http\Requests\Users\RefUserToB4URequest;
use App\Http\Requests\Users\UpdateAccBalanceRequest;
use App\Http\Requests\Users\SendMessageRequest;
use App\Http\Requests\Users\UserB4UIdRequest;
use App\Http\Requests\Users\UserIdRequest;
use App\Http\Requests\Users\UserMessageRequest;
use App\Http\Requests\Users\ViewPartnerDetailsRequest;
use App\Mail\RefNewUserMail;
use App\Model\AttractiveFunds;
use App\Model\Referral;
use App\Newplanslimit;
use App\ref_investment_bonus_rules;
use App\settings;
use App\solds;
use App\SubscriptionChange;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\NewUserRegistrations;
use App\Mail\NewPartnerRegistrations;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\UserAccounts;
use App\VerifyUser;
use App\plans;
use App\confirmations;
use App\users;
use App\deposits;
use App\Donation;
use App\withdrawals;
use App\admin_logs;
use DB;
use Exception;
use App\currency_rates;
use App\login_history;
use App\Currencies;
use App\Logs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client as TwilioClient;

/**
 * Class AdminController
 *
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return int|void
     */
    public function totalSubUsers()
    {

        $AuthUser = Auth::user();
        if (!$AuthUser instanceof User) {
            return redirect(url('/'));
        }


        $loged_user_id = $AuthUser->id;
        $loged_parent_id = $AuthUser->parent_id;
        $Loged_user_uid = $AuthUser->u_id;

        $secondLine = [];
        $thirdLine = [];
        $fourthLine = [];
        $fifthLine = [];
        $secondTotal = 0;
        $thirdTotal = 0;
        $fourthTotal = 0;
        $fifthTotal = 0;

        $firstLine = users::select('u_id', 'parent_id')->where('parent_id', $Loged_user_uid)->get();

        $firstTotal = count($firstLine);

        if (isset($firstLine)) {
            $totalAmount = 0;
            $counter = 0;
            $row = 0;//$firstTotal = count($firstLine);
            for ($i = 0; $i < $firstTotal; $i++) {
                $result = users::select('u_id', 'parent_id')
                    ->where('parent_id', $firstLine[$i]->u_id)
                    ->get();
                $count2 = count($result);
                if ($count2 == 0) {
                    continue;
                }
                array_push($secondLine, $result);
                //  for($j=0; $j<$count2; $j++)
                // {
                //    $totalAmount = $totalAmount + $result[$j]->account_bal;
                // }
                $counter++;
                $row = $row + $count2;
            }
            $secondLine = $secondLine;
            $secondTotal = $row;
        }

        if (isset($secondLine)) {
            $totalAmount2 = 0;
            $counter2 = 0;
            $rows = 0;

            for ($i = 0; $i < count($secondLine); $i++) {
                foreach ($secondLine[$counter2] as $lines) {
                    $result = users::select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($thirdLine, $result);
                    // for($j=0; $j<$count2; $j++)
                    //{
                    //  $totalAmount2 = $totalAmount2 + $result[$j]->account_bal;
                    //}
                    $rows = $rows + $count2;
                }
                $counter2++;
            }
            $thirdLine = $thirdLine;
            $thirdTotal = $rows;
        }
        if (isset($thirdLine)) {
            $totalAmount3 = 0;
            $counter3 = 0;
            $rows1 = 0;//$count = count($thirdLine);

            for ($i = 0; $i < count($thirdLine); $i++) {
                foreach ($thirdLine[$counter3] as $lines) {
                    $result = users::select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();

                    $count2 = count($result);

                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fourthLine, $result);
                    // for($j=0; $j<$count2; $j++)
                    //  {
                    //    $totalAmount3 = $totalAmount3 + $result[$j]->account_bal;
                    //  }
                    $rows1 = $rows1 + $count2;
                }
                $counter3++;
            }
            $fourthLine = $fourthLine;
            $fourthTotal = $rows1;
        }

        if (isset($fourthLine)) {
            $counter4 = 0;
            $totalAmount4 = 0;
            $rows2 = 0;
            for ($i = 0; $i < count($fourthLine); $i++) {
                foreach ($fourthLine[$counter4] as $lines) {
                    $result = users::select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fifthLine, $result);
                    //for($j=0; $j<$count2; $j++)
                    //{
                    //    $totalAmount4 = $totalAmount4 + $result[$j]->account_bal;
                    //}
                    $rows2 = $rows2 + $count2;
                }
                $counter4++;
            }
            $fifthLine = $fifthLine;
            $fifthTotal = $rows2;
        }
        //echo "FirstLine - ".$firstTotal."SecondLine - "
        //.$secondTotal. "ThirdLine - "
        //.$thirdTotal."FourthLine - "
        //.$fourthTotal."FifthLine - "
        //.$fifthTotal;
        //exit;
        $total = $firstTotal + $secondTotal + $thirdTotal + $fourthTotal + $fifthTotal;
        return $total;
        exit;
    }

    /**
     * Get all the sub users
     *
     * @return int|void
     */
    public function getTotalSubUsers()
    {
        //  return 1;
        return $this->totalSubUsers();
    }

    /**
     * Admin dashboard page
     *
     * @param Request $request input
     *
     * @return Factory|RedirectResponse|View
     */
    public function dashboard(Request $request)
    {

        $user = Auth::user();
        $remaining_total_count = 52;
        //logout user if not approved
        if ($user->status != "active") {
            $request->session()->flush();
            $request->session()->put('reged', 'yes');
            return redirect()->route('dashboard');
        }

        //Check if the user is referred by someone after a successful registration
        $userid = $user->id;
        $user_uid = $user->u_id;

        $history = login_history::where('user_uid', $user_uid)->orderby('id', 'DESC')->first();

        $accounts = UserAccounts::where('user_id', $userid);

        $latestprofit = UserAccounts::whereIn('user_accounts.id', function ($query) {
            $query->select('user_accounts.id')->where('user_accounts.created_at', \Illuminate\Support\Facades\DB::raw("(select max('user_accounts.created_at') from user_accounts)"));
        })->orderBy('id', 'desc')->toSql();


        $latestprofit_new = DB::select(DB::raw("SELECT sum(latest_profit) as latestprofit 
        FROM user_accounts
        WHERE id IN (SELECT id FROM user_accounts WHERE created_at = (SELECT MAX(created_at)  FROM user_accounts)) and user_id='" . $userid . "' ORDER BY id DESC"));


        if ($accounts->count()) {
            $accounts = $accounts->first();

        } else {
            $accounts = new UserAccounts();
            $accounts->user_id = $userid;
            $accounts->save();
        }

        $plans = plans::where('id', $user->plan)->first();
        $subUsers = 0;//$this->totalSubUsers();

        ## get last currency rate from cache
        $ratesQuery = Cache::remember('lastCurrentRate', settings::TopCacheTimeOut, function () {
            return current_rate::orderby('created_at', 'DESC')->first();
        });

        if ($user->type == 1 || $user->type == '1') {
            $withdrawals = $deposits = [];
            $withdrawals = withdrawals::leftJoin('users', 'withdrawals.user', '=', 'users.id')
                ->select('withdrawals.*', 'users.u_id')
                ->orderBy('withdrawals.id', 'desc')
                ->take(5)->get();

            $deposits = deposits::leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id')
                ->orderBy('deposits.id', 'desc')
                ->take(5)->get();

            $withdrawalsFee = $this->totalwithdrawalsfee();
            $Deductionamnt = $this->totaldeductionfee();
            $Donation = $this->totaldonations();


            $CovidFund = Donation::where('donation_type', 0)->sum('usd_amount');
            $OrphanFund = Donation::where('donation_type', 1)->sum('usd_amount');
            $MasjidFund = Donation::where('donation_type', 2)->sum('usd_amount');
            $QurbanFund = Donation::where('donation_type', 3)->sum('usd_amount');
            $NeedyFund = Donation::where('donation_type', 4)->sum('usd_amount');
            //   dd("dd");

            return view('admin.dashboard')->with(
                array('CovidFund' => $CovidFund, 'OrphanFund' => $OrphanFund, 'NeedyFund' => $NeedyFund,
                    'MasjidFund' => $MasjidFund, 'QurbanFund' => $QurbanFund, 'title' => 'Admin Panel', 'uplan' => $plans,
                    'total_sub_users' => $subUsers,
                    'withdrawals' => $withdrawals, 'deposits' => $deposits,
                    'accounts' => $accounts, 'ex_rates' => $ratesQuery,
                    'withdrawalsFee' => $withdrawalsFee, 'Deductionamnt' => $Deductionamnt,
                    'Donation' => $Donation)
            );
        } else {


            $this->SetRank($userid);

            $newPlanDetails = Newplanslimit::where('plan_id',$user->plan)->first();
            $withdrawals = withdrawals::where('user', $user->id)
                ->where('status', '!=', 'Cancelled')
                ->where('is_manual_cancel', '!=', '2')
                ->orderBy('id', 'desc')
                ->take(5)
                ->get();
            $deposits = deposits::where('user_id', $user->id)
                ->where('status', '!=', 'Cancelled')
                ->where('status', '!=', 'Sold')
                ->orderBy('id', 'desc')->take(5)->get();
            $history1 = login_history::where('user_uid', $user_uid)->orderBy('id', 'desc')->take(5)->get();
        }
//        $user_sub = SubscriptionChange::where('user_id', $userid)->where('is_exp', 0)->first();
//        if ($user_sub) {
//
//            $remaining_count = $remaining_total_count - $user_sub['count'];
//        } else {
            $remaining_count = 0;
//        }


        return view('dashboard')->with(
            array(
                'title' => 'User Panel',
                'uplan' => $plans,
                'newPlanDetails' => $newPlanDetails,
                'total_sub_users' => $subUsers,
                'withdrawals' => $withdrawals,
                'deposits' => $deposits,
                'accounts' => $accounts,
                'ex_rates' => $ratesQuery,
                'history' => $history,
                'history1' => $history1,
                'user' => $user,
                'latestprofit' => $latestprofit,
                'latestprofit_new' => $latestprofit_new,
                'remaining_count' => $remaining_count
            )
        );
    }


    public function SetRank($user_id)
    {
        $user_uid = Auth::user()->u_id;
        $user_rank = Auth::user()->rank;
        $user_plan = Auth::user()->plan;

        $BlueMoonPlanChilds = count(Referral::BlueMoonPlanChilds($user_id));
        $SilverRankChilds = count(Referral::SilverRankChilds($user_id));
        $AuroraPlanChilds = count(Referral::AuroraPlanChilds($user_id));
        $CoordinatorRankChilds = count(Referral::CoordinatorRankChilds($user_id));
        $CullinanPlanChilds = count(Referral::CullinanPlanChilds($user_id));
        $DiamondRankChilds = count(Referral::DiamondRankChilds($user_id));

        if (isset($user_rank) && !is_null($user_rank) && $user_rank != 'NULL') {
            $rank = 'NULL';
        }
        ////DIAMOND && PLATINUM
        //Platinum
        if ($user_rank == 'Diamond' && isset($DiamondRankChilds) && $DiamondRankChilds >= 10) {
            $rank = 'Platinum';
            //Diamond
        } elseif ($user_plan >= 4 && isset($CullinanPlanChilds) && $CullinanPlanChilds >= 10 &&
            $user_rank != 'Platinum' && $user_rank != 'Diamond') {
            $rank = 'Diamond';
            ////COORDINATOR && EXECUTIVE
            //Executive
        } elseif ($user_rank == 'Coordinator' && isset($CoordinatorRankChilds) && $CoordinatorRankChilds >= 10) {
            $rank = 'Executive';
            //Coordinator
        } elseif ($user_plan >= 3 && isset($AuroraPlanChilds) && $AuroraPlanChilds >= 10 && $user_rank != 'Coordinator'
            && $user_rank != 'Executive' && $user_rank != 'Diamond' && $user_rank != 'Platinum') {
            $rank = 'Coordinator';
            ////SILVER && GOLD
            //Gold
        } elseif ($user_rank == 'Silver' && isset($SilverRankChilds) && $SilverRankChilds >= 10) {
            $rank = 'Gold';
            //Silver
        } elseif ($user_plan >= 2 && isset($BlueMoonPlanChilds) && $BlueMoonPlanChilds >= 10
            && (is_null($user_rank) || $user_rank == 'NULL')) {
            $rank = 'Silver';
        }

        if (isset($rank)) {
            $event = "UserRank updated to " . $rank;
            if ($rank == 'NULL') {
                $event = "UserRank DownGraded from " . $user_rank;
            }
            $this->adminLogs($user_uid, '', '', $event);

            users::findOrUpdate($user_id, ['rank' => $rank]);
        }
    }

    /**
     * @return float|int
     */
    public function totalwithdrawalsfee()
    {
        $exrate = current_rate::where('id', '=', 1)->first();

        $withdrawalsFeeUSD = withdrawals::where('currency', 'USD')->sum('withdrawal_fee');

        $withdrawalsFeeBTC = withdrawals::where('currency', 'BTC')->sum('withdrawal_fee');
        $BTCrate = $exrate->rate_btc;
        $withdrawalsFeeBTC = $withdrawalsFeeBTC * $BTCrate;

        $withdrawalsFeeETH = withdrawals::where('currency', 'ETH')->sum('withdrawal_fee');
        $ETHrate = $exrate->rate_eth;
        $withdrawalsFeeETH = $withdrawalsFeeETH * $ETHrate;

        $withdrawalsFeeBCH = withdrawals::where('currency', 'BCH')->sum('withdrawal_fee');
        $BCHrate = $exrate->rate_bch;
        $withdrawalsFeeBCH = $withdrawalsFeeBCH * $BCHrate;

        $withdrawalsFeeLTC = withdrawals::where('currency', 'LTC')->sum('withdrawal_fee');
        $LTCrate = $exrate->rate_ltc;
        $withdrawalsFeeLTC = $withdrawalsFeeLTC * $LTCrate;

        $withdrawalsFeeDASH = withdrawals::where('currency', 'DASH')->sum('withdrawal_fee');
        $DASHrate = $exrate->rate_dash;
        $withdrawalsFeeDASH = $withdrawalsFeeDASH * $DASHrate;

        $withdrawalsFeeZEC = withdrawals::where('currency', 'ZEC')->sum('withdrawal_fee');
        $ZECrate = $exrate->rate_zec;
        $withdrawalsFeeZEC = $withdrawalsFeeZEC * $ZECrate;

        $withdrawalsFeeXRP = withdrawals::where('currency', 'XRP')->sum('withdrawal_fee');
        $XRPrate = $exrate->rate_xrp;
        $withdrawalsFeeXRP = $withdrawalsFeeXRP * $XRPrate;

        return $withdrawalsFeeUSD
            + $withdrawalsFeeBTC
            + $withdrawalsFeeETH
            + $withdrawalsFeeBCH
            + $withdrawalsFeeLTC
            + $withdrawalsFeeDASH
            + $withdrawalsFeeZEC
            + $withdrawalsFeeXRP;
    }

    /**
     * @return float|int
     */
    public function totaldeductionfee()
    {
        $exrate = current_rate::where('id', '=', 1)->first();

        $BTCrate = $exrate->rate_btc;
        $ETHrate = $exrate->rate_eth;
        $BCHrate = $exrate->rate_bch;
        $LTCrate = $exrate->rate_ltc;
        $DASHrate = $exrate->rate_dash;
        $ZECrate = $exrate->rate_zec;
        $XRPrate = $exrate->rate_xrp;


        $DeductamntUSD = solds::where('currency', 'USD')->sum('sale_deduct_amount');

        $DeductamntBTC = solds::where('currency', 'BTC')->sum('sale_deduct_amount');
        $DeductamntBTC = $DeductamntBTC * $BTCrate;

        $DeductamntETH = solds::where('currency', 'ETH')->sum('sale_deduct_amount');
        $DeductamntETH = $DeductamntETH * $ETHrate;

        $DeductamntBCH = solds::where('currency', 'BCH')->sum('sale_deduct_amount');
        $DeductamntBCH = $DeductamntBCH * $BCHrate;

        $DeductamntLTC = solds::where('currency', 'LTC')->sum('sale_deduct_amount');
        $DeductamntLTC = $DeductamntLTC * $LTCrate;

        $DeductamntDASH = solds::where('currency', 'DASH')->sum('sale_deduct_amount');
        $DeductamntDASH = $DeductamntDASH * $DASHrate;

        $DeductamntZEC = solds::where('currency', 'ZEC')->sum('sale_deduct_amount');
        $DeductamntZEC = $DeductamntZEC * $ZECrate;

        $DeductamntXRP = solds::where('currency', 'XRP')->sum('sale_deduct_amount');
        $DeductamntXRP = $DeductamntXRP * $XRPrate;

        return $DeductamntUSD
            + $DeductamntBTC
            + $DeductamntETH
            + $DeductamntBCH
            + $DeductamntLTC
            + $DeductamntDASH
            + $DeductamntZEC
            + $DeductamntXRP;
    }

    /**
     * @return float|int
     */
    public function totaldonations()
    {
        $exrate = current_rate::where('id', '=', 1)->first();

        $DonationUSD = withdrawals::where('currency', 'USD')->sum('donation');

        $DonationBTC = withdrawals::where('currency', 'BTC')->sum('donation');
        $BTCrate = $exrate->rate_btc;
        $DonationBTC = $DonationBTC * $BTCrate;

        $DonationETH = withdrawals::where('currency', 'ETH')->sum('donation');
        $ETHrate = $exrate->rate_eth;
        $DonationETH = $DonationETH * $ETHrate;

        $DonationBCH = withdrawals::where('currency', 'BCH')->sum('donation');
        $BCHrate = $exrate->rate_bch;
        $DonationBCH = $DonationBCH * $BCHrate;

        $DonationLTC = withdrawals::where('currency', 'LTC')->sum('donation');
        $LTCrate = $exrate->rate_ltc;
        $DonationLTC = $DonationLTC * $LTCrate;

        $DonationDASH = withdrawals::where('currency', 'DASH')->sum('donation');
        $DASHrate = $exrate->rate_dash;
        $DonationDASH = $DonationDASH * $DASHrate;

        $DonationZEC = withdrawals::where('currency', 'ZEC')->sum('donation');
        $ZECrate = $exrate->rate_zec;
        $DonationZEC = $DonationZEC * $ZECrate;

        $DonationXRP = withdrawals::where('currency', 'XRP')->sum('donation');
        $XRPrate = $exrate->rate_xrp;
        $DonationXRP = $DonationXRP * $XRPrate;

        return $DonationUSD
            + $DonationBTC
            + $DonationETH
            + $DonationBCH
            + $DonationLTC
            + $DonationDASH
            + $DonationZEC
            + $DonationXRP;
    }

    /**
     * @return Factory|View
     */
    public function searchUser()
    {
        return view('admin/searchUser')->with(
            array(
                'title' => 'Search user'
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return false|mixed|string
     */
    public function viewUserDetailsParent()
    {
        $userParntID = Auth::user()->parent_id;

        $userinfo = users::where('u_id', $userParntID)->get();
        $userUID = $userinfo[0]->u_id;
        $userParentId = $userinfo[0]->parent_id;
        $userName = $userinfo[0]->name;
        $userPhoneNo = $userinfo[0]->phone_no;
        $userPlanID = $userinfo[0]->plan;
        $email = $userinfo[0]->email;
        $usrplan = plans::where('id', $userPlanID)->first();
        if (isset($usrplan)) {
            $planname = $usrplan->name;
        } else {
            $planname = "Plan NOT SET";
        }
        $udetails = (string)view(
            'admin.partials._parent_details',
            compact(
                'userUID',
                'userName',
                'planname',
                'email',
                'userPhoneNo'
            )
        );
        return json_encode($udetails);

    }


    /**
     * @param Request $request
     *
     * @return false|mixed|string
     */
    public function viewPartnerDetail(ViewPartnerDetailsRequest $request)
    {
        $user_id = $request['id'];

        $getReferrals = Referral::where('parent_u_id', Auth::user()->u_id)->pluck('child_id');
        if (!in_array($user_id, $getReferrals->toArray())) {
            return response()->json(['user_not_found']);
        }

        if ($user_id != "") {
            $partner = "";
            if (isset($request['partner'])) {
                $partner = $request['partner'];
                // \App\Model\Referral::sync($user_id);
            }

            $userinfo = users::leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select(
                    'users.id as userid',
                    'users.u_id',
                    'users.parent_id',
                    'users.old_pass',
                    'users.plan',
                    'users.name',
                    'users.phone_no',
                    'users.type',
                    'users.email',
                    'users.awarded_flag',
                    'user_accounts.*'
                )
                ->where('users.id', $user_id)
                ->get();

            $userUID = $userinfo[0]->u_id;
            $userParentId = $userinfo[0]->parent_id;
            $userName = $userinfo[0]->name;
            $userPhoneNo = $userinfo[0]->phone_no;
            $userPlanID = $userinfo[0]->plan;
            $email = $userinfo[0]->email;
            $oldpassword = $userinfo[0]->old_pass;
            $isverify = $userinfo[0]->is_manual_verified;
            $dblverify = $userinfo[0]->double_verified;
            $awarded = $userinfo[0]->awarded_flag;

            $usrplan = plans::where('id', $userPlanID)->first();

            if (isset($usrplan)) {
                $planname = $usrplan->name;
            } else {
                $planname = "Plan NOT SET";
            }
            $udetails = (string)view(
                'admin.partials._partner_details',
                compact(
                    'userUID',
                    'userName',
                    'userParentId',
                    'planname',
                    'email',
                    'userPhoneNo',
                    'userinfo'
                )
            );
            return response()->json($udetails);
        }
    }

    /**
     * @param Request $request
     *
     * @return false|mixed|string
     */
    public function viewUserDetails(UserIdRequest $request)
    {
        $user_id = Auth::user()->id;
        $partner = "";
        if (isset($request['partner'])) {
            $partner = $request['partner'];
        }


        if (isset($request['type'])) {
            $type = $request['type'];
        } else {
            $type = "";
        }
        $userinfo = users::leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
            ->select(
                'users.id as userid',
                'users.u_id',
                'users.parent_id',
                'users.old_pass',
                'users.plan',
                'users.name',
                'users.type',
                'users.email',
                'users.awarded_flag',
                'user_accounts.*'
            )
            ->where('users.id', Auth::user()->id)
            ->get();

        $userUID = $userinfo[0]->u_id;
        $userParentId = $userinfo[0]->parent_id;
        $userName = $userinfo[0]->name;
        $userPlanID = $userinfo[0]->plan;
        $oldpassword = $userinfo[0]->old_pass;
        $isverify = $userinfo[0]->is_manual_verified;
        $dblverify = $userinfo[0]->double_verified;
        $awarded = $userinfo[0]->awarded_flag;

        $verify = admin_logs::where('user_id', $userUID)
            ->where('event', 'User Verified')
            ->orderby('id', 'DESC')
            ->first();
        $verify_by = isset($verify->admin_id) ? $verify->admin_id : '';


        $usrplan = plans::where('id', $userPlanID)->first();
        if (isset($usrplan)) {
            $planname = $usrplan->name;
        } else {
            $planname = "Plan NOT SET";
        }

        $details = (string)view(
            'admin.partials._user_details',
            compact(
                'isverify',
                'verify_by',
                'user_id',
                'dblverify',
                'userUID',
                'userinfo',
                'planname',
                'type',
                'partner'
            )
        );

        // dd($details);
        return json_encode($details);
    }


    /**
     * Save a new user
     *
     * @param Request $request form input
     *
     * @return RedirectResponse
     *
     * @throws ValidationException
     * @throws ConfigurationException
     * @throws TwilioException
     */
    public function saveuser(RefUserToB4URequest $request)
    {
        // Get the last created order
        $randomNo = rand(1000, 9999);

        $phoneNoAr = is_valid_phone_no($request->phone_no, $request->get('country_coe', null));
        if (empty($phoneNoAr['is_valid']) || $phoneNoAr['is_valid'] === false) {
            return back()->with('errormsg', "$request->phone_no is not a valid phone number");
        } else {
            $contactNo = empty($phoneNoAr['formatted_phone_no']) ? null : $phoneNoAr['formatted_phone_no'];
        }

        $lastUser = User::orderBy('created_at', 'desc')->first();
        if (!$lastUser) {    // We get here if there is no user at all
            $unique_id = "B4U0001";
        } else {
            $number = substr($lastUser->u_id, 3);
            $number = $number + 1;
            $unique_id = "B4U00" . $number;
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->Country = $request->country_name;
        $user->password = bcrypt($request['password']);
        $user->plan = 1;
        $user->photo = 'male.png';
        $user->phone_no = $contactNo;
        $user->u_id = $unique_id;
        $user->status = 'inactive';
        $user->sms_varify_code = $randomNo;
        $user->parent_id = Auth::user()->u_id;
        $user->ref_link = site_settings()->site_address . '/ref/' . $unique_id;
        $user->save();

        users::InsertUserToCallCenterService($user->id,$user->u_id,$user->phone_no);


        if ($user->parent_id == "B4U0001") {
            \Illuminate\Support\Facades\DB::select('CALL  generate_referral_tree(' . $user->id . ',0,1)');
        } else {
            \Illuminate\Support\Facades\DB::select('CALL  generate_referral_tree(' . $user->id . ',0,Null)');
        }
        users::findOrUpdate(Auth::user()->id, ['ref_link' => site_settings()->site_address . '/ref/' . $unique_id]);

        $verifyUser = new VerifyUser();
        $verifyUser->user_id = $user->id;
        $verifyUser->token = Str::random(40);
        $verifyUser->save();

        $userAccount = new UserAccounts();
        $userAccount->user_id = $user->id;
        $userAccount->save();

        Mail::to($request->email)->send(new RefNewUserMail($user->email, $request['password'], $user->u_id, $user->name, Auth::user()->name));

        $user->sendEmailVerificationNotification();
        ## create  cash box account for all users..
        $user->assignCashBoxAccountOfAllCurrencies();

        return redirect()->back()->with('message', 'User Registered Successfully!');
    }

    //update Profile photo to DB

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function updatephoto(Request $request)
    {
        $this->validate(
            $request,
            [
                'photo' => 'mimes:jpg,jpeg,png|max:5000',
            ]
        );
        //photo
        $img = $request->file('photo');
        $upload_dir = 'images';

        $image = $img->getClientOriginalName();
        $move = $img->move($upload_dir, $image);
        users::where('id', $request['id'])
            ->update(
                [
                    'photo' => $image,
                ]
            );
        return redirect()->back()
            ->with('message', 'Photo Updated successfully.');
    }

    //return add account form

    /**
     * View account details
     *
     * @param Request $request input
     *
     * @return Factory|View
     */
    public function accountdetails(Request $request)
    {
        return view('updateacct')->with(
            array(
                'title' => 'Update account details'
            )
        );
    }
    //update account and contact info

    /**
     * Update user account
     *
     * @param Request $request form input
     *
     * @return RedirectResponse
     */
    public function updateacct(Request $request)
    {
        users::where('id', $request['id'])
            ->update(
                [
                    'bank_name' => $request['bank_name'],
                    'account_name' => $request['account_name'],
                    'account_no' => $request['account_number'],
                    'btc_address' => $request['btc_address'],
                    'eth_address' => $request['eth_address'],
                    'bch_address' => $request['bch_address'],
                    'ltc_address' => $request['ltc_address'],
                    'xrp_address' => $request['xrp_address'],
                    'dash_address' => $request['dash_address'],
                    'zec_address' => $request['zec_address'],
                    //'dsh_address'     =>    $request['dsh_address'],
                ]
            );
        return redirect()->back()
            ->with('message', 'User updated successfully.');
    }

    //return add change password form

    /**
     * change password page
     *
     * @param Request $request user input
     *
     * @return Factory|View
     */
    public function changepassword(Request $request)
    {
        return view('changepassword')->with(
            array(
                'title' => 'Change Password'
            )
        );
    }

    //Update Password

    /**
     * Update password
     *
     * @param Request $request input
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function updatepass(Request $request)
    {
        if (!password_verify($request['old_password'], $request['current_password'])) {
            return redirect()->back()->with('message', 'Incorrect Old Password');
        }
        $this->validate(
            $request,
            [
                'password_confirmation' => 'same:password',
                'password' => 'min:6',
            ]
        );

        users::where('id', $request['id'])
            ->update(
                [
                    'password' => bcrypt($request['password']),
                ]
            );
        return redirect()->back()->with('message', 'Password Updated successfully.');
    }

    /**
     * @param $unique_id
     * @param $contactNo
     * @param $msg
     *
     * @return string
     * @throws ConfigurationException
     * @throws TwilioException
     */
    public function sendSMS1($unique_id, $contactNo, $msg)
    {
        $twilioAccountSid = $_ENV["TWILIO_SID"];
        $twilioAuthToken = $_ENV["TWILIO_TOKEN"];
        $twilioFromNumber = $_ENV["TWILIO_NUMBER"];

        $client = new TwilioClient($twilioAccountSid, $twilioAuthToken);
        $client->messages->create(
        // Where to send a text message (your cell phone?)
            $contactNo,
            array(
                'from' => $twilioFromNumber,
                'body' => 'Dear ' . $unique_id . " " . $msg
            )
        );
        return "success";
    }

    public function showmsg()
    {
        $users = users::where('id', Auth::user()->id)->first();
        $users->msg_bit = 0;
        $users->save();
        echo ' <div class="form-group" style="text-align:center;" > <p>' . $users->msg . '</p> </div>';

    }

    /**
     * @param $unique_id
     * @param $contactNo
     * @param $randomNo
     *
     * @return string
     * @throws ConfigurationException
     * @throws TwilioException
     */
    public function sendSMS($unique_id, $contactNo, $randomNo)
    {
        $twilioAccountSid = env('TWILIO_SID', 'AC2d34c16c1be9c8933438780e4ea98935');
        $twilioAuthToken = env('TWILIO_TOKEN', '8a19a5224c69cbf47ac952c778763a53');
        $twilioFromNumber = env('TWILIO_NUMBER', '+16144125274');

        $client = new TwilioClient($twilioAccountSid, $twilioAuthToken);
        $client->messages->create(
        // Where to send a text message (your cell phone?)
            $contactNo,
            array(
                'from' => $twilioFromNumber,
                'body' => $unique_id . ' Thank you for register on B4U GLOBAL, Your Activation Code :' . $randomNo
            )
        );
        return "success";
    }

    /**
     * @param CurrencyIdRequest $updateCurrencyRequest
     * @return Factory|View
     */
    public function currencyUpdate(CurrencyIdRequest $updateCurrencyRequest)
    {
        $title = 'Currency update';
        $currency = Currencies::where('id', $updateCurrencyRequest->id)->first();
        return view('currency_update')->with(
            array(
                'curr' => $currency,
                'title' => $title,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */

    /**
     * @return Factory|View
     */
    public function profitCalculator()
    {
        return view('user.profit.profitCalculator')
            ->with(
                array(
                    'title' => 'CalculateTotal Profit'
                )
            );
    }


    /**
     * @param Request $request
     */
    public function findProfit(ProfitCalculator $request)
    {
        $from = date("Y-m-d 00:00:00", strtotime($request['approvedAt']));
        $to = date("Y-m-d 23:59:59", strtotime($request['soldAt']));
        $amount = $request['amount'];

        $profitPercentage = currency_rates::whereBetween('created_at', [$from, $to])->sum('today_profit');
        if ($profitPercentage > 0) {
            $profitAmount = ($amount * $profitPercentage) / 100;
        } else {
            $profitAmount = 0;
        }

        $profitPercentage = number_format((float)$profitPercentage, 2, '.', '');
        $profitAmount = number_format((float)$profitAmount, 6, '.', '');

        echo "Total Profit Percentage : 
            <b> $profitPercentage %</b><br>Profit Calculated Amount :
              <b>$profitAmount</b>";
    }


    /**
     * Details of users account if they updated their account info
     * @return Factory|View|void
     */
    public function userHistory()
    {
        try {
            $Userinfo = User::with(['accountHistory'])->findOrfail(Auth::user()->id);
            return view("admin.user_account_history", compact("Userinfo"));
        } catch (Exception $e) {
            Log::error("error is userHistory method " . $e->getMessage());
            return abort(404);
        }
    }
}

