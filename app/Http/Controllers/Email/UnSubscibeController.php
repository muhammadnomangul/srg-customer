<?php

namespace App\Http\Controllers\Email;

use App\Http\Requests\Users\UserUnSubRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnSubscibeController extends Controller
{
    //
    public function unsubscribe(UserUnSubRequest $userUnSubRequest)
    {
        $observed = $userUnSubRequest->hash;
        $predicted = hash('sha256', $userUnSubRequest->user_id . "" . env('APP_KEY'));
        if ($observed != $predicted) {
            return "Invalid Link";
        }

        $user = User::findOrFail($userUnSubRequest->user_id);
        if ($user->is_subscribe == 0) {
            return "This link is expired";
        }

        $user->is_subscribe = 0;
        $user->save();


        return "\nYou are  Un-Subscribed Successfully";
    }
}
