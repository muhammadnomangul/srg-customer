<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestConstants;
use App\User;
use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CallCenterController extends Controller
{
    public function __construct()
    {
    }
    public function call(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
        return "heheheh";
    }

    public function callAllowed(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }

    public function updateFirstCall(Request $request){
        if ($request->api_token == "zLvAPcohTm1ohBUzrw9P"){
            users::whereId($request->user_id)->update(['is_first_call' => 1]);
            return response([
                'result'    => true
            ],200);
        }else{
            return response([
                'result'    => false
            ],400);
        }
    }
    public function cb(Request $request){
        Artisan::call("create:cbcreditifdebitcreated");

    }
    public function image(Request $request){
        ## upload file only and only if deposit type is re-invenst
//        return $request;
//        return $request->file('fileurl');
       // Muhamamd, 4:23 PM
$filesize = filesize($request->file('fileurl'));
        $filesize = round($filesize / 1024 / 1024, 1); // megabytes with 1 digit

         if($filesize > 1){
             return response()->json(['status'=>400,'code'=>0,'msg'=>'Image size cannot be greater then 1 MB!','data'=>null]);
         }else{
             $uploadedFile = ImageController::uploadImageToGoogleCloud($request->file('fileurl'));

             if ($uploadedFile['status']) {
                 $image = !empty($uploadedFile['result']['imageName']) ? $uploadedFile['result']['imageName'] : null;

                 return $image;
             } else {
                 $data = $uploadedFile;
                 return $data;
//            \Illuminate\Support\Facades\Log::error('Error while uploading file at Deposit',
//                [
//                    'aws-file-upload-url' => $request->get('fileurl')
//                ]);


//            return back();
             }
         }


    }
    public function sendMail(Request $request){
        $name = $request->name;

        return $name;


        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
        $email = 'nomangul934@gmail.com';
        $code = 1111;
        Artisan::call("send:mael $email $code");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }
    public function mailer(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
//        $email = 'nomangul934@gmail.com';
        $email = $request->email;
        $code = $request->otp;

        Artisan::call("send:mael $email $code");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }
    public function forgetMail(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
//        $email = 'nomangul934@gmail.com';
        $email = $request->email;
        $userId = $request->userId;
        $link = $request->link;


        Artisan::call("send:forgetLink $email $userId $link");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }
    public function RefMail(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
//        $email = 'nomangul934@gmail.com';

        $userEmail = $request->email;
        $password = $request->password;
        $b4uId = $request->b4uId;
        $invitedUsername = urldecode( $request->invitedUsername);
        $inviteBy =urldecode($request->inviteBy);

        Artisan::call("send:RefEmail $userEmail $password $b4uId $invitedUsername $inviteBy");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }

    public function verifyEmail(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
//        $email = 'nomangul934@gmail.com';

        $userEmail = $request->email;
        $otp_code = $request->otp_code;

        Artisan::call("send:verifyEmail $userEmail $otp_code");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }

    public function forgetPassword(Request $request){
        $apiToken = "zLvAPcohTm1ohBUzrw9P";
//        Artisan::call("send:mael {$request['email']} {$request['otp_code']}");
//        $email = 'nomangul934@gmail.com';
        $email = $request->email;
        $code = $request->otp;

        Artisan::call("send:forget $email $code");

        return $apiToken;
        if($apiToken != $request->api_token){
            return response()->json([
                'is_allowed' => 'no',
                'message '=>'Invalid Api-Token'
            ],403);
        }
        $validator = Validator::make($request->all(),
            [
                'phone_no'  => RequestConstants::PhoneNoValidationRequire,
            ]);
        if ($validator->fails()) {
            return response()->json([
                'is_allowed' => 'no',
                'message '=>$validator->errors()
            ],422);
        }
        $phoneNo = "+" . $request->phone_no;
        $user = users::where('phone_no','LIKE', $phoneNo)->first();
        if($user){
            if($user->is_first_call == 0){
                $user->is_first_call = 1;
                $user->save();
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            elseif ($user->is_call_sub == 1){
                return response()->json([
                    'is_allowed' => 'yes',
                    'message' => 'Success'
                ], 200);
            }
            else{
                return response()->json([
                    'is_allowed' => 'no',
                    'message' => 'Call Package is not subscribed'
                ], 404);
            }
        }else{
            return response()->json([
                'is_allowed' => 'no',
                'message' => 'User Not Found'
            ], 401);
        }

    }
}
