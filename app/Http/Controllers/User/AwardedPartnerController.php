<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\ref_investment_bonus_rules;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Laravel\VaporCli\Regions;
use Sabberworm\CSS\Settings;

class AwardedPartnerController extends Controller
{

    // Awarded Partners New Link
    public function awarded_partners($level = 1)
    {
        if (1 == Auth::user()->type || '1' == Auth::user()->type || 1 == Auth::user()->awarded_flag) {

            ini_set('max_execution_time', 30000);
            $loged_user_id = Auth::user()->id;
            $loged_parent_id = Auth::user()->parent_id;
            $Loged_user_uid = Auth::user()->u_id;
            $Loged_user_plan = Auth::user()->plan;
            $is_fixed_deposit = Auth::user()->is_fixed_deposit;
            $bonusPercentage = ref_investment_bonus_rules::where('id', $Loged_user_plan)->first();
            $first_line = $bonusPercentage->first_line;
            $second_line = $bonusPercentage->second_line;
            $third_line = $bonusPercentage->third_line;
            $fourth_line = $bonusPercentage->fourth_line;
            $fifth_line = $bonusPercentage->fifth_line;


            if ($level == 1) {
                $response = Cache::remember($loged_user_id . '_level_1', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->firstLevel($Loged_user_uid);
                    $response['total'] = $response['firstTotal'];
                    $response['level'] = $level;
                    $response['user_level_count'] = $response['count1Final'];
                    return $response;
                });
                return view('awarded_partners.index', $response);
            }

            if ($level == 2) {

                $response = Cache::remember($loged_user_id . '_level_2', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->secondLevel($Loged_user_uid);
                    $response['total'] = $response['secondTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['secondLine'];
                    $response['counter'] = 0;
                    $response['user_level_count'] = $response['count2Final'];
                    return $response;
                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 3) {

                $response = Cache::remember($loged_user_id . '_level_3', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->thirdLevel($Loged_user_uid);
                    $response['total'] = $response['thirdTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['thirdLine'];
                    $response['counter'] = 1;
                    $response['user_level_count'] = $response['count3Final'];
                    return $response;
                });


                return view('awarded_partners.two_level', $response);
            }

            if ($level == 4) {
                $response = Cache::remember($loged_user_id . '_level_4', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->fourLevel($Loged_user_uid);
                    $response['total'] = $response['fourthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['fourthLine'];
                    $response['counter'] = 2;
                    $response['user_level_count'] = $response['count4Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 5) {
                $response = Cache::remember($loged_user_id . '_level_5', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->fiveLevel($Loged_user_uid);
                    $response['total'] = $response['fifthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['fifthLine'];
                    $response['counter'] = 3;
                    $response['user_level_count'] = $response['count5Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 6) {
                $response = Cache::remember($loged_user_id . '_level_6', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->sixLevel($Loged_user_uid);
                    $response['total'] = $response['sixthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['sixthLine'];
                    $response['counter'] = 4;
                    $response['user_level_count'] = $response['count6Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 7) {
                $response = Cache::remember($loged_user_id . '_level_7', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->sevenLevel($Loged_user_uid);
                    $response['total'] = $response['seventhTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['seventhLine'];
                    $response['counter'] = 5;
                    $response['user_level_count'] = $response['count7Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 8) {
                $response = Cache::remember($loged_user_id . '_level_8', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->eightLevel($Loged_user_uid);
                    $response['total'] = $response['eighthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['eighthLine'];
                    $response['counter'] = 6;
                    $response['user_level_count'] = $response['count8Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 9) {
                $response = Cache::remember($loged_user_id . '_level_9', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->nineLevel($Loged_user_uid);
                    $response['total'] = $response['ninthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['ninthLine'];
                    $response['counter'] = 7;
                    $response['user_level_count'] = $response['count9Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 10) {
                $response = Cache::remember($loged_user_id . '_level_10', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->tenthLevel($Loged_user_uid);
                    $response['total'] = $response['tenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['tenthLine'];
                    $response['counter'] = 8;
                    $response['user_level_count'] = $response['count10Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 11) {
                $response = Cache::remember($loged_user_id . '_level_11', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->elevenLevel($Loged_user_uid);
                    $response['total'] = $response['eleventhTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['eleventhLine'];
                    $response['counter'] = 9;
                    $response['user_level_count'] = $response['count11Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 12) {
                $response = Cache::remember($loged_user_id . '_level_12', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->twelveLevel($Loged_user_uid);
                    $response['total'] = $response['twelvethTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['twelvethLine'];
                    $response['counter'] = 10;
                    $response['user_level_count'] = $response['count12Final'];
                    return $response;

                });
                return view('awarded_partners.two_level', $response);
            }

            if ($level == 13) {
                $response = Cache::remember($loged_user_id . '_level_13', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->thirteenLevel($Loged_user_uid);
                    $response['total'] = $response['thirteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['thirteenthLine'];
                    $response['counter'] = 11;
                    $response['user_level_count'] = $response['count13Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 14) {
                $response = Cache::remember($loged_user_id . '_level_14', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->fourteenLevel($Loged_user_uid);
                    $response['total'] = $response['fourteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['fourteenthLine'];
                    $response['counter'] = 12;
                    $response['user_level_count'] = $response['count14Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 15) {
                $response = Cache::remember($loged_user_id . '_level_15', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->fifteenLevel($Loged_user_uid);
                    $response['total'] = $response['fifteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['fifteenthLine'];
                    $response['counter'] = 13;
                    $response['user_level_count'] = $response['count15Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 16) {
                $response = Cache::remember($loged_user_id . '_level_16', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->sixteenLevel($Loged_user_uid);
                    $response['total'] = $response['sixteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['sixteenthLine'];
                    $response['counter'] = 14;
                    $response['user_level_count'] = $response['count16Final'];
                    return $response;

                });
                return view('awarded_partners.two_level', $response);
            }

            if ($level == 17) {
                $response = Cache::remember($loged_user_id . '_level_17', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->seventeenLevel($Loged_user_uid);
                    $response['total'] = $response['seventeenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['seventeenthLine'];
                    $response['counter'] = 15;
                    $response['user_level_count'] = $response['count17Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 18) {
                $response = Cache::remember($loged_user_id . '_level_18', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->eighteenLevel($Loged_user_uid);
                    $response['total'] = $response['eighteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['eighteenthLine'];
                    $response['counter'] = 16;
                    $response['user_level_count'] = $response['count18Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 19) {
                $response = Cache::remember($loged_user_id . '_level_19', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->nineteenLevel($Loged_user_uid);
                    $response['total'] = $response['ninteenthTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['ninteenthLine'];
                    $response['counter'] = 17;
                    $response['user_level_count'] = $response['count19Final'];
                    return $response;

                });

                return view('awarded_partners.two_level', $response);
            }

            if ($level == 20) {
                $response = Cache::remember($loged_user_id . '_level_20', \App\settings::minCacheTimeOut, function () use ($Loged_user_uid, $level) {
                    $response = $this->twenteenLevel($Loged_user_uid);
                    $response['total'] = $response['twentiethTotal'];
                    $response['level'] = $level;
                    $response['linee'] = $response['twentiethLine'];
                    $response['counter'] = 18;
                    $response['user_level_count'] = $response['count20Final'];
                    return $response;

                });
                return view('awarded_partners.two_level', $response);
            }

            dd('die.');

            $firstLine = [];
            $firstTotal = 0;
            $count1Final = 0;

            if ($level == 1) {


            }


            // Calculate Level-2 Partners
            $count2Final = 0;
            $secondLine = [];
            $secondTotal = 0;
            if (isset($firstLine)) {
                $result = '';
                $level_ids = collect($firstLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count2Final = count($result);
                $secondTotal = $result->sum("total");
                array_push($secondLine, $result);

            }
            // Calculate Level-3 Partners

            $count3Final = 0;
            $thirdLine = [];
            $thirdTotal = 0;

            if (isset($secondLine)) {
                $result = '';
                $level_ids = collect($secondLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count3Final = count($result);
                $thirdTotal = $result->sum("total");
                array_push($thirdLine, $result);
            }

            // Calculate Level-4 Partners
            $count4Final = 0;
            $fourthLine = [];
            $fourthTotal = 0;

            if (isset($thirdLine)) {
                $result = '';
                $level_ids = collect($thirdLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count4Final = count($result);
                $fourthTotal = $result->sum("total");
                array_push($fourthLine, $result);
            }

            // Calculate Level-5 Partners
            $count5Final = 0;
            $fifthLine = [];
            $fifthTotal = 0;

            if (isset($fourthLine)) {
                $result = '';
                $level_ids = collect($fourthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count5Final = count($result);
                $fifthTotal = $result->sum("total");
                array_push($fifthLine, $result);
            }

            // Calculate Level-6 Partners
            $count6Final = 0;
            $sixthLine = [];
            $sixthTotal = 0;

            if (isset($fifthLine)) {
                $result = '';
                $level_ids = collect($fifthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count6Final = count($result);
                $sixthTotal = $result->sum("total");
                array_push($sixthLine, $result);
            }

            // Calculate Level7 Partners
            $count7Final = 0;
            $seventhLine = [];
            $seventhTotal = 0;

            if (isset($sixthLine)) {
                $result = '';
                $level_ids = collect($sixthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count7Final = count($result);
                $seventhTotal = $result->sum("total");
                array_push($seventhLine, $result);
            }

            // Calculate Level8 Partners
            $count8Final = 0;
            $eighthLine = [];
            $eighthTotal = 0;

            if (isset($seventhLine)) {
                $result = '';
                $level_ids = collect($seventhLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count8Final = count($result);
                $eighthTotal = $result->sum("total");
                array_push($eighthLine, $result);

            }

            // Calculate Level9 Partners
            $count9Final = 0;
            $ninthLine = [];
            $ninthTotal = 0;

            if (isset($eighthLine)) {
                $result = '';
                $level_ids = collect($eighthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count9Final = count($result);
                $ninthTotal = $result->sum("total");
                array_push($ninthLine, $result);
            }

            // Calculate Level-10 Partners
            $count10Final = 0;
            $tenthLine = [];
            $tenthTotal = 0;
            if (isset($ninthLine)) {
                $result = '';
                $level_ids = collect($ninthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count10Final = count($result);
                $tenthTotal = $result->sum("total");
                array_push($tenthLine, $result);
            }

            // Calculate Level-11 Partners
            $count11Final = 0;
            $eleventhLine = [];
            $eleventhTotal = 0;
            if (isset($tenthLine)) {
                $result = '';
                $level_ids = collect($tenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count11Final = count($result);
                $eleventhTotal = $result->sum("total");
                array_push($eleventhLine, $result);

            }

            // Calculate Level-12 Partners
            $count12Final = 0;
            $twelvethLine = [];
            $twelvethTotal = 0;
            if (isset($eleventhLine)) {
                $result = '';
                $level_ids = collect($eleventhLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count12Final = count($result);
                $twelvethTotal = $result->sum("total");
                array_push($twelvethLine, $result);

            }

            // Calculate Level-13 Partners
            $count13Final = 0;
            $thirteenthLine = [];
            $thirteenthTotal = 0;
            if (isset($twelvethLine)) {
                $result = '';
                $level_ids = collect($twelvethLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count13Final = count($result);
                $thirteenthTotal = $result->sum("total");
                array_push($thirteenthLine, $result);

            }

            // Calculate Level-14 Partners
            $count14Final = 0;
            $fourteenthLine = [];
            $fourteenthTotal = 0;
            if (isset($thirteenthLine)) {
                $result = '';
                $level_ids = collect($thirteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count14Final = count($result);
                $fourteenthTotal = $result->sum("total");
                array_push($fourteenthLine, $result);
            }

            // Calculate Level-15 Partners
            $count15Final = 0;
            $fifteenthLine = [];
            $fifteenthTotal = 0;
            if (isset($fourteenthLine)) {
                $result = '';
                $level_ids = collect($fourteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count15Final = count($result);
                $fifteenthTotal = $result->sum("total");
                array_push($fifteenthLine, $result);

            }

            // Calculate Level-16 Partners
            $count16Final = 0;
            $sixteenthLine = [];
            $sixteenthTotal = 0;
            if (isset($fifteenthLine)) {
                $result = '';
                $level_ids = collect($fifteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count16Final = count($result);
                $sixteenthTotal = $result->sum("total");
                array_push($sixteenthLine, $result);

            }

            // Calculate Level-17 Partners
            $count17Final = 0;
            $seventeenthLine = [];
            $seventeenthTotal = 0;
            if (isset($sixteenthLine)) {
                $result = '';
                $level_ids = collect($sixteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count17Final = count($result);
                $seventeenthTotal = $result->sum("total");
                array_push($seventeenthLine, $result);
            }

            // Calculate Level-18 Partners
            $count18Final = 0;
            $eighteenthLine = [];
            $eighteenthTotal = 0;
            if (isset($seventeenthLine)) {
                $result = '';
                $level_ids = collect($seventeenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count18Final = count($result);
                $eighteenthTotal = $result->sum("total");
                array_push($eighteenthLine, $result);

            }
            // Calculate Level-19 Partners
            $count19Final = 0;
            $ninteenthLine = [];
            $ninteenthTotal = 0;
            if (isset($eighteenthLine)) {
                $result = '';
                $level_ids = collect($eighteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count19Final = count($result);
                $ninteenthTotal = $result->sum("total");
                array_push($ninteenthLine, $result);
            }
            // Calculate Level-20 Partners
            $count20Final = 0;
            $twentiethLine = [];
            $twentiethTotal = 0;
            if (isset($ninteenthLine)) {
                $result = '';
                $level_ids = collect($ninteenthLine)->flatten()->pluck('u_id')->all();
                $result = DB::table('users')
                    ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.u_id',
                        'users.parent_id',
                        'users.name',
                        'users.email',
                        'users.created_at',
                        DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                    )
                    ->whereIn('users.parent_id', $level_ids)
                    ->groupBy('users.id')
                    ->orderBy('total', 'DESC')
                    ->get();
                $count20Final = count($result);
                $twentiethTotal = $result->sum("total");
                array_push($twentiethLine, $result);

            }

            $account_balance = $firstTotal + $secondTotal + $thirdTotal + $fourthTotal + $fifthTotal;

            $account_balance = $account_balance + $sixthTotal + $seventhTotal + $eighthTotal + $ninthTotal + $tenthTotal;

            $account_balance = $account_balance + $eleventhTotal + $twelvethTotal + $thirteenthTotal + $fourteenthTotal + $fifteenthTotal;
            $account_balance = $account_balance + $sixteenthTotal + $seventeenthTotal + $eighteenthTotal + $ninteenthTotal + $twentiethTotal;
            $title = 'All Partners Info';
            return view('user.awarded.awarded_partners')->with(
                [
                    'level' => $level,
                    'title' => $title,
                    'account_balance' => $account_balance,

                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'thirdLine' => $thirdLine,
                    'fourthLine' => $fourthLine,
                    'fifthLine' => $fifthLine,
                    'sixthLine' => $sixthLine,
                    'seventhLine' => $seventhLine,
                    'eighthLine' => $eighthLine,
                    'ninthLine' => $ninthLine,
                    'tenthLine' => $tenthLine,
                    'eleventhLine' => $eleventhLine,
                    'twelvethLine' => $twelvethLine,
                    'thirteenthLine' => $thirteenthLine,
                    'fourteenthLine' => $fourteenthLine,
                    'fifteenthLine' => $fifteenthLine,
                    'sixteenthLine' => $sixteenthLine,
                    'seventeenthLine' => $seventeenthLine,
                    'eighteenthLine' => $eighteenthLine,
                    'ninteenthLine' => $ninteenthLine,
                    'twentiethLine' => $twentiethLine,

                    'firstTotal' => $firstTotal,
                    'secondTotal' => $secondTotal,
                    'thirdTotal' => $thirdTotal,
                    'fourthTotal' => $fourthTotal,
                    'fifthTotal' => $fifthTotal,
                    'sixthTotal' => $sixthTotal,
                    'seventhTotal' => $seventhTotal,
                    'eighthTotal' => $eighthTotal,
                    'ninthTotal' => $ninthTotal,
                    'tenthTotal' => $tenthTotal,
                    'eleventhTotal' => $eleventhTotal,
                    'twelvethTotal' => $twelvethTotal,
                    'thirteenthTotal' => $thirteenthTotal,
                    'fourteenthTotal' => $fourteenthTotal,
                    'fifteenthTotal' => $fifteenthTotal,
                    'sixteenthTotal' => $sixteenthTotal,
                    'seventeenthTotal' => $seventeenthTotal,
                    'eighteenthTotal' => $eighteenthTotal,
                    'ninteenthTotal' => $ninteenthTotal,
                    'twentiethTotal' => $twentiethTotal,

                    'userlevel_1' => $count1Final, 'userlevel_2' => $count2Final, 'userlevel_3' => $count3Final, 'userlevel_4' => $count4Final, 'userlevel_5' => $count5Final,
                    'userlevel_6' => $count6Final, 'userlevel_7' => $count7Final, 'userlevel_8' => $count8Final, 'userlevel_9' => $count9Final, 'userlevel_10' => $count10Final,
                    'userlevel_11' => $count11Final, 'userlevel_12' => $count12Final, 'userlevel_13' => $count13Final, 'userlevel_14' => $count14Final, 'userlevel_15' => $count15Final,
                    'userlevel_16' => $count16Final, 'userlevel_17' => $count17Final, 'userlevel_18' => $count18Final, 'userlevel_19' => $count19Final, 'userlevel_20' => $count20Final,
                ]
            );
        } else {
            return redirect()->back()->with('Errormsg', 'Invalid Link!');
        }
    }

    public function firstLevel($Loged_user_uid)
    {
        $firstLine = DB::table('users')
            ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.u_id',
                'users.parent_id',
                'users.name',
                'users.email',
                'users.created_at',
                DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
            )
            ->where('users.parent_id', $Loged_user_uid)
            ->groupBy('users.id')
            ->orderBy('total', 'DESC')
            ->get();

        $firstTotal = DB::table('users')
            ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
            ->select('deposits.total_amount as total')
            ->where('users.parent_id', $Loged_user_uid)
            ->where('deposits.status', 'Approved')
            ->where('deposits.trans_type', 'NewInvestment')
            ->sum('deposits.total_amount');

        $count1Final = count($firstLine);

        return [
            'firstLine' => $firstLine,
            'firstTotal' => $firstTotal,
            'count1Final' => $count1Final,
        ];
    }

    public function secondLevel($Loged_user_uid)
    {
        $secondLine = [];
        $firstLevel = $this->firstLevel($Loged_user_uid);
        if (is_array($firstLevel) && !empty($firstLevel['firstLine'])) {
            $firstLine = $firstLevel['firstLine'];
            $level_ids = collect($firstLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count2Final = count($result);
            $secondTotal = $result->sum("total");

            array_push($secondLine, $result);

            return [
                'secondLine' => $secondLine,
                'secondTotal' => $secondTotal,
                'count2Final' => $count2Final,
            ];
        } else {
            return [];
        }
    }

    public function thirdLevel($Loged_user_uid)
    {
        $thirdLine = [];
        $secondLevel = $this->secondLevel($Loged_user_uid);
        if (is_array($secondLevel) && !empty($secondLevel['secondLine'])) {
            $secondLine = $secondLevel['secondLine'];

            $level_ids = collect($secondLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count3Final = count($result);
            $thirdTotal = $result->sum("total");
            array_push($thirdLine, $result);

            return [
                'thirdLine' => $thirdLine,
                'thirdTotal' => $thirdTotal,
                'count3Final' => $count3Final,
            ];
        } else {
            return [];
        }
    }

    public function fourLevel($Loged_user_uid)
    {
        $fourthLine = [];
        $thirdLevel = $this->thirdLevel($Loged_user_uid);
        if (is_array($thirdLevel) && !empty($thirdLevel['thirdLine'])) {
            $thirdLine = $thirdLevel['thirdLine'];

            $level_ids = collect($thirdLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count4Final = count($result);
            $fourthTotal = $result->sum("total");
            array_push($fourthLine, $result);

            return [
                'fourthLine' => $fourthLine,
                'fourthTotal' => $fourthTotal,
                'count4Final' => $count4Final,
            ];
        } else {
            return [];
        }
    }

    public function fiveLevel($Loged_user_uid)
    {
        $fifthLine = [];
        $fourLevel = $this->fourLevel($Loged_user_uid);
        if (is_array($fourLevel) && !empty($fourLevel['fourthLine'])) {
            $fourLevel = $fourLevel['fourthLine'];

            $level_ids = collect($fourLevel)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count5Final = count($result);
            $fifthTotal = $result->sum("total");
            array_push($fifthLine, $result);

            return [
                'fifthLine' => $fifthLine,
                'fifthTotal' => $fifthTotal,
                'count5Final' => $count5Final,
            ];
        } else {
            return [];
        }
    }

    public function sixLevel($Loged_user_uid)
    {
        $sixthLine = [];
        $fiveLevel = $this->fiveLevel($Loged_user_uid);
        if (is_array($fiveLevel) && !empty($fiveLevel['fifthLine'])) {
            $fifthLine = $fiveLevel['fifthLine'];

            $level_ids = collect($fifthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count6Final = count($result);
            $sixthTotal = $result->sum("total");
            array_push($sixthLine, $result);

            return [
                'sixthLine' => $sixthLine,
                'sixthTotal' => $sixthTotal,
                'count6Final' => $count6Final,
            ];
        } else {
            return [];
        }
    }

    public function sevenLevel($Loged_user_uid)
    {
        $seventhLine = [];
        $sixLevel = $this->sixLevel($Loged_user_uid);
        if (is_array($sixLevel) && !empty($sixLevel['sixthLine'])) {
            $sixthLine = $sixLevel['sixthLine'];

            $level_ids = collect($sixthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count7Final = count($result);
            $seventhTotal = $result->sum("total");
            array_push($seventhLine, $result);

            return [
                'seventhLine' => $seventhLine,
                'seventhTotal' => $seventhTotal,
                'count7Final' => $count7Final,
            ];
        } else {
            return [];
        }
    }

    public function eightLevel($Loged_user_uid)
    {
        $eighthLine = [];
        $sevenLevel = $this->sevenLevel($Loged_user_uid);
        if (is_array($sevenLevel) && !empty($sevenLevel['seventhLine'])) {
            $seventhLine = $sevenLevel['seventhLine'];

            $level_ids = collect($seventhLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count8Final = count($result);
            $eighthTotal = $result->sum("total");
            array_push($eighthLine, $result);

            return [
                'eighthLine' => $eighthLine,
                'eighthTotal' => $eighthTotal,
                'count8Final' => $count8Final,
            ];
        } else {
            return [];
        }
    }

    public function nineLevel($Loged_user_uid)
    {
        $ninthLine = [];
        $eightLevel = $this->eightLevel($Loged_user_uid);
        if (is_array($eightLevel) && !empty($eightLevel['eighthLine'])) {
            $eightLevel = $eightLevel['eighthLine'];

            $level_ids = collect($eightLevel)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count9Final = count($result);
            $ninthTotal = $result->sum("total");
            array_push($ninthLine, $result);

            return [
                'ninthLine' => $ninthLine,
                'ninthTotal' => $ninthTotal,
                'count9Final' => $count9Final,
            ];
        } else {
            return [];
        }
    }

    public function tenthLevel($Loged_user_uid)
    {
        $tenthLine = [];
        $nineLevel = $this->nineLevel($Loged_user_uid);
        if (is_array($nineLevel) && !empty($nineLevel['ninthLine'])) {
            $ninthLine = $nineLevel['ninthLine'];

            $level_ids = collect($ninthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count10Final = count($result);
            $tenthTotal = $result->sum("total");
            array_push($tenthLine, $result);

            return [
                'tenthLine' => $tenthLine,
                'tenthTotal' => $tenthTotal,
                'count10Final' => $count10Final,
            ];
        } else {
            return [];
        }
    }

    public function elevenLevel($Loged_user_uid)
    {
        $eleventhLine = [];
        $tenthLevel = $this->tenthLevel($Loged_user_uid);
        if (is_array($tenthLevel) && !empty($tenthLevel['tenthLine'])) {
            $tenthLine = $tenthLevel['tenthLine'];

            $level_ids = collect($tenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count11Final = count($result);
            $eleventhTotal = $result->sum("total");
            array_push($eleventhLine, $result);

            return [
                'eleventhLine' => $eleventhLine,
                'eleventhTotal' => $eleventhTotal,
                'count11Final' => $count11Final,
            ];
        } else {
            return [];
        }
    }

    public function twelveLevel($Loged_user_uid)
    {
        $twelvethLine = [];
        $elevenLevel = $this->elevenLevel($Loged_user_uid);
        if (is_array($elevenLevel) && !empty($elevenLevel['eleventhLine'])) {
            $eleventhLine = $elevenLevel['eleventhLine'];

            $level_ids = collect($eleventhLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count12Final = count($result);
            $twelvethTotal = $result->sum("total");
            array_push($twelvethLine, $result);

            return [
                'twelvethLine' => $twelvethLine,
                'count12Final' => $count12Final,
                'twelvethTotal' => $twelvethTotal,
            ];
        } else {
            return [];
        }
    }

    public function thirteenLevel($Loged_user_uid)
    {
        $thirteenthLine = [];
        $twelveLevel = $this->twelveLevel($Loged_user_uid);
        if (is_array($twelveLevel) && !empty($twelveLevel['twelvethLine'])) {
            $twelvethLine = $twelveLevel['twelvethLine'];

            $level_ids = collect($twelvethLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count13Final = count($result);
            $thirteenthTotal = $result->sum("total");
            array_push($thirteenthLine, $result);

            return [
                'thirteenthLine' => $thirteenthLine,
                'count13Final' => $count13Final,
                'thirteenthTotal' => $thirteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function fourteenLevel($Loged_user_uid)
    {
        $fourteenthLine = [];
        $thirteenLevel = $this->thirteenLevel($Loged_user_uid);
        if (is_array($thirteenLevel) && !empty($thirteenLevel['thirteenthLine'])) {
            $thirteenthLine = $thirteenLevel['thirteenthLine'];

            $level_ids = collect($thirteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count14Final = count($result);
            $fourteenthTotal = $result->sum("total");
            array_push($fourteenthLine, $result);

            return [
                'fourteenthLine' => $fourteenthLine,
                'count14Final' => $count14Final,
                'fourteenthTotal' => $fourteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function fifteenLevel($Loged_user_uid)
    {
        $fifteenthLine = [];
        $fourteenthLine = $this->fourteenLevel($Loged_user_uid);
        if (is_array($fourteenthLine) && !empty($fourteenthLine['fourteenthLine'])) {
            $fourteenthLine = $fourteenthLine['fourteenthLine'];

            $level_ids = collect($fourteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count15Final = count($result);
            $fifteenthTotal = $result->sum("total");
            array_push($fifteenthLine, $result);

            return [
                'fifteenthLine' => $fifteenthLine,
                'count15Final' => $count15Final,
                'fifteenthTotal' => $fifteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function sixteenLevel($Loged_user_uid)
    {
        $sixteenthLine = [];
        $fifteenthLine = $this->fifteenLevel($Loged_user_uid);
        if (is_array($fifteenthLine) && !empty($fifteenthLine['fifteenthLine'])) {
            $fifteenthLine = $fifteenthLine['fifteenthLine'];

            $level_ids = collect($fifteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count16Final = count($result);
            $sixteenthTotal = $result->sum("total");
            array_push($sixteenthLine, $result);

            return [
                'sixteenthLine' => $sixteenthLine,
                'count16Final' => $count16Final,
                'sixteenthTotal' => $sixteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function seventeenLevel($Loged_user_uid)
    {
        $seventeenthLine = [];
        $sixteenthLine = $this->sixteenLevel($Loged_user_uid);
        if (is_array($sixteenthLine) && !empty($sixteenthLine['sixteenthLine'])) {
            $sixteenthLine = $sixteenthLine['sixteenthLine'];

            $level_ids = collect($sixteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count17Final = count($result);
            $seventeenthTotal = $result->sum("total");
            array_push($seventeenthLine, $result);

            return [
                'seventeenthLine' => $seventeenthLine,
                'count17Final' => $count17Final,
                'seventeenthTotal' => $seventeenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function eighteenLevel($Loged_user_uid)
    {
        $eighteenthLine = [];
        $seventeenthLine = $this->seventeenLevel($Loged_user_uid);
        if (is_array($seventeenthLine) && !empty($seventeenthLine['seventeenthLine'])) {
            $seventeenthLine = $seventeenthLine['seventeenthLine'];

            $level_ids = collect($seventeenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count18Final = count($result);
            $eighteenthTotal = $result->sum("total");
            array_push($eighteenthLine, $result);

            return [
                'eighteenthLine' => $eighteenthLine,
                'count18Final' => $count18Final,
                'eighteenthTotal' => $eighteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function nineteenLevel($Loged_user_uid)
    {
        $ninteenthLine = [];
        $eighteenthLine = $this->eighteenLevel($Loged_user_uid);
        if (is_array($eighteenthLine) && !empty($eighteenthLine['eighteenthLine'])) {
            $eighteenthLine = $eighteenthLine['eighteenthLine'];

            $level_ids = collect($eighteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count19Final = count($result);
            $ninteenthTotal = $result->sum("total");
            array_push($ninteenthLine, $result);
            return [
                'ninteenthLine' => $ninteenthLine,
                'count19Final' => $count19Final,
                'ninteenthTotal' => $ninteenthTotal,
            ];
        } else {
            return [];
        }
    }

    public function twenteenLevel($Loged_user_uid)
    {
        $twentiethLine = [];
        $ninteenthLine = $this->nineteenLevel($Loged_user_uid);
        if (is_array($ninteenthLine) && !empty($ninteenthLine['ninteenthLine'])) {
            $ninteenthLine = $ninteenthLine['ninteenthLine'];

            $level_ids = collect($ninteenthLine)->flatten()->pluck('u_id')->all();
            $result = DB::table('users')
                ->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.u_id',
                    'users.parent_id',
                    'users.name',
                    'users.email',
                    'users.created_at',
                    DB::raw(' IF((deposits.trans_type = "NewInvestment" && deposits.status = "Approved"),SUM(deposits.total_amount),0) as total')
                )
                ->whereIn('users.parent_id', $level_ids)
                ->groupBy('users.id')
                ->orderBy('total', 'DESC')
                ->get();
            $count20Final = count($result);
            $twentiethTotal = $result->sum("total");
            array_push($twentiethLine, $result);

            return [
                'twentiethLine' => $twentiethLine,
                'count20Final' => $count20Final,
                'twentiethTotal' => $twentiethTotal,
            ];


        } else {
            return [];
        }
    }

    public function clear_cache()
    {
        $loggedInUserId = Auth::user()->id;
        for ($x = 0; $x < 21; $x++) {
            Cache::pull($loggedInUserId . '_level_' . $x);
        }
        return back();
    }
}
