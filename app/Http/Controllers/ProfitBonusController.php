<?php

namespace App\Http\Controllers;

use App\currency_rates;
use App\UserAccounts;
use App\withdrawals;
use Illuminate\Support\Facades\Auth;
use App\daily_profit_bonus;
use App\daily_investment_bonus;
use App\referal_profit_bonus_rules;
use DB;

class ProfitBonusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
        set_time_limit(0);
        // GET EMAIL TEMPLATE DATA SENT IN EMAILS
    }


    public function daily_profit()
    {
       /* $daily_profit_list = DB::table('daily_profit_bonus')
            ->select('daily_profit_bonus.*')
            ->where('daily_profit_bonus.user_id', Auth::user()->u_id)
            ->where('daily_profit_bonus.trade_id', '!=', "")
            ->orderby('daily_profit_bonus.created_at', 'DESC')->get();*/
       $newuserId = Auth::user()->id;
       $totalProfitBonus = round(daily_profit_bonus::where('user', '=', $newuserId)->sum('today_profit'), 2);

       $ratesQueryResults =currency_rates::orderby('created_at', 'DESC')->first();
        //    $rates_usd          = $ratesQueryResults->rate_usd;
       /*  $rates_btc = $ratesQueryResults->rate_btc;
        $rates_eth = $ratesQueryResults->rate_eth;
        $rates_ltc = $ratesQueryResults->rate_ltc;
        $rates_bch = $ratesQueryResults->rate_bch;
        $rates_xrp = $ratesQueryResults->rate_xrp;
        $rates_dash = $ratesQueryResults->rate_dash;
        $rates_zec = $ratesQueryResults->rate_zec;

        //$totalProfitBonus = "0";
            */

        $totalProfitBonusUSD = daily_profit_bonus::where('user', '=', $newuserId)->where('currency', '=', "USD")->sum('today_profit');
        $totalProfitBonusBTC = daily_profit_bonus::where('user', '=', $newuserId)->where('currency', '=', "BTC")->sum('crypto_profit');
        $totalProfitBonusETH = daily_profit_bonus::where('user', '=', $newuserId)->where('currency', '=', "ETH")->sum('crypto_profit');
     /*   $totalProfitBonusLTC = daily_profit_bonus::where('user', '=', $logeduserId)->where('currency', '=', "LTC")->sum('crypto_profit');
        $totalProfitBonusBCH = daily_profit_bonus::where('user', '=', $logeduserId)->where('currency', '=', "BCH")->sum('crypto_profit');
        $totalProfitBonusXRP = daily_profit_bonus::where('user', '=', $logeduserId)->where('currency', '=', "XRP")->sum('crypto_profit');
        $totalProfitBonusDASH = daily_profit_bonus::where('user', '=', $logeduserId)->where('currency', '=', "DASH")->sum('crypto_profit');
        $totalProfitBonusZEC = daily_profit_bonus::where('user', '=', $logeduserId)->where('currency', '=', "ZEC")->sum('crypto_profit');
*/

        $old_profit_data = UserAccounts::where('user_id', $newuserId)->first();
        $old_profit_usd = $old_profit_data->old_profit_usd;
        $old_profit_btc = $old_profit_data->old_profit_btc;
        $old_profit_eth = $old_profit_data->old_profit_eth;

       /* $totalNew = $totalProfitBonusUSD + ($totalProfitBonusBTC * $rates_btc) + ($totalProfitBonusETH * $rates_eth) + ($totalProfitBonusLTC * $rates_ltc) + ($totalProfitBonusBCH * $rates_bch) + ($totalProfitBonusXRP * $rates_xrp) + ($totalProfitBonusDASH * $rates_dash) + ($totalProfitBonusZEC * $rates_zec);*/
        $totalOld = $old_profit_usd + ($old_profit_btc * $ratesQueryResults->rate_btc) + ($old_profit_eth * $ratesQueryResults->rate_eth);
        //$total_USD = $totalNew + $totalOld;
        $total_USD = $totalProfitBonus + $totalOld;

        $withdrawn_usd = withdrawals::where('user', $newuserId)->where('currency', '=', "USD")->where('payment_mode', '=', "Profit")->where('created_at', '>=', "2018-12-15")->where('status', '=', "Approved")->sum('amount');
        $withdrawn_btc = withdrawals::where('user', $newuserId)->where('currency', '=', "BTC")->where('payment_mode', '=', "Profit")->where('created_at', '>=', "2018-12-15")->where('status', '=', "Approved")->sum('amount');
        $withdrawn_eth = withdrawals::where('user', $newuserId)->where('currency', '=', "ETH")->where('payment_mode', '=', "Profit")->where('created_at', '>=', "2018-12-15")->where('status', '=', "Approved")->sum('amount');

        $pre_withdrawn_usd = withdrawals::where('user', $newuserId)->where('currency', '=', "USD")->where('payment_mode', '=', "Profit")->where('created_at', '<', "2018-12-15")->where('status', '=', "Approved")->sum('amount');
        $pre_withdrawn_btc = withdrawals::where('user', $newuserId)->where('currency', '=', "BTC")->where('payment_mode', '=', "Profit")->where('created_at', '<', "2018-12-15")->where('status', '=', "Approved")->sum('amount');
        $pre_withdrawn_eth = withdrawals::where('user', $newuserId)->where('currency', '=', "ETH")->where('payment_mode', '=', "Profit")->where('created_at', '<', "2018-12-15")->where('status', '=', "Approved")->sum('amount');

        return view(
            'daily_profit',
            ['title' => 'Daily Profit',
             'total_USD' => $total_USD ,'totalProfitBonus' => $totalProfitBonus,
             'totalProfitBonusUSD' => $totalProfitBonusUSD,'totalProfitBonusBTC' => $totalProfitBonusBTC,'totalProfitBonusETH' => $totalProfitBonusETH,
             'old_profit_usd' => $old_profit_usd,'old_profit_btc' => $old_profit_btc,'old_profit_eth' => $old_profit_eth,
             'withdrawn_usd' => $withdrawn_usd, 'withdrawn_btc' => $withdrawn_btc, 'withdrawn_eth' => $withdrawn_eth,
             'pre_withdrawn_usd' => $pre_withdrawn_usd, 'pre_withdrawn_btc' => $pre_withdrawn_btc, 'pre_withdrawn_eth' => $pre_withdrawn_eth
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     * Purpose: Daily Profit data in json.
     */
    public function daily_profit_json()
    {
        $daily_profit_list = daily_profit_bonus::where('user', Auth::user()->id)->where('trade_id', '!=', "")->orderBy('id', 'desc');
        return datatables()->of($daily_profit_list)->toJson();
    }

    //Return Daily Bonus list

    public function daily_bonus()
    {
        return view('daily_bonus', ['title' => 'Daily Bonus']);
    }


    public function daily_bonus_json()
    {
        $logeduserId = Auth::user()->u_id;
        $daily_bonus_list = daily_investment_bonus::where('parent_id', $logeduserId)
                          ->where('trade_id', '!=', "")
                          ->orderby('created_at', 'DESC');
        return datatables()->of($daily_bonus_list)->addIndexColumn()->toJson();
    }
}
