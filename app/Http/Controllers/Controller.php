<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UserIdRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use App\settings;
use App\UserAccounts;
use App\VerifyUser;

//use App\account;
use App\plans;
use App\agents;
use App\users;
use App\deposits;
use App\withdrawals;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use DB;
use App\currency_rates;
use App\Currencies;
use App\Logs;
use App\admin_logs;
use App\login_history;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\daily_investment_bonus;
use Illuminate\Support\Facades\Storage;


use Maatwebsite\Excel\Facades\Excel;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

//use Excel;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

//    public $settings;
    protected $mail_from_address;
    protected $mail_from_name;

    public function __construct()
    {
        // $this->middleware('auth');
        $this->mail_from_address = config('mail.from.address');
        $this->mail_from_name = config('mail.from.name');
    }

    public function checkCallPackageSubscriptionLimit(){
        $date = date('y-m-d');
        if($date >= date('y-m-d', strtotime(Auth::user()->call_sub_at . '+1 Month'))){
            $user = Auth::user();
            $user->is_call_sub = 0;
            $user->save();
            users::updateCallPackageOnCallCenterService($user->id,false);
        }
    }

    public function saveLogs($title, $details, $userid, $user_uid, $curr, $amt, $pre_amt, $curr_amt, $approvedby)
    {
        $userLogs = new Logs();
        $userLogs->title = $title;
        $userLogs->details = $details;
        $userLogs->user_id = $userid;
        $userLogs->user_uid = $user_uid;
        $userLogs->currency = $curr;
        $userLogs->amount = $amt;
        $userLogs->pre_amount = $pre_amt;
        $userLogs->curr_amount = $curr_amt;
        $userLogs->approved_by = $approvedby;
        $userLogs->save();
        $userlogsid = $userLogs->id;

        if ($userlogsid != "") {
            return "success";
        } else {
            return "false";
        }
        exit;
    }

    public function adminLogs($user_id, $admin_id, $trade_id, $event)
    {
        $adminLogs = new admin_logs();
        $adminLogs->user_id = $user_id;
        $adminLogs->admin_id = $admin_id;
        $adminLogs->trade_id = $trade_id;
        $adminLogs->event = $event;
        $adminLogs->save();

        $adminLogsid = $adminLogs->id;

        if ($adminLogsid != "") {
            return "success";
        } else {
            return "false";
        }
    }

    // Referral Users view
    public function referuser()
    {
        return view('referuser')->with(
            array(
                'title' => 'Refer user',
                'settings' => settings::getSettings()
            )
        );
    }


    //Skip enter account details
    public function skip_account(Request $request)
    {
        $request->session()->put('skip_account', 'skip account');
        return redirect()->route('dashboard');
    }


    //Clear user Account
    public function clearacct(Request $request, $id)
    {
        users::where('id', $id)
            ->update(['account_bal' => '0', 'ref_bonus' => '0',]);
        return redirect()->back()->with('message', 'Account cleared to $0.00');
    }


    //route Refer user route
    public function ref(Request $request)
    {
        if (count(users::where('id', Auth::user()->id)->first()) == 1) {
            $request->session()->put('ref_by', Auth::user()->id);
        }
        return redirect()->route('home');

    }

    //Daily User Logs
    public function dailyLogs()
    {
        // $daily_logs_list = DB::table('logs')->orderby('logs.created_at', 'DESC')->get()->first();
        // $daily_logs_list2 = Logs::get();
        // dd($daily_logs_list2);

        $settings = settings::getSettings();
        $title = 'Daily User Logs';

        return view('admin/logs', ['settings' => $settings, 'title' => $title]);
    }

    public function loginhistory($user_uid, $ip, $location)
    {
        $history = new login_history();
        $history->user_uid = $user_uid;
        $history->ip = $ip;
        $history->location = $location;
        $history->save();
    }

    public function ip_info($ip = null, $purpose = "location", $deep_detect = true)
    {
        $output = null;
        if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                }
            }
        }
        $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), null, strtolower(trim($purpose)));
        $support = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city" => @$ipdat->geoplugin_city,
                            "state" => @$ipdat->geoplugin_regionName,
                            "country" => @$ipdat->geoplugin_countryName,
                            "country_code" => @$ipdat->geoplugin_countryCode,
                            "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1) {
                            $address[] = $ipdat->geoplugin_regionName;
                        }
                        if (@strlen($ipdat->geoplugin_city) >= 1) {
                            $address[] = $ipdat->geoplugin_city;
                        }
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    public function getUserIP()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    public function loginhistorytable()
    {
        $history2 = login_history::where('user_uid', Auth::user()->u_id)->orderby('created_at', 'DESC')->get();

        $settings = settings::getSettings();
        $title = 'Login History';

        return view('loginhistory', ['settings' => $settings, 'title' => $title, 'history2' => $history2,]);
    }


    //email Template
}
