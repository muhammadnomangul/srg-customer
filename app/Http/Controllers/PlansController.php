<?php

namespace App\Http\Controllers;

use App\Http\Requests\Plans\AddUpdatePlansRequest;
use App\Http\Requests\Plans\PlansIdRequest;
use App\ref_investment_bonus;
use App\ref_profit_bonus;
use App\settings;
use Carbon\Carbon;
use Dompdf\Image\Cache;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Model\Referral;
use DB;
use App\users;
use App\plans;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use App\Console\Commands\PlansCron;

class PlansController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */

    //  ALL Plans Functions
    //Plans route
    public function plans()
    {
        $title = 'Plans';
        $JoinsQuery = plans::join('referal_investment_bonus_rules', 'referal_investment_bonus_rules.plan_id', '=', 'plans.id')
            ->join('referal_profit_bonus_rules', 'referal_profit_bonus_rules.plan_id', '=', 'plans.id')
            ->where('plans.type', 'Main')->orderby('plans.id', 'ASC')->get();
        return view('plans', ['plans' => $JoinsQuery, 'pplans' => $JoinsQuery, 'title' => $title]);
    }

    //Promo Plans route
    public function pplans()
    {
        return view('pplans')
            ->with(
                array(
                    'title' => 'Promo Plans',
                    'plans' => plans::where('type', 'promo')->get(),
                )
            );
    }

    //Jon a plan
    public function joinplan(PlansIdRequest $joinPlanRequest)
    {
        $plan = plans::where('id', $joinPlanRequest->id)->first();
        if ($plan->type == 'Main') {
            users::findOrUpdate(Auth::user()->id,
                    [
                        'plan' => $plan->id,
                        'entered_at' => Carbon::now(),
                    ]
                );
        } elseif ($plan->type == 'Promo') {
            users::findOrUpdate(Auth::user()->id,
                    [
                        'promo_plan' => $plan->id,
                    ]
                );
        }
        return redirect()->route('dashboard')
            ->with('message', 'Congratulations! You successfully joined a plan.');
    }

}
