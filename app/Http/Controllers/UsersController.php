<?php

namespace App\Http\Controllers;

use App\Albums;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Contact\SendContactRequest;
use App\Http\Requests\GoogleAuthiSetupOneTimePasswordRequest;
use App\Http\Requests\Misc\NumberValidationRequest;
use App\Http\Requests\Referrals\ReferralChildIdRequest;
use App\Http\Requests\Referrals\SaveReferralsTree;
use App\Http\Requests\RequestConstants;
use App\Http\Requests\SavePinTwoFARequest;
use App\Http\Requests\UpdateUserKycRequest;
use App\Http\Requests\User\EditPhoneRequest;
use App\Http\Requests\Users\EditAccountInfoRequest;
use App\Http\Requests\Users\EditWalletAccountInformation;
use App\Http\Requests\Users\GetUserInfoRequest;
use App\Http\Requests\Users\ResetUserPasswordRequest;
use App\Http\Requests\Users\Update2FAAuthenticationRequest;
use App\Http\Requests\Users\UpdatePasswordRequest;
use App\Http\Requests\Users\UpdateRoleRequest;
use App\Http\Requests\Users\UpdateUserAvatar;
use App\Http\Requests\Users\UserDeleteRequest;
use App\Http\Requests\Users\UserIdRequest;
use App\ImageGallery;
use App\Kyc;
use App\Logs;
use App\settings;
use App\solds;
use App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Log;
use fypyuhu\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\agents;
use App\users;
use App\plans;
use App\Model\Referral;
use App\AlbumImages;
use App\deposits;
use App\withdrawals;
use DB;
use App\Banks;
use App\Event;
use App\UserWithdrawRule;
use Mail;
use App\Countries;
use App\User;
use App\UserAccountHistory;
use App\VerifyUser;
use App\UserAccounts;
use App\OTPToken;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use PragmaRX\Google2FAQRCode\Google2FA;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    /**
     * @return Factory|View
     * @throws Exception
     */
    public function index()
    {

        $JoinsQuery = \Illuminate\Support\Facades\Cache::remember(config('cachevalue.mplansCache'), settings::minCacheTimeOut, function () {
            return plans::join('referal_investment_bonus_rules', 'referal_investment_bonus_rules.plan_id', '=', 'plans.id')
                ->join('referal_profit_bonus_rules', 'referal_profit_bonus_rules.plan_id', '=', 'plans.id')
                ->where('plans.type', 'Main')->orderby('plans.id', 'ASC')->get();
        });
        $withdrawals = Cache::remember(config('cachevalue.withdrawals'), settings::TopCacheTimeOut, function () {
            return withdrawals::leftJoin('users', 'withdrawals.user', '=', 'users.id')
                ->select('withdrawals.*', 'users.u_id')
                ->where('withdrawals.amount', '>', 0)
                ->orderBy('withdrawals.id', 'desc')
                ->take(6)->get();

        });
        $solds = Cache::remember(config('cachevalue.soldCache'), settings::TopCacheTimeOut, function () {
            return solds::leftJoin('users', 'solds.user_id', '=', 'users.id')
                ->select('solds.*', 'users.u_id')
                ->where('solds.amount', '>', 0)
                ->orderBy('solds.id', 'desc')
                ->take(6)->get();

        });
        $deposits = Cache::remember(config('cachevalue.depositCache'), settings::TopCacheTimeOut, function () {
            return deposits::leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id')
                ->where('deposits.amount', '>', 0)
                ->orderBy('deposits.id', 'desc')
                ->take(6)->get();

        });
        $images_gallery = Cache::remember(config('cachevalue.imageGalleryCache'), settings::minCacheTimeOut, function () {
            return ImageGallery::orderBy('id', 'desc')->take(6)->get();
        });
        $promo_id = 13;
        $album_gallery = Cache::remember(config('cachevalue.albumGalleryCache'), settings::minCacheTimeOut, function () use ($promo_id) {
            return Albums::where('id', '!=', $promo_id)->orderBy('id', 'desc')->take(2)->get();
        });
        $promo = Cache::remember(config('cachevalue.albumsCache'), settings::minCacheTimeOut, function () use ($promo_id) {
            $albumsRecords = Albums::where('id', $promo_id)->first();
            if (empty($albumsRecords)) {
                return [];
            } else {
                return $albumsRecords;
            }
        });
        $promo_imgs = Cache::remember(config('cachevalue.albumsIdCache'), settings::minCacheTimeOut, function () use ($promo_id) {
            return AlbumImages::where('albums_id', $promo_id)->orderBy('id', 'desc')->get();
        });
        $events = [];
        $data = Cache::remember(config('cachevalue.eventsAllCache'), settings::minCacheTimeOut, function () {
            return Event::all();
        });


        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new DateTime($value->start_date),
                    new DateTime($value->end_date . ' +1 day')
                /*,
                null,
                [
                'color' => '#ff0000',
                'url' => '#',
                ] */
                );
            }
        }

        $calendar = Calendar::addEvents($events);

        $defaultOptions = [
            'header' => [
                'left' => 'title',
                /*
                'left' => 'prev,next ',
                'right' => 'month,agendaWeek,agendaDay',
                'title'=>'Weekend', */
            ],
            'defaultView' => 'month',
            'firstDay' => 1,
            'height' => 500,
            'weekMode' => 'liquid',
            'aspectRatio' => 2,
        ];

        $calendar->setOptions($defaultOptions);
        return view('home.index')->with(array(
            'title' => site_settings()->site_title,
            'pplans' => $JoinsQuery,
            'withdrawals' => $withdrawals,
            'deposits' => $deposits,
            'solds' => $solds,
            'promo' => $promo,
            'promo_imgs' => $promo_imgs,
            'calendar' => $calendar,
            'images_gallery' => $images_gallery,
            'album_gallery' => $album_gallery
        ));
    }

    /**
     * Licensing and registration route
     *
     * @return Factory|View
     */
    public function licensing()
    {
        return view('home.licensing')
            ->with(
                array(
                    'title' => 'Licensing, regulation and registration',
                )
            );
    }

    /**
     * @return Factory|View
     */
    public function terms()
    {
        return view('home.terms')
            ->with(
                array(
                    'title' => 'Terms of Service',
                )
            );
    }

    /**
     * Privacy policy route
     *
     * @return Factory|View
     */
    public function privacy()
    {
        return view('home.agreement')
            ->with(
                array(
                    'title' => 'Privacy Policy',
                )
            );
    }

    /**
     * @return Factory|View
     */
    public function partnership_agreement()
    {
        return view('home.partnership_agreement')
            ->with(
                array(
                    'title' => 'Partnership Agreement',
                )
            );
    }

    /**
     * Render FAQ page
     *
     * @return Factory|View
     */
    public function faq()
    {
        return view('home.faq')
            ->with(
                array(
                    'title' => 'FAQs',
                )
            );
    }

    /**
     * Render about us page
     *
     * @return Factory|View
     */
    public function about()
    {
        return view('home.about')
            ->with(
                array(
                    'title' => 'About',
                )
            );
    }

    /**
     * Pcalculator route
     *
     * @return Factory|View
     */
    public function pcalculator()
    {
        return view('home.profitCalculator')
            ->with(
                array(
                    'title' => 'ProfitCalculator',
                )
            );
    }

    /**
     * Contact route
     *
     * @return Factory|View
     */
    public function contact()
    {
        return view('home.contact')
            ->with(
                array(
                    'title' => 'Contact',
                )
            );
    }

    /**
     * Render contact us page
     *
     * @return Factory|View
     */
    public function contactus()
    {
        return view('home.contactus')
            ->with(
                array(
                    'title' => 'Contact Us',
                )
            );
    }

    /**
     * Update profile photo to DB*
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function updatephoto(UpdateUserAvatar $request)
    {

        $uploadedFile = SignedUrlUploadController::uploadImageToGoogleCloud($request->get('fileurl'), 'profile_Img');
        if ($uploadedFile['status']) {
            $image = !empty($uploadedFile['result']['imageName']) ? $uploadedFile['result']['imageName'] : null;
        } else {
            \Illuminate\Support\Facades\Log::error('Error while updating profile picture',
                [
                    'aws-file-upload-url' => $request->request->get('fileurl')
                ]);
        }
        if (empty($image)) {
            $image = Auth::user()->photo;
        }

        users::findOrUpdate(Auth::user()->id, ['photo' => $image]);
        return redirect("/dashboard/accountdetails")->with('message', 'Profile photo has been updated Successful');

    }

    /**
     * @param Update2FAAuthenticationRequest $request
     * @param Google2FA $google2FA
     * @return Application|RedirectResponse|Redirector
     * @Purpose Change User's 2FA Authentication Process
     */
    public function change2FaAuthentication(Update2FAAuthenticationRequest $request, Google2FA $google2FA)
    {
        try {
            $loggedInUser = Auth::user();

            ## Before proceed, verify is really value is changed by him.
            if ($request->auth_type == $loggedInUser->two_fa_auth_type) {
                return back();
            } else {
                ## if authenticate type if google two fa then redirect user to setup his authentication method
                if ($request->auth_type == 2 && empty($loggedInUser->google2fa_secret)) {
                    ## take user to setup google authenticator first time.
                    return TwoFAAuthenticateFacade::getBarCodeScannerImage($google2FA);
                } elseif ($request->auth_type == 5 && empty($loggedInUser->secure_pin)) {
                    ## take Pin from User
                    $request->session()->put('otpVerified',1);
                    return TwoFAAuthenticateFacade::savedPinSetup();
                } else {
                    $loggedInUser->two_fa_auth_type = $request->auth_type;
                    $loggedInUser->save();
                    return redirect(url('dashboard/accountdetails'))->with('message', 'Two factory authentication type has been changed.');
                }

            }
        } catch (Exception $exception) {
            \Illuminate\Support\Facades\Log::error($exception->getMessage());
            return redirect(url('dashboard/accountdetails'));
        }
    }

    /**
     *
     * @Purpose :: Setup google authi, this function accept google authi one time password and check is this authenticate or not.
     * @param GoogleAuthiSetupOneTimePasswordRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function setupGoogle2FA(GoogleAuthiSetupOneTimePasswordRequest $request)
    {

        ## save user google 2fa authenticate.
        $loggedInUser = Auth::user();
        try {
            $loggedInUser->google2fa_secret = $request->request->get('secret');
            $loggedInUser->save();

            ## merge six different input into one..
            $oneTimePassword = TwoFAAuthenticateFacade::mergeSixOneTimePasswordInputIntoOne($request);
            ## now validate, is entered password against the current bar code, is right or not.
            $authenticator = app(Authenticator::class)->boot($request);
            if ($authenticator->isAuthenticated()) {
                ## User has successfully tried first time google authenticator so, now enable google two for his profile.
                $loggedInUser->two_fa_auth_type = 2;
                $loggedInUser->save();
                \Illuminate\Support\Facades\Session::flash('message', 'Two factory authentication type has been changed.');
                return redirect(url('dashboard/accountdetails'));
            } else {
                ## if user is not authenticated then revert back is google authenticator code
                $loggedInUser->google2fa_secret = null;
                $loggedInUser->save();
                \Illuminate\Support\Facades\Session::flash('errormsg', 'Invalid Google 2FA Password, Please retry');
                return redirect(url('dashboard/accountdetails'));
            }

        } catch (Exception $exception) {
            ## if user is not authenticated then revert back is google authenticator code
            $loggedInUser->google2fa_secret = null;
            $loggedInUser->save();
            \Illuminate\Support\Facades\Log::error($exception->getMessage());
            return redirect(url('dashboard/accountdetails'));
        }
    }

    /**
     * @Purpose :: Setup Save PIN Two FA,
     * @param SavePinTwoFARequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function setupSavedPin(SavePinTwoFARequest $request)
    {
        ## save user google 2fa authenticate.
        $loggedInUser = Auth::user();
        try {
            ## merge six different input into one..
            $savedResponse = null;

            TwoFAAuthenticateFacade::mergeSixOneTimePasswordInputIntoOne($request);
            $oneTimePassword = $request->request->get('one_time_password', null);
            if ($request->session()->get('otpVerified') == 1 ) {
                if (!empty($oneTimePassword)) {
                    $loggedInUser->two_fa_auth_type = 5;
                    $loggedInUser->secure_pin = User::generateEncryptedTwoFAPIN($oneTimePassword);
                    $savedResponse = $loggedInUser->save();
                    Session::remove('otpVerified');
                    ##
                    if (env('APP_DEBUG', false)) {
                        \Illuminate\Support\Facades\Log::info('one time password is ' . $oneTimePassword, [
                            'all request' => $request->all()
                        ]);
                    }
                }
            }
            if ($savedResponse) {
                ## User has successfully tried first time PIN Controller so, now Saved PIN
                \Illuminate\Support\Facades\Session::flash('message', 'Two factory authentication type has been changed, to saved PIN');
            } else {
                \Illuminate\Support\Facades\Session::flash('errormsg', 'Failed to enable Saved PIN 2FA');
            }

            return redirect(url('dashboard/accountdetails'));
        } catch (Exception $exception) {
            ## if user is not authenticated then revert back is google saved pin
            $loggedInUser->two_fa_auth_type = 1;
            $loggedInUser->secure_pin = null;
            $loggedInUser->save();
            \Illuminate\Support\Facades\Log::error($exception->getMessage());
            return redirect(url('dashboard/accountdetails'));
        }
    }

    /**
     * Return add account form
     *
     * @param Request $request
     *
     * @return Factory|View
     */
    public function accountdetails()
    {
        $AuthUser = Auth::user();
//        dd($AuthUser);
        $account_history = $AuthUser->accountHistory()->latest()->first();
        $countries = Countries::get();
        $xrp_address = explode(",", $AuthUser->xrp_address);
        return view('updateacct')->with(
            array(
                'title' => 'Update account details',
                'history' => $account_history,
                'xrp_address' => $xrp_address,
                'countries' => $countries,
                'user' => $AuthUser
            )
        );
    }
    public function accountdetailsPhone()
    {
        $AuthUser = Auth::user();
        $account_history = $AuthUser->accountHistory()->latest()->first();
        $countries = Countries::get();
        $xrp_address = explode(",", $AuthUser->xrp_address);
        return view('user_profile.updateacct')->with(
            array(
                'title' => 'Update account details',
                'history' => $account_history,
                'xrp_address' => $xrp_address,
                'countries' => $countries
            )
        );
    }

    /**
     * Update account and contact info
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */

    public function validated($address){
        $decoded = $this->decodeBase58($address);

        $d1 = hash("sha256", substr($decoded,0,21), true);
        $d2 = hash("sha256", $d1, true);

        if(substr_compare($decoded, $d2, 21, 4)){
            throw new \Exception("bad digest");
        }
        return true;
    }
    public function decodeBase58($input) {
        $alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

        $out = array_fill(0, 25, 0);
        for($i=0;$i<strlen($input);$i++){
            if(($p=strpos($alphabet, $input[$i]))===false){
                dd('invalid character found');
                throw new \Exception("invalid character found");
            }
            $c = $p;
            for ($j = 25; $j--; ) {
                $c += (int)(58 * $out[$j]);
                $out[$j] = (int)($c % 256);
                $c /= 256;
                $c = (int)$c;
            }
            if($c != 0){
                throw new \Exception("address too long");
            }
        }

        $result = "";
        foreach($out as $val){
            $result .= chr($val);
        }

        return $result;
    }
    public function updateacct(EditWalletAccountInformation $request)
    {
        //  \Illuminate\Support\Facades\DB::taUser::ble('users')->where('id', $request['id'])->update(
//       $result =  $this->validated($request->btc_address);
        $string = $request->btc_address;

        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $string))
        {
            return redirect("/dashboard/accountdetails")->with('error', 'Invalid btc Address ');

            // one or more of the 'special characters' found in $string
        }

//        dd($result);
        $data = [
            'btc_address' => $request->btc_address,
            'eth_address' => $request->eth_address,
            'bch_address' => $request->bch_address,
            'ltc_address' => $request->ltc_address,
            'dash_address' => $request->dash_address,
            'zec_address' => $request->zec_address,
            'rsc_address' => $request->rsc_address,
            'usdt_address' => $request->usdt_address,
        ];
        if (!empty($request['xrp_address1']) && !empty($request['xrp_address2'])) {
            $data = array_merge(['xrp_address' => $request->xrp_address1 . ',' . $request->xrp_address2], $data);
        } elseif ((!empty($request['xrp_address1']) && empty($request['xrp_address2'])) || (empty($request['xrp_address1']) && !empty($request['xrp_address2']))) {
            $request->validate([
                'xrp_address1' => 'required',
                'xrp_address2' => 'required',
            ], [
                'xrp_address1.required' => 'XRP-Address field is missing. Please fill both XRP-Address and XRP-Tag together.',
                'xrp_address2.required' => 'XRP-Tag field is missing. Please fill both XRP-Address and XRP-Tag together.',
            ]);
        }

        users::findOrUpdate(Auth::user()->id, $data);


        $userAccountHistory = new UserAccountHistory();
        $userAccountHistory->user_id = Auth::user()->id;
        $userAccountHistory->btc_address = $request->btc_address;
        $userAccountHistory->eth_address = $request->eth_address;
        $userAccountHistory->bch_address = $request->bch_address;
        $userAccountHistory->ltc_address = $request->ltc_address;
        $userAccountHistory->xrp_address = $request->xrp_address1 . ',' . $request->xrp_address2;
        $userAccountHistory->dash_address = $request->dash_address;
        $userAccountHistory->zec_address = $request->zec_address;
        $userAccountHistory->rsc_address = $request->rsc_address;
        $userAccountHistory->usdt_address = $request->usdt_address;
        $userAccountHistory->updated_by = (app('request')->session()->get("Admin_Id")) ? app('request')->session()->get("Admin_Id") : Auth::user()->id;
        $userAccountHistory->save();

        return redirect("/dashboard/accountdetails")->with('message', 'User updated Sucessful');

    }

    public function updateprofile(EditAccountInfoRequest $request)
    {

//        dd($request);
//        $phoneNoAr = is_valid_phone_no($request->phone_no, $request->get('country_coe', null));
//        if (empty($phoneNoAr['is_valid']) || $phoneNoAr['is_valid'] === false) {
//            return back()->with('errormsg', "$request->phone_no is not a valid phone number");
//        } else {
//            $phoneNo = empty($phoneNoAr['formatted_phone_no']) ? null : $phoneNoAr['formatted_phone_no'];
//        }

        $country = $request['Country'];
        $countryID = 0;
        if ($country != "") {
            $countryDetails = Countries::where('country_name', 'Like', $country)->first();
            $countryID = $countryDetails->id;
        }
        if($country != "Pakistan"){

             $title  = strtolower($request->usa_account_holder_name);
             $name  = strtolower($request->account_name);
             if($title != "" &&  $name != ""){
                 $result = strcmp($title,$name);
                 if($result != 0){

                     return back()->with('errormsg', "Account Title and Account Holder Name should be same ..");
                 }
             }

        }

        if (isset($request['bank_name']) && $request['bank_name'] != "") {
            $bankName = $request['bank_name2'];
            $countryDetails = Banks::where('country_name', 'Like', $country)->where('bank_name', 'Like', $bankName)->first();
            if (!isset($countryDetails)) {
                $addBank = new Banks();
                $addBank->country_id = $countryID;
                $addBank->country_name = $country;
                $addBank->bank_name = $bankName;
                $addBank->save();
            }
        }

         if(Auth::user()->Country == "Canada"){
//            dd($request->usa_account_holder_name);
            users::findOrUpdate(Auth::user()->id,
                [
                    'usa_account_holder_name' => $request->usa_account_holder_name,
                    'date_of_birth' => $request->date_of_birth,
                    'full_address' => $request->full_address,
                    'full_bank_name' => $request->full_bank_name,
                    'institute_bank_number' => $request->institute_bank_number,
                    'transit_number' => $request->transit_number,
//                    'routing_number' => $request->routing_number,
                    'bank_account_type' => $request->bank_account_type

                ]
            );
        } if(Auth::user()->Country == "United States"
            || Auth::user()->Country == "United States minor outlying islands"){
//            dd($request->usa_account_holder_name);
            users::findOrUpdate(Auth::user()->id,
                [
                    'usa_account_holder_name' => $request->usa_account_holder_name,
                    'date_of_birth' => $request->date_of_birth,
                    'full_address' => $request->full_address,
                    'full_bank_name' => $request->full_bank_name,
                    'routing_number' => $request->routing_number,
                    'bank_account_type' => $request->bank_account_type

                ]
            );
        } if(Auth::user()->Country == "United Kingdom"){
//            dd($request->usa_account_holder_name);
            users::findOrUpdate(Auth::user()->id,
                [
                    'usa_account_holder_name' => $request->usa_account_holder_name,
                    'date_of_birth' => $request->date_of_birth,
                    'full_address' => $request->full_address,
                    'full_bank_name' => $request->full_bank_name,
                    'routing_number' => $request->routing_number,
                    'bank_account_type' => $request->bank_account_type,
                    'updateprofile' => $request->updateprofile

                ]
            );
        }
        if(Auth::user()->Country == "Europe"
            || Auth::user()->Country == "France"
            || Auth::user()->Country == "France, Metropolitan"
            || Auth::user()->Country == "French Guiana"
            || Auth::user()->Country == "French Polynesia"
            || Auth::user()->Country == "French Southern Territories"
            || Auth::user()->Country == "Georgia"
            || Auth::user()->Country == "Germany"
            || Auth::user()->Country == "Greece"
            || Auth::user()->Country == "Greenland"
            || Auth::user()->Country == "Italy"){
//            dd($request->usa_account_holder_name);
            users::findOrUpdate(Auth::user()->id,
                [
                    'usa_account_holder_name' => $request->usa_account_holder_name,
                    'date_of_birth' => $request->date_of_birth,
                    'full_address' => $request->full_address,
                    'full_bank_name' => $request->full_bank_name,
//                    'routing_number' => $request->routing_number,
                    'bank_account_type' => $request->bank_account_type,
                    'iban_number' => $request->iban_number

                ]
            );
        }
//        dd('kjfwe');
        users::findOrUpdate(Auth::user()->id,
            [
                'bank_name' => $request->bank_name,
                'account_name' => $request->account_name,
                'account_no' => $request->account_no,
                'acc_hold_No' => $request->acc_hold_No,
                'name' => $request->name,
//                'phone_no' => $phoneNo,
//                'email' => $request->email,
                'Country' => $request->Country,
                'kin_bank_info' => $request->kin_bank_info
            ]
        );


        $userAccountHistory = new UserAccountHistory();
        $userAccountHistory->user_id = Auth::user()->id;
        $userAccountHistory->bank_name = $request->bank_name;
        $userAccountHistory->account_name = $request->account_name;
        $userAccountHistory->account_no = $request->account_no;
        $userAccountHistory->acc_hold_No = $request->acc_hold_No;
        $userAccountHistory->updated_by = (app('request')->session()->get("Admin_Id")) ? app('request')->session()->get("Admin_Id") : Auth::user()->id;
        $userAccountHistory->save();


        ## Keep null at email_verified_at when use change his email again.
//        if ($request->user()->email !== $request->email) {
//
//            ## check is email is attached with other user or not..
//            ## before going forward, please check, Is request email is not attached to other and verified by him. I tried to do with the Request Validation file, But didn't proper answer, so, in emergency case i m writing query here.
//            $isEmailApproveOrAttachedWithOtherUser = User::checkIsEmailVerifiedAndAttachedToOtherUser($request->email);
//            if ($isEmailApproveOrAttachedWithOtherUser) {
//                return back()->with('errormsg', 'This email has attached to an other user & its approved by him');
//            }
//
//            $LoggedInUser = Auth::user();
//            $LoggedInUser->email_verified_at = null;
//            $LoggedInUser->email = $request->email;
//            $LoggedInUser->save();
//            \Illuminate\Support\Facades\Log::info('Users having id ' . Auth::user()->id . ' email has been changed so, he need to verify an email', [
//                'previous_email' => $request->user()->email,
//                'request_eamil' => $request->email,
//            ]);
//        }

        return redirect("/dashboard/accountdetails")->with('successmsg', 'User updated Successfully');
    }
    public function updateprofilePhone(EditPhoneRequest $request)
    {
        $phoneNoAr = is_valid_phone_no($request->phone_no, $request->get('country_coe', null));

      //  dd($phoneNoAr);
        if (empty($phoneNoAr['is_valid']) || $phoneNoAr['is_valid'] === false) {
            return back()->with('errormsg', "$request->phone_no is not a valid phone number");
        } else {
            $phoneNo = empty($phoneNoAr['formatted_phone_no']) ? null : $phoneNoAr['formatted_phone_no'];
        }


        $user = Auth::user();
        $user->phone_no = $phoneNo;
        $user->save();

        users::updatePhoneNoOnCallCenterService($user->id,$phoneNo);
//        dd([$phoneNoAr,$phoneNo,$user->phone_no]);





        return redirect("/dashboard/accountdetailsPhone")->with('successmsg', 'User updated Successfully');
    }
    public function LoginFromAdmin(Request $request){
//        "adminId" => "1"
//      "token" => "1|1614153444|84134"
//      "password" => "$2y$10$YdU9KA5Qr2MMYdc.yLhj3.JrFMe8m1lJ.7X8ej9hojKn7Flh/UszS"
//      "userid" => "2"

        $user_id = $request->userid;
        $final_token_request  = $request->token;


        $user = User::where('id',$user_id)->first();
        $user_token = $user['access_token'];
        if($user_token){
            $pieces_request = explode("|", $final_token_request);
            $request_admin_id = $pieces_request[0];
            $request_time_hash = $pieces_request[1];
            $request_random = $pieces_request[2];

            $pieces = explode("|", $user_token);
            $admin_id_db = $pieces[0];
            $old_timestamp = $pieces[1];
            $random_no = $pieces[2];

            $timestamp = Carbon::now()->getTimestamp()  ;
            $time_diff = $timestamp - $old_timestamp;

            if($request_admin_id == $admin_id_db && $request_time_hash == $old_timestamp && $request_random == $random_no){
                if($time_diff < 200){
                    Auth::LoginUsingId($user_id);
                    $user->access_token = null;
                    \Illuminate\Support\Facades\Session::put('admin-login',1);
                    $user->save();
                    return Redirect::route('dashboard');

                }else{
                    $user->access_token = null;
                    $user->save();
                    return Redirect::to('http://admin.b4ugloabl.com');
                }
            }else{
                $user->access_token = null;
                $user->save();
                return Redirect::to('http://admin.b4ugloabl.com');
            }
        }else{
            $user->access_token = null;
            $user->save();
            return Redirect::to('http://admin.b4ugloabl.com');
        }
    }

    /**
     * Return bank details form
     *
     * @param Request $request
     */
    public function banksDetails(Request $request)
    {
        $country = $request['country'];
        if (isset($request['country'])) {
            $banksAll = Banks::where('country_name', 'LIKE', $country)->orderBy('bank_name')->get();
            if (isset($banksAll)) {
                echo json_encode($banksAll);
            } else {
                $banksAll = "Bank Not Found";
                echo $banksAll;
            }
        } else {
            echo "Invalid Request";
        }
    }

    /**
     * Return add change password form
     *
     * @param Request $request
     *
     * @return Factory|View
     */
    public function changepassword()
    {
        return view('changepassword')->with(array('title' => 'Change Password'));
    }

    /**
     * Update Password
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function updatepass(UpdatePasswordRequest $request)
    {
        if (!password_verify($request['old_password'], Auth::user()->password)) {
            return redirect()->back()->with('errormsg', 'Incorrect Old Password');
        }
        users::findOrUpdate(Auth::user()->id, ['password' => bcrypt($request['password'])]);
        return redirect()->back()->with('message', 'Password Updated Sucessful');
    }

    /** @return Factory|View
     */
    public function referuser()
    {
        return view('referuser')->with(
            array(
                'title' => 'Refer user',
            )
        );
    }

    /**
     * Daily user logs for user
     *
     * @return Factory|View
     */

    /**
     *
     */
    public function viewibanimage()
    {
        $image = "images/IBAN.jpg";

        $details = '<p style="text-align:center;"><small>An IBAN is a unique number that is generated for each and every account held with HBL. The IBAN for Pakistan will be 24 digits in length and will contain the following information; Country Code, Security Digits, Bank Code followed by your current Bank Account Number </small><br><small style="color:red;text-align:left !important;">Note: Please add characters without spaces or dashes.</small><br><img width="80%" src="' . asset($image) . '"> </p> <br/>';

        echo $details;

        exit;
    }

    /**
     * @return Factory|View
     */
    public function getKyc()
    {
        $user = Auth::user();
        $kyc = $user->kyc ?? new Kyc();
        $user_kyc_relations = DB::table('user_kyc_relations')->get();
        $view = 'edit';
        return view('user.kyc', compact('user', 'kyc','user_kyc_relations' ,'view'));
    }

    /**
     * @param UpdateUserKycRequest $request
     *
     * @return RedirectResponse
     */
    public function postKyc(UpdateUserKycRequest $request)
    {
        $user = Auth::user();
        $data = $request->getValidRequest();
//        dd($data);
        if ($user->kyc()->count() > 0) {
            $user->kyc()->update($data);
        } else {
            $data['status'] = -1;
            $user->kyc()->create($data);
        }
        return redirect()->back()->with('successmsg', 'KYC information has been updated successfully');
    }

    /**
     * Render user KYC page
     *
     * @param  $user
     *
     * @return Factory|View
     */
    public function showKyc()
    {
        $user = Auth::user();
        $kyc = $user->kyc ?? new Kyc();
        $view = 'show';
        return view('user.kyc', compact('user', 'kyc', 'view'));
    }

    /**
     * @Purpose Show Account Blocked page, if account is not blocked then redirect user to dashboard either redirect user to Blocked Page
     * */
    public function accountBlock()
    {
        $isAccountBlocked = User::isAccountBlocked();
        if ($isAccountBlocked) {
            return view('account_blocked');
        } else {
            return redirect(url('dashboard'));
        }
    }

}
