<?php

namespace App\Http\Controllers;

use App\Currencies;
use App\currency_rates;
use App\Http\Requests\Deposits\ViewDepositDetailsRequest;
use App\withdrawals;
use App\deposits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function __construct()
    {
        // $this->settings = \App\settings::whereId(1)->first();
    }

    // get user withdrawls on dashboard
    public function getWithDrawals()
    {
        $wAmount = 0;
        $currenciesAll = Currencies::where('status', 'Active')->get();
        if (isset($currenciesAll)) {
            foreach ($currenciesAll as $curr) {
                $curruncy = $curr->code;
                $withdrawAmount = withdrawals::where('user', Auth::user()->id)
                    ->where('currency', $curruncy)
                    ->where('status', 'Approved')
                    ->value(DB::raw('SUM(amount + donation)'));

                if ('USD' == $curruncy) {
                    $wAmount = $wAmount + $withdrawAmount;
                } else {
                    $curr_small = $curr->small_code;
                    $rateval = 'rate_' . $curr_small;
                    $erates = currency_rates::select($rateval)
                        ->orderBy('id', 'desc')
                        ->first();
                    $exrate = $erates->$rateval;
                    $wAmount = $wAmount + ($withdrawAmount * $exrate);
                }
            }
        }

        return site_settings()->currency .
            number_format($wAmount, 2) .
            '<i class="fa fa-arrow-up" style="color:green; font-size:12px;"></i>';
    }

    // get total bonus for the dashboard
    public function getTotalBonus()
    {
        $totalInvestmentBonus = 0;
        $totalInvestmentBonus = DB::select(
            'CALL calculateOverallBonus("' . Auth::user()->u_id . '",@payout)'
        )[0]->total_investment;

        return site_settings()->currency .
            $totalInvestmentBonus .
            '<i class="fa fa-arrow-up" style="color:green; font-size:12px;"></i>';
    }

    ////get details of deposit using id
}
