<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\LoginRequest;
use App\settings;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Model\Referral;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    protected $username;

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
        $this->redirectTo = url()->previous();
    }

    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'u_id';
        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    public function showLoginForm(Request $request)
    {
        $request->session()->put('back_to_admin', false);
        if ($request->session()->has('reged')) {
            $msg = 'Registration successful!';
        } else {
            $msg = '';
        }

        return view('auth.login')
            ->with(
                [
                    'rmessage' => $msg,
                    'title' => 'Login',
                    'settings' => settings::getSettings(),
                ]
            );
    }

    public function loginForm(Request $request)
    {
        $request->session()->put('back_to_admin', false);
        if ($request->session()->has('reged')) {
            $msg = 'Registration successfull!';
        } else {
            $msg = '';
        }

        return view('frontend.login.index')
            ->with(
                [
                    'rmessage' => $msg,
                    'title' => 'Login',
                    'settings' => settings::getSettings(),
                ]
            );
    }

    public function username()
    {
        //   session(['back_to_admin'=>false]);
        return $this->username;
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'));
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages(
            [
                $this->username() => [trans('Invalid Username / Password / Account not approved.')],
                'g-recaptcha-response.required' => ['Invalid Google Captcha'],
            ]
        );
    }

    protected function authenticated(Request $request, $user)
    {

        if (env('is_allowed_for_long_ip', '0') <> 1) {
            $isAllowed = settings::isPanelAllowedForAdmins($request->getHttpHost());
            if (!$isAllowed) {
                return redirect(url('login'));
            }
        }

        if ('blocked' == $user->status) {
            auth()->logout();
            return back()->with('warning', 'Your account has been set to Defaulter. Please contact to our support team.');
        } elseif ('defaulter' == $user->status || 'Defaulter' == $user->status) {
            auth()->logout();
            return back()->with('warning', 'Your account has been set to Defaulter. Please contact to our support team.');
        } elseif ('inactive' == $user->status) {
            $user->status = 'active';
            $user->save();

//            $userid = \Auth::user()->id;
            // Session::set('variableName', $value);
//            auth()->logout();
//            $status = "Activate your account using code that you have recived in your contact no OR Check your email for verification code.";
//            return redirect('verifysms')->with(array('status' => $status, 'userid' => $userid));
            return redirect()->route('dashboard');
        } elseif ("active" == $user->status || "wrong_attempt" == $user->status ) {
            $user_uid = Auth::user()->u_id;
            if ('B4U0001' != $user_uid) {
                $user_uid = Auth::user()->u_id;
                $ip = $this->getUserIP();
                $location = $this->ip_info($ip, 'Country');
                $this->loginhistory($user_uid, $ip, $location);
            }
            if (Auth::user()->is_call_sub == 1) {
                $this->checkCallPackageSubscriptionLimit();
            }
            return redirect()->route('dashboard');
        }
        //return redirect()->intended($this->redirectPath());
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(LoginRequest $request)
    {

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $request->request->set('remember', false);
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


}
