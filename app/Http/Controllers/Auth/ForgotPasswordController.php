<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\SendResetLinkEmailRequest;
use App\settings;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\User;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
//        $settings = settings::getSettings();
    }

    public function sendResetLinkEmail(SendResetLinkEmailRequest $request)
    {
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        try {
            return Password::RESET_LINK_SENT == $response
                ? $this->sendResetLinkResponse($request, $response)
                : $this->sendResetLinkFailedResponse($request, $response);
        } catch (ValidationException $e) {
            return $e->getMessage();
        }
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return bool
     */
    protected function update(array $data)
    {
        return User::update(
            [
                'password' => bcrypt($data['password']),
            ]
        );

        /**
         * @deprecated 28-11-2020
         * */
//        $email = @trim(stripslashes($data['email']));
//        $password = @trim(stripslashes($data['password']));
//        $subject = 'Changed password for B4U Global';
//        $message = "Dear value customer, Your change password request is completed successfully.\n\n Your new password is " . $password . "\n\n
//				please Click on <a href=''>B4U Global Login</a> to Login.\n\n
//				Thanks For using B4U Global.
//				";
//        $email_to = $email; //replace with your email
//        $email_from = $this->mail_from_address;
//        $from_Name = $this->mail_from_name . ' Password Change';
//
//        //  $success = @mail($email_to, $subject, $message, 'From: <'.$from_Name.'>');
//        Mail::to($email_to)->send(new ForgotPassword($userName, $userUID, $userid, $password));

    }
}
