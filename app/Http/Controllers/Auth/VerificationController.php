<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResendEmailVerificationRequest;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Carbon\Traits\Week;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
//        $this->middleware('throttle:10,1')->only('verify', 'resend');
    }

    /**
     * Resend the email verification notification.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function resend(ResendEmailVerificationRequest $request)
    {
        try {

            if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $request->user_email)) {
                return back()->with('errormsg', $request->user_email . ' is not valid Email Address');
            }

            ## retry code in 2 mints.
            $LoggedInUser = Auth::user();

            if (!empty($LoggedInUser->last_email_send_at) && Carbon::parse($LoggedInUser->last_email_send_at)->diffInMilliseconds() < 110000) {
                return $request->wantsJson()
                    ? new JsonResponse([], 202)
                    : back()->with('resent', true);
            }

            ## validate email address
            if (!filter_var($request->user_email, FILTER_VALIDATE_EMAIL)) {
                return back()->with('errormsg', "$request->user_email is not a valid email address");
            }

            ## before going forward, please check, Is request email is not attached to other and verified by him. I tried to do with the Request Validation file, But didn't proper answer, so, in emergency case i m writing query here.
            $isEmailApproveOrAttachedWithOtherUser = User::checkIsEmailVerifiedAndAttachedToOtherUser($request->user_email);
            if ($isEmailApproveOrAttachedWithOtherUser) {
                return back()->with('errormsg', 'This email has attached to an other user & its approved by him');
            }

            ## save this email to user's current email address.

            $LoggedInUser->email = $request->user_email;
            $LoggedInUser->last_email_send_at = Carbon::now();
            $LoggedInUser->save();

            if ($request->user()->hasVerifiedEmail()) {
                return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect($this->redirectPath());
            }
            $request->user()->sendEmailVerificationNotification();
            return $request->wantsJson()
                ? new JsonResponse([], 202)
                : back()->with('resent', true);

        } catch (\Exception $exception) {
            return $request->wantsJson() ? new JsonResponse(['msg' => $exception->getMessage()], 500) : back()->with('resent', true);
        }

    }


    /**
     * Show the email verification notice.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        return $request->user()->hasVerifiedEmail()
            ? redirect($this->redirectPath())
            : view('auth.verify');
    }
}
