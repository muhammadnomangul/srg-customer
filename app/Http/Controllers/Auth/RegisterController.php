<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Users\UserB4UIdRequest;
use App\Http\Requests\Auth\ResendSMSRequest;
use App\Model\Referral;
use App\User;
use App\users;
use App\VerifyUser;
use App\UserAccounts;
use App\settings;
use App\Http\Controllers\Controller;
use App\WebHook\php;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log as LogAlias;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

//use App\Mail\DemoEmail;
use App\Mail\NewUserRegistrations;
use App\Mail\NewPartnerRegistrations;

//use Mail as Email;
use Illuminate\Support\Str;
use Twilio\Rest\Client as TwilioClient;
use Illuminate\Validation\Rule;
use App\OTPToken;
use App\Http\Controllers\PinController;
use Auth;
use DB;
use Exception;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';


    protected $user_password;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        // GET EMAIL TEMPLATE DATA SENT IN EMAILS
        /*
//        function getEmailTemplate($event)
//        {
//            $emailDetails = email_templates::where('title', 'Like', $event)->first();
//
//            return $emailDetails;
//        }

        $twilioAccountSid   = getenv("TWILIO_SID");
        $twilioAuthToken    = getenv("TWILIO_TOKEN");
        $twilioSMSNumber    = getenv("TWILIO_NUMBER");

        $this->twilioClient = new TwilioClient($twilioAccountSid, $twilioAuthToken);

        */
    }

    public function showRegistrationForm()
    {
        function ip_info($ip = null, $purpose = "location", $deep_detect = true)
        {
            $output = null;
            if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
                $ip = $_SERVER["REMOTE_ADDR"];
                if ($deep_detect) {
                    if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    }
                    if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                    }
                }
            }
            $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), null, strtolower(trim($purpose)));
            $support = array("country", "countrycode", "state", "region", "city", "location", "address");
            $continents = array(
                "AF" => "Africa",
                "AN" => "Antarctica",
                "AS" => "Asia",
                "EU" => "Europe",
                "OC" => "Australia (Oceania)",
                "NA" => "North America",
                "SA" => "South America"
            );
            if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
                $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
                if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                    switch ($purpose) {
                        case "location":
                            $output = array(
                                "city" => @$ipdat->geoplugin_city,
                                "state" => @$ipdat->geoplugin_regionName,
                                "country" => @$ipdat->geoplugin_countryName,
                                "country_code" => @$ipdat->geoplugin_countryCode,
                                "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                                "continent_code" => @$ipdat->geoplugin_continentCode
                            );
                            break;
                        case "address":
                            $address = array($ipdat->geoplugin_countryName);
                            if (@strlen($ipdat->geoplugin_regionName) >= 1) {
                                $address[] = $ipdat->geoplugin_regionName;
                            }
                            if (@strlen($ipdat->geoplugin_city) >= 1) {
                                $address[] = $ipdat->geoplugin_city;
                            }
                            $output = implode(", ", array_reverse($address));
                            break;
                        case "city":
                            $output = @$ipdat->geoplugin_city;
                            break;
                        case "state":
                            $output = @$ipdat->geoplugin_regionName;
                            break;
                        case "region":
                            $output = @$ipdat->geoplugin_regionName;
                            break;
                        case "country":
                            $output = @$ipdat->geoplugin_countryName;
                            break;
                        case "countrycode":
                            $output = @$ipdat->geoplugin_countryCode;
                            break;
                    }
                }
            }
            return $output;
        }

        function getUserIP()
        {
            $client = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote = $_SERVER['REMOTE_ADDR'];

            if (filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            } else {
                $ip = $remote;
            }

            return $ip;
        }

        $user_ip = getUserIP();

        $user_country = ip_info("Visitor", "Country");
        $user_countrycode = ip_info("Visitor", "countrycode");
        $static_country = ip_info("197.210.45.67", "Country");

        return view('auth.register')
            ->with(
                array(
                    'user_country' => $user_country,
                    'user_countrycode' => $user_countrycode,
                    'static_country' => $static_country,
                    'title' => 'Register',
                    'settings' => settings::getSettings(),
                )
            );
    }

    public function registerForm()
    {
        function ip_info($ip = null, $purpose = "location", $deep_detect = true)
        {
            $output = null;
            if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
                $ip = $_SERVER["REMOTE_ADDR"];
                if ($deep_detect) {
                    if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    }
                    if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                    }
                }
            }
            $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), null, strtolower(trim($purpose)));
            $support = array("country", "countrycode", "state", "region", "city", "location", "address");
            $continents = array(
                "AF" => "Africa",
                "AN" => "Antarctica",
                "AS" => "Asia",
                "EU" => "Europe",
                "OC" => "Australia (Oceania)",
                "NA" => "North America",
                "SA" => "South America"
            );
            if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
                $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
                if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                    switch ($purpose) {
                        case "location":
                            $output = array(
                                "city" => @$ipdat->geoplugin_city,
                                "state" => @$ipdat->geoplugin_regionName,
                                "country" => @$ipdat->geoplugin_countryName,
                                "country_code" => @$ipdat->geoplugin_countryCode,
                                "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                                "continent_code" => @$ipdat->geoplugin_continentCode
                            );
                            break;
                        case "address":
                            $address = array($ipdat->geoplugin_countryName);
                            if (@strlen($ipdat->geoplugin_regionName) >= 1) {
                                $address[] = $ipdat->geoplugin_regionName;
                            }
                            if (@strlen($ipdat->geoplugin_city) >= 1) {
                                $address[] = $ipdat->geoplugin_city;
                            }
                            $output = implode(", ", array_reverse($address));
                            break;
                        case "city":
                            $output = @$ipdat->geoplugin_city;
                            break;
                        case "state":
                            $output = @$ipdat->geoplugin_regionName;
                            break;
                        case "region":
                            $output = @$ipdat->geoplugin_regionName;
                            break;
                        case "country":
                            $output = @$ipdat->geoplugin_countryName;
                            break;
                        case "countrycode":
                            $output = @$ipdat->geoplugin_countryCode;
                            break;
                    }
                }
            }
            return $output;
        }

        function getUserIP()
        {
            $client = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote = $_SERVER['REMOTE_ADDR'];

            if (filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            } else {
                $ip = $remote;
            }

            return $ip;
        }

        $user_ip = getUserIP();

        $user_country = ip_info("Visitor", "Country");
        $user_countrycode = ip_info("Visitor", "countrycode");
        $static_country = ip_info("197.210.45.67", "Country");

        return view('frontend.register.index')
            ->with(
                array(
                    'user_country' => $user_country,
                    'user_countrycode' => $user_countrycode,
                    'static_country' => $static_country,
                    'title' => 'Register',
                    'settings' => settings::getSettings(),
                )
            );
    }


    /*
    protected function redirectTo()
    {
        return '/login';
    }
    */


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['phone_no'] = str_replace(" ", "", trim($data['phone_no']));
        // dd($data);
        return Validator::make(
            $data,
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone_no' => 'required',
                'password' => 'required|string|min:6|confirmed',
                'sign' => 'required',
                'g-recaptcha-response' => 'required|captcha',
                'parent_id' => 'required',
                'country' => 'required',
            ],
            [
                'g-recaptcha-response.required' => 'Invalid Google Captcha',
            ]
        );
    }


    /**
     * Handle a registration request for the application.
     *
     * @param Requests\Auth\RegisterRequest $request
     * @return RedirectResponse|Response
     */
    public function register(Requests\Auth\RegisterRequest $request)
    {

        try {

            ## validate email address
            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                return back()->with('errormsg', "$request->email is not a valid email address");
            }

            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);

            if ($this->registered($request, $user) && $user instanceof User) {
                $user->sendEmailVerificationNotification();
                ## create  cash box account for all users..
                $user->assignCashBoxAccountOfAllCurrencies();
                return redirect('dashboard');
            } else {
                return redirect($this->redirectPath());
            }
        } catch (\Exception $e) {
            LogAlias::info("Register user exception.  " . $e->getMessage() . "" . $e->getline() . "" . $e->getFile());
            return redirect($this->redirectPath());
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        // dd($data);
        $contactNo = str_replace(" ", "", trim($data['phone_no']));
        //echo $contactNo;
        //exit;
        $randomNo = rand(1000, 9999);
        // Get the last created order
        $lastUser = User::orderBy('id', 'desc')->first();
        if (!$lastUser) {    // We get here if there is no user at all
            // If there is no number set it to 0, which will be 1 at the end.

            $unique_id = "B4U0001";
            $number = 0;
        } else {
            $number = substr($lastUser->u_id, 3);
            $number = intval($number) + 1;
            $unique_id = "B4U00" . $number;
            // If we have B4U0001 in the database then we only want the number
            // So the substr returns this 0001
        }
    if($unique_id == 'B4U00499999'){
        $unique_id = "B4U00500001";
    }

        if (isset($data['parent_id'])) {
            $parentId = strtoupper($data['parent_id']);
            $parent_Id = trim($parentId);

            $parentInfo1 = users::where('u_id', '=', $parentId)->first();
            if (isset($parentInfo1)) {
                $parent_id = $parentId;

                if ($parent_id == $unique_id) {
                    $parent_id = "B4U0001";
                }
            } else {
                $parent_id = "B4U0001";
            }
        } else {
            $parent_id = "B4U0001";
        }
        $settings = settings::getSettings();

        $name = @trim(stripslashes($data['name']));
        $password = @trim(stripslashes($data['password']));
        $email_to = @trim(stripslashes($data['email']));

        $this->user_password = $password;

        $isValidPhoneNo = is_valid_phone_no($contactNo, empty($data['country_coe']) ? null : $data['country_coe']);
        if (empty($isValidPhoneNo['is_valid']) || $isValidPhoneNo['is_valid'] === false) {
            $contactNo = null;
        } else {
            $contactNo = empty($isValidPhoneNo['formatted_phone_no']) ? null : $isValidPhoneNo['formatted_phone_no'];
        }

        // create new user
        $user = new User();
        $user->name = trim($data['name']);
        $user->email = NULL;
        $user->email = trim($data['email']);
        $user->phone_no = $contactNo;
        $user->password = trim(bcrypt($data['password']));
        $user->Country = trim($data['country']);
        $user->plan = 1;
        $user->status = 'active';
        $user->u_id = $unique_id;
        $user->parent_id = $parent_id;
        $user->sms_varify_code = $randomNo;
        $user->last_email_send_at = Carbon::now();
        $user->ref_link = $settings->site_address . '/ref/' . $unique_id;
        $user->save();

        users::InsertUserToCallCenterService($user->id,$user->u_id,$user->phone_no);

        if (isset($user->id)) {
            $verifyUser = new VerifyUser();
            $verifyUser->user_id = $user->id;
            $verifyUser->token = Str::random(40);
            $verifyUser->save();
            $userAcc = new UserAccounts();
            $userAcc->user_id = $user->id;
            $userAcc->save();
        }
        // // Send Activations Code Through SMS
        // $this->sendSMS($unique_id,$contactNo,$randomNo);
        return $user;
    }


    // Register and redirct User
    protected function registered(Request $request, $user)
    {
        try {
            if ($user->parent_id == "B4U0001") {
                \Illuminate\Support\Facades\DB::select('CALL  generate_referral_tree(' . $user->id . ',0,1)');
            } else {
                \Illuminate\Support\Facades\DB::select('CALL  generate_referral_tree(' . $user->id . ',0,Null)');
            }
        } catch (\Exception $ex) {
            LogAlias::error("ERROR on calling generate_referral_tree for user(" . $user->id . ")   " . $ex->getMessage());
        }

        return true;
    }

    // verify User Throgh EMail Link
    public function verifyUser(Requests\Users\UserVerificationTokenRequest $userVerificationToken)
    {
        $verifyUser = VerifyUser::where('token', $userVerificationToken->token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if ($user->status != "active") {
                $user->status = "active";
                $user->save();

                $status = "Your e-mail is verified. You can now login.";
            } elseif ($user->status == "active") {
                $status = "Your e-mail is already verified. You can now login.";
            } else {
                return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
            }
        } else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/login')->with('status', $status);
    }


    // Send Emails

    public function sendEMailtoParent($parentName, $parentEmail, $parentID, $unique_id, $newUserEmail)
    {
        // Call of Refferal Functions
        \App\Model\Referral::sync($parentID);

        if ($parentEmail != '') {
            $name = @trim(stripslashes($parentName));
            $email_to = @trim(stripslashes($parentEmail));
            $subject = "New User Registered At Your Downline Using B4U Global.";
            $email_from = $this->mail_from_address;
            $from_Name = $this->mail_from_name;

            $message =
                "<html>
						<body align=\"left\" style=\"height: 100%;\">
                        <div>
							<div>
								<table style=\"width: 100%;\">
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											Congratulations! Dear " . $name . ",
										</td>
									</tr>
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											A new user has registered using your referral ID (as your partner), His/Her ID is : '" . $unique_id . "' and email id is: " . $newUserEmail . ".
											You can contact his/her. You will recieve referral bonus from his/her Investment. Good job carry on.
										</td>
									</tr>
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											Thanks for using  " . $from_Name . ".
										</td>
									</tr>
									<tr>
										<td style=\"padding:10px 0; text-align:left;\">
											Your Sincerely,
										</td>
									</tr>
									<tr>
										<td style=\"padding:10px 0; text-align:left;\">
											Team " . $from_Name . "
										</td>
									</tr>
									
										
								</table>
							</div>
						</div>
					</body>
				</html>";
            //exit("Registered User ". $message);

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From: B4U New Partner Registration <' . $this->mail_from_address . '>' . "\r\n";
            //$headers .= 'Cc: myboss@example.com' . "\r\n";

            $success = @mail($email_to, $subject, $message, $headers);
        }
    }

    public function userRegistrationEmail($name, $email_to, $password, $smsCode, $unique_id, $parent_id)
    {
        $subject = "You have Registered On B4U Global successfully.";
        $email_from = $this->mail_from_address;
        $from_Name = $this->mail_from_name;
        //$path            = url('user/verify', $user->verifyUser->token);
        $path = "";
        $message = "<html>
                        <body align=\"left\" style=\"height: 100%;\">
                            <div>
								<div>
									<table style=\"width: 100%;\">
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Dear " . $name . ",
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Thank you for joining " . $from_Name . ".
											</td>
										</tr>";
        if ($unique_id != "" && $smsCode != "") {
            $message .= "<tr>
                                							<td style=\"text-align:left; padding:10px 0;\">
                                								 Your login id : <strong>" . $unique_id . "</strong> Your Varification Code : <strong>" . $smsCode . "</strong>	
                                							</td>
                                					</tr>";
        }

        if ($parent_id != "" && $password != "") {
            $message .= "<tr>
                                							<td style=\"text-align:left; padding:10px 0;\">
                                								Your password : <strong>" . $password . "</strong> and  Refferal id : <strong>" . $parent_id . "</strong>
                                							</td>
                                					</tr>
                                					<tr>
                                						<td style=\"text-align:left; padding:10px 0;\">
                                							Enter the Verification Pin Code on following link.
                                							<a href=\"https://b4uglobal.com/verifysms\">Enter Code</a>
                                						</td>
                                					</tr>";
        }
        if ($path != "") {
            $message .= "<tr>
                                							<td style=\"text-align:left; padding:10px 0;\">
                                												
                                								<form action='" . $path . "' method=\"get\">
                                										Please click on <button class=\"btn-primary\" type=\"submit\" formaction='" . $path . "'>Verify Email</button> to join us.
                                								</form>
                                							</td>
                                					</tr>";
        }

        $message .= "<tr>
                                							<td style=\"text-align:left; padding:10px 0;\">
                                								Thanks for using  " . $from_Name . ".
                                							</td>
                                					</tr>
                                					<tr>
                                							<td style=\"padding:10px 0; text-align:left;\">
                                								Your Sincerely,
                                							</td>
                                					</tr>
                                					<tr>
                                							<td style=\"padding:10px 0; text-align:left;\">
                                								Team " . $from_Name . "
                                							</td>
                                					</tr>
                                					<tr>
                                							<td style=\"padding:10px 0; text-align:left;\">
                                								<p style=\" color:red; \" > Disclaimer: Don't pay/recieve cash to/from anyone.
                                								B4U Global will not be responsible for any loss. Your membership in B4U Global is by your own will.</p>
                                							</td>
                                					</tr>					
                                    </table>
				                </div>
                            </div>
            			</body>
            		</html>";
        //exit("Registered User ". $message);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: B4U New Registrations <' . $this->mail_from_address . '>' . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";

        $success = @mail($email_to, $subject, $message, $headers);
        //    return $user;
    }


    public function validateParentId(UserB4UIdRequest $request)
    {
        $parentid = $request->input('b4uid');
        $isExists = \App\User::where('u_id', $parentid)->first();
        if ($isExists) {
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }
}
