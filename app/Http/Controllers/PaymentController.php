<?php

namespace App\Http\Controllers;

use App\CBCredits;
use App\Console\Commands\PlansCron;
use App\current_rate;
use App\Http\Requests\Currency\CurrencyConversion;
use App\Http\Requests\Deposits\ApproveDepositRequest;
use App\Http\Requests\Deposits\MakeDepositRequest;
use App\Http\Requests\Deposits\MakeDepositViewRequest;
use App\Http\Requests\Deposits\UpdateDepositStatus;
use App\Http\Requests\Deposits\ViewDepositDetailsRequest;
use App\Http\Requests\Misc\AmountLimitCheckRequest;
use App\Http\Requests\Solds\MakeSoldRequest;
use App\Http\Requests\Users\GetAccountInfoRequest;
use App\Model\Deposit;
use App\plans;
use App\settings;
use Illuminate\Support\Facades\DB;
use Log;
use Mail;
use Session;
use App\solds;
use App\users;
use App\Account;
use App\deposits;
use App\OTPToken;
use Carbon\Carbon;
use App\admin_logs;
use App\Currencies;
use App\UserAccounts;
use App\currency_rates;
use App\Model\Referral;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\daily_investment_bonus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        set_time_limit(0);
        $this->settings = settings::getSettings();
        // $this->middleware('auth');
        //saveLogs($title,$details,$userid,$curr,$amt,$pre_amt,$approvedby)
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|Response|\Illuminate\View\View
     */
    //payment route
    public function depositCheck(Request $request){
        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        $ratesQuery = current_rate::first();
        $currencytype = strtolower($request['currencytype']);
        $amount = $request['amount'];
        $depositMode = $request['depositMode'];
        $curr = strtolower($currencytype);
        $rateVal = 'rate_' . $curr;
        $currencyRate = $ratesQuery->$rateVal;
        $accProfit = 'profit_' . $curr;
        $userProfit = $userAccInfo->$accProfit;
        $accBalSold = 'sold_bal_' . $curr;
        $userbalanceSold = $userAccInfo->$accBalSold;
        $reference_bonus = $userAccInfo->reference_bonus;
        $totalUsd = $amount * $currencyRate;
//        echo 'amount '. $amount;
        if (isset($currencytype) && isset($ratesQuery)) {
            $curr = strtolower($currencytype);
            $rateVal = 'rate_' . $curr;
            $currencyRate = $ratesQuery->$rateVal;
            $accProfit = 'profit_' . $curr;
            $userProfit = $userAccInfo->$accProfit;
            $accBalSold = 'sold_bal_' . $curr;
            $userbalanceSold = $userAccInfo->$accBalSold;
            $reference_bonus = $userAccInfo->reference_bonus;
            $reinvest_type = $request['selectedType'];
            $totalUsd = $amount * $currencyRate;
            if($depositMode == 'new' && $currencytype != 'usd'){
                $amtval = !empty($request['amount']) ? $request['amount'] : '';
                if ($currencytype != 'usd') {
                    $rate = 'rate_' . $currencytype;
                    $convertedrate = ((double)$ratesQuery->$rate * (double)$amtval);
                    echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                }
            }
            if ($currencytype == 'USD' && $reinvest_type == 'Bonus') {
                if ($totalUsd < site_settings()->reinvest_limit || $amount > $reference_bonus) {
//                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . ' Or You have insufficient balance for this request!');
//                    echo '<strong style="color:green;">' . 1 . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
//                    echo '<strong style="color:red;">Amount is less than  . site_settings()->reinvest_limit . ' Or You have insufficient balance for this request</strong><input type="hidden" name="currconamt" class="currconamt">';
                    echo '<strong style="color:red;">Amount is less than'. site_settings()->reinvest_limit  . 'Or You have insufficient balance for this request!</strong><input type="hidden" name="currconamt" class="currconamt">';

                }else{
                    echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

                }
            } elseif ($currencytype != 'usd' && $reinvest_type == 'Bonus') {
                echo '<strong style="color:red;"> '.$currencytype.'selected currency not allowed for reinvest Bonus</strong><input type="hidden" name="currconamt" class="currconamt">';

//                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid request! selected currency not allowed for reinvest Bonus');
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';

            } elseif ($reinvest_type == 'Profit' && $totalUsd < site_settings()->reinvest_limit && $amount > $userProfit) {
//                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . '! Or You have insufficient balance for this request!');
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                echo '<strong style="color:red;">'  . $amount  ." ".$currencytype. ' ise Greater Then Your total Profit ('.$userProfit.') </strong><input type="hidden" name="currconamt" class="currconamt">';


            }elseif ($reinvest_type == 'Profit' && $totalUsd < site_settings()->reinvest_limit && $amount <= $userProfit) {
//                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . '! Or You have insufficient balance for this request!');
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                $ratesQuery = Cache::remember('lastCurrencyRate', settings::TopCacheTimeOut, function () {
                    return current_rate::orderby('created_at', 'DESC')->first();
                });

                $currencytype = strtolower($request['currencytype']);
                $amtval = !empty($request['amount']) ? $request['amount'] : '';
                if ($currencytype != 'usd') {
                    $rate = 'rate_' . $currencytype;
                    $convertedrate = ((double)$ratesQuery->$rate * (double)$amtval);
                    echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                } else {
                    echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

                }
                echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

            } elseif ($reinvest_type == 'Sold' && $totalUsd < site_settings()->reinvest_limit) {
                if($amount > $userbalanceSold){

                    echo '<strong style="color:red;">'  . $amount  ." ".$currencytype. ' is Greater Then Your total Sold ('.$userbalanceSold.') </strong><input type="hidden" name="currconamt" class="currconamt">';
                }

//                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . '! Or You have insufficient balance for this request!');
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
//                echo '<strong style="color:red;">' . site_settings()->reinvest_limit  . ' ' . $request['currencytype'] . ' is equal to  ' . $totalUsd . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
            }elseif ($reinvest_type == 'Sold' && ($totalUsd < site_settings()->reinvest_limit || $amount <= $userbalanceSold)) {
                $ratesQuery = Cache::remember('lastCurrencyRate', settings::TopCacheTimeOut, function () {
                    return current_rate::orderby('created_at', 'DESC')->first();
                });

                $currencytype = strtolower($request['currencytype']);
                $amtval = !empty($request['amount']) ? $request['amount'] : '';
                if ($currencytype != 'usd') {
                    $rate = 'rate_' . $currencytype;
                    $convertedrate = ((double)$ratesQuery->$rate * (double)$amtval);
                    echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                } else {
                    echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

                }
                echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

            }
        } else {
            echo '<strong style="color:red;"></strong><input type="hidden" name="currconamt" class="currconamt">';

//            return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid Currency or Rates');
        }
        exit();
    }
    public function currencyconversion(CurrencyConversion $request)
    {
        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        $ratesQuery = current_rate::first();
        $currencytype = strtolower($request['currencytype']);
        $amount = $request['amount'];
        $curr = strtolower($currencytype);
        $rateVal = 'rate_' . $curr;
        $currencyRate = $ratesQuery->$rateVal;
        $accProfit = 'profit_' . $curr;
        $userProfit = $userAccInfo->$accProfit;
        $accBalSold = 'sold_bal_' . $curr;
        $userbalanceSold = $userAccInfo->$accBalSold;
        $reference_bonus = $userAccInfo->reference_bonus;
        $ratesQuery = Cache::remember('lastCurrencyRate', settings::TopCacheTimeOut, function () {
            return current_rate::orderby('created_at', 'DESC')->first();
        });

        $currencytype = strtolower($request['currencytype']);
        $selectedType = $request['selectedType'];
        $amtval = !empty($request['amount']) ? $request['amount'] : '';
        $rate = 'rate_' . $currencytype;
        $convertedrate = ((double)$ratesQuery->$rate * (double)$amtval);

        if($currencytype != 'usd' && $selectedType == 'Bonus'){

            echo '<strong style="color:#ff0000;"> selected currency not allowed for reinvest Bonus</strong><br><input type="hidden" name="currconamt" class="currconamt">';

        }
        else {
            if($selectedType == 'Profit'){
                if($amount > $userProfit){
                    echo '<strong style="color:red;"> Amount is Greater then available Profit</strong><br><input type="hidden" name="currconamt" class="currconamt">';

                }else{
                    if($currencytype != 'usd'){

                        echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                    }

                }
            }
            if($selectedType == 'Sold'){
                if($amount >= $userbalanceSold){
                    echo '<strong style="color:red;"> Amount is Greater then available Sold Amount</strong><br><input type="hidden" name="currconamt" class="currconamt">';

                }else{
                    if($currencytype != 'usd') {
                        echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                    }
                }
            }
            if($selectedType == 'Bonus'){
                if($amount >= $reference_bonus){
                    echo '<strong style="color:red;"> Amount is Greater then available Bounus Amount</strong><br><input type="hidden" name="currconamt" class="currconamt">';

                }else{
                    if($currencytype != 'usd'){

                        echo '<strong style="color:green;">' . $amtval . ' ' . $request['currencytype'] . ' is equal to  ' . $convertedrate . ' USD dollars </strong><input type="hidden" name="currconamt" class="currconamt">';
                    }

                }
            }

        }
//        else {
//            echo '';
//        }

        exit();
    }
    public function getAccountInfoPost(GetAccountInfoRequest $request)
    {
        $currency = $request['curr'];
        $type = $request['type'];
        $donation_limit = 10;
        $settings = settings::getSettings();
        $ratesQuery = current_rate::first();
        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();


        $bonus_bal = $userAccInfo->reference_bonus;
        if (isset($currency)) {
            $curr = strtolower($currency);
            $rate = 'rate_' . $curr;
            $sold = 'sold_bal_' . $curr;
            $profit = 'profit_' . $curr;
        }


        $profit_amt = $userAccInfo->$profit;
        $sold_bal = $userAccInfo->$sold;

        if ($curr != 'usd') {
            $sold_bal_usd = $sold_bal * $ratesQuery->$rate;
            $profit_amt_usd = $profit_amt * $ratesQuery->$rate;
        } else {
            $sold_bal_usd = $sold_bal;
            $profit_amt_usd = $profit_amt;
        }
        if ($type == 'Bonus' && $bonus_bal >= site_settings()->withdraw_limit && $curr == 'usd') {
            echo $data = '<strong style="color:green;"> Your Available Bonus : $' . $bonus_bal . '</strong><input type="hidden" name="availablemt" value=' . $bonus_bal . ' id="availablemt">';
        } elseif ($type == 'Profit' && $profit_amt_usd >= site_settings()->withdraw_limit) {
            if ($curr != 'usd') {
                echo $data =
                    '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . '). Total in USD ' . $profit_amt_usd . '.</strong><input type="hidden" name="availablemt" value=' . $profit_amt . ' id="availablemt">';
            } else {
                echo $data =
                    '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . ').</strong><input type="hidden" name="availablemt" value=' . $profit_amt . ' id="availablemt">';
            }
        } elseif ($type == 'Sold' && $sold_bal_usd >= site_settings()->withdraw_limit) {
            if ($curr != 'usd') {
                echo $data =
                    '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . '). Total in USD ' . $sold_bal_usd . '</strong><input type="hidden" name="availablemt" value=' . $sold_bal . ' id="availablemt">';
            } else {
                echo $data =
                    '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . ').</strong><input type="hidden" name="availablemt" value=' . $sold_bal . ' id="availablemt">';
            }
        } else {
            echo $data =
                '<strong style="color:red;">Your Available Amount is less than $' . site_settings()->withdraw_limit . '<input type="hidden" name="error" value="1" id="error"></strong>';
        }
        exit;
    }




    public function payment(Request $request)
    {
        // redirect user to new deposite create screen if users country is not selected
        if (Auth::user() && !Auth::user()->Country) {
            return redirect(url('dashboard/deposits'));
        }


        $amount = $request->session()->get('amount');
        $payment_mode = $request->session()->get('payment_mode');
        $plan_id = $request->session()->get('plan_id') ? $request->session()->get('plan_id') : Auth::user()->plan;
        $deposit_mode = $request->session()->get('deposit_mode');
        $currency = $request->session()->get('currency');
        if ($request->session()->get('reinvest_type')) {
            $reinvest_type = $request->session()->get('reinvest_type');
        } else {
            $reinvest_type = '';
        }

        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        //   $ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();
        $activeDeposits = deposits::where('user_id', Auth::user()->id)->where('status', 'Approved')->get();
        $totalActiveDeposits = count($activeDeposits);

        $prevProfitReinvest =
            deposits::where('user_id', Auth::user()->id)->where('reinvest_type', 'Profit')->where('currency', $currency)->orderBy('created_at', 'DESC')->first();

        if (isset($prevProfitReinvest) && isset($prevProfitReinvest->created_at)) {
            $createdAt = $prevProfitReinvest->created_at;
            $dateDifferece = $this->calculateDate($createdAt);
        } else {
            $dateDifferece = 1;
        }

        if ($deposit_mode == 'reinvest') {
            if (isset($currency) && isset($ratesQuery)) {
                $curr = strtolower($currency);
                $rateVal = 'rate_' . $curr;
                $currencyRate = $ratesQuery->$rateVal;
                $accProfit = 'profit_' . $curr;
                $userProfit = $userAccInfo->$accProfit;
                $accBalSold = 'sold_bal_' . $curr;
                $userbalanceSold = $userAccInfo->$accBalSold;
                $reference_bonus = $userAccInfo->reference_bonus;

                $totalUsd = $amount * $currencyRate;
                if ($currency == 'USD' && $reinvest_type == 'Bonus') {
                    if ($totalUsd < site_settings()->reinvest_limit || $amount > $reference_bonus) {
                        return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . ' Or You have insufficient balance for this request!');
                    } elseif ($totalActiveDeposits == 0) {
                        return redirect()->intended('dashboard/deposits')->with('errormsg', 'Bonus Reinvestment Not Allowed ! You have no approved deposits in your deposits account.');
                    }
                } elseif ($currency != 'USD' && $reinvest_type == 'Bonus') {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid request! selected currency not allowed for reinvest Bonus');
                } elseif ($reinvest_type == 'Profit' && ($totalUsd < site_settings()->reinvest_limit || $amount > $userProfit)) {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . '! Or You have insufficient balance for this request!');
                } elseif ($reinvest_type == 'Profit' && $dateDifferece != 1) {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Profit Reinvest Not Allowed ! You can create only 1 Profit Reinvest against a Currency within 1 Month.');
                } elseif ($reinvest_type == 'Sold' && ($totalUsd < site_settings()->reinvest_limit || $amount > $userbalanceSold)) {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . site_settings()->reinvest_limit . '! Or You have insufficient balance for this request!');
                }
            } else {
                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid Currency or Rates');
            }
        }
        $title = 'Make deposit';

        return view('payment', ['currencies' => Currencies::getCurrencies(), 'amount' => $amount, 'currency' => $currency, 'payment_mode' => $payment_mode, 'reinvest_type' => $reinvest_type, 'deposit_mode' => $deposit_mode, 'plan_id' => $plan_id, 'title' => $title]);
    }





    public function calculateDate($created_at)
    {
        /*
            $carbonCreatedAt = \Carbon\Carbon::parse($created_at);
            $carbonNow = \Carbon\Carbon::now();
            if($carbonNow->greaterThan($carbonCreatedAt) && $carbonNow->diffInMonths($created_at) > 1){
                return 1;
            }else{
                return 0;
            }
        */
        // Update lastest Profit for users
        $todayDate = date('Y-m-d');
        $creationDate = date('Y-m-d', strtotime($created_at));
        $date1MonthAfterCreate =
            date('Y-m-d', strtotime($creationDate . '+1 Month')); //Date After 1 month of Approved
        $date1 = date_create($creationDate);
        $date = date_create($todayDate);

        // Calculate Dates Differance1 in days
        $diff = date_diff($date1, $date);
        $diff_in_days = $diff->days;
        $diff_in_days2 = $diff->format('%R%a Days');
        $dateDifferance = date('Y-m-d', strtotime($created_at . $diff_in_days2));

        if ($dateDifferance >= $date1MonthAfterCreate) {
            return 1;
        } else {
            return 0;
        }
    }

    public function amountLimitCheck(AmountLimitCheckRequest $request)
    {
        //Investment amount info
        $amount = $request['amount'];
        $curr = $request['curr'];
        //  $ratesQuery = curency_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();
        if ($curr != 'USD' || $curr != 'usd') {
            $currency = strtolower($curr);
            $rateVal = 'rate_' . $currency;
            $rate = $ratesQuery->$rateVal;
            $totalUsd = $amount * $rate;
        } else {
            $totalUsd = $amount;
        }
        echo $totalUsd;
        exit;
    }

    public function deposit(MakeDepositViewRequest $request)
    {

        ## block deposit below then 710$
        $loggedInUser = Auth::user();
        if (strtolower($loggedInUser->Country) == 'pakistan' && strtolower($request->currency) == 'usd' && $request->amount < 710 && $request->deposit_mode == 'new') {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'Pakistani user can only deposit more than $710');
        }


        if ($request['deposit_mode'] == 'new') {
            \Illuminate\Support\Facades\Session::forget('re-invest');
        }

        //store payment info in session
        $request->session()->put('amount', $request['amount']);
        $request->session()->put('payment_mode', $request['payment_mode']);
        $request->session()->put('deposit_mode', $request['deposit_mode']);
        $request->session()->put('currency', $request['currency']);
        if (isset($request['reinvest_type'])) {
            $request->session()->put('reinvest_type', $request['reinvest_type']);
        }
        if (isset($request['pay_type'])) {
            $request->session()->put('pay_type', $request['pay_type']);
            $request->session()->put('plan_id', $request['plan_id']);
        }
        $title = 'Make deposit';
        $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();

        // $ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();

        return redirect()->route('payment', ['title' => $title, 'accountsInfo' => $userAccInfo, 'ratesQuery' => $ratesQuery]);
    }

    ////////////////
    public function deposits_json()
    {
        $deposits = deposits::where('user_id', Auth::user()->id)
            ->where('status', '!=', 'Cancelled')
            ->where('status', '!=', 'Deleted')
            ->orderby('created_at', 'DESC');

        return datatables()->of($deposits)->toJson();
    }

    //Return deposit route for customers
    public function deposits()
    {
        $deposits = deposits::where('user_id', Auth::user()->id)
            ->where('status', '!=', 'Cancelled')
            ->where('status', '!=', 'Deleted')
            ->orderby('created_at', 'DESC')->get();
        $title = 'Deposits';
        $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();
        // $ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();
        $cashBoxAccounts = Auth::user()->cashBoxAccounts()->get();
        return view('deposits', ['title' => $title, 'deposits' => $deposits, 'accountsInfo' => $userAccInfo, 'ratesQuery' => $ratesQuery, 'currencies' => Currencies::getCurrencies(), 'cashBoxAccounts' => $cashBoxAccounts]);
    }

    //Save deposit requests
    public function savedeposit(MakeDepositRequest $request)
    {
//        dd('ee');
        if (env('APP_DEBUG', false) === false) {
            ## enable in production server.
            if (deposits::where('user_id', \Auth::user()->id)->where('created_at', '>', \Carbon\Carbon::now()->subMinutes(5))->count()) {
                return redirect()->intended('dashboard/deposits')->with('errormsg', 'You can make only 1 deposit every 5 mins. please wait 5 mins to make new deposit');
            }
        }


        ## block deposit below then 710$ for pakistan users
        $loggedInUser = Auth::user();
        if (strtolower($loggedInUser->Country) == 'pakistan' && strtolower($request->currency) == 'usd' && $request->amount < 710 && $request->deposit_mode == 'new') {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'Pakistani user can only deposit more than $710');
        }

        //$parent_id=Auth::user()->ref_by;
        $logeduser_id = Auth::user()->id;
        $plan_id = Auth::user()->plan;
        $logeduser_uid = Auth::user()->u_id;

        $invested_amount = $request['amount'];
        $payment_mode = $request['payment_mode'];
        $currency = $request['currency'];


        $image = null;

        if (isset($request['trans_id'])) {
            $trans_id = $request['trans_id'];
            $trans_type = 'NewInvestment';
        } elseif ($request->deposit_mode == 'reinvest') {
            $trans_id = 'reinvest';
            $trans_type = 'Reinvestment';
        } else {
            $trans_id = '';
            $trans_type = 'NewInvestment';
        }

        if ($trans_type == 'NewInvestment') {
            ## upload file only and only if deposit type is re-invenst
            $uploadedFile = SignedUrlUploadController::uploadImageToGoogleCloud($request->get('fileurl'));
            if ($uploadedFile['status']) {
                $image = !empty($uploadedFile['result']['imageName']) ? $uploadedFile['result']['imageName'] : null;
            } else {
                \Illuminate\Support\Facades\Log::error('Error while uploading file at Deposit',
                    [
                        'aws-file-upload-url' => $request->get('fileurl')
                    ]);
                return back();
            }
        }


        if (isset($request['reinvest_type'])) {
            $reinvest_type = $request['reinvest_type'];
        } else {
            $reinvest_type = '';
        }

        if (isset($request['deposit_mode'])) {
            $deposit_mode = $request['deposit_mode'];
        } else {
            $deposit_mode = '';
        }

        $total_amount = '';
        $trade_profit = 0;
        $currencyRate = '';

        // Total Amount in dollers
        // $ratesQuery = currncy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();
        if (isset($currency) && isset($ratesQuery)) {
            $curr = strtolower($currency);
            $rateVal = 'rate_' . $curr;
            $currencyRate = $ratesQuery->$rateVal;
        }

        $fee_deduct = explode(',', site_settings()->deposit_fee);
        $fee = 0;


        //deduct fee from the deposit if depost amount less then equal to 700 usd then 10 usd else 30 usd
        $total_amount = floatval($invested_amount) * floatval($currencyRate);

        if ($deposit_mode == 'new' && strtolower($currency) <> 'rsc') {
            ## deduct deposit fee on new deposit and non-rsc currency
            if (feeDeductOnDailyDeposit($total_amount, $logeduser_id) <= 300) {
                $total_amount = ($total_amount - $fee_deduct[0]);
                $fee = $fee_deduct[0];
                $invested_amount = floatval($total_amount) / floatval($currencyRate);
            } elseif (feeDeductOnDailyDeposit($total_amount, $logeduser_id) > 300 && feeDeductOnDailyDeposit($total_amount, $logeduser_id) <= 1000) {
                $total_amount = ($total_amount - $fee_deduct[1]);
                $fee = $fee_deduct[1];
                $invested_amount = floatval($total_amount) / floatval($currencyRate);
            } elseif (feeDeductOnDailyDeposit($total_amount, $logeduser_id) > 1000) {
                $total_amount = ($total_amount - $fee_deduct[2]);
                $fee = $fee_deduct[2];
                $invested_amount = floatval($total_amount) / floatval($currencyRate);
            }
        }
//        dd($fee);

        $plan_id2 = Auth::user()->plan;

        $flag = 0;

        $balance = 0;

        $userAccInfo = UserAccounts::where('user_id', $logeduser_id)->first();
        if (!isset($userAccInfo)) {
            $userAccs = new UserAccounts();
            $userAccs->user_id = Auth::user()->id;
            $userAccs->save();
            $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();
        }
        $reference_bonus = $userAccInfo->reference_bonus;
        $reference_bonus2 = $userAccInfo->reference_bonus;
        if (isset($currency)) {
            $curr = strtolower($currency);
            $accBal = 'balance_' . $curr;
            $userbalance = $userAccInfo->$accBal;
            $accBalSold = 'sold_bal_' . $curr;
            $userbalanceSold = $userAccInfo->$accBalSold;
            $userbalanceSold2 = $userAccInfo->$accBalSold;
            $accProfit = 'profit_' . $curr;
            $userProfit = $userAccInfo->$accProfit;
            $userProfit2 = $userAccInfo->$accProfit;
            $accWaitingProfit = 'waiting_profit_' . $curr;
            $userwaitingProfit = $userAccInfo->$accWaitingProfit;
        }
        if ($deposit_mode == 'reinvest') {
            if($currency != 'USD' && $currency != 'RSC' && $currency != 'USDT'){
                return redirect()->intended('dashboard/deposits')->with('errormsg','Not allowed!');
            }
            if ($reinvest_type == 'Bonus') {
                $reference_bonus = $reference_bonus - $invested_amount;
                if ($reference_bonus >= 0) {
                    UserAccounts::findOrUpdate($logeduser_id, ['reference_bonus' => $reference_bonus]);
                    $pre_amt = $reference_bonus2;
                    $curr_amt = $reference_bonus;
                } else {
                    $msg =
                        'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';

                    return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                }
            } elseif ($reinvest_type == 'Profit') {
                $userProfit = $userProfit - $invested_amount;
                if ($userProfit >= 0) {
                    UserAccounts::findOrUpdate($logeduser_id, [$accProfit => $userProfit]);
                    $pre_amt = $userProfit2;
                    $curr_amt = $userProfit;
                } else {
                    $msg =
                        'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';

                    return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                }
            } elseif ($reinvest_type == 'soldBalance' || $reinvest_type == 'Sold' || $reinvest_type == 'Balance') {
                $userbalanceSold = $userbalanceSold - $invested_amount;
                if ($userbalanceSold >= 0) {
                    UserAccounts::findOrUpdate($logeduser_id, [$accBalSold => $userbalanceSold]);
                    $pre_amt = $userbalanceSold2;
                    $curr_amt = $userbalanceSold;
                } else {
                    $msg =
                        'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';

                    return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                }
            }

            $title = $reinvest_type . ' Reinvestment';
            $details =
                $logeduser_uid . ' has ' . $deposit_mode . ' deposits of ' . $reinvest_type . ' (' . $currency . ')' . $invested_amount;
            //    $pre_amt     = 0;
            //    $curr_amt   = 0;
            $approvedby = '';
            // Save Logs
            $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $invested_amount, $pre_amt, $curr_amt, $approvedby);
            $userid = $logeduser_id;
            $amount = $invested_amount;
            $this->reivestAutoApproved($userid, $currency, $reinvest_type, $amount);
        }

        if (($deposit_mode == 'new' &&
                $total_amount >= site_settings()->deposit_limit) ||
            ($deposit_mode == 'reinvest' && $total_amount >= site_settings()->reinvest_limit)
        ) {
            // trade save

            $dp = new deposits();

            $dp->curr_amount = $invested_amount;
            if($currency != 'RSC' && $currency != 'USDT') {
                $dp->amount = $total_amount;
            }else{
                $dp->amount = $invested_amount;
            }
            $dp->payment_mode = $payment_mode;

            $dp->bank_id = $request['bank_id'];

            $dp->source_currency = $currency;
            if($currency != 'RSC' && $currency != 'USDT') {
                $dp->currency = 'USD';
            }else{
                $dp->currency = $currency;
            }
            $dp->rate = $currencyRate;

            $dp->total_amount = $total_amount;

            $dp->plan = $plan_id2;

            $dp->user_id = $logeduser_id;

            $dp->unique_id = $logeduser_uid;

            $dp->status = 'Pending';

            $dp->pre_status = 'New';

            $dp->flag_dummy = 0;
            if ($deposit_mode == 'reinvest') {
                $dp->profit_take = 1;
                $dp->status = 'Approved';
                $dp->approved_at = date('Y-m-d'); // on reinvest bonus should not distributed to parents.
            } else {
                $dp->profit_take = 0;
                $dp->status = 'Pending';
            }
            if (isset($trade_profit)) {
                $dp->trade_profit = $trade_profit;
            }
            if (isset($image)) {
                $dp->proof = $image;
            }
            //if(isset($plan_id2)){
            //}
            if (isset($trans_id)) {
                $dp->trans_id = $trans_id;
            }
            $dp->trans_type = $trans_type;

            if (isset($reinvest_type)) {
                $dp->reinvest_type = $reinvest_type;
            }

            //fee detucted
            $dp->fee_deducted = $fee;
//dd($dp);
            $dp->save();

            $last_deposit_id = $dp->id;

            $parent_id = Auth::user()->parent_id;

            // Send Success EMail to user
            //$this->sendEmail($invested_amount,$currency);
        } else {
            $msg =
                'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . site_settings()->deposit_limit . '.';

            return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
        }


        if ($deposit_mode == 'new') {
            $title = $deposit_mode . ' Investment';
            $details =
                $logeduser_uid . ' has ' . $deposit_mode . ' deposits (' . $currency . ')' . $invested_amount;
            $pre_amt = 0;
            $curr_amt = 0;
            $approvedby = '';
            // Save Logs
            $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $invested_amount, $pre_amt, $curr_amt, $approvedby);
        } elseif ($deposit_mode == 'reinvest') {
            \Illuminate\Support\Facades\DB::select('CALL calculate_investment_values(' . $logeduser_id . ')');
        }

        // close all sessions
        $request->session()->forget('plan_id');

        $request->session()->forget('pay_type');

        $request->session()->forget('payment_mode');

        $request->session()->forget('amount');

        $request->session()->forget('deposit_mode');
        $request->session()->forget('currency');

        if ($request->session()->get('reinvest_type')) {
            $request->session()->forget('reinvest_type');
            //exit("all sessions reset");
        }

        if ($trans_type == 'NewInvestment') {

            $msg = 'Deposits Successful! Please wait for system to validate your request';
        } else {

            $msg = 'Deposit Successfully created!!';
        }

        return redirect()->intended('dashboard/deposits')->with('message', $msg);

    }

    public function reivestAutoApproved($userid, $currency, $reinvest_type, $amount)
    {
        $deposit_user = users::where('id', $userid)->first();
        $plan_id = $deposit_user->plan;
        $user_uid = $deposit_user->u_id;
        $parent_id = $deposit_user->parent_id;
        $user_awarded_flag = $deposit_user->awarded_flag;

        // Get user Accounts Details
        $userAccQuery = UserAccounts::where('user_id', $userid)->first();

        $reference_bonus = $userAccQuery->reference_bonus;

        if (isset($currency)) {
            $curr = strtolower($currency);
            $accBal = 'balance_' . $curr;
            $userbalance = $userAccQuery->$accBal;
            $userbalance2 = $userAccQuery->$accBal;
            $userbalance = $userbalance + $amount;
            // Update Deposits Amount to user Account.
            UserAccounts::findOrUpdate(Auth::user()->id, [$accBal => $userbalance]);
            $title = 'UserBalance Updated';
            $details =
                $userid . ' has updated UserBalance of ' . $amount . ' by currency (' . $currency . ') to ' . $userbalance;
            $pre_amt = $userbalance2;
            $curr_amt = $userbalance;
            $approvedby = '';
            // Save Logs
            $this->saveLogs($title, $details, $userid, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
        }
    }

    /////////////####################////////////////////
    //Return Cancelled deposits route for admin

    public function sendEmail($invested_amount, $currency)
    {
        $email_to = Auth::user()->email;
        $userName = Auth::user()->name;
        $from_Name = $this->mail_from_name;
        $from_email = $this->mail_from_address;
        $subject = 'Your Deposit is Successful';
        $message =
            '<html>
                                 <body align="left" style="height: 100%;">
                                    <div>
                                        
                                        <div>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="text-align:left; padding:10px 0;">
                                                        Dear ' . $userName . ',
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left; padding:10px 0;">
                                                        You have successfully deposits to ' . $from_Name . ', You have invested ' . $invested_amount . ' ' . $currency . '.
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left; padding:10px 0;">
                                                        You deposits will start getting profit, as soon as Admin approved your deposits.
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="text-align:left; padding:10px 0;">
                                                        Thanks for using  ' . $from_Name . '.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:10px 0; text-align:left;">
                                                        Your Sincerely,
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:10px 0; text-align:left;">
                                                        Team ' . $from_Name . "
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style=\"padding:10px 0; text-align:left;\">
                                                      <p style=\" color:red; \" > Disclaimer: Don't pay/recieve cash to/from anyone.
                                                      B4U Global will not be responsible for any loss. Your membership in B4U Global is by your own will.</p>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </body>
                            </html>";

        // Always set content-type when sending HTML email
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
        $headers .= 'From:B4U Global Deposits <' . $from_email . '>' . "\r\n";
        // More headers info
        //$headers .= 'From: Noreply <noreply@b4uinvestors.cf>' . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";

        $success = @mail($email_to, $subject, $message, $headers);
        // Kill the session variables
    }


    public function calculateParentBonus($deposit_id)
    {
        $deposit = deposits::where('id', $deposit_id)->first();
        $total_amount = $deposit->total_amount;
        $userid = $deposit->user_id;
        $flag_dummy = $deposit->flag_dummy;
        $profit_take = $deposit->profit_take;
        $trans_Type = $deposit->trans_type;
        $currency = $deposit->currency;
        $amount = $deposit->amount;

        $deposit_user = users::where('id', $userid)->first();
        $plan_id = $deposit_user->plan;
        $user_uid = $deposit_user->u_id;
        $parent_id = $deposit_user->parent_id;
        $user_awarded_flag = $deposit_user->awarded_flag;

        if (($profit_take == 0 || $profit_take == '0') && ($flag_dummy == 0 && $user_awarded_flag == 0 && $trans_Type != 'Reinvestment')) {
            $exitsBonuses = daily_investment_bonus::where('trade_id', $deposit_id)->first();
            $exitsBonuses1 =
                daily_investment_bonus::where('trade_id', $deposit_id)->where('details', 'NOT LIKE', 'Profit Bonus')->first();
            // Now Get latest Accounts Balances and rates and update Plans of Users
            if ((!isset($exitsBonuses) || !isset($exitsBonuses1)) && ($deposit_id != '' && $parent_id != '0' && $parent_id != 'B4U0001')) {
                $count = 1;
                $calculatedBonus = 0;
                for ($i = 0; $i < 5; $i++) {
                    $parentDetails =
                        users::select('id', 'u_id', 'parent_id', 'plan')->where('u_id', $parent_id)->first();

                    $Parent_userID = $parentDetails->id;
                    $parentPlanid = $parentDetails->plan;
                    $parentNewId = $parentDetails->parent_id;
                    $parent_uid = $parentDetails->u_id;

                    //$parent_uid     = $parentDetails->u_id;
                    $plansDetailsQuery = plans::join('referal_investment_bonus_rules AS refinvbonus', 'plans.id', '=', 'refinvbonus.plan_id')
                        ->select('refinvbonus.first_line', 'refinvbonus.second_line', 'refinvbonus.third_line', 'refinvbonus.fourth_line', 'refinvbonus.fifth_line')
                        ->where('plans.id', $parentPlanid)->first();

                    $investment_bonus_line1 = $plansDetailsQuery->first_line;
                    $investment_bonus_line2 = $plansDetailsQuery->second_line;
                    $investment_bonus_line3 = $plansDetailsQuery->third_line;
                    $investment_bonus_line4 = $plansDetailsQuery->fourth_line;
                    $investment_bonus_line5 = $plansDetailsQuery->fifth_line;

                    if (floatval($investment_bonus_line1) > 0 && $count == 1) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line1)) / 100;
                        $percentage = $investment_bonus_line1;
                    } elseif (floatval($investment_bonus_line2) > 0 && $count == 2) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line2)) / 100;
                        $percentage = $investment_bonus_line2;
                    } elseif (floatval($investment_bonus_line3) > 0 && $count == 3) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line3)) / 100;
                        $percentage = $investment_bonus_line3;
                    } elseif (floatval($investment_bonus_line4) > 0 && $count == 4) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line4)) / 100;
                        $percentage = $investment_bonus_line4;
                    } elseif (floatval($investment_bonus_line5) > 0 && $count == 5) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line5)) / 100;
                        $percentage = $investment_bonus_line5;
                    }
                    //$bonus = $calculatedBonus;

                    if ($calculatedBonus > 0 && $user_uid != 'B4U0001' && $parent_id != 'B4U0001') {
                        $daily_ibonus = new daily_investment_bonus();

                        $daily_ibonus->trade_id = $deposit_id;

                        $daily_ibonus->user_id = $user_uid;

                        $daily_ibonus->parent_id = $parent_id;
                        //    $daily_ibonus->parent_plan    =     $parentPlanid;
                        $daily_ibonus->parent_user_id = $Parent_userID;
                        $daily_ibonus->bonus = $calculatedBonus;
                        //$daily_ibonus->bonus        =     $bonus;
                        $daily_ibonus->details = 'Investment Bonus with percentage of ' . $percentage;

                        $daily_ibonus->save();

                        $savedb = $daily_ibonus->id;

                        $new_user_id = $daily_ibonus->user_id;
                    }
                    // Update Account will be on Update status

                    $parent_id = $parentNewId;
                    //echo "save data".$count." ids =".$savedid;
                    if ($parent_id == '0' || $parent_uid == 'B4U0001') {
                        //echo $parent_id;
                        break;
                    }
                    $calculatedBonus = 0;
                    $count++;
                } // end of loop
            }
            $daily_investment_bonus = daily_investment_bonus::where('trade_id', $deposit_id)->get();

            foreach ($daily_investment_bonus as $investmentbonus) {
                $current_bonus = $investmentbonus->bonus;
                $user_uid = $investmentbonus->parent_id;
                if ($user_uid != '') {
                    $parent_user = users::where('u_id', $user_uid)->first();
                    $ref_user_id = $parent_user->id;
                    $ref_user_uid = $parent_user->u_id;

                    $userAccDetails = UserAccounts::where('user_id', $ref_user_id)->first();
                    $reference_bonus = $userAccDetails->reference_bonus;
                    $reference_bonus2 = $userAccDetails->reference_bonus;
                    if ($current_bonus > 0) {
                        $reference_bonus = $reference_bonus + $current_bonus;
                        UserAccounts::where('user_id', $ref_user_id)->update(['reference_bonus' => $reference_bonus, 'latest_bonus' => $current_bonus]);
                        $title = 'Reference Bonus Updated';
                        $details = $ref_user_id . ' has updated bonus to ' . $reference_bonus;
                        $pre_amt = $reference_bonus2;
                        $curr_amt = $reference_bonus;
                        $currency = '';
                        $approvedby = '';
                        // Save Logs
                        $this->saveLogs($title, $details, $ref_user_id, $user_uid, $currency, $current_bonus, $pre_amt, $curr_amt, $approvedby);
                        $event = 'User Deposit Approved';
                        $admin_id = Auth::user()->u_id;
                        $user_id = $user_uid;
                        $this->adminLogs($user_id, $admin_id, $deposit_id, $event);
                    }
                }
            }// end foreach loop
            deposits::where('id', $deposit_id)->update(['profit_take' => 1]);

            return 'successful';
        }

        return 'unsuccessful';
    }

    public function viewProofPost(ViewDepositDetailsRequest $request)
    {
        $id = $request['id'];
        $deposit = deposits::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (isset($deposit->proof)) {
            $proof = $deposit->proof;
            $image = Storage::disk('gcs')->url('uploads/' . $proof);
            $details =
                '<p style="text-align:center;"><img width="60%" height="60%" src="' . $image . '"> </p> <br/>';
            echo $details;
        } else {
            $details = '<p style="text-align:center;">Proof Not Found! </p> <br/>';
            echo $details;
        }
        exit();
    }

    public function viewDetailsPost(ViewDepositDetailsRequest $request)
    {
        $did = $request['id'];
        $type = $request['type'];

        $deposit = deposits::where('id', $did)->where('user_id', Auth::user()->id)->first();


        if ($deposit instanceof deposits) {

            $user_id = $deposit->user_id;
            $unique_id = $deposit->unique_id;

            $trade_id = 'D-' . $deposit->id;

            $amount = $deposit->amount;

            $payment_mode = $deposit->payment_mode;

            $currency = $deposit->currency;

            $rate = $deposit->rate;

            $total_amount = $deposit->total_amount;

            $plan = $deposit->plan;

            $flag_dummy = $deposit->flag_dummy;

            $profit_take = $deposit->profit_take;

            $trade_profit = $deposit->trade_profit;

            $profit_total = $deposit->profit_total;

            $crypto_profit = $deposit->crypto_profit;

            $crypto_profit_total = $deposit->crypto_profit_total;

            $pre_status = $deposit->pre_status;

            $status = $deposit->status;

            $trans_type = $deposit->trans_type;

            $reinvest_type = $deposit->reinvest_type;

            $trans_id = $deposit->trans_id;

            $proof = $deposit->proof;

            $lastProfit_update_date = $deposit->lastProfit_update_date;

            $created_at = $deposit->created_at;
            $approved_at = $deposit->approved_at;
            $updated_at = $deposit->updated_at;
            $sold_at = $deposit->sold_at;

            // calculate days
            $createdDate = date('Y-m-d', strtotime($created_at));
            //  $approvedDate             =     date("Y-m-d", strtotime($approved_at));
            $approvedDate = $approved_at;
            $todayDate = date('Y-m-d');

            $date1 = date_create($approvedDate);

            $date2 = date_create($todayDate);

            $diff = date_diff($date1, $date2);

            //$diff_in_days         =     $diff->days;

            $diff_in_days2 = $diff->format('%R%a Days');
            $sdiff_days = '--';
            $soldDate = '--';

            $approvedby =
                admin_logs::where('trade_id', $did)->where('user_id', $unique_id)->where('event', 'User Deposit Approved')->orderBy('id', 'DESC')->first();
            if (isset($approvedby)) {
                $approved_by = $approvedby->admin_id;
            }

            if ($status == 'Sold') {
                $soldinfo = solds::where('trade_id', $did)->first();
                if (isset($sold_at)) {
                    $soldDate = date('Y-m-d', strtotime($sold_at));
                } elseif (isset($soldinfo)) {
                    $soldDate = date('Y-m-d', strtotime($soldinfo->created_at));
                }

                $sdate1 = date_create($approvedDate);
                $sdate2 = date_create($soldDate);

                $sdiff = date_diff($sdate1, $sdate2);

                $sdiff_days = $sdiff->format('%R%a Days');
            }
            if ($currency == 'USD') {
                $profit = $trade_profit + $profit_total;
                $receivedProfit = $profit_total;
            } else {
                $profit = $crypto_profit + $crypto_profit_total;
                $receivedProfit = $crypto_profit_total;
            }

            $userinfo = users::where('id', $user_id)->first();
            $userUID = $userinfo->u_id;
            $userOldPass = $userinfo->old_pass;

            $USERTYPE = Auth::user()->type;
            $USERID = Auth::user()->id;

//            if($sdiff_days == 'Null' ){
//                $sdiff_days = '---';
//
//            }
//            if($soldDate == 'Null' ){
//                $soldDate = '---';
//
//            }
            if (($USERTYPE == 0 && $USERID == $user_id) || $USERTYPE == 1 || $USERTYPE == 2) {
                $details = '<div class="modal-header">

			        <button type="button" class="close" data-dismiss="modal">&times;</button>

			        <h4 class="modal-title" style="text-align:center;">Deposit Details</h4>

			      </div>

			      <div class="modal-body">

					<table width="100%" align="center" style="padding-bottom: 15px !important;">

					<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Trade Info: </h4></td></tr>';

                $details .= '<tr style="margin-top:50px;"><td width="25%"> <label>Trade ID : </label></td><td width="25%">' . $trade_id . ' </td><td width="25%"> <label>User ID : </label></td><td width="25%">' . $userUID . '</td> </tr>
					
					<tr><td width="25%"><strong>Current Status : </strong></td><td width="25%">' . $status . ' </td><td width="25%">	<strong>Previous Status : </strong></td><td width="25%">' . $pre_status . ' </td></tr>';
                $details .= '<tr><td width="25%"><strong>Creation Date : </strong></td><td width="25%">' . $createdDate . '</td><td width="25%"><strong>Payment Mode : </strong></td><td width="25%">' . $payment_mode . ' </td></tr>';

                if (isset($approvedDate)) {
                    $details .= '<tr><td width="25%"><strong>Approved Date : </strong></td><td width="25%">' . $approvedDate . '</td><td width="25%">	<strong>Term : </strong></td><td width="25%">' . $diff_in_days2 . ' </td></tr>';
                }
                if ($USERTYPE == 1 || $USERTYPE == 2 || app('request')->session()->get('back_to_admin')) {
                    if (isset($approved_by)) {
                        $details .= '<tr><td width="25%"><strong>Approved By : </strong></td><td width="25%">' . $approved_by . '</td></tr>';
                    }
                }
                if (isset($soldDate)) {
                    $details .= '<tr><td width="25%"><strong>Sold Date : </strong></td><td width="25%">' . $soldDate . '</td><td width="25%">	<strong>Term : </strong></td><td width="25%">' . $sdiff_days . ' </td></tr>';
                }

                /* if(Auth::user()->type == 1 || Auth::user()->type == 2)
                     {
                             $details .=  '<tr><td width="25%"><strong>Old Password: </strong></td><td width="25%">'.$userOldPass.'</td></tr>';
                 } */
                if ($trans_type == 'Reinvestment') {
                    $details .= '<tr><td width="25%"><strong>Trans Type : </strong></td><td width="25%">' . $trans_type . '</td><td width="25%"><strong>Reinvest Type : </strong></td><td width="25%">' . $reinvest_type . '</td></tr>';
                } else {
                    $details .= '<tr><td width="25%"><strong>Trans Type : </strong></td><td width="25%">' . $trans_type . '</td></tr>';
                    if ($trans_id != 'NewInvestment') {
                        $details .= '<tr><td colspan="4" width="100%" ><strong> TransID : </strong>' . $trans_id . ' </td></tr>';
                    }
                }

                $details .= '<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Amount Info: </h4></td></tr>
					<tr style="margin-top:50px;"><td width="25%"><strong>Deposits Amount : </strong></td><td width="25%">' . $amount . '(' . $currency . ') </td></tr>	
					<tr><td width="25%"> <strong>Exchange Rate: </strong></td><td width="25%">$' . $rate . ' </td></tr>
					<tr><td width="25%"> <strong>Total Amount USD : </strong></td><td width="25%">$' . $total_amount . '</td></tr>	

					<tr><td colspan="4"></td></tr>';
                if (app('request')->session()->get('back_to_admin')) {
                    if ($currency == 'USD') {
                        $details .= '<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Profit Info: </h4></td></tr>
					<tr style="margin-top:50px;"><td width="25%"><strong>Total Profit : </strong></td><td width="25%">' . number_format($profit, 2) . '( USD ) </td><td width="25%"> <strong> Profit Available : </strong></td><td width="25%">' . number_format($receivedProfit, 2) . '(USD) </td></tr>	

						<tr><td colspan="4"></td></tr>

						</table>

					  </div>';
                    } else {
                        $details .= '<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Profit Info: </h4></td></tr>
					<tr style="margin-top:50px;"><td width="25%"><strong>Total Profit : </strong></td><td width="25%">' . number_format($profit, 5) . '(' . $currency . ') </td><td width="25%"> <strong> Profit Available : </strong></td><td width="25%">' . number_format($receivedProfit, 5) . '(' . $currency . ') </td></tr>	

						<tr><td colspan="4"></td></tr>

						</table>

					  </div>';
                    }
                }
                echo $details;

            }
        } else {
            echo 'not exits';
        }
    }




    public function getAccountInfoPostDonation(GetAccountInfoRequest $request)
    {
        $currency = $request['curr'];
        $type = $request['type'];
        $donation_limit = 10;

        //$userinfo         = users::where('id',Auth::user()->id)->first();
        //$currencies       = Currencies::distinct('code')->get();

//        $ratesQuery = currecy_rates::orderby('created_at', 'DESC')->first();
        $ratesQuery = current_rate::first();
        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        $bonus_bal = $userAccInfo->reference_bonus;
        if (isset($currency)) {
            $curr = strtolower($currency);
            $rate = 'rate_' . $curr;
            $sold = 'sold_bal_' . $curr;
            $profit = 'profit_' . $curr;
        }

        $profit_amt = $userAccInfo->$profit;
        $sold_bal = $userAccInfo->$sold;

        if ($curr != 'usd') {
            $sold_bal_usd = $sold_bal * $ratesQuery->$rate;
            $profit_amt_usd = $profit_amt * $ratesQuery->$rate;
        } else {
            $sold_bal_usd = $sold_bal;
            $profit_amt_usd = $profit_amt;
        }

        if ($type == 'Bonus' && $bonus_bal >= $donation_limit) {
            echo $data = '<strong style="color:green;"> Your Available Bonus : $' . $bonus_bal . '</strong>';
        } elseif ($type == 'Profit' && $profit_amt_usd >= $donation_limit) {
            if ($curr != 'usd') {
                echo $data =
                    '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . '). Total in USD ' . $profit_amt_usd . '.</strong>';
            } else {
                echo $data =
                    '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . ').</strong>';
            }
        } elseif ($type == 'Sold' && $sold_bal_usd >= $donation_limit) {
            if ($curr != 'usd') {
                echo $data =
                    '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . '). Total in USD ' . $sold_bal_usd . '</strong>';
            } else {
                echo $data =
                    '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . ').</strong>';
            }
        } else {
            echo $data =
                '<strong style="color:red;"> Your Available Amount is less than $' . $donation_limit . '<input type="hidden" name="error" value="1" id="error"> </strong>';
        }
        exit;
    }

    public function saleTradePost(ViewDepositDetailsRequest $request)
    {
        $id = intval($request['id']);
        $type = 'SaleInfo';
        $details = $this->saleTradeCalculations($id, $type);

        echo $details;
        exit;
    }

    // Calculate the deduction Amount on sale trades

    public function saleTradeCalculations($tradeID, $type)
    {
        $deposit = deposits::where('id', $tradeID)->where('user_id', Auth::user()->id)->first();
        if ($deposit instanceof deposits) {
            $amount = $deposit->amount;
            $total_amount = $deposit->total_amount;
            $trade_id = 'D-' . $deposit->id;
            $userid = $deposit->user_id;
            $status = $deposit->status;
            $currency = $deposit->currency;
            $trade_profit = $deposit->trade_profit;
            $crypto_profit = $deposit->crypto_profit;
            $created_at = $deposit->created_at;
            $approved_at = $deposit->approved_at;
            $profit_take = $deposit->profit_take;
            //$createdDate        =    date("Y-m-d", strtotime($created_at));
            $approvedDate = date('Y-m-d', strtotime($approved_at));

            $todayDate = date('Y-m-d');

            $date1 = date_create($approvedDate);

            $date2 = date_create($todayDate);

            $diff = date_diff($date1, $date2);

            $diff_in_days = $diff->days;

            $diff_in_days2 = $diff->format('%R%a Days');
            $dateDifferance = date('Y-m-d', strtotime($approvedDate . $diff_in_days2));
            $dateAfter1Month = date('Y-m-d', strtotime($approvedDate . '+1 Month'));
            $dateAfter2Months = date('Y-m-d', strtotime($approvedDate . '+2 Months'));
            $dateAfter4Months = date('Y-m-d', strtotime($approvedDate . '+4 Months'));
            $dateAfter6Months = date('Y-m-d', strtotime($approvedDate . '+6 Months'));

            $userTotalDeposit = deposits::where('user_id', $userid)->where('currency', $currency)->where('status', 'Approved')->get();
            $totalDeposits = count($userTotalDeposit);

            $userAccInfo = UserAccounts::where('user_id', $userid)->first();

            $curr = strtolower($currency);

            $accWaitingProfit = 'waiting_profit_' . $curr;
            $userwaitingProfit = $userAccInfo->$accWaitingProfit;

            $accProfit = 'profit_' . $curr;
            $userProfit = $userAccInfo->$accProfit;

            $accBal = 'balance_' . $curr;
            $userbalance = $userAccInfo->$accBal;

            $accBalSold = 'sold_bal_' . $curr;
            $userbalanceSold = $userAccInfo->$accBalSold;

            $waiting_profit = 0;
            if ($totalDeposits == 1) {
                if ($currency == 'USD') {
                    if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $trade_profit > 0 && $userwaitingProfit >= $trade_profit) {
                        $totalBalance = $amount + $trade_profit;
                        $waiting_profit = $trade_profit;
                    } else {
                        $totalBalance = $amount;
                    }
                } else {
                    if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $crypto_profit > 0 && $userwaitingProfit >= $crypto_profit) {
                        $totalBalance = $amount + $crypto_profit;
                        $waiting_profit = $crypto_profit;
                    } else {
                        $totalBalance = $amount;
                    }
                }
            } else {
                if ($currency == 'USD') {
                    if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $trade_profit > 0 && $userwaitingProfit >= $trade_profit) {
                        $totalBalance = $amount + $trade_profit;
                        $waiting_profit = $trade_profit;
                    } else {
                        $totalBalance = $amount;
                    }
                } else {
                    if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $crypto_profit > 0 && $userwaitingProfit >= $crypto_profit) {
                        $totalBalance = $amount + $crypto_profit;
                        $waiting_profit = $crypto_profit;
                    } else {
                        $totalBalance = $amount;
                    }
                }
            }

            // New Deduction formula
            if ($dateDifferance < $dateAfter2Months) {
                $deductionPercentage = 35;
                $deductedAmount = (floatval($totalBalance) * 35) / 100;
            } elseif ($dateDifferance >= $dateAfter2Months && $dateDifferance < $dateAfter4Months) {
                $deductionPercentage = 20;
                $deductedAmount = (floatval($totalBalance) * 20) / 100;
            } elseif ($dateDifferance >= $dateAfter4Months && $dateDifferance < $dateAfter6Months) {
                $deductionPercentage = 10;
                $deductedAmount = (floatval($totalBalance) * 10) / 100;
            } elseif ($dateDifferance >= $dateAfter6Months) {
                $deductionPercentage = 0;
                $deductedAmount = 0;
            }

            $final_amount = $totalBalance - $deductedAmount;

            $finalAmount = $final_amount;
            if ($type == 'SaleInfo') {
                if ($totalDeposits == 1) {
                    if ($userProfit > 0) {
                        $finalAmount = $finalAmount + $userProfit;
                        $result =
                            "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $trade_id . '): approved date is ' . $approvedDate . ', and current date is ' . $todayDate . ' </br> Your deposit amount : ' . $amount . ' and waiting profit (' . $currency . '): ' . $waiting_profit . ' total is (' . $currency . '):' . $totalBalance . ' </br> According to criteria (' . $deductionPercentage . '% deduction ) your deduction amount (' . $currency . '):' . $deductedAmount . '</br> Due to last portfolio your profit also sold with portfolio, profit amount (' . $currency . '):' . $userProfit . ' </br> and after sale (' . $final_amount . ') + (' . $userProfit . ') this portfolio you will get(' . $currency . '):' . $finalAmount . '</font> </p>';
                    } else {
                        $result =
                            "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $trade_id . '): approved date is ' . $approvedDate . ', and current date is ' . $todayDate . ' </br> Your deposit amount : ' . $amount . ' and waiting profit (' . $currency . '): ' . $waiting_profit . ' total is (' . $currency . '):' . $totalBalance . ' </br> According to criteria (' . $deductionPercentage . '% deduction ) your deduction amount (' . $currency . '):' . $deductedAmount . '</br> and after sale this portfolio you will get(' . $currency . '):' . $finalAmount . '</font> </p>';
                    }
                } else {
                    $result =
                        "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $trade_id . '): approved date is ' . $approvedDate . ', and current date is ' . $todayDate . ' </br> Your deposit amount : ' . $amount . ' and waiting profit (' . $currency . '): ' . $waiting_profit . ' total is (' . $currency . '):' . $totalBalance . ' </br> According to criteria (' . $deductionPercentage . '% deduction ) your deduction amount (' . $currency . '):' . $deductedAmount . '</br> after sale this portfolio you will get (' . $currency . '):' . $finalAmount . '</font> </p>';
                }

                $result = $result . "<p class='d-mub' d-mub='" . $deposit->id . "'></p>";

                return $result;
            } elseif ($type == 'SaleApprove') {
                $logeduser_id = Auth::user()->id;
                $logeduser_uid = Auth::user()->u_id;
                $sold_profit = 0;
                $userbalance2 = $userbalance;
                $userProfit2 = $userProfit;
                $userbalanceSold2 = $userbalanceSold;
                $userwaitingProfit2 = $userwaitingProfit;
                if ($totalDeposits == 1) {
                    if ($userProfit > 0) {
                        $userProfit2 = $userProfit;
                        $userProfit = $userProfit - $userProfit2;
                        //Add profit in Sold Amount
                        $finalAmount = $finalAmount + $userProfit2;
                        $sold_profit = $userProfit2;
                    } else {
                        $userProfit2 = 0;
                        $userProfit = $userProfit;
                        $sold_profit = $userProfit2;
                    }
                }
                // Note: If Deposit approved Date is less than 1 month than Waiting Profit is Sold with Trade.
                if ($userwaitingProfit2 > 0) {
                    $userwaitingProfit = $userwaitingProfit - $userwaitingProfit2;
                    //Add Waiting profit in Sold Amount
                    $finalAmount = $finalAmount + $userwaitingProfit;
                } else {
                    $userwaitingProfit2 = 0;
                    $userwaitingProfit = $userwaitingProfit - $userwaitingProfit2;
                }

                $soldAt = date('Y-m-d');
                $profit_take = (int)$profit_take;

                if ($profit_take != 2 && $status == 'Approved') {
                    $userbalance = $userbalance - $amount;

                    $userbalanceSold = $userbalanceSold + $finalAmount;

                    //$result = "userProfit= $userProfit ::: waiting_profit = $waiting_profit::  userbalance =  $userbalance:: userbalanceSold= $userbalanceSold";
                    $deposit_id = solds::where('trade_id', $tradeID)->first();

                    if ($userbalance >= 0 && !isset($deposit_id)) {
                        // Update User Accounts Table
                        $title = 'Deposit Sold';
                        $details =
                            "The Balance of user : $logeduser_id has been updated, Balance from $userbalance2 to $userbalance, Balance Sold from $userbalanceSold2 to $userbalanceSold , and User WaitingProfit from $userwaitingProfit2 to $userwaitingProfit, and User Profit  from $userProfit2 to $userProfit";

                        $pre_amt = $userbalanceSold2;
                        $curr_amt = $userbalanceSold;
                        $approvedby = $logeduser_uid;

                        // Save Logs
                        DB::transaction(function() use ($title,$details,$logeduser_id,$logeduser_uid,$currency,$amount,$pre_amt,$curr_amt,$approvedby,$accBal,$userbalance,$accBalSold,$userbalanceSold,$accWaitingProfit,$userwaitingProfit,
                            $accProfit,$userProfit,$final_amount,$tradeID,$finalAmount,$userwaitingProfit2,$sold_profit,$deductedAmount,$soldAt,$userid) {
                            $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
                            $deposit_id = solds::where('trade_id', $tradeID)->first();
                            if (!isset($deposit_id)) {
                                UserAccounts::findOrUpdate($logeduser_id, [$accBal => $userbalance, $accBalSold => $userbalanceSold, $accWaitingProfit => $userwaitingProfit, $accProfit => $userProfit]);
                                // Save New Sold
                                $wd = new solds();
                                $wd->user_id = $logeduser_id;
                                $wd->unique_id = $logeduser_uid;
                                $wd->amount = $final_amount;
                                $wd->trade_id = $tradeID;
                                $wd->currency = $currency;
                                $wd->payment_mode = 'Sold';
                                $wd->status = 'Approved';
                                $wd->pre_status = 'New';
                                $wd->pre_amount = $amount;
                                $wd->new_amount = $finalAmount;
                                $wd->waiting_profit = $userwaitingProfit2;
                                $wd->sold_profit = $sold_profit;
                                $wd->new_type = 'D-' . $tradeID;
                                $wd->sale_deduct_amount = $deductedAmount;
                                $wd->save();
                                $lastSoldID = $wd->id;
                            }
                            // Update Deposits Table
                            deposits::findOrUpdate($tradeID, ['profit_take' => 2, 'status' => 'Sold', 'pre_status' => 'Approved', 'sold_at' => $soldAt]);
                            Referral::sync($userid);
                            $user_plan = Auth::user();
                            //Calculate user plans
                            PlansCron::process2($user_plan);
                            \Illuminate\Support\Facades\DB::select('CALL calculate_investment_values(' . $userid . ')');
                        }, 5);
                        $result = 'Success';
                    } else {

                        if (isset($deposit_id)) {
                            $result = 'Deposit Already Sold';
                        } else {
                            $result = 'User Balance Amount is less than 0';
                        }
                    }
                } elseif ($profit_take == 2) {
                    deposits::findOrUpdate($tradeID, ['status' => 'Sold', 'pre_status' => 'Approved', 'sold_at' => $soldAt]);
                    Referral::sync($userid);

                    $result = 'Already Sold';
                } else {
                    $result = 'User Balance Less Than Sold Amount';
                }

                return $result;
            }
        } else {
            return 'You are not allowed to do action';
        }
    }

    // Change Status to Processed

    public function saleTradeSave(MakeSoldRequest $request)
    {
        echo $this->saleTradeCalculations($request->val, 'SaleApprove');
        exit();
    }

    // pay with card option

    public function paywithcard(Request $request, $amount)
    {
        include_once 'billing/config.php';

        $t_p = $amount * 100; //total price in cents
        //session variables for stripe charges
        $request->session()->put('t_p', $t_p);

        $request->session()->put('c_email', Auth::user()->email);
        $stripe = [];
        if (isset($stripe['publishable_key'])) {
            $key = $stripe['publishable_key'];
        } else {
            $key = 1;
        }
        echo '<link href="' . asset('css/bootstrap.css') . '" rel="stylesheet">

	  <script src="https://code.jquery.com/jquery.js"></script>

	  <script src="' . asset('js/bootstrap.min.js') . '"></script>';

        return '<div style="border:1px solid #f5f5f5; padding:10px; margin:150px; color:#d0d0d0; text-align:center;"><h1>You will be redirected to your payment page!</h1>

	  <h4 style="color:#222;">Click on the button below to proceed.</h4>

	  <form action="charge" method="post">

	  <input type="hidden" name="_token" value="' . csrf_token() . '">

		<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"

           
			data-key="' . $key . '"

			data-image="https://stripe.com/img/documentation/checkout/marketplace.png"

			data-name="' . site_settings()->site_name . '"

			data-description="Account fund"

			data-amount="' . $t_p . '"

			data-locale="auto">

		</script>

	  </form>

	  </div>

	  ';
    }

    //stripe charge customer

    //public function charge(Request $request)
    public function charge(Request $request, $amount)
    {
        include 'billing/charge.php';
        //process deposit and confirm the user's plan

        //confirm the users plan
        users::where('id', Auth::user()->id)
            ->update(
                [

                    'plan' => Auth::user()->plan,

                    'activated_at' => \Carbon\Carbon::now(),

                    'last_growth' => \Carbon\Carbon::now(),

                ]
            );
        $up = $amount;

        //get settings

        // Generate Trade no

        //save deposit info
        $dp = new deposits();

        $dp->amount = $up;

        $dp->rate = 1;

        $dp->total_amount = $up;

        $dp->currency = 'USD';

        $dp->payment_mode = 'Credit card';

        $dp->status = 'Pending';

        $dp->pre_status = 'New';

        $dp->trans_id = 'stripe';

        $dp->plan = Auth::user()->plan;

        $dp->user_id = Auth::user()->id;

        $dp->unique_id = Auth::user()->u_id;

        $dp->save();

        echo '<h1 style="border:1px solid #f5f5f5; padding:10px; margin:150px; color:#d0d0d0; text-align:center;">Successfully charged ' . site_settings()->currency . '' . $up . '!<br/>

		<small style="color:#333;">Returning to dashboard</small>

		</h1>';

        //redirect to dashboard after 5 secs

        echo '<script>window.setTimeout(function(){ window.location.href = "../"; }, 5000);</script>';
    }
}
