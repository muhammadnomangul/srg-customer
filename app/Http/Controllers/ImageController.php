<?php

namespace App\Http\Controllers;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class ImageController extends Controller
{
    //
    const BucketName = 'b4uglobaltmp';
    public $AwsS3Client;
    const S3FileUploadACL = 'public-read';

    public function __construct()
    {
//        $this->middleware('auth');
        $credentials = new Credentials(
            config('b4uglobal.AWS_KEY'),
            config('b4uglobal.AWS_SECRET')
        );

        $this->AwsS3Client = new S3Client([
            'region' => config('b4uglobal.AWS_REGION'),
            'version' => config('b4uglobal.AWS_VERSION'),
            'credentials' => $credentials,
            'options' => [],
        ]);
    }


    /*
     * ======================= AWS
     * */

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getPostObjectUrl(Request $request)
    {

//        $validator = Validator::make($request->all(), [
//            'filename' => 'required'
//        ]);
//
//
//        if ($validator->fails()) {
//            return response()->json(['status' => false, 'message' => $validator->getMessageBag()]);
//        }

        $Key = $this->filename_prefix() . 'nnnnnnn';
        $formInputs = ['acl' => self::S3FileUploadACL, 'key' => $Key];
        $options = [
            ['acl' => self::S3FileUploadACL],
            ['bucket' => self::BucketName],
            ['Key' => $Key]

        ];
        $expires = '+1 hours';
        $postObject = new \Aws\S3\PostObjectV4(
            $this->AwsS3Client,
            self::BucketName,
            $formInputs,
            $options,
            $expires
        );


        $formInputs = [];
        foreach ($postObject->getFormInputs() as $key => $value) {
            $formInputs[strtr($key, '-', '_')] = $value;
        }

        return response()->json(
            ['status' => true, 'message' => 'url found', 'result' => ['formInputs' => $formInputs, 'formAttributes' => $postObject->getFormAttributes()]]
        );


    }

    /**
     * @param $objectKey
     * @return JsonResponse
     */
    public function getObjectUrl($objectKey)
    {
        try {
            $objectUrl = $this->AwsS3Client->getObjectUrl(self::BucketName, $objectKey);
            return response()->json(['status' => true, 'message' => 'url found', 'result' => ['url' => $objectUrl]]);
        } catch (Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Purpose:: Delete Object
     * @param $objectKeyName
     */
    public function deleteObject($objectKeyName)
    {
        $this->AwsS3Client->deleteObject([
            'Bucket' => self::BucketName,
            'Key' => $objectKeyName
        ]);

    }


    /*
     * ======================= GOOGLE CLOUD
     * */


    public function fileStoreTempDir()
    {
        $path = storage_path('app/temp') . DIRECTORY_SEPARATOR;
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }

        return $path;
    }


    public static function uploadImageToGoogleCloudErrorResponse()
    {

    }

    /**
     * @param $fileUrl
     * @param string $cloudUploadPath
     * @return array|\Illuminate\Http\RedirectResponse
     * Purpose Upload Image to Google Cloud and clean storage.
     */
    public static function uploadImageToGoogleCloud($fileUrl, $cloudUploadPath = 'uploads')
    {
//        return $fileUrl;
//        if($fileUrl){
//            echo $fileUrl;
//        }else {
//            echo "ni hoya bc ";
//        }
        if (empty($fileUrl)) {

            return [
                'status' => false,
                'message' => 'File name is empty'
            ];

        }
        try {
            $user_id = 1 ;
            $file_name_prefix =  $user_id . '_' . time() . '_' . md5(bcrypt(rand(0, 99999))) . '_';
//            $fileName_ = (new self())->filename_prefix();
            $fileName_ = $file_name_prefix;
            $fileNameAr = explode('_', $fileName_);

//            if (empty($fileNameAr[0])) {
//                return [
//                    'status' => false,
//                    'message' => 'Invalid user id'
//                ];
//            } else if ($fileNameAr[0] != Auth::user()->id) {
//                return [
//                    'status' => false,
//                    'message' => 'Wrong Attempt'
//                ];
//            }

            // temporary upload images to temp folder
            ## image name
            $imageName = $fileName_ . '.png';
            ## image path
            $imagePath = $fileName_ . $imageName;
            imagepng(imagecreatefromstring(file_get_contents($fileUrl)), $imagePath);
            $newlyCreatedScannedImage = storage_path('app/temp') . DIRECTORY_SEPARATOR . $fileName_ . 'scannedImage.png';


            // File and new size of image to scan is this image or not
            $percent = 0.9;
            list($width, $height) = getimagesize($imagePath);
            $newWidth = $width * $percent;
            $newHeight = $height * $percent;
            $thumb = imagecreatetruecolor($newWidth, $newHeight);

            $source = imagecreatefrompng($imagePath);
            imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            imagepng($thumb, $newlyCreatedScannedImage);

            $path = Storage::disk('gcs')->putFile(DIRECTORY_SEPARATOR . $cloudUploadPath, $newlyCreatedScannedImage, Filesystem::VISIBILITY_PUBLIC);

            // delete temp file
            is_file($imagePath) ? unlink($imagePath) : null;
            // after upload delete the scanned image
            is_file($newlyCreatedScannedImage) ? unlink($newlyCreatedScannedImage) : null;

            $imageName = str_replace($cloudUploadPath . DIRECTORY_SEPARATOR, '', $path);

            return [
                'status' => true,
                'message' => 'File uploaded',
                'result' => [
                    'imageName' => $imageName,
                    'imagePath' => $path
                ]
            ];

        } catch (Exception $exception) {
            return [
                'status' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    public function filename_prefix()
    {
        $user_id = 1 ;
        return $user_id . '_' . time() . '_' . md5(bcrypt(rand(0, 99999))) . '_';
    }

}
