<?php

namespace App\Http\Controllers;

use App\current_rate;
use App\Http\Requests\EmailTemplate\EmailTemplateIdRequest;
use App\Http\Requests\EmailTemplate\SaveEmailTemplateRequest;
use App\Http\Requests\EmailTemplate\UpdateEmailTemplateRequest;
use App\Http\Requests\Misc\TopUpRequest;
use App\Http\Requests\Settings\UpdateSettingsRequest;
use App\Http\Requests\Users\ViewUserDetailsRequest;
use App\Model\Referral;
use App\withdrawals;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\settings;
use App\users;
use App\ph;
use App\gh;
use App\deposits;
use App\email_templates;
use App\Currencies;
use DB;
use App\UserAccounts;
use Mail;
use File;
use Session;
use Socialite;
use Artisan;
use Illuminate\Support\Facades\Cache;
use App\OTPToken;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');

    }


    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    //return settings form

    public function subscribeAlpha()
    {
        $user = Auth::user();
        $user->is_sub_alpha = 0;
        $user->save();
        $url = "http://tiny.cc/n7lupz";
        return Redirect::to($url);
    }
    public function check(){

        $user = Auth::user();
        $userAccount = UserAccounts::whereUserId($user->id)->first();
        $rate = current_rate::first();
        // user account
        // get all currency values of profits
        // store in array in dollars index named as currency code
        // return the highest code value from array ..
        // store currency code as well
       // dd($userAccount);
        $max_amount =0;
        $currCode ='USD';
        $currCodesm ='usd';
         foreach (Currencies::getCurrencies() as $curr){

             $currProfit =  'profit_'.$curr->small_code;

             $currRate = 'rate_'.$curr->small_code;
             $currRate = $rate->$currRate;
             $profitAmount_in_dollars  =  $userAccount->$currProfit * $currRate;

             if($max_amount < $profitAmount_in_dollars ){
                 $max_amount=$profitAmount_in_dollars;
                 $currCode = $curr->code;
                 $currCodesm = $curr->small_code;

             }
         }
         $data = [
             'code' => $currCode,
             'amount' => $max_amount,
             'currCodesm' => $currCodesm,
         ];

         return $data;




    }
    public function subcribeCallPckg(){
        $user = Auth::user();
        $u_id = $user->u_id;
        if($user->plan >= 2) {
            $userAccount = UserAccounts::whereUserId($user->id)->first();
            $rate = current_rate::first();
            $deduction = 0;

//            foreach (Currencies::getCurrencies() as $curr){
//                $currProfit =  'profit_'.$curr->small_code;
//                $currRate = 'rate_'.$curr->small_code;
//                $currRate = $rate->$currRate;
//                $profitAmount_in_dollars  =  $userAccount->$currProfit * $currRate;
//                if($profitAmount_in_dollars >= 50 && $deduction == 0 ){
//                    $amount = $profitAmount_in_dollars - 50;
//                    $amount =  $amount/$currRate;
//                    $deduction_amount = 50/$currRate;
//                    $withdrawal = new withdrawals();
//                    $withdrawal = $withdrawal->addSubscriptionChargesWithdrawal($user->id,'Profit','',$deduction_amount,'$max_amount');
//                    UserAccounts::findOrUpdate($user->id,[$currProfit => $amount]);
//                    $deduction = 1;
//                    $msg = "Your package has been successfully subscribed and amount has been deducted from profit ";
//                }
//            }
            $data  = $this->check();
            $code = $data['currCodesm'];
            $codebig = $data['code'];
            $currProfit =  'profit_'.$code;
            $currRate = 'rate_'.$code;
            $currRate = $rate->$currRate;
            $profitAmount_in_dollars  =  $userAccount->$currProfit * $currRate;
            if($profitAmount_in_dollars >= 50 && $deduction == 0 ){
                $amount = $profitAmount_in_dollars - 50;
                $pre_amount = $userAccount->$currProfit;
                $amount =  $amount/$currRate;
                $deduction_amount = 50/$currRate;
                $post_amount = $userAccount->$currProfit - $deduction_amount;
                $withdrawal = new withdrawals();
                if($codebig == 'USD'){

                    $withdrawal = $withdrawal->addSubscriptionChargesWithdrawal($user->id,$u_id,'Profit',$pre_amount,'',$post_amount,'',$deduction_amount,$codebig);
                }else{
                    $withdrawal = $withdrawal->addSubscriptionChargesWithdrawal($user->id,$u_id,'Profit',$pre_amount,$post_amount,$profitAmount_in_dollars,'',$deduction_amount,$codebig);

                }
                UserAccounts::findOrUpdate($user->id,[$currProfit => $amount]);
                $deduction = 1;
                $msg = "Your package has been successfully subscribed and amount has been deducted from profit ";
            }
            if($deduction == 0 && $userAccount->reference_bonus >= 50){
                $pre_amount_bouns  = $userAccount->reference_bonus;
                $amount = $userAccount->reference_bonus - 50;
                $wiith = new withdrawals();
                $wiith = $wiith->addSubscriptionChargesWithdrawal($user->id,$u_id,'Bonus',$pre_amount_bouns,'',$amount,'','50','USD');
                UserAccounts::findOrUpdate($user->id,['reference_bonus' => $amount]);
                $msg = "Your package has been successfully subscribed and amount has been deducted from bonus";
            } elseif ($deduction == 0){
                $amount = $userAccount->charges + 50;
                UserAccounts::findOrUpdate($user->id,['charges' => $amount]);
                $msg = "Action Successful.. you dont have enough profit or bonus . Your amount will be deducted from your next withdrawal.";
//                /* return redirect()->intended('dashboard')->with('errormsg', 'You dont have enough profit or bonus to subscribe this offer');*/
            }

            $user->is_call_sub = 1;
            $user->call_sub_at = date('Y-m-d');
            $user->save();

           users::updateCallPackageOnCallCenterService($user->id,true);
            return redirect()->intended('dashboard')->with('message',  $msg);
        }else{
            return redirect()->intended('dashboard')->with('errormsg', 'Please upgrade your plan to avail this package');
        }
    }

    public function settingsEmail($user_uid, $email, $details)
    {
        $email_to = "toseefamir@gmail.com";

        $currentdate = date('d-m-Y', strtotime("-1 days"));

        $from_Name = getMailFromName();
        $from_email = getSupportMailFromAddress();
        $subject = "Be alert !! B4U Settings has been changed";
        $message =
            "<html>
                         <body align=\"left\" style=\"height: 100%;\">
                            <div>
								
								<div>
									<table style=\"width: 100%;\">
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												<h1>Dear Sir</h1>
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												<h1>Please notify that,</h1>
											</td>
										</tr>
										
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Your Settings has been modified by " . $user_uid . ".
														
											</td>
										</tr>
										
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												having email  " . $email . ". This changing has been set at " . $currentdate . ".
											</td>
										</tr>

										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Your Sincerely,
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Team " . $from_Name . "
												
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												<a  href=\"{{ url('/') }}\" >
													<image src=\"{{ url('images/b4u-investment1.png') }}\">
												</a>
												
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
													<span style=\"font-size:12.8px;color:#500050;\">Office :&nbsp;</span>
													<a rel=\"nofollow\" style=\"color:#1155cc;\" target=\"_blank\" href=\"https://maps.google.com/?q=Via+Antonio+Salandra,18,00187+Roma&amp;entry=gmail&amp;source=g\">Via Antonio Salandra,18,00187 Roma</a>&nbsp;RM Italy.
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												<span style=\"font-size:12.8px;color:#500050;\">Phone :&nbsp;</span>
												<a rel=\"nofollow\" style=\"color:#1155cc;font-size:12.8px;\" target=\"_blank\" href=\"https://bitcoins4u.my/newsletter/tel:+39%2006%20421273\">0039-06-421273</a>
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												<span style=\"font-size:12.8px;color:#500050;\">Email :&nbsp;</span>
												<a rel=\"nofollow\" style=\"color:#1155cc;\" ymailto=\"mailto:support@bitcoin4u.my\" target=\"_blank\" href=\"mailto:support@bitcoin4u.my\">support@bitcoins4u.my</a>
											
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												<span style=\"font-size:12.8px;color:#500050;\">Website :&nbsp;</span>
												<a rel=\"nofollow\" style=\"color:#1155cc;font-size:12.8px;\" target=\"_blank\" href=\"http://www.b4uglobal.com/\">www.b4uglobal.com</a>
											</td>
										</tr>
										
									</table>
								</div>
							</div>
						</body>
					</html>";
        //echo $message;
        //exit;
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From:B4U Global Support <' . $from_email . '>' . "\r\n";
        // More headers info
        //$headers .= 'From: Noreply <noreply@b4uinvestors.cf>' . "\r\n";
        $headers .= 'Cc: alrehmancorner@gmail.com' . "\r\n";

        $success = @mail($email_to, $subject, $message, $headers);
    }

    //Trading history routes
    public function tradinghistory()
    {
        return view('thistory')
            ->with(
                array(

                    'amount' => array_random([10.12, 2.3, 5.7, 20, 80.22, 50.89, 30, 40.23, 5, 60, 89, 4, 200.76, 140, 410.34, 103.34]),

                    'id' => array_random(['#2321', '#8735', '#0286', '#1037', '#9027', '#3426', '#0082', '#3246', '#0002', '#1002', '#0052', '#2321']),

                    'profitloss' => array_random(['Profit', 'Loss']),

                    'date' => Carbon::now(),

                    'title' => 'Trading History'
                )
            );
    }

    //email Template

    public function emailTemplateList()
    {
        return view('emailTemplateList')->with(
            array(

                'title' => 'List All Email Templates',
                'emailTemplates' => email_templates::all(),

            )
        );
    }

    //email Template

    public function emailTemplate()
    {
        return view('emailTemplate')->with(
            array(
                'title' => 'Save New Email Template',
            )
        );
    }

    //Save mail Content

    public function saveEmailTemp(SaveEmailTemplateRequest $request)
    {
        $wd = new email_templates();
        $wd->title = $request['title'];
        $wd->message = $request['message'];
        $wd->from_email = $request['from_email'];
        $wd->body = htmlspecialchars($request['body']);
        $wd->save();
        return redirect()->back()->with('successmsg', 'Action Successful!');
    }

    public function cacheClear()
    {
        Cache::flush();
        //php artisan cache:clear
        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        //php artisan view:clear
        \Illuminate\Support\Facades\Artisan::call('view:clear');
        file_put_contents(storage_path('logs/laravel.log'), '');
        return redirect('dashboard/')->with('successmsg', 'Cache Cleared, and logs are deleted successfully!');
    }
}