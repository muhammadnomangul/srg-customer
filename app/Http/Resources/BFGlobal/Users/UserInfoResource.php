<?php

namespace App\Http\Resources\BFGlobal\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'userref' => $this->id,
            'username' => $this->name,
            'useremail' => $this->email,
            'userphone' => $this->phone_no
        ];
    }
}
