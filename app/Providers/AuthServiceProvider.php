<?php

namespace App\Providers;

use App\currency_rates;
use App\deposits;
use App\Donation;
use App\Logs;
use App\Model\Deposit;
use App\plans;
use App\Policies\CurrencyRatePolicy;
use App\Policies\DepositPolicy;
use App\Policies\DonationPolicy;
use App\Policies\LogPolicy;
use App\Policies\PlanPolicy;
use App\Policies\PromotionPolicy;
use App\Policies\SoldPolicy;
use App\Policies\UserPolicy;
use App\Policies\WithdrawalPolicy;
use App\Promotion;
use App\solds;
use App\User;
use App\withdrawals;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Logs::class => LogPolicy::class,
        User::class => UserPolicy::class,
        deposits::class => DepositPolicy::class,
        Deposit::class => DepositPolicy::class,
        withdrawals::class => WithdrawalPolicy::class,
        Donation::class => DonationPolicy::class,
        solds::class => SoldPolicy::class,
        Promotion::class => PromotionPolicy::class,
        plans::class => PlanPolicy::class,
        currency_rates::class => CurrencyRatePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $user = Auth::user();
        Gate::define('manage-donations', function ($user) {
            if ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT)){
                return true;
            }
            return false;
        });
        Gate::define('user-sections', function ($user) {
            if ( $user->is_user(SITE_NORMAL_USER)
              || $user->is_user(SITE_ADMIN) 
              || $user->is_user(SITE_MANAGER) 
              || $user->is_user(SITE_AGENT) 
              || $user->is_user(SITE_COMMITTEE) 
              || $user->is_user(SITE_COUNTRY_MANAGER)
               ) {
                return true;
            }
            return false;
        });
        Gate::define('search-user', function ($user) {
            if ($user->is_user(SITE_ADMIN)
                || $user->is_user(SITE_MANAGER)
                || $user->is_user(SITE_SUPER_ADMIN)
                || $user->is_user(SITE_AGENT)
            ) {
                return true;
            }
            return false;
        });
        Gate::define('view-only-admin', function ($user) {
            if ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN)) {
                return true;
            }
            return false;
        });
        Gate::define('view-only-admin-manager', function ($user) {
            if ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_MANAGER)) {
                return true;
            }
            return false;
        });

        Gate::define('update-block', function ($user) {
            if (\Illuminate\Support\Facades\Session::has('admin-login')) {
                return false;
            }else{

                return true;
            }
        });
    }
}
