<?php

namespace App\Providers;

use App\settings;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $settings = settings::getSettings();

//        view()->composer(['footer','home.index','home.plans-all','admin.users','referuser','donations.index'], function ($view) use ($settings) {
//            $view->with([
//                'settings' => $settings
//            ]);
//        });
        View::share('settings', $settings);
    }
}
