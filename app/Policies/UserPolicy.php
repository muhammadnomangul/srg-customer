<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT));
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }
    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function updateRole(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }
    /**
     * Determine whether the user can login the another user.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function login(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_MANAGER));
    }

    public function blockUnblock(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_MANAGER));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }
}
