<?php

namespace App\Policies;

use App\User;
use App\Deposit;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DepositPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any deposits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT));
    }
    /**
     * Determine whether the user can view any deposits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewOwn(User $user)
    {
        return ($user->is_user(SITE_NORMAL_USER) || $user->is_user(SITE_COMMITTEE));
    }

    /**
     * Determine whether the user can view the deposit.
     *
     * @param  \App\User  $user
     * @param  \App\Deposit  $deposit
     * @return mixed
     */
    public function view(User $user, Deposit $deposit)
    {
        //
    }

    /**
     * Determine whether the user can create deposits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the deposit.
     *
     * @param  \App\User  $user
     * @param  \App\Deposit  $deposit
     * @return mixed
     */
    public function update(User $user, Deposit $deposit)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }
    /**
     * Determine whether the user can manage the deposit.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function manage(User $user)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_MANAGER) ||  $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can delete the deposit.
     *
     * @param  \App\User  $user
     * @param  \App\Deposit  $deposit
     * @return mixed
     */
    public function delete(User $user, Deposit $deposit)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can restore the deposit.
     *
     * @param  \App\User  $user
     * @param  \App\Deposit  $deposit
     * @return mixed
     */
    public function restore(User $user, Deposit $deposit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the deposit.
     *
     * @param  \App\User  $user
     * @param  \App\Deposit  $deposit
     * @return mixed
     */
    public function forceDelete(User $user, Deposit $deposit)
    {
        //
    }
}
