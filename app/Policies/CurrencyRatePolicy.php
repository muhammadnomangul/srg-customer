<?php

namespace App\Policies;

use App\User;
use App\currency_rates;
use Illuminate\Auth\Access\HandlesAuthorization;

class CurrencyRatePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any currency_rates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can view the currency_rates.
     *
     * @param  \App\User  $user
     * @param  \App\currency_rates  $currencyRates
     * @return mixed
     */
    public function view(User $user, currency_rates $currencyRates)
    {
        //
    }

    /**
     * Determine whether the user can create currency_rates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the currency_rates.
     *
     * @param  \App\User  $user
     * @param  \App\currency_rates  $currencyRates
     * @return mixed
     */
    public function update(User $user, currency_rates $currencyRates)
    {
        //
    }

    /**
     * Determine whether the user can delete the currency_rates.
     *
     * @param  \App\User  $user
     * @param  \App\currency_rates  $currencyRates
     * @return mixed
     */
    public function delete(User $user, currency_rates $currencyRates)
    {
        //
    }

    /**
     * Determine whether the user can restore the currency_rates.
     *
     * @param  \App\User  $user
     * @param  \App\currency_rates  $currencyRates
     * @return mixed
     */
    public function restore(User $user, currency_rates $currencyRates)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the currency_rates.
     *
     * @param  \App\User  $user
     * @param  \App\currency_rates  $currencyRates
     * @return mixed
     */
    public function forceDelete(User $user, currency_rates $currencyRates)
    {
        //
    }
}
