<?php

namespace App\Policies;

use App\User;
use App\Logs;
use Illuminate\Auth\Access\HandlesAuthorization;

class LogPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any logs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can view the logs.
     *
     * @param  \App\User  $user
     * @param  \App\Logs  $logs
     * @return mixed
     */
    public function view(User $user, Logs $logs)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }

    /**
     * Determine whether the user can create logs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the logs.
     *
     * @param  \App\User  $user
     * @param  \App\Logs  $logs
     * @return mixed
     */
    public function update(User $user, Logs $logs)
    {
        //
    }

    /**
     * Determine whether the user can delete the logs.
     *
     * @param  \App\User  $user
     * @param  \App\Logs  $logs
     * @return mixed
     */
    public function delete(User $user, Logs $logs)
    {
        //
    }

    /**
     * Determine whether the user can restore the logs.
     *
     * @param  \App\User  $user
     * @param  \App\Logs  $logs
     * @return mixed
     */
    public function restore(User $user, Logs $logs)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the logs.
     *
     * @param  \App\User  $user
     * @param  \App\Logs  $logs
     * @return mixed
     */
    public function forceDelete(User $user, Logs $logs)
    {
        //
    }
}
