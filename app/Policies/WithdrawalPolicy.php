<?php

namespace App\Policies;

use App\User;
use App\withdrawals;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class WithdrawalPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any withdrawals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT));
    }


    public function canAction(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN)  );
    }

     public function viewList(User $user)
    {
        return ($user->is_user(SITE_ADMIN) ||  $user->is_user(SITE_SUPER_ADMIN) ||  $user->is_user(SITE_COUNTRY_MANAGER)  );
    }

     public function Paid(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_COUNTRY_MANAGER)  );
    }
    /**
     * Determine whether the user can view any withdrawals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewOwn(User $user)
    {
        return ($user->is_user(SITE_NORMAL_USER) || $user->is_user(SITE_COMMITTEE));
    }

    /**
     * Determine whether the user can view the withdrawals.
     *
     * @param  \App\User  $user
     * @param  \App\withdrawals  $withdrawals
     * @return mixed
     */
    public function view(User $user, withdrawals $withdrawals)
    {
        //
    }

    /**
     * Determine whether the user can create withdrawals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the withdrawals.
     *
     * @param  \App\User  $user
     * @param  \App\withdrawals  $withdrawals
     * @return mixed
     */
    public function update(User $user, withdrawals $withdrawals)
    {
        //
    }

    /**
     * Determine whether the user can delete the withdrawals.
     *
     * @param  \App\User  $user
     * @param  \App\withdrawals  $withdrawals
     * @return mixed
     */
    public function delete(User $user, withdrawals $withdrawals)
    {
        //
    }

    /**
     * Determine whether the user can restore the withdrawals.
     *
     * @param  \App\User  $user
     * @param  \App\withdrawals  $withdrawals
     * @return mixed
     */
    public function restore(User $user, withdrawals $withdrawals)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the withdrawals.
     *
     * @param  \App\User  $user
     * @param  \App\withdrawals  $withdrawals
     * @return mixed
     */
    public function forceDelete(User $user, withdrawals $withdrawals)
    {
        //
    }
}
