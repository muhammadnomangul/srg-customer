<?php

namespace App\Policies;

use App\User;
use App\plans;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PlanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any plans.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN));
    }
    /**
     * Determine whether the user can view any plans.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewOwn(User $user)
    {
        return ($user->is_user(SITE_NORMAL_USER) || $user->is_user(SITE_COMMITTEE));
    }

    /**
     * Determine whether the user can view the plans.
     *
     * @param  \App\User  $user
     * @param  \App\plans  $plans
     * @return mixed
     */
    public function view(User $user, plans $plans)
    {
        //
    }

    /**
     * Determine whether the user can create plans.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the plans.
     *
     * @param  \App\User  $user
     * @param  \App\plans  $plans
     * @return mixed
     */
    public function update(User $user, plans $plans)
    {
        //
    }

    /**
     * Determine whether the user can delete the plans.
     *
     * @param  \App\User  $user
     * @param  \App\plans  $plans
     * @return mixed
     */
    public function delete(User $user, plans $plans)
    {
        //
    }

    /**
     * Determine whether the user can restore the plans.
     *
     * @param  \App\User  $user
     * @param  \App\plans  $plans
     * @return mixed
     */
    public function restore(User $user, plans $plans)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the plans.
     *
     * @param  \App\User  $user
     * @param  \App\plans  $plans
     * @return mixed
     */
    public function forceDelete(User $user, plans $plans)
    {
        //
    }
}
