<?php

namespace App\Policies;

use App\User;
use App\Donation;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DonationPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any donations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT));
    }
    /**
     * Determine whether the user can view any donations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewOwn(User $user)
    {
        return ($user->is_user(SITE_NORMAL_USER) || $user->is_user(SITE_COMMITTEE));
    }

    /**
     * Determine whether the user can view the donation.
     *
     * @param  \App\User  $user
     * @param  \App\Donation  $donation
     * @return mixed
     */
    public function view(User $user, Donation $donation)
    {
        //
    }

    /**
     * Determine whether the user can create donations.
     *+
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the donation.
     *
     * @param  \App\User  $user
     * @param  \App\Donation  $donation
     * @return mixed
     */
    public function update(User $user, Donation $donation)
    {
        //
    }

    /**
     * Determine whether the user can delete the donation.
     *
     * @param  \App\User  $user
     * @param  \App\Donation  $donation
     * @return mixed
     */
    public function delete(User $user, Donation $donation)
    {
        //
    }

    /**
     * Determine whether the user can restore the donation.
     *
     * @param  \App\User  $user
     * @param  \App\Donation  $donation
     * @return mixed
     */
    public function restore(User $user, Donation $donation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the donation.
     *
     * @param  \App\User  $user
     * @param  \App\Donation  $donation
     * @return mixed
     */
    public function forceDelete(User $user, Donation $donation)
    {
        //
    }
}
