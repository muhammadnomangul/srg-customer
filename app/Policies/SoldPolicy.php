<?php

namespace App\Policies;

use App\User;
use App\solds;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class SoldPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any solds.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return ($user->is_user(SITE_ADMIN) || $user->is_user(SITE_MANAGER) || $user->is_user(SITE_COUNTRY_MANAGER) || $user->is_user(SITE_SUPER_ADMIN) || $user->is_user(SITE_AGENT));
    }
/**
     * Determine whether the user can view any solds.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewOwn(User $user)
    {
        return ($user->is_user(SITE_NORMAL_USER) || $user->is_user(SITE_AGENT));
    }

    /**
     * Determine whether the user can view the solds.
     *
     * @param  \App\User  $user
     * @param  \App\solds  $solds
     * @return mixed
     */
    public function view(User $user, solds $solds)
    {
        //
    }

    /**
     * Determine whether the user can create solds.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the solds.
     *
     * @param  \App\User  $user
     * @param  \App\solds  $solds
     * @return mixed
     */
    public function update(User $user, solds $solds)
    {
        //
    }

    /**
     * Determine whether the user can delete the solds.
     *
     * @param  \App\User  $user
     * @param  \App\solds  $solds
     * @return mixed
     */
    public function delete(User $user, solds $solds)
    {
        //
    }

    /**
     * Determine whether the user can restore the solds.
     *
     * @param  \App\User  $user
     * @param  \App\solds  $solds
     * @return mixed
     */
    public function restore(User $user, solds $solds)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the solds.
     *
     * @param  \App\User  $user
     * @param  \App\solds  $solds
     * @return mixed
     */
    public function forceDelete(User $user, solds $solds)
    {
        //
    }
}
