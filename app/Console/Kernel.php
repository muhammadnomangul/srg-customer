<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ProfitCalculationsCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('create:cashboxaccount')->withoutOverlapping()->everySixHours();
        $schedule->command('create:cbcreditoncbwithdrawalapprove')->everyTenMinutes();
        $schedule->command('unsuspend:wrong:attempt')->everyTenMinutes();
        $schedule->command('currRate:Update')->everyTenMinutes();
//        $schedule->command('refferal:partners 16999')->twiceDaily(1, 13);
//        $schedule->command('refferal:partners 1')->twiceDaily(1, 13);
//        $schedule->command('refferal:partners 16993')->twiceDaily(1, 13);
//        $schedule->command('refferal:partners 4341')->twiceDaily(1, 13);
//        $schedule->command('refferal:partners 11475')->twiceDaily(1, 13);
//        $schedule->command('refferal:partners 2210')->twiceDaily(1, 13);
        $schedule->command('pending_approve_id_fix')->everyTenMinutes();
//        $schedule->command('create:cbcreditifdebitcreated')->everyThirtyMinutes();


        //$schedule->command('autoProfit:Update')->weekdays()->at('17:00');

//        $schedule->command('create:cbcreditifdebitcreated')->everyFifteenMinutes();
//		$schedule->command('profit:day')->weekdays()->at('22:00');
//		$schedule->command('profit2:day2')->weekdays()->at('22:00');
//		$schedule->command('profitCron:daily')->weekdays()->at('22:00');
//
//		$schedule->command('NonUSDProfitCron:daily')->weekdays()->at('22:30');
//        $schedule->command('USDProfitCron:daily')->weekdays()->at('22:30');
//		$schedule->command('ReinvestProfitCron:daily')->weekdays()->at('22:30');
//
//		$schedule->command('emails:daily')->weekdays()->at('22:30');
//        $schedule->command('bonus:find')->weekdays()->at('22:30');
//
//        $schedule->command('emails:eid')->weekdays()->at('22:30');
//        $schedule->command('fixed_deposit:cron')->weekdays()->at('00:00')->runInBackground();
//        $schedule->command('refresh:promo:investor')->twiceDaily(1, 13);
        //$schedule->command('bonus:find')->weekdays()->at('22:30');
        //$schedule->command('profit:day')->everyFiveMinutes();
        //->everyMinute();
        // $schedule->command('profit:day')->hourly();
        //->dailyAt('21:00');
        //->everyFiveMinutes();
        //->daily();
        //->cron('*/1 * * * * *'); every min
        //->dailyAt('13:00');
    }
// For run local execute this cmd => php artisan profit:day

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
