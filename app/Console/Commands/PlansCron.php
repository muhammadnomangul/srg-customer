<?php

namespace App\Console\Commands;

use App\Currencies;
use App\current_rate;
use App\Model\AttractiveFunds;
use App\Model\Referral;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

//use Illuminate\Support\Facades\Mail as Email;
//use illuminate\contract\Mailer;
//use App\User;
use App\users;
use App\plans;
use App\bonus_history;
use App\deposits;
use App\UserAccounts;
use App\currency_rates;
use App\daily_investment_bonus;
use App\daily_profit_bonus;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use DB;
use DateTime;
use Mail;

class PlansCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plans:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Plans to all customers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
    }


    static function countSubUsers2($user_id, $plan = 1)
    {
        $level = self::plantolevel($plan);

        $childs = \App\Model\Referral::select(['child_id'])->where('parent_id', $user_id)->where('level', '<=', $level)->distinct('child_id')->get();

        return count($childs);
    }

    /////////////####################////////////////////
    static function countSubUsers($user_id, $plan = 1)
    {
        $level = self::plantolevel($plan);

        $user = User::findOrFail($user_id);
        $loged_user_id = $user->id;
        $loged_parent_id = $user->parent_id;
        $Loged_user_uid = $user->u_id;

        $secondLine = [];
        $thirdLine = [];
        $fourthLine = [];
        $fifthLine = [];
        $secondTotal = 0;
        $thirdTotal = 0;
        $fourthTotal = 0;
        $fifthTotal = 0;

        $firstLine = DB::table('users')->select('u_id', 'parent_id')->where('parent_id', $Loged_user_uid)->get();

        $firstTotal = count($firstLine);

        if (isset($firstLine)) {
            $totalAmount = 0;
            $counter = 0;
            $row = 0;//$firstTotal = count($firstLine);
            for ($i = 0; $i < $firstTotal; $i++) {
                $result = DB::table('users')->select('u_id', 'parent_id')->where('parent_id', $firstLine[$i]->u_id)->get();
                $count2 = count($result);
                if ($count2 == 0) {
                    continue;
                }
                array_push($secondLine, $result);
                //  for($j=0; $j<$count2; $j++)
                // {
                //	$totalAmount = $totalAmount + $result[$j]->account_bal;
                // }
                $counter++;
                $row = $row + $count2;
            }
            $secondLine = $secondLine;
            $secondTotal = $row;
        }

        if (isset($secondLine)) {
            $totalAmount2 = 0;
            $counter2 = 0;
            $rows = 0;

            for ($i = 0; $i < count($secondLine); $i++) {


                foreach ($secondLine[$counter2] as $lines) {
                    $result = DB::table('users')->select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($thirdLine, $result);
                    // for($j=0; $j<$count2; $j++)
                    //{
                    //  $totalAmount2 = $totalAmount2 + $result[$j]->account_bal;
                    //}
                    $rows = $rows + $count2;
                }
                $counter2++;
            }
            $thirdLine = $thirdLine;
            $thirdTotal = $rows;
        }
        if (isset($thirdLine)) {

            $totalAmount3 = 0;
            $counter3 = 0;
            $rows1 = 0;//$count = count($thirdLine);

            for ($i = 0; $i < count($thirdLine); $i++) {

                foreach ($thirdLine[$counter3] as $lines) {

                    $result = DB::table('users')->select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();

                    $count2 = count($result);

                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fourthLine, $result);
                    // for($j=0; $j<$count2; $j++)
                    //  {
                    //	$totalAmount3 = $totalAmount3 + $result[$j]->account_bal;
                    //  }
                    $rows1 = $rows1 + $count2;
                }
                $counter3++;
            }
            $fourthLine = $fourthLine;
            $fourthTotal = $rows1;
        }

        if (isset($fourthLine)) {
            $counter4 = 0;
            $totalAmount4 = 0;
            $rows2 = 0;
            for ($i = 0; $i < count($fourthLine); $i++) {

                foreach ($fourthLine[$counter4] as $lines) {
                    $result = DB::table('users')->select('u_id', 'parent_id')->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fifthLine, $result);
                    //for($j=0; $j<$count2; $j++)
                    //{
                    //	$totalAmount4 = $totalAmount4 + $result[$j]->account_bal;
                    //}
                    $rows2 = $rows2 + $count2;
                }
                $counter4++;
            }
            $fifthLine = $fifthLine;
            $fifthTotal = $rows2;
        }
        //echo "FirstLine - ".$firstTotal."SecondLine - ".$secondTotal. "ThirdLine - ".$thirdTotal."FourthLine - ".$fourthTotal."FifthLine - ".$fifthTotal;
        //exit;
        $total = 0;
        if ($level >= 1) {
            $total = $firstTotal;
        }
        if ($level >= 2) {
            $total = $total + $secondTotal;
        }
        if ($level >= 3) {
            $total = $total + $thirdTotal;
        }
        if ($level >= 4) {
            $total = $total + $fourthTotal;
        }
        if ($level >= 5) {
            $total = $total + $fifthTotal;
        }
        return $total;
    }

    static public function totalSubUsersInvestment($id, $plan = 1)
    {
        $level = self::plantolevel($plan);
        $userinfo = users::where('id', $id)->first();
        $loged_user_id = $userinfo->id;
        $loged_parent_id = $userinfo->parent_id;
        $Loged_user_uid = $userinfo->u_id;

        //$ratesQuery = currecy_rates::orderBy('created_at', 'desc')->first();
        $ratesQuery = current_rate::first();


        $usdRate = $ratesQuery->rate_usd;
        $bitcoinRate = $ratesQuery->rate_btc;
        $bitcashRate = $ratesQuery->rate_bch;
        $ethereumRate = $ratesQuery->rate_eth;
        $litecoinRate = $ratesQuery->rate_ltc;
        $rippleRate = $ratesQuery->rate_xrp;
        $dashRate = $ratesQuery->rate_dash;
        $zcashRate = $ratesQuery->rate_zec;


        $secondLine = [];
        $thirdLine = [];
        $fourthLine = [];
        $fifthLine = [];
        $secondTotal = 0;
        $thirdTotal = 0;
        $fourthTotal = 0;
        $fifthTotal = 0;

        $firstLine = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.u_id', 'users.parent_id', 'users.created_at', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp')
            ->where('users.parent_id', $Loged_user_uid)
            ->get();

        $firstTotal1 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_usd as acc_bal_usd')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_usd');
        $firstTotal2 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_btc as acc_bal_btc')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_btc');
        $firstTotal3 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_eth as acc_bal_eth')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_eth');
        $firstTotal4 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_ltc as acc_bal_ltc')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_ltc');

        $firstTotal5 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_bch as acc_bal_bch')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_bch');
        $firstTotal6 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_xrp as acc_bal_xrp')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_xrp');
        $firstTotal7 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_dash as acc_bal_dash')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_dash');
        $firstTotal8 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_zec as acc_bal_zec')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_zec');

        //echo $firstTotal1." BTC = ". $firstTotal2 ." ETH = ".$firstTotal3 ." LTC = ".$firstTotal4 ." BCH = ".$firstTotal5." XRP = ". +$firstTotal6 ;

        $firstTotal = $firstTotal1 + $firstTotal2 * $bitcoinRate + $firstTotal3 * $ethereumRate + $firstTotal4 * $litecoinRate + $firstTotal5 * $bitcashRate + $firstTotal6 * $rippleRate + $firstTotal7 * $dashRate + $firstTotal8 * $zcashRate;

        if (isset($firstLine)) {
            $totalAmount = 0;
            $totalUsd = 0;
            $totalBtc = 0;
            $totalEth = 0;
            $totalBch = 0;
            $totalLtc = 0;
            $totalXrp = 0;
            $totalDash = 0;
            $totalZec = 0;

            $counter = 0;
            $row = 0; //$firstTotal = count($firstLine);
            for ($i = 0; $i < count($firstLine); $i++) {
                // $result = DB::table('users')->select('u_id','parent_id')->where('parent_id', $firstLine[$i]->u_id)->get();
                $result = DB::table('users')
                    ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                    ->select('users.u_id', 'users.parent_id', 'users.created_at', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                        'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                    ->where('parent_id', $firstLine[$i]->u_id)->get();
                $count2 = count($result);
                if ($count2 == 0) {
                    continue;
                }
                array_push($secondLine, $result);
                for ($j = 0; $j < $count2; $j++) {
                    $totalUsd = $totalUsd + $result[$j]->usd;
                    $totalBtc = $totalBtc + $result[$j]->btc;
                    $totalEth = $totalEth + $result[$j]->eth;
                    $totalLtc = $totalLtc + $result[$j]->ltc;
                    $totalBch = $totalBch + $result[$j]->bch;
                    $totalXrp = $totalXrp + $result[$j]->xrp;
                    $totalDash = $totalDash + $result[$j]->dash;
                    $totalZec = $totalZec + $result[$j]->zec;
                }
                $counter++;
                //$row = $row+$count2;
            }
            $secondLine = $secondLine;
            $totalAmount = $totalUsd + $totalBtc * $bitcoinRate + $totalEth * $ethereumRate + $totalBch * $bitcashRate + $totalLtc * $litecoinRate + $totalXrp * $rippleRate + $totalDash * $dashRate + $totalZec * $zcashRate;
            $secondTotal = $totalAmount;
            //$secondTotal = $row;
        }
        if (isset($secondLine)) {
            $totalAmount2 = 0;
            $totalUsd2 = 0;
            $totalBtc2 = 0;
            $totalEth2 = 0;
            $totalBch2 = 0;
            $totalLtc2 = 0;
            $totalXrp2 = 0;
            $totalDash2 = 0;
            $totalZec2 = 0;
            $counter2 = 0;
            $rows = 0;

            for ($i = 0; $i < count($secondLine); $i++) {
                foreach ($secondLine[$counter2] as $lines) {
                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'users.created_at', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($thirdLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd2 = $totalUsd2 + $result[$j]->usd;
                        $totalBtc2 = $totalBtc2 + $result[$j]->btc;
                        $totalEth2 = $totalEth2 + $result[$j]->eth;
                        $totalLtc2 = $totalLtc2 + $result[$j]->ltc;
                        $totalBch2 = $totalBch2 + $result[$j]->bch;
                        $totalXrp2 = $totalXrp2 + $result[$j]->xrp;
                        $totalDash2 = $totalDash2 + $result[$j]->dash;
                        $totalZec2 = $totalZec2 + $result[$j]->zec;
                    }
                    //$rows = $rows+$count2;
                }
                $counter2++;
            }
            $thirdLine = $thirdLine;
            $totalAmount2 = $totalUsd2 + $totalBtc2 * $bitcoinRate + $totalEth2 * $ethereumRate + $totalBch2 * $bitcashRate + $totalLtc2 * $litecoinRate + $totalXrp2 * $rippleRate + $totalDash2 * $dashRate + $totalZec2 * $zcashRate;;

            $thirdTotal = $totalAmount2; // $thirdTotal = $rows;
        }
        if (isset($thirdLine)) {
            $totalAmount3 = 0;
            $totalUsd3 = 0;
            $totalBtc3 = 0;
            $totalEth3 = 0;
            $totalBch3 = 0;
            $totalLtc3 = 0;
            $totalXrp3 = 0;
            $totalDash3 = 0;
            $totalZec3 = 0;
            $counter3 = 0;
            $rows1 = 0;//$count = count($thirdLine);
            for ($i = 0; $i < count($thirdLine); $i++) {
                foreach ($thirdLine[$counter3] as $lines) {

                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'users.created_at', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();

                    $count2 = count($result);

                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fourthLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd3 = $totalUsd3 + $result[$j]->usd;
                        $totalBtc3 = $totalBtc3 + $result[$j]->btc;
                        $totalEth3 = $totalEth3 + $result[$j]->eth;
                        $totalLtc3 = $totalLtc3 + $result[$j]->ltc;
                        $totalBch3 = $totalBch3 + $result[$j]->bch;
                        $totalXrp3 = $totalXrp3 + $result[$j]->xrp;
                        $totalDash3 = $totalDash3 + $result[$j]->dash;
                        $totalZec3 = $totalZec3 + $result[$j]->zec;
                    }
                    // $rows1 = $rows1+$count2;
                }
                $counter3++;
            }
            $fourthLine = $fourthLine;

            $totalAmount3 = $totalUsd3 + $totalBtc3 * $bitcoinRate + $totalEth3 * $ethereumRate + $totalBch3 * $bitcashRate + $totalLtc3 * $litecoinRate + $totalXrp3 * $rippleRate + $totalDash3 * $dashRate + $totalZec3 * $zcashRate;;

            $fourthTotal = $totalAmount3; //$fourthTotal  = $rows1;
        }

        if (isset($fourthLine)) {
            $totalAmount4 = 0;
            $totalUsd4 = 0;
            $totalBtc4 = 0;
            $totalEth4 = 0;
            $totalBch4 = 0;
            $totalLtc4 = 0;
            $totalXrp4 = 0;
            $totalDash4 = 0;
            $totalZec4 = 0;
            $counter4 = 0;
            $rows2 = 0;
            for ($i = 0; $i < count($fourthLine); $i++) {

                foreach ($fourthLine[$counter4] as $lines) {
                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'users.created_at', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fifthLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd4 = $totalUsd4 + $result[$j]->usd;
                        $totalBtc4 = $totalBtc4 + $result[$j]->btc;
                        $totalEth4 = $totalEth4 + $result[$j]->eth;
                        $totalLtc4 = $totalLtc4 + $result[$j]->ltc;
                        $totalBch4 = $totalBch4 + $result[$j]->bch;
                        $totalXrp4 = $totalXrp4 + $result[$j]->xrp;
                        $totalDash4 = $totalDash4 + $result[$j]->dash;
                        $totalZec4 = $totalZec4 + $result[$j]->zec;
                    }
                    // $rows2 = $rows2+$count2;
                }
                $counter4++;
            }
            $fifthLine = $fifthLine;
            $totalAmount4 = $totalUsd4 + $totalBtc4 * $bitcoinRate + $totalEth4 * $ethereumRate + $totalBch4 * $bitcashRate + $totalLtc4 * $litecoinRate + $totalXrp4 * $rippleRate + $totalDash4 * $dashRate + $totalZec4 * $zcashRate;;

            $fifthTotal = $totalAmount4; //  $fifthTotal = $rows2;
        }
        //echo "FirstLine - ".$firstTotal."SecondLine - ".$secondTotal. "ThirdLine - ".$thirdTotal."FourthLine - ".$fourthTotal."FifthLine - ".$fifthTotal;
        //exit;

        $total = 0;
        if ($level >= 1) {
            $total = $firstTotal;
        }
        if ($level >= 2) {
            $total = $total + $secondTotal;
        }
        if ($level >= 3) {
            $total = $total + $thirdTotal;
        }
        if ($level >= 4) {
            $total = $total + $fourthTotal;
        }
        if ($level >= 5) {
            $total = $total + $fifthTotal;
        }

        //$total = $firstTotal+$secondTotal+$thirdTotal+$fourthTotal+$fifthTotal;
        return $total;

    }

    static function plantolevel($plan)
    {
        if ($plan == 1) {
            return 3;
        }
        if ($plan == 2) {
            return 3;
        }

        if ($plan >= 3) {
            return 5;
        }
    }
    /////////////////##################//////////////


    /**
     * Execute the console command.
     *
     * @return mixed
     */

    static function planByinvestment($total_investment)
    {
        // Update  User Plans According to rules
        $plansQuery = DB::table('plans')->get();

        $plan_personal = 0;
        $plan_personal1 = 0;
        $plan_personal2 = 0;
        $plan_personal3 = 0;
        $plan_personal4 = 0;
        $plan_personal5 = 0;
        $plan_structural = 0;
        $plan_structural1 = 0;
        $plan_structural2 = 0;
        $plan_structural3 = 0;
        $plan_structural4 = 0;
        $plan_structural5 = 0;

        if (isset($plansQuery[0])) {
            $plan_personal = $plansQuery[0]->personel_investment_limit;
            $plan_structural = $plansQuery[0]->structural_investment_limit;
        }
        if (isset($plansQuery[1])) {
            $plan_personal1 = $plansQuery[1]->personel_investment_limit;
            $plan_structural1 = $plansQuery[1]->structural_investment_limit;
        }
        if (isset($plansQuery[2])) {
            $plan_personal2 = $plansQuery[2]->personel_investment_limit;
            $plan_structural2 = $plansQuery[2]->structural_investment_limit;
        }
        if (isset($plansQuery[3])) {
            $plan_personal3 = $plansQuery[3]->personel_investment_limit;
            $plan_structural3 = $plansQuery[3]->structural_investment_limit;
        }
        if (isset($plansQuery[4])) {
            $plan_personal4 = $plansQuery[4]->personel_investment_limit;
            $plan_structural4 = $plansQuery[4]->structural_investment_limit;
        }
        if (isset($plansQuery[5])) {
            $plan_personal5 = $plansQuery[5]->personel_investment_limit;
            $plan_structural5 = $plansQuery[5]->structural_investment_limit;
        }
        $plan = 1;

        if ($total_investment < $plan_personal1) {
            return 1;
        }

        if ($total_investment < $plan_personal2) {
            return 2;
        }

        if ($total_investment < $plan_personal3) {
            return 3;
        }

        if ($total_investment < $plan_personal4) {
            return 4;
        }

        if ($total_investment < $plan_personal5) {
            return 5;
        }

        return 6;

    }

    static function planByStructure($id)
    {
        // Update  User Plans According to rules
        $plansQuery = DB::table('plans')->get();

        $plan_personal = 0;
        $plan_personal1 = 0;
        $plan_personal2 = 0;
        $plan_personal3 = 0;
        $plan_personal4 = 0;
        $plan_personal5 = 0;
        $plan_structural = 0;
        $plan_structural1 = 0;
        $plan_structural2 = 0;
        $plan_structural3 = 0;
        $plan_structural4 = 0;
        $plan_structural5 = 0;

        if (isset($plansQuery[0])) {
            $plan_personal = $plansQuery[0]->personel_investment_limit;
            $plan_structural = $plansQuery[0]->structural_investment_limit;
        }
        if (isset($plansQuery[1])) {
            $plan_personal1 = $plansQuery[1]->personel_investment_limit;
            $plan_structural1 = $plansQuery[1]->structural_investment_limit;
        }
        if (isset($plansQuery[2])) {
            $plan_personal2 = $plansQuery[2]->personel_investment_limit;
            $plan_structural2 = $plansQuery[2]->structural_investment_limit;
        }
        if (isset($plansQuery[3])) {
            $plan_personal3 = $plansQuery[3]->personel_investment_limit;
            $plan_structural3 = $plansQuery[3]->structural_investment_limit;
        }
        if (isset($plansQuery[4])) {
            $plan_personal4 = $plansQuery[4]->personel_investment_limit;
            $plan_structural4 = $plansQuery[4]->structural_investment_limit;
        }
        if (isset($plansQuery[5])) {
            $plan_personal5 = $plansQuery[5]->personel_investment_limit;
            $plan_structural5 = $plansQuery[5]->structural_investment_limit;
        }

        //   $totalStructural = self::totalSubUsersInvestment($id,1);


        $totalStructural = self::totalSubUsersInvestment($id, 2);

        if ($totalStructural < $plan_structural1) {
            return 1;
        }

        $totalStructural = self::totalSubUsersInvestment($id, 3);

        if ($totalStructural < $plan_structural2) {
            return 2;
        }
        // $totalStructural = self::totalSubUsersInvestment($id,4);

        if ($totalStructural < $plan_structural3) {
            return 3;
        }
        // $totalStructural = self::totalSubUsersInvestment($id,5);

        if ($totalStructural < $plan_structural4) {
            return 4;
        }

        if ($totalStructural < $plan_structural5) {
            return 5;
        }

        return 6;


    }

    static function process($user)
    {

        return self::process2($user);
        /*
        $acc_active_investment = 0;

        $acc_active_investment = deposits::where('user_id', $user->id)->where('status', 'Approved')->sum('total_amount');

        $plan123 = 1;
        // Now Get latest Accounts Balances and rates and update Plans of Users

        $acc_bal_total = $acc_active_investment;


        $investmentPlan = self::planByinvestment($acc_bal_total);

        $structuralPlan = self::planByStructure($userid);

        $plan123 = $investmentPlan;
        if ($structuralPlan > $plan123) {
                $plan123 = $structuralPlan;
        }

        $updatePlan = users::where('id', $userid)->update(['plan' => $plan123]);


        Log::info('Plan updated For '.$user_uid.' On '.date('d-m-Y'). '(' . date('l') . ') and Current User Plan :' . $plan123);
        */

    }


    public function handle()
    {
        $todayDay = date('l');  // Monday, tuesday...
        $currentdate = date('d-m-Y');
        $disabled = 0;  /* Note:Flag  Disabled =0 for Active And  Disabled =1 for Not Active */
        $plan123 = 1;
        if (($todayDay != "Saturday" && $disabled == 0) || ($todayDay != "Sunday" && $disabled == 0)) {
            $emailFlag = 0;

            $usersInfo = DB::table('users')
                ->where('status', 'active')
                ->where('u_id', 'B4U0008192')
                ->orderby('id', 'desc')
                ->get();
            // Now Get latest Accounts Balances and rates and update Plans of Users
        //    $ratesQuery = DB::table('currecy_rates')->orderBy('created_at', 'desc')->first();
            $ratesQuery = current_rate::first();

            $lastProfitRate = $ratesQuery->today_profit;

            if (isset($lastProfitRate)) {

                foreach ($usersInfo as $user) {
                    self::process($user);
                }// End Foreach Loop
            }
        } else {
            //return redirect()->back()->with('errormsg', 'Rates not Find! Profit and Bonuses are not calculated.');
            echo "Cron not run on saturday and sunday";
            Log::info('Today date is ' . $currentdate . ' and day is :' . date('l') . ' User Plan :' . $plan123);
            Log::info('Cron Job Not Successful ,Today is ' . $todayDay . ' and cron not run on weekends.');
            //Log::write('Rates not Find! Profit and Bonuses are not calculated.')
        }
    }

    static public function totalSubUsersInvestment2($id, $plan = 1)
    {
        $level = self::plantolevel($plan);

        $data = DB::select("select sum(rs.active_new_investment) as amount from referrals r
        inner join referrals_investments_summation rs ON rs.child_id = r.child_id
        where parent_id = " . $id . " and r.level <= " . $level . "");
        return $data[0]->amount;
        //return Referral::where('parent_id', $id)->where('level', '<=', $level)->sum('active_new_investment');

        //dd($sum);
        //    return AttractiveFunds::totalAttractiveFundByLevelRecursiveSum($id,$level);
    }

    static function planByStructure2($id)
    {
        // Update  User Plans According to rules
        $plansQuery = DB::table('plans')->get();

        $plan_personal = 0;
        $plan_personal1 = 0;
        $plan_personal2 = 0;
        $plan_personal3 = 0;
        $plan_personal4 = 0;
        $plan_personal5 = 0;
        $plan_structural = 0;
        $plan_structural1 = 0;
        $plan_structural2 = 0;
        $plan_structural3 = 0;
        $plan_structural4 = 0;
        $plan_structural5 = 0;

        if (isset($plansQuery[0])) {
            $plan_personal = $plansQuery[0]->personel_investment_limit;
            $plan_structural = $plansQuery[0]->structural_investment_limit;
        }
        if (isset($plansQuery[1])) {
            $plan_personal1 = $plansQuery[1]->personel_investment_limit;
            $plan_structural1 = $plansQuery[1]->structural_investment_limit;
        }
        if (isset($plansQuery[2])) {
            $plan_personal2 = $plansQuery[2]->personel_investment_limit;
            $plan_structural2 = $plansQuery[2]->structural_investment_limit;
        }
        if (isset($plansQuery[3])) {
            $plan_personal3 = $plansQuery[3]->personel_investment_limit;
            $plan_structural3 = $plansQuery[3]->structural_investment_limit;
        }
        if (isset($plansQuery[4])) {
            $plan_personal4 = $plansQuery[4]->personel_investment_limit;
            $plan_structural4 = $plansQuery[4]->structural_investment_limit;
        }
        if (isset($plansQuery[5])) {
            $plan_personal5 = $plansQuery[5]->personel_investment_limit;
            $plan_structural5 = $plansQuery[5]->structural_investment_limit;
        }

        //   $totalStructural = self::totalSubUsersInvestment2($id,1);


        $totalStructural = self::totalSubUsersInvestment2($id, 2);

        if ($totalStructural < $plan_structural1) {
            return 1;
        }

        $totalStructural = self::totalSubUsersInvestment2($id, 3);

        if ($totalStructural < $plan_structural2) {
            return 2;
        }
        // $totalStructural = self::totalSubUsersInvestment2($id,4);

        if ($totalStructural < $plan_structural3) {
            return 3;
        }
        // $totalStructural = self::totalSubUsersInvestment2($id,5);

        if ($totalStructural < $plan_structural4) {
            return 4;
        }

        if ($totalStructural < $plan_structural5) {
            return 5;
        }

        return 6;


        //return $plan;
    }


    static function process2($user)
    {
        ######## Change made by Basit ###################
        $result = \Illuminate\Support\Facades\DB::select('Select decide_plan(?,?,?) as plan', [$user->id, 0, 0]);
        $plan = $result[0]->plan;

        if (isset($user->plan) && $user->plan != $plan) {
            $planType = "greater";
            if ($plan < $user->plan) {
                $planType = "smaller";
            }
            self::saveBonusHistory($user->id, $planType);
            \Illuminate\Support\Facades\DB::table('users')->where('id', $user->id)->update(['plan' => $plan]);
        }

    }

    static function saveBonusHistory($user_id, $planType)
    {
        try {

            //$user_id			=  $user_id;
            $userDetails = \App\users::where('id', $user_id)->first();
            $userUID = $userDetails->u_id;
            $userPlan = $userDetails->plan;

            $planDetails = \App\plans::where('id', $userPlan)->first();
            $planName = $planDetails->name;

            $maxfirstTotal = \App\Model\AttractiveFunds::totalMaxAttractiveFundByLevel($user_id, 1);
            $maxsecondTotal = \App\Model\AttractiveFunds::totalMaxAttractiveFundByLevel($user_id, 2);
            $maxthirdTotal = \App\Model\AttractiveFunds::totalMaxAttractiveFundByLevel($user_id, 3);
            $maxfourthTotal = \App\Model\AttractiveFunds::totalMaxAttractiveFundByLevel($user_id, 4);
            $maxfifthTotal = \App\Model\AttractiveFunds::totalMaxAttractiveFundByLevel($user_id, 5);

            $bonusPercentage = \App\ref_investment_bonus_rules::where('id', $userPlan)->first();

            $first_line = $bonusPercentage->first_line;
            $second_line = $bonusPercentage->second_line;
            $third_line = $bonusPercentage->third_line;
            $fourth_line = $bonusPercentage->fourth_line;
            $fifth_line = $bonusPercentage->fifth_line;

            $oldBonusTotal = 0;
            $current_level_bonus = 0;
            $newfirstTotal = 0;
            $newsecondTotal = 0;
            $newthirdTotal = 0;
            $newfourthTotal = 0;
            $newfifthTotal = 0;
            $currentBonusFirst = 0;
            $currentBonusSecond = 0;
            $currentBonusThird = 0;
            $currentBonusFourth = 0;
            $currentBonusFifth = 0;

            $oldBonusDetails = bonus_history::where('user_id', $user_id)->orderBy('created_at', 'DESC')->first();

            if (isset($oldBonusDetails)) {

                $maxlevel1 = $oldBonusDetails->maxlevel1;
                $maxlevel2 = $oldBonusDetails->maxlevel2;
                $maxlevel3 = $oldBonusDetails->maxlevel3;
                $maxlevel4 = $oldBonusDetails->maxlevel4;
                $maxlevel5 = $oldBonusDetails->maxlevel5;

                $current_level_bonus = $oldBonusDetails->current_level_bonus;
                $oldBonusTotal = $oldBonusDetails->bonus_total;

                /*
                $current_max1 			= $oldBonusDetails->current_max1;
                $current_max2 			= $oldBonusDetails->current_max2;
                $current_max3 			= $oldBonusDetails->current_max3;
                $current_max4 			= $oldBonusDetails->current_max4;
                $current_max5 			= $oldBonusDetails->current_max5;
                */

                if ($planType != "smaller") {

                    $newfirstTotal = floatval($maxfirstTotal) - floatval($maxlevel1);
                    $newsecondTotal = floatval($maxsecondTotal) - floatval($maxlevel2);
                    $newthirdTotal = floatval($maxthirdTotal) - floatval($maxlevel3);
                    $newfourthTotal = floatval($maxfourthTotal) - floatval($maxlevel4);
                    $newfifthTotal = floatval($maxfifthTotal) - floatval($maxlevel5);


                    $currentBonusFirst = (floatval($newfirstTotal) * floatval($first_line)) / 100;
                    $currentBonusSecond = (floatval($newsecondTotal) * floatval($second_line)) / 100;
                    $currentBonusThird = (floatval($newthirdTotal) * floatval($third_line)) / 100;
                    $currentBonusFourth = (floatval($newfourthTotal) * floatval($fourth_line)) / 100;
                    $currentBonusFifth = (floatval($newfifthTotal) * floatval($fifth_line)) / 100;

                }
            } else {

                $currentBonusFirst = (floatval($maxfirstTotal) * floatval($first_line)) / 100;
                $currentBonusSecond = (floatval($maxsecondTotal) * floatval($second_line)) / 100;
                $currentBonusThird = (floatval($maxthirdTotal) * floatval($third_line)) / 100;
                $currentBonusFourth = (floatval($maxfourthTotal) * floatval($fourth_line)) / 100;
                $currentBonusFifth = (floatval($maxfifthTotal) * floatval($fifth_line)) / 100;

            }
            $current_level_bonus = $currentBonusFirst + $currentBonusSecond + $currentBonusThird;
            if ($userPlan >= '3') {
                $current_level_bonus = $current_level_bonus + $currentBonusFourth + $currentBonusFifth;
            }
            $finalBonusTotal = $current_level_bonus + $oldBonusTotal;

            $bonusHistory = new bonus_history();
            $bonusHistory->user_id = $user_id;
            $bonusHistory->unique_id = $userUID;
            $bonusHistory->plan_id = $userPlan;
            $bonusHistory->plan_name = $planName;
            $bonusHistory->maxlevel1 = $maxfirstTotal;
            $bonusHistory->maxlevel2 = $maxsecondTotal;
            $bonusHistory->maxlevel3 = $maxthirdTotal;
            $bonusHistory->current_max1 = $newfirstTotal;
            $bonusHistory->current_max2 = $newsecondTotal;
            $bonusHistory->current_max3 = $newthirdTotal;
            if ($userPlan >= '3') {

                $bonusHistory->maxlevel4 = $maxfourthTotal;
                $bonusHistory->maxlevel5 = $maxfifthTotal;
                $bonusHistory->current_max4 = $newfourthTotal;
                $bonusHistory->current_max5 = $newfifthTotal;
            }
            $bonusHistory->bonus_total = $finalBonusTotal;
            $bonusHistory->current_level_bonus = $current_level_bonus;
            $bonusHistory->save();
        } catch (\Exception $ex) {
            //echo $ex->getMessage();
        }

        //dd(\DB::getQueryLog());
    }

}

//Run this command in server to execute live cron job  =>   cd /home/b4uinvestors/public_html && php artisan schedule:run >> null 2>&1
