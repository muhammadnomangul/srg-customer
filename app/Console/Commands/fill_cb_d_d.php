<?php

namespace App\Console\Commands;

use App\Model\Deposit;
use Illuminate\Console\Command;

class fill_cb_d_d extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill_cb_d_d';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $data =  \DB::select("SELECT cb_debits.id as cb_debits_id , deposits.id as deposit_id FROM cb_debits,deposits WHERE cb_debits.deposit_id = deposits.id and deposits.cb_debit_id is null ORDER BY `deposit_id` ASC ");
        foreach($data as $d){
            Deposit::where('id',$d->deposit_id)->update(['cb_debit_id'=>$d->cb_debits_id]);
            $this->info(json_encode($d->deposit_id));

        }
        return 0;
    }
}
