<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBCredits;
use App\User;
use Illuminate\Console\Command;

class CreateCreditOfPromotAchiver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:credit:promo:achiever';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Promo Achievers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $allFiles = \Illuminate\Support\Facades\File::allFiles(storage_path('promo_achiver/in_doing_temp_credit_amount_list/newList'));
            foreach ($allFiles as $file) {

                $filePath = $file->getPathname();
                $dataList = array_map('str_getcsv', file($filePath));

                foreach ($dataList as $data) {
                    if (!empty($data[0])) {
                        $user = User::where('u_id', $data[0])->first();
                        if ($user instanceof User) {
                            $cbAccount = CBAccounts::where('currencies_id',1)->whereUserId($user->id)->first();
                            if(!$cbAccount){
                                $cbAccount = CBAccounts::makeCBAccount(1,$user->id);
                            }
                            /** @var User $user */
                            CBCredits::createCredit($cbAccount->id,25000,0,'balance','reward','Car Achiever','approved');
                            $this->info(' Creating Cash BOx Credit of User having ID ' . $data[0]);
                        } else {
                            $this->error('B4u id is invalid ' . $data[0]);
                        }

                    } else {
                        $this->error('B4U id is missing.. ');
                    }

                }


            }
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
