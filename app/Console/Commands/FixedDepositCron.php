<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\deposits;
use App\FixDepositHistory;
use App\UserAccounts;
use Exception,Log,DB;
use Carbon\Carbon;
class FixedDepositCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixed_deposit:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This cron will get the fixed deposit from history table and then check if created at difference is about on month and end date less then current date';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            Log::info("Fixed deposit crone running");
            //  \DB::enableQueryLog();
            // ->addMonthsNoOverflow(1)
            $list = FixDepositHistory::where("is_done",0)->get();
            // Log::info($list);
            if($list){
                foreach($list as $val){
                    $created_date = Carbon::parse($val->created_at)->format('Y-m-d');
                    $enddate = Carbon::parse($val->end_date)->format('Y-m-d');
                    $now = Carbon::now();
                    $interval = $now->diffInYears($val->created_at);
                    // Log::info($interval);
                    //  Log::info(Carbon::today()->toDateString());
                    if($enddate >= $created_date && $interval == 1)
                   {
                    $amount_total = $val->deposit->total_amount;
                    $profit_total = $val->deposit->profit_total;
                     if($profit_total > 500)
                         $deduct = 15;
                     else
                         $deduct = 5;
                     //add into history table
                     $history = new FixDepositHistory();
                     $history->deposit_id = $val->deposit_id;
                     $history->user_id = $val->user_id;
                     $history->amount = $amount_total;
                     $history->profit = $profit_total;
                     $history->deduction = $deduct;
                     $history->new_amount = ($amount_total+($profit_total-$deduct));
                     $history->plan_id = $val->plan_id;
                     $history->end_date = $val->end_date;
                     $history->save();
 
                     // update history done
                     $val->is_done = 1;
                     $val->save();
 
                     //update deposit table
                     $depo_obj = deposits::find($val->deposit_id);
                     $depo_obj->update( ['total_amount' => $history->new_amount,'latest_profit' => 0,'trade_profit' => 0,'profit_total' => 0,'latest_crypto_profit' => 0,'crypto_profit' => 0,'crypto_profit_total' => 0,'new_total' => 0]);
                   }
                   elseif($enddate < $created_date){
                       // update history done
                        $val->is_done = 1;
                        $val->save();
                        //update deposit table
                        // $soldAt = date("Y-m-d");
                        $depo_obj = deposits::find($val->deposit_id);
                        $depo_obj->update(['profit_take' => 2]);
                        Log::info("profit take 2 set for ".$val->deposit_id);
                    }
                   
                }
            }
        }catch(Exception $e)
        {
            Log::error("Something went wrong in crone FixedDepositCron ".$e);
        }
      
    }
}
