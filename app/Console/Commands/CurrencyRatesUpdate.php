<?php

namespace App\Console\Commands;

use App\current_rate;
use App\settings;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CurrencyRatesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currRate:Update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    /*public function handle()
    {
        $apiGetRates = "https://ewallet.b4uwallet.com/api/v2/peatio/public/markets/tickers";
        $file_headers = @get_headers($apiGetRates);
        if (isset($file_headers)) {
            if (isset($file_headers[0]) && ('HTTP/1.1 404 Not Found' == $file_headers[0] || 'HTTP/1.0 404 Not Found' == $file_headers[0])) {
                $msg = "Rate not founded";
            } else {
                $urlRate = 'https://ewallet.b4uwallet.com/api/v2/peatio/public/markets/tickers';
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $urlRate);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($curl);
                $data_main = json_decode($response);
                $this->info('responseOfRequest' . $response);
                curl_close($curl);
                $currentRate = current_rate::first();
                $threshold = settings::first();
                $update = 0;
                if ($response) {
                    $apiRSC = $data_main->rscusd->ticker->last;
                    $RSC = $currentRate->rate_rsc;
                    if (($apiRSC - $RSC >= $threshold->rsc_threshold) || ($RSC - $apiRSC >= $threshold->rsc_threshold)) {
                        $RSC = $apiRSC;
                        $update = 1;
                    }
                    $apiBTC = $data_main->btcusd->ticker->last;
                    $BTC = $currentRate->rate_btc;
                    if (($apiBTC - $BTC >= $threshold->btc_threshold) || ($BTC - $apiBTC >= $threshold->btc_threshold)) {
                        $BTC = $apiBTC;
                        $update = 1;
                    }
                    $apiLTC = $data_main->ltcusd->ticker->last;
                    $LTC = $currentRate->rate_ltc;
                    if (($apiLTC - $LTC >= $threshold->ltc_threshold) || ($LTC - $apiLTC >= $threshold->ltc_threshold)) {
                        $LTC = $apiLTC;
                        $update = 1;
                    }
                    $apiXRP = $data_main->xrpusd->ticker->last;
                    $XRP = $currentRate->rate_xrp;
                    if (($apiXRP - $XRP >= $threshold->xrp_threshold) || ($XRP - $apiXRP >= $threshold->xrp_threshold)) {
                        $XRP = $apiXRP;
                        $update = 1;
                    }
                    $apiDASH = $data_main->dashusd->ticker->last;
                    $DASH = $currentRate->rate_dash;
                    if (($apiDASH - $DASH >= $threshold->dash_threshold) || ($DASH - $apiDASH >= $threshold->dash_threshold)) {
                        $DASH = $apiDASH;
                        $update = 1;
                    }
                    $apiETH = $data_main->ethusd->ticker->last;
                    $ETH = $currentRate->rate_eth;
                    if (($apiETH - $ETH >= $threshold->eth_threshold) || ($ETH - $apiETH >= $threshold->eth_threshold)) {
                        $ETH = $apiETH;
                        $update = 1;
                    }
                    $apiZEC = $data_main->zecusd->ticker->last;
                    $ZEC = $currentRate->rate_zec;
                    if (($apiZEC - $ZEC >= $threshold->zec_threshold) || ($ZEC - $apiZEC >= $threshold->zec_threshold)) {
                        $ZEC = $apiZEC;
                        $update = 1;
                        $this->info('zec rate should be change');
                    }
                    $apiBCH = $data_main->bchusd->ticker->last;
                    $BCH = $currentRate->rate_bch;
                    if (($apiBCH - $BCH >= $threshold->bch_threshold) || ($BCH - $apiBCH >= $threshold->bch_threshold)) {
                        $BCH = $apiBCH;
                        $update = 1;
                    }
                    $msg = "Success";
                    $this->info('update value is ' . $update);
                    $this->info('ZEC new vaule is  ' . $ZEC);
                    if ($update == 1) {
                        $currentRate->rate_btc = $BTC;
                        $currentRate->rate_eth = $ETH;
                        $currentRate->rate_ltc = $LTC;
                        $currentRate->rate_bch = $BCH;
                        $currentRate->rate_xrp = $XRP;
                        $currentRate->rate_dash = $DASH;
                        $currentRate->rate_zec = $ZEC;
                        $currentRate->rate_rsc = $RSC;
                        $customResponseValueCheck = $currentRate->save();
                        $this->info('currency-rates-has-been-updated ' . $customResponseValueCheck);
                        Cache::pull('lastCurrencyRate');
                        Cache::pull('lastCurrentRate');
                        $msg = "Updated";
                    }
                } else {
                    $msg = "No Data Founded";
                }
            }
        } else {
            $msg = "Invalid Api Request";
        }
        app('App\Http\Controllers\Controller')->adminLogs('', '', '', 'CurrencyRates_' . $msg);
        echo $msg;
    }*/
    public function handle()
    {
        $urlRate = "https://api.coinpaprika.com/v1/tickers";
        $file_headers = @get_headers($urlRate);
        if (isset($file_headers)) {
            if (isset($file_headers[0]) && ('HTTP/1.1 404 Not Found' == $file_headers[0] || 'HTTP/1.0 404 Not Found' == $file_headers[0])) {
                $msg = "Rate not founded";
            } else {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $urlRate);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($curl);
                $data_main = json_decode($response);
            //    $urlRateRSC = "https://api.coinpaprika.com/v1/tickers/rsc-rscoin";
                $urlRateRSC = 'https://ewallet.b4uwallet.com/api/v2/peatio/public/markets/tickers';
                $curlRSC = curl_init();
                curl_setopt($curlRSC, CURLOPT_URL, $urlRateRSC);
                curl_setopt($curlRSC, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curlRSC, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curlRSC, CURLOPT_SSL_VERIFYPEER, false);
                $responseRSC = curl_exec($curlRSC);
                $data_mainRSC = json_decode($responseRSC);
                ## Check for array number
                $currArray = ['BTC','ETH','BCH','LTC','XRP','DASH','ZEC'];
                $currArrayKey = [];
                foreach ($data_main as $key => $value){
                    if(in_array($value->symbol ,$currArray)){
                        $currArrayKey[$value->symbol] = $key;
                    }
                }
                curl_close($curl);
                curl_close($curlRSC);
                $currentRate = current_rate::first();
                $threshold = settings::first();
                $update = 0;
                if ($response) {
                  //  $apiRSC = $data_mainRSC->quotes->USD->price;
                    $apiRSC = $data_mainRSC->rscusd->ticker->last;
                    $RSC = $currentRate->rate_rsc;
                    if (($apiRSC - $RSC >= $threshold->rsc_threshold) || ($RSC - $apiRSC >= $threshold->rsc_threshold)) {
                        $RSC = $apiRSC;
                        $update = 1;
                    }
                    $apiBTC = $data_main[$currArrayKey['BTC']]->quotes->USD->price;
                    $BTC = $currentRate->rate_btc;
                    if (($apiBTC - $BTC >= $threshold->btc_threshold) || ($BTC - $apiBTC >= $threshold->btc_threshold)) {
                        $BTC = $apiBTC;
                        $update = 1;
                    }
                    $apiLTC = $data_main[$currArrayKey['LTC']]->quotes->USD->price;
                    $LTC = $currentRate->rate_ltc;
                    if (($apiLTC - $LTC >= $threshold->ltc_threshold) || ($LTC - $apiLTC >= $threshold->ltc_threshold)) {
                        $LTC = $apiLTC;
                        $update = 1;
                    }
                    $apiXRP = $data_main[$currArrayKey['XRP']]->quotes->USD->price;
                    $XRP = $currentRate->rate_xrp;
                    if (($apiXRP - $XRP >= $threshold->xrp_threshold) || ($XRP - $apiXRP >= $threshold->xrp_threshold)) {
                        $XRP = $apiXRP;
                        $update = 1;
                    }
                    $apiDASH = $data_main[$currArrayKey['DASH']]->quotes->USD->price;
                    $DASH = $currentRate->rate_dash;
                    if (($apiDASH - $DASH >= $threshold->dash_threshold) || ($DASH - $apiDASH >= $threshold->dash_threshold)) {
                        $DASH = $apiDASH;
                        $update = 1;
                    }
                    $apiETH = $data_main[$currArrayKey['ETH']]->quotes->USD->price;
                    $ETH = $currentRate->rate_eth;
                    if (($apiETH - $ETH >= $threshold->eth_threshold) || ($ETH - $apiETH >= $threshold->eth_threshold)) {
                        $ETH = $apiETH;
                        $update = 1;
                    }
                    $apiZEC = $data_main[$currArrayKey['ZEC']]->quotes->USD->price;
                    $ZEC = $currentRate->rate_zec;
                    if (($apiZEC - $ZEC >= $threshold->zec_threshold) || ($ZEC - $apiZEC >= $threshold->zec_threshold)) {
                        $ZEC = $apiZEC;
                        $update = 1;
                    }
                    $apiBCH = $data_main[$currArrayKey['BCH']]->quotes->USD->price;
                    $BCH = $currentRate->rate_bch;
                    if (($apiBCH - $BCH >= $threshold->bch_threshold) || ($BCH - $apiBCH >= $threshold->bch_threshold)) {
                        $BCH = $apiBCH;
                        $update = 1;
                    }
                    $msg = "Success";
                    if ($update == 1) {
                        $currentRate->rate_btc = $BTC;
                        $currentRate->rate_eth = $ETH;
                        $currentRate->rate_ltc = $LTC;
                        $currentRate->rate_bch = $BCH;
                        $currentRate->rate_xrp = $XRP;
                        $currentRate->rate_dash = $DASH;
                        $currentRate->rate_zec = $ZEC;
                        $currentRate->rate_rsc = $RSC;
                        $currentRate->save();
                        Cache::pull('lastCurrencyRate');
                        Cache::pull('lastCurrentRate');
                        $msg = "Updated";
                    }
                } else {
                    $msg = "No Data Founded";
                }
            }
        } else {
            $msg = "Invalid Api Request";
        }
        app('App\Http\Controllers\Controller')->adminLogs('', '', '', 'CurrencyRates_' . $msg);
        echo $msg;
    }
}
