<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use Illuminate\Console\Command;

class ETCWork extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etc:work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $CBAccounts = CBAccounts::where('current_balance', '<', '0')->get();

            foreach ($CBAccounts as $CBAccount) {
                $CBDebits = CBDebits::where('cb_account_id', $CBAccount->id);
                $CBCredits = CBCredits::where('cb_account_id', $CBAccount->id);
                if ($CBDebits->count() == 0 && $CBCredits->count() == 0) {
                    $CBAccount->current_balance = 0;
                    $CBAccount->locked_balance = 0;
                    $CBAccount->save();
                    $this->info('cashbox account id is :: '. $CBAccount->id);
                }
            }

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
