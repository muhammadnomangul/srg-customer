<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;


class Reverse extends Command
{
    /**
     * This command/Queue is not implemented yet.
     *
     * @var string
     */
    protected $signature = 'reverse:dailyProfit';

    /**
     * The console command description. 
     *
     * @var string
     */
    protected $description = 'Reverse Daily Profit if some user got it more than one time per day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $today          = date('l');
            // $currentDate    = date('Y-m-d');
            $currentDate        = "2020-02-10";
            $currentDateCron    = $currentDate . " 00:00:00";

            $currencyRate = DB::table('currency_rates')->where('created_at', $currentDateCron)->orderby('created_at', 'DESC')->first();
            
            if (isset($currencyRate)) {
                // Fire mail to admin to start cron
                // Add into logs

                // query all duplicate records of same date  from daily_profit_bonus
                $dailyDoubleProfits = DB::table('daily_profit_bonus')
                    ->select('*')
                    ->where('created_at', '=', $currentDateCron)
                    ->get()
                    ->first();

                // $deposits = DB::table('deposits')->where('status','Approved')->where('approved_at','<=',$currentDateCron)->orderby('created_at','DESC')->get();

                dd($dailyDoubleProfits);
            } else {
                // Print Logs
                $this->info('Excahnge Rates not Find! Profit and Bonuses are not calculated.');
                Log::info('Cron Job Not Successful ,Rates not Find! Profit and Bonuses are not calculated.');
                Log::info('Today date is ' . date('d-m-Y h:i:s') . ' and day is :' . date('l'));
                //Log::write('Rates not Find! Profit and Bonuses are not calculated.')
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            $errorMsg       = $ex->getMessage();

            // Log::info('Today date is '.date('d-m-Y h:i:s').' and Reverse Daily Profit not successful, Due to '.$errorMsg);
            // $emailto            = "mubixhb4u@gmail.com";
            // $emailto2           = "dev.zeemehmood@gmail.com";
            // $sendEmails3        =  $this->sendEmailToMe($emailto,$lastProfitRate,$currentCronDate,$errorMsg);
            // $sendEmailsToXee3   =  $this->sendEmailToMe($emailto2,$lastProfitRate,$currentCronDate,$errorMsg);
        }
    }
}
