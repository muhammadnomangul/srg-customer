<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class GenerateReferralTree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:tree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Referral Deposit and add missing Referral Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
           
            $users = User::where('id', '>', 118252)->orderBy('id', 'DESC')->get();

            foreach ($users as $key=>$user) {
                $result = DB::table('referrals')->where('child_id', $user->id)->get();
                
                if (!$result->isEmpty()) {
                    $collectio_array = $result->pluck('parent_u_id')->toArray();
                
                    if (in_array("B4U0001", $collectio_array)) {
                        $this->info("Admin is parent of:".$user->u_id. '|'.$user->id);
                    } elseif (count($result) >= 5) {
                        $this->info($user->u_id.'|'.$user->id." has Complete Tree");
                    } else {
                        DB::select('CALL  generate_referral_tree('.$user->id.',0,Null)');
                        $this->info("Referral tree is Updated for:".$user->u_id. '|'.$user->id);
                    }
                } else {
                    DB::select('CALL  generate_referral_tree('.$user->id.',0,Null)');
                    $this->info("New Referral tree is added for:".$user->u_id. '|'.$user->id);
                }
            }
        } catch (\Exception $ex) {
            $errorMsg = $ex->getMessage();
            \Log::info('Today date is ' . date('d-m-Y h:i:s') . ' and Generate Referral cron is not successful, Due to ' . $errorMsg);
        }
    }
}
