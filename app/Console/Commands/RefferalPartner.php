<?php

namespace App\Console\Commands;

use App\bonus_history;
use App\daily_investment_bonus;
use App\deposits;
use App\Model\AttractiveFunds;
use App\Model\Referral;
use App\ref_investment_bonus_rules;
use App\User;
use App\withdrawals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RefferalPartner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refferal:partners {UserId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $user_id = $this->argument('UserId');
        \Auth::loginUsingId($user_id);
        $user = User::where('id',$user_id)->first();
        $loged_user_id = $user->id;
        $loged_parent_id = $user->parent_id;
        $Loged_user_uid = $user->u_id;
        $Loged_user_plan = $user->plan;

        $bonusPercentage = ref_investment_bonus_rules::where('id', $Loged_user_plan)->first();
        $first_line = $bonusPercentage->first_line;
        $second_line = $bonusPercentage->second_line;
        $third_line = $bonusPercentage->third_line;
        $fourth_line = $bonusPercentage->fourth_line;
        $fifth_line = $bonusPercentage->fifth_line;
        $title = 'All Partners Info';
        $bonushistory = bonus_history::where('user_id', $loged_user_id)->orderby('created_at', 'DESC')->first();

        $firstTotal = AttractiveFunds::totalAttractiveFundByLevel($loged_user_id, 1);
        $secondTotal = AttractiveFunds::totalAttractiveFundByLevel($loged_user_id, 2);
        $thirdTotal = AttractiveFunds::totalAttractiveFundByLevel($loged_user_id, 3);
        $fourthTotal = AttractiveFunds::totalAttractiveFundByLevel($loged_user_id, 4);
        $fifthTotal = AttractiveFunds::totalAttractiveFundByLevel($loged_user_id, 5);

        $bonuswithdrawalQuery = withdrawals::select('usd_amount', 'donation')
            ->where('user', $loged_user_id)
            ->where('payment_mode', 'Bonus')
            ->where(
                function ($query) {
                    $query->where('status', 'Approved')
                        ->orWhere('status', 'Pending');
                }
            );

        $bonuswithdrawal = $bonuswithdrawalQuery->sum('usd_amount');
        $bonuswithdrawaldonation = $bonuswithdrawalQuery->sum('donation');

        $bonuswithdrawal = $bonuswithdrawal + $bonuswithdrawaldonation;

        $bonusreinvestment = deposits::select('total_amount')
            ->where('user_id', $loged_user_id)
            ->where('trans_type', 'Reinvestment')
            ->where('reinvest_type', 'Bonus')
            ->where(
                function ($query) {
                    $query->where('status', 'Approved')
                        ->orWhere('status', 'Sold');
                }
            )->sum('total_amount');

        // Profit Bonus
        $profitbonus = daily_investment_bonus::select('bonus')
            ->where('parent_id', $Loged_user_uid)
            ->where('details', 'LIKE', 'Profit Bonus')
            ->sum('bonus');

        $totalAttractiveFund = $firstTotal + $secondTotal + $thirdTotal;
        if ($Loged_user_plan >= 3) {
            $totalAttractiveFund = $totalAttractiveFund + $fourthTotal + $fifthTotal;
        }

        $maxTotal = 0;
        $maxfirstTotal = 0;
        $maxsecondTotal = 0;
        $maxthirdTotal = 0;
        $maxfourthTotal = 0;
        $maxfifthTotal = 0;
        $bonusTotal = 0;
        $bonusFirst = 0;
        $bonusSecond = 0;
        $bonusThird = 0;
        $bonusFourth = 0;
        $bonusFifth = 0;
        $finaltotalbonus = 0;
        $oldBonusTotal = 0;
        $current_level_bonus = 0;
        $newfirstTotal = 0;
        $newsecondTotal = 0;
        $newthirdTotal = 0;
        $newfourthTotal = 0;
        $newfifthTotal = 0;

        $bonusTotal = $bonusTotal + $oldBonusTotal;
        $finaltotalbonus = $profitbonus + $bonusTotal - ($bonuswithdrawal + $bonusreinvestment);

        // Get Referrals Level Wise
        $levelReferrals = [];

        for ($level = 1; $level <= 5; ++$level) {
            if ($level <= 3 || $Loged_user_plan >= 3) {
                $levelReferrals[$level] = Referral::referralByLevel($loged_user_id, $level);
            }
        }

        // Get deposit list levelwise
        $levelReferralDeposit = [];
        for ($level = 1; $level <= 5; ++$level) {
            if ($level <= 3 || $Loged_user_plan >= 3) {
                $child_user = null;
                foreach ($levelReferrals[$level] as $referraldepo) {
                    $child_user[] = $referraldepo->child_id;
                }

                $child_users[$level] = $child_user;

                if (null == $child_user) {
                    $child_users[$level] = [];
                }

                $child_user = null;

                $levelReferralDeposit[$level] = deposits::getActiveInvestmentList($child_users[$level]);
            }
        }
        $BlueMoonPlanChildsList = Referral::BlueMoonPlanChilds($loged_user_id);
        $SilverRankChildsList = Referral::SilverRankChilds($loged_user_id);
        $AuroraPlanChildsList = Referral::AuroraPlanChilds($loged_user_id);
        $CoordinatorRankChildsList = Referral::CoordinatorRankChilds($loged_user_id);
        $CullinanPlanChildsList = Referral::CullinanPlanChilds($loged_user_id);
        $DiamondRankChildsList = Referral::DiamondRankChilds($loged_user_id);


//        $data = [
//
//            'levelReferrals' => $levelReferrals,
//            'levelReferralDeposit'=> $levelReferralDeposit,
//            'maxTotal'=> $maxTotal,
//            'maxfifthTotal'=> $maxfifthTotal,
//            'maxfourthTotal'=> $maxfourthTotal,
//            'maxthirdTotal'=> $maxthirdTotal,
//            'maxsecondTotal'=> $maxsecondTotal,
//            'maxfirstTotal'=> $maxfirstTotal,
//            'firstTotal'=> $firstTotal,
//            'secondTotal'=> $secondTotal,
//            'thirdTotal'=> $thirdTotal,
//            'fourthTotal'=> $fourthTotal,
//            'fifthTotal'=> $fifthTotal,
//            'totalAttractiveFund'=> $totalAttractiveFund,
//            'bonusFirst'=> $bonusFirst,
//            'bonusSecond'=> $bonusSecond,
//            'bonusThird'=> $bonusThird,
//            'bonusFourth'=> $bonusFourth,
//            'bonusFifth'=> $bonusFifth,
//            'bonusTotal'=> $bonusTotal,
//            'bonuswithdrawal'=> $bonuswithdrawal,
//            'bonusreinvestment'=> $bonusreinvestment,
//            'finaltotalbonus'=> $finaltotalbonus,
//            'profitbonus'=> $profitbonus,
//            'bonushistory'=> $bonushistory,
//            'BlueMoonPlanChildsList'=> $BlueMoonPlanChildsList,
//            'SilverRankChildsList'=> $SilverRankChildsList,
//            'AuroraPlanChildsList'=> $AuroraPlanChildsList,
//            'CoordinatorRankChildsList'=> $CoordinatorRankChildsList,
//            'CullinanPlanChildsList'=> $CullinanPlanChildsList,
//            'DiamondRankChildsList'=> $DiamondRankChildsList
//
//        ];
        $data =  view('user.partners._after')->with([

            'levelReferrals' => $levelReferrals,
            'levelReferralDeposit'=> $levelReferralDeposit,
            'maxTotal'=> $maxTotal,
            'maxfifthTotal'=> $maxfifthTotal,
            'maxfourthTotal'=> $maxfourthTotal,
            'maxthirdTotal'=> $maxthirdTotal,
            'maxsecondTotal'=> $maxsecondTotal,
            'maxfirstTotal'=> $maxfirstTotal,
            'firstTotal'=> $firstTotal,
            'secondTotal'=> $secondTotal,
            'thirdTotal'=> $thirdTotal,
            'fourthTotal'=> $fourthTotal,
            'fifthTotal'=> $fifthTotal,
            'totalAttractiveFund'=> $totalAttractiveFund,
            'bonusFirst'=> $bonusFirst,
            'bonusSecond'=> $bonusSecond,
            'bonusThird'=> $bonusThird,
            'bonusFourth'=> $bonusFourth,
            'bonusFifth'=> $bonusFifth,
            'bonusTotal'=> $bonusTotal,
            'bonuswithdrawal'=> $bonuswithdrawal,
            'bonusreinvestment'=> $bonusreinvestment,
            'finaltotalbonus'=> $finaltotalbonus,
            'profitbonus'=> $profitbonus,
            'bonushistory'=> $bonushistory,
            'BlueMoonPlanChildsList'=> $BlueMoonPlanChildsList,
            'SilverRankChildsList'=> $SilverRankChildsList,
            'AuroraPlanChildsList'=> $AuroraPlanChildsList,
            'CoordinatorRankChildsList'=> $CoordinatorRankChildsList,
            'CullinanPlanChildsList'=> $CullinanPlanChildsList,
            'DiamondRankChildsList'=> $DiamondRankChildsList

        ]);

        Cache::put('refferalPartners'.$loged_user_id, "".$data);
        $this->info('Data Loaded Successfully !!');
//        return 0;
    }
}
