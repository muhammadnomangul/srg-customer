<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
//use Illuminate\Support\Facades\Mail as Email;
//use illuminate\contract\Mailer;
//use App\User;
use App\users;
use App\deposits;
use App\UserAccounts;
use App\currency_rates;
use App\daily_investment_bonus;
use App\daily_profit_bonus;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use App\Event;
use DB;
use DateTime;
use Mail;
use Stripe\Plan;

class ProfitCalculationsCronBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profit2:day2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate daily profit,distribute bonuses.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
		set_time_limit(0);
		
		
    }

	
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		try{
			
			$todayDay 			= date('l');
			$previousDate 		= date('Y-m-d',strtotime("-1 days"));
			$currentCronDate 	= date('Y-m-d',strtotime("-2 days"));
			//$currentCronDate 	= date('Y-m-d');
			$currentDateCron 	= $currentCronDate." 00:00:00";
			//Update Exchange Rates
			$this->updateExchangeRates($currentDateCron);
				
			$rquery 		= DB::table('currency_rates')->orderby('created_at','DESC')->first();
				
			if(isset($rquery))
			{
					$lastProfitRate 	=  $rquery->today_profit;
			    	//	$lastProfitRate 	=  '0.3';
					
					
					//Update Events Table
				/*	$currentDate 		=  date("Y-m-d");
					$equery 			=  new Event();
					$equery->title      =  $lastProfitRate."%";
					$equery->start_date =  $currentDate;
					$equery->end_date   =  $currentDate;
					$equery->save();*/
                    $emailto            = getDevelopersMailAddresses();
					$start 				= "Started";
					//$sendEmails  = sendEmailToDeveloper($emailto,$lastProfitRate,$currentCronDate,$start);
				
					//######## GET ALL TRADES AND CALCULATE PROFIT  ##########
					$this->info('Today date is '.date('d/m/Y h:i:s').' and today Profitcalculation Percentage : '.$lastProfitRate);
					Log::info('Today date is '.date('d-m-Y h:i:s').' and today Profitcalculation Percentage : '.$lastProfitRate);
					$emailFlag = 0;
					$deposits = DB::table('deposits')->where('deposits.status','Approved')->where('id','15546')->orwhere('id','16298')->get();
					
					//$currentDateCron = $currentCronDate." 00:00:00";
					//DB::enableQueryLog();
					foreach($deposits as $trade)
					{
						$did   							= $trade->id;
					//	$dailyprofitQuery 				= DB::table('daily_profit_bonus')->where('trade_id',$did)->where('created_at',"2019-03-19 00:00:00")->first();
						$dailyprofitQuery 			= DB::table('daily_profit_bonus')->where('trade_id',$did)->where('created_at',$currentDateCron)->first();
						//dd(DB::getQueryLog());
						if(!isset($dailyprofitQuery))
						{
							$user_id   					= $trade->user_id;
							$unique_id   				= "D-".$trade->id;
							$amount   					= $trade->amount;
							$payment_mode   			= $trade->payment_mode;
							$currency   				= $trade->currency;
							$rate   					= $trade->rate;
							$total_amount   			= $trade->total_amount;
							$plan   					= $trade->plan;
							$status   					= $trade->status;
							$latest_profit   			= $trade->latest_profit;
							$trade_profit   			= $trade->trade_profit;
							$profit_total   			= $trade->profit_total;
							$flag_dummy   				= $trade->flag_dummy;
							$latest_crypto_profit   	= $trade->latest_crypto_profit;
							$crypto_profit  			= $trade->crypto_profit;
							$crypto_profit_total  		= $trade->crypto_profit_total;
							$lastProfit_update_date  	= $trade->lastProfit_update_date;
							$created_at   				= $trade->created_at;
							// Calculate Todays Profit 
							$today_profit 	= (floatval($total_amount)*$lastProfitRate)/100;
							
							//if($currency == "BTC" || $currency == "ETH" || $currency == "BCH" || $currency == "LTC" || $currency == "XRP")
							if($currency != "USD")
							{
								$today_crypto_profit	= (floatval($amount)*$lastProfitRate)/100;
							}else{
								$today_crypto_profit	= 0;
							}
							// Get user Info 
							$userInfo 					= users::where('id',$user_id)->first();
							PlansCron::process($userInfo);
							$userInfo 					= users::where('id',$user_id)->first();

							$parent_id					= $userInfo->parent_id;
							$user_uid					= $userInfo->u_id;
							$user_name					= $userInfo->name;
							$user_email					= $userInfo->email;
							$user_awarded_flag 			= $userInfo->awarded_flag ;
							
							$userAccInfo 				= UserAccounts::where('user_id',$user_id)->first();
							$latest_bonus				= $userAccInfo->latest_bonus;
							$latest_profit				= $userAccInfo->latest_profit;
							$reference_bonus			= $userAccInfo->reference_bonus;
							$cur 						= strtolower($currency);
							$accProfitVal				= "profit_".$cur;
							$lastAvailProfit			= $userAccInfo->$accProfitVal;
							// Save Calculated Profit
							$profit_bonus				=	new daily_profit_bonus();
							$profit_bonus->trade_id		= 	$did;
							$profit_bonus->user_id		= 	$user_uid;
							$profit_bonus->currency		= 	$currency;
							$profit_bonus->trade_amount	= 	$total_amount;
							$profit_bonus->last_profit	= 	$lastAvailProfit;
							$profit_bonus->percetage	=   $lastProfitRate;
							$profit_bonus->today_profit	= 	$today_profit;
							$profit_bonus->created_at	= 	$currentCronDate;
							//if($currency == "BTC" || $currency == "ETH" || $currency == "BCH" || $currency == "LTC" || $currency == "XRP")
							if($currency != "USD")
							{
								$profit_bonus->crypto_amount = 	$amount;
								$profit_bonus->crypto_profit = 	$today_crypto_profit;
							}
							
							$profit_bonus->save();
							// Get User Accounts Info
							
							// Update lastest Profit for users
							$currentDateTime			= date("Y-m-d h:i:s");
							$todayDate					= date("Y-m-d");
							$createdDate				= date("Y-m-d", strtotime($created_at));
							$date1MonthAfterCreat		= date("Y-m-d", strtotime($createdDate."+1 Month"));//Date After 1 month of creation
						
							$date						= date_create($todayDate);
							$dateDifferance 			= "";
							if($createdDate != "")
							{
								$date1					=	date_create($createdDate);
								// Calculate Dates Differance1 in days 
								$diff					= date_diff($date1,$date);
								$diff_in_days 			= $diff->days;
								$diff_in_days2 			= $diff->format("%R%a Days");
								$dateDifferance			= date("Y-m-d", strtotime($created_at.$diff_in_days2));
								//echo $diff_in_days2."<br>";
							}
							
							$trade_profit 				= $trade_profit + $today_profit; //deposits
							$profit_total 				= $profit_total + $trade_profit;// deposits
						
							if($currency != "USD")
							{
								$crypto_profit			= $crypto_profit + $today_crypto_profit;//deposits
								$crypto_profit_total	= $crypto_profit_total + $today_crypto_profit;//deposits
							}else{
								$crypto_profit			= $crypto_profit + $today_crypto_profit;//deposits
								$crypto_profit_total	= $crypto_profit_total + $today_crypto_profit;//deposits
							} 
							//Update Daily Profit On Trades 
							if(($lastProfit_update_date == "" || $lastProfit_update_date == "null") && $date1MonthAfterCreat != ""  && $dateDifferance == $date1MonthAfterCreat)
							{ 	
								deposits::where('id',$did)->update(['latest_profit' =>$today_profit,'trade_profit' => 0,'profit_total'=>$profit_total,'latest_crypto_profit' =>$today_crypto_profit,'crypto_profit' =>0,'crypto_profit_total' => $crypto_profit_total,'lastProfit_update_date'=>$currentDateTime]);
								if(isset($currency))
								{
									$curr 					= strtolower($currency);
									$accBal					= "balance_".$curr;
									$userbalance			= $userAccInfo->$accBal;
									$accBalSold				= "sold_bal_".$curr;
									$userbalanceSold		= $userAccInfo->$accBalSold;
									$accProfit				= "profit_".$curr;
									$userProfit				= $userAccInfo->$accProfit;
									$accWaitingProfit		= "waiting_profit_".$curr;
									$userwaitingProfit		= $userAccInfo->$accWaitingProfit;
									if($currency == "USD")
									{
										$latest_profit 		= $today_profit;
										$userProfit 		= $userProfit + $trade_profit;
										$userwaitingProfit 	= $userwaitingProfit - $trade_profit;
									}else{
										$latest_profit 		= $today_crypto_profit;
										$userProfit 		= $userProfit + $crypto_profit;
										$userwaitingProfit 	= $userwaitingProfit - $crypto_profit;
									}
									if($userwaitingProfit < 0)
									{
										$userwaitingProfit = 0;
									}	
									UserAccounts::where('user_id',$user_id)->update([$accProfit=> $userProfit,$accWaitingProfit=> $userwaitingProfit,'latest_profit' =>$latest_profit]);
								}
								
							}else if(($lastProfit_update_date != "" || $lastProfit_update_date != "Null"))
							{ 	
						
								deposits::where('id',$did)->update(['latest_profit' =>$today_profit,'trade_profit' => 0,'profit_total'=>$profit_total,'latest_crypto_profit' =>$today_crypto_profit,'crypto_profit' =>0,'crypto_profit_total' => $crypto_profit_total]);
								
								if(isset($currency))
								{
									$curr 					= strtolower($currency);
									$accBal					= "balance_".$curr;
									$userbalance			= $userAccInfo->$accBal;
									$accBalSold				= "sold_bal_".$curr;
									$userbalanceSold		= $userAccInfo->$accBalSold;
									$accProfit				= "profit_".$curr;
									$userProfit				= $userAccInfo->$accProfit;
									$accWaitingProfit		= "waiting_profit_".$curr;
									$userwaitingProfit		= $userAccInfo->$accWaitingProfit;
									if($currency == "USD")
									{
										$latest_profit 		= $today_profit;
										$userProfit 		= $userProfit + $today_profit;
									}else{
										$latest_profit 		= $today_crypto_profit;
										$userProfit 		= $userProfit + $today_crypto_profit;
									}
									
									UserAccounts::where('user_id',$user_id)->update([$accProfit=> $userProfit,'latest_profit' =>$latest_profit]);
								}
								
								
							}else
							{
								deposits::where('id',$did)->update(['latest_profit' =>$today_profit,'trade_profit' => $trade_profit,'latest_crypto_profit'=>$today_crypto_profit,'crypto_profit'=>$crypto_profit]);
								
								if(isset($currency))
								{
									$curr 					= strtolower($currency);
									$accBal					= "balance_".$curr;
									$userbalance			= $userAccInfo->$accBal;
									$accBalSold				= "sold_bal_".$curr;
									$userbalanceSold		= $userAccInfo->$accBalSold;
									$accProfit				= "profit_".$curr;
									$userProfit				= $userAccInfo->$accProfit;
									$accWaitingProfit		= "waiting_profit_".$curr;
									$userwaitingProfit		= $userAccInfo->$accWaitingProfit;
									if($currency == "USD")
									{
										$latest_profit 		= $today_profit;
										$userwaitingProfit 	= $userwaitingProfit + $today_profit;
									}else{
										$latest_profit 		= $today_crypto_profit;
										$userwaitingProfit 	= $userwaitingProfit + $today_crypto_profit;
									}
									if($userwaitingProfit < 0)
									{
										$userwaitingProfit = 0;
									}	
									UserAccounts::where('user_id',$user_id)->update([$accWaitingProfit=> $userwaitingProfit,'latest_profit' =>$latest_profit]);
								}
								
							}
						
							// Print Logs	
							$this->info('On Trade ='.$unique_id.' UserId = '.$user_uid.' Recieved Profit Amount: '.$latest_profit.'<br>');
							Log::info('On Trade ='.$unique_id.' UserId = '.$user_uid.' Recieved Profit Amount: '.$latest_profit .'<br>');
										
							// Send Emails to users
							
							//$sendEmails  =  sendEmail($unique_id,$user_name,$user_email,$today_profit);
							
						
							
							// update Parent Bonus According to Rules of Profit
			
							$count  = 1;	  
							if($parent_id != "0" && $parent_id != "" && $flag_dummy == 0 && $user_awarded_flag == 0)
							{
								$calculatedBonus  = 0;
								for($i=0; $i<5; $i++)
								{
									
									$parentDetails 	= DB::table('users')
										->select('id','u_id','parent_id','plan')
										->where('u_id',$parent_id)
										->first();
									if(isset($parentDetails))
									{
										$Parent_userID 		= $parentDetails->id;
										$parentPlanid 		= $parentDetails->plan;
										$parentNewId 		= $parentDetails->parent_id;
										$parent_uid 		= $parentDetails->u_id;
										//Getting Rules of Profit
										$plansDetailsQuery = DB::table('plans')
											->join('referal_profit_bonus_rules AS refprofit','plans.id','=', 'refprofit.plan_id')
											->select('refprofit.first_pline','refprofit.second_pline','refprofit.third_pline','refprofit.fourth_pline','refprofit.fifth_pline')
											->where('plans.id',$parentPlanid)->first();
			
										$profit_line1 	= $plansDetailsQuery->first_pline;
										$profit_line2 	= $plansDetailsQuery->second_pline;
										$profit_line3 	= $plansDetailsQuery->third_pline;
										$profit_line4 	= $plansDetailsQuery->fourth_pline;
										$profit_line5 	= $plansDetailsQuery->fifth_pline;	
										
										if(floatval($profit_line1) > 0 && $count==1 ) 
										{
											$calculatedBonus = (floatval($today_profit) * floatval($profit_line1))/100;
											$percentage 	 =  $profit_line1;
										}else if(floatval($profit_line2) > 0 && $count==2)
										{
											$calculatedBonus = (floatval($today_profit) * floatval($profit_line2))/100;
											$percentage 	 =  $profit_line2;
										}else if(floatval($profit_line3) > 0 && $count==3)
										{
											$calculatedBonus = (floatval($today_profit) * floatval($profit_line3))/100;
											$percentage 	 =  $profit_line3;
										}else if(floatval($profit_line4) > 0 && $count==4)
										{
											$calculatedBonus = (floatval($today_profit) * floatval($profit_line4))/100;
											$percentage 	 =  $profit_line4;
										}else if(floatval($profit_line5) > 0 && $count==5)
										{
											$calculatedBonus = (floatval($today_profit) * floatval($profit_line5))/100;
											$percentage 	 =  $profit_line5;
										} 
										
										$bonus = floatval($calculatedBonus);
										$parentAccInfo 		= UserAccounts::where('user_id',$Parent_userID)->first();
									
										$referenceBonus 	= $parentAccInfo->reference_bonus;
										$pre_bonus_amt 	    = $parentAccInfo->reference_bonus;
									
										$referenceBonus 	= floatval($referenceBonus) + floatval($bonus);  // Accounts Table
										$new_bonus_amt      = $referenceBonus;
										if($bonus > 0)
										{
											$daily_ibonus				=	new daily_investment_bonus();
											$daily_ibonus->trade_id		= 	$did;
											$daily_ibonus->user_id		= 	$user_uid;
											$daily_ibonus->parent_id	= 	$parent_id;
											$daily_ibonus->parent_plan	= 	$parentPlanid;
											$daily_ibonus->bonus		= 	$bonus;
											$daily_ibonus->pre_bonus_amt= 	$pre_bonus_amt;
											$daily_ibonus->new_bonus_amt= 	$new_bonus_amt;
											$daily_ibonus->created_at	= 	$currentCronDate;
											//$daily_ibonus->details		= "Profit Bonus With Percentage of ".$lastProfitRate." %";
											$daily_ibonus->details		= 	"Profit Bonus";
											$daily_ibonus->save();
										
											// update bonus for parents 
										
											if($referenceBonus >= 0)
											{
												UserAccounts::where('user_id',$Parent_userID)->update(['reference_bonus'=>$referenceBonus,'latest_bonus'=> $bonus]);
											}
											// Print Logs	
											$this->info('On Trade ='.$unique_id.' Level-'.$count.' ParentId = '.$parentNewId.' Recieved Bonus Amount: '.$bonus.' New Total Bonus Amount: '.$referenceBonus.'<br>');
											Log::info('On Trade ='.$unique_id.' Level-'.$count.' ParentId = '.$parentNewId.' Recieved Bonus Amount: '.$bonus .' New Total Bonus Amount: '.$referenceBonus .'<br>');
										
										}	
										$parent_id = $parentNewId;
										if($parent_id == '0' || $parent_uid == "B4U0001")
										{	//echo $parent_id;
											break;
										}
										
			
										$count++;
										$calculatedBonus = 0;
									}//end of if
								} // end of for loop
							}//end if	
						}// end of exit trade
						/* else{
							dd($dailyprofitQuery);
							continue;
						} */
						
					}// end of foreach loop	
					$end = "Successful";
					$sendEmails2  = sendEmailToDeveloper($emailto,$lastProfitRate,$currentCronDate,$end);
					if($emailFlag == 0)
					{	// Print Logs
						$this->info('Profit and Bonuses are calculated successful');
						Log::info('Cron Job Successful ,Profit and Bonuses are calculated successfully.');
					}else{ 	// Print Logs
						$this->info('Profit and Bonuses are calculated successful.');
						Log::info('Cron Job Successful ,Profit and Bonuses are calculated successfully.');
					}
					//Log::write('event', 'Profit and Bonuses are calculated successful! and emails sent to all customers.')
			}else{ 	// Print Logs
					//return redirect()->back()->with('errormsg', 'Rates not Find! Profit and Bonuses are not calculated.');
					
					$this->info('Rates not Find! Profit and Bonuses are not calculated.');
					Log::info('Cron Job Not Successful ,Rates not Find! Profit and Bonuses are not calculated.');
					Log::info('Today date is '.date('d-m-Y h:i:s').' and day is :'.date('l'));
					//Log::write('Rates not Find! Profit and Bonuses are not calculated.')
			}
		
		}catch(\Exception $ex){
			//echo $ex->getMessage();
			$errorMsg = $ex->getMessage();
			Log::info('Today date is '.date('d-m-Y h:i:s').' and cron not successful, Due to '.$errorMsg);
			$error = "Error";
		//	$sendEmails3  = sendEmailToDeveloper($emailto,$lastProfitRate,$currentCronDate,$errorMsg);
		}
	}
	
	//Update ExchangeRates
	function updateExchangeRates($currentDateCron)
	{
		$currentRates = DB::table('current_rate')->orderby('created_at', 'DESC')->first();
		
		if(isset($currentRates))
		{
			$currencyRates = DB::table('currency_rates')->orderby('created_at', 'DESC')->first();
			if(isset($currencyRates) && $currencyRates->created_at != $currentDateCron)
			{//save rates info
				$rquery = new currency_rates();
				$rquery->rate_usd 		= 1;
				$rquery->rate_btc 		= $currentRates->rate_btc;
				$rquery->rate_eth 		= $currentRates->rate_eth;
				$rquery->rate_bch 		= $currentRates->rate_bch;
				$rquery->rate_ltc 		= $currentRates->rate_ltc;
				$rquery->rate_xrp 		= $currentRates->rate_xrp;
				$rquery->rate_zec 		= $currentRates->rate_zec;
				$rquery->rate_dash 		= $currentRates->rate_dash;
				$rquery->created_at 	= $currentDateCron;
				$rquery->today_profit 	= $currentRates->today_profit;
				$rquery->save();
			}
		}
	}
	
	
	/*function sendEmailToMe($user_email, $percentage, $date, $startEnd)
	{
		// Send Emails to users
    				
    	
		if(isset($user_email) && isset($percentage) && isset($date))
		{	
    					//$email_to 		= $user_email;	
    			//$userName		= $user_name;
    			
    			$from_Name		= getMailFromName();
    			$from_email		= getSupportMailFromAddress();
				if($startEnd=="Successful")
				{
					$subject		= "B4U Global Profit Cron Job successfully Runs for Date ($date) profit percentage $percentage %";
				}else if($startEnd=="Started")
				{
					$subject		= "B4U Global Profit Cron Job Started successfully! from B4U Global";
				}else{
					$subject		= "B4U Global Profit Cron Not Successfull!";
				}
    			$message 		= 
    							 "<html>
    								 <body align=\"left\" style=\"height: 100%;\">
    									<div>
    										
    										<div>
    											<table style=\"width: 100%;\">
    												<tr>
    													<td style=\"text-align:left; padding:10px 0;\">
    														<h1>Dear Member B4U Global</h1>
    													</td>
    												</tr>";
								if($startEnd=="Started")
								{
									$message 	.="<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job Runs successfully and date is ".$date.".</h1>
														</td>
													</tr>";
								}else if($startEnd=="Successful")
								{
									$message 	.="<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job Completed successfully, and today Profit Percentage is ".$percentage." % and date is ".$date.".</h1>
														</td>
												</tr>";
								}else{
									$message 	.="<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															<h1>This Email is to inform you about today Profit Cron Job not successfull, </h1>
															<p> Due to haveing Error : ".$startEnd."</p>
														</td>
												</tr>";
								}

									$message 	.="<tr>
														<td style=\"text-align:left; padding:10px 0;\">
    														Thanks
														</td>
													</tr>
													<tr>
    													<td style=\"padding:10px 0; text-align:left;\">
    														Your Sincerely,
    													</td>
    												</tr>
    												<tr>
    													<td style=\"padding:10px 0; text-align:left;\">
    														Team B4U GLOBAL
    														
    													</td>
    												</tr>
    												
    												
    											</table>
    										</div>
    									</div>
    								</body>
    							</html>"; 
    					//echo $message;
    					//exit;
    					// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From:B4U Global Support <'.$from_email.'>' . "\r\n";
    						// More headers info
				//$headers .= 'From: Noreply <noreply@b4uinvestors.cf>' . "\r\n";
				$headers .= 'Cc: mudassar.mscit@gmail.com' . "\r\n";
							
				// Send Emails to users 					
				$success = @mail($user_email, $subject, $message , $headers);
	
		}
		
	}*/
}

//Run this command in server to execute live cron job  =>   cd /home/b4uinvestors/public_html && php artisan schedule:run >> null 2>&1