<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RefreshPromoInvestor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:promo:investor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh Promo Investor by removing all data from Investor table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**e
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        $from = '2019-01-01';
        $from = '2020-07-01';
        $to = '2020-12-31';

        ## add new data in cache..
        Cache::put('promo_investor', DB::select('CALL promo_investors("' . $from . '","' . $to . '")'));

    }
}
