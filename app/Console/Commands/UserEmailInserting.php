<?php

namespace App\Console\Commands;

use App\users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserEmailInserting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /* $users =   DB::table('users')
             ->select('email',DB::raw('COUNT(id) as count'))
             ->groupBy('email')
             ->orderBy('count','DESC')
             ->get();*/

        /*  foreach ($users as $user){
             if($user->count > 1){
                 $emailUser = New users();
                 $emailUser->email =
             }
          } */
        users::whereNull('email')->orderBy('id','asc')->chunk(1000,function($users){
            foreach($users as $user){
                $users_backup = DB::table('users_backup')->select('email')->whereId($user->id);
                if($users_backup->count()) {
                    $users_backup =  $users_backup->first();
                    try {
                        $users = new users;
                        $users->whereId($user->id)->whereNull('email')->update(['email'=>$users_backup->email]);
                        echo "Row Inserted " . $users_backup->email . "\n";
                    }catch (\Exception $exception) {
                        echo $exception->getMessage() . "\n";
                        echo "Line No.  ". $exception->getLine() . "\n";
                    }
                }else{
                    echo "no user found" .  $users_backup->email . "\n";
                }
            }
        },100);
        return 0;
    }
}
