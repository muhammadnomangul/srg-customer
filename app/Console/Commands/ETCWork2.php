<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use Illuminate\Console\Command;

class ETCWork2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etc:work2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $CBAccounts = CBAccounts::where('locked_balance', '<', '0')->where('current_balance', '=', '0')->get();
            foreach ($CBAccounts as $CBAccount) {
                $CBDebitsApproved = CBDebits::where('cb_account_id', $CBAccount->id)->where('status', 'approved');

                $CBDebits = CBDebits::where('cb_account_id', $CBAccount->id);

                $CBCredits = CBCredits::where('cb_account_id', $CBAccount->id);
                $CBCreditApproved = CBCredits::where('cb_account_id', $CBAccount->id)->where('status', 'approved');


                if (($CBDebitsApproved->count() == $CBDebits->count()) && ($CBCredits->count() == $CBCreditApproved->count())) {

                    $this->info('cashbox account id is :: ' . $CBAccount->id);

                    if ($CBDebits->sum('amount') == $CBCredits->sum('amount')) {
//                    $CBAccount->current_balance = 0;
                        $CBAccount->locked_balance = 0;
                        $CBAccount->save();
                        $this->info('cashbox account id is :: ' . $CBAccount->id);
                    }

                } else {

                }


            }

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
