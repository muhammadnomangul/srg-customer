<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBDebits;
use App\User;
use App\UserAccounts;
use App\withdrawals;
use Illuminate\Console\Command;

class CreateWithDrawalCallSubscribers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw:call';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data =  \DB::select("SELECT * FROM `user_accounts` WHERE charges > 1");
        foreach ($data as $d){
            $user = User::where('id',$d->user_id)->first();
            $wiith = new withdrawals();
            $wiith = $wiith->addSubscriptionChargesWithdrawal($d->user_id,$user['u_id'],'Profit','','','','',$d->charges,'USD');
            $this->info($d->user_id);
            $this->info($d->charges);
            UserAccounts::where('user_id',$d->user_id)->update(['charges'=>0]);
//            $a->update(['current_balance'=> $a->current_balance - $d->expected_fees]);
        }
        return 0;
    }
}
