<?php

namespace App\Console\Commands\Deposit;

use App\daily_profit_bonus;
use App\deposits;
use App\User;
use Illuminate\Console\Command;

class CalculateReInvestDepositCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:deposit:reinvest:amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate total reinvest deposits amount';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::where('parent_id', 'B4U00012680')->orderBy('id', 'asc')->chunk(100, function ($usersList) {
            foreach ($usersList as $user) {
//                $this->info('User id .' . $user->id);
                \Illuminate\Support\Facades\DB::select('CALL calculate_investment_values(' . $user->id . ')');
            }
        });
    }
}
