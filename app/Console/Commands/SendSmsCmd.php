<?php

namespace App\Console\Commands;

use App\Http\Controllers\PinController;
use App\Jobs\SendOtp;
use App\Jobs\SendSms;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client as TwilioClient;

class SendSmsCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:sms {number} {user_id} {msg}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $contactNo = $this->argument('number');
        $user_id = $this->argument('user_id');
        $randomNo = $this->argument('msg');
//        $unique_id = $this->argument('number');
        $country = 'Pakistan';
        if ($contactNo) {
            SendSms::dispatch($contactNo, $randomNo, $country, $user_id);

            ///save otp into database
            ///
            $this->info('heheheh');

        }

//        $unique_id = $this->argument('number');
//        $user_id = $this->argument('user_id');
//        $contactNo = $this->argument('number');
//        $randomNo = $this->argument('msg');
//
////        $check_phone = $this->validatePhone($contactNo);
//      //  public function sendSMS($unique_id, $contactNo, $randomNo)
//
//
//        $twilioAccountSid = env('TWILIO_SID', 'AC2d34c16c1be9c8933438780e4ea98935');
//        $twilioAuthToken = env('TWILIO_TOKEN', '8a19a5224c69cbf47ac952c778763a53');
//        $twilioFromNumber = env('TWILIO_NUMBER', '+16144125274');
//
//        $client = new TwilioClient($twilioAccountSid, $twilioAuthToken);
//        $client->messages->create(
//        // Where to send a text message (your cell phone?)
//            $contactNo,
//            array(
//                'from' => $twilioFromNumber,
//                'body' => $unique_id . ' Thank you for register on B4U GLOBAL, Your Activation Code :' . $randomNo
//            )
//        );
//        return "success";
//
//        return 0;
    }
    public function validatePhone($phoneNumber)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        try {
            $swissNumberProto = $phoneUtil->parse($phoneNumber);
            $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::NATIONAL);
            return $phoneUtil->isValidNumber($swissNumberProto);

        } catch (\libphonenumber\NumberParseException $e) {
            /*  \Illuminate\Support\Facades\Log::error('Error while validate phone number at Pin Controller', [
                  'error-message' => $e->getMessage(),
                  'error-line' => $e->getLine(),
                  'invalid-phone-number' => $phoneNumber
              ]);*/
            return false;
        }


    }
}
