<?php

namespace App\Console\Commands\CashBox;

use App\CBAccounts;
use App\CBDebits;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ApproveDebitOnApproveDeposit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approve:debitonapprovedeposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // and deposits.amount > cb_debits.amount
        DB::transaction(function () {

            $CBDebit = DB::select(
                "SELECT deposits.id as deposits_id, deposits.cb_debit_id as cb_debit_id, deposits.amount as deposit_amount, cb_debits.amount as debit_amount, 
                            deposits.status as deposit_status, cb_debits.status as debit_status, deposits.created_at, cb_debits.cb_account_id 
                            from deposits 
                            left join cb_debits on deposits.cb_debit_id = cb_debits.id where cb_debit_id is not null and deposits.status <> cb_debits.status 
                            and deposits.status = 'Approved' and cb_debits.status = 'pending'
                            ORDER BY deposits.`id` DESC"
            );
            foreach ($CBDebit as $debit) {
                $cbDebitId = $debit->cb_debit_id;
                ## debit found
                $CBDebit = CBDebits::find($cbDebitId);
                if ($CBDebit instanceof CBDebits) {
                    if ($CBDebit->status == 'pending' && $CBDebit->type == 'newdeposit' && $CBDebit->category == 'deposit') {

//                        $CBDebit->status = 'approved';
//                        $CBDebit->save();

//                        if ($CBDebit->status == 'approved') {
//                            DB::table(CBAccounts::$tableName)
//                                ->lockForUpdate()
//                                ->where('id', $CBDebit->cb_account_id)
//                                ->where('locked_balance', '>=', $CBDebit->amount)
//                                ->decrement('locked_balance', $CBDebit->amount);
//                        }


                        $isAllowedToDoTransaction = DB::table(CBAccounts::$tableName)
                            ->where('id', $CBDebit->cb_account_id)
                            ->where('locked_balance', '>=', $CBDebit->amount)->lockForUpdate()->first();

                        if ($isAllowedToDoTransaction && !empty($isAllowedToDoTransaction->id)) {
                            $CBDebit->status = 'approved';
                            $CBDebit->save();

                            if ($CBDebit->status == 'approved') {
                                DB::table(CBAccounts::$tableName)
                                    ->where('id', $isAllowedToDoTransaction->id)
                                    ->decrement('locked_balance', $CBDebit->amount);
                            }
                        }

                        $this->info($cbDebitId);
                    }
                }
            }

        }, 2);

    }
}
