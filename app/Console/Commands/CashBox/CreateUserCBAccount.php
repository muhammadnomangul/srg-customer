<?php

namespace App\Console\Commands\CashBox;

use App\CBCredits;
use App\CBDebits;
use App\User;
use App\withdrawals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateUserCBAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:cashboxaccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Cash Box Accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $UserList = \Illuminate\Support\Facades\DB::select("
SELECT id FROM users WHERE users.id NOT IN (SELECT cb_accounts.user_id FROM cb_accounts)
");
        foreach ($UserList as $user) {
            try {
                $user = User::find($user->id);
                if ($user instanceof User) {
                    $this->info('Creating Cash Box accounts of User having Id  ' . $user->id);
                    /** @var User $user */
                    $user->assignCashBoxAccountOfAllCurrencies();
                } else {
                    $this->error('This is not valid user id ' . $user->id);
                }
            } catch (\Exception $exception) {
                $this->error('Error While creating Cashbox account of user  ' . $user->id . '  error is ' . $exception->getMessage());
            }
        }




        ##
//        $CBDebits = \Illuminate\Support\Facades\DB::select("SELECT * from cb_debits where type = 'transfer' and cb_debits.cb_credit_id is null and transfer_to_cash_box_account_id is null and transfer_to_user_id is null order by id desc");
//        foreach ($CBDebits as $CBDebit) {
//            try {
//                CBCredits::createInternalTransferCreditFromCBDebit($CBDebit->id);
//                $this->info("create credit if debit is created and credit_id is missing in debit table,,, CBDebit id {$CBDebit->id}");
//            } catch (\Exception $exception) {
//                $this->error($exception->getMessage());
//            }
//        }

        ##

//        $CBDebits = \Illuminate\Support\Facades\DB::select("SELECT * from cb_debits where type = 'transfer' and transfer_to_cash_box_account_id is null and transfer_to_user_id is null order by id desc");
//        foreach ($CBDebits as $CBDebit) {
//            try {
//                $CBDebit = CBDebits::find($CBDebit->id);
//                if ($CBDebit instanceof CBDebits) {
//                    ## get credit id.
//                    $CreditId = $CBDebit->cb_credit_id;
//                    $CBCredits = CBCredits::find($CreditId);
//                    if ($CBCredits instanceof CBCredits) {
//
//                        $CBDebit->transfer_to_cash_box_account_id = $CBCredits->cb_account_id;
//                        $CBDebit->transfer_to_user_id = $CBCredits->user_id;
//                        $CBDebit->save();
//                        $this->info("adding transfer to cashbox reference CB Debit {$CBDebit->id}");
//                    }
//                }
//            } catch (\Exception $exception) {
//                $this->error('Error While creating Cashbox account of user  ' . $user->id . '  error is ' . $exception->getMessage());
//            }
//        }


        ##
//        $CBCredits = \Illuminate\Support\Facades\DB::select("SELECT * FROM `cb_credits` WHERE category = 'balance' and type = 'transfer' and came_from_cb_account_id is null and came_from_user_id is null");
//        foreach ($CBCredits as $CBCredit) {
//            try {
//                $CBCredit = CBCredits::find($CBCredit->id);
//                if ($CBCredit instanceof CBCredits) {
//                    ## get credit id.
//                    $CBDebitId = $CBCredit->cb_debit_id;
//                    $CBDebits = CBDebits::find($CBDebitId);
//                    if ($CBDebits instanceof CBDebits) {
//
//                        $CBCredit->came_from_cb_account_id = $CBDebits->cb_account_id;
//                        $CBCredit->came_from_user_id = $CBDebits->user_id;
//                        $CBCredit->save();
//                        $this->info("adding transfer to cashbox reference CB Credit {$CBCredit->id}");
//
//                    }
//                }
//            } catch (\Exception $exception) {
//                $this->error('Error While creating Cashbox account of user  ' . $user->id . '  error is ' . $exception->getMessage());
//            }
//        }

        ## create cashbox's withdrawal credit if credit of specific withdrawal is not created.
//        withdrawals::where('fund_type', 'cashbox')->where('status', 'Approved')->where('pre_status', 'New')->orderBy('withdrawals.id', 'DESC')->where('id', '>', 416173)->chunk(1000, function ($withdrawals) {

//        withdrawals::where('fund_type', 'cashbox')->where('status', 'Approved')->where('pre_status', 'New')->orderBy('withdrawals.id', 'DESC')->chunk(1000, function ($withdrawals) {
//            foreach ($withdrawals as $withdrawal) {
//                try {
//
//                    $CBCredit = CBCredits::where('withdrawal_id', $withdrawal->id)->first();
//                    if ($CBCredit instanceof CBCredits) {
//                        $this->info('not found, you can stop command.');
//                    } else {
//                        $this->info('Credit credit of withdrawal  ' . $withdrawal->id);
////                        DB::select("CALL createCashBoxCreditFromApprovedWithdrawal({$withdrawal->id})");
//                    }
//
//                } catch (\Exception $exception) {
//                    $this->error('Error While creating Cashbox account of user  ' . $withdrawal->id . '  error is ' . $exception->getMessage());
//                }
//            }
//        });

    }
}
