<?php

namespace App\Console\Commands\CashBox;

use App\CBCredits;
use App\withdrawals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PlaceCBCreditIdInApproveWithdrawals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'placecbcredit:inapprovewithdrawal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Place CashBox Credit Id in Withdrawal Table, If CB Credit Id is missing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $Withdrawals = DB::select("
select * from withdrawals where status = 'Approved' and fund_type = 'cashbox' and cb_credit_id is null ORDER BY `id` DESC
");
            foreach ($Withdrawals as $withdrawal) {
                $withdrawalId = $withdrawal->id;
                $CBCredit = CBCredits::where('withdrawal_id', $withdrawalId)->first();
                if ($CBCredit instanceof CBCredits) {
                    ## if credit has been generated
                    $withdrawal = withdrawals::find($withdrawalId);
                    $withdrawal->cb_credit_id = $CBCredit->id;
                    $withdrawal->save();
                    $this->info("Credit Id {$CBCredit->id} has been placed in withdrawal {$withdrawalId}");
                } else {
                    $this->error('withdrawal is for cashbox but the credit on base of withdrawal ' . $withdrawalId . ' is not created');
                }
            }

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
