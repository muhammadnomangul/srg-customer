<?php

namespace App\Console\Commands\CashBox;

use App\CBCredits;
use Illuminate\Console\Command;

class CreateCashBoxCreditIfDebitCreated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:cbcreditifdebitcreated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ## fundtransfer, if debit is generated and credit is not generated then generate credit.
        $CBDebits = \Illuminate\Support\Facades\DB::select("
SELECT * from cb_debits where category='balance' AND type = 'transfer'
 and date(`created_at`) > '2020-12-18' and cb_credit_id is null  order by id DESC
 ");
        foreach ($CBDebits as $CBDebit) {
            try {
//                $this->info($CBDebit->id);
                if($CBDebit->status == 'inprocessing'){
                    CBCredits::createInternalTransferCreditFromCBDebit($CBDebit->id,'pending');
                }else{
                    CBCredits::createInternalTransferCreditFromCBDebit($CBDebit->id,$CBDebit->status);
                }
                $this->info("create credit if debit is created and credit_id is missing in debit table,,, CBDebit id {$CBDebit->id}");
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
    }
}
