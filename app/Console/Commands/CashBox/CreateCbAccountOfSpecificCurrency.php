<?php

namespace App\Console\Commands\CashBox;

use App\CBAccounts;
use App\User;
use App\users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateCbAccountOfSpecificCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createCb:curr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Cashbox account for a new currency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       /* $UserList = users::get();
        foreach ($UserList as $user) {
            try {
                CBAccounts::getOrCreateIfCBAccountByCurrencyId(10,$user->id);
            } catch (\Exception $exception) {
                $this->error('Error While creating Cashbox account of user  ' . $user->id . '  error is ' . $exception->getMessage());
            }
        }*/
        users::orderby('created_at', 'DESC')->chunk(1000, function ($users){
            foreach ($users as $user) {
                try {
                    CBAccounts::getOrCreateIfCBAccountByCurrencyId(10,$user->id);
                } catch (\Exception $exception) {
                    $this->error('Error While creating Cashbox account of user  ' . $user->id . '  error is ' . $exception->getMessage());
                }
            }
        });
    }
}
