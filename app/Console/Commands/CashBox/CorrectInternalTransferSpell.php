<?php

namespace App\Console\Commands\CashBox;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CorrectInternalTransferSpell extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:internaltransfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $CBDebits = DB::select("Select * from cb_debits where cb_debits.reason like '%Interval%' ");
            foreach ($CBDebits as $CBDebit) {
                $CBDebit = CBDebits::find($CBDebit->id);
                if ($CBDebit instanceof CBDebits) {
                    $NewReason = str_replace('Interval', 'Internal', $CBDebit->reason);
                    $CBDebit->reason = $NewReason;
                    $this->info($CBDebit->reason);
                    $CBDebit->save();
                }
            }
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
