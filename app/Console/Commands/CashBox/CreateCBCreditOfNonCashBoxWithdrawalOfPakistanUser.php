<?php

namespace App\Console\Commands\CashBox;

use App\CBCredits;
use App\withdrawals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateCBCreditOfNonCashBoxWithdrawalOfPakistanUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createcbCreditofnoncashbox:withdrawalofpakistanuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $Withdrawals = DB::select("SELECT withdrawals.id, user, unique_id, amount, currency, fund_type 
FROM `withdrawals` left join users on withdrawals.user = users.id where is_verify = 1 
and withdrawals.status = 'pending' and currency = 'USD' and users.Country = 'Pakistan' 
and fund_type is null
");
            foreach ($Withdrawals as $withdrawal) {
                $withdrawalId = $withdrawal->id;
                $Withdrawal = withdrawals::find($withdrawalId);
                if ($Withdrawal instanceof withdrawals) {
                    $this->info('processing ' . $Withdrawal->id);

                    $Withdrawal->fund_type = 'cashbox';
                    $Withdrawal->status = 'Approved';
                    $saveResponse = $Withdrawal->save();

                    if ($saveResponse) {
                        $this->error("{$Withdrawal->id} withdrawal, is to cashbox and approved");
                    } else {
                        $this->error("{$Withdrawal->id} withdrawal, is not updated to cashbox.");
                    }
                }
            }

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
