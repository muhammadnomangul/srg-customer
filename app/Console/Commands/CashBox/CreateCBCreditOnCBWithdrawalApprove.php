<?php

namespace App\Console\Commands\CashBox;

use App\CBCredits;
use App\withdrawals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateCBCreditOnCBWithdrawalApprove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:cbcreditoncbwithdrawalapprove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $Withdrawals = DB::select("SELECT id as withdrawal_id FROM `withdrawals` 
where fund_type = 'cashbox' and status= 'Approved' 
and id not in (SELECT withdrawal_id FROM `cb_credits` where withdrawal_id is not null) limit 500");
            foreach ($Withdrawals as $withdrawal) {
                try {
                    $withdrawalId = $withdrawal->withdrawal_id;
                    $Withdrawal = withdrawals::find($withdrawalId);
                    if ($Withdrawal instanceof withdrawals) {
                        $this->info('processing ' . $Withdrawal->id);
                        $CBCredit = CBCredits::createCBCreditOnCBWithdrawalApprove($Withdrawal);
                        if ($CBCredit instanceof CBCredits) {
                            $this->info("Credit has been generated {$CBCredit->id} on withdrawal {$Withdrawal->id}");
                        } else {
                            $this->error("Credit not generate on withdrwal {$Withdrawal->id} and message from function is , {$CBCredit}");
                        }
                    }
                } catch (\Exception $exception) {
                    $this->error('error while processing withdrawal');
                }

            }

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
