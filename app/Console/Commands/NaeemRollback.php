<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBDebits;
use App\deposits;
use App\Model\Deposit;
use App\User;
use Illuminate\Console\Command;

class NaeemRollback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'naeem:btcrollback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Naeem Deposit RollBack.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {


            $deposits = deposits::where('payment_mode', 'cashbox')->get();

            foreach ($deposits as $deposit) {

                ## find debit from deposits.

                $CBDebits = CBDebits::where('deposit_id', $deposit->id);

                if ($CBDebits->count()) {
                } else {
                    $this->info('deposit id not found of user :: ' . $deposit->id);
                    /** @var deposits $deposit */
                    $deposit->notes = 'fake';
                    $deposit->save();
                }
            }


//            $response = \Illuminate\Support\Facades\DB::select(
//                "SELECT * FROM `cb_debits` WHERE date(`created_at`) = '2020-12-02' and type = 'topdown' and transfer_to_user_id = 16291"
//            );
//
//            foreach ($response as $key => $re) {
//                ##
//                if ($re) {
//
//                    ## get debit.
//                    $CBDebit = CBDebits::find($re->id);
//                    if ($CBDebit instanceof CBDebits) {
//
//                        ## get account id
//                        $CBAccount = CBAccounts::where('user_id', $CBDebit->user_id)->where('currencies_id', 1)->first();
//
//                        if ($CBAccount instanceof CBAccounts) {
//                            $this->info('key is running ' . $key . ' :: having cb debit id  is ' . $CBDebit->id . '  have cb account id :: ' . $CBAccount->id);
//                            $CBDebit->currencies_id = 1;
//                            $CBDebit->cb_account_id = $CBAccount->id;
//                            $CBDebit->type = 'adjustment';
//                            $CBDebit->status = 'approved';
//                            $CBDebit->admin_notes = 'Deposits has been send to B4U users and Naeem mistakenly asked to Suleman to create credit and then he get thought he has already gave them money so, this deposit is generate in reward to revert credit';
//                            $CBDebit->save();
//                        }
//                    }
//
//
//                    ## generate debit in
////                    \Illuminate\Support\Facades\DB::select(" CALL `createCashBoxDebit`('USD', {$re->amount}, {$re->user_id}, 'b4uwallet','topdown','approved', 'Withdrawal Profit bonus transfer to User account by B4U',{$re->cb_account_id} ,16291, NULL)");
//                }
//            }

//            $allFiles = \Illuminate\Support\Facades\File::allFiles(storage_path('naeem_etc'));
//            foreach ($allFiles as $file) {
//
//                $filePath = $file->getPathname();
//                $dataList = array_map('str_getcsv', file($filePath));
//
//                foreach ($dataList as $data) {
//                    if (!empty($data[0])) {
//                        $user = User::where('u_id', $data[0])->first();
//                        if ($user instanceof User) {
//
//                            $cashboxAccount
//                                = $user->cashBoxAccounts()->where('currencies_id', 2)->first();
//
//                            if ($cashboxAccount instanceof CBAccounts) {
//                                \Illuminate\Support\Facades\DB::select(" CALL `createCashBoxDebit`('BTC', {$data[1]}, {$user->id}, 'b4uwallet','topdown','approved', 'Withdrawal Profit bonus transfer to User account by B4U',{$cashboxAccount->id} ,16291, NULL)");
//                            }
//
//                            /** @var User $user */
//
//                            $this->info(' Creating Cash BOx Credit of User having ID ' . $data[0] . ' is created of amount' . $data[1]);
//                        } else {
//                            $this->error('B4u id is invalid ' . $data[0]);
//                        }
//
//                    } else {
//                        $this->error('B4U id is missing.. ');
//                    }
//
//                }
//
//
//            }


        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
