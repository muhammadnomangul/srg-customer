<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ETCWork3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etc:work3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {


            $response = DB::select(
                "SELECT sum(cb_debits.amount) as d,sum(cb_credits.amount) as c, cb_accounts.current_balance b, cb_debits.cb_account_id,cb_accounts.locked_balance FROM 
`cb_debits`,cb_accounts,cb_credits where cb_accounts.id = cb_debits.cb_account_id and cb_credits.cb_account_id = cb_debits.cb_account_id and cb_debits.category = 'b4uwallet' 
AND cb_debits.type = 'adjustment' GROUP by cb_account_id HAVING c = b"
            );

            $this->info(count($response));
            foreach ($response as $re) {
                $this->info('account id ' . $re->cb_account_id);
                $CBAccountId = CBAccounts::find($re->cb_account_id);
                if ($CBAccountId instanceof CBAccounts) {
                    $CBAccountId->current_balance = 0;
                    $CBAccountId->save();
                }
            }


//            CBAccounts::where('current_balance', '<', '0')->orWhere('locked_balance', '<', '0')->chunk(1000, function ($CBAccounts) {
//                foreach ($CBAccounts as $CBAccount) {
//
//                    $CBCredits = CBCredits::where('cb_account_id', $CBAccount->id);
//                    $CBDebits = CBDebits::where('cb_account_id', $CBAccount->id)->where('transfer_to_user_id', '<>', '16291');
//                    $CBDebitProcessing = CBDebits::where('cb_account_id', $CBAccount->id)->where('status', 'inprocessing');
//
//                    $currentAmount = ($CBCredits->sum('amount') - $CBDebits->sum('amount'));
//                    $lockedBalance = $CBDebitProcessing->sum('amount');
//
//                    $this->info('cashbox account id :: ' . $CBAccount->id);
//                    $this->info('current amount ::' . $currentAmount);
//                    $this->info('locked amount ::' . $lockedBalance);
//
//
//                    if ($currentAmount < 0) {
//                        ## if current balance is in -ve
//
//                        $InverseAllCredits = CBCredits::where('came_from_cb_account_id', $CBAccount->id);
//                        $this->error('total transfer amount :: ' . $InverseAllCredits->sum('amount'));
//
//                        foreach ($InverseAllCredits->get() as $inverseAllCredit) {
//                            $this->info('credit id is  ' . $inverseAllCredit->id);
//
//                            // delete their credits.
//
//                            // get all deposits from this cash box account
//                            $newDeposit = CBDebits::where('cb_account_id', $inverseAllCredit->cb_account_id)->where('type', 'new_deposit');
//                            foreach ($newDeposit as $newdep) {
//                                $this->info('new deposit create by fake balance ' . $newdep->deposit_id);
//                            }
//                        }
//
//                    }
//
//                    $this->info('====================');
//
//
//                }
//            });

        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
