<?php

namespace App\Console\Commands;

use App\daily_profit_bonus;
use App\User;
use Illuminate\Console\Command;

class AddUserIdProfit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:profit:user:id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        daily_profit_bonus::whereNull('user')->orderBy('id','asc')->chunk(50,function($dpbs){
            foreach($dpbs as $dpb){
                $user = User::select(['id'])->where('u_id',$dpb->user_id);
                if($user){
                    $user = $user->first();
                    $dpb->user = $user->id;
                    $dpb->save();
                }
            }
        },100);
        return 0;
    }
}
