<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
//use App\Mail\DemoEmail;
//use App\Mail\ProfitPercentageEmail;
use App\Mail\TestMail;
//use Illuminate\Support\Facades\Mail as Email;
//use illuminate\contract\Mailer;
use DB;
use App\users;
use App\currency_rates;
use DateTime;

class SendEidEmailsCron extends Command
{
    
    /**     * The name and signature of the console command.     *     * @var string     */
    
    protected $signature = 'emails:eid';
    /**     * The console command description.     *     * @var string     */
    protected $description = 'Send emails to all customers.';
    /**     * Create a new command instance.     *     * @return void     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
    }
    
    /**     * Execute the console command.     *     * @return mixed     */
    public function handle()
    {
        $todayDay 	 = date('l');
        $currentdate = date('d-m-Y');
        /* Monday, tuesday...
         //$currentdate = date('d-m-Y',strtotime("-2 days"));  */
        $disabled   = 0;
        /* Note:Flag  Disabled =0 for Active And  Disabled =1 for Not Active */
        $emailFlag  = 0;
        if ($disabled == 0) {
            $id ='14039';
            $usersInfo = DB::table('users')
            ->where('id', '<=', $id)
            ->where('is_subscribe', '1')
            ->where('status', 'active')
            ->orderby('id', 'DESC')
            ->get();
            
            /*  $usersInfo = DB::table('users')
              ->where('email','mubixhb4u@gmail.com')
              ->where('email','mudassar.mscit@gmail.com')
              ->orWhere('email','mudassar_mscit@yahoo.com')

              ->orderby('id','DESC')
              ->get();
              */
                    
           
            /* Now Get latest Accounts Balances and rates and update Plans of Users       */
            $ratesQuery 	= DB::table('currency_rates')->orderby('created_at', 'DESC')->first();
            $lastProfitRate = $ratesQuery->today_profit;
            if (isset($lastProfitRate) && $emailFlag == 0) {
                foreach ($usersInfo as $user) {
                    if (isset($user->name)) {
                        $userName   		= $user->name;
                    } else {
                        $userName   		= "";
                    }
                    $userid   			= $user->id;
                    $userUID   			= $user->u_id;
                    $user_email   		= $user->email;
                    $is_subscribe   	= $user->is_subscribe;
                    if (isset($user_email) && isset($userUID) && $is_subscribe == 1) {
                        Mail::to($user_email)->send(new TestMail('Mudassar'));
                        
                        //$message = (new  \App\Mail\ProfitPercentageEmail($userName,$userUID,$userid,$lastProfitRate))->onQueue('emails');
                        //Mail::to($user)->queue($message);
                        
                        echo "Email successfully send to $userUID  \n";
                        Log::info('Email Successfully Sent to '.$userUID.' On '.$currentdate. '('.date('l'));
                    }
                }/* End Foreach Loop	*/
                $emailFlag = 1;
            }
        } else {
            /*return redirect()->back()->with('errormsg', 'Rates not Find! Profit and Bonuses are not calculated.');     */
            echo "Cron not run on saturday and sunday";
            Log::info('Today date is '.$currentdate.' and day is :'.date('l'));
            Log::info('Cron Job Not Successful ,Today is '.$todayDay.' and cron not run on weekends.');
            /*Log::write('Rates not Find! Profit and Bonuses are not calculated.');*/
        }
    }
}




/*if(isset($user_email) && isset($userUID ) && $is_subscribe == 1){    //Mail::to($user_email)->send(new ProfitPercentageEmail($userName,$userUID,$userid,$lastProfitRate));    $from_Name		= "B4U Global";    $from_email		= "support@b4utrades.com";    $subject		= "Today's profit B4U Global";    $message 		=        "<html>    								 <body align=\"left\" style=\"height: 100%;\">    									<div>    										<div>    											<table style=\"width: 100%;\">    												<tr>    													<td style=\"text-align:left; padding:10px 0;\">    														<h1>Dear Member ".$userUID."</h1>    													</td>    												</tr>    												<tr>    													<td style=\"text-align:left; padding:10px 0;\">    														<h1>Congratulation,</h1>    													</td>    												</tr>    												<tr>    													<td style=\"text-align:left; padding:10px 0;\">    														Today ".$currentdate." our trading brought of <br><font size=\"6\" color=\"#f3f3f3\" style=\"background-color:#5e0750;\">".$lastProfitRate."%</font>    													</td>    												</tr>    												<tr>    													<td style=\"text-align:left; padding:10px 0;\">    														Dividends was accrued on your investment portfolios.    													</td>    												</tr>    												<tr>    													<td style=\"text-align:left; padding:10px 0;\">    														Thanks for using  ".$from_Name.".    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														Your Sincerely,    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														Team ".$from_Name."    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														<a  href=\"{{ url('/') }}\" >    															<image src=\"{{ url('images/b4u-investment1.png') }}\">    														</a>    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														<span style=\"font-size:12.8px;color:#500050;\">Office :&nbsp;</span>															<p>	SR DIGITAL WORLD SPC Taman Melawati 53100 Kuala Lumpur Malaysia. </p>    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														<span style=\"font-size:12.8px;color:#500050;\">Phone :&nbsp;</span>    												  													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														<span style=\"font-size:12.8px;color:#500050;\">Email :&nbsp;</span>    														<a rel=\"nofollow\" style=\"color:#1155cc;\" ymailto=\"mailto:support@b4utrades.com\" target=\"_blank\" href=\"mailto:support@b4utrades.com\">support@b4utrades.com</a>    													</td>    												</tr>    												<tr>    													<td style=\"padding:10px 0; text-align:left;\">    														<span style=\"font-size:12.8px;color:#500050;\">Website :&nbsp;</span>    														<a rel=\"nofollow\" style=\"color:#1155cc;font-size:12.8px;\" target=\"_blank\" href=\"https://www.b4uglobal.com/\">www.b4uglobal.com</a>    													</td>    												</tr>    													<tr>    													<td style=\"padding:10px 0; text-align:left;\">     														<a rel=\"nofollow\" style=\"color:#1155cc;font-size:12.8px;\" target=\"_blank\" href=\"".route('unsubscribe',[$userid,hash('sha256',$userid ."".env('APP_KEY'))])."\">Unsubscribe</a>    													</td>    												</tr>    											</table>    										</div>    									</div>    								</body>    							</html>";    $headers = "MIME-Version: 1.0" . "\r\n";    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";    $headers .= 'From:B4U Global Support <'.$from_email.'>' . "\r\n";    $success = @mail($user_email, $subject, $message , $headers);    Log::info('Email Successfully Sent to '.$user_uid.' On '.$currentdate. '('.date('l'));}*///Run this command in server to execute live cron job  =>   cd /home/b4uinvestors/public_html && php artisan schedule:run >> null 2>&1<?php
