<?php

namespace App\Console\Commands;

use App\login_history;
use App\users;
use Illuminate\Console\Command;

class BlockInactiveUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'block:oldUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will block the users which are inactive from last 150 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        users::where('status','active')->chunk(1000,function ($users){
            foreach ($users as $user){
               echo  "$user->u_id \n";
               $lastLogin = login_history::where('user_uid',$user->u_id)->latest()->first();
               if ($lastLogin) {
                   $dateDifference = date_diff($lastLogin->created_at, now());
                   if ($dateDifference->days > 150) {
                   //    echo "$lastLogin->user_uid | $lastLogin->created_at  | $dateDifference->days \n";
                       $user->status = 'blocked';
                       $user->save();
                       app('App\Http\Controllers\Controller')->adminLogs($lastLogin->user_uid, '', '', 'UserBlocked_'.$dateDifference->days);
                   }
               }else{
                   $dateDifference = date_diff($user->created_at, now());
                   if ($dateDifference->days > 150) {
                      // echo  "User Not Login Yet: $user->u_id | $user->created_at  | $dateDifference->days \n";
                       $user->status = 'blocked';
                       $user->save();
                       app('App\Http\Controllers\Controller')->adminLogs($user->u_id, '', '', 'UserBlocked_LHNF_'.$dateDifference->days);
                   }
               }
            }
        });
    }
}
