<?php

namespace App\Console\Commands;

use App\admin_logs;
use App\CBAccounts;
use App\CBDebits;
use App\User;
use App\withdrawals;
use Illuminate\Console\Command;

class PendingApproveId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pending_approve_id_fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data =  \DB::select("SELECT id,unique_id FROM withdrawals where is_verify = 0 and ( adminid is null or adminid = -1 ) and status = 'Pending'");
        foreach ($data as $d){

//            $this->info(json_encode($d));
//            $this->info($d->unique_id);
            $withDrawalB4uId = $d->unique_id;

            $q = "SELECT admin_id from admin_logs WHERE admin_logs.user_id = '$withDrawalB4uId' and admin_logs.event='User Verified' ORDER BY admin_logs.id DESC LIMIT 1";
            //$this->info($q);

            $adminB4Uid =  \DB::select($q);
           // $this->info(json_encode($adminB4Uid));
//

            if(count($adminB4Uid)){
                $adminid = User::where('u_id',$adminB4Uid[0]->admin_id)->pluck('id');
                if( count($adminid)){

                    $adminid = $adminid[0];

                    withdrawals::where('id',$d->id)->update([
                        'adminid'=>$adminid
                    ]);
                    $this->info($d->id);

                }
            }else{
                withdrawals::where('id',$d->id)->update([
                    'adminid'=>-1
                ]);
            }



//            $a =  CBAccounts::where('id',$d->cb_account_id)->lockForUpdate()->first();
//
//            CBDebits::where('id',$d->cb_debits_id)->update(['amount'=>$d->expected]);
//            $a->update(['current_balance'=> $a->current_balance - $d->expected_fees]);
        }


        return 0;
    }
}
