<?php

namespace App\Console\Commands;


use App\CBAccounts;
use App\CBDebits;
use Illuminate\Console\Command;

class cashbox_deduction_fee_fix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cashbox_deduction_fee_fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data =  \DB::select("SELECT cb_debits.id AS cb_debits_id,cb_debits.cb_account_id AS cb_account_id, deposits.id AS deposit_id, Round(cb_debits.amount), Round(deposits.amount + deposits.fee_deducted) as expected, Round(deposits.amount + deposits.fee_deducted) - Round(cb_debits.amount) as expected_fees FROM cb_debits, deposits WHERE deposits.currency = 'USD' and deposit_id IS NOT NULL AND cb_debits.deposit_id = deposits.id AND Round(deposits.amount + deposits.fee_deducted) != Round(cb_debits.amount) and Round(cb_debits.amount) - Round(deposits.amount + deposits.fee_deducted) < 1 ");
        foreach ($data as $d){

            $this->info(json_encode($d));
            $this->info($d->deposit_id);
           $a =  CBAccounts::where('id',$d->cb_account_id)->lockForUpdate()->first();

            CBDebits::where('id',$d->cb_debits_id)->update(['amount'=>$d->expected]);
            $a->update(['current_balance'=> $a->current_balance - $d->expected_fees]);
        }

        return 0;
    }
}
