<?php

namespace App\Console\Commands;

use App\CBCredits;
use App\CBDebits;
use Illuminate\Console\Command;

class PutCBDebitIdInCreditIdAndViseVersa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'put:cbdebitcredit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $CBDebits = CBDebits::whereNull('cb_credit_id')
            ->where('category', 'balance')
            ->where('type', 'transfer')
            ->whereNotNull('transfer_to_cash_box_account_id')
            ->whereNotNull('transfer_to_user_id')
            ->chunk(100, function ($cbDebits) {
                foreach ($cbDebits as $cbDebit) {
//                    $createdAtDate = date('Y-m-d', strtotime($cbDebit->created_at));
                    $createdAtDate = $cbDebit->created_at;
                    $CBCredits = CBCredits::where('category', 'balance')
                        ->where('type', 'transfer')
                        ->where('cb_account_id', $cbDebit->transfer_to_cash_box_account_id)
                        ->where('currencies_id', $cbDebit->currencies_id)
                        ->where('amount', $cbDebit->amount)
                        ->whereDate('created_at', $createdAtDate);

                    if ($CBCredits->count() == 1 && $cbDebit instanceof CBDebits) {
                        $CBC = $CBCredits->first();
//                    $cbDebit->cb_credit_id = $CBC->id;
//                    $cbDebit->save();

//                     $CBC->cb_debit_id = $cbDebit->id;
//                        $CBC->save();

                        //                        $this->info("One Credit found against one Debit ");

                    } else {
                        $this->info("Against Debit {$cbDebit->id} found credits " . $CBCredits->count());
                    }

                }
            });
    }
}
