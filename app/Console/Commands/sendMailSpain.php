<?php

namespace App\Console\Commands;

use App\Mail\SpecialEmail;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class sendMailSpain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:spain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $todayDay 	 = date('l');
        $currentdate = date('d-m-Y');
        $disabled   = 0;
        /* Note:Flag  Disabled =0 for Active And  Disabled =1 for Not Active */

        if ($disabled == 0) {
            $id ='1916';
            $emailFlag  = 0;
            User::where('Country', '=', 'Spain')
//            User::where('Country', '!=', 'Pakistan')->where('id', '=', $id)
                ->where('id', '>', $id)
                ->chunk(100, function ($usersInfo) use ($emailFlag,$currentdate){
                    if ($emailFlag == 0) {
                        foreach ($usersInfo as $user) {
                            if (isset($user->name)) {
                                $userName   		= $user->name;
                            } else {
                                $userName   		= "Member";
                            }
                            $userid   			= $user->id;
                            $userUID   			= $user->u_id;
                            $user_email   		= $user->email;
                            if (isset($user_email)) {
                                Mail::to($user_email)->send(new SpecialEmail($userName, $userUID, $userid, $user_email));

                                //$message = (new  \App\Mail\ProfitPercentageEmail($userName,$userUID,$userid,$lastProfitRate))->onQueue('emails');
                                //Mail::to($user)->queue($message);

                                echo "Email successfully send to ID: $userid | UID: $userUID \n";
                                \Illuminate\Support\Facades\DB::select("INSERT INTO `email_logger` (`id`, `user_id`, `user_uid`, `status`) VALUES (NULL, '$userid', '$userUID', 'sent');");

                                Log::info('Email Successfully Sent to '.$userUID.' On '.$currentdate. '('.date('l'));
                            }else{
                                \Illuminate\Support\Facades\DB::select("INSERT INTO `email_logger` (`id`, `user_id`, `user_uid`, `status`) VALUES (NULL, '$userid', '$userUID', 'error');");

                            }
                        }/* End Foreach Loop	*/
                        $emailFlag = 1;
                    }
                });
//             dd(count($usersInfo));

            /* Now Get users and start sending emails to them. */

        } else {
            /*return redirect()->back()->with('errormsg', 'Rates not Find! Profit and Bonuses are not calculated.');     */
            echo "";
            Log::info('Today date is '.$currentdate.' and day is :'.date('l'));
            Log::info('All Users Special Mail Cron Job unSuccessful, Today is '.$todayDay.'');
        }

    }
}
