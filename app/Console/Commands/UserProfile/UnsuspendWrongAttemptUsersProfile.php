<?php

namespace App\Console\Commands\UserProfile;

use App\User;
use App\users;
use Aws\Ecr\Exception\EcrException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UnsuspendWrongAttemptUsersProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unsuspend:wrong:attempt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unsuspend PIN Controller wrong attempt B4U users profile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            User
                ::where('max_attempts', '>=', User::MaxAttempts)
                ->where('status', '=', 'wrong_attempt')
                ->orderBy('id', 'asc')->chunk(100, function ($users) {
                    foreach ($users as $user) {
                        User::unBlockUserWrongAttemptAccount($user);
                    }
                });


//            $updateResponse = DB::table('cb_accounts')
//                ->where('cb_accounts.id', $CBAccount->id)
//                ->where('cb_accounts.current_balance', '>=', $amount)->lockForUpdate();

//            User
//                ::where('max_attempts', '>=', User::MaxAttempts)
//                ->where('wrong_attempt_unsuspended_times', '=<', User::unBlockWrongAttemptUnsuspendedTimes)
//                ->orderBy('id', 'asc')->chunk(100, function ($users) {
//                    foreach ($users as $user) {
//                        User::unBlockUserWrongAttemptAfterSpecificTime($user);
//                    }
//                });
        } catch (\Exception$exception) {
            $this->error('Error while suspended b4u user account ' . $exception->getMessage());
        }
    }
}
