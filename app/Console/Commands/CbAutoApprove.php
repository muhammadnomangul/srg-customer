<?php

namespace App\Console\Commands;

use App\CBAccounts;
use App\deposits;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CbAutoApprove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cb:auto-approve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ## find deposit.
//        $depositId = 123;
//        $Deposit = \App\deposits::find($depositId);
        $Deposits = \App\deposits::where('payment_mode','cashbox')->where('status','pending')->where('id',845326)->get();
        foreach ($Deposits as $Deposit){
            if ($Deposit->payment_mode == 'cashbox' && strtolower($Deposit->status) == 'pending') {
                ## find debit from deposit.
                $debitId = $Deposit->cb_debit_id;
                ## find is valid debit.
                $CBDebits = \App\CBDebits::find($debitId);
                if ($CBDebits instanceof \App\CBDebits && strtolower($CBDebits->status) == 'pending') {
                    ## i found the debit now find the CashBox Account of User.
                    $CBAccount = \App\CBAccounts::find($CBDebits->cb_account_id);
                    if ($CBAccount instanceof \App\CBAccounts) {
                        ## check is enough locked balance available, to deduct.
                        $LockedBalance = CBAccounts::getCashBoxLockedBalanceByCurrencyCodeUser($Deposit->currency, $CBDebits->user_id);
                        if ($LockedBalance < $Deposit->amount) {
                            $this->info($Deposit->id.'   Deposit amount is less than locked balance');
                            //  return ['status' => false, 'message' => 'It seems, This user has duplicate Deposits from CashBox because locked amount is not equal to deposit amount'];
//                            return ['statusCode' => 300, 'message' => 'Deposit amount is less than locked balance'];
                        }
                        switch (strtolower($Deposit->status)) {
                            case 'pending':
//                                $CBDebits->setConnection('mysql3');
                                $CBDebits->status = 'approved';
                                $CBDebits->save();

//                                $CBAccount->setConnection('mysql3');
                                $CBAccount->locked_balance = floatval($CBAccount->locked_balance) - floatval($CBDebits->amount);
                                $CBAccount->save();
//                                return ['statusCode' => 200, 'message' => 'Cash Box Debit Approved'];

                                $this->info($Deposit->id.'  Cash Box Debit Approved');

                                $is_gift_user = 0;
//                                $id = $depositId;
                                $deposit_tenure = 0;
                                $approvedDate = date('Y-m-d');
//                                $deposit = deposits::where('id', $id)->first();
                                $response = DB::select('CALL approve_deposit(' . $Deposit->id . ',' . $Deposit->user_id . ',' . $deposit_tenure . ',"' . $approvedDate . '")');

                                break;
                            case 'cancelled':
                            case 'deleted':

//                                $CBDebits->setConnection('mysql3');
//                                $CBDebits->status = $Deposit->status;
//                                $CBDebits->save();
//
//                                $CBAccount->setConnection('mysql3');
//                                $CBAccount->locked_balance = floatval($CBAccount->locked_balance) - floatval($CBDebits->amount);
//                                $CBAccount->current_balance = floatval($CBAccount->current_balance) - floatval($CBDebits->amount);
//                                $CBAccount->save();

                                break;
                        }
                    } else {
                        $this->info($Deposit->id.'  Cash Box Account is not found');
//                        return ['statusCode' => 300, 'message' => 'Cash Box Account is not found'];
                    }
                } else {
                    $this->info($Deposit->id.'  Cash Box Debit not found');
//                    return ['statusCode' => 300, 'message' => 'Cash Box Debit not found'];
                }
            }
        }

        return 0;
    }
}
