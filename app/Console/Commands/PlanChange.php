<?php

namespace App\Console\Commands;

use App\User;
use App\users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PlanChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:change {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $users = User::all();
        User::orderBy('id','Desc')->chunk(1000, function ($users) {
            foreach ($users as $user) {
//
                echo "$user->u_id  \n";
                $user_id = $user->id;
                $result = \Illuminate\Support\Facades\DB::select('Select decide_plan2(?,?,?) as plan', [$user_id, 0, 0]);
//                $this->info(json_encode($result));
                $plan = $result[0]->plan;
                //
                $planType = "";
                if ($user->plan != $plan && $plan > 0) {
                    $planType = "greater";
                    if ($plan < $user->plan) {
                        $planType = "smaller";
                    }
                    $data = DB::select("INSERT INTO `user_plans_log` (`id`, `user_id`, `old_plan_id`, `new_plan_id`, `created_by`, `created_at`, `updated_at`) VALUES (NULL, '$user_id', '$user->plan', '$plan', '2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);");
                    echo "$planType | $user->u_id \n";
                    $user->plan = $plan;
                    $user->save();

                }
               /* if($plan == 'NULL' || $plan == null){
                    $user->plan = 1;
                    $user->save();
                }*/
            }

        });
//        foreach($users as $us){
//
//        $user_id = $us->id;
//        $this->info(1);
//        }
//        $user_id = $this->argument('user_id');
        ######## Change made by Basit ###################
//        $result = \Illuminate\Support\Facades\DB::select('Select decide_plan2(?,?,?) as plan', [$user_id, 0, 0]);
//        $this->info(json_encode($result));
//        $plan = $result[0]->plan;
//        $user = users::where('id', $user_id)->first();
//        $user_old_plan = $user->plan;
//        $planType = '';
//        if (isset($user->plan) && $user->plan != $plan) {
//            $planType = "greater";
//            if ($plan < $user->plan) {
//                $planType = "smaller";
//            }
////            self::saveBonusHistory($user->id, $planType);
//        }
//        \Illuminate\Support\Facades\DB::table('users')->where('id', $user_id)->update(['plan' => $plan]);

//        $data = DB::select("INSERT INTO `user_plans_log` (`id`, `user_id`, `old_plan_id`, `new_plan_id`, `created_by`, `created_at`, `updated_at`) VALUES (NULL, '$user_id', '$user_old_plan', '$plan', '2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);");
//        $this->info('plan changer ');
//        $this->info($planType);
//        $this->info($plan);
        return 0;
    }
}
