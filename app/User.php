<?php

namespace App;

use App\Mail\UserEmailVerificationEmail;
use App\UserAccountHistory;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\ParameterBag;

class  User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    const MaxAttempts = 3;
    const unBlockWrongAttemptMinutes = 10;
    const unBlockWrongAttemptUnsuspendedTimes = 10000;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $rememberTokenName = false;
    protected $guarded = ['id'];
    public $user_types = [
        SITE_NORMAL_USER => 'Normal User',
        SITE_ADMIN => 'Admin',
        SITE_MANAGER => 'Manager',
        SITE_AGENT => 'Agent',
        SITE_COMMITTEE => 'Committee User',
        SITE_SUPER_ADMIN => 'Super Admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function UserAcc()
    {
        return $this->hasOne('App\UserAccounts');
    }

    public function dplan()
    {
        return $this->belongsTo('App\plans', 'plan');
    }

    public function accountHistory()
    {
        return $this->hasMany("App\UserAccountHistory")->orderBy('created_at', 'asc');
    }

    // loads only direct children - 1 level
    public function children()
    {
        return $this->hasMany('App\User', 'parent_id', "u_id");
    }

    // recursive, loads all descendants
    public function descendants()
    {
        return $this->children()->with('descendants');
    }

    // parent
    public function parent()
    {
        return $this->belongsTo('App\User', 'parent_id', "u_id");
    }

    // all ascendants
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public function deposits()
    {
        return $this->hasMany("App\deposits", 'user_id', 'id');
    }

    public function kyc()
    {
        return $this->hasOne(Kyc::class, 'user_id', 'id');
    }

    public function otpTokens($nonUsed = false)
    {
        return $this->hasMany(OTPToken::class, 'user_id');
    }

    public function is_super_admin()
    {
        return $this->is_super_admin ? true : false;
    }

    public function is_user($type)
    {
        $user_type = (int)$this->type;
        if ($type == SITE_SUPER_ADMIN) {
            return $this->is_super_admin;
        } else {
            return $user_type == $type;
        }
    }

    public function type()
    {
        if ($this->is_super_admin()) {
            return $this->user_types[SITE_SUPER_ADMIN];
        } else if (isset($this->user_types[$this->type])) {
            return $this->user_types[$this->type];
        } else {
            return 'Invalid User!';
        }
    }


    /**
     * @param ParameterBag $parameterBag
     * @return array
     * @Purpose Search for B4uGlobal Users
     */
    public static function advanceSearch(ParameterBag $parameterBag)
    {

        try {
            $queryBuilder = User::where('id', '<>', 0);
            if ($parameterBag instanceof ParameterBag) {
                if ($parameterBag->get('col')) {
                    $queryBuilder->select($parameterBag->get('col'));
                }
            }


            ## return result
            switch ($parameterBag->get('result')) {
                case 'pluck':
                    $results = $queryBuilder->pluck($parameterBag->get('col'));
                    /** @var \Illuminate\Support\Collection $results */
                    $results = $results->all();
                    break;
                default:
                    $results = $queryBuilder->get();
                    break;
            }


            return [
                'status' => true,
                'results' => $results,
            ];

        } catch (Exception $exception) {
            return [
                'status' => false,
                'results' => [],
                'message' => $exception->getMessage()
            ];
        }

    }

    public static function hasDeposit(int $depositId)
    {
        return (new self)->deposits()->find($depositId);
    }

    /**
     * @Purpose Increase SMS credit
     * @param $userId - to which you want to increase credit.
     * @param $noOfSMS - how many sms has been sent to him
     * @return bool
     */
    public static function increase_user_sms_credit($userId = null, $noOfSMS = 1)
    {
        ## get the user.
        $loggedInUser = $userId ? users::find($userId) : Auth::user();
        if ($loggedInUser instanceof User) {
            ## given valid user
            $loggedInUser->sms_payable_amount = ($loggedInUser->sms_payable_amount) + (config('b4uglobal.PER_SMS_CHARGES_TO_USER_BY_B4UGLOBAL') * $noOfSMS);
            $loggedInUser->save();
            return true;
        } else {
            return false;
        }

    }

    /**
     * Mark the given user's email as verified.
     * @return bool
     */
    public function markEmailAsVerified()
    {
        try {
            $loggedInUser = $this;
            $loggedInUser->email_verified_at = Carbon::now();
            $loggedInUser->save();
        } catch (Exception $exception) {
            Log::error('Error while Marking email as verified', [
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'LineNO' => $exception->getLine()
            ]);
            return $exception->getMessage();
        }

    }

    /**
     * Get Current User
     * @param int|null $userId
     * @return User|\Illuminate\Contracts\Auth\Authenticatable|null
     */
    public static function getUserObj(int $userId = null)
    {

        if ($userId) {
            return User::find($userId);
        } else {
            return Auth::user();
        }
    }

    /**
     * Get User's email By user id.
     * @param int|null $userId
     * @return User|\Illuminate\Contracts\Auth\Authenticatable|null
     */
    public static function getUserEmailById(int $userId){
        return self::getUserObj($userId)->email;
    }

    /**
     * =====================  Email Verification ===============
     * */

    /**
     * @Purpose : Check An Email address is approved or attached with other users or not.
     * @param $emailAddress
     * @return mixed
     */
    public static function checkIsEmailVerifiedAndAttachedToOtherUser($emailAddress)
    {
        return User::where('email', $emailAddress)->where('id', '!=', Auth::user()->id)->count();
    }

    /**
     * @Purpose Send Verification Email to User, with Queue
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new UserEmailVerificationEmail());
    }

    /**
     * @param int $randomCode
     * @return string
     * @throws GuzzleException
     * @Purpose  Send Otp Call to user
     */
    public static function sendOtpToUserByCall(int $randomCode)
    {
        /** @var User $loggedInUser */
        $loggedInUser = Auth::user();
        $RemoteEndPoint = 'http://bsms.its.com.pk/apiotp.php';
        $ClientRequest = new \GuzzleHttp\Client();
        $ClientResponse = $ClientRequest->request('GET', $RemoteEndPoint, [
            'query' => [
                'key' => '753bfbf780eec18ed5ac7fbd0e2cbdec',
                'mobile' => $loggedInUser->phone_no,
                'otp' => $randomCode,
            ]
        ]);

        if ($ClientResponse->getStatusCode() === 200) {
            ## Call Response..
            $responseBody = json_decode($ClientResponse->getBody());
            foreach ($responseBody as $body) {
                if ($body->errorno == 0 and strtolower($body->status) == 'success') {
                    (new self())->increase_user_sms_credit($loggedInUser->id, 1);
                } else {
                    Log::error('ErrorWhileCallingAtOtpVerification', [
                        'responseMessage' => json_encode($responseBody)
                    ]);
                }
            }
            return 'success';
        } else {
            Log::error('ErrorWhileCallingAtOtpVerification', [
                'responseMessage' => 'It seems Call link Api response is down and not responding to ' . $ClientResponse->getStatusCode()
            ]);
            return 'failed';
        }

    }

    /**
     * @Purpose: Encrypt Two FA Secure PIN
     * @param $pin
     * @return string|null
     */
    public static function generateEncryptedTwoFAPIN($pin)
    {
        return encrypt($pin);
    }

    /**
     * @Purpose : Descrypt Secure PIN
     * @return mixed|null
     */
    public function getDecryptedTwoFAPIN()
    {
        return $this->secure_pin ? decrypt($this->secure_pin) : null;
    }

    /**
     * =====================  B4U User's Account Management ===============
     * */

    /**
     * @Purpose: Check is User Account is blocked or not
     * @return bool
     */
    public static function isAccountBlocked()
    {
        /** @var User $loggedInUser */
        $loggedInUser = Auth::user();
        if ($loggedInUser instanceof User) {
            $FreshUserFromDB = User::find($loggedInUser->id);
            if ($FreshUserFromDB->max_attempts >= self::MaxAttempts && $FreshUserFromDB->status == 'wrong_attempt') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * @Purpose: Unblock User's wrong attempt account
     * @param User $FreshUserFromDB
     * @return bool
     */
    public static function unBlockUserWrongAttemptAccount(User $FreshUserFromDB)
    {
        $FreshUserFromDB->max_attempts = 0;
        $FreshUserFromDB->status = 'active';
        $FreshUserFromDB->wrong_attempt_unsuspended_times = ($FreshUserFromDB->wrong_attempt_unsuspended_times + 1);
        return $FreshUserFromDB->push();

    }

    /**
     * @Purpose: Unblock User's wrong attempt account after specific time
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public static function unBlockUserWrongAttemptAfterSpecificTime(User $user)
    {
        $NowTime = new Carbon();
        $SuspendTime = new Carbon($user->wrong_attempt_suspended_at);
        $SuspendedGapInMinutes = $NowTime->diffInMinutes($SuspendTime);

        if ($SuspendedGapInMinutes >= self::unBlockWrongAttemptMinutes) {
            return self::unBlockUserWrongAttemptAccount($user);
        } else {
            return false;
        }
    }

    /**
     * @Purpose: System will auto unsuspend user wrong attempts block account, if system's unsuspended time is  ($time) or less then ($time)
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public static function unBlockUserWrongAttemptIfUnsuspendedTimes(User $user)
    {
        if ($user->wrong_attempt_unsuspended_times <= self::unBlockWrongAttemptUnsuspendedTimes) {
            return self::unBlockUserWrongAttemptAfterSpecificTime($user);
        } else {
            ## It might be attacker, who account is going to suspend more than 5 times.
            return false;
        }
    }


    /**
     * @purpose Get account unsuspended times
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function userAccountWillUnBlockAt()
    {
        $SuspendTime = new Carbon($this->wrong_attempt_suspended_at);
        return $SuspendTime->addMinutes(self::unBlockWrongAttemptMinutes)->format('d (D) -M-Y H:i');
    }


    /**
     * =====================  Cash Box ===============
     * */

    /**
     * @Purpose Get All Cash Box Accounts..
     * */
    public function cashBoxAccounts()
    {
        return $this->hasMany(CBAccounts::class, 'user_id')->addSelect([
            'curr_precision' => Currencies::getCurrencyPrecisionQueryObject(CBAccounts::$tableNameCurrenciesColumnRef),
        ]);
    }

    /**
     * @Purpose Get Cash Box Account of specific currency.
     * @param string $currencyCode
     * @return HasMany|null
     */
    public function getCashBoxAccountOfSpecificCurrency(string $currencyCode)
    {
        $Currencies = Currencies::where('code', $currencyCode)->first();
        if ($Currencies instanceof Currencies) {
            return $this->hasMany(CBAccounts::class, 'user_id')->where('currencies_id', $Currencies->id)->first();
        } else {
            return null;
        }
    }

    /**
     * @param int $accountId
     * @return HasMany
     * @Purpose Get Logged In User CashBox Debits
     */
    public function cashBoxDebits(int $accountId)
    {
        return $this->hasMany(CBDebits::class, 'user_id')->addSelect([
            'curr_precision' => Currencies::getCurrencyPrecisionQueryObject(CBDebits::$tableNameCurrenciesColumnRef),
            'transfer_to' => 'users.u_id',
            'transfer_to_name' => 'users.name'
        ])->leftJoin('users', CBDebits::$tableName . '.transfer_to_user_id', '=', 'users.id')->where('cb_account_id', $accountId);
    }

    /**
     * @param int $accountId
     * @return HasMany
     * @Purpose Get Logged In User CashBox Debits
     */
    public function cashBoxCredits(int $accountId)
    {
        return $this->hasMany(CBCredits::class, 'user_id')->addSelect([
            'curr_precision' => Currencies::getCurrencyPrecisionQueryObject(CBCredits::$tableNameCurrenciesColumnRef),
            'came_from_uid' => 'users.u_id',
            'came_from_name' => 'users.name'
        ])->leftJoin('users', CBCredits::$tableName . '.came_from_user_id', '=', 'users.id')->where('cb_account_id', $accountId);
    }

    /**
     * @Purpose Create User's Cash box account of all Currencies
     */
    public function assignCashBoxAccountOfAllCurrencies()
    {
        ## This one is the right way to enter into database.. fetch currencies  from database and then insert into cash box accounts.
//        $Currencies = Currencies::all();
//        foreach ($Currencies as $currency) {
//            dump($currency->id);
//            dump($currency->code);
//        }

        ## Make an currency array to reduce the system processing, because, we are going to bulk insertion of cash box accounts of single user.
        $CurrenciesAr = [
            1 => 'USD',
            9 => 'RSC',
            2 => 'BTC',
            3 => 'ETH',
            4 => 'BCH',
            5 => 'LTC',
            6 => 'XRP',
            7 => 'DASH',
            8 => 'ZEC',
            10 => 'USDT',
        ];

        try {
            foreach ($CurrenciesAr as $CurrencyId => $CurrencyCode) {
                ## before insert a new record validate is there already have an account of same currency in the cash box account table...
                CBAccounts::getOrCreateIfCBAccountByCurrencyId($CurrencyId, $this->id);
            }
        } catch (Exception $exception) {
            Log::error('Error While Making User CashBox account', [
                $exception->getMessage()
            ]);
        }


    }

    /**
     * @purpose Get bank detail of user on base of Currency id of current user
     * @param string $currencyCode
     */
    public static function getCashOutTransferToDetailOnBaseOfCurrency(string $currencyCode)
    {
        $loggedInUser = null;
        switch ($currencyCode) {

        }

    }

    /**
     * @param $searchBy
     * @return mixed
     * @Purpose :: Get User Minimum Information by Email or Id.
     */
    public static function getUserMinimumInfo($searchBy)
    {
        ## make an eloquent
        $Eloquent = User::select('name', 'email', 'phone_no', 'id');

        ## search user by id
        if (filter_var($searchBy, FILTER_VALIDATE_EMAIL)) {
            ## search user by email
            $searchByArray = [
                'email' => $searchBy
            ];
        } else {
            $searchByArray = [
                'id' => $searchBy
            ];
        }

        return $Eloquent->where($searchByArray)->first();
    }
}
