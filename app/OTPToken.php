<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OTPToken extends Model
{
    protected $table = "otp_tokens";

    const EXPIRATION_TIME = 15; // minutes]


    /**
     * Table Details
     * @Column  otp_type: 1 belongs to otp is used, 2 is used to tell this one is expired, and 0 declare that this OTP is available and not used, and not expired.
     *
     * */

    protected $fillable = [
        'code',
        'user_id',
        'used'
    ];

    public function __construct(array $attributes = [])
    {
        if (!isset($attributes['code'])) {
            $attributes['code'] = $this->generateCode();
        }

        parent::__construct($attributes);
    }

    /**
     * Generate code
     *
     * @return string
     */
    public function generateCode()
    {
        $code = mt_rand(100000, 999999);
        return $code;
    }

    /**
     * True if the token is not used nor expired
     *
     * @return bool
     */
    public function isValid()
    {
        return !$this->isUsed() && !$this->isExpired();
    }

    /**
     * Is the current token used
     *
     * @return bool
     */
    public function isUsed()
    {
        return $this->used;
    }

    /**
     * Is the current token expired
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->created_at->diffInMinutes(\Carbon\Carbon::now()) > static::EXPIRATION_TIME;
    }


    public function generateEncryptedCode()
    {
        return encrypt($this->generateCode());
    }

    public function getDecryptedCode()
    {
        return decrypt($this->code);
    }

    /**
     * @param User $user
     * @param string $otpOf
     * @Purpose: Generate new Otp in database.
     * @return OTPToken
     */
    public static function generateEmailOtp(User $user, string $otpOf): OTPToken
    {

        $otpType = 'email_' . $otpOf;

        ## expire old otps.
        self::expireOtpToken($user, $otpType, 2);

        $instance = (new self);
        $instance->code = $instance->generateEncryptedCode();
        $instance->user_id = $user->id;
        $instance->otp_type = 'email_' . $otpOf;
        $instance->save();
        return $instance;
    }

    /**
     * @param User $user
     * @param string $otpOf
     * @Purpose: Generate new Otp in database.
     * @return OTPToken
     */
    public static function generateSMSOtp(User $user, string $otpOf): OTPToken
    {

        $otpType = 'phone_' . $otpOf;
        ## expire old otps.
        self::expireOtpToken($user, $otpType, 2);

        $instance = (new self);
        $instance->code = $instance->generateEncryptedCode();
        $instance->user_id = $user->id;
        $instance->otp_type = $otpType;
        $instance->save();
        return $instance;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @Purpose Expire OTP Tokens..
     * @param User $user
     * @param string $otp_ty
     * @param int $isUsed
     * @return bool
     */
    public static function expireOtpToken(User $user, string $otp_ty, int $isUsed = 1)
    {
        return DB::table('otp_tokens')->where('otp_tokens.user_id', $user->id)->where('otp_tokens.otp_type', $otp_ty)->where('used', 0)->update(['used' => $isUsed]);
    }
}
