<?php

namespace App;

use App\Model\Deposit;
use App\Model\Rate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

/**
 * @property  created_by
 */
class withdrawals extends Model
{
    public static function approveAndPaid($withdrawId, $referenceId, $trans_Id)
    {
        $withdrawal = withdrawals::where('id', $withdrawId)->where('status', 'Pending');
        if ($withdrawal->count()) {
            $withdrawal = $withdrawal->first();
            $withdrawal->status= 'Approved';
            $withdrawal->is_paid= 1;
            $withdrawal->bank_reference_id = $referenceId;
            $withdrawal->trans_id = $trans_Id;
            $withdrawal->save();
            echo "done for withdrawal(".$withdrawal->id.") <br>";
        }
    }
    public function getCreatedAtDate()
    {
        return \Carbon\Carbon::parse($this->created_at)->toDateString();
    }

    public function date_string_new_system()
    {
        return "2018-12-15";
    }

    public function isBeforeNewSystem()
    {
        $cdate = \Carbon\Carbon::createFromFormat('Y-m-d', $this->getCreatedAtDate());
        $new_system_start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->date_string_new_system());
        return $cdate->lessThan($new_system_start_date);
    }

    /**
     * @return bool
     */
    public function isNewSystem()
    {
        return !$this->isBeforeNewSystem();
    }
    public function duser()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function toUsd()
    {
        if ($this->status == 'Approved') {
            return round($this->pre_amount * Rate::at(\Carbon\Carbon::parse($this->created_at)->toDateString(), $this->currency), 2);
        }
        return round($this->pre_amount * Rate::last($this->currency), 2);
    }


    public static function newSystemWithdrawalTotal($user_id, $currency)
    {

       // $amount_column = 'crypto_amount';
        // if($currency == 'usd'){
        //     $amount_column = 'amount';
        //}
        $amount_column =  'pre_amount';
        return withdrawals::whereRaw('user = ? and currency = ? and (status like ? or status like ? ) and is_manual_cancel = 0 and date(created_at) >= ? and payment_mode like ?', [$user_id,$currency,'Pending','Approved','2018-12-15','profit'])->sum($amount_column);
    }
    public static function newSystemWithdrawals($user_id)
    {
        return withdrawals::whereRaw('user = ? and date(created_at) >= ? and payment_mode like ? and (status like ? or status like ? )', [$user_id,'2018-12-15','profit','Pending','Approved']);
    }


    public function createdBy()
    {
        return $this->belongsTo("App\users", 'created_by');
    }
    public function verifiedUser()
    {
        return $this->belongsTo("App\admin_logs", 'unique_id');
    }

    public static function getWithdrawlsQuery($rate_pkr, $sold, $start = null, $end = null, $offset = null)
    {
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select(
                'users.account_no',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                //                DB::raw('SUM(amount) as total_amount'),
                'banks.bank_code'
            )
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where(function ($q) {
                $q->where('users.account_no', 'LIKE', '%' . 'Pk' . '%')
                    ->orWhere('users.account_no', 'LIKE', '%' . 'Ak' . '%');
            })
            ->where('users.bank_name', 'NOT LIKE', 'Meezan Bank')
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.amount', '<', 1333)
            ->where('withdrawals.is_verify', '=', 1)
            ->where('withdrawals.is_manual_cancel', '!=', 3);
        if($sold == 0) {
            $withdrawals->where('withdrawals.payment_mode', 'NOT LIKE', 'Sold');
        }else{
            $withdrawals->where('withdrawals.payment_mode', 'LIKE', 'Sold');
        }   
        if ($start) {
            $withdrawals->where('withdrawals.id', '>=', $start);
        }
        if ($end) {
            $withdrawals->where('withdrawals.id', '<=', $end);
        }
        if ($offset) {
            $withdrawals->offset($offset);
        }


        $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');
        return $withdrawals;
    }

    public static function getWithdrawlsQueryMBFT($rate_pkr, $sold, $start = null, $end = null, $offset = null)
    {    
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select(
                'users.account_no',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                'banks.bank_code'
            )
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('users.bank_name', 'LIKE', 'Meezan Bank')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where('withdrawals.amount', '<', 1333)
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.is_verify', '=', 1)
            ->where('withdrawals.is_manual_cancel', '!=', 3);
        if($sold == 0) {
            $withdrawals->where('withdrawals.payment_mode', 'NOT LIKE', 'Sold');
        }else{
            $withdrawals->where('withdrawals.payment_mode', 'LIKE', 'Sold');
        }
        if ($start) {
            $withdrawals->where('withdrawals.id', '>=', $start);
        }
        if ($end) {
            $withdrawals->where('withdrawals.id', '<=', $end);
        }
        if ($offset) {
            $withdrawals->offset($offset);
        }


        $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');

        /*
           $w = $withdrawals->select('withdrawals.id','withdrawals.payment_mode')->get();
           $c = 0;
           echo $sold . " <br>";
           foreach ($w as $y) {
               echo $c++ . "+  ";
           }fffffffffff
           exit($w);*/
  
        return $withdrawals;
    }


    public static function getWithdrawlsQueryAskariIBFT($rate_pkr, $start = null, $end = null, $offset = null)
    {
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select(
                'users.account_no',
                'users.account_name',
                'users.bank_name',
                'withdrawals.unique_id',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                //                DB::raw('SUM(amount) as total_amount'),
                'banks.askari_bank_codes',
                'banks.askari_bank_names'
            )
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('withdrawals.payment_mode', 'NOT LIKE', 'Sold')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where(function ($q) {
                $q->where('users.account_no', 'LIKE', '%' . 'Pk' . '%')
                    ->orWhere('users.account_no', 'LIKE', '%' . 'Ak' . '%');
            })
            ->where('users.bank_name', 'NOT LIKE', 'Askari Bank')
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.amount', '<', 1333)
            ->where('withdrawals.is_verify', '=', 1)
            ->where('withdrawals.is_manual_cancel', '!=', 3);
        if ($start) {
            $withdrawals->where('withdrawals.id', '>=', $start);
        }
        if ($end) {
            $withdrawals->where('withdrawals.id', '<=', $end);
        }
        if ($offset) {
            $withdrawals->offset($offset);
        }


        $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');
        return $withdrawals;
    }


    /* public static function getWithdrawlsQueryFaysalIBFT($rate_pkr, $sold_GT2, $start = null, $end = null, $offset = null)
    {
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select(
                'users.account_no',
                'users.account_name',
                'users.bank_name',
                'withdrawals.unique_id',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                //                DB::raw('SUM(amount) as total_amount'),
                'banks.faysal_bank_codes',
                'banks.faysal_bank_names'
            )
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where(function ($q) {
                $q->where('users.account_no', 'LIKE', '%' . 'Pk' . '%')
                    ->orWhere('users.account_no', 'LIKE', '%' . 'Ak' . '%');
            })
            ->where('users.bank_name', 'NOT LIKE', 'Faysal Bank')
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.is_manual_cancel', '!=', 3);
        if($sold_GT2 == 2) {
           $withdrawals->where('withdrawals.amount', '>=', 1333)
                       ->where('withdrawals.is_verify', '=', 2);
        }else{
           $withdrawals->where('withdrawals.amount', '<', 1333)
                       ->where('withdrawals.is_verify', '=', 1);
        }      
        if($sold_GT2 == 0) {
            $withdrawals->where('withdrawals.payment_mode', 'NOT LIKE', 'Sold');
        }elseif ($sold_GT2 == 1) {
            $withdrawals->where('withdrawals.payment_mode', 'LIKE', 'Sold');
        }     
        if ($start) {
            $withdrawals->where('withdrawals.id', '>=', $start);
        }
        if ($end) {
            $withdrawals->where('withdrawals.id', '<=', $end);
        }
        if ($offset) {
            $withdrawals->offset($offset);
        }


        $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');
        return $withdrawals;
    }*/




    public static function getWithdrawlsQueryBankIslamiIBFT($rate_pkr, $start = null, $end = null, $offset = null)
    {
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select(
                'users.account_no',
                'users.account_name',
                'users.bank_name',
                'withdrawals.unique_id',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                //                DB::raw('SUM(amount) as total_amount'),
                'banks.bank_islami_codes',
                'banks.bank_islami_names'
            )
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('withdrawals.payment_mode', 'NOT LIKE', 'Sold')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where(function ($q) {
                $q->where('users.account_no', 'LIKE', '%' . 'Pk' . '%')
                    ->orWhere('users.account_no', 'LIKE', '%' . 'Ak' . '%');
            })
            ->where('users.bank_name', 'NOT LIKE', 'Dubai Islamic Bank')
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.amount', '<', 1333)
            ->where('withdrawals.is_verify', '=', 1)
            ->where('withdrawals.is_manual_cancel', '!=', 3);
        if ($start) {
            $withdrawals->where('withdrawals.id', '>=', $start);
        }
        if ($end) {
            $withdrawals->where('withdrawals.id', '<=', $end);
        }
        if ($offset) {
            $withdrawals->offset($offset);
        }


        $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');
        return $withdrawals;
    }
    /*  public static function getWithdrawlsQueryAskari_IBT($rate_pkr, $start = null, $end = null, $offset = null)
    {
        $withdrawals = withdrawals::join('users', 'users.id', '=', 'withdrawals.user')
            ->join('banks', function ($join) {
                $join->on('users.bank_name', '=', 'banks.bank_name');
            })
            ->select('users.account_no',
                DB::raw("withdrawals.id AS Reference_No"),
                DB::raw("withdrawals.amount*$rate_pkr AS amount"),
                'banks.bank_code')
            ->where('users.Country', 'LIKE', 'Pakistan')
            ->where('users.type', '!=', 4)
            ->where('withdrawals.status', 'LIKE', 'Pending')
            ->where('users.bank_name', 'LIKE', 'Askari Bank')
            ->where('withdrawals.currency', 'LIKE', 'USD')
            ->where('withdrawals.amount', '<', 1333)
            ->where('withdrawals.is_paid', '=', 0)
            ->where('withdrawals.is_verify', '=', 1)
            ->where('withdrawals.is_manual_cancel','!=', 3);
            if($start){
                $withdrawals->where('withdrawals.id', '>=', $start);
            }
            if($end){

                $withdrawals->where('withdrawals.id', '<=', $end);
            }
            if($offset){
                $withdrawals->offset($offset);
            }


            $withdrawals->whereNull('withdrawals.fund_type')
            ->groupBy('withdrawals.id');
        return $withdrawals;
    }*/
    public function addSubscriptionChargesWithdrawal($user_id,$u_id,$payment_mode,$pre_amount,$crypto_amount,$usd_amount,$fund_type,$charges,$currency='USD'){
        $subscription_charge = new self();
        $subscription_charge->user = $user_id;
        $subscription_charge->unique_id = $u_id; //nullable
        $subscription_charge->amount = $charges;
        $subscription_charge->status = 'Approved';
        $subscription_charge->pre_status = 'New';//nullable
        $subscription_charge->payment_mode = $payment_mode;
        $subscription_charge->crypto_amount = $crypto_amount;//nullable
        $subscription_charge->usd_amount = $usd_amount;//nullable
        $subscription_charge->pre_amount = $pre_amount;
        $subscription_charge->currency = $currency;
        $subscription_charge->withdrawal_fee = 0;
        $subscription_charge->donation = 0;
        $subscription_charge->new_type = null;//nullable
        $subscription_charge->fund_type = null;//nullable
        $subscription_charge->fund_receivers_id = null;//nullable
        $subscription_charge->paid_flag = 0;
        $subscription_charge->flag_dummy = 0;
        $subscription_charge->is_verify = 1;
        $subscription_charge->verified_at = null;//nullable
        $subscription_charge->verified_by = null;//nullable
        $subscription_charge->approved_by = null;//nullable
        $subscription_charge->approved_at = null;//nullable
        $subscription_charge->paid_by = null;//nullable
        $subscription_charge->paid_at = null;//nullable
        $subscription_charge->is_paid = 1;
        $subscription_charge->fund_type = 'callcharges';
        $subscription_charge->bank_reference_id = null;//nullable
        $subscription_charge->paid_img   = null;//nullable
        $subscription_charge->is_manual_cancel = 0;
        $subscription_charge->created_at = Carbon::now();//nullable
        $subscription_charge->updated_at = Carbon::now();//nullable
        $subscription_charge->created_by  = null;//nullable
        $subscription_charge->batch_id = null;//nullable

//        $subscription_charge->sub_charges = $charges;
        $subscription_charge->save();
        if ($subscription_charge){
            return $subscription_charge;
        }else{
            return false;
        }

//        // save New Withdrawal
//        $wd = new withdrawals();
//        $wd->user = $user_id;
//
//        $wd->amount = $finalAmount;  // withdrawal Amount
//        $wd->pre_amount = $balance; // Previous Acc Balance
//        if ($currency != "USD") {
//            $wd->crypto_amount = $amount;
//        }
//        $wd->usd_amount = $totalUsd;
//        $wd->donation = $donation + $donation2;
//        $wd->currency = $currency;
//        $wd->unique_id = $user_uid;
//        $wd->payment_mode = $withdraw_mode;
//        $wd->status = 'Pending';
//        $wd->pre_status = 'New';
//        if (Auth::user()->awarded_flag == 2 || Auth::user()->awarded_flag == '2') {
//            $wd->flag_dummy = 1;
//        }
//
//        $wd->withdrawal_fee = $withdrawal_fee; // Previous Acc Balance
//
//        if (isset($request['fund_type'])) {
//            $wd->fund_type = $request['fund_type'];
//
//            $wd->fund_receivers_id = $fundRecieverID;
//        }
//
//        // save the created by id for withdrawls
//        $createdby = (app('request')->session()->get("Admin_Id")) ? app('request')->session()->get("Admin_Id") : Auth::user()->id;
//        $wd->created_by = $createdby;
//
//        // dd($wd);
//        $wd->save();
//
//        $last_withdrawal_id = $wd->id;
//
//        // send email functions
//        $this->sendWithdrawalSuccessMail($last_withdrawal_id, $amount, $finalAmount, $currency);
    }
}
