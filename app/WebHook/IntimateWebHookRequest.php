<?php


namespace App\WebHook;

/**
 * IntimateCashBoxTransferToB4UWallet constructor.
 */
class IntimateWebHookRequest extends WebHook
{

    /**
     * @param string $endPoint
     * @Purpose Set End Point, where to hit the request
     */
    public function setEndPoint(string $endPoint)
    {
        $this->EndPoint = $endPoint;
    }

    /**
     * @param array $payload
     * @Purpose Set Pay Load, what to share
     */
    public function setPayLoad(array $payload)
    {
        $this->PayLoad = $payload;
    }

    /**
     * @param array $headers
     * @Purpose Set Headers
     */
    public function setHeaders(array $headers)
    {
        $this->Headers = $headers;
    }

    /**
     * @param string $EndPoint
     * @param array $PayLoad
     * @param array $Headers
     * @Purpose Send request with data payload.
     */
    public static function send(string $EndPoint, array $PayLoad, array $Headers = [])
    {
        $selfThis = (new self());
        ($selfThis)->setEndPoint($EndPoint);
        ($selfThis)->setPayLoad($PayLoad);
        ($selfThis)->setHeaders($Headers);
        $response = ($selfThis)->sendRequest();
        return $response->successful();
    }

}
