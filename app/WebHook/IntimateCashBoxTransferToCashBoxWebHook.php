<?php


namespace App\WebHook;

use App\Http\Helpers\MiscHelpers;
use Carbon\Carbon;

/**
 * IntimateCashBoxTransferToB4UWallet constructor.
 */
class IntimateCashBoxTransferToCashBoxWebHook extends WebHook
{
    /**
     * @param string $endPoint
     * @Purpose Set End Point, where to hit the request
     */
    public function setEndPoint(string $endPoint)
    {
        $this->EndPoint = $endPoint;
    }

    /**
     * @param array $payload
     * @Purpose Set Pay Load, what to share
     * @Pyaload Example Should be like:::
     * [
     *      'sender_id' => null,
     *      'receiver_id' => null,
     *      'currency' => null,
     *      'amount' => null,
     * ]
     */
    public function setPayLoad(array $payload)
    {
        $this->PayLoad = $payload;
    }

    /**
     * @param array $headers
     * @Purpose Set Headers
     */
    public function setHeaders(array $headers)
    {
        $nonce = Carbon::now()->getPreciseTimestamp(3);
        $this->Headers = [
            'X-Auth-Signature' => hash_hmac('sha256', $nonce . config('b4uglobal.B4UWalletCashBoxTransactionIntimateApiKey'), config('b4uglobal.B4UWalletCashBoxTransactionIntimateApiSecret')),
            'X-Auth-Apikey' => config('b4uglobal.B4UWalletCashBoxTransactionIntimateApiKey'),
            'X-Auth-Nonce' => (int)$nonce,
        ];

        dump($this->Headers);
    }

    /**
     * @param array $PayLoad
     * @return bool
     * @Purpose Send request with data payload.
     */
    public static function send(array $PayLoad)
    {
        $selfThis = (new self());
        ($selfThis)->setEndPoint(config('b4uglobal.B4UWalletCashBoxTransactionIntimateURL'));
        ($selfThis)->setPayLoad($PayLoad);
        ($selfThis)->setHeaders($PayLoad);
        $response = ($selfThis)->sendRequest();
        return $response->successful();
    }

}
