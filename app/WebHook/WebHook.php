<?php


namespace App\WebHook;


use Illuminate\Support\Facades\Http;

abstract class WebHook
{
    protected $EndPoint;
    protected $PayLoad;
    protected $Headers;
    protected $Response;

    ## set end point
    abstract public function setEndPoint(string $endPoint);

    ## set payload
    abstract public function setPayLoad(array $payload);

    ## set headers
    public function setHeaders(array $headers)
    {
        return $this->Headers = $headers;
    }

    ## request to endpoint.
    public function sendRequest($method)
    {
        $this->Response = Http::withHeaders($this->Headers)->$method($this->EndPoint, $this->PayLoad);
        return $this->Response;
    }
}
