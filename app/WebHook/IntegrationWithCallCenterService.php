<?php


namespace App\WebHook;


use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class IntegrationWithCallCenterService extends WebHook
{

    public function setEndPoint(string $endPoint)
    {
        // TODO: Implement setEndPoint() method.
        $this->EndPoint = $endPoint;
    }

    public function setPayLoad(array $payload)
    {
        // TODO: Implement setPayLoad() method.
        $this->PayLoad = $payload;
    }

    public function setHeaders(array $headers)
    {
        $this->Headers = $headers;
    }

    public static function send(string $endPoint,array $payLoad = null,string $method =  'post')
    {
        try {
            $selfThis = (new self());
            ($selfThis)->setEndPoint(config('b4uglobal.CallCenterService') . $endPoint);
            ($selfThis)->setPayLoad($payLoad);
            ($selfThis)->setHeaders(config('b4uglobal.CallCenterServiceHeaders'));
            $response = ($selfThis)->sendRequest($method);
        }catch (\Exception $exception){
            Log::error('error while doing new entry in credit', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
            return true;
        }
        if($response->successful()) {
            return json_decode($response->body());
        }else{
            Log::error($response->body());
            return false;
        }
    }
}