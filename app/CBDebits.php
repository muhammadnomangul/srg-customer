<?php

namespace App;

use App\Jobs\DebitJob;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CBDebits extends Model
{
    /**
     * @var mixed
     */
    static $tableName = 'cb_debits';
    protected $table = 'cb_debits';
    static $tableNameCurrenciesColumnRef = 'cb_debits.currencies_id';


    /**
     * @param int $cashBoxAccountId
     * @param float $amount
     * @param float $fee
     * @param string|null $category
     * @param string|null $type
     * @param string|null $reason
     * @param string $status
     * @param bool $doMinsInLock
     * @param deposits|null $Deposit
     * @param int|null $transfer_to_cash_box_account
     * @param int|null $transfer_to_user
     * @return CBCredits|string
     *
     * @Purpose Create Debit of User.
     */
    public static function createDebit(int $cashBoxAccountId, float $amount, float $fee, string $category = null, string $type = null, string $reason = null, string $status = 'Pending',
                                       bool $doMinsInLock = false, deposits $Deposit = null, int $transfer_to_cash_box_account = null, int $transfer_to_user = null)
    {
            $CBDebit = null;
            $CBAccount = CBAccounts::find($cashBoxAccountId);
            $totalAmount = $amount + $fee;

            if ($CBAccount instanceof CBAccounts) {
                ## Query for the Update
                $updateResponse = DB::table('cb_accounts')
                    ->where('cb_accounts.id', $CBAccount->id)
                    ->where('cb_accounts.current_balance', '>=', $totalAmount)->lockForUpdate()->first();

                if ($updateResponse && !empty($updateResponse->id)) {
                    ## update current balance -- decrease it
                    $updateAccBalanceResponse = DB::table('cb_accounts')
                        ->where('cb_accounts.id', $updateResponse->id)->decrement('current_balance', $totalAmount);

                    ## if balance is decrease.
                    if ($updateAccBalanceResponse === 1) {
                        if ($doMinsInLock) {
                            ## decrease locked-balance
                            DB::table('cb_accounts')
                                ->where('cb_accounts.id', $updateResponse->id)->increment('locked_balance', $totalAmount);
                        }

                        ## create debit
                        $CBDebit = new CBDebits();
                        $CBDebit->user_id = $CBAccount->user_id;
                        $CBDebit->cb_account_id = $CBAccount->id;
                        $CBDebit->amount = $amount;
                        $CBDebit->fee = $fee;
                        $CBDebit->currencies_id = $CBAccount->currencies_id;
                        $CBDebit->category = $category;
                        $CBDebit->type = $type;
                        $CBDebit->reason = $reason;
                        $CBDebit->status = $status;
                        $CBDebit->transfer_to_cash_box_account_id = $transfer_to_cash_box_account;
                        $CBDebit->transfer_to_user_id = $transfer_to_user;
                        ## if deposit id is given
                        if ($Deposit) {
                            if ($Deposit instanceof deposits) {
                                ## validate deposit id.
                                $CBDebit->deposit_id = $Deposit->id;
                            } else {
                                throw new \Exception('No Such Deposit Found in our records.');
                            }
                        }
                        $CBDebit->save();
                    }
                }

                return $CBDebit;
            } else {
                throw  new \Exception('Cash box account not found');
            }

    }

    /**
     * @Purpose :: Approve Debit whenever Deposit approve
     * @param int $depositId
     * @return \Illuminate\Http\RedirectResponse
     */
    public static function approveDebitOnDepositApprove(int $depositId)
    {
        ## find deposit.
        $Deposit = \App\deposits::find($depositId);

        if ($Deposit instanceof \App\deposits && $Deposit->payment_mode == 'cashbox' && strtolower($Deposit->pre_status) == 'new') {
            ## find debit from deposit.
            $debitId = $Deposit->cb_debit_id;
            ## find is valid debit.
            $CBDebits = \App\CBDebits::find($debitId);


            if ($CBDebits instanceof \App\CBDebits && strtolower($CBDebits->status) == 'pending') {

                ## i found the debit now find the CashBox Account of User.
                $CBAccount = \App\CBAccounts::find($CBDebits->cb_account_id);

                if ($CBAccount instanceof \App\CBAccounts) {

                    ## check is enough locked balance available, to deduct.
                    $LockedBalance = CBAccounts::getCashBoxLockedBalanceByCurrencyCode($Deposit->currency);
                    if ($LockedBalance < $Deposit->amount) {
                        return back('It seems, This user has duplicate Deposits from CashBox because locked amount is not equal to deposit amount');
                    }

                    switch (strtolower($Deposit->status)) {
                        case 'approved':

                            $CBDebits->status = $Deposit->status;
                            $CBDebits->save();

                            $CBAccount->locked_balance = floatval($CBAccount->locked_balance) - floatval($CBDebits->amount);
                            $CBAccount->save();


                            break;
                        case 'cancelled':
                        case 'deleted':

                            $CBDebits->status = $Deposit->status;
                            $CBDebits->save();

                            $CBAccount->locked_balance = floatval($CBAccount->locked_balance) - floatval($CBDebits->amount);
                            $CBAccount->current_balance = floatval($CBAccount->current_balance) + floatval($CBDebits->amount);
                            $CBAccount->save();

                            break;
                        case 'pending':
                            return back()->with('errormsg', 'Deposit status is pending, Please approve it to approve Cash Box Debit.');
                    }

                } else {
                    return back()->with('errormsg', 'Cash Box Account is not found');
                }
            } else {
                return back()->with('errormsg', 'Cash Box Debit not found');
            }
        }
    }

    /**
     * @Purpose:: Update CashBox CBDebit , Credit Id.
     * @param CBDebits $CBDebits
     * @param CBCredits $CBCredits
     * @return CBDebits|string
     */
    public static function updateCBCreditId(CBDebits $CBDebits, CBCredits $CBCredits)
    {
        try {

            ## check is already credit id exits in debit table.
            $CBDebitsF = CBDebits::where('cb_credit_id', $CBCredits->id)->first();
            if ($CBDebitsF instanceof CBDebits) {
                return $CBDebitsF;
            }

            $CBDebits->cb_credit_id = $CBCredits->id;
            $CBDebits->save();
            return $CBDebits;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * @Purpose Get current balance with the deduction fee on CashOUt, or This function check is user allowed to CashOut From CashBox Account
     * @param string $currencyCode
     * @param int $feeOnWhatAmount
     * @return mixed
     */
    public static function isAllowedToCashOut(string $currencyCode, $feeOnWhatAmount)
    {
        ## Deduction Fee
        $deductionFee = 0;
        ## CashBox Balance
        $cashBoxBalance = CBAccounts::getCashBoxCurrentBalanceByCurrencyCode($currencyCode);
        $OuterObject = (CBAccounts::class);
        $currencyCode = strtolower($currencyCode);
        $shouldTransfer = false;
        $message = null;

        switch ($currencyCode) {
            case 'usd';
                $feeOnWhatAmountInUSD = $feeOnWhatAmount;
                if ($feeOnWhatAmount <= $OuterObject::cashOutB4UFeeMaxRange && $feeOnWhatAmount >= $OuterObject::cashOutB4UFeeMinRange) {
                    $deductionFee = $OuterObject::cashOutB4UFee;
                } else if ($feeOnWhatAmount > $OuterObject::cashOutB4UFeeMaxRange) {
                    $deductionFee = $OuterObject::cashOutB4UFeeOnBigAmount;
                }

                if ($feeOnWhatAmount >= $OuterObject::minimumCashOutTransferInDollars) {
                    $shouldTransfer = ($cashBoxBalance) >= ($feeOnWhatAmount + $deductionFee);
                } else {
                    $shouldTransfer = false;
                    $message = $OuterObject::minimumCashOutTransferErrorMessage;
                }

                ## Country Limit for Users
//                $loggedInUser = Auth::user();
//                if (strtolower($loggedInUser->Country) == 'pakistan' && $feeOnWhatAmountInUSD < 710) {
//                    $shouldTransfer = false;
//                    $message = $OuterObject::minimumCashOutForPakistaniUserTransferErrorMessage;
//                }

                break;
            case 'rsc':
                $ConvertedAmountInUSD = CBAccounts::ConvertCBAmountInUSD($currencyCode, $feeOnWhatAmount);
                $feeOnWhatAmountInUSD = $ConvertedAmountInUSD['usd_amount'];
                if ($feeOnWhatAmountInUSD >= $OuterObject::minimumCashOutTransferInDollars) {
                    ## no deduction fee on rsc so, just need to check is amount available how much we transfer amount.
                    $shouldTransfer = ($cashBoxBalance >= $feeOnWhatAmount);
                } else {
                    $shouldTransfer = false;
                    $message = $OuterObject::minimumCashOutTransferErrorMessage;
                }
                break;
            default:
                $ConvertedAmountInUSD = CBAccounts::ConvertCBAmountInUSD($currencyCode, $feeOnWhatAmount);

                $feeOnWhatAmountInUSD = $ConvertedAmountInUSD['usd_amount'];
                if ($feeOnWhatAmountInUSD >= $OuterObject::minimumCashOutTransferInDollars) {
                    ##
                    if ($feeOnWhatAmountInUSD <= $OuterObject::cashOutB4UFeeMaxRange && $feeOnWhatAmountInUSD >= $OuterObject::cashOutB4UFeeMinRange) {
                        $deductionFee = $OuterObject::cashOutB4UFee / $ConvertedAmountInUSD['currency_rate'];
                    } else if ($feeOnWhatAmountInUSD > $OuterObject::cashOutB4UFeeMaxRange) {
                        $deductionFee = $OuterObject::cashOutB4UFeeOnBigAmount / $ConvertedAmountInUSD['currency_rate'];
                    }
                }

                if ($feeOnWhatAmountInUSD >= $OuterObject::minimumCashOutTransferInDollars) {
                    ## should system do this transaction or not
                    $shouldTransfer = ($cashBoxBalance) >= ($feeOnWhatAmount + $deductionFee);
                } else {
                    $shouldTransfer = false;
                    $message = $OuterObject::minimumCashOutTransferErrorMessage;
                }
                break;
        }


        return [
            'should_transfer' => $shouldTransfer,
            'deduction_fee' => $deductionFee,
            'amount_to_cash_out' => $feeOnWhatAmount,
            'amount_user_have' => $cashBoxBalance,
            'total_amount_deduct_form_cas_box' => ($deductionFee + $feeOnWhatAmount),
            'currency' => $currencyCode,
            'message' => $message ? $message : ($shouldTransfer ? 'You can transfer Amount' : "Sorry, you don't have enough amount to CashOut"),
            'usd_amount' => empty($ConvertedAmountInUSD['usd_amount']) ? $feeOnWhatAmount : $ConvertedAmountInUSD['usd_amount']
        ];

    }


    /**
     * @Purpose Create CashBox debit on CashBox Deposit
     * @param int $cbDepositId
     * @return mixed
     */
    public static function createCashBoxDebitOnDepositCreate(int $cbDepositId)
    {
        try {
            /** @var deposits $Deposit */
            ## fetch deposit
            $Deposit = deposits::find($cbDepositId);

            if ($Deposit instanceof deposits && $Deposit->payment_mode == 'cashbox' && $Deposit->amount && $Deposit->currency) {

                /** @var User $User */
                ## fetch User
                $User = User::find($Deposit->user_id);
                if ($User instanceof User) {

                    /** @var CBAccounts $CashBoxAccount */
                    ## fetch the CashBox account who is going to transfer.
                    $CashBoxAccount = $User->getCashBoxAccountOfSpecificCurrency($Deposit->currency);
                    if ($CashBoxAccount instanceof CBAccounts) {

                        ## if CashBox account has been created
                        $CBDebit = CBDebits::createDebit($CashBoxAccount->id,
                            $Deposit->amount, 0, 'deposit',
                            'newdeposit', "Create Deposit in B4U Global D#{$Deposit->id}", 'pending',
                            true, $Deposit);

                        ##
                        if (!$CBDebit instanceof CBDebits) {
                            $Deposit->payment_mode = 'cashbox-failed';
                            $Deposit->save();
                        } else {
                            $Deposit->trans_id = "CB-DB-{$CBDebit->id}";
                            $Deposit->cb_debit_id = $CBDebit->id;
                            $Deposit->save();
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            Log::error('ErrorOnMakingDebitOnDeposit', [
                'message' => $exception->getMessage()
            ]);
        }
    }

    public static function approveDebit(CBDebits $cbDebit)
    {
        try {
            if($cbDebit->status == 'pending'){
                $cbDebit->status = 'approved';
                $cbDebit->save();

                if ($cbDebit){
                    $CBAccount = CBAccounts::find($cbDebit->cb_account_id);
                    $totalAmount = $cbDebit->amount + $cbDebit->fee;
                    $cbAccountResponse = CBAccounts::whereId($CBAccount->id)->where('locked_balance', '>=', $totalAmount)->lockForUpdate()->first();
                    ## if balance is decrease.
                    if ($cbAccountResponse) {
                            ## decrease locked-balance
                        return CBAccounts::whereId($cbAccountResponse->id)->decrement('locked_balance', $totalAmount);
                }
            }
        }
        }catch(\Exception $exception){
            Log::error('error while doing Approve Cb transfer', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
            return $exception->getMessage();
        }
    }
}
