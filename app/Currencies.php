<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
    protected $table = 'currencies';
    // protected $fillable = ['name','code','smallcode'];

    // Get currencies in object or get name of currencies.
    public static function getCurrencies($columnName = null)
    {
        $queryBuilder = Currencies::distinct('code')->where('status', 'Active')->orderBy('sort_order');
        if ($columnName) {
            $queryBuilder = $queryBuilder->pluck($columnName);
        } else {
            $queryBuilder = $queryBuilder->get();
        }
        return $queryBuilder;
    }


    /**
     * @param string $compareTableCurrencyId
     * @return mixed
     * @Purpose:: Get Currency Precision Value
     */
    public static function getCurrencyPrecisionQueryObject(string $compareTableCurrencyId)
    {
        return Currencies::selectRaw('currency_precision')->whereColumn('currencies.id', $compareTableCurrencyId);
    }

    /**
     * @param int $currencyId
     * @return mixed|null
     * @Purpose Get currency code by currency id.
     */
    public static function getCurrencyCodeById(int $currencyId)
    {
        $CurrencyRow = Currencies::find($currencyId);
        if ($CurrencyRow instanceof Currencies) {
            return $CurrencyRow->code;
        } else {
            return null;
        }
    }
}
