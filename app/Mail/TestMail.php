<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $orderPrice;
    public function __construct($orderPrice)
    {
        $this->orderPrice = $orderPrice;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from(getMailFromAddress(), getMailFromName())->subject('MESSAGE FROM THE CHAIRMAN B4U Group')
		->view('mails.test');
		
		/* ->attach(public_path('/images').'/image_boss.jpg', [
                        'as' => 'chairman_img.jpg',
                        'mime' => 'image/jpeg',
                      ]); */


    }
}
