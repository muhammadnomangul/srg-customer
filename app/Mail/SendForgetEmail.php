<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendForgetEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user_email;
    protected $user_otp;
    public function __construct($user_email, $user_otp)
    {

        $this->user_email = $user_email;
        $this->user_otp =  $user_otp;
    }

    public function build()
    {
        return $this->from(getMailFromAddress(), getMailFromName())->subject("Forget Password Email")
            ->view('mails.forgetPassword')->with(array('user_otp' => $this->user_otp));

        /* ->attach(public_path('/images').'/image_boss.jpg', [
                        'as' => 'chairman_img.jpg',
                        'mime' => 'image/jpeg',
                      ]); */
    }
}
