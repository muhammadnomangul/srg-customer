<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProfitPercentageEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

	protected $userName;
	protected $userUID;
	protected $userid;
	protected $lastProfitRate;

    public function __construct($userName,$userUID,$userid,$lastProfitRate)
    {
        $this->userName 		= $userName;
		$this->userUID 			= $userUID;
		$this->userid 			= $userid;
		$this->lastProfitRate 	= $lastProfitRate;
    }

    /**
     * Build the message.
     *
     * @return $this
    */
	
    public function build()
    {
        $userName 		= $this->userName;
		$userUID 		= $this->userUID;
		$userid 		= $this->userid;
		$lastProfitRate = $this->lastProfitRate;
		$subject		= "Today's profit B4U Global";
		
        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.profitPercentageEmail', compact('userName','userUID','userid','lastProfitRate'));

    }
}
