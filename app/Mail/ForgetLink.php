<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    protected $user_email;
    protected $user_id;
    protected $link;
    public function __construct($user_email, $user_id, $link)
    {

        $this->user_email = $user_email;
        $this->user_id =  $user_id;
        $this->link = $link;
    }

    public function build()
    {
        return $this->from(getMailFromAddress(), getMailFromName())->subject("Forget Email")
            ->view('mails.forgetPasswordLink')->with(array('link' => $this->link));

        /* ->attach(public_path('/images').'/image_boss.jpg', [
                        'as' => 'chairman_img.jpg',
                        'mime' => 'image/jpeg',
                      ]); */
    }
}