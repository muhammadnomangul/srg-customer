<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserRegistrations extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	protected $name;
	protected $password;
    protected $smsCode;
	protected $unique_id;
	protected $parent_id;

    public function __construct($name, $password, $smsCode, $unique_id, $parent_id)
    {
        $this->name 		= $name;
		$this->password 	= $password;
        $this->smsCode 		= $smsCode;
		$this->unique_id 	= $unique_id;
		$this->parent_id 	= $parent_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name 		= $this->name;
		$password 	= $this->password;
        $smsCode 	= $this->smsCode;
		$unique_id 	= $this->unique_id;
		$parent_id 	= $this->parent_id;
		$subject	= "Welcome B4U Global, You have registered On B4U Global successfully.";
        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.newRegisterations', compact('name', 'password', 'smsCode', 'unique_id', 'parent_id'));
    }
}
