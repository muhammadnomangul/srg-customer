<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CashboxReportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    /**
     * CashboxReportMail constructor.
     * @param $userId
     * @param $cbAccountId
     * @param $debitId
     */
    protected $userId;
    protected $cbAccountId;
    protected $debitId;
    protected $message;
    public function __construct($userId, $cbAccountId, $debitId)
    {
        $this->userId = $userId;
        $this->cbAccountId = $cbAccountId;
        $this->debitId = $debitId;
        $this->message =  "There is an issue in a cashbox balance of $userId having cashbox Id of $cbAccountId after transfer ID of $debitId";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getMailFromAddress(), getMailFromName())->subject("Cashbox Report Issue")->view('mails.supportTeamMail')->with(['msg' => $this->message,]);
    }

}