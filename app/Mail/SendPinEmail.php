<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception,Log;

class SendPinEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $userName;
    protected $userUID;
    protected $userid;
    protected $pin;


    public function __construct($userName, $userUID, $userid, $pin)
    {
        $this->userName = $userName;
        $this->userUID = $userUID;
        $this->userid = $userid;
        $this->pin = $pin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        try{
            $userName = $this->userName;
            $userUID = $this->userUID;
            $userid = $this->userid;
            $pin = $this->pin;
            $subject = "Email Verification Pin";
    
            return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.sendPinEmail', compact('userName', 'userUID', 'userid', 'pin'));
        }catch(Exception $e){
            \Illuminate\Support\Facades\Log::error("Something went wrong. in SendPinEmail ".$e->getMessage());
            return false;
        }
        
    }
}
