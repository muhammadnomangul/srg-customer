<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SpecialEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $userName;
    protected $userUID;
    protected $userid;
    protected $user_email;

    public function __construct($userName, $userUID, $userid, $user_email)
    {
        $this->userName = $userName;
        $this->userUID = $userUID;
        $this->userid = $userid;
        $this->pin = $user_email;
    }

    public function build()
    {
        return $this->from(getMailFromAddress(), getMailFromName())->subject("User Id has been changed")
        ->view('mails.special_mail')->with(array('userName' => $this->userName,'userUID' => $this->userUID));
        
        /* ->attach(public_path('/images').'/image_boss.jpg', [
                        'as' => 'chairman_img.jpg',
                        'mime' => 'image/jpeg',
                      ]); */
    }
}