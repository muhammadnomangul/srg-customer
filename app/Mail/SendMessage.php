<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $userName;
    protected $userUID;

    protected $msg;


    public function __construct($userName, $userUID, $msg)
    {
        $this->userName = $userName;
        $this->userUID = $userUID;
        // $this->userid = $userid;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        $userName = $this->userName;
        $userUID = $this->userUID;
        //  $userid = $this->userid;
        $msg = $this->msg;
        $subject = "Important Message";

        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.sendMessage', compact('userName', 'userUID', 'msg'));

    }
}
