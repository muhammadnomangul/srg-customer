<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RefNewUserMail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $name;
    protected $password;
    protected $smsCode;
    protected $unique_id;
    protected $parent_id;
    private $userEmail;
    private $b4uId;
    private $invitedUsername;
    private $inviteBy;

    /**
     * Create a new message instance.
     *
     * @param $userEmail
     * @param $password
     * @param $b4uId
     * @param $invitedUsername
     * @param $inviteBy
     */
    public function __construct($userEmail, $password, $b4uId, $invitedUsername, $inviteBy)
    {

        $this->userEmail = $userEmail;
        $this->password = $password;
        $this->b4uId = $b4uId;
        $this->invitedUsername = $invitedUsername;
        $this->inviteBy = $inviteBy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Congratulations, You are invited to B4U Global.";
        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.refNewUser', [
            'email' => $this->userEmail,
            'password' => $this->password,
            'b4uid' => $this->b4uId,
            'invitedUser' => $this->invitedUsername,
            'inviteBy' => $this->inviteBy
        ]);
    }
}
