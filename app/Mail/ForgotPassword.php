<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $userName;
    protected $userUID;
    protected $userid;
    protected $password;

    /**
     */

    public function __construct($userName, $userUID, $userid, $password)
    {
        $this->userName = $userName;
        $this->userUID = $userUID;
        $this->userid = $userid;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        $userName = $this->userName;
        $userUID = $this->userUID;
        $userid = $this->userid;
        $password = $this->password;
        $subject = "Forgot Password Pin";

        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.forgotpassword', compact('userName', 'userUID', 'userid', 'password'));

    }
}
