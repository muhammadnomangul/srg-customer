<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPartnerRegistrations extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $email_to;
    protected $parentName;
    protected $unique_id;


    public function __construct($email_to, $parentName, $unique_id)
    {
        $this->email_to 	= $email_to;
        $this->parentName 	= $parentName;
        $this->unique_id    = $unique_id;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_to 		= $this->email_to;
        $parentName 	= $this->parentName;
        $unique_id 		= $this->unique_id;
		$subject        = "B4U Global, A New User Registered At Your Downline.";
        return $this->from(getMailFromAddress(), getMailFromName())->subject($subject)->view('mails.newPartnerRegistrations', compact('email_to', 'parentName', 'unique_id'));
    }
}
