<?php


namespace App\TwoFAAuth\Authentication;


class TwoFAAuthenticateFactory
{
    /**
     * @Purpose : Enable Two FA process
     * @param $serviceName
     * @return EmailTwoFAAuthenticate|GoogleTwoFAAuthenticate|SMSTwoFAAuthenticate|null
     */
    public static function getInstance($serviceName)
    {
        $instanceResponse = null;
        switch ($serviceName) {
            case 0:
                $instanceResponse = EmailTwoFAAuthenticate::getInstance();
                break;
            case 1:
                $instanceResponse = SMSTwoFAAuthenticate::getInstance();
                break;
            case 2:
                $instanceResponse = GoogleTwoFAAuthenticate::getInstance();
                break;
            case 5:
                $instanceResponse = UserPinTwoFAAuthenticate::getInstance();
                break;
        }
        return $instanceResponse;
    }
}