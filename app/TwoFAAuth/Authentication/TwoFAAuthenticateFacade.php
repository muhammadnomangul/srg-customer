<?php


namespace App\TwoFAAuth\Authentication;


use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FAQRCode\Google2FA;

class TwoFAAuthenticateFacade extends TwoFAAuthenticateFactory
{


    /**
     * @Steps to Enable OTP
     * 1. Wrap all forms input fields into div of class form-content
     * 2. Add class to form submit button is, o-s and change button type form submit to button
     * 3. Add attribute mod with the form element to tell which type of otp is ending..
     * 4 Implement middle 2faAuth at route where form is posting...
     * */

    public static $verificationMessageAvailableFor = [
        0, 1, 2, 5,
    ];

    public static $b4u2faMethods = [
        'none' => [
            'name' => 'None',
            'extraInfo' => '',
            'cost' => '',
            'value' => 3,
            'badge_class' => 'success'
        ],

        'email' => [
            'name' => 'Email',
            'extraInfo' => '',
            'cost' => 'Free',
            'value' => 0,
            'badge_class' => 'success'
        ],

        'google_auth' => [
            'name' => 'Google Authenticator',
            'extraInfo' => "<a href='https://youtu.be/17rykTIX_HY' target='_blank'>What is it?</a>",
            'cost' => 'Free',
            'value' => 2,
            'badge_class' => 'success'
        ],
//        'pin_auth' => [
//            'name' => 'Setup Pin/Password',
//            'extraInfo' => null,
//            'cost' => 'Free',
//            'value' => 4,
//            'badge_class' => 'success'
//        ],

        'pin_auth' => [
            'name' => 'Setup Pin/Password',
            'extraInfo' => null,
            'cost' => 'Free',
            'value' => 5,
            'badge_class' => 'success'
        ],

        'sms' => [
            'name' => '(SMS/CALL) & Email',
            'extraInfo' => '',
            'cost' => '$ 0.07/SMS',
            'value' => 1,
            'badge_class' => 'danger'
        ],
    ];


    private function __construct()
    {
    }


    public static function b4uAllowed2FaMethod()
    {
        if (!Auth::user()->email_verified_at && !empty(self::$b4u2faMethods['email'])) {
            unset(self::$b4u2faMethods['email']);
        }
        return self::$b4u2faMethods;
    }

    /**
     * @param $serviceName
     * @return EmailTwoFAAuthenticate|GoogleTwoFAAuthenticate|SMSTwoFAAuthenticate|null
     * @Purpose: Get Instance
     */
    public static function getInstance($serviceName)
    {
        return parent::getInstance($serviceName);
    }

    /**
     * @param int $currentOtp
     * @param User $user
     * @param string $enteredOtp
     * @param string $otpType
     * @param null $request
     * @return bool|void
     * @Purpose:: Authenticate Otp
     */
    public static function authenticate(int $currentOtp, User $user, \Illuminate\Http\Request $request = null)
    {
        return self::getInstance($currentOtp)->authenticate($user, $request);
    }


    /**
     * @param int $currentOtp
     * @param User $user
     * @param string $otpOf
     * @return bool|void
     * @Purpose :: Send Otp to user
     */
    public static function sendOtp(int $currentOtp, User $user, string $otpOf)
    {
        return self::getInstance($currentOtp)->sendOtp($user, $otpOf);
    }


    /**
     * @param Google2FA $google2FA
     * @return bool|Application|Factory|View|void
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     * @Purpose :: Send Otp to user
     */
    public static function getBarCodeScannerImage(Google2FA $google2FA)
    {
        $registrationData['2fa_secret'] = $google2FA->generateSecretKey();
        $image = $google2FA->getQRCodeInline(env('APP_NAME'), Auth::user()->email, $registrationData['2fa_secret']);
        return view("user.googleauthi", [
            'QR_Image' => $image,
            'secret' => $registrationData['2fa_secret'],
        ]);

    }

    public static function savedPinSetup()
    {
        return view("user.savepin", [
            'QR_Image' => null,
            'secret' => null,
        ]);

    }


    /**
     * @purpose Merge six input otp fields into one.
     * @param $request
     * @return string
     */
    public static function mergeSixOneTimePasswordInputIntoOne($request)
    {
        $one_time_password = $request->get('one') . $request->get('two') . $request->get('three') . $request->get('four') . $request->get('five') . $request->get('six');
        return $request->request->set('one_time_password', $one_time_password);
    }

    /**
     * @Purpose Get Authenticate message
     * @param int $currentOtp
     * @return string
     */
    public static function getAnOtpMessage(int $currentOtp)
    {
        if (in_array($currentOtp, self::$verificationMessageAvailableFor)) {
            return self::getInstance($currentOtp)->getMessage();
        } else {
            return 'Please type verification code';
        }
    }


}