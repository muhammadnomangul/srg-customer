<?php


namespace App\TwoFAAuth\Authentication;


use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

abstract class TwoFAAuthenticate
{
    public $MaxAttempts = 3;

    ## Authenticate Otp
    abstract public function authenticate(User $user, Request $request);

    ## Send Otp
    abstract public function sendOtp(User $loggedInUser, string $otpType);

    ## Generate Otp
    abstract public function generateOtp(User $user, string $otpOf): string;

    ## Get latest Otp
    abstract public function getLatestOtp(User $user, string $otpType);

    ## redirect success response
    abstract public function redirectSuccessResponse(User $user, string $oty_type);

    ## redirect failure response
    abstract public function redirectFailureResponse(int $userId);

    ## redirect success response
    abstract public function redirectSuccessResponseOfSendOtp(int $userId, string $otp): JsonResponse;

    ## redirect failure response
    abstract public function redirectFailureResponseOfSendOtp(string $errorMessage): JsonResponse;

    ## validate receiver
    abstract public function validateReceiver(string $validate): bool;

    abstract public function logging(string $logMessage, array $logDetails = []);

    ## get authenticate message.
    abstract public function getMessage();

    ## block user if he/she tries 3 time wrong password.

    /**
     * @purpose Block profile on wrong attempts
     * @param int $userId
     * @return string
     */
    public function blockProfileOnWrongAttempts(int $userId)
    {
        $UserObject = User::find($userId);
        $maxAttemptsResponse = $UserObject->increment('max_attempts', 1);
        if ($maxAttemptsResponse && $UserObject->max_attempts >= $this->MaxAttempts) {
            $UserObject->status = 'wrong_attempt';
            $UserObject->wrong_attempt_suspended_at = Carbon::now();
            $isBlocked = $UserObject->save();
            if ($isBlocked) {
                return url('dashboard');
            }
        }

        return $UserObject->status;
    }

    /**
     * @purpose Flush Max Attempts
     * @param int $userId
     * @return bool
     */
    public function flushAttempts(int $userId)
    {
        $UserObject = User::find($userId);
        if ($UserObject instanceof User) {
            $UserObject->max_attempts = 0;
            $UserObject->wrong_attempt_suspended_at = null;
            return $isBlocked = $UserObject->save();
        }
        return true;
    }
}