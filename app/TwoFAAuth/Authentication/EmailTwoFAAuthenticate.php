<?php


namespace App\TwoFAAuth\Authentication;


use App\Mail\SendPinEmail;
use App\OTPToken;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailTwoFAAuthenticate extends TwoFAAuthenticate
{

    /**
     * @var null
     * @Purpose Create an instance
     */
    private static $instance = null;
    private static $otpTypePrefix = 'email_';

    /**
     * EmailTwoFAAuthenticate constructor.
     * @Purpose Make Singleton and will not access by new instance.
     */
    private function __construct()
    {
    }

    /**
     * @return EmailTwoFAAuthenticate|null
     * @Purpose:: Create a Singleton Instance
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new EmailTwoFAAuthenticate();
        }
        return self::$instance;
    }

    /**
     * @param User $user
     * @param Request $request
     * @return bool
     * @Purpose :: Authenticate OTP
     */
    public function authenticate(User $user, Request $request)
    {
        $latestOtp = (int)$this->getLatestOtp($user, $request->request->get('mod', 'tmp'));
        if ($latestOtp == $request->request->get('etp')) {
            return $this->redirectSuccessResponse($user, $request->request->get('mod', 'tmp'));
        } else {
            return $this->redirectFailureResponse($user->id);
        }
    }

    /**
     * @param User $loggedInUser
     * @param string $otpOf
     * @return JsonResponse
     */
    public function sendOtp(User $loggedInUser, string $otpOf): JsonResponse
    {
        try {
            $validateReceiver = $this->validateReceiver($loggedInUser->email);
            if ($validateReceiver) {
                $otpPin = self::generateOtp($loggedInUser, $otpOf);
                ## send email
                Mail::to($loggedInUser->email)->send(new SendPinEmail($loggedInUser->name, $loggedInUser->u_id, $loggedInUser->id, $otpPin));
                return $this->redirectSuccessResponseOfSendOtp($loggedInUser->id, $otpPin);
            } else {
                return $this->redirectFailureResponseOfSendOtp('One Time Password email is not sent to user, ' . $loggedInUser->id . ', ');
            }
        } catch (\Exception $e) {
            return $this->redirectFailureResponseOfSendOtp($e->getMessage());
        }
    }

    /**
     * @param User $user
     * @param string $otpOf
     * @Purpose :: Generate new Otp
     * @return string
     */
    public function generateOtp(User $user, string $otpOf): string
    {
        $OtpToken = OTPToken::generateEmailOtp($user, $otpOf);
        return (string)$OtpToken->getDecryptedCode();
    }

    /**
     * @param User $user
     * @param string $otpType
     * @Purpose Get the last Otp to authenticate
     * @return Model|HasMany|object|null
     */
    public function getLatestOtp(User $user, string $otpType)
    {
        $otpToken = $user->otpTokens()->where('otp_type', self::$otpTypePrefix . $otpType)->where('otp_tokens.user_id', $user->id)->where('otp_tokens.used', 0)->latest()->first();
        if ($otpToken instanceof OTPToken) {
            return $otpToken->getDecryptedCode();
        } else {
            return null;
        }
    }

    /**
     * @param User $user
     * @param string $oty_type
     * @return bool
     * @purpose:: Return success response, if code has been matched
     */
    public function redirectSuccessResponse(User $user, string $oty_type)
    {
        OTPToken::expireOtpToken($user, self::$otpTypePrefix . $oty_type);
        $this->flushAttempts($user->id);
        return true;
    }

    /**
     * @param int $userId
     * @return false
     * @Purpose:: Return failure response, if code didn't matched
     */
    public function redirectFailureResponse(int $userId)
    {
        $this->blockProfileOnWrongAttempts($userId);
        return false;
    }

    /**
     * @param string $validate
     * @return bool
     * @Purpose Validate is email is write or not.
     */
    public function validateReceiver(string $validate): bool
    {
        return filter_var($validate, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param int $id
     * @param string $otpPin
     * @return JsonResponse
     * @purpose:: Return success response, if code has been matched
     */
    public function redirectSuccessResponseOfSendOtp(int $id, string $otpPin): JsonResponse
    {
        ## Log Send Otp
        $this->logging('send Otp via email to user, ' . $id, [
            $otpPin,
        ]);
        return response()->json([
            'status' => true,
            'message' => 'Otp has been sent'
        ]);
    }

    /**
     * @param $errMessage
     * @return JsonResponse
     * @Purpose:: Return failure response, if code didn't matched
     */
    public function redirectFailureResponseOfSendOtp($errMessage): JsonResponse
    {
        $this->logging("Something went wrong in send Otp email " . $errMessage);
        return response()->json([
            'status' => false,
            'message' => $errMessage
        ]);
    }

    public function logging(string $logMessage, array $logDetails = [])
    {
        if (env('APP_DEBUG', false)) {
            Log::error($logMessage, $logDetails);
        }
    }

    public function getMessage()
    {
        return 'An Otp has been sent to Register Email Address';
    }
}