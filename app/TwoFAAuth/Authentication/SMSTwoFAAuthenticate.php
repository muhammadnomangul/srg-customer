<?php


namespace App\TwoFAAuth\Authentication;


use App\Jobs\SendOtp;
use App\Mail\SendPinEmail;
use App\OTPToken;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SMSTwoFAAuthenticate extends TwoFAAuthenticate
{
    private static $instance = null;
    private static $otpTypePrefix = 'phone_';

    private function __construct()
    {
    }

    /**
     * @return SMSTwoFAAuthenticate
     * @Purpose:: Create a Singleton Instance
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new SMSTwoFAAuthenticate();
        }
        return self::$instance;
    }


    /**
     * @param User $user
     * @param Request $request
     * @return bool
     * @Purpose :: Authenticate OTP
     */
    public function authenticate(User $user, Request $request)
    {
        $latestOtp = (int)$this->getLatestOtp($user, $request->request->get('mod', 'tmp'));
        if ($latestOtp == $request->request->get('etp')) {
            return $this->redirectSuccessResponse($user, $request->request->get('mod', 'tmp'));
        } else {
            return $this->redirectFailureResponse($user->id);
        }
    }

    /**
     * @param User $loggedInUser
     * @param string $otpOf
     * @return JsonResponse
     */
    public function sendOtp(User $loggedInUser, string $otpOf)
    {
        try {

            if ($this->validateReceiver(($loggedInUser->phone_no ? $loggedInUser->phone_no : 00))) {
                $otpPin = self::generateOtp($loggedInUser, $otpOf);
                ## send otp,
                ## At sms send both email and sms for the OTP.
                Mail::to($loggedInUser->email)->send(new SendPinEmail($loggedInUser->name, $loggedInUser->u_id, $loggedInUser->id, $otpPin));
                SendOtp::dispatch($loggedInUser->phone_no, $otpPin, $loggedInUser->Country, $loggedInUser->id);

                return $this->redirectSuccessResponseOfSendOtp($loggedInUser->id, $otpPin);
            } else {
                return $this->redirectFailureResponseOfSendOtp('Invalid User phone number');
            }
        } catch (\Exception $e) {
            return $this->redirectFailureResponseOfSendOtp("Something went wrong in send Otp via SMS " . $e->getMessage());
        }
    }


    /**
     * @param User $user
     * @param string $otpOf
     * @Purpose :: Generate new Otp
     * @return string
     */
    public function generateOtp(User $user, string $otpOf): string
    {
        $OtpToken = OTPToken::generateSMSOtp($user, $otpOf);
        return (string)$OtpToken->getDecryptedCode();
    }

    /**
     * @param User $user
     * @param string $otpType
     * @Purpose Get the last Otp to authenticate
     * @return Model|HasMany|object|null
     */
    public function getLatestOtp(User $user, string $otpType)
    {
        $otpToken = $user->otpTokens()->where('otp_type', self::$otpTypePrefix . $otpType)->where('otp_tokens.user_id', $user->id)->where('otp_tokens.used', 0)->latest()->first();
        if ($otpToken instanceof OTPToken) {
            return $otpToken->getDecryptedCode();
        } else {
            return null;
        }
    }

    /**
     * @param User $user
     * @param string $oty_type
     * @return bool
     * @purpose:: Return success response, if code has been matched
     */
    public function redirectSuccessResponse(User $user, string $oty_type)
    {
        OTPToken::expireOtpToken($user, self::$otpTypePrefix . $oty_type);
        $this->flushAttempts($user->id);
        return true;
    }

    /**
     * @param int $userId
     * @return false
     * @Purpose:: Return failure response, if code didn't matched
     */
    public function redirectFailureResponse(int $userId)
    {
        $this->blockProfileOnWrongAttempts($userId);
        return false;
    }

    /**
     * @param string $validate
     * @return bool
     * @Purpose Validate Phone Number
     */
    public function validateReceiver(string $validate): bool
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        try {
            $swissNumberProto = $phoneUtil->parse($validate);
            $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::NATIONAL);
            return $phoneUtil->isValidNumber($swissNumberProto);
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }
    }

    /**
     * @param int $id
     * @param string $otpPin
     * @return JsonResponse
     * @purpose:: Return success response, if code has been matched
     */
    public function redirectSuccessResponseOfSendOtp(int $id, string $otpPin): JsonResponse
    {
        ## Log Send Otp
        $this->logging('send Otp via sms to user, ' . $id, [
            $otpPin,
        ]);
        return response()->json([
            'status' => true,
            'message' => 'Otp has been sent'
        ]);
    }

    /**
     * @param $errMessage
     * @return JsonResponse
     * @Purpose:: Return failure response, if code didn't matched
     */
    public function redirectFailureResponseOfSendOtp($errMessage): JsonResponse
    {
        $this->logging($errMessage);
        return response()->json([
            'status' => false,
            'message' => $errMessage
        ]);
    }


    public function logging(string $logMessage, array $logDetails = [])
    {
        if (env('APP_DEBUG', false)) {
            Log::error($logMessage, $logDetails);
        }
    }

    public function getMessage()
    {
        return 'An Otp has been sent to Register Email Address and Registered Phone number';
    }

}