<?php


namespace App\TwoFAAuth\Authentication;


use App\OTPToken;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class UserPinTwoFAAuthenticate extends TwoFAAuthenticate
{
    private static $instance = null;
    private static $otpTypePrefix = 'savedpin_';


    private function __construct()
    {
    }

    /**
     * @return null
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new UserPinTwoFAAuthenticate();
        }
        return self::$instance;
    }

    public function authenticate(User $user, Request $request)
    {

        $latestOtp = (int)$this->getLatestOtp($user, $request->request->get('mod', 'temp'));
        if ($latestOtp == $request->request->get('etp')) {
            return $this->redirectSuccessResponse($user, $request->request->get('mod'));
        } else {
            return $this->redirectFailureResponse($user->id);
        }

    }

    public function sendOtp(User $loggedInUser, string $otpType)
    {
        // TODO: Implement sendOtp() method.
    }

    public function generateOtp(User $user, string $otpOf): string
    {
    }

    public function getLatestOtp(User $user, string $otpType)
    {
        return $user->getDecryptedTwoFAPIN();
    }

    public function redirectSuccessResponse(User $user, string $oty_type)
    {
        $this->flushAttempts($user->id);
        return true;
    }

    public function redirectFailureResponse(int $userId)
    {
        $this->blockProfileOnWrongAttempts($userId);
        return false;
    }

    public function redirectSuccessResponseOfSendOtp(int $userId, string $otp): JsonResponse
    {
    }

    public function redirectFailureResponseOfSendOtp(string $errorMessage): JsonResponse
    {
    }

    public function validateReceiver(string $validate): bool
    {
        // TODO: Implement validateReceiver() method.
    }

    public function logging(string $logMessage, array $logDetails = [])
    {
    }

    public function getMessage()
    {
        return 'Please enter your secure PIN';
    }
}