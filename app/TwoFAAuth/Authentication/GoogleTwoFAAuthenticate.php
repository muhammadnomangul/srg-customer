<?php


namespace App\TwoFAAuth\Authentication;


use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class GoogleTwoFAAuthenticate extends TwoFAAuthenticate
{
    private static $instance = null;

    private function __construct()
    {
    }

    /**
     * @return null
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new GoogleTwoFAAuthenticate();
        }
        return self::$instance;
    }

    public function authenticate(User $user, Request $request)
    {

        $request->request->set('one_time_password', $request->request->get('etp', 'temp'));
        $authenticator = app(Authenticator::class)->boot($request);
        if ($authenticator->isAuthenticated() && $user->google2fa_secret) {
            return $this->redirectSuccessResponse($user, $request->request->get('mod', 'temp'));
        } else {
            return $this->redirectFailureResponse($user->id);
        }

    }

    public function sendOtp(User $loggedInUser, string $otpType)
    {
        // TODO: Implement sendOtp() method.
    }

    public function generateOtp(User $user, string $otpOf): string
    {
        // TODO: Implement generateOtp() method.
    }

    public function getLatestOtp(User $user, string $otpType)
    {
        // TODO: Implement getLatestOtp() method.
    }

    public function redirectSuccessResponse(User $user, string $oty_type = null)
    {
        $this->flushAttempts($user->id);
        return true;
    }

    public function redirectFailureResponse(int $userId)
    {
        $this->blockProfileOnWrongAttempts($userId);
        return false;
    }

    public function redirectSuccessResponseOfSendOtp(int $userId, string $otp): JsonResponse
    {
        // TODO: Implement redirectSuccessResponseOfSendOtp() method.
    }

    public function redirectFailureResponseOfSendOtp(string $errorMessage): JsonResponse
    {
        // TODO: Implement redirectFailureResponseOfSendOtp() method.
    }

    public function validateReceiver(string $validate): bool
    {
        // TODO: Implement validateReceiver() method.
    }

    public function logging(string $logMessage, array $logDetails = [])
    {
        // TODO: Implement logging() method.
    }

    public function getMessage()
    {
        return 'An Otp has been sent to Google Authenticator App';
    }

}