<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UserRating extends Model
{
    public static function CreateUserRating(int $userId,int $cbAccountId,string $type = null,float $amount,string $status,int $rating,int $creditId = null,int $debitId = null,string $comments = null,int $ratingBy){
        try {
            $userRating = new self();
            $userRating->user_id = $userId;
            $userRating->cb_account_id = $cbAccountId;
            $userRating->type = $type;
            $userRating->amount = $amount;
            $userRating->status = $status;
            $userRating->rating = $rating;
            $userRating->credit_id = $creditId;
            $userRating->debit_id = $debitId;
            $userRating->comments = $comments;
            $userRating->rating_by = $ratingBy;
            $userRating->save();

            return $userRating;
        }catch (\Exception $exception){
            Log::Error('Something went wrong in CreateUserRating',[
                 'errorMessage' => $exception->getMessage(),
                'errorFile' => $exception->getFile(),
                'errorLine' => $exception->getLine()
            ]);

            return  "ErrorCode: ". $exception->getCode() .  "ErrorMessage: ". $exception->getMessage();
        }
    }
}
