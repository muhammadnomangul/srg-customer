<?php

namespace App\Jobs\CashBox;

use App\CBAccounts;
use App\CBCredits;
use App\CBDebits;
use App\Jobs\CashBox\B4UWallet\SendTransferRequestToB4UWalletJob;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CreateCreditFundTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $cbDebitId;
    private $status;

    /**
     * Create a new job instance.
     * @param int $cbDebitId
     * @param int $user_id
     * @param string $status
     */

    // worker use::php artisan queue:work --queue=cbft
    public function __construct(int $cbDebitId, int $user_id,string $status = 'approved')
    {
        $this->queue = 'cb_' . getUserGroup($user_id);
        $this->cbDebitId = $cbDebitId;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {

        DB::transaction(function () {
            ## Find Debit.
            $CBDebit = CBDebits::find($this->cbDebitId);
            if ($CBDebit instanceof CBDebits) {
                ## debit found.
                $TransferToCashBoxAccount = CBAccounts::find($CBDebit->transfer_to_cash_box_account_id);
                $TransferToUser = User::find($CBDebit->transfer_to_user_id);
                $TransferFromCashBoxAccount = CBAccounts::find($CBDebit->cb_account_id);
                $TransferFromUser = User::find($CBDebit->user_id);

                if ($TransferToCashBoxAccount instanceof CBAccounts && $TransferToUser instanceof User && $TransferFromCashBoxAccount instanceof CBAccounts && $TransferFromUser instanceof User) {
                    ## Create Credit.
                  /*  $CBCredit = CBCredits::createCredit($TransferToCashBoxAccount->id, $CBDebit->amount, 0, 'balance', 'transfer', 'Internal Transfer from B4U User #' . $TransferFromUser->u_id,
                        $this->status, $CBDebit->id, $TransferFromCashBoxAccount->id, $TransferFromUser->id);
                    if ($CBCredit instanceof CBCredits && $CBCredit->id) {
                        CBDebits::updateCBCreditId($CBDebit, $CBCredit);*/
                    if($CBDebit->status == 'inprocessing'){
                        $CBCredit =  CBCredits::createInternalTransferCreditFromCBDebit($CBDebit->id,'pending');
                    }else{
                        $CBCredit = CBCredits::createInternalTransferCreditFromCBDebit($CBDebit->id,$CBDebit->status);
                    }
                    if ($CBCredit) {
                        Log::info("Credit Created against {$CBDebit->id}");
                    } else {
                        Log::error('In-Transfer, Failed to Create CashBox Credit but debit has been created', [
                            'credit' => $CBCredit
                        ]);
                    }
                }

            }
        },1);

        ## Intimate B4U Wallet that we've successfully transfer in B4U Global.
      //  SendTransferRequestToB4UWalletJob::dispatch($this->cbDebitId);
    }
}
