<?php

namespace App\Jobs\CashBox\B4UWallet;

use App\CBDebits;
use App\Currencies;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendTransferRequestToB4UWalletJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $cbDebitId;

    /**
     * Create a new job instance.
     *
     * @param int $cbDebitId
     */
    public function __construct(int $cbDebitId)
    {
        $this->queue = 'bfuwallet';
        $this->cbDebitId = $cbDebitId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $CBDebit = CBDebits::find($this->cbDebitId);

        if ($CBDebit instanceof CBDebits) {
            ## transfer amount
            $TransferAmount = $CBDebit->amount;
            ## transfer currency
            $TransferCurrency = Currencies::getCurrencyCodeById($CBDebit->currencies_id);
            ## sender user id
            $SenderId = $CBDebit->user_id;
            ## receiver user id
            $ReceiverId = $CBDebit->transfer_to_user_id;

            if ($TransferAmount && $TransferCurrency && $SenderId && $ReceiverId) {
                ## intimate b4uWallet that we a cashBox transaction
                $cashBoxIntimateResponse = \App\WebHook\IntimateCashBoxTransferToCashBoxWebHook::send([
                    ## send -- there should be sender email
                    'member' => User::getUserEmailById($SenderId),
                    ## receiver -- there should be receiver email
                    'r_id' => User::getUserEmailById($ReceiverId),
                    ## currency
                    'currency' => $TransferCurrency,
                    ## amount.
                    'amount' => $TransferAmount
                ]);

                Log::info('CashBoxTransferIntimationResponse', [
                    'responseResult' => $cashBoxIntimateResponse
                ]);
            }

        }
    }
}
