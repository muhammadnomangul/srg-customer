<?php

namespace App\Jobs\CashBox;

use App\CBAccounts;
use App\CBDebits;
use App\deposits;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CreateDebitOnDeposit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $cbDepositId;
    /**
     * @var int
     */
    private $user_id;

    /**
     * Create a new job instance.
     *
     * @param int $cbDepositId
     * @param int $user_id
     */
    public function __construct(int $cbDepositId, int $user_id)
    {
        $this->queue = 'cb_' . getUserGroup($user_id);
        $this->cbDepositId = $cbDepositId;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('test working... ');
//        $CBDepositId = $this->cbDepositId;
//        return DB::transaction(function () use ($CBDepositId) {
//            CBDebits::createCashBoxDebitOnDepositCreate($CBDepositId);
//        }, config('b4uglobal.RETRY_CASH_BOX_TRANS'));
    }
}
