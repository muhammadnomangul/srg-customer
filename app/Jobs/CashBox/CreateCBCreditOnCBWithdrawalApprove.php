<?php

namespace App\Jobs\CashBox;

use App\CBCredits;
use App\withdrawals;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateCBCreditOnCBWithdrawalApprove implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $withdrawalId;

    /**
     * Create a new job instance.
     *
     * @param int $withdrawalId
     * @param int $user_id
     */
    public function __construct(int $withdrawalId, int $user_id)
    {
        //
        $this->queue = 'cb_' . getUserGroup($user_id);
        $this->withdrawalId = $withdrawalId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $Withdrawal = withdrawals::find($this->withdrawalId);
        if ($Withdrawal instanceof withdrawals) {
            CBCredits::createCBCreditOnCBWithdrawalApprove($Withdrawal);
        }
    }
}
