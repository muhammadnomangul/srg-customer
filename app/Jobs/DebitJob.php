<?php

namespace App\Jobs;

use App\CBDebits;
use App\User;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


/**
 * @deprecated
 * */
class DebitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var CBDebits
     */
    private $CBDebits;

    /**
     * Create a new job instance.
     *
     * @param int $user_id
     */
    public function __construct(int $No)
    {
        $this->queue = 'cb_' . $No;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        Log::info('Job is processing.... CB Queue is working..');
    }
}
