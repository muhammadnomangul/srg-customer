<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client as TwilioClient;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $contactNo;
    protected $randomNo;
    protected $country;
    /**
     * @var null
     */
    private $userId;

    public function __construct($contactNo, $randomNo, $country, $userId = null)
    {
        $this->contactNo = $contactNo;
        $this->randomNo = $randomNo;
        $this->country = $country;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return string|void
     * @throws GuzzleException
     * @throws ConfigurationException
     * @throws TwilioException
     */
    public function handle()
    {
        if (empty($this->userId)) {
            $this->userId = 123;
        }

        if (env('APP_ENV', 'local') == 'local' || env('APP_DEBUG', true)) {
            Log::info('sending-Phone-call-on-phone-no', [
                'phone-no' => $this->contactNo
            ]);
            sendOtpToUserByCall2($this->contactNo, $this->randomNo, $this->userId);
            ## don't send message at following conditions.
        } elseif (substr($this->contactNo, 0, 3) == '+92') {
            sendOtpToUserByCall2($this->contactNo, $this->randomNo, $this->userId);
        } else {
            $contactNo = str_replace(' ', '', $this->contactNo);
            $twilioAccountSid = env('TWILIO_SID', 'AC186454122ca47adb623506126d65edf1');
            $twilioAuthToken = env('TWILIO_TOKEN', '696077df95c2a4376991856bb6fbdbc1');
            $twilioFromNumber = 'B4U Global';
            $client = new TwilioClient($twilioAccountSid, $twilioAuthToken);
            $clientMessageResponse = $client->messages->create(
                trim($contactNo),
                array(
                    'from' => $twilioFromNumber,
                    'body' => 'Dear Customer, Your OTP is : ' . $this->randomNo
                )
            );
//            User::increase_user_sms_credit($this->userId, 1);
        }


    }
}
