<?php

namespace App\Jobs;

use App\CBAccounts;
use App\CBDebits;
use App\Mail\CashboxReportMail;
use App\Mail\OtpEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CashboxGenerateReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $debitId;
    protected $cbAccountId;
    protected $userId;
    protected $user_id;

    public function __construct($userId, $cbAccountId, $debitId,$user_id)
    {
        $this->queue = 'cb_' . getUserGroup($user_id);
        $this->debitId = $debitId;
        $this->cbAccountId = $cbAccountId;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $balanceCalculation = CBAccounts::balanceCalculation($this->cbAccountId);
            $cbAccount = CBAccounts::whereId($this->cbAccountId)->first();
            $cbDebit = CBDebits::whereId($this->debitId)->first();
            $supportEmail = "hafiznaeemkhan0306@gmail.com";
           // $supportEmail = "mubixhb4u@gmail.com";
            if ((round($balanceCalculation->expected_current,0) == round($cbAccount->current_balance,0)) &&
                (round($balanceCalculation->expected_locked,0)   == round($cbAccount->locked_balance,0) )
            ) {
                $cbDebit->status = 'pending';
                $cbDebit->save();
            } else {
                $cbDebit->is_defaulter = 1;
                $cbDebit->save();
                Mail::to($supportEmail)->send(new CashboxReportMail($this->userId, $this->cbAccountId, $this->debitId));
            }
        }catch (\Exception $exception){
            Log::error('error while generation cb Report', [
                'errorMessage' => $exception->getMessage(),
                'errorLine' => $exception->getLine(),
                'errorFile' => $exception->getFile()
            ]);
        }
    }
}
