<?php


namespace App\Http\Controllers;


use App\Console\Commands\PlansCron;
use App\Model\Referral;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\settings;

use App\users;

use App\withdrawals;

use App\deposits;

use App\solds;

use App\confirmations;

use App\plans;

use App\Account;

use App\currency_rates;

use App\UserAccounts;

use App\email_templates;

use App\daily_profit_bonus;

use App\daily_investment_bonus;

use App\referal_profit_bonus_rules;

use App\ref_investment_bonus_rules;

use App\ref_profit_bonus_rules;
use App\Currencies;
use DB;

use DateTime;
use Mail;


class PaymentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
        set_time_limit(0);
        // $this->middleware('auth');
        //saveLogs($title,$details,$userid,$curr,$amt,$pre_amt,$approvedby)
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    //payment route

    public function payment(Request $request)
    {
        $amount = $request->session()->get('amount');
        $payment_mode = $request->session()->get('payment_mode');
        $pay_type = $request->session()->get('pay_type');
        $plan_id = $request->session()->get('plan_id');
        $deposit_mode = $request->session()->get('deposit_mode');
        $currency = $request->session()->get('currency');
        if ($request->session()->get('reinvest_type')) {
            $reinvest_type = $request->session()->get('reinvest_type');
        } else {
            $reinvest_type = "";
        }

        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        $ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();
        $settings = settings::getSettings();
        $activeDeposits = deposits::where('user_id', Auth::user()->id)->where('status', 'Approved')->get();
        $totalActiveDeposits = count($activeDeposits);
        //echo "Curr :".$currency." deposit_mode :".$deposit_mode." reinvest_type :".$reinvest_type ;
        //exit;
        if ($deposit_mode == "reinvest") {
            if (isset($currency) && isset($ratesQuery)) {
                $curr = strtolower($currency);
                $rateVal = "rate_" . $curr;
                $currencyRate = $ratesQuery->$rateVal;
                $accProfit = "profit_" . $curr;
                $userProfit = $userAccInfo->$accProfit;
                $accBalSold = "sold_bal_" . $curr;
                $userbalanceSold = $userAccInfo->$accBalSold;
                $reference_bonus = $userAccInfo->reference_bonus;
                //$accBal				  = "balance_".$curr;
                //$userbalance			  = $userAccInfo->$accBal;
                //$accWaitingProfit		  = "waiting_profit_".$curr;
                //$userwaitingProfit	  = $userAccInfo->$accWaitingProfit;

                $totalUsd = $amount * $currencyRate;
                if ($currency == "USD" && $reinvest_type == "Bonus") {

                    if ($totalUsd < $settings->reinvest_limit || $amount > $reference_bonus) {
                        return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . $settings->reinvest_limit . ' Or You have insufficient balance for this request!');
                    } else if ($totalActiveDeposits == 0) {
                        return redirect()->intended('dashboard/deposits')->with('errormsg', 'Bonus Reinvestment Not Allowed ! You have no approved deposits in your deposits account.');
                    }
                } else if ($currency != "USD" && $reinvest_type == "Bonus") {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid request! selected currency not allowed for reinvest Bonus');
                } else if ($reinvest_type == "Profit" && ($totalUsd < $settings->reinvest_limit || $amount > $userProfit)) {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . $settings->reinvest_limit . '! Or You have insufficient balance for this request!');
                } else if ($reinvest_type == "Sold" && ($totalUsd < $settings->reinvest_limit || $amount > $userbalanceSold)) {
                    return redirect()->intended('dashboard/deposits')->with('errormsg', 'Amount is less than ' . $settings->reinvest_limit . '! Or You have insufficient balance for this request!');
                }
            } else {
                return redirect()->intended('dashboard/deposits')->with('errormsg', 'Invalid Currency or Rates');
            }
        }
        $currencies = Currencies::distinct('code')->where('status', 'Active')->get();
        $title = 'Make deposit';
        //Return payment page
        return view('payment', ['currencies' => $currencies, 'amount' => $amount, 'currency' => $currency, 'payment_mode' => $payment_mode, 'reinvest_type' => $reinvest_type, 'deposit_mode' => $deposit_mode, 'pay_type' => $pay_type, 'plan_id' => $plan_id, 'settings' => $settings, 'title' => $title]);

    }


    public function amountLimitCheck(Request $request)
    {
        //Investment amount info
        $amount = $request['amount'];
        $curr = $request['curr'];
        $currencies = Currencies::distinct('code')->where('status', 'Active')->get();
        $ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();
        $settings = settings::getSettings();
        if ($curr != "USD" || $curr != "usd") {
            $currency = strtolower($curr);
            $rateVal = "rate_" . $currency;
            $rate = $ratesQuery->$rateVal;
            $totalUsd = $amount * $rate;
        } else {
            $totalUsd = $amount;
        }
        echo $totalUsd;
        exit;
    }

    public function deposit(Request $request)
    {
        //store payment info in session
        $request->session()->put('amount', $request['amount']);
        $request->session()->put('payment_mode', $request['payment_mode']);
        $request->session()->put('deposit_mode', $request['deposit_mode']);
        $request->session()->put('currency', $request['currency']);
        if (isset($request['reinvest_type'])) {
            $request->session()->put('reinvest_type', $request['reinvest_type']);
        }
        if (isset($request['pay_type'])) {
            $request->session()->put('pay_type', $request['pay_type']);
            $request->session()->put('plan_id', $request['plan_id']);
        }
        $settings = settings::getSettings();
        $title = 'Make deposit';
        $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();
        $ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();

        return redirect()->route('payment', ['settings' => $settings, 'title' => $title, 'accountsInfo' => $userAccInfo, 'ratesQuery' => $ratesQuery]);
    }

    //Return deposit route for customers
    public function deposits()
    {
        $deposits = deposits::where('user_id', Auth::user()->id)
            ->where('status', '!=', 'Cancelled')
            ->where('status', '!=', 'Deleted')
            ->orderby('created_at', 'DESC')->get();

        $settings = settings::getSettings();
        $title = 'Deposits';
        $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();
        //echo "<pre>";
        $currencies = Currencies::distinct('code')->where('status', 'Active')->get();
        //print_r($currencies);
        //exit;
        $ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();
        return view('deposits', ['settings' => $settings, 'title' => $title, 'deposits' => $deposits, 'accountsInfo' => $userAccInfo, 'ratesQuery' => $ratesQuery, 'currencies' => $currencies]);
    }


    //Save deposit requests
    public function savedeposit(Request $request)
    {
        
        if ($request->deposit_mode == "reinvest") {

            $this->middleware('2faAuth');
            $validator = $this->validate($request, [
                'amount' => 'required|numeric',
                'payment_mode' => 'required',
                'reinvest_type' => 'required',
            ]);
        } else {
            $validator = $this->validate($request, [
                'trans_id' => 'required|string|max:150',
                /** @deprecated 16-Aug-2020 */
//                'proof' 		=> 'mimes:jpg,jpeg,png|max:3000',
                'amount' => 'required|numeric',
                'payment_mode' => 'required',
                // 'currency' => 'required',
            ]);
        }
        if ($validator != '') {

            //$parent_id=Auth::user()->ref_by;
            $logeduser_id = Auth::user()->id;
            $plan_id = Auth::user()->plan;
            $logeduser_uid = Auth::user()->u_id;

            $invested_amount = $request['amount'];
            $payment_mode = $request['payment_mode'];
            $currency = $request['currency'];
            /** @deprecated 16-Aug-2020 */
//            $img = $request->file('proof');
            $image = '';

            /** @deprecated 16-Aug-2020 */
//            if ($img != "") {
//                $upload_dir = 'uploads';
//                $time = date("dhis");
//                $image = $time . "_" . $img->getClientOriginalName();
//                $move = $img->move($upload_dir, $image);
//            }

            if (isset($request['trans_id'])) {
                $trans_id = $request['trans_id'];
                $trans_type = "NewInvestment";
            } else if ($request->deposit_mode == "reinvest") {
                $trans_id = 'reinvest';
                $trans_type = "Reinvestment";
            } else {
                $trans_id = '';
                $trans_type = "NewInvestment";
            }

            if (isset($request['reinvest_type'])) {
                $reinvest_type = $request['reinvest_type'];
            } else {
                $reinvest_type = "";
            }

            if (isset($request['deposit_mode'])) {
                $deposit_mode = $request['deposit_mode'];
            } else {
                $deposit_mode = "";
            }

            $total_amount = "";
            $trade_profit = 0;
            $currencyRate = "";

            // Total Amount in dollers
            $ratesQuery = DB::table('currency_rates')->orderby('created_at', 'DESC')->first();

            if (isset($currency) && isset($ratesQuery)) {
                $curr = strtolower($currency);
                $rateVal = "rate_" . $curr;
                $currencyRate = $ratesQuery->$rateVal;
            }
            $total_amount = floatval($invested_amount) * floatval($currencyRate);


            $plan_id2 = Auth::user()->plan;

            $flag = 0;

            $balance = 0;
            $settings = settings::getSettings();
            $userAccInfo = UserAccounts::where('user_id', $logeduser_id)->first();
            if (!isset($userAccInfo)) {
                $userAccs = new UserAccounts();
                $userAccs->user_id = Auth::user()->id;
                $userAccs->save();
                $userAccInfo = UserAccounts::where('id', Auth::user()->id)->first();
            }
            $reference_bonus = $userAccInfo->reference_bonus;
            $reference_bonus2 = $userAccInfo->reference_bonus;
            if (isset($currency)) {
                $curr = strtolower($currency);
                $accBal = "balance_" . $curr;
                $userbalance = $userAccInfo->$accBal;
                $accBalSold = "sold_bal_" . $curr;
                $userbalanceSold = $userAccInfo->$accBalSold;
                $userbalanceSold2 = $userAccInfo->$accBalSold;
                $accProfit = "profit_" . $curr;
                $userProfit = $userAccInfo->$accProfit;
                $userProfit2 = $userAccInfo->$accProfit;
                $accWaitingProfit = "waiting_profit_" . $curr;
                $userwaitingProfit = $userAccInfo->$accWaitingProfit;

            }
            if ($deposit_mode == "reinvest") {
                if ($reinvest_type == "Bonus") {
                    $reference_bonus = $reference_bonus - $invested_amount;
                    if ($reference_bonus >= 0) {
                        UserAccounts::where('user_id', $logeduser_id)->update(['reference_bonus' => $reference_bonus]);
                        $pre_amt = $reference_bonus2;
                        $curr_amt = $reference_bonus;
                    } else {
                        $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . $settings->deposit_limit . '.';
                        return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                    }
                } else if ($reinvest_type == "Profit") {
                    $userProfit = $userProfit - $invested_amount;
                    if ($userProfit >= 0) {
                        UserAccounts::where('user_id', $logeduser_id)->update([$accProfit => $userProfit]);
                        $pre_amt = $userProfit2;
                        $curr_amt = $userProfit;
                    } else {
                        $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . $settings->deposit_limit . '.';
                        return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                    }
                } else if ($reinvest_type == "soldBalance" || $reinvest_type == "Sold" || $reinvest_type == "Balance") {
                    $userbalanceSold = $userbalanceSold - $invested_amount;
                    if ($userbalanceSold >= 0) {
                        UserAccounts::where('user_id', $logeduser_id)->update([$accBalSold => $userbalanceSold]);
                        $pre_amt = $userbalanceSold2;
                        $curr_amt = $userbalanceSold;
                    } else {
                        $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . $settings->deposit_limit . '.';
                        return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
                    }
                }

                $title = $reinvest_type . " Reinvestment";
                $details = $logeduser_uid . " has " . $deposit_mode . " deposits of " . $reinvest_type . " (" . $currency . ")" . $invested_amount;
                //	$pre_amt 	= 0;
                //	$curr_amt   = 0;
                $approvedby = "";
                // Save Logs
                $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $invested_amount, $pre_amt, $curr_amt, $approvedby);

            }
            if (($deposit_mode == "new" && $total_amount >= $settings->deposit_limit) || ($deposit_mode == "reinvest" && $total_amount >= $settings->reinvest_limit)) {
                // trade save

                $dp = new deposits();

                $dp->amount = $invested_amount;

                $dp->payment_mode = $payment_mode;

                $dp->currency = $currency;

                $dp->rate = $currencyRate;

                $dp->total_amount = $total_amount;

                $dp->plan = $plan_id2;

                $dp->user_id = $logeduser_id;

                $dp->status = 'Pending';

                $dp->pre_status = 'New';

                $dp->flag_dummy = 0;
                if ($deposit_mode == "reinvest") {
                    $dp->profit_take = 1;  // on reinvest bonus should not distributed to parents.
                } else {
                    $dp->profit_take = 0;
                }
                if (isset($trade_profit)) {

                    $dp->trade_profit = $trade_profit;

                }
                if (isset($image)) {

                    $dp->proof = $image;

                }
                if (isset($plan_id2)) {
                }
                if (isset($trans_id)) {

                    $dp->trans_id = $trans_id;

                }
                $dp->trans_type = $trans_type;
                if (isset($reinvest_type)) {

                    $dp->reinvest_type = $reinvest_type;
                }

                $dp->save();

                $last_deposit_id = $dp->id;
                //	$last_deposit_uniqueid 	= 	$dp->unique_id;

                $parent_id = Auth::user()->parent_id;

            } else {
                $msg = 'Deposits not successful! Your Deposit amount is insufficient, minimum deposit limit is $' . $settings->deposit_limit . '.';
                return redirect()->intended('dashboard/deposits')->with('errormsg', $msg);
            }
            $useremil = Auth::user()->email;
            if ($useremil != "") {
                $email_to = $useremil;
                $userName = Auth::user()->name;
                $from_Name = "B4U Global";
                $from_email = "noreply@b4utrades.com";
                $subject = "Your Deposit is Successful";
                $message =
                    "<html>
                         <body align=\"left\" style=\"height: 100%;\">
                            <div>
								
								<div>
									<table style=\"width: 100%;\">
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Dear " . $userName . ",
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												You have successfully deposits to " . $from_Name . ", You have invested <br><strong>" . $invested_amount . " " . $currency . ". </strong><br>Your deposits id is <strong>D-" . $last_deposit_id . ".</strong>
												
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												You deposits will start getting profit, as soon as Admin approved your deposits.
											</td>
										</tr>
										
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Thanks for using  " . $from_Name . ".
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Your Sincerely,
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Team " . $from_Name . "
											</td>
										</tr>
										
									</table>
								</div>
							</div>
						</body>
					</html>";

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From:B4U Global Deposits <' . $from_email . '>' . "\r\n";
                // More headers info
                //$headers .= 'From: Noreply <noreply@b4uinvestors.cf>' . "\r\n";
                //$headers .= 'Cc: myboss@example.com' . "\r\n";

                $success = @mail($email_to, $subject, $message, $headers);
                // Kill the session variables
            }
            if ($deposit_mode == "new") {
                $title = $deposit_mode . " Investment";
                $details = $logeduser_uid . " has " . $deposit_mode . " deposits (" . $currency . ")" . $invested_amount;
                $pre_amt = 0;
                $curr_amt = 0;
                $approvedby = "";
                // Save Logs
                $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $invested_amount, $pre_amt, $curr_amt, $approvedby);
            }


            // close all sessions
            $request->session()->forget('plan_id');

            $request->session()->forget('pay_type');

            $request->session()->forget('payment_mode');

            $request->session()->forget('amount');

            $request->session()->forget('deposit_mode');
            $request->session()->forget('currency');

            if ($request->session()->get('reinvest_type')) {
                $request->session()->forget('reinvest_type');
                //exit("all sessions reset");
            }

            $msg = 'Deposits Successful! Please wait for system to validate your request';

            return redirect()->intended('dashboard/deposits')->with('message', $msg);
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }


    /////////////####################////////////////////

    public function totalSubUsersInvestment($id)
    {
        $userinfo = users::where('id', $id)->first();
        $loged_user_id = $userinfo->id;
        $loged_parent_id = $userinfo->parent_id;
        $Loged_user_uid = $userinfo->u_id;

        $ratesQuery = currency_rates::orderby('id', 'desc')->first();

        $usdRate = $ratesQuery->rate_usd;
        $bitcoinRate = $ratesQuery->rate_btc;
        $bitcashRate = $ratesQuery->rate_bch;
        $ethereumRate = $ratesQuery->rate_eth;
        $litecoinRate = $ratesQuery->rate_ltc;
        $rippleRate = $ratesQuery->rate_xrp;
        $dashRate = $ratesQuery->rate_dash;
        $zcashRate = $ratesQuery->rate_zec;


        $secondLine = [];
        $thirdLine = [];
        $fourthLine = [];
        $fifthLine = [];
        $secondTotal = 0;
        $thirdTotal = 0;
        $fourthTotal = 0;
        $fifthTotal = 0;

        $firstLine = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.u_id', 'users.parent_id', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp')
            ->where('users.parent_id', $Loged_user_uid)
            ->get();

        $firstTotal1 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_usd as acc_bal_usd')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_usd');
        $firstTotal2 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_btc as acc_bal_btc')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_btc');
        $firstTotal3 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_eth as acc_bal_eth')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_eth');
        $firstTotal4 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_ltc as acc_bal_ltc')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_ltc');

        $firstTotal5 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_bch as acc_bal_bch')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_bch');
        $firstTotal6 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_xrp as acc_bal_xrp')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_xrp');
        $firstTotal7 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_dash as acc_bal_dash')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_dash');
        $firstTotal8 = DB::table('users')
            ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.parent_id', 'user_accounts.balance_zec as acc_bal_zec')
            ->where('parent_id', '=', $Loged_user_uid)->sum('user_accounts.balance_zec');

        //echo $firstTotal1." BTC = ". $firstTotal2 ." ETH = ".$firstTotal3 ." LTC = ".$firstTotal4 ." BCH = ".$firstTotal5." XRP = ". +$firstTotal6 ;

        $firstTotal = $firstTotal1 + $firstTotal2 * $bitcoinRate + $firstTotal3 * $ethereumRate + $firstTotal4 * $litecoinRate + $firstTotal5 * $bitcashRate + $firstTotal6 * $rippleRate + $firstTotal7 * $dashRate + $firstTotal8 * $zcashRate;

        if (isset($firstLine)) {
            $totalAmount = 0;
            $totalUsd = 0;
            $totalBtc = 0;
            $totalEth = 0;
            $totalBch = 0;
            $totalLtc = 0;
            $totalXrp = 0;
            $totalDash = 0;
            $totalZec = 0;

            $counter = 0;
            $row = 0; //$firstTotal = count($firstLine);
            for ($i = 0; $i < count($firstLine); $i++) {
                // $result = DB::table('users')->select('u_id','parent_id')->where('parent_id', $firstLine[$i]->u_id)->get();
                $result = DB::table('users')
                    ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                    ->select('users.u_id', 'users.parent_id', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                        'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                    ->where('parent_id', $firstLine[$i]->u_id)->get();
                $count2 = count($result);
                if ($count2 == 0) {
                    continue;
                }
                array_push($secondLine, $result);
                for ($j = 0; $j < $count2; $j++) {
                    $totalUsd = $totalUsd + $result[$j]->usd;
                    $totalBtc = $totalBtc + $result[$j]->btc;
                    $totalEth = $totalEth + $result[$j]->eth;
                    $totalLtc = $totalLtc + $result[$j]->ltc;
                    $totalBch = $totalBch + $result[$j]->bch;
                    $totalXrp = $totalXrp + $result[$j]->xrp;
                    $totalDash = $totalDash + $result[$j]->dash;
                    $totalZec = $totalZec + $result[$j]->zec;
                }
                $counter++;
                //$row = $row+$count2;
            }
            $secondLine = $secondLine;
            $totalAmount = $totalUsd + $totalBtc * $bitcoinRate + $totalEth * $ethereumRate + $totalBch * $bitcashRate + $totalLtc * $litecoinRate + $totalXrp * $rippleRate + $totalDash * $dashRate + $totalZec * $zcashRate;
            $secondTotal = $totalAmount;
            //$secondTotal = $row;
        }
        if (isset($secondLine)) {
            $totalAmount2 = 0;
            $totalUsd2 = 0;
            $totalBtc2 = 0;
            $totalEth2 = 0;
            $totalBch2 = 0;
            $totalLtc2 = 0;
            $totalXrp2 = 0;
            $totalDash2 = 0;
            $totalZec2 = 0;
            $counter2 = 0;
            $rows = 0;

            for ($i = 0; $i < count($secondLine); $i++) {
                foreach ($secondLine[$counter2] as $lines) {
                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($thirdLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd2 = $totalUsd2 + $result[$j]->usd;
                        $totalBtc2 = $totalBtc2 + $result[$j]->btc;
                        $totalEth2 = $totalEth2 + $result[$j]->eth;
                        $totalLtc2 = $totalLtc2 + $result[$j]->ltc;
                        $totalBch2 = $totalBch2 + $result[$j]->bch;
                        $totalXrp2 = $totalXrp2 + $result[$j]->xrp;
                        $totalDash2 = $totalDash2 + $result[$j]->dash;
                        $totalZec2 = $totalZec2 + $result[$j]->zec;
                    }
                    //$rows = $rows+$count2;
                }
                $counter2++;
            }
            $thirdLine = $thirdLine;
            $totalAmount2 = $totalUsd2 + $totalBtc2 * $bitcoinRate + $totalEth2 * $ethereumRate + $totalBch2 * $bitcashRate + $totalLtc2 * $litecoinRate + $totalXrp2 * $rippleRate + $totalDash2 * $dashRate + $totalZec2 * $zcashRate;;

            $thirdTotal = $totalAmount2; // $thirdTotal = $rows;
        }
        if (isset($thirdLine)) {
            $totalAmount3 = 0;
            $totalUsd3 = 0;
            $totalBtc3 = 0;
            $totalEth3 = 0;
            $totalBch3 = 0;
            $totalLtc3 = 0;
            $totalXrp3 = 0;
            $totalDash3 = 0;
            $totalZec3 = 0;
            $counter3 = 0;
            $rows1 = 0;//$count = count($thirdLine);
            for ($i = 0; $i < count($thirdLine); $i++) {
                foreach ($thirdLine[$counter3] as $lines) {

                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();

                    $count2 = count($result);

                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fourthLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd3 = $totalUsd3 + $result[$j]->usd;
                        $totalBtc3 = $totalBtc3 + $result[$j]->btc;
                        $totalEth3 = $totalEth3 + $result[$j]->eth;
                        $totalLtc3 = $totalLtc3 + $result[$j]->ltc;
                        $totalBch3 = $totalBch3 + $result[$j]->bch;
                        $totalXrp3 = $totalXrp3 + $result[$j]->xrp;
                        $totalDash3 = $totalDash3 + $result[$j]->dash;
                        $totalZec3 = $totalZec3 + $result[$j]->zec;
                    }
                    // $rows1 = $rows1+$count2;
                }
                $counter3++;
            }
            $fourthLine = $fourthLine;

            $totalAmount3 = $totalUsd3 + $totalBtc3 * $bitcoinRate + $totalEth3 * $ethereumRate + $totalBch3 * $bitcashRate + $totalLtc3 * $litecoinRate + $totalXrp3 * $rippleRate + $totalDash3 * $dashRate + $totalZec3 * $zcashRate;;

            $fourthTotal = $totalAmount3; //$fourthTotal  = $rows1;
        }

        if (isset($fourthLine)) {
            $totalAmount4 = 0;
            $totalUsd4 = 0;
            $totalBtc4 = 0;
            $totalEth4 = 0;
            $totalBch4 = 0;
            $totalLtc4 = 0;
            $totalXrp4 = 0;
            $totalDash4 = 0;
            $totalZec4 = 0;
            $counter4 = 0;
            $rows2 = 0;
            for ($i = 0; $i < count($fourthLine); $i++) {

                foreach ($fourthLine[$counter4] as $lines) {
                    $result = DB::table('users')
                        ->leftJoin('user_accounts', 'user_accounts.user_id', '=', 'users.id')
                        ->select('users.u_id', 'users.parent_id', 'user_accounts.balance_usd as usd', 'user_accounts.balance_btc as btc',
                            'user_accounts.balance_eth as eth', 'user_accounts.balance_ltc as ltc', 'user_accounts.balance_bch as bch', 'user_accounts.balance_xrp as xrp', 'user_accounts.balance_dash as dash', 'user_accounts.balance_zec as zec')
                        ->where('parent_id', $lines->u_id)->get();
                    $count2 = count($result);
                    if ($count2 == 0) {
                        continue;
                    }
                    array_push($fifthLine, $result);
                    for ($j = 0; $j < $count2; $j++) {
                        $totalUsd4 = $totalUsd4 + $result[$j]->usd;
                        $totalBtc4 = $totalBtc4 + $result[$j]->btc;
                        $totalEth4 = $totalEth4 + $result[$j]->eth;
                        $totalLtc4 = $totalLtc4 + $result[$j]->ltc;
                        $totalBch4 = $totalBch4 + $result[$j]->bch;
                        $totalXrp4 = $totalXrp4 + $result[$j]->xrp;
                        $totalDash4 = $totalDash4 + $result[$j]->dash;
                        $totalZec4 = $totalZec4 + $result[$j]->zec;
                    }
                    // $rows2 = $rows2+$count2;
                }
                $counter4++;
            }
            $fifthLine = $fifthLine;
            $totalAmount4 = $totalUsd4 + $totalBtc4 * $bitcoinRate + $totalEth4 * $ethereumRate + $totalBch4 * $bitcashRate + $totalLtc4 * $litecoinRate + $totalXrp4 * $rippleRate + $totalDash4 * $dashRate + $totalZec4 * $zcashRate;;

            $fifthTotal = $totalAmount4; //  $fifthTotal = $rows2;
        }
        //echo "FirstLine - ".$firstTotal."SecondLine - ".$secondTotal. "ThirdLine - ".$thirdTotal."FourthLine - ".$fourthTotal."FifthLine - ".$fifthTotal;
        //exit;
        $total = $firstTotal + $secondTotal + $thirdTotal + $fourthTotal + $fifthTotal;

        return $total;
        exit;
    }

    /////////////////##################//////////////

    //update to processed deposits

    /////////////////##################//////////////

    function calculateParentBonus($deposit_id)
    {
        $deposit = deposits::where('id', $deposit_id)->first();
        $total_amount = $deposit->total_amount;
        $userid = $deposit->user_id;
        $flag_dummy = $deposit->flag_dummy;
        $profit_take = $deposit->profit_take;
        $trans_Type = $deposit->trans_type;
        $currency = $deposit->currency;
        $amount = $deposit->amount;

        $deposit_user = users::where('id', $userid)->first();
        $plan_id = $deposit_user->plan;
        $user_uid = $deposit_user->u_id;
        $parent_id = $deposit_user->parent_id;
        $user_awarded_flag = $deposit_user->awarded_flag;

        if (($profit_take == 0 || $profit_take == "0") && ($flag_dummy == 0 && $user_awarded_flag == 0 && $trans_Type != "Reinvestment")) {
            $exitsBonuses = daily_investment_bonus::where('trade_id', $deposit_id)->first();
            $exitsBonuses1 = daily_investment_bonus::where('trade_id', $deposit_id)->where('details', 'NOT LIKE', 'Profit Bonus')->first();
            // Now Get latest Accounts Balances and rates and update Plans of Users
            if ((!isset($exitsBonuses) || !isset($exitsBonuses1)) && ($deposit_id != "" && $parent_id != "0" && $parent_id != "B4U0001")) {

                $count = 1;
                $calculatedBonus = 0;
                for ($i = 0; $i < 5; $i++) {
                    $parentDetails = DB::table('users')->select('id', 'u_id', 'parent_id', 'plan')->where('u_id', $parent_id)->first();

                    $Parent_userID = $parentDetails->id;
                    $parentPlanid = $parentDetails->plan;
                    $parentNewId = $parentDetails->parent_id;
                    $parent_uid = $parentDetails->u_id;

                    //$parent_uid 	= $parentDetails->u_id;
                    $plansDetailsQuery = DB::table('plans')
                        ->join('referal_investment_bonus_rules AS refinvbonus', 'plans.id', '=', 'refinvbonus.plan_id')
                        ->select('refinvbonus.first_line', 'refinvbonus.second_line', 'refinvbonus.third_line', 'refinvbonus.fourth_line', 'refinvbonus.fifth_line')
                        ->where('plans.id', $parentPlanid)->first();

                    $investment_bonus_line1 = $plansDetailsQuery->first_line;
                    $investment_bonus_line2 = $plansDetailsQuery->second_line;
                    $investment_bonus_line3 = $plansDetailsQuery->third_line;
                    $investment_bonus_line4 = $plansDetailsQuery->fourth_line;
                    $investment_bonus_line5 = $plansDetailsQuery->fifth_line;

                    if (floatval($investment_bonus_line1) > 0 && $count == 1) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line1)) / 100;
                        $percentage = $investment_bonus_line1;
                    } else if (floatval($investment_bonus_line2) > 0 && $count == 2) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line2)) / 100;
                        $percentage = $investment_bonus_line2;
                    } else if (floatval($investment_bonus_line3) > 0 && $count == 3) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line3)) / 100;
                        $percentage = $investment_bonus_line3;
                    } else if (floatval($investment_bonus_line4) > 0 && $count == 4) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line4)) / 100;
                        $percentage = $investment_bonus_line4;
                    } else if (floatval($investment_bonus_line5) > 0 && $count == 5) {
                        $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line5)) / 100;
                        $percentage = $investment_bonus_line5;
                    }
                    //$bonus = $calculatedBonus;

                    if ($calculatedBonus > 0 && $user_uid != "B4U0001" && $parent_id != "B4U0001") {
                        $daily_ibonus = new daily_investment_bonus();

                        $daily_ibonus->trade_id = $deposit_id;

                        $daily_ibonus->user_id = $user_uid;

                        $daily_ibonus->parent_id = $parent_id;
                        $daily_ibonus->parent_plan = $parentPlanid;

                        $daily_ibonus->bonus = $calculatedBonus;
                        //$daily_ibonus->bonus		= 	$bonus;
                        $daily_ibonus->details = "Investment Bonus with percentage of " . $percentage;

                        $daily_ibonus->save();

                        $savedb = $daily_ibonus->id;

                        $new_user_id = $daily_ibonus->user_id;
                    }
                    // Update Account will be on Update status

                    $parent_id = $parentNewId;
                    //echo "save data".$count." ids =".$savedid;
                    if ($parent_id == '0' || $parent_uid == "B4U0001") {
                        //echo $parent_id;
                        break;
                    }
                    $calculatedBonus = 0;
                    $count++;

                } // end of loop

            }
            $daily_investment_bonus = daily_investment_bonus::where('trade_id', $deposit_id)->get();

            foreach ($daily_investment_bonus as $investmentbonus) {
                $current_bonus = $investmentbonus->bonus;
                $user_uid = $investmentbonus->parent_id;
                if ($user_uid != '') {
                    $parent_user = users::where('u_id', $user_uid)->first();
                    $ref_user_id = $parent_user->id;
                    $ref_user_uid = $parent_user->u_id;

                    $userAccDetails = UserAccounts::where('user_id', $ref_user_id)->first();
                    $reference_bonus = $userAccDetails->reference_bonus;
                    $reference_bonus2 = $userAccDetails->reference_bonus;
                    if ($current_bonus > 0) {
                        $reference_bonus = $reference_bonus + $current_bonus;
                        UserAccounts::where('user_id', $ref_user_id)->update(['reference_bonus' => $reference_bonus, 'latest_bonus' => $current_bonus]);
                        $title = "Reference Bonus Updated";
                        $details = $ref_user_id . " has updated bonus to " . $reference_bonus;
                        $pre_amt = $reference_bonus2;
                        $curr_amt = $reference_bonus;
                        $currency = "";
                        $approvedby = "";
                        // Save Logs
                        $this->saveLogs($title, $details, $ref_user_id, $user_uid, $currency, $current_bonus, $pre_amt, $curr_amt, $approvedby);
                        $event = "User Deposit Approved";
                        $admin_id = Auth::user()->u_id;
                        $user_id = $user_uid;
                        $this->adminLogs($user_id, $admin_id, $deposit_id, $event);
                    }
                }
            }// end foreach loop
            deposits::where('id', $deposit_id)->update(['profit_take' => 1]);
            return "successful";
        }
        return "unsuccessful";
    }

    //update to processed deposits

    public function approvedDeposit(Request $request)
    {
        $id = $request['id'];
        $approvedDate = date('Y-m-d');
        if (Auth::user()->type == 1 || Auth::user()->type == 2) {
            $deposit = deposits::where('id', $id)->first();
            $total_amount = $deposit->total_amount;
            $userid = $deposit->user_id;
            $unique_id = "D-" . $deposit->id;
            $flag_dummy = $deposit->flag_dummy;
            $profit_take = $deposit->profit_take;
            $pre_status = $deposit->pre_status;
            $transID = $deposit->trans_id;
            $trans_Type = $deposit->trans_type;
            $currency = $deposit->currency;
            $amount = $deposit->amount;

            $deposit_user = users::where('id', $userid)->first();
            $plan_id = $deposit_user->plan;
            $user_uid = $deposit_user->u_id;
            $parent_id = $deposit_user->parent_id;
            $user_awarded_flag = $deposit_user->awarded_flag;

            // Get user Accounts Details
            $userAccQuery = UserAccounts::where('user_id', $userid)->first();
            if (!isset($userAccQuery)) {
                $userAccs = new UserAccounts();
                $userAccs->user_id = $userid;
                $userAccs->save();
                $userAccQuery = UserAccounts::where('user_id', $userid)->first();
            }
            //$latest_bonus		=	$userAccInfo->latest_bonus;
            $reference_bonus = $userAccQuery->reference_bonus;
            $trade_id = $request['id'];
            if ($profit_take == 0 || $trans_Type == "Reinvestment") {
                if (isset($currency)) {
                    $curr = strtolower($currency);
                    $accBal = "balance_" . $curr;
                    $userbalance = $userAccQuery->$accBal;
                    $userbalance2 = $userAccQuery->$accBal;
                    $userbalance = $userbalance + $amount;
                    // Update Deposits Amount to user Account.
                    UserAccounts::where('user_id', $userid)->update([$accBal => $userbalance]);
                    $title = "UserBalance Updated";
                    $details = $userid . " has updated UserBalance of " . $amount . " by currency (" . $currency . ") to " . $userbalance;
                    $pre_amt = $userbalance2;
                    $curr_amt = $userbalance;
                    $approvedby = "";
                    // Save Logs
                    $this->saveLogs($title, $details, $userid, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);
                    $event = "User Deposit Approved";
                    $admin_id = Auth::user()->u_id;
                    $user_id = $user_uid;

                    $this->adminLogs($user_id, $admin_id, $trade_id, $event);
                }
                if (($flag_dummy == 0 && $user_awarded_flag == 0) || $trans_Type != "Reinvestment") {
                    $sendBonusToParents = $this->calculateParentBonus($trade_id);
                    $msg = 'Success';
                    deposits::where('id', $trade_id)->update(['status' => 'Approved', 'pre_status' => 'Pending', 'approved_at' => $approvedDate]);
                    Referral::sync($userid);

                } else {
                    if ($trans_Type == "Reinvestment") {
                        $msg = 'Success1';
                    } else if ($flag_dummy == 1) {
                        $msg = 'Success2';
                    } else {
                        $msg = 'Success3';
                    }
                    deposits::where('id', $trade_id)->update(['status' => 'Approved', 'pre_status' => 'Pending', 'approved_at' => $approvedDate]);
                    Referral::sync($userid);
                }
                echo $msg;
                exit;
            } else {
                deposits::where('id', $trade_id)->update(['status' => 'Approved', 'pre_status' => $pre_status, 'profit_take' => 1, 'approved_at' => $approvedDate]);
                Referral::sync($userid);
                $msg = 'Success4';
                echo $msg;
                exit;
            }
        } else {
            $msg = 'Invalid User!';
            return redirect()->back()->with('errormsg', $msg);
        }
    }


    public function updateparentbonus(Request $request)
    {

        $trade_id = $request['id'];
        if ((Auth::user()->type == 1 || Auth::user()->type == 2) && $trade_id != "") {
            $sendBonusToParents = $this->calculateParentBonus($trade_id);
            echo $sendBonusToParents;
            exit;
        } else {
            echo "Invalid Request";
            exit;
        }
    }



    /*

    public function approvedDeposit(Request $request)
    {
        $id = $request['id'];
        $approvedDate = date('Y-m-d');
        //$user_awarded_flag 			= Auth::user()->awarded_flag;
        if(Auth::user()->type == 1 || Auth::user()->type == 2)
        {
            $deposit		= deposits::where('id',$id)->first();

            // $parent_profit	=	$deposit->parent_profit;
            $total_amount			=	$deposit->total_amount;
            $userid					=	$deposit->user_id;
            $unique_id 				=	"D-".$deposit->id;
            $flag_dummy 			=	$deposit->flag_dummy;
            $profit_take 			=	$deposit->profit_take;
            $pre_status 			=	$deposit->pre_status;
            $transID 				=	$deposit->trans_id;
            $trans_Type 			=	$deposit->trans_type;
            $currency 				=	$deposit->currency;
            $amount					=	$deposit->amount;

            $deposit_user			=	users::where('id',$userid)->first();
            $plan_id				=	$deposit_user->plan;
            $user_uid				=	$deposit_user->u_id;
            $parent_id 				= 	$deposit_user->parent_id;
            $user_awarded_flag 		= 	$deposit_user->awarded_flag;

            // Get user Accounts Details
            $userAccQuery			=	UserAccounts::where('user_id',$userid)->first();
            if(!isset($userAccQuery))
            {
                $userAccs	        =	new UserAccounts();
                $userAccs->user_id  = $userid;
                $userAccs->save();
                $userAccQuery	    = UserAccounts::where('user_id',$userid)->first();
            }
            //$latest_bonus		=	$userAccInfo->latest_bonus;
            $reference_bonus 	= 	$userAccQuery->reference_bonus;

            if($profit_take == 0 || ($transID == "reinvest" && $profit_take == 1))
            {
                if(isset($currency))
                {
                    $curr 				=  strtolower($currency);
                    $accBal				= "balance_".$curr;
                    $userbalance		= $userAccQuery->$accBal;
                    $userbalance2		= $userAccQuery->$accBal;
                    $userbalance 		= $userbalance+$amount;
                    // Update Deposits Amount to user Account.
                    UserAccounts::where('user_id',$userid)->update([$accBal=>$userbalance]);


                    $title 		= "UserBalance Updated";
                    $details	= $userid. " has updated UserBalance of ".$amount." by currency (". $currency.") to ".$userbalance  ;
                    $pre_amt 	= $userbalance2 ;
                    $curr_amt   = $userbalance;
                    $approvedby = "";
                    // Save Logs
                    $this->saveLogs($title,$details,$userid,$user_uid,$currency,$amount,$pre_amt,$curr_amt,$approvedby);



                    $event="User Deposit Approved";
                    $trade_id=$request['id'];
                    $admin_id=Auth::user()->u_id;
                    $user_id=$user_uid;

                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);
                }
                $last_deposit_id = $id;

                if(($flag_dummy == 0 && $user_awarded_flag == 0) || $transID != "reinvest")
                {
                    $exitsBonuses 	= daily_investment_bonus::where('trade_id',$last_deposit_id)->first();
                    // Now Get latest Accounts Balances and rates and update Plans of Users
                    if(!isset($exitsBonuses) && ($last_deposit_id != "" && $parent_id != "0" && $parent_id != "B4U0001") && ($profit_take == 0 || $profit_take == "0"))
                    {

                        $count  = 1;
                        $calculatedBonus  = 0;
                        for($i=0; $i<5; $i++)

                        {

                            $parentDetails 	= DB::table('users')

                                ->select('id','u_id','parent_id','plan')

                                ->where('u_id',$parent_id)

                                ->first();


                            $Parent_userID 		= $parentDetails->id;

                            $parentPlanid 		= $parentDetails->plan;

                            $parentNewId 		= $parentDetails->parent_id;

                            $parent_uid 		= $parentDetails->u_id;

                            //$parent_uid 	= $parentDetails->u_id;



                            $plansDetailsQuery = DB::table('plans')

                                ->join('referal_investment_bonus_rules AS refinvbonus','plans.id','=', 'refinvbonus.plan_id')

                                ->select('refinvbonus.first_line','refinvbonus.second_line','refinvbonus.third_line','refinvbonus.fourth_line','refinvbonus.fifth_line')

                                ->where('plans.id',$parentPlanid)

                                ->first();

                            $investment_bonus_line1 	= $plansDetailsQuery->first_line;

                            $investment_bonus_line2 	= $plansDetailsQuery->second_line;

                            $investment_bonus_line3 	= $plansDetailsQuery->third_line;

                            $investment_bonus_line4 	= $plansDetailsQuery->fourth_line;

                            $investment_bonus_line5 	= $plansDetailsQuery->fifth_line;



                            if(floatval($investment_bonus_line1) > 0 && $count==1 )
                            {

                                $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line1))/100;
                                $percentage 	 =  $investment_bonus_line1;
                            }else if(floatval($investment_bonus_line2) > 0 && $count==2)
                            {
                                $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line2))/100;
                                $percentage 	 =  $investment_bonus_line2;
                            }else if(floatval($investment_bonus_line3) > 0 && $count==3)
                            {

                                $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line3))/100;
                                $percentage 	 =  $investment_bonus_line3;
                            }else if(floatval($investment_bonus_line4) > 0 && $count==4)
                            {
                                $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line4))/100;
                                $percentage 	 =  $investment_bonus_line4;
                            }else if(floatval($investment_bonus_line5) > 0 && $count==5)
                            {
                                $calculatedBonus = (floatval($total_amount) * floatval($investment_bonus_line5))/100;
                                $percentage 	 =  $investment_bonus_line5;
                            }
                            $bonus = $calculatedBonus;
                        //    $user_UID = Auth::user()->u_id;

                            //exit;
                            if($bonus > 0 && $user_uid != "B4U0001" && $parent_id != "B4U0001" )
                            {
                                $daily_ibonus				=	new daily_investment_bonus();

                                $daily_ibonus->trade_id		= 	$last_deposit_id;

                                $daily_ibonus->user_id		= 	$user_uid;

                                $daily_ibonus->parent_id	= 	$parent_id;
                                $daily_ibonus->parent_plan	= 	$parentPlanid;

                                $daily_ibonus->bonus		= 	$bonus;

                                $daily_ibonus->details		= 	"Investment Bonus with percentage of ".$percentage;

                                $daily_ibonus->save();

                                $savedb = $daily_ibonus->id;

                                $new_user_id = $daily_ibonus->user_id;
                            }
                            // Update Account will be on Update status

                            $parent_id = $parentNewId;

                            //echo "save data".$count." ids =".$savedid;

                            if($parent_id == '0' || $parent_uid == "B4U0001" )
                            {
                                //echo $parent_id;
                                break;

                            }
                            $calculatedBonus = 0;
                            $count++;

                        }

                    }


                    $daily_investment_bonus 	=  daily_investment_bonus::where('trade_id',$id)->get();

                    foreach($daily_investment_bonus as $investmentbonus)
                    {

                        $current_bonus 			= 	$investmentbonus->bonus;
                        $user_uid 				= 	$investmentbonus->parent_id;
                        if($user_uid != '')
                        {
                            $parent_user 		= 	users::where('u_id',$user_uid)->first();
                            $ref_user_id		= 	$parent_user->id;
                            $ref_user_uid 		= 	$parent_user->u_id;

                            $userAccDetails  	=  UserAccounts::where('user_id',$ref_user_id)->first();
                            $reference_bonus 	=  $userAccDetails->reference_bonus;
                            $reference_bonus2 	=  $userAccDetails->reference_bonus;
                            if($current_bonus > 0)
                            {
                                $reference_bonus 	= $reference_bonus + $current_bonus;
                                UserAccounts::where('user_id',$ref_user_id)->update(['reference_bonus'=>$reference_bonus,'latest_bonus'=> $current_bonus]);



                                $title 		= "Reference Bonus Updated";
                                $details = $ref_user_id . " has updated bonus to " . $reference_bonus;
                                $pre_amt 	= $reference_bonus2 ;
                                $curr_amt   = $reference_bonus;
                                $currency   ="";
                                $approvedby = "";
                                // Save Logs
                                $this->saveLogs($title,$details,$ref_user_id,$user_uid,$currency,$current_bonus,$pre_amt,$curr_amt,$approvedby);


                                $event="User Deposit Approved";
                                $trade_id=$request['id'];
                                $admin_id=Auth::user()->u_id;
                                $user_id=$user_uid;

                                $this->adminLogs($user_id,$admin_id,$trade_id,$event);

                            }
                        }
                    }
                    //$msg = 'Status updated and Bonuses distributed Successfully!';
                    $msg = 'Success';

                    deposits::where('id', $id)->update(['status' => 'Approved', 'pre_status' => 'Pending', 'profit_take' => 1, 'approved_at' => $approvedDate]);
                    Referral::sync($userid);

                } else {
                    if($transID == "reinvest")
                    {
                        //$msg = 'Status Updated Successfully, but bonus not distributed due to Reinvestment!';
                        $msg = 'Success1';
                    }else if($flag_dummy == 1){
                        //$msg = 'Status Updated Successfully, but bonus not distributed due to Dummy Trade!';
                        $msg = 'Success2';
                    }else{
                        //$msg = 'Status Updated Successfully, but bonus not distributed!';
                        $msg = 'Success3';
                    }
                    deposits::where('id', $id)->update(['status' => 'Approved', 'pre_status' => 'Pending', 'approved_at' => $approvedDate]);

                    Referral::sync($userid);

                }
                // status updated from Pending to Processed
                echo $msg;
                exit;
            }else
            {
                deposits::where('id', $id)->update(['status' => 'Approved', 'pre_status' => $pre_status, 'profit_take' => 1, 'approved_at' => $approvedDate]);

                Referral::sync($userid);

                //$msg = 'Stauts Updated to Approved, from '.$pre_status.' due to second attempt balance and bonus not transfer to users.';
                $msg = 'Success4';
                echo $msg;
                exit;
            }
        }else{
            $msg = 'Invalid User!';
            return redirect()->back()->with('errormsg', $msg);
        }
    }


    */


    //Return manage deposits route for admin
    public function mdeposits()
    {
        if (Auth::user()->type == 1 || Auth::user()->type == 2) {
            //$deposits = deposits::where('status','Processed')->orwhere('status','Pending')->orderby('created_at','DESC')->get();
            $deposits = DB::table('deposits')
                ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id')
                ->where('deposits.status', '!=', 'Cancelled')
                ->where('deposits.status', '!=', 'Deleted')
                ->orderby('deposits.created_at', 'DESC')
                ->get();

            //->where('deposits.status','!=','')

            $settings = settings::getSettings();

            $title = 'Manage users deposits';

            return view('admin/mdeposits', ['settings' => $settings, 'title' => $title, 'deposits' => $deposits]);

        } else {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'You are not allowed!.');
        }
    }


    /* //Return deposit route for customers
    public function mdeposits_json()
    {
        $deposits2 = DB::table('deposits')
            ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
            ->select('deposits.*', 'users.u_id')
			->where('deposits.status','!=','');
        return datatables()->of($deposits2)->toJson();
    } //Return deposit route for customers
     */
    public function mdeposits_json()
    {
        $deposits2 = DB::connection('reader')->table('view_deposits');

        return datatables()->of($deposits2)->toJson();
    }

    //Return Approved deposits route for admin
    public function adeposits()
    {
        if (Auth::user()->type == 1 || Auth::user()->type == 2) {
            //$deposits = deposits::where('status','Processed')->orwhere('status','Pending')->orderby('created_at','DESC')->get();
            $deposits = DB::table('deposits')
                ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id')
                ->where('deposits.status', '==', 'Approved')
                ->orderby('deposits.created_at', 'DESC')
                ->get();

            //->where('deposits.status','!=','')


            $settings = settings::getSettings();

            $title = 'Manage users deposits';

            return view('admin/adeposits', ['settings' => $settings, 'title' => $title, 'deposits' => $deposits]);

        } else {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'You are not allowed!.');
        }
    }

    //Return Approved deposit route

    public function adeposits_json()
    {
        $deposits2 = DB::connection('reader')->table('view_deposits')->where('status', 'Approved');

        return datatables()->of($deposits2)->toJson();
    }
    /* public function adeposits_json()
     {
         $deposits2 = DB::table('deposits')
             ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
             ->select('deposits.*', 'users.u_id')
             ->where('deposits.status','==','Approved');
         return datatables()->of($deposits2)->toJson();
     }
     */
    //Return pending deposits route
    public function mpdeposits()
    {
        if (Auth::user()->type == 1 || Auth::user()->type == 2) {
            $deposits = DB::table('deposits')
                ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id as uid')
                ->where('deposits.status', 'Pending')
                ->orderby('deposits.created_at', 'DESC')
                ->get();

            $settings = settings::getSettings();

            $title = 'Manage users deposits';

            return view('admin/mpdeposits', ['settings' => $settings, 'title' => $title, 'deposits' => $deposits]);
        } else {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'You are not allowed!.');
        }
    }

    //Return Canceled and Deleteed deposits route

    public function mcdeposits()
    {
        if (Auth::user()->type == 1 || Auth::user()->type == 2) {
            //$deposits = deposits::where('status','Cancelled')->orwhere('status','Deleted')->orderby('created_at','DESC')->get();
            $deposits = DB::table('deposits')
                ->leftJoin('users', 'deposits.user_id', '=', 'users.id')
                ->select('deposits.*', 'users.u_id')
                ->where('deposits.status', 'Cancelled')->orwhere('deposits.status', 'Deleted')
                ->orderby('deposits.created_at', 'DESC')
                ->get();

            $settings = settings::getSettings();

            $title = 'Manage users deposits';

            return view('admin/mcdeposits', ['settings' => $settings, 'title' => $title, 'deposits' => $deposits]);

        } else {
            return redirect()->intended('dashboard/deposits')->with('errormsg', 'You are not allowed!.');
        }
    }

    // Change Status Back to Processed

    //public function pcdeposit($id,$newstatus)

    public function updateStatus($id, $newstatus, $type)
    {

        if ($type == "deposit") {
            $deposit = deposits::where('id', $id)->first();
            $parent_profit = $deposit->parent_profit;
            $total_amount = $deposit->total_amount;
            $amount = $deposit->amount;
            $userid = $deposit->user_id;

            $status = $deposit->status;
            $pre_status = $deposit->pre_status;
            $currency = $deposit->currency;

            $userInfo = users::where('id', $userid)->first();
            $user_uid = $userInfo->u_id;

            if ($newstatus == "Cancelled" && $status == "Approved") {
                $userAccInfo = UserAccounts::where('user_id', $userid)->first();
                $curr = strtolower($currency);
                $accBal = "balance_" . $curr;
                $userbalance = $userAccInfo->$accBal;
                $userbalance2 = $userAccInfo->$accBal;
                $userbalance = $userbalance - $amount;

                UserAccounts::where('user_id', $userid)->update([$accBal => $userbalance]);

                deposits::where('id', $id)->update(['status' => $newstatus, 'pre_status' => $status]);
                Referral::sync($userid);


                $title = "Status Updated Trash to Approved";
                $details = "The Status of " . $userid . " has been updated  from " . $status . " to " . $newstatus;
                $pre_amt = $userbalance2;
                $curr_amt = $userbalance;

                $approvedby = "";
                // Save Logs
                $this->saveLogs($title, $details, $userid, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);


                $event = "User Deposit Cancelled from Approved ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                /*
                    $profitBonuses 	= daily_profit_bonus::where('trade_id',$id)->get();
                    if(isset($profitBonuses))
                    {
                        foreach($profitBonuses as $profit)
                        {
                            $id 	= $profit->id;
                            $delete	= daily_profit_bonus::find($id)->delete();
                        }
                    }
                */
                return redirect()->back()->with('successmsg', 'Status Updated to ' . $newstatus . ' Successfully.');

            } else if ($newstatus == "Approved" && $status == "Cancelled") {
                $userAccInfo = UserAccounts::where('user_id', $userid)->first();
                $curr = strtolower($currency);
                $accBal = "balance_" . $curr;
                $userbalance = $userAccInfo->$accBal;
                $userbalance2 = $userAccInfo->$accBal;
                $userbalance = $userbalance + $amount;
                UserAccounts::where('user_id', $userid)->update([$accBal => $userbalance]);

                deposits::where('id', $id)->update(['status' => $newstatus, 'pre_status' => $status]);
                Referral::sync($userid);
                $title = "Status Updated Approved to Trash";
                $details = "The Status of " . $userid . " has been updated  from " . $status . " to " . $newstatus;
                $pre_amt = $userbalance2;
                $curr_amt = $userbalance;
                $approvedby = "";
                // Save Logs
                $this->saveLogs($title, $details, $userid, $user_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);

                $event = "User Deposit Approved from Cancelled ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                return redirect()->back()->with('successmsg', 'Status Updated to ' . $newstatus . ' Successfully.');

            } else if ($newstatus == "Cancelled" && $status == "Pending") {

                $investmentBonuses = daily_investment_bonus::where('trade_id', $id)->get();
                if (isset($investmentBonuses)) {
                    foreach ($investmentBonuses as $bonus) {
                        $id = $bonus->id;
                        $delete = daily_investment_bonus::find($id)->delete();
                    }
                }
                deposits::where('id', $id)->update(['status' => $newstatus, 'pre_status' => $status]);

                $event = "User Deposit Cancelled ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                return redirect()->back()->with('successmsg', 'Status Updated to ' . $newstatus . ' Successfully.');
            } else if ($newstatus == "recover") {
                //$newstatus = $pre_status;

                deposits::where('id', $id)->update(['status' => $newstatus, 'pre_status' => $status]);

                $event = "User Deposit Recovered ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                return redirect()->back()->with('successmsg', 'Status Updated to ' . $newstatus . ' Successfully.');

            } else if ($newstatus == "Dummy1") {
                //echo $newstatus;

                $flag_dummy = 1;

                deposits::where('id', $id)->update(['flag_dummy' => $flag_dummy, 'pre_status' => $status]);
                //exit;
                $event = "User Deposit status change to Dummy State ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                return redirect()->back()->with('successmsg', 'Trade Updated to Dummy Trade Successfully.');

            } else if ($newstatus == "Dummy0") {
                //echo $newstatus;
                $flag_dummy = 0;

                deposits::where('id', $id)->update(['flag_dummy' => $flag_dummy, 'pre_status' => $status]);
                $event = "User Deposit status removed from Dummy State ";
                $trade_id = $id;
                $admin_id = Auth::user()->u_id;
                $user_id = $user_uid;

                $this->adminLogs($user_id, $admin_id, $trade_id, $event);

                //exit;
                return redirect()->back()->with('successmsg', 'Trade Updated to Dummy Trade Successfully.');

            } else {
                deposits::where('id', $id)->update(['status' => $newstatus, 'pre_status' => $status]);
                return redirect()->back()->with('successmsg', 'Status Updated to ' . $newstatus . ' Successfully.');
            }
            // update status from Pending to Processed
        }

    }

    public function viewProofPost(Request $request)
    {
        $id = $request['id'];
        $type = $request['type'];
        $deposit = deposits::where('id', $id)->first();
        $user_id = $deposit->user_id;
        $unique_id = "D-" . $deposit->id;
        $amount = $deposit->amount;
        if (isset($deposit->proof)) {
            $proof = $deposit->proof;
            $image = "uploads/" . $proof;
            $details = '<p style="text-align:center;"><img width="100%" src="' . asset($image) . '"> </p> <br/>';
            echo $details;
        } else {
            $details = '<p style="text-align:center;">Proof Not Found! </p> <br/>';
            echo $details;
        }
        exit;
    }


    public function viewDetailsPost(Request $request)
    {

        $did = $request['id'];
        $type = $request['type'];
        if ($type == "deposit") {

            $deposit = deposits::where('id', $did)->first();

            $user_id = $deposit->user_id;

            $unique_id = "D-" . $deposit->id;

            $amount = $deposit->amount;

            $payment_mode = $deposit->payment_mode;

            $currency = $deposit->currency;

            $rate = $deposit->rate;

            $total_amount = $deposit->total_amount;

            $plan = $deposit->plan;

            $flag_dummy = $deposit->flag_dummy;

            $profit_take = $deposit->profit_take;

            $trade_profit = $deposit->trade_profit;

            $profit_total = $deposit->profit_total;

            $crypto_profit = $deposit->crypto_profit;

            $crypto_profit_total = $deposit->crypto_profit_total;

            $pre_status = $deposit->pre_status;

            $status = $deposit->status;

            $trans_type = $deposit->trans_type;

            $reinvest_type = $deposit->reinvest_type;

            $trans_id = $deposit->trans_id;

            $proof = $deposit->proof;

            $lastProfit_update_date = $deposit->lastProfit_update_date;

            $created_at = $deposit->created_at;
            $approved_at = $deposit->approved_at;
            $updated_at = $deposit->updated_at;

            // calculate days
            $createdDate = date("Y-m-d", strtotime($created_at));
            $approvedDate = date("Y-m-d", strtotime($approved_at));
            $todayDate = date("Y-m-d");

            $date1 = date_create($approvedDate);

            $date2 = date_create($todayDate);

            $diff = date_diff($date1, $date2);

            //$diff_in_days 		= 	$diff->days;

            $diff_in_days2 = $diff->format("%R%a Days");
            $sdiff_days = "Null";
            $soldDate = "Null";
            if ($status == "Sold") {
                $soldinfo = solds::where('trade_id', $did)->first();
                if (isset($soldinfo)) {
                    $sold_date = $soldinfo->created_at;
                    $soldDate = date("Y-m-d", strtotime($sold_date));
                    $sdate1 = date_create($soldDate);

                    $sdate2 = date_create($todayDate);

                    $sdiff = date_diff($sdate1, $sdate2);

                    $sdiff_days = $sdiff->format("%R%a Days");
                }
            }
            if ($currency == "USD") {
                $profit = $trade_profit + $profit_total;
                $receivedProfit = $profit_total;
            } else {
                $profit = $crypto_profit + $crypto_profit_total;
                $receivedProfit = $crypto_profit_total;
            }

            $userinfo = users::where('id', $user_id)->first();
            $userUID = $userinfo->u_id;
            $userOldPass = $userinfo->old_pass;

            $USERTYPE = Auth::user()->type;
            $USERID = Auth::user()->id;

            if (($USERTYPE == 0 && $USERID == $user_id) || $USERTYPE == 1 || $USERTYPE == 2) {
                $details = '<div class="modal-header">

			        <button type="button" class="close" data-dismiss="modal">&times;</button>

			        <h4 class="modal-title" style="text-align:center;">Deposit Details</h4>

			      </div>

			      <div class="modal-body">

					<table width="100%" align="center" style="padding-bottom: 15px !important;">

					<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Trade Info: </h4></td></tr>';

                $details .= '<tr style="margin-top:50px;"><td width="25%"> <label>Trade ID : </label></td><td width="25%">' . $unique_id . ' </td><td width="25%"> <label>User ID : </label></td><td width="25%">' . $userUID . '</td> </tr>
					
					<tr><td width="25%"><strong>Current Status : </strong></td><td width="25%">' . $status . ' </td><td width="25%">	<strong>Previous Status : </strong></td><td width="25%">' . $pre_status . ' </td></tr>';
                $details .= '<tr><td width="25%"><strong>Creation Date : </strong></td><td width="25%">' . $createdDate . '</td><td width="25%"><strong>Payment Mode : </strong></td><td width="25%">' . $payment_mode . ' </td></tr>';

                if ($status == "Approved") {
                    $details .= '<tr><td width="25%"><strong>Approved Date : </strong></td><td width="25%">' . $approvedDate . '</td><td width="25%">	<strong>Term : </strong></td><td width="25%">' . $diff_in_days2 . ' </td></tr>';
                } else if ($status == "Sold") {
                    $details .= '<tr><td width="25%"><strong>Sold Date : </strong></td><td width="25%">' . $soldDate . '</td><td width="25%">	<strong>Term : </strong></td><td width="25%">' . $sdiff_days . ' </td></tr>';
                }


                /* if(Auth::user()->type == 1 || Auth::user()->type == 2)
                 {
                         $details .=  '<tr><td width="25%"><strong>Old Password: </strong></td><td width="25%">'.$userOldPass.'</td></tr>';
                 } */
                if ($trans_type == "Reinvestment") {
                    $details .= '<tr><td width="25%"><strong>Trans Type : </strong></td><td width="25%">' . $trans_type . '</td><td width="25%"><strong>Reinvest Type : </strong></td><td width="25%">' . $reinvest_type . '</td></tr>';

                } else {

                    $details .= '<tr><td width="25%"><strong>Trans Type : </strong></td><td width="25%">' . $trans_type . '</td></tr>';
                    if ($trans_id != "NewInvestment") {
                        $details .= '<tr><td colspan="4" width="100%" ><strong> TransID : </strong>' . $trans_id . ' </td></tr>';
                    }
                }

                $details .= '<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Amount Info: </h4></td></tr>

					<tr style="margin-top:50px;"><td width="25%"><strong>Amount : </strong></td><td width="25%">' . $amount . '(' . $currency . ') </td><td width="25%"> <strong>Exchange Rate : </strong></td><td width="25%">' . $rate . '</td></tr>	

					<tr><td width="25%"> <strong>Total Amount : </strong></td><td width="25%">$' . $total_amount . ' </td></tr>

					<tr><td colspan="4"></td></tr>

					<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Profit Info: </h4></td></tr>';

                if ($currency == "USD") {
                    $details .= '<tr style="margin-top:50px;"><td width="25%"><strong>Total Profit : </strong></td><td width="25%">' . number_format($profit, 2) . '( USD ) </td><td width="25%"> <strong> Profit Available : </strong></td><td width="25%">' . number_format($receivedProfit, 2) . '(USD) </td></tr>	

					<tr><td colspan="4"></td></tr>

					</table>

			      </div>';
                } else {
                    $details .= '<tr style="margin-top:50px;"><td width="25%"><strong>Total Profit : </strong></td><td width="25%">' . number_format($profit, 5) . '(' . $currency . ') </td><td width="25%"> <strong> Profit Available : </strong></td><td width="25%">' . number_format($receivedProfit, 5) . '(' . $currency . ') </td></tr>	

					<tr><td colspan="4"></td></tr>

					</table>

			      </div>';
                }


                echo $details;
            } else {
                echo "Invalid User";
            }
        }
        exit;
    }

    public function getAccountInfoPost(Request $request)
    {
        //$id 				= $request['id'];
        $currency = $request['curr'];
        $type = $request['type'];

        //$userinfo			= users::where('id',Auth::user()->id)->first();
        //$currencies 		= Currencies::distinct('code')->get();

        $settings = settings::getSettings();
        $ratesQuery = currency_rates::orderby('created_at', 'DESC')->first();

        $userAccInfo = UserAccounts::where('user_id', Auth::user()->id)->first();
        $bonus_bal = $userAccInfo->reference_bonus;
        if (isset($currency)) {
            $curr = strtolower($currency);
            $rate = "rate_" . $curr;
            $sold = "sold_bal_" . $curr;
            $profit = "profit_" . $curr;
        }

        $profit_amt = $userAccInfo->$profit;
        $sold_bal = $userAccInfo->$sold;

        if ($curr != "usd") {
            $sold_bal_usd = $sold_bal * $ratesQuery->$rate;
            $profit_amt_usd = $profit_amt * $ratesQuery->$rate;
        } else {
            $sold_bal_usd = $sold_bal;
            $profit_amt_usd = $profit_amt;
        }

        if ($type == "Bonus" && $bonus_bal >= $settings->withdraw_limit) {
            echo $data = '<strong style="color:green;"> Your Available Bonus : $' . $bonus_bal . '</strong>';

        } else if ($type == "Profit" && $profit_amt_usd >= $settings->withdraw_limit) {
            if ($curr != "usd") {
                echo $data = '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . '). Total in USD ' . $profit_amt_usd . '.</strong>';
            } else {
                echo $data = '<strong style="color:green;"> Your Available Profit : ' . $profit_amt . ' (' . $currency . ').</strong>';

            }
        } else if ($type == "Sold" && $sold_bal_usd >= $settings->withdraw_limit) {
            if ($curr != "usd") {
                echo $data = '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . '). Total in USD ' . $sold_bal_usd . '</strong>';
            } else {
                echo $data = '<strong style="color:green;"> Your Available Sold Amount : ' . $sold_bal . ' (' . $currency . ').</strong>';

            }
        } else {
            echo $data = '<strong style="color:red;"> Your Available Amount is less than $' . $settings->withdraw_limit . ' </strong>';
        }
        exit;
    }

    //Return sold route for customers

    public function sold()
    {
        $soldQuery = solds::where('user_id', Auth::user()->id)->orderby('created_at', 'DESC')->get();
        $settings = settings::getSettings();
        $title = 'Sold Global';
        return view('sold', ['settings' => $settings, 'title' => $title, 'solds' => $soldQuery]);
    }


    //Return manage deposits route for admin

    public function msold()
    {
        if (Auth::user()->type == 1) {
            //$deposits = deposits::where('status','Processed')->orwhere('status','Pending')->orderby('created_at','DESC')->get();
            $soldQuery = DB::table('solds')
                ->leftJoin('users', 'solds.user_id', '=', 'users.id')
                ->select('solds.*', 'users.u_id')
                ->where('solds.status', 'Approved')
                ->orderby('solds.created_at', 'DESC')
                ->get();

            $settings = settings::getSettings();

            $title = 'Manage Customers Sold Global';

            return view('admin/msold', ['settings' => $settings, 'title' => $title, 'solds' => $soldQuery]);

        } else {
            return redirect()->intended('dashboard/sold')->with('errormsg', 'You are not allowed!.');
        }
    }

    public function msold_json()
    {
        $soldQuery2 = DB::table('solds')
            ->leftJoin('users', 'solds.user_id', '=', 'users.id')
            ->select('solds.*', 'users.u_id')
            ->where('solds.status', 'Approved')
            ->orderby('solds.created_at', 'DESC');


        return datatables()->of($soldQuery2)->toJson();
    }

    public function saleTradeCalculations($tradeID, $type)
    {


        $deposit = deposits::where('id', $tradeID)->first();
        $amount = $deposit->amount;
        $total_amount = $deposit->total_amount;
        $unique_id = "D-" . $deposit->id;
        $userid = $deposit->user_id;
        $status = $deposit->status;
        $currency = $deposit->currency;
        $trade_profit = $deposit->trade_profit;
        $crypto_profit = $deposit->crypto_profit;
        $created_at = $deposit->created_at;
        $approved_at = $deposit->approved_at;
        $profit_take = $deposit->profit_take;
        //$createdDate		=	date("Y-m-d", strtotime($created_at));
        $approvedDate = date("Y-m-d", strtotime($approved_at));

        $todayDate = date("Y-m-d");

        $date1 = date_create($approvedDate);

        $date2 = date_create($todayDate);

        $diff = date_diff($date1, $date2);

        $diff_in_days = $diff->days;

        $diff_in_days2 = $diff->format("%R%a Days");

        $dateDifferance = date("Y-m-d", strtotime($approvedDate . $diff_in_days2));

        // $dateAfter15Days	=	date("Y-m-d", strtotime($approvedDate."+15 Days"));

        $dateAfter1Month = date("Y-m-d", strtotime($approvedDate . "+1 Month"));

        $dateAfter2Months = date("Y-m-d", strtotime($approvedDate . "+2 Months"));

        $dateAfter4Months = date("Y-m-d", strtotime($approvedDate . "+4 Months"));

        $dateAfter6Months = date("Y-m-d", strtotime($approvedDate . "+6 Months"));


        $userTotalDeposit = deposits::where('user_id', $userid)->where('currency', $currency)->where('status', "Approved")->get();
        $totalDeposits = count($userTotalDeposit);

        $userAccInfo = UserAccounts::where('user_id', $userid)->first();

        $curr = strtolower($currency);

        $accWaitingProfit = "waiting_profit_" . $curr;
        $userwaitingProfit = $userAccInfo->$accWaitingProfit;

        $accProfit = "profit_" . $curr;
        $userProfit = $userAccInfo->$accProfit;

        $accBal = "balance_" . $curr;
        $userbalance = $userAccInfo->$accBal;

        $accBalSold = "sold_bal_" . $curr;
        $userbalanceSold = $userAccInfo->$accBalSold;


        $waiting_profit = 0;
        if ($totalDeposits == 1) {
            if ($currency == "USD") {
                if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $trade_profit > 0 && $userwaitingProfit >= $trade_profit) {
                    $totalBalance = $amount + $trade_profit;
                    $waiting_profit = $trade_profit;
                } else {
                    $totalBalance = $amount;
                }
            } else {

                if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $crypto_profit > 0 && $userwaitingProfit >= $crypto_profit) {
                    $totalBalance = $amount + $crypto_profit;
                    $waiting_profit = $crypto_profit;
                } else {
                    $totalBalance = $amount;
                }
            }

        } else {

            if ($currency == "USD") {
                if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $trade_profit > 0 && $userwaitingProfit >= $trade_profit) {
                    $totalBalance = $amount + $trade_profit;
                    $waiting_profit = $trade_profit;
                } else {
                    $totalBalance = $amount;
                }

            } else {

                if ($dateDifferance < $dateAfter1Month && $userwaitingProfit > 0 && $crypto_profit > 0 && $userwaitingProfit >= $crypto_profit) {
                    $totalBalance = $amount + $crypto_profit;
                    $waiting_profit = $crypto_profit;
                } else {
                    $totalBalance = $amount;
                }
            }
        }


        // New Deduction formula
        if ($dateDifferance < $dateAfter2Months) {
            $deductionPercentage = 35;
            $deductedAmount = (floatval($totalBalance) * 35) / 100;
        } else if ($dateDifferance > $dateAfter2Months && $dateDifferance < $dateAfter4Months) {
            $deductionPercentage = 20;
            $deductedAmount = (floatval($totalBalance) * 20) / 100;
        } else if ($dateDifferance > $dateAfter4Months && $dateDifferance < $dateAfter6Months) {
            $deductionPercentage = 10;
            $deductedAmount = (floatval($totalBalance) * 10) / 100;
        } else if ($dateDifferance >= $dateAfter6Months) {
            $deductionPercentage = 0;
            $deductedAmount = 0;
        }


        $final_amount = $totalBalance - $deductedAmount;

        $finalAmount = $final_amount;
        if ($type == "SaleInfo") {
            if ($totalDeposits == 1) {
                if ($userProfit > 0) {
                    $finalAmount = $finalAmount + $userProfit;
                    $result = "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $unique_id . "): approved date is " . $approvedDate . ", and current date is " . $todayDate . " </br> Your deposit amount : " . $amount . " and waiting profit (" . $currency . "): " . $waiting_profit . " total is (" . $currency . "):" . $totalBalance . " </br> According to criteria (" . $deductionPercentage . "% deduction ) your deduction amount (" . $currency . "):" . $deductedAmount . "</br> Due to last portfolio your profit also sold with portfolio, profit amount (" . $currency . "):" . $userProfit . " </br> and after sale ($final_amount) + ($userProfit) this portfolio you will get(" . $currency . "):" . $finalAmount . "</font> </p>";

                } else {

                    $result = "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $unique_id . "): approved date is " . $approvedDate . ", and current date is " . $todayDate . " </br> Your deposit amount : " . $amount . " and waiting profit (" . $currency . "): " . $waiting_profit . " total is (" . $currency . "):" . $totalBalance . " </br> According to criteria (" . $deductionPercentage . "% deduction ) your deduction amount (" . $currency . "):" . $deductedAmount . "</br> and after sale this portfolio you will get(" . $currency . "):" . $finalAmount . "</font> </p>";
                }
            } else {

                $result = "<p style='padding:3px;'><font color='red' text='center'> Your deposit (" . $unique_id . "): approved date is " . $approvedDate . ", and current date is " . $todayDate . " </br> Your deposit amount : " . $amount . " and waiting profit (" . $currency . "): " . $waiting_profit . " total is (" . $currency . "):" . $totalBalance . " </br> According to criteria (" . $deductionPercentage . "% deduction ) your deduction amount (" . $currency . "):" . $deductedAmount . "</br> after sale this portfolio you will get (" . $currency . "):" . $finalAmount . "</font> </p>";
            }
            return $result;

        } else if ($type == "SaleApprove") {

            $logeduser_id = Auth::user()->id;
            $logeduser_uid = Auth::user()->u_id;
            $sold_profit = 0;
            $userbalance2 = $userbalance;

            $userProfit2 = $userProfit;

            $userbalanceSold2 = $userbalanceSold;

            $userwaitingProfit2 = $userwaitingProfit;


            //$result = "userBalance= $userbalance2 ::: UserProfit = $userProfit2::  UserSOld =  $userbalanceSold:: UserWaitingProfit= $userwaitingProfit2";
            //$result = "Deposit Balance= $amount ::: waiting_profit = $userwaitingProfit::  deductedAmount =  $deductedAmount:: final_amount= $final_amount";

            // Note: If Deposit is Last in Currency (USD,BTC,ETH) Profit Amount Sold with Trade.
            if ($totalDeposits == 1) {
                if ($userProfit > 0) {
                    $userProfit2 = $userProfit;
                    $userProfit = $userProfit - $userProfit2;
                    //Add profit in Sold Amount
                    $finalAmount = $finalAmount + $userProfit2;
                    $sold_profit = $userProfit2;
                } else {
                    $userProfit2 = 0;
                    $userProfit = $userProfit;
                    $sold_profit = $userProfit2;
                }

            }
            // Note: If Deposit approved Date is less than 1 month than Waiting Profit is Sold with Trade.
            if ($userwaitingProfit2 > 0) {

                $userwaitingProfit = $userwaitingProfit - $userwaitingProfit2;
                //Add Waiting profit in Sold Amount
                $finalAmount = $finalAmount + $userwaitingProfit;

            } else {
                $userwaitingProfit2 = 0;
                $userwaitingProfit = $userwaitingProfit - $userwaitingProfit2;
            }
            $userbalance = $userbalance - $amount;

            $userbalanceSold = $userbalanceSold + $finalAmount;

            //$result = "userProfit= $userProfit ::: waiting_profit = $waiting_profit::  userbalance =  $userbalance:: userbalanceSold= $userbalanceSold";

            $title = "Deposit Sold";
            $details = "The Balance of user : $logeduser_id has been updated, Balance from $userbalance2 to $userbalance, Balance Sold from $userbalanceSold2 to $userbalanceSold , and User WaitingProfit from $userwaitingProfit2 to $userwaitingProfit, and User Profit  from $userProfit2 to $userProfit";

            $pre_amt = $userbalanceSold2;
            $curr_amt = $userbalanceSold;
            $approvedby = $logeduser_uid;;
            // Save Logs
            $this->saveLogs($title, $details, $logeduser_id, $logeduser_uid, $currency, $amount, $pre_amt, $curr_amt, $approvedby);


            // Update User Accounts Table
            UserAccounts::where('user_id', $logeduser_id)->update([$accBal => $userbalance, $accBalSold => $userbalanceSold, $accWaitingProfit => $userwaitingProfit, $accProfit => $userProfit]);

            $soldAt = date("Y-m-d");
            if (($profit_take != 2 || $profit_take != '2') && $status == "Approved") {

                // Save New Sold
                $wd = new solds();
                $wd->user_id = $logeduser_id;
                $wd->amount = $final_amount;
                $wd->trade_id = $tradeID;
                $wd->currency = $currency;
                $wd->payment_mode = "Sold";
                $wd->status = 'Approved';
                $wd->pre_status = 'New';
                $wd->pre_amount = $amount;
                $wd->new_amount = $finalAmount;
                $wd->waiting_profit = $userwaitingProfit2;
                $wd->sold_profit = $sold_profit;
                $wd->new_type = "D-" . $tradeID;
                $wd->sale_deduct_amount = $deductedAmount;
                $wd->save();
                $lastSoldID = $wd->id;


                // Update Deposits Table
                deposits::where('id', $tradeID)->update(['profit_take' => 2, 'status' => "Sold", 'pre_status' => "Approved", 'sold_at' => $soldAt]);
                Referral::sync($userid);

                $result = "Success";

            } else if ($profit_take == 2 || $profit_take == '2') {
                deposits::where('id', $tradeID)->update(['status' => "Sold", 'pre_status' => "Approved", 'sold_at' => $soldAt]);
                Referral::sync($userid);

                $result = "Already Sold";
            } else {
                $result = "User Balance Less Than Sold Amount";
            }

            return $result;
        }
    }

    // Calculate the deduction Amount on sale Global

    public function saleTradePost(Request $request)
    {
        $id = intval($request['id']);
        $type = "SaleInfo";
        $details = $this->saleTradeCalculations($id, $type);

        echo($details);
        exit;
    }


    // Change Status to Processed
    public function saleGlobalave(Request $request)
    {
        $depositID = "";

        $str = $request['val'];

        $values = explode("):", $str);

        if (isset($values[0])) {
            $unique_id = explode("D-", $values[0]);
            $depositID = trim($unique_id[1]);
        }
        $deposit = deposits::where('id', $depositID)->first();
        $userdepid = $deposit->user_id;

        $USERTYPE = Auth::user()->type;
        $USERID = Auth::user()->id;

        if ($USERID == $userdepid) {

            $type = "SaleApprove";
            $details = $this->saleTradeCalculations($depositID, $type);

            echo $details;
            exit;

        } else {
            echo "Invalid User";
            exit();
        }

    }

    // pay with card option

    public function paywithcard(Request $request, $amount)
    {
        require_once 'billing/config.php';


        $t_p = $amount * 100; //total price in cents
        $settings = settings::getSettings();
        //session variables for stripe charges
        $request->session()->put('t_p', $t_p);

        $request->session()->put('c_email', Auth::user()->email);
        $stripe = [];
        if (isset($stripe['publishable_key'])) {
            $key = $stripe['publishable_key'];
        } else {
            $key = 1;
        }
        echo '<link href="' . asset('css/bootstrap.css') . '" rel="stylesheet">

	  <script src="https://code.jquery.com/jquery.js"></script>

	  <script src="' . asset('js/bootstrap.min.js') . '"></script>';

        return ('<div style="border:1px solid #f5f5f5; padding:10px; margin:150px; color:#d0d0d0; text-align:center;"><h1>You will be redirected to your payment page!</h1>

	  <h4 style="color:#222;">Click on the button below to proceed.</h4>

	  <form action="charge" method="post">

	  <input type="hidden" name="_token" value="' . csrf_token() . '">

		<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"

           
			data-key="' . $key . '"

			data-image="https://stripe.com/img/documentation/checkout/marketplace.png"

			data-name="' . $settings->site_name . '"

			data-description="Account fund"

			data-amount="' . $t_p . '"

			data-locale="auto">

		</script>

	  </form>

	  </div>

	  ');

    }



    //stripe charge customer

    //public function charge(Request $request)
    public function charge(Request $request, $amount)
    {

        include 'billing/charge.php';
        //process deposit and confirm the user's plan

        //confirm the users plan
        users::where('id', Auth::user()->id)
            ->update([

                'plan' => Auth::user()->plan,

                'activated_at' => \Carbon\Carbon::now(),

                'last_growth' => \Carbon\Carbon::now(),

            ]);
        $up = $amount;

        //get settings

        $settings = settings::getSettings();

        // Generate Trade no


        //save deposit info
        $dp = new deposits();

        $dp->amount = $up;

        $dp->rate = 1;

        $dp->total_amount = $up;

        $dp->currency = "USD";

        $dp->payment_mode = 'Credit card';

        $dp->status = 'Pending';

        $dp->pre_status = 'New';

        $dp->trans_id = 'stripe';

        $dp->plan = Auth::user()->plan;

        $dp->user_id = Auth::user()->id;

        $dp->save();

        echo '<h1 style="border:1px solid #f5f5f5; padding:10px; margin:150px; color:#d0d0d0; text-align:center;">Successfully charged ' . $settings->currency . '' . $up . '!<br/>

		<small style="color:#333;">Returning to dashboard</small>

		</h1>';

        //redirect to dashboard after 5 secs

        echo '<script>window.setTimeout(function(){ window.location.href = "../"; }, 5000);</script>';
    }

}

//////////////////////Deposits Routes Ends Here ////////////////////////////////////


//Note these two works same 


//->latest()->first();

//->orderby('created_at','Desc')->first();