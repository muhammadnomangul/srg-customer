<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use App\settings;
use App\UserAccounts;
use App\VerifyUser;
//use App\account;
use App\plans;
use App\agents;
use App\confirmations;
use App\users;
use App\deposits;
use App\withdrawals;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use DB;
use App\currency_rates;
use App\Currencies;
use App\Logs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\daily_investment_bonus;

class AdminController extends Controller
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

 
    public function __construct()
    {
                  

       // $this->middleware('auth');
    }
	
	public function totalSubUsers()
	{
		$loged_user_id		= Auth::user()->id;
		$loged_parent_id	= Auth::user()->parent_id;
		$Loged_user_uid		= Auth::user()->u_id;
			
		$secondLine = [];   $thirdLine  = [];   $fourthLine = [];   $fifthLine  = [];   
		$secondTotal = 0; $thirdTotal  = 0; $fourthTotal = 0; $fifthTotal  = 0;
			
		$firstLine = DB::table('users')->select('u_id','parent_id')->where('parent_id', $Loged_user_uid)->get();

		$firstTotal  =  count($firstLine);

		if(isset($firstLine))
		{ 
			$totalAmount = 0; $counter=0; $row = 0;//$firstTotal = count($firstLine);
			for($i=0; $i<$firstTotal; $i++)
			{
				$result = DB::table('users')->select('u_id','parent_id')->where('parent_id', $firstLine[$i]->u_id)->get();
				$count2 = count($result); 
				if($count2 == 0)
				{
					continue;
				} 
				array_push($secondLine, $result);
				 //  for($j=0; $j<$count2; $j++)
				 // {
				 //	$totalAmount = $totalAmount + $result[$j]->account_bal;
				 // } 
				$counter++;
				$row = $row+$count2;
			}
			  $secondLine = $secondLine;  $secondTotal = $row;
		}

		if(isset($secondLine))
		{ 
			$totalAmount2 = 0; $counter2=0; $rows = 0;

			for($i=0; $i<count($secondLine); $i++)
			{

					
				foreach($secondLine[$counter2] as $lines)
				{
					$result = DB::table('users')->select('u_id','parent_id')->where('parent_id',  $lines->u_id)->get();
					$count2 = count($result); 
					if($count2 == 0)
					{
						continue;
					} 
					array_push($thirdLine, $result);
					// for($j=0; $j<$count2; $j++)
					//{
						//  $totalAmount2 = $totalAmount2 + $result[$j]->account_bal;
					//} 
					$rows = $rows+$count2;
				}
				$counter2++;
			}
			$thirdLine = $thirdLine; $thirdTotal = $rows;
		}
		if(isset($thirdLine))
		{ 

			$totalAmount3 = 0;$counter3=0; $rows1 = 0;//$count = count($thirdLine);

			for($i=0; $i<count($thirdLine); $i++)
			{
				 
				foreach($thirdLine[$counter3] as $lines)
				{

				  $result = DB::table('users')->select('u_id','parent_id')->where('parent_id', $lines->u_id)->get();

				  $count2 = count($result);

				  if($count2 == 0)
				  {
					continue;
				  }
				  array_push($fourthLine, $result);
				// for($j=0; $j<$count2; $j++)
				//  {
				//	$totalAmount3 = $totalAmount3 + $result[$j]->account_bal;
				//  } 
				  $rows1 = $rows1+$count2;
				}
				$counter3++;
			}
				$fourthLine   = $fourthLine; $fourthTotal  = $rows1;
		}

		if(isset($fourthLine))
		{ 
			  $counter4=0; $totalAmount4 = 0; $rows2 = 0;
			  for($i=0; $i<count($fourthLine); $i++)
			  {
				
				foreach($fourthLine[$counter4] as $lines)
				{
				  $result = DB::table('users')->select('u_id','parent_id')->where('parent_id', $lines->u_id)->get();
				  $count2 = count($result); 
				  if($count2 == 0)
				  {
					continue;
				  } 
				  array_push($fifthLine, $result);
				  //for($j=0; $j<$count2; $j++)
				  //{
				  //	$totalAmount4 = $totalAmount4 + $result[$j]->account_bal;
				  //} 
				  $rows2 = $rows2+$count2;
				}
				$counter4++;
			  }
			$fifthLine = $fifthLine;   $fifthTotal = $rows2;
		}
		//echo "FirstLine - ".$firstTotal."SecondLine - ".$secondTotal. "ThirdLine - ".$thirdTotal."FourthLine - ".$fourthTotal."FifthLine - ".$fifthTotal;
		//exit;
		$total = $firstTotal+$secondTotal+$thirdTotal+$fourthTotal+$fifthTotal;
		return $total; 
		exit;
	} 

    function getTotalSubUsers(){
        //  return 1;
            return $this->totalSubUsers();
    }  
	
	public function dashboard(Request $request)
	{
        //log user out if not approved
		if(Auth::user()->status != "active"){
			$request->session()->flush();
			$request->session()->put('reged','yes');
			return redirect()->route('dashboard');
		}
		//Check if the user is referred by someone after a successful registration
		$userid        	= Auth::user()->id;
		$accounts = UserAccounts::where('user_id', $userid);
		if($accounts->count()){
			$accounts =	$accounts->first();
		}else{
            $accounts	        = new UserAccounts();
            $accounts->user_id  = $userid;
            $accounts->save();
        }
		$plans			= plans::where('id', Auth::user()->plan)->first();
		$subUsers 		= 0;//$this->totalSubUsers();
		$settings 		= settings::getSettings();
		$ratesQuery 	= currency_rates::orderby('created_at','DESC')->first();
		
		$currenciesAll  = Currencies::where('status','Active')->get();
		$wAmount 		= 0;
		$totalInvestmentBonus = round(daily_investment_bonus::where('parent_id', '=',\Auth::user()->u_id)->sum('bonus'),2);

		if(isset($currenciesAll))
		{
			foreach($currenciesAll as $curr)
			{
				$curruncy			= $curr->code;
				$withdrawAmount 	= withdrawals::where('user',$userid)->where('currency',$curruncy)->where('status','Approved')->sum('amount');
				if($curruncy == "USD")
				{
					$wAmount = $wAmount + $withdrawAmount;
				}else{
					$curr_small	= $curr->small_code;
					$rateval	= "rate_".$curr_small;
					$erates 	= DB::table('currency_rates')->select($rateval)->orderBy('id', 'desc')->first();
					$exrate 	= $erates->$rateval;
					$wAmount 	= $wAmount + ($withdrawAmount * $exrate);
				}
			}
		}
		if(Auth::user()->type == 1  ||  Auth::user()->type == '1')
		{
		    $withdrawals 	=	$deposits 		=[];
			$withdrawals 	= DB::table('withdrawals')
									->leftJoin('users', 'withdrawals.user', '=', 'users.id')
									->select('withdrawals.*', 'users.u_id')
									->orderBy('withdrawals.id', 'desc')
									->take(5)->get();
									
			$deposits 		= DB::table('deposits')
									->leftJoin('users', 'deposits.user_id', '=', 'users.id')
									->select('deposits.*', 'users.u_id')
									->orderBy('deposits.id', 'desc')
									->take(5)->get();
						return view('admin.dashboard')->with(array('title'=>'Admin Panel','uplan' => $plans,'total_sub_users'=>$subUsers,'totalInvestmentBonus'=>$totalInvestmentBonus,
								'settings' => $settings,'withdrawals'=>$withdrawals,'deposits'=>$deposits,'accounts'=>$accounts,'ex_rates'=>$ratesQuery,'wAmount'=>$wAmount
							));						
											
		}else{
			$withdrawals 	= withdrawals::where('user',Auth::user()->id)->where('status','!=','Cancelled')->orderBy('id', 'desc')->take(5)->get();
			$deposits 		= deposits::where('user_id',Auth::user()->id)->where('status','!=','Cancelled')->orderBy('id', 'desc')->take(5)->get();
		}
		
		return view('dashboard')->with(array('title'=>'User Panel','uplan' => $plans,'total_sub_users'=>$subUsers,
			'settings' => $settings,'withdrawals'=>$withdrawals,'deposits'=>$deposits,'accounts'=>$accounts,'ex_rates'=>$ratesQuery,'wAmount'=>$wAmount,'totalInvestmentBonus'=>$totalInvestmentBonus
		));
	}

	//Return manage users route
	public function all_users()
	{
       return view('admin/users_all')->with(array(
         'title'=>'All users',
        // 'users' => users::all(),
         'users' => users::orderby('created_at','DESC')->get(),
         'settings' => settings::getSettings(),
        ));
	}
 
//Return manage users route
	public function manageusers()
	{
		if(Auth::user()->type == 1  ||  Auth::user()->type == '1' ||  Auth::user()->type == '2')
		{

       return view('admin/users')->with(array(
         'title'=>'All users',
        // 'users' => users::all(),
         'users' => users::orderby('created_at','DESC')->get(),
         'settings' => settings::getSettings(),
        ));

       }
	    else
		{
            return redirect()->back()->with('Errormsg','Invalid Link!');
		}

	} 
    public function manageusers_json()
    {
           // ini_set('memory_limit', '-1');
        $users2 = DB::connection('reader')->table('users')->select(['id','name','u_id','parent_id','email','status','created_at'])->orderby('created_at','DESC');
      
        // $users2 = DB::connection('reader')->table('users')->orderby('created_at','DESC');
        return datatables()->of($users2)->toArray();
    }
	
	public function viewUserDetails(Request $request)
	{

		$user_id = $request['id'];
		if(isset($request['type']))
		{
			$type = $request['type'];
		}else
		{
			$type = "";
		}
		if($user_id != "")
		{

		$userinfo 		= DB::table('users')

								->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
								->select('users.id as userid','users.u_id','users.parent_id','users.old_pass','users.plan','users.name','users.type','users.email', 'user_accounts.*')
								->where('users.id',$user_id)
								->get();
		 
		//	$userinfo 		= DB::table('users')
		//						->leftJoin('view_user_accounts', 'users.id', '=', 'view_user_accounts.user_id')
		//						->select('users.*', 'view_user_accounts.*')->where('users.id',$user_id)->get();

		
			$userNo		    = $user_id;			
			$userUID		= $userinfo[0]->u_id;
			$userPlanID 	= $userinfo[0]->plan;
			$oldpassword 	= $userinfo[0]->old_pass;
			$isverify       = $userinfo[0]->is_manual_verified;
			
			
			$usrplan		= plans::where('id',$userPlanID)->first();
			if(isset($usrplan))
			{
				$planname = $usrplan->name;
			}else{
				$planname = "Plan NOT SET";
			}

			$details = '<div class="modal-header">

			        <button type="button" class="close" data-dismiss="modal">&times;</button>

			        <h4 class="modal-title" style="text-align:center;">User Details</h4>

			      </div>

			      <div class="modal-body">

					<table width="90%" align="center">

					<tr class="modal-header" style="color:red; padding-bottom: 25px !important;" ><td colspan="2"><h4 class="modal-title">User Info: Sr# '.$userNo.'</h4></td>';


			if(Auth::user()->type == 1 || Auth::user()->type == 2)
			{
					
				 if($isverify == 1 || $isverify == '1')
				 {

        $details .= '<td colspan="2" ><p style="color:green; float:right; border: 3px solid green; border-radius: 3px; padding: 15px; ">Verified <i class="fas fa-check"></i></p></td></tr>';
				}
				else
				{
		$details .= '<td colspan="2" ><h4 style="float:right;"><p id="verify"><a class="btn btn-primary" onclick="is_verify_Func('.$userNo.')" href="#">Verify</a></p></h4></td></tr>';
				} 

              }
				
				
			
			   $details .= '<tr style="padding-bottom: 25px !important;"><td> <label>Name: </label></td><td>'.$userinfo[0]->name.' </td>
                

			   <td > <label>User ID : </label></td><td>'.$userUID.'</td> </tr>

					<tr style="padding-bottom: 25px !important;"><td ><strong>User Plan: </strong></td><td>'.$planname.' </td><td>	<strong>Parent Id : </strong></td><td>'.$userinfo[0]->parent_id.' </td></tr>	

					<tr style="padding-bottom: 25px !important;"><td><strong>Email : </strong></td><td>'.$userinfo[0]->email.' </td></tr>';
                  

			if(Auth::user()->type == 1 || Auth::user()->type == 2)
			{
				if($userinfo[0]->type == 1)
				{
					$type = "SuperAdmin";
				}else if($userinfo[0]->type == 2)
				{
					$type = "Manager";
				}else if($userinfo[0]->type == 3)
				{
					$type = "Awarded";
				}else{
					$type = "Customer";
				}
					$details .=  '<tr><td width="25%"><strong>Old Password: </strong></td><td width="25%">'.$oldpassword.'</td>
					<td width="25%"><strong>Role :</strong></td><td width="25%">'.$type.'</td></tr>';
			} 		
			$details .=  '<tr><td colspan="4"></td></tr>';
			if($type == "partners"){				
				$details .= '<tr style="margin-top:50px;><td colspan="4"></td></tr>
							<tr class="modal-header" style="color:red;padding-bottom: 25px !important; "><td colspan="4"><h4 class="modal-title">Deposits Balance Info:</h4></td></tr>

							<tr style="padding-bottom: 25px !important;"><td>'.number_format($userinfo[0]->balance_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->balance_btc, 8, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->balance_eth, 8, '.', '').'(ETH)</td><td>'.number_format($userinfo[0]->balance_bch, 6, '.', '').'(BCH)</td></tr>
							<tr style="padding-bottom: 25px !important;"><td>'.number_format($userinfo[0]->balance_ltc, 6, '.', '').'(LTC)</td><td>'.number_format($userinfo[0]->balance_dash, 6, '.', '').'(DASH)</td><td>'.number_format($userinfo[0]->balance_xrp, 6, '.', '').'(XRP)</td><td>'.number_format($userinfo[0]->balance_zec, 6, '.', '').'(ZEC)</td></tr>
				</table></div>';		
			}else
			{

				$details .= '<tr class="modal-header"><td colspan="4"><h4 class="modal-title"> Accounts Balance Info : </h4></td></tr>
						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Bonus: </h4></td></tr>	
						<tr style="margin-top:50px;"><td><strong>Bonus : </strong></td><td>'.number_format($userinfo[0]->reference_bonus, 2, '.', '').'</td><td><strong>Latest Bonus:</strong></td><td>'.number_format($userinfo[0]->latest_bonus, 2, '.', '').'</td></tr>';	
    			if(Auth::user()->type == 1 || Auth::user()->type == 2)
    			{
    				$details .= '<tr style="margin-top:50px;"><td><strong>Old Bonus : </strong></td><td>'.number_format($userinfo[0]->old_ref_bonus, 2, '.', '').'</td></tr>';	
    			}
				$details .= '<tr><td colspan="4"></td></tr>

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Profit Info: </h4></td></tr>

						<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->profit_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->profit_btc, 8, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->profit_eth, 8, '.', '').'(ETH)</td><td>'.number_format($userinfo[0]->profit_bch, 6, '.', '').'(BCH)</td></tr>
						<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->profit_ltc, 6, '.', '').'(LTC)</td><td>'.number_format($userinfo[0]->profit_dash, 6, '.', '').'(DASH)</td><td>'.number_format($userinfo[0]->profit_xrp, 6, '.', '').'(XRP)</td><td>'.number_format($userinfo[0]->profit_zec, 6, '.', '').'(ZEC)</td></tr>'
						
						;
			if(Auth::user()->type == 1 || Auth::user()->type == 2)
			{		
				$details .= '

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Old Profit Info: </h4></td></tr>

						<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->old_profit_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->old_profit_btc, 8, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->old_profit_eth, 8, '.', '').'(ETH)</td></tr>';

				}
					$details .= '<tr><td colspan="4"></td></tr>
							<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Deposits Balance Info:</h4></td></tr>
							<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->balance_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->balance_btc, 8, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->balance_eth, 8, '.', '').'(ETH)</td><td>'.number_format($userinfo[0]->balance_bch, 6, '.', '').'(BCH)</td></tr>
							<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->balance_ltc, 6, '.', '').'(LTC)</td><td>'.number_format($userinfo[0]->balance_dash, 6, '.', '').'(DASH)</td><td>'.number_format($userinfo[0]->balance_xrp, 6, '.', '').'(XRP)</td><td>'.number_format($userinfo[0]->balance_zec, 6, '.', '').'(ZEC)</td></tr>	
							<tr><td colspan="4"></td></tr>
							<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Sold Balance Info:</h4></td></tr>
							<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->sold_bal_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->sold_bal_btc, 8, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->sold_bal_eth, 8, '.', '').'(ETH)</td><td>'.number_format($userinfo[0]->sold_bal_bch, 6, '.', '').'(BCH)</td></tr>
							<tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->sold_bal_ltc, 6, '.', '').'(LTC)</td><td>'.number_format($userinfo[0]->sold_bal_dash,6, '.', '').'(DASH)</td><td>'.number_format($userinfo[0]->sold_bal_xrp, 6, '.', '').'(XRP)</td><td>'.number_format($userinfo[0]->sold_bal_zec, 6, '.', '').'(ZEC)</td></tr>	
							</table></div>'; 
			}
			echo $details;

		}
	
	}
   
   
	public function saveuser(Request $request)
	{
		$this->validate($request,[
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
			'Answer' => 'same:Captcha_Result',
		]);
		// Get the last created order
		$lastUser = User::orderBy('created_at', 'desc')->first();
		if (! $lastUser )
		{	// We get here if there is no user at all
			// If there is no number set it to 0, which will be 1 at the end.
			$unique_id = "B4U0001";
		}
		else 
		{
			$number 	= substr($lastUser->u_id, 3);
			$number 	= $number + 1;
			$unique_id 	= "B4U000".$number;
			// If we have B4U0001 in the database then we only want the number
			// So the substr returns this 0001
		}
	   $settings	=	settings::getSettings();
	   $user = User::create([
                'name'      => $request['name'],
                'email'     => $request['email'],
                'password'  => bcrypt($request['password']),
                'plan'      => "1",
				'photo'		=> 'male.png',
    			'u_id' 		=> $unique_id,
                'parent_id'	=> Auth::user()->u_id,
                'ref_link'  => $settings->site_address.'/ref/'.$unique_id 	//assign referal link to user
            ]); 
		$thisid = $user->id;
	
	 

		$userinfo 	=  users::where('id', $thisid)->update([
		   'ref_link' => $settings->site_address.'/ref/'.$unique_id,
		   ]);
		// exit;
		$verifyUser = VerifyUser::create([
    			'user_id' => $thisid,
    			'token' => str_random(40)
    	]);
		$userAcc = UserAccounts::create([
    		'user_id' => $thisid,
    	]);
		$userinfo =  users::where('id', $thisid)->first();
		if($request['email'] != '')
		{
			$name       	= @trim(stripslashes($request['name'])); 
			$password       = @trim(stripslashes($request['password'])); 
			$email_to      	= @trim(stripslashes($request['email']));
			$subject    	= "You have Registered On B4U Global successfully.";
			$email_from 	= "b4uglobal@gmail.com";
			//$email_from 	= "noreply@b4uglobal.com";
			$from_Name 		= "B4U Global";
			$path			= url('user/verify', $user->verifyUser->token);
				//Please click on <a class=\"btn btn-primary\" href='".$path."'> Verify Email</a> to join us.
				//<div><img src='".$logoPath."' style='width: 200px; padding:0 0 20px 0; text-align:left;'></div>
			$message = 
                    "<html>
                        <body align=\"left\" style=\"height: 100%;\">
                            <div>
								
								<div>
									<table style=\"width: 100%;\">
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Dear ".$name.",
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Thank you for registered on ". $from_Name.". Your login Email : ".$email_to." and password : ".$password."
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												
												<form action='".$path."' method=\"get\">
													Please click on <button type=\"submit\" formaction='".$path."'>Verify Email</button> to join us.
												</form>
											</td>
										</tr>
										<tr>
											<td style=\"text-align:left; padding:10px 0;\">
												Thanks for using  ".$from_Name.".
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Your Sincerely,
											</td>
										</tr>
										<tr>
											<td style=\"padding:10px 0; text-align:left;\">
												Team ".$from_Name."
											</td>
										</tr>
										
									</table>
								</div>
							</div>
						</body>
				</html>"; 
				//exit("Registered User ". $message);
				
				// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				// More headers
			$headers .= 'From: B4U New Registrations <noreply@b4uglobal.com>' . "\r\n";
				//$headers .= 'Cc: myboss@example.com' . "\r\n";
						
			$success = @mail($email_to, $subject, $message , $headers);
			//	return $user;
		} 
		
		$parent_id 	= Auth::user()->u_id;
		$name 		= Auth::user()->name;
		$email 		= Auth::user()->email;
		
		
		if($parent_id != "")
		{
			$parentEmail	=	$email;
			$parentName		=	$name;
			if($parentEmail != '')
			{
				$name       	= @trim(stripslashes($parentName));  
				$email_to      	= @trim(stripslashes($parentEmail));
				$subject    	= "New User Registered At Your Downline Using B4U Global.";
				$email_from 	= "noreply@b4uglobal.com";
				//$email_from 	= "noreply@b4uglobal.com";
				$from_Name 		= "B4U Global";
				$message = 
                "<html>
                    <body align=\"left\" style=\"height: 100%;\">
                        <div>
							<div>
								<table style=\"width: 100%;\">
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											Dear ".$name.",
										</td>
									</tr>
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											A new user <h3>'$unique_id'</h3> has registered at your downline. You will recieve bonuses from that user as soon as he/she will invest at our system.
										</td>
									</tr>
									<tr>
										<td style=\"text-align:left; padding:10px 0;\">
											Thanks for using  ".$from_Name.".
										</td>
									</tr>
									<tr>
										<td style=\"padding:10px 0; text-align:left;\">
											Your Sincerely,
										</td>
									</tr>
									<tr>
										<td style=\"padding:10px 0; text-align:left;\">
											Team ".$from_Name."
										</td>
									</tr>
										
								</table>
							</div>
						</div>
					</body>
				</html>"; 
				//exit("Registered User ". $message);
				
					// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					// More headers
				$headers .= 'From: B4U Downline Registrations <noreply@b4uglobal.com>' . "\r\n";
					//$headers .= 'Cc: myboss@example.com' . "\r\n";
							
				$success = @mail($email_to, $subject, $message , $headers);
			}
		}	
		return redirect()->back()->with('message', 'User Registered Sucessful!');
	}
 

   ///Hide User
   public function hide($id){
	 
	   Logs::where('id',$id)
	   ->update([
	   'hide_bit' => '1',
	   ]);

        return redirect()->back()
	   ->with('message', 'Action Sucessful!');
	}

	////Unhide User

	public function show($id){
	 
	   Logs::where('id',$id)
	   ->update([
	   'hide_bit' => '0',
	   ]);


	   return redirect()->back()
	   ->with('message', 'Action Sucessful!');
	}
    
	//block user
	public function ublock($id){
	 
	   users::where('id',$id)
	   ->update([
	   'status' => 'blocked',
	   ]);


         $event="User status has been change to 'Blocked'";
         $trade_id="";
         $admin_id=Auth::user()->u_id;
       
        $user_id1=users::where('id',$id)->select('users.u_id')->first();
        $user_id=$user_id1->u_id;


         $this->adminLogs($user_id,$admin_id,$trade_id,$event);




	   return redirect()->back()
	   ->with('message', 'Action Sucessful!');
	}

	//unblock user
	public function unblock($id){

	   users::where('id',$id)
	   ->update([
	   'status' => 'active',
	   ]);
    
         $event="User status has been change to 'Active'";
         $trade_id="";
         $admin_id=Auth::user()->u_id;
       
        $user_id1=users::where('id', $id)->select('users.u_id')->first();
        $user_id=$user_id1->u_id;


         $this->adminLogs($user_id,$admin_id,$trade_id,$event);

     

	   return redirect()->back()
	   ->with('message', 'Action Sucessful!');
	}

	  public function verifyUserAcc(Request $request){
	 	 $id 		= $request['id'];
	 //	 echo $id;
	 //	 exit(" i am in controller");
       // $type 		= $request['type'];
	   UserAccounts::where('user_id',$id)->update([
	   'is_manual_verified' => 1
	   ]);

	   echo "Successful";
	   exit();
	  
	}
 
   //update Profile photo to DB
	public function updatephoto(Request $request)
	{
       
		$this->validate($request,[
			'photo' => 'mimes:jpg,jpeg,png|max:5000',
		]);
         //photo
        $img=$request->file('photo');
        $upload_dir='images';
         
        $image=$img->getClientOriginalName();
        $move=$img->move($upload_dir, $image);
         users::where('id', $request['id'])
         ->update([
         'photo' => $image,
         ]);
        return redirect()->back()
         ->with('message', 'Photo Updated Sucessful');
	}

   //return add account form
	public function accountdetails(Request $request){
		return view('updateacct')->with(array(
       'title'=>'Update account details',
       'settings' => settings::getSettings()
       ));
	}
   //update account and contact info
	public function updateacct(Request $request){
   
         users::where('id', $request['id'])
         ->update([
         'bank_name' 	=> $request['bank_name'],
         'account_name' => $request['account_name'], 
         'account_no' 	=>	$request['account_number'], 
         'btc_address' 	=>	$request['btc_address'], 
         'eth_address' 	=>	$request['eth_address'], 
		 'bch_address' 	=>	$request['bch_address'], 
         'ltc_address' 	=>	$request['ltc_address'], 
		 'xrp_address' 	=>	$request['xrp_address'],
		 'dash_address' =>	$request['dash_address'],
		 'zec_address' 	=>	$request['zec_address'],
		 //'dsh_address' 	=>	$request['dsh_address'],
         ]);
         return redirect()->back()
         ->with('message', 'User updated Sucessful');
	}

   //return add change password form
	public function changepassword(Request $request){
		return view('changepassword')->with(array('title'=>'Change Password','settings' => settings::getSettings()));
	}

   //Update Password
   public function updatepass(Request $request)
   {
		if(!password_verify($request['old_password'],$request['current_password']))
		{
			return redirect()->back()->with('message', 'Incorrect Old Password');
		}
		$this->validate($request, [
			'password_confirmation' => 'same:password',
			'password' => 'min:6',
		]);

        users::where('id', $request['id'])
        ->update([
			'password' => bcrypt($request['password']),
        ]);
        return redirect()->back()->with('message', 'Password Updated Sucessful');
	} 
 //Update User Accounts 
	public function updateAccBalance(Request $request)
	{
		$user_Id 			= 	$request['userid'];
		$userAccBalUpdate	=	UserAccounts::where('user_id',$user_Id)->first();
		
		if(isset($request['reference_bonus'])){
			$reference_bonus 	= $request['reference_bonus'];
		}else{
			$reference_bonus 	= $userAccBalUpdate->reference_bonus;
		}
		
		/* 
		if(isset($request['is_manual_verified']))
		{
			$is_manual_verified 	= $request['is_manual_verified']; 
		}else{
			$is_manual_verified 	= $userAccBalUpdate->is_manual_verified;
		} 
		*/
		// USD
		if(isset($request['balance_usd'])){
			$balance_usd 			= $request['balance_usd'];
		}else{
			$balance_usd 			= $userAccBalUpdate->balance_usd;
		} 
		
		if(isset($request['sold_bal_usd'])){
			$sold_bal_usd 			=	$request['sold_bal_usd'];
		}else{
			$sold_bal_usd 			= $userAccBalUpdate->sold_bal_usd;
		} 
		
		if(isset($request['profit_usd'])){		 
			$profit_usd 			=	$request['profit_usd'];
		}else{
			$profit_usd 			= $userAccBalUpdate->profit_usd;
		}
		
		if(isset($request['waiting_profit_usd'])){
			$waiting_profit_usd 	=	$request['waiting_profit_usd']; 
		}else{
			$waiting_profit_usd 	= $userAccBalUpdate->waiting_profit_usd;
		} 
		
		// BTC
		if(isset($request['balance_btc'])){
         $balance_btc 				= $request['balance_btc'];
		 }else{
			$balance_btc 			= $userAccBalUpdate->balance_btc;
		}
		
		if(isset($request['sold_bal_btc'])){
         $sold_bal_btc 				=	$request['sold_bal_btc'];
		}else{
			$sold_bal_btc 			= $userAccBalUpdate->sold_bal_btc;
		}
		
		if(isset($request['profit_btc'])){		 
         $profit_btc 				=	$request['profit_btc'];
		}else{
			$profit_btc 			= $userAccBalUpdate->profit_btc;
		}
		
		if(isset($request['waiting_profit_btc'])){
         $waiting_profit_btc 		=	$request['waiting_profit_btc']; 
		}else{
			$waiting_profit_btc 	= $userAccBalUpdate->waiting_profit_btc;
		} 
		// ETH
		if(isset($request['balance_eth'])){
         $balance_eth 				= $request['balance_eth'];
		}else{
			$balance_eth 			= $userAccBalUpdate->balance_eth;
		} 
		if(isset($request['sold_bal_eth'])){
         $sold_bal_eth 				=	$request['sold_bal_eth'];
		}else{
			$sold_bal_eth 			= $userAccBalUpdate->sold_bal_eth;
		} 
		
		if(isset($request['profit_eth'])){		 
         $profit_eth 				=	$request['profit_eth'];
		}else{
			$profit_eth 			=  	$userAccBalUpdate->profit_eth;
		} 
		if(isset($request['waiting_profit_eth'])){
         $waiting_profit_eth 		= 	$request['waiting_profit_eth']; 
		}else{
			$waiting_profit_eth 	= 	$userAccBalUpdate->waiting_profit_eth;
		} 
		
		// BCH
		if(isset($request['balance_bch'])){
         $balance_bch 				= $request['balance_bch'];
		}else{
			$balance_bch 			= $userAccBalUpdate->balance_bch;
		} 
		if(isset($request['sold_bal_bch'])){
         $sold_bal_bch				=	$request['sold_bal_bch'];
		}else{
			$sold_bal_bch 			= $userAccBalUpdate->sold_bal_bch;
		} 
		
		if(isset($request['profit_bch'])){		 
			$profit_bch 			=	$request['profit_bch'];
		}else{
			$profit_bch 			=  	$userAccBalUpdate->profit_bch;
		} 
		
		if(isset($request['waiting_profit_bch'])){
			$waiting_profit_bch 	= 	$request['waiting_profit_bch']; 
		}else{
			$waiting_profit_bch 	= 	$userAccBalUpdate->waiting_profit_bch;
		} 
		
// LTC
		if(isset($request['balance_ltc'])){
			$balance_ltc 			= $request['balance_ltc'];
		}else{
			$balance_ltc 			= $userAccBalUpdate->balance_ltc;
		} 
		if(isset($request['sold_bal_ltc'])){
			$sold_bal_ltc 				=	$request['sold_bal_ltc'];
		}else{
			$sold_bal_ltc 			= $userAccBalUpdate->sold_bal_ltc;
		} 
		
		if(isset($request['profit_ltc'])){		 
			$profit_ltc 			=	$request['profit_ltc'];
		}else{
			$profit_ltc 			=  	$userAccBalUpdate->profit_ltc;
		} 
		if(isset($request['waiting_profit_ltc'])){
			$waiting_profit_ltc 	= 	$request['waiting_profit_ltc']; 
		}else{
			$waiting_profit_ltc 	= 	$userAccBalUpdate->waiting_profit_ltc;
		} 
// XRP
		if(isset($request['balance_xrp'])){
         $balance_xrp 				= $request['balance_xrp'];
		}else{
			$balance_xrp 			= $userAccBalUpdate->balance_xrp;
		} 
		if(isset($request['sold_bal_xrp'])){
         $sold_bal_xrp				=	$request['sold_bal_xrp'];
		}else{
			$sold_bal_xrp 			=   $userAccBalUpdate->sold_bal_xrp;
		} 
		
		if(isset($request['profit_xrp'])){		 
			$profit_xrp 			=	$request['profit_xrp'];
		}else{
			$profit_xrp 			=  	$userAccBalUpdate->profit_xrp;
		} 
		if(isset($request['waiting_profit_eth'])){
			$waiting_profit_xrp 	= 	$request['waiting_profit_xrp']; 
		}else{
			$waiting_profit_xrp 	= 	$userAccBalUpdate->waiting_profit_xrp;
		} 
// DASH
		if(isset($request['balance_dash'])){
         $balance_dash 				= $request['balance_dash'];
		}else{
			$balance_dash 			= $userAccBalUpdate->balance_dash;
		} 
		if(isset($request['sold_bal_dash'])){
         $sold_bal_dash 			=	$request['sold_bal_dash'];
		}else{
			$sold_bal_dash 			=  $userAccBalUpdate->sold_bal_dash;
		} 
		
		if(isset($request['profit_dash'])){		 
			$profit_dash 			=	$request['profit_dash'];
		}else{
			$profit_dash 			=  	$userAccBalUpdate->profit_dash;
		} 
		if(isset($request['waiting_profit_dash'])){
			$waiting_profit_dash 		= 	$request['waiting_profit_dash']; 
		}else{
			$waiting_profit_dash 	= 	$userAccBalUpdate->waiting_profit_dash;
		} 
		
//ZEC
		if(isset($request['balance_zec'])){
         $balance_zec 				= $request['balance_zec'];
		}else{
			$balance_zec 			= $userAccBalUpdate->balance_zec;
		} 
		if(isset($request['sold_bal_zec'])){
         $sold_bal_zec 			=	$request['sold_bal_zec'];
		}else{
			$sold_bal_zec 			=  $userAccBalUpdate->sold_bal_zec;
		} 
		
		if(isset($request['profit_zec'])){		 
			$profit_zec 			=	$request['profit_zec'];
		}else{
			$profit_zec 			=  	$userAccBalUpdate->profit_zec;
		} 
		if(isset($request['waiting_profit_zec'])){
			$waiting_profit_zec 		= 	$request['waiting_profit_zec']; 
		}else{
			$waiting_profit_zec 	= 	$userAccBalUpdate->waiting_profit_zec;
		}
		//'is_manual_verified' =>$is_manual_verified,
		UserAccounts::where('user_id', $request['userid'])
         ->update([
			'reference_bonus' =>$reference_bonus,
			'balance_usd' =>$balance_usd,'sold_bal_usd' =>$sold_bal_usd,
			'profit_usd' =>$profit_usd,'waiting_profit_usd' =>$waiting_profit_usd,
			
			'balance_btc' =>$balance_btc,'sold_bal_btc' =>$sold_bal_btc,
			'profit_btc' =>$profit_btc,'waiting_profit_btc' =>$waiting_profit_btc,
			
			'balance_eth' =>$balance_eth,'sold_bal_eth' =>$sold_bal_eth,
			'profit_eth' =>$profit_eth,'waiting_profit_eth' =>$waiting_profit_eth,
			
			'balance_bch' =>$balance_bch,'sold_bal_bch' =>$sold_bal_bch,
			'profit_bch' =>$profit_bch,'waiting_profit_bch' =>$waiting_profit_bch,
			
			'balance_ltc' =>$balance_ltc,'sold_bal_ltc' =>$sold_bal_ltc,
			'profit_ltc' =>$profit_ltc,'waiting_profit_ltc' =>$waiting_profit_ltc,
			
			'balance_dash' =>$balance_dash,'sold_bal_dash' =>$sold_bal_dash,
			'profit_dash' =>$profit_dash,'waiting_profit_dash' =>$waiting_profit_dash,
			
			'balance_xrp' =>$balance_xrp,'sold_bal_xrp' =>$sold_bal_xrp,
			'profit_xrp' =>$profit_xrp,'waiting_profit_xrp' =>$waiting_profit_xrp,
			
			'balance_zec' =>$balance_zec,'sold_bal_zec' =>$sold_bal_zec,
			'profit_zec' =>$profit_zec,'waiting_profit_zec' =>$waiting_profit_zec,
			//'dsh_address' 	=>	$request['dsh_address'],
        ]);
         $event="User Balance has been updated";
         $trade_id="";
         $admin_id=Auth::user()->u_id;
       
        $user_id1=users::where('id',$request['userid'])->select('users.u_id')->first();
        $user_id=$user_id1->u_id;


         $this->adminLogs($user_id,$admin_id,$trade_id,$event);


		return redirect()->back()->with('message', 'Account Updated Sucessfully');
	}
	//update account and contact info
	public function updateUserBal(Request $request)
	{
   /* 	if($userAccBalance->is_manual_verified == 1)
			{
				$acc_details 	.=  '<label class="col-sm-6" style="color:green;">Staus : Varified </label></div>';
			}else{
				$acc_details 	.=  '<label class="col-sm-6" style="color:red;">Staus : Un-Varified </label></div>';
			}  */
		$user_Id		 = 	$request['id'];
		
        $userAccBalance	 =	UserAccounts::where('user_id',$user_Id)->first();

		$acc_details 	 =  '<div class"row form-group"><h3 style="padding-left:12px;">USD </h3></div><div class"row form-group"><label class="col-sm-6"> Status :'; 
			if($userAccBalance->is_manual_verified == 1)
			{
				$acc_details 	.=  'Varified';
			}else{
				$acc_details 	.=  'Un-Varified';
			}
		$acc_details 	.=  '</label></div>';
	
			$acc_details 	.=  '<div class"row form-group">
								<label class="col-sm-3">Ref Bonus:</label>
								<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->reference_bonus.'" type="text" name="reference_bonus" id="reference_bonus" required>
								<div>';
							
			$acc_details 	.=  '<div class"row form-group">
								<label class="col-sm-3">Balance :</label>

								<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_usd.'" type="text" name="balance_usd" id="balance_usd" required>
							
								<label class="col-sm-3">Sold Balance:</label>

								<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_usd.'" type="text" name="sold_bal_usd" id="sold_bal_usd" required>
							
							</div>
							<div class"row form-group">
								<label class="col-sm-3">Profit:</label>

								<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_usd.'" type="text" name="profit_usd" id="profit_usd" required>
								<label class="col-sm-3">Waiting Profit:</label>

								<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_usd.'" type="text" name="waiting_profit_usd" id="waiting_profit_usd" required>
							</div>';


		$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;">BTC </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_btc.'" type="text" name="balance_btc" id="balance_btc" required>
							<label class="col-sm-3">Sold Balance :</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_btc.'" type="text" name="sold_bal_btc" id="sold_bal_btc" required>
						
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_btc.'" type="text" name="profit_btc" id="profit_btc" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_btc.'" type="text" name="waiting_profit_btc" id="waiting_profit_btc" required>
						</div>';



		$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> ETH </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_eth.'" type="text" name="balance_eth" id="balance_eth" required>
							<label class="col-sm-3">Sold Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_eth.'" type="text" name="sold_bal_eth" id="sold_bal_eth" required>
	
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_eth.'" type="text" name="profit_eth" id="profit_eth" required>
								<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_eth.'" type="text" name="waiting_profit_eth" id="waiting_profit_eth" required>
						</div>';
			
			$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> BCH </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_bch.'" type="text" name="balance_bch" id="balance_bch" required>
							<label class="col-sm-3">Sold Balance :</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_bch.'" type="text" name="sold_bal_bch" id="sold_bal_bch" required>
						
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_bch.'" type="text" name="profit_bch" id="profit_bch" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_bch.'" type="text" name="waiting_profit_bch" id="waiting_profit_bch" required>
						</div>';



		$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> LTC </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_ltc.'" type="text" name="balance_ltc" id="balance_ltc" required>
							<label class="col-sm-3">Sold Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_ltc.'" type="text" name="sold_bal_ltc" id="sold_bal_ltc" required>
	
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_ltc.'" type="text" name="profit_ltc" id="profit_ltc" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_ltc.'" type="text" name="waiting_profit_ltc" id="waiting_profit_ltc" required>
						</div>';
			$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> DASH </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_dash.'" type="text" name="balance_dash" id="balance_dash" required>
							<label class="col-sm-3">Sold Balance :</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_dash.'" type="text" name="sold_bal_dash" id="sold_bal_dash" required>
						
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_dash.'" type="text" name="profit_dash" id="profit_dash" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_dash.'" type="text" name="waiting_profit_dash" id="waiting_profit_dash" required>
						</div>';



		$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> XRP </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_xrp.'" type="text" name="balance_xrp" id="balance_xrp" required>
							<label class="col-sm-3">Sold Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_xrp.'" type="text" name="sold_bal_xrp" id="sold_bal_xrp" required>
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_xrp.'" type="text" name="profit_xrp" id="profit_xrp" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_xrp.'" type="text" name="waiting_profit_xrp" id="waiting_profit_xrp" required>
						</div>';
		$acc_details .= '<div class"row form-group"><h3 style="padding-left:12px;"> ZEC </h3></div>
						<div class"row form-group">
							<label class="col-sm-3">Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->balance_zec.'" type="text" name="balance_zec" id="balance_zec" required>
							<label class="col-sm-3">Sold Balance:</label>
							<input style="padding:5px;" class="form-input col-sm-3" placeholder="Enter expected return" value="'.$userAccBalance->sold_bal_zec.'" type="text" name="sold_bal_zec" id="sold_bal_zec" required>
						</div>
						<div class"row form-group">
							<label class="col-sm-3">Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->profit_zec.'" type="text" name="profit_zec" id="profit_zec" required>
							<label class="col-sm-3">Waiting Profit:</label>
							<input style="padding:5px;" class="form-input col-sm-3" value="'.$userAccBalance->waiting_profit_zec.'" type="text" name="waiting_profit_zec" id="waiting_profit_zec" required>
						</div>';

			$acc_details .= '<div class"row form-group"><label class="col-sm-12"></label></div>
							<div class"row form-group">
								<input type="hidden" name="userid" value="'.$userAccBalance->user_id.'">
								<input type="hidden" name="_token" value="'.csrf_token().'">
								<label class="col-sm-3"></label>
								<input type="submit" class="btn btn-sm btn-primary" value="Update Balance">
								<button type="button" class="btn btn-sm btn-default" id="reset" onclick="resetFunc();">Reset</button>
								
							</div>';
			
		echo $acc_details;
	}


}

