<?php



namespace App\Http\Controllers;



use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\settings;
use App\users;
use App\ph;
use App\gh;
use App\withdrawals;
use App\deposits;
use App\currency_rates;
use App\deposit_investment_bonus;
use App\ref_investment_bonus_rules;
use App\ref_profit_bonus_rules;
use App\UserAccounts;
use App\Currencies;
use DB;
use Mail;

class WithdrawalController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()
    {
		$this->middleware('auth');
		// GET EMAIL TEMPLATE DATA SENT IN EMAILS
		function getEmailTemplate($event)
        {
			$emailDetails = email_templates::where('title', 'Like', $event)->first();

			return $emailDetails;
		}
    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

		/*  $rules = [

			 'trans_id' => 'required|string|max:255',

			  'proof' => 'mimes:jpg,jpeg,png|max:4000',

			  //|digits_between:10,10

			];

			$customMessages = [

					'trans_id.required' => 'Transaction ID field is required.'

				];

			$validator =  $this->validate($request, $rules, $customMessages); 

	*/

	//Return withdrawals route

	public function withdrawals()
	{

		$title 				=	'withdrawals';

		$settings			=	settings::getSettings();

		$widrawalQry 		= 	withdrawals::where('user', Auth::user()->id)
		                           ->where('status','!=','Cancelled')
				                    ->where('status','!=','Deleted')
		                            ->orderby('created_at','DESC')->get();
		
		$UserAccountsQry 	= 	UserAccounts::where('id', Auth::user()->id)->first();
		$currency_RatesQry 	= 	currency_rates::orderby('created_at','DESC')->first();
		$currencies 		=   Currencies::distinct('code')->where('status','Active')->get();
		//DB::enableQueryLog();
		//dd($UserAccountsQry);
		
		$uid 				= Auth::user()->u_id;

		$parentid 			= Auth::user()->parent_id;

		$related_users  	= users::where('users.parent_id', '=',$uid )->orwhere('users.u_id', '=', $parentid)

		->select('users.id','users.u_id','users.parent_id','users.name','users.email')->get();

		return view('withdrawals')->with(array('title'=>$title,'withdrawals' => $widrawalQry,'settings' => $settings,'usersList' => $related_users,'accountsInfo'=>$UserAccountsQry,'ratesQuery'=>$currency_RatesQry,'currencies'=>$currencies));	
	} 

	  
		 //Return ADMIN manage withdrawals route

	public function mwithdrawals()
	{
		if(Auth::user()->type == 1)
		{
			$title 			=	'Manage users withdrawals';

			$settings		=	settings::getSettings();
			//$UserAccountsQry 	= 	UserAccounts::where('id', Auth::user()->id)->first();
			//$withdrawals 	= 	withdrawals::where('status','Approved')->orwhere('status','Pending')->orderby('created_at','DESC')->get();
			$withdrawals 	= 	DB::table('withdrawals')
							->join('user_accounts AS accounts','withdrawals.user','=','accounts.user_id')
							->join('users', 'users.id', '=', 'withdrawals.user')
							->select('withdrawals.*','accounts.total_deduct','users.u_id')
							->where('withdrawals.status','Approved')
							->orwhere('withdrawals.status','Pending')
							->orderby('created_at','DESC')
							->get(); 
			
			return view('admin/mwithdrawals')->with(array('title'=>$title,'withdrawals' => $withdrawals,'settings' => $settings));

		}else{
			  return redirect()->intended('dashboard/withdrawals')->with('message', 'You are not allowed!.');
		}
	}



	//Return Cancel withdrawals route

	public function mcwithdrawal()
	{
		if(Auth::user()->type == 1)
		{
				 $title 			=	'Manage Trashed withdrawals';

				 $settings			=	settings::getSettings();

				 $withdrawals 		= 	withdrawals::where('status','Cancelled')->orwhere('status','Deleted')->orderby('created_at','DESC')->get();
				//$UserAccountsQry 	= 	UserAccounts::where('id', Auth::user()->id)->first();
				 return view('admin/mcwithdrawal')->with(array('title'=>$title,'withdrawals' => $withdrawals,'settings' => $settings));

		}else{
			 return redirect()->intended('dashboard/withdrawals')->with('message', 'You are not allowed!.');
		}
	}
	
	
	public function pwithdrawals()
	{
		if(Auth::user()->type == 1)
		{
			$title 			=	'Manage Pending withdrawals';

			$settings		=	settings::getSettings();
			$withdrawals 	= 	DB::table('withdrawals')
							    ->join('user_accounts AS accounts','withdrawals.user','=','accounts.user_id')
							    ->join('users', 'users.id', '=', 'withdrawals.user')
							    ->select('withdrawals.*','accounts.total_deduct','users.u_id')
							    ->where('withdrawals.status','Pending')
							    ->orderby('created_at','DESC')
							    ->get(); 
			
			return view('admin/pwithdrawals')->with(array('title'=>$title,'withdrawals' => $withdrawals,'settings' => $settings));

		}else{
			  return redirect()->intended('dashboard/withdrawals')->with('message', 'You are not allowed!.');
		}
	}
		
	public function awithdrawals()
	{
		if(Auth::user()->type == 1)
		{
			$title 			=	'Manage Approved withdrawals';

			$settings		=	settings::getSettings();
			$withdrawals 	= 	DB::table('withdrawals')
							    ->join('user_accounts AS accounts','withdrawals.user','=','accounts.user_id')
							    ->join('users', 'users.id', '=', 'withdrawals.user')
							    ->select('withdrawals.*','accounts.total_deduct','users.u_id')
							    ->where('withdrawals.status','Approved')
							    ->orderby('created_at','DESC')
							    ->get(); 
			
			return view('admin/awithdrawals')->with(array('title'=>$title,'withdrawals' => $withdrawals,'settings' => $settings));

		}else{
			  return redirect()->intended('dashboard/withdrawals')->with('message', 'You are not allowed!.');
		}
	}

	//Return relatedUsers Ajax Search

	public function relatedUsers(Request $request)
	{
		$searchData	= $request['search'];

		$uid = Auth::user()->u_id;

		$parentid = Auth::user()->parent_id;

		$related_users  =  users::where('users.parent_id', '=',$uid )->where('users.u_id', '=', $parentid)

		->where('users.name','Like','%'.$searchData.'%')->orwhere('users.u_id','Like','%'.$searchData.'%')

		->select('users.id','users.u_id','users.parent_id','users.name','users.email')->take(5);

		echo json_encode($related_users);
		//echo $related_users;
	}


	public function updateWStatus($id,$newstatus)
	{
		$withdraw			=	withdrawals::where('id',$id)->first();
		$currency			=	$withdraw->currency;
		$payment_mode		=	$withdraw->payment_mode;
		$total_amount		=	$withdraw->amount;
		$userid				=	$withdraw->user;
		$status				=	$withdraw->status;
		$pre_status			=	$withdraw->pre_status;

		$user				=	users::where('id',$withdraw->user)->first();
		$user_id 			= 	$user->id;
		$user_Uid 			= 	$user->u_id;
		if($newstatus == "Recover")
		{
			$newstatus = $pre_status;
			deposits::where('id',$id)->update(['status' => $newstatus,'pre_status' => $status]);
      
                    $event="User Withdrawal Recovered from Dummy";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  



			return redirect()->back()->with('successmsg', 'Status Updated to '.$newstatus.' Successfully.');
		}
		else if($newstatus == "Dummy1" || $newstatus == "Dummy0")
		{
			$flag_dummy	= substr($newstatus,5);
			withdrawals::where('id',$id)->update(['flag_dummy' => $flag_dummy,'pre_status' => $status]);

			if($newstatus == "Dummy1")
               {    $event="User Withdrawal move to Dummy state";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid ;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  
                 }

                 else if($newstatus == "Dummy0")
               {    $event="User Withdrawal remove from Dummy state";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid ;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  
                 }


			return redirect()->back()->with('successmsg', 'Updated to Dummy withdrawal Successfully.');
		}else{
			withdrawals::where('id',$id)->update(['status' => $newstatus,'pre_status' => $status]);

			      $event="User Withdrawal Cancelled";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  
			return redirect()->back()->with('successmsg', 'Status Updated to '.$newstatus.' Successfully.');
		}
	}
///Paid Deposited///
	public function paidwithdrawal($id)
	{
		withdrawals::where('id',$id)->update(['is_paid' => 1,]);
 
        return redirect()->back()->with('successmsg', 'Status Updated Successfully.');
		 
	}
	//Save withdrawal requests
	public function withdrawal(Request $request)
	{
	    
            if(!PinController::validate_pin($request->email_pin)){
                   return redirect()->back()
          ->with('errormsg', 'Invalid Email Pin ');
            }
		if(isset($request['withdraw_mode']))
		{
			$withdraw_mode = $request['withdraw_mode'];
		}else{
			$withdraw_mode = "";
		}
		
		if(isset($request['amount']))
		{
			$amount			= $request['amount'];
		}else{
			$amount			= 0;
		}
		if(isset($request['currency']))
		{
			$currency		= $request['currency'];
		}else{
			$currency		= "";
		}
		
		$user_id				=  Auth::user()->id;

		// Accounts Info
		$userAccInfo 			=  UserAccounts::where('user_id',$user_id)->first();
		
		// Currency Rates Query
		$ratesQuery 			=  currency_rates::orderBy('created_at', 'desc')->first();
		$settings				=  settings::getSettings();
		$activeDeposits 		=  deposits::where('user_id', Auth::user()->id)->where('status','Approved')->get();
		$totalActiveDeposits  	=  count($activeDeposits);
		
		if($withdraw_mode != "" && isset($userAccInfo) && isset($ratesQuery))
		{
			$reference_bonus	= $userAccInfo->reference_bonus;
			$reference_bonus2	= $userAccInfo->reference_bonus;
			//exit;
			if(isset($currency) )
			{
				$curr 					= strtolower($currency);
			
				$rateVal				= "rate_".$curr;
				$currencyRate			= $ratesQuery->$rateVal;

				$accProfit				= "profit_".$curr;
				$userProfit				= $userAccInfo->$accProfit;
				$userProfit2			= $userAccInfo->$accProfit;

				$accBalSold				= "sold_bal_".$curr;
				$userbalanceSold		= $userAccInfo->$accBalSold;
				$userbalanceSold2		= $userAccInfo->$accBalSold;

				$accBal				  	= "balance_".$curr;
				$userbalance			= $userAccInfo->$accBal;
				$userbalance2			= $userAccInfo->$accBal;
				//$accWaitingProfit		= "waiting_profit_".$curr;
				//$userwaitingProfit	= $userAccInfo->$accWaitingProfit;
				$totalUsd				= $amount * $currencyRate;
				
				// Set Withdrawal Deduct Fee
				if($totalUsd <= 500)
				{
					if($currency != "USD")
					{
						$withdrawal_fee	 = 5 / $currencyRate;	
					}else{
						$withdrawal_fee	 = 5;
					}
					$finalAmount  =  $amount - $withdrawal_fee;
				}else if($totalUsd > 500)
				{
					if($currency != "USD")
					{
						$withdrawal_fee	 = 15 / $currencyRate;	
					}else{
						$withdrawal_fee	 = 15;
					}
					$finalAmount  =  $amount - $withdrawal_fee;
				}
				if($currency == "USD" && $withdraw_mode == "Bonus")
				{ 
					if($totalUsd < $settings->withdraw_limit || $amount > $reference_bonus)
					{
						return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $'.$settings->withdraw_limit.'! Or You have insufficient balance for this request!');
					}else if($totalActiveDeposits==0)
					{
						return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Bonus Withdrawal Not Allowed ! You have no approved deposits in your deposits account.');
					}
				}
				else if($currency != "USD" && $withdraw_mode == "Bonus")
				{
					return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Invalid request! selected currency not allowed for reinvest Bonus');
				}
				else if($withdraw_mode == "Profit" && ($totalUsd < $settings->withdraw_limit || $amount > $userProfit))
				{
					return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $'.$settings->withdraw_limit.'! Or You have insufficient balance for this request!');
				}else if($withdraw_mode == "Sold" && ($totalUsd < $settings->withdraw_limit || $amount > $userbalanceSold))
				{
					return redirect()->intended('dashboard/withdrawals')->with('errormsg', 'Amount is less than $'.$settings->withdraw_limit.'! Or You have insufficient balance for this request!');
				}
			
				/* $lastwithdrawal = withdrawals::orderBy('created_at', 'desc')->first();
				if (! $lastwithdrawal )
				{	
					$unique_id = "W-1";
				}
				else 
				{
					// If we have Trade- in the database then we only want the number
					// So the substr returns this 0001
				//	$number 	= substr($lastwithdrawal->unique_id, 2);
				// $number 	= intval($number) + 1;
			 	    $number 	= intval($lastwithdrawal->id) + 1;
					
					$unique_id 	= "W-".$number;
				} */
				
				if(($withdraw_mode == "Bonus" || $withdraw_mode == "Profit" || $withdraw_mode == "Sold") && $totalUsd >= $settings->withdraw_limit)
				{
					if($withdraw_mode == "Bonus")
					{
						$balance = $reference_bonus;
					}else if($withdraw_mode == "Profit")
					{
						$balance = $userProfit;
					}else if($withdraw_mode == "Sold")
					{
						$balance = $userbalanceSold;
					}else{
						$balance = 0;
					}
					$wd	= new withdrawals();
					$wd->user			= $user_id;
					$user_uid		    = Auth::user()->u_id;
					$wd->amount			= $finalAmount;  // withdrawal Amount
					$wd->currency		= $currency;
					//$wd->unique_id		= $unique_id;
					$wd->payment_mode	= $withdraw_mode;
					$wd->status			= 'Pending';
					$wd->pre_status		= 'New';
					$wd->pre_amount		= $balance; // Previous Acc Balance
					$wd->withdrawal_fee	= $withdrawal_fee; // Previous Acc Balance
					if(isset($request['fund_type']))
					{
						$wd->new_type	= $request['fund_type'];
					}
					$wd->save();
						
					$last_withdrawal_id =  $wd->id;
					//$last_withdrawal_id =  "W-".$wd->id;
					$dateTime			=	date("Y-m-d H:i:s");
					if($withdraw_mode == "Bonus")
					{
						$reference_bonus = 	$reference_bonus - $amount;
						UserAccounts::where('user_id',$user_id)->update(['reference_bonus'=>$reference_bonus,]);

					$title 		= $withdraw_mode." Withdrawal";  
        			$details	= $user_id. " has withdraw ".$withdraw_mode." from bonus of ".$reference_bonus2;
        			$pre_amt 	=$reference_bonus2 ;
        			$curr_amt   = $reference_bonus;
        			$approvedby = "";
        			// Save Logs 
        			$this->saveLogs($title,$details,$user_id,$user_uid,$currency,$amount,$pre_amt,$curr_amt,$approvedby);
        			
					}else if($withdraw_mode == "Profit")
					{	
						$userProfit = $userProfit - $amount;
						UserAccounts::where('user_id',$user_id)->update([$accProfit=>$userProfit,]);


					$title 		= $withdraw_mode." Withdrawal";  
        			$details	= $user_id. " has withdraw ".$withdraw_mode." from Profit of ".$userProfit2;
        			$pre_amt 	=$userProfit2 ;
        			$curr_amt   = $userProfit;
        			$approvedby = "";
        			// Save Logs 
        			$this->saveLogs($title,$details,$user_id,$user_uid,$currency,$amount,$pre_amt,$curr_amt,$approvedby);
        			
							
					}else if($withdraw_mode == "Sold")
					{
						$userbalanceSold = 	$userbalanceSold - $amount;
						UserAccounts::where('user_id',$user_id)->update([$accBalSold =>$userbalanceSold,]);


					$title 		= $withdraw_mode." Withdrawal";  
        			$details	= $user_id. " has withdraw ".$withdraw_mode." from balanceSold of ".$userbalanceSold2;
        			$pre_amt 	=$userbalanceSold2 ;
        			$curr_amt   = $userbalanceSold;
        			$approvedby = "";
        			// Save Logs 
        			$this->saveLogs($title,$details,$user_id,$user_uid,$currency,$amount,$pre_amt,$curr_amt,$approvedby);
        			
					}
					
					if(Auth::user()->email != "")
					{
						$email_to 		= Auth::user()->email;	
						$userName		= Auth::user()->name;
						$from_Name		= "B4U Global";
						$from_email		= "noreply@b4uglobal.com";
						$subject		= "Your Widrawal Request is Successful";
						$message = 
								"<html>
									<body align=\"left\" style=\"height: 100%;\">
										<div>
											
											<div>
												<table style=\"width: 100%;\">
													<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															Dear ".$userName.",
														</td>
													</tr>
													<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															Your requested withdrawal is successful, to ".$from_Name.", your withdrawal id is W-".$last_withdrawal_id.".
															
														</td>
													</tr>
													<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															Your withdrawal amount is ".$amount."(".$currency."),after deduction of withdrawal fee you will recieve ".$finalAmount." (".$currency."). Note: (if withdrawal amount <= 500 than $5 fee will be charged. and if withdrawal amount greater than $500 than $15 fee will be charged.)
														</td>
													</tr>
													<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															You will get your withdraw amount, as soon as Admin approved your request.
														</td>
													</tr>
													
													<tr>
														<td style=\"text-align:left; padding:10px 0;\">
															Thanks for using  ".$from_Name.".
														</td>
													</tr>
													<tr>
														<td style=\"padding:10px 0; text-align:left;\">
															Your Sincerely,
														</td>
													</tr>
													<tr>
														<td style=\"padding:10px 0; text-align:left;\">
															Team ".$from_Name."
														</td>
													</tr>
													
												</table>
											</div>
										</div>
									</body>
								</html>"; 
						
						// Always set content-type when sending HTML email
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= 'From:B4U Global Withdrawals<'.$from_email.'>' . "\r\n";
							// More headers info
						//$headers .= 'Cc: myboss@example.com' . "\r\n";
								
						$success = @mail($email_to, $subject, $message , $headers);
					}
					$successmsg = 'Action Successful! Please wait for system to approve your withdrawal request.';
					return redirect()->back()->with('successmsg', $successmsg);

				}else if($withdraw_mode == "FundTransfer")
				{
					if(isset($request['fund_receivers_id']))
					{
						$fund_receivers_id	= $request['fund_receivers_id'];
					}
					if(isset($request['fund_type']))
					{
						$fund_type			= $request['fund_type'];
					}
					if($amount >= $settings->withdraw_limit)
					{
						$wd	=	new withdrawals();

						$wd->user			= $user_id;
						//$wd->amount= $accountSale;
						$wd->amount				= $amount;
						$wd->currency			= $currency;
						//$wd->unique_id			= $unique_id;
						$wd->payment_mode		= $withdraw_mode;
						$wd->status				= 'New';
						$wd->pre_status			= 'New';
						$wd->new_type			= $fund_type;
						$wd->fund_receivers_id	= $fund_receivers_id;
						//$wd->pre_status		= '';
						//$wd->flag_dummy		= 0;
						//$wd->paid_flag		= 0;
						$wd->save();

						return redirect()->back()->with('successmsg', 'Action Successful! Please verify through your email id and than wait for system to approve your FundTransfer request.');
					}else{
						return redirect()->back()->with('errormsg', 'Sorry, your balance is insufficient for this request. Minimum amount limit is $'.$settings->withdraw_limit.'.');
					}
				}
			} // end currenc if
		} // end mode if
	}// end of function

     //process withdrawals

	public function pwithdrawal($id)
	{
		//withdrawal info 
		$withdrawal			=	withdrawals::where('id',$id)->first();
		$amountWithdrawal 	= 	$withdrawal->amount;
		$currency 			= 	$withdrawal->currency;
		$dateWithdrawal 	= 	$withdrawal->created_at;
		$modeWithdrawal 	= 	$withdrawal->payment_mode;
		$paid_flag 			= 	$withdrawal->paid_flag;
		$crypto_amount 		= 	$withdrawal->crypto_amount;

		//$sale_deduct_amount	=	$withdrawal->sale_deduct_amount;
		$userWithdrawal		=	$withdrawal->user;
		$flag_dummy			=	$withdrawal->flag_dummy;
		$new_type			=	$withdrawal->new_type;
		$fund_receivers_id	=	$withdrawal->fund_receivers_id;
		// User info
		$user				=	users::where('id',$withdrawal->user)->first();
		$user_id 			= 	$user->id;
		$user_Uid 			= 	$user->u_id;

		
		if($paid_flag == 0)
		{

			if($modeWithdrawal == "Bonus")
			{
				//$pre_amount = floatval($referance_bonus) + floatval($amountWithdrawal);

				withdrawals::where('id',$id)->update(['status' => 'Approved','paid_flag'=>1,]);

				    $event="User Withdrawal Approved";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  

				return redirect()->back()->with('successmsg', 'Action Successful!');

			}else if($modeWithdrawal == "Profit")
			{
			
				//$pre_amount 	= floatval($profit_balance) + floatval($amountWithdrawal);

				withdrawals::where('id',$id)->update(['status' => 'Approved','paid_flag'=>1,]);

				 $event="User Withdrawal Approved";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  

				return redirect()->back()->with('successmsg','Action Successful!');

			}else if($modeWithdrawal == "Sold")
			{
		
				//$pre_amount 	= floatval($balance) + floatval($amountWithdrawal);

				withdrawals::where('id',$id)->update(['status' => 'Approved','paid_flag'=>1,]);

						 $event="User Withdrawal Approved";
                    $trade_id=$id;
                    $admin_id=Auth::user()->u_id;

                    
                    $user_id=$user_Uid;


                    $this->adminLogs($user_id,$admin_id,$trade_id,$event);  

				return redirect()->back()->with('successmsg','Action Successful!');

			}else if($modeWithdrawal == "FundTransfer")
			{

				if($fund_receivers_id != "" && $new_type != "")
				{
					withdrawals::where('id',$id)->update(['status' => 'Approved','paid_flag'=>1]);
					
					$fund_receivers_user	=	users::where('u_id',$fund_receivers_id)->first();
					$user_id2 				= 	$fund_receivers_user->id;
					$user_Uid2				= 	$fund_receivers_user->u_id;
					
					// Accounts Info
					$userAccInfo 			= UserAccounts::where('user_id',$user_id)->first();
					$referance_bonus		= $userAccInfo->referance_bonus;
					
					$userAccInfo2 			= UserAccounts::where('user_id',$user_id2)->first();
					$referance_bonus2		= $userAccInfo2->referance_bonus;
					
					if(isset($currency) && isset($userAccInfo) && isset($userAccInfo2))
					{
						$curr 					= strtolower($currency);
						$accBal					= "balance_".$curr;
						$userbalance			= $userAccInfo->$accBal;
						$accBalSold				= "sold_bal_".$curr;
						$userbalanceSold		= $userAccInfo->$accBalSold;
						$accProfit				= "profit_".$curr;
						$userProfit				= $userAccInfo->$accProfit;
						$accWaitingProfit		= "waiting_profit_".$curr;
						$userwaitingProfit		= $userAccInfo->$accWaitingProfit;
						
						$userbalance2			= $userAccInfo2->$accBal;
						$userbalanceSold2		= $userAccInfo2->$accBalSold;
						$userProfit2			= $userAccInfo2->$accProfit;
						$userwaitingProfit2		= $userAccInfo2->$accWaitingProfit;
						
					}
					if($new_type == "Bonus")
					{

						$referance_bonus 	= 	$referance_bonus - $amountWithdrawal;// remove from sender account

						$referance_bonus2 	= 	$referance_bonus2 + $amountWithdrawal;// Add to reciver account

						$dateTime			=	date("Y-m-d H:i:s");

						UserAccounts::where('user_id',$user_id)->update(['referance_bonus'=>$referance_bonus,]);
						
						UserAccounts::where('user_id',$user_id2)->update(['referance_bonus'=>$referance_bonus2,]);

						
						
						//users::where('id',$user_id)->update(['ref_bonus'=>$ref_bonus,'last_growth'=>$dateTime]);
						//users::where('id',$user_id2)->update(['ref_bonus'=>$ref_bonus2,'last_growth'=>$dateTime]);

					}else if($new_type == "Profit")
					{
						// remove from sender account
						$userProfit 	= 	$userProfit - $amountWithdrawal;
						UserAccounts::where('user_id',$user_id)->update([$accProfit=>$userProfit,]);
						// Add to reciver account
						$userProfit2 	= 	$userProfit2 + $amountWithdrawal;
						UserAccounts::where('user_id',$user_id2)->update([$accProfit=>$userProfit2,]);
						
						
					}else if($new_type == "Sold")
					{
						// remove from sender account
						$userbalanceSold 	= 	$userbalanceSold - $amountWithdrawal;
						UserAccounts::where('user_id',$user_id)->update([$accBalSold=>$userbalanceSold,]);
						// Add to reciver account
						$userbalanceSold2 	= 	$userbalanceSold2 + $amountWithdrawal;
						UserAccounts::where('user_id',$user_id2)->update([$accBalSold=>$userbalanceSold2,]);
						
					} 


				   

					return redirect()->back()->with('successmsg', 'Action Successful!');

				}else{
					return redirect()->back()->with('errormsg', 'Action not successful! Invalid Reciever or Fundtype.');
				}

			}

			//return redirect()->back()->with('errormsg', 'Action not successful! Amount is less than 25 dollers.');
			
		} 
		else{
			withdrawals::where('id',$id)->update(['status' => 'Approved']);
			return redirect()->back()->with('successmsg', 'Action successful!');
		}

    }
    
    
    
    public function viewWithdrawDetailsPost(Request $request)
	{

		$id 	= trim($request['id']);
		$type 	= $request['type'];
		
		$wid = intval($id);
		//echo $wid;
		//exit;
		$withdrawal = withdrawals::where('id',$wid)->first();
		
			$user_id				=	$withdrawal->user;

			$unique_id				=	"W-".$withdrawal->id;

			$amount					=	$withdrawal->amount;

			$pre_status				=	$withdrawal->pre_status;

			$status					=	$withdrawal->status;

			$payment_mode			=	$withdrawal->payment_mode;

			$pre_amount				=	$withdrawal->pre_amount;

			$new_amount				=	$withdrawal->new_amount;

			$withdrawal_fee			=	$withdrawal->withdrawal_fee;

			$new_type				=	$withdrawal->new_type;

			$fund_receivers_id		=	$withdrawal->fund_receivers_id;

			$paid_flag				=	$withdrawal->paid_flag;

			$flag_dummy				=	$withdrawal->flag_dummy;

			$created_at				=	$withdrawal->created_at;

			$updated_at				=	$withdrawal->updated_at;

			

			$userinfo				= users::where('id',$user_id)->first();
		//print_r($userinfo);
		//exit;
			$userUID				= $userinfo->u_id;
			$totalAmount 			= $amount + $withdrawal_fee;

            $USERTYPE       =Auth::user()->type;
            $USERID      =Auth::user()->id;

            if(($USERTYPE == 0 && $USERID == $user_id ) || $USERTYPE == 1 || $USERTYPE == 2 )
        {

			$details =  '<div class="modal-header">

			        <button type="button" class="close" data-dismiss="modal">&times;</button>

			        <h4 class="modal-title" style="text-align:center;">Withdraw Details</h4>

			      </div>

			      <div class="modal-body">

					<table width="90%" align="center" style="padding-bottom: 15px !important;">

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Withdrawal Info: </h4></td></tr>

						<tr><td width="25%"> <strong>Wuthdraw ID : </strong></td><td width="25%">'.$unique_id.' </td><td width="25%"> <strong>User ID : </strong></td><td width="25%">'.$userUID.'</td> </tr>

						<tr><td width="25%"><strong>Current Status : </strong></td><td width="25%">'.$status.'</td><td width="25%">	<strong>Previous Status : </strong></td><td width="25%">'.$pre_status.' </td></tr>

						<tr><td width="25%"><strong>Payment Mode : </strong></td><td width="25%">'.$payment_mode.'</td></tr>';

			/* if($payment_mode == "FundTransfer") {

				$details .=	'<tr><td width="25%"><strong>Fund Type : </strong></td><td>'.$new_type.'</td><td width="25%"><strong>Fund Reciever ID : </strong></td><td width="25%">'.$fund_receivers_id.'</td width="25%"></tr>';	

			} */

			$details .=	'<tr><td colspan="4"></td></tr>

						<tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Amount Info: </h4></td></tr>

						<tr><td colspan="2"><strong>Total Amount: </strong></td><td colspan="2">$'.$totalAmount.' </td></tr>
						<tr><td colspan="2"><strong>User Recieved Amount: </strong></td><td colspan="2">'.$amount.'</td></tr>
						<tr><td colspan="2"> <strong>Deduction Fee: </strong></td><td colspan="2">'.$withdrawal_fee.'</td></tr>

						<tr><td colspan="4"></td></tr>';

			
			$details .='</table></div>';

			echo $details;
		}
		else
		{
			echo "Invalid User!";
		}
		//exit;
	}
	
	///////////////END viewWithdrawDetailsPost ///////////////

}



