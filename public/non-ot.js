$(function () {
    $(document).on('click', '.o-s', function (e) {
        e.preventDefault();
        $(this).closest('form').submit();
        $(this).html('processing...').attr('disabled', true);
    });
})