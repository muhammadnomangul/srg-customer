/**
 * @author Suleman Khan <sulaman@sulaman.pk>
 * Purpose: Handling File Management system to AWS
 * */


$(function () {
    let validImageTypes = ['image/gif', 'image/jpeg', 'image/png', 'image/jpg'];
    let validCSVTypes = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv', 'application/vnd.ms-excel'];

    let FileUploadObject = null;
    let FileUploadedAWSS3URL = null;
    let loaderHtml = "<span class='loaderSpan' style='color:red'><i class='fa fa-spinner fa-spin'></i> Processing,, Please wait</span>";
    let inputFileUrl = $('input[name=fileurl]');


    // get singed url
    function getSingedUrl(filename) {
        // disable form
        disableEnableForm(true);

        if (!filename) {
            return alert('File not selected');
        }
        fetch('/dashboard/signed-url-upload?filename=' + filename).then(res => res.json()).then(res => {
            if (res.status) {
                uploadFileToAWS(res.result);
            } else {
                return alert('Upload URI failed');
            }
        }).catch(() => {
            disableEnableForm(false)
        })
    }

    // Upload file to aws
    function uploadFileToAWS(awsResultObj) {

        let formData = new FormData();
        formData.append('Key', awsResultObj.formInputs.key);
        formData.append('acl', awsResultObj.formInputs.acl);
        formData.append('X-Amz-Algorithm', awsResultObj.formInputs.X_Amz_Algorithm);
        formData.append('Policy', awsResultObj.formInputs.Policy);
        formData.append('X-Amz-Credential', awsResultObj.formInputs.X_Amz_Credential);
        formData.append('X-Amz-Date', awsResultObj.formInputs.X_Amz_Date);
        formData.append('X-Amz-Signature', awsResultObj.formInputs.X_Amz_Signature);
        formData.append('file', FileUploadObject)

        fetch(awsResultObj.formAttributes.action, {
            method: 'POST',
            body: formData,
        }).then(res => {
            if (res.status === 204) {
                getObjectUploadURL(awsResultObj.formInputs.key);
            }
        }).catch(() => {
            disableEnableForm(false)
        });
    }

    // fetch Object uploaded url
    function getObjectUploadURL(ObjectKey) {
        if (!ObjectKey) {
            return alert('Object key is missing');
        }

        fetch('/dashboard/get-object-url/' + ObjectKey).then(res => res.json()).then(res => {

            if (res.status) {
                FileUploadedAWSS3URL = res.result.url;
                setS3UploadObjectUrlInInputTag(FileUploadedAWSS3URL);
            } else {
                return alert('Failed, To fetch Object URL');
            }
        }).catch(() => {
            disableEnableForm(false)
        })
    }

    // set upload image url in the input and send url to server.
    function setS3UploadObjectUrlInInputTag() {
        if ($(inputFileUrl).length) {
            $(inputFileUrl).val(FileUploadedAWSS3URL);
        } else {
            alert('Input with name fileurl is missing');
        }

        return disableEnableForm(false);
    }

    // disable or enable form
    function disableEnableForm(disable = true) {
        $('input, select, textarea, button').prop('disabled', disable);
        if (disable) {
            $(inputFileUrl).parent('div').append(loaderHtml);
        } else {
            $('span.loaderSpan').remove();
        }

    }

    function validateFileType(inputAcceptance) {
        let validateInputFrom = [];
        switch (inputAcceptance) {
            case '_cv':
                validateInputFrom = validCSVTypes;
                break;
            case '_img':
            default:
                validateInputFrom = validImageTypes;
                break;
        }

        return validateInputFrom;
    }

    $(document).on('change', '.file-upload-aws', function () {

        FileUploadObject = $(this)[0].files[0];

        // in case of multiple files.
        if ($(this).attr('data-attr') === 'multiple') {
            let inputName = $(this).attr('data-name');
            inputFileUrl = $('input[name=' + inputName + '_fileurl');
        }

        // check file extension.
        if ($.inArray(FileUploadObject['type'], validateFileType($(this).attr('data-info'))) < 0) {
            alert("Only Images allowed with these extension, gif,jpeg,png,jpg ");
            // reset input file.
            $(this).val('');
            return false;
        }

        getSingedUrl(FileUploadObject.name);
    });

});