// Load Creditsh
function loadCreditRecords(creditURI) {
    $('#creditTable').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": false,
        "ajax": {
            "url": creditURI,
            "type": "POST",
            data: {
                ac: accId
            }
        },
        "deferRender": true,
        "columns": [
            {
                "data": "id", "title": "Sr.#", "searchable": true,
                "render": function (data, type, full, meta) {
                    return 'CB-CR-' + data;
                }
            },
            // {
            //     "data": "cb_account_id", "title": "Account Name#", "searchable": true,
            //     "render": function (data, type, row, meta) {
            //         return currentAccName;
            //     }
            // },
            {
                "data": "amount", "title": "Amount", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    return parseFloat(data).toFixed(row.curr_precision) + ' (' + currentAccCurrency + ')';
                }
            },
            {
                "data": "fee", "title": "Deduction Fee", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    return parseFloat(data).toFixed(row.curr_precision) + ' (' + currentAccCurrency + ')';
                }

            },
            {
                "data": "status", "title": "Status", "searchable": true,
                'render': function (data, type, row) {
                    return formatStatusResponse(data);
                }
            },
            {
                "data": "type", "title": "Type", "searchable": true,
                'render': function (data, type, row) {
                    return formatTypeResponse(data);
                }
            },
            {
                "data": 'reason', "title": "Reason", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    let reason = data;
                    if (row.u_id) {
                        reason += '  <b>' + row.u_id + '(' + row.name + ')</b>';
                    }
                    return reason;
                }
            },
            {
                "data": "created_at", "title": "Transaction At", "orderable": true, "searchable": false,
                'render': function (data, type, row) {
                    return getDateInString(data);
                }
            },
        {
            "data": "id", "title": "Action", "searchable": false,
            'render': function (data, type,row) {
                if(row.type === 'transfer' && (row.status === 'cancelled' || row.status === 'approved') && row.is_rating === 0){
                    return  `<a class="label label-success" style="color: whitesmoke"  href="#"  data-toggle="modal" data-target="#cbRatingModal" onclick="cbRatingToUser(${row.id},'cr');" >Give Rating to User</a>`;
                }
                else {
                    return  "";
                }
            }
        }
        ],
        "order": [6, "desc"]
    });
}
function formatStatusResponse(data) {
    let responseResult = data;
    switch (data) {
        case 'approved':
            responseResult = '<span class="label label-success" style="background-color: #9e2187">Approved</span>';
            break;
        case 'pending':
            responseResult = '<span class="label label-info">Pending</span>';
            break;
        case 'failed':
            responseResult = '<span class="label label-warning">Failed</span>';
            break;
        case 'rejected':
            responseResult = '<span class="label label-danger">Rejected</span>';
            break;
        case 'cancelled':
            responseResult = '<span class="label label-danger">Cancelled</span>';
            break;
        case 'inprocessing':
            responseResult = '<span class="label label-warning">In Processing</span>';
            break;
    }
    return responseResult;
}
function formatTypeResponse(data) {
    let responseResult = data;
    switch (data) {
        case 'topdown':
            responseResult = '<span class="label label-success" style="background-color: #9e2187">Cash Out</span>';
            break;
        case 'reward':
            responseResult = '<span class="label label-success" style="background-color: #e3c922">Reward</span>';
            break;
        case 'transfer':
            responseResult = '<span class="label label-info">Transfer</span>';
            break;
        case 'profit':
            responseResult = '<span class="label label-success">Profit</span>';
            break;
        case 'bonous':
        case 'bonus':
            responseResult = '<span class="label label-primary">Bonus</span>';
            break;
        case 'sold':
            responseResult = '<span class="label label-danger" style="background-color: #0ba7e7">Sold</span>';
            break;
        case 'topup':
            responseResult = '<span class="label label-warning" style="background-color: #1b2ae7">Top Up</span>';
            break;
        case 'adjustment':
            responseResult = '<span class="label label-warning">Adjustment</span>';
            break;
        case 'smsfee':
            responseResult = '<span class="label label-warning" style="background-color: #e71b62">SMS Fee</span>';
            break;
        case 'newdeposit':
            responseResult = '<span class="label label-warning" style="background-color: #3f9911">New Deposit</span>';
            break;
    }
    return responseResult;
}
// Load Debits
function loadDebitRecords(debitURI) {
    $('#debitTable').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": false,
        "ajax": {
            "url": debitURI,
            "type": "POST",
            data: {
                ac: accId
            }
        },
        "deferRender": true,
        "columns": [
            {
                "data": "id", "title": "Sr.#", "searchable": true,
                "render": function (data, type, full, meta) {
                    return 'CB-DB-' + data
                }
            },
            {
                "data": "amount", "title": "Amount", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    htmL = parseFloat(data).toFixed(row.curr_precision) + ' (' + currentAccCurrency + ')';
                    if (row.type === 'transfer' && row.status === 'pending'){
                        htmL += '<br><a class="label label-warning" style="color: whitesmoke"  href="#" onclick="approveCbTransfer('  + row.id + ');" >Cash Received</a>';
                    }
                    return htmL;
                }
            },
            {
                "data": "fee", "title": "Deduction Fee", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    return parseFloat(data).toFixed(row.curr_precision) + ' (' + currentAccCurrency + ')';
                }
            },
            {
                "data": "status", "title": "Status", "searchable": true,
                'render': function (data, type, row) {
                    return formatStatusResponse(data);
                }
            },
            {
                "data": "type", "title": "Type", "searchable": true,
                'render': function (data, type, row) {
                    return formatTypeResponse(data);
                }
            },
            {
                "data": 'reason', "title": "Reason", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    let reason = data;
                    if (row.u_id) {
                        reason += '  <b>' + row.u_id + '(' + row.name + ')</b>';
                    }

                    return reason;
                }
            },
            {
                "data": "created_at", "title": "Transaction At", "orderable": true, "searchable": false,
                'render': function (data, type, row) {
                    return getDateInString(data);
                }
            },
            {
                "data": "id", "title": "Action", "searchable": false,
                'render': function (data, type, row) {
                    if (row.type === 'transfer' && (row.status === 'cancelled' || row.status === 'approved') && row.is_rating === 0) {
                        return `<a class="label label-success" style="color: whitesmoke"  href="#"  data-toggle="modal" data-target="#cbRatingModal" onclick="cbRatingToUser(${row.id},'db');" >Give Rating to User</a>`;
                    } else {
                        return "";
                    }
                }
            }
        ],
        "order": [6, "desc"]
    });
}
function approveCbTransfer(id) {
    var conf = confirm("Do you really want to proceed?");
    if (conf) {
        jQuery.ajax({
            type: "GET",
            url: approveCbTransferURL + '/' + id,
            data: {"_token": "{{ csrf_token() }}"},
            success: function (response) {
                console.log(response);
                alert("Success");
                window.location.reload();
            }, error: function (response) {
                console.log(response);
                alert("Something went wrong. Please try again");
            }
        });
    } else
        return false;
}
function cbRatingToUser(id,type){
    $('input.cbRatingUser').val(id);
    $('input.transType').val(type);
}
// show hide fund transfer
function showHideAddNewBenficitaryOption(show) {
    let inputBox = $('#fundreciever2');
    if (show) {
        $(inputBox).show();
    } else {
        $(inputBox).hide();
    }
}

// Search Benficary
function searchBaneficiary() {
    let fundReceiver = $("#fundreciever_id2").val();

    if (fundReceiver) {
        $.ajax({
            type: "POST",
            url: searchBenficiary,
            data: {
                benficiaryid: fundReceiver,
                "_token": csrfToken
            },
            success: function (response) {
                showInformationOfBeneficiary();
                $("#ben_id").html(response);
            },
            error: function (response) {
                if (response.status === 422) {
                    showInformationOfBeneficiary();
                    $("#ben_id").html('<strong style="color:red;">Beneficiary id not valid. Please add correct Beneficiary !(Note: You cannot enter your own User_id) </strong>')
                }
            }
        });
    }

}

function validationFailsImpact(input, errorMsg = 'Please Fill out required fields') {
    $(input).addClass('required')
    $('.errors').removeClass('hide').text(errorMsg);
}

function removeValidationErrorAlert() {
    $('.errors').addClass('hide');
}

function isAllowedToCashOut() {
    let amount = $('.cashoutamount').val();
    let currency = $('select.cashboxcbaccount :selected').val();
    if (amount && currency) {
        jQuery.ajax({
            type: "get",
            url: isAllowedTopDownURL,
            data: {
                'cb_account': currency,
                'amount': amount,
                "_token": $('meta[name=csrf-token]').attr('content')
            },
            success: function (response) {
                $('div.responseCashOut').html(response);
                if ($('.shouldtransfer').hasClass('isyes')) {
                    $('.cashoutbtn').prop('disabled', false);
                } else {
                    $('.cashoutbtn').prop('disabled', true);
                }
            }, error: function (response) {
            }
        });
    }
}


function validateFundTransfer(closestForm) {

    let validationFail = false;
    let amountInput = $(closestForm).find(amountEntered);
    let enteredAmountVal = parseFloat($(amountInput).val());
    let availBal = $(closestForm).find('.currencyValCB option:selected').attr('data-ab')
    let isBenificaryInputAvailable = $(closestForm).find(addNewBenificiaryInput).length;
    $(closestForm).find('input[required=required] , select[required=required]').each(function (key, input) {
        if (!$(input).val()) {
            validationFailsImpact($(input))
            validationFail = true;
        } else {
            $(input).removeClass('required')
        }
    })

    if (isBenificaryInputAvailable && typeof addNewBenificiaryInput !== 'undefined') {
        if ($(addNewBenificiaryInput).is(':visible')) {

            if (!$(addNewBenificiaryInput).val()) {
                validationFail = true;
                validationFailsImpact($(addNewBenificiaryInput));
            }

            // Check is benificary is valid or not..
            if ($('.has-success-in-beni').length === 0) {
                validationFail = true;
                validationFailsImpact($(addNewBenificiaryInput), 'Invalid Beneficiary Details');
            }
        }
    }

    if (enteredAmountVal <= 0 || availBal < enteredAmountVal) {
        validationFail = true;
        validationFailsImpact(amountInput, enteredAmountVal === 0 ? 'Transfer Amount should be greater than 0' : 'Transfer Amount less than or equal to Available balance');
    }

    return validationFail;
}

function showInformationOfBeneficiary() {
    let userRatingID = $("#fund_receiver_id").val();
    if (userRatingID === 'notexist'){
        $("#benId").html('');
        userRatingID = $("#fundreciever_id2").val();
    }
   if(userRatingID){
      jQuery.ajax({
        type : "get",
        url : userRatingInfoURL,
        data :  {
            'b4uid' : userRatingID,
            "_token": "{{ csrf_token()}}"
        },
        success: function (response){
            $("#benId").html(response);
        },error : function (response){
            console.log(response);
            alert("Something went wrong.");
          }
      });
   }
}

$(function () {

    $("#withdrawalModal").on('shown.bs.modal', function () {
        $('#formABC')[0].reset();
    });

    $("#topUpModal").on('shown.bs.modal', function () {
        $('#formABCTopUp')[0].reset();
    });

    // on Cash box account change.
    $(document).on('change', 'select[name=fund_receivers_id]', function () {
        let selectedValue = $(this).val();
        if (selectedValue === 'notexist') {
            showInformationOfBeneficiary();
            showHideAddNewBenficitaryOption(true);
        } else {
            showInformationOfBeneficiary();
            showHideAddNewBenficitaryOption(false)
        }
    });
    $(document).on('click', '.t-s', function () {
        let validationFail = validateFundTransfer($(this).closest('form'));
        if (validationFail) {
        } else {
            removeValidationErrorAlert();
            $(this).removeClass('t-s').addClass('o-s').trigger('click');
        }
    });

    $($('.t-s').closest('form')).on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $(document).on('keyup', '.cashoutamount', function () {
        isAllowedToCashOut();
    });

    $(document).on('change', '.cashboxcbaccount', function () {
        $('.cashoutamount').val(0);
        isAllowedToCashOut();
    })


    document.getElementById("fundreciever_id2").addEventListener("focusout", searchBaneficiary);


});
