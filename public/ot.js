/**
 * @author Suleman Khan <sulaman@sulaman.pk>
 * Purpose: OTP Model to send Otp to users.
 * */


$(function () {

    let verificationCodeBlock = $('div.verificationBlock');
    let verificationCodeInputBlock = $('div.verificationInputBlock input.inputs');
    let showOtpBlockBtn = $('.o-s');
    let closestForm = null;
    let otpMode = null;
    let formHTMl = null;
    let enteredOtp = null;

    /**
     * @Purpose :: Auto focusing inputs when user tries to type OTP
     * */
    function focusAutoInputs() {
        $(document).on('keyup', '.inputs', async function () {
            if (parseInt(this.value.length) === parseInt(this.maxLength)) {
                $(this).next('input').focus();
            }
            await isValidOtpEntered();
        });
    }

    focusAutoInputs();

    function enableFormSubmitButton(disable = false) {
        $(closestForm).find('button.proceed-2f').prop('disabled', disable);
    }

    /**
     * @Purpose :: Verify, Is valid otp entered or not.
     * */
    async function isValidOtpEntered() {
        getOTP(true).then(res => {
            res === 6 ? enableFormSubmitButton(false) : enableFormSubmitButton(true);
        });
    }


    /**
     * @Purpose :: Get One time password.
     * */
    async function getOTP(returnLength = false) {
        let enteredOtpInputBlock = $(closestForm).find('input[name=etp]');
        await $('input.inputs').each((index, values) => {
            if (index === 0) {
                enteredOtp = $(values).val();
            } else {
                enteredOtp += $(values).val();
            }
        });

        $(enteredOtpInputBlock).val(enteredOtp);

        if (returnLength) {
            return $(enteredOtpInputBlock).val().length;
        } else {
            return $(enteredOtpInputBlock).val();
        }

    }

    /**
     * @Purpose :: Send OTP button enable and disable.
     * */
    function sendOtpButtonDisableEnable(disable = true) {
        let ButtonText = 'Proceed';
        // disable following form elements.
        $('input, select, textarea, button').prop('disabled', disable);
        // empty enter otp input values.
        $(closestForm).find('input.inputs').val('');

        if (disable) {
            ButtonText = "<span class='loaderSpan' style='color:white'><i class='fa fa-spinner fa-spin'></i> Processing,, Please wait</span>";
            $(showOtpBlockBtn).html(ButtonText);
        } else {
            $(showOtpBlockBtn).html(ButtonText);
        }
    }


    /**
     * @Purpose Validate Otp type
     * */
    function validateSendOtp(mod) {
        // return alphaBetsRegex.test(mod);
    }

    /**
     * @Purpose :: Send OTP to User
     * */
    function sendOtp(otpMode, isResend = false) {

        // validate mode
        if (!otpMode) {
            return false;
        }

        // disable button
        sendOtpButtonDisableEnable(true);
        // send otp
        fetch('send-otp/' + otpMode).then((res) => {
            return res.json();
        }).then((res) => {
        }).finally(() => {

            // enable button
            sendOtpButtonDisableEnable(false);
            // disable proceed button
            enableFormSubmitButton(true);

            if (!isResend) {
                // show one time password form.
                showOneTimePasswordForm();
            }

        }).catch(() => {
        });
    }

    /**
     * @Purpose :: Show one time password form.
     * */
    function showOneTimePasswordForm() {
        $(closestForm).find('input').closest('label').parent('div').addClass('hide');
        // add otp input inside form
        $(closestForm).append('<input name="etp" class="form-control hide" /><input name="mod" class="hide" value="' + otpMode + '">');
        $(closestForm).find('div.form-content').addClass('hide');

        // show one time password input.
        $(closestForm).append($(verificationCodeBlock).html());
        $(closestForm).find('input.inputs-un').addClass('inputs').removeClass('inputs-un').attr('required', 'required');
        // disable form till now all inputs are filled or not.
        $(closestForm).find('button.proceed-2f').prop('disabled', true);
    }


    /**
     * @Purpose :: Submit real form
     * */
    function submitActualForm() {
        $(closestForm).find('button').attr('disabled', true);
        $(closestForm).submit();
    }


    /**
     * @Purpose :: Send OTP
     * */
    $(document).on('click', '.o-s', function (e) {
        e.preventDefault();
        // file all forms
        closestForm = $(this).closest('form');
        otpMode = $(closestForm).attr('mod');
        sendOtp(otpMode);
    });

    /**
     * @Purpose :: Resend Send OTP
     * */
    $(document).on('click', '.resend-2f', function () {
        sendOtp(otpMode, true);
    })

    /**
     * @Purpose :: Proceed Two FA authentication
     * */
    $(document).on('click', '.proceed-2f', function () {
        submitActualForm();
    })

});