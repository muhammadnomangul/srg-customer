const mix = require('laravel-mix');

mix.setPublicPath('public/css');
if (mix.inProduction()) {
    mix.version();
}

mix.sass('resources/assets/sass/app.scss', 'public/css');
