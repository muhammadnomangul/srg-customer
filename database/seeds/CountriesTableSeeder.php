<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'country_code' => 'AF',
                'country_name' => 'Afghanistan',
                'dialCode' => 93,
            ),
            1 => 
            array (
                'id' => 2,
                'country_code' => 'AL',
                'country_name' => 'Albania',
                'dialCode' => 355,
            ),
            2 => 
            array (
                'id' => 3,
                'country_code' => 'DZ',
                'country_name' => 'Algeria',
                'dialCode' => 213,
            ),
            3 => 
            array (
                'id' => 4,
                'country_code' => 'DS',
                'country_name' => 'American Samoa',
                'dialCode' => 1684,
            ),
            4 => 
            array (
                'id' => 5,
                'country_code' => 'AD',
                'country_name' => 'Andorra',
                'dialCode' => 376,
            ),
            5 => 
            array (
                'id' => 6,
                'country_code' => 'AO',
                'country_name' => 'Angola',
                'dialCode' => 244,
            ),
            6 => 
            array (
                'id' => 7,
                'country_code' => 'AI',
                'country_name' => 'Anguilla',
                'dialCode' => 1264,
            ),
            7 => 
            array (
                'id' => 8,
                'country_code' => 'AQ',
                'country_name' => 'Antarctica',
                'dialCode' => 672,
            ),
            8 => 
            array (
                'id' => 9,
                'country_code' => 'AG',
                'country_name' => 'Antigua and Barbuda',
                'dialCode' => 1268,
            ),
            9 => 
            array (
                'id' => 10,
                'country_code' => 'AR',
                'country_name' => 'Argentina',
                'dialCode' => 54,
            ),
            10 => 
            array (
                'id' => 11,
                'country_code' => 'AM',
                'country_name' => 'Armenia',
                'dialCode' => 374,
            ),
            11 => 
            array (
                'id' => 12,
                'country_code' => 'AW',
                'country_name' => 'Aruba',
                'dialCode' => 297,
            ),
            12 => 
            array (
                'id' => 13,
                'country_code' => 'AU',
                'country_name' => 'Australia',
                'dialCode' => 61,
            ),
            13 => 
            array (
                'id' => 14,
                'country_code' => 'AT',
                'country_name' => 'Austria',
                'dialCode' => 43,
            ),
            14 => 
            array (
                'id' => 15,
                'country_code' => 'AZ',
                'country_name' => 'Azerbaijan',
                'dialCode' => 994,
            ),
            15 => 
            array (
                'id' => 16,
                'country_code' => 'BS',
                'country_name' => 'Bahamas',
                'dialCode' => 1242,
            ),
            16 => 
            array (
                'id' => 17,
                'country_code' => 'BH',
                'country_name' => 'Bahrain',
                'dialCode' => 973,
            ),
            17 => 
            array (
                'id' => 18,
                'country_code' => 'BD',
                'country_name' => 'Bangladesh',
                'dialCode' => 880,
            ),
            18 => 
            array (
                'id' => 19,
                'country_code' => 'BB',
                'country_name' => 'Barbados',
                'dialCode' => 1246,
            ),
            19 => 
            array (
                'id' => 20,
                'country_code' => 'BY',
                'country_name' => 'Belarus',
                'dialCode' => 375,
            ),
            20 => 
            array (
                'id' => 21,
                'country_code' => 'BE',
                'country_name' => 'Belgium',
                'dialCode' => 32,
            ),
            21 => 
            array (
                'id' => 22,
                'country_code' => 'BZ',
                'country_name' => 'Belize',
                'dialCode' => 501,
            ),
            22 => 
            array (
                'id' => 23,
                'country_code' => 'BJ',
                'country_name' => 'Benin',
                'dialCode' => 229,
            ),
            23 => 
            array (
                'id' => 24,
                'country_code' => 'BM',
                'country_name' => 'Bermuda',
                'dialCode' => 1441,
            ),
            24 => 
            array (
                'id' => 25,
                'country_code' => 'BT',
                'country_name' => 'Bhutan',
                'dialCode' => 975,
            ),
            25 => 
            array (
                'id' => 26,
                'country_code' => 'BO',
                'country_name' => 'Bolivia',
                'dialCode' => 591,
            ),
            26 => 
            array (
                'id' => 27,
                'country_code' => 'BA',
                'country_name' => 'Bosnia and Herzegovina',
                'dialCode' => 387,
            ),
            27 => 
            array (
                'id' => 28,
                'country_code' => 'BW',
                'country_name' => 'Botswana',
                'dialCode' => 267,
            ),
            28 => 
            array (
                'id' => 29,
                'country_code' => 'BV',
                'country_name' => 'Bouvet Island',
                'dialCode' => 0,
            ),
            29 => 
            array (
                'id' => 30,
                'country_code' => 'BR',
                'country_name' => 'Brazil',
                'dialCode' => 55,
            ),
            30 => 
            array (
                'id' => 31,
                'country_code' => 'IO',
                'country_name' => 'British Indian Ocean Territory',
                'dialCode' => 0,
            ),
            31 => 
            array (
                'id' => 32,
                'country_code' => 'BN',
                'country_name' => 'Brunei Darussalam',
                'dialCode' => 673,
            ),
            32 => 
            array (
                'id' => 33,
                'country_code' => 'BG',
                'country_name' => 'Bulgaria',
                'dialCode' => 359,
            ),
            33 => 
            array (
                'id' => 34,
                'country_code' => 'BF',
                'country_name' => 'Burkina Faso',
                'dialCode' => 226,
            ),
            34 => 
            array (
                'id' => 35,
                'country_code' => 'BI',
                'country_name' => 'Burundi',
                'dialCode' => 257,
            ),
            35 => 
            array (
                'id' => 36,
                'country_code' => 'KH',
                'country_name' => 'Cambodia',
                'dialCode' => 855,
            ),
            36 => 
            array (
                'id' => 37,
                'country_code' => 'CM',
                'country_name' => 'Cameroon',
                'dialCode' => 237,
            ),
            37 => 
            array (
                'id' => 38,
                'country_code' => 'CA',
                'country_name' => 'Canada',
                'dialCode' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'country_code' => 'CV',
                'country_name' => 'Cape Verde',
                'dialCode' => 238,
            ),
            39 => 
            array (
                'id' => 40,
                'country_code' => 'KY',
                'country_name' => 'Cayman Islands',
                'dialCode' => 1345,
            ),
            40 => 
            array (
                'id' => 41,
                'country_code' => 'CF',
                'country_name' => 'Central African Republic',
                'dialCode' => 236,
            ),
            41 => 
            array (
                'id' => 42,
                'country_code' => 'TD',
                'country_name' => 'Chad',
                'dialCode' => 235,
            ),
            42 => 
            array (
                'id' => 43,
                'country_code' => 'CL',
                'country_name' => 'Chile',
                'dialCode' => 56,
            ),
            43 => 
            array (
                'id' => 44,
                'country_code' => 'CN',
                'country_name' => 'China',
                'dialCode' => 86,
            ),
            44 => 
            array (
                'id' => 45,
                'country_code' => 'CX',
                'country_name' => 'Christmas Island',
                'dialCode' => 61,
            ),
            45 => 
            array (
                'id' => 46,
                'country_code' => 'CC',
            'country_name' => 'Cocos (Keeling) Islands',
                'dialCode' => 61,
            ),
            46 => 
            array (
                'id' => 47,
                'country_code' => 'CO',
                'country_name' => 'Colombia',
                'dialCode' => 57,
            ),
            47 => 
            array (
                'id' => 48,
                'country_code' => 'KM',
                'country_name' => 'Comoros',
                'dialCode' => 269,
            ),
            48 => 
            array (
                'id' => 49,
                'country_code' => 'CG',
                'country_name' => 'Congo',
                'dialCode' => 242,
            ),
            49 => 
            array (
                'id' => 50,
                'country_code' => 'CK',
                'country_name' => 'Cook Islands',
                'dialCode' => 682,
            ),
            50 => 
            array (
                'id' => 51,
                'country_code' => 'CR',
                'country_name' => 'Costa Rica',
                'dialCode' => 506,
            ),
            51 => 
            array (
                'id' => 52,
                'country_code' => 'HR',
            'country_name' => 'Croatia (Hrvatska)',
                'dialCode' => 385,
            ),
            52 => 
            array (
                'id' => 53,
                'country_code' => 'CU',
                'country_name' => 'Cuba',
                'dialCode' => 53,
            ),
            53 => 
            array (
                'id' => 54,
                'country_code' => 'CY',
                'country_name' => 'Cyprus',
                'dialCode' => 357,
            ),
            54 => 
            array (
                'id' => 55,
                'country_code' => 'CZ',
                'country_name' => 'Czech Republic',
                'dialCode' => 420,
            ),
            55 => 
            array (
                'id' => 56,
                'country_code' => 'DK',
                'country_name' => 'Denmark',
                'dialCode' => 45,
            ),
            56 => 
            array (
                'id' => 57,
                'country_code' => 'DJ',
                'country_name' => 'Djibouti',
                'dialCode' => 253,
            ),
            57 => 
            array (
                'id' => 58,
                'country_code' => 'DM',
                'country_name' => 'Dominica',
                'dialCode' => 1767,
            ),
            58 => 
            array (
                'id' => 59,
                'country_code' => 'DO',
                'country_name' => 'Dominican Republic',
                'dialCode' => 1809,
            ),
            59 => 
            array (
                'id' => 60,
                'country_code' => 'TP',
                'country_name' => 'East Timor',
                'dialCode' => 0,
            ),
            60 => 
            array (
                'id' => 61,
                'country_code' => 'EC',
                'country_name' => 'Ecuador',
                'dialCode' => 593,
            ),
            61 => 
            array (
                'id' => 62,
                'country_code' => 'EG',
                'country_name' => 'Egypt',
                'dialCode' => 20,
            ),
            62 => 
            array (
                'id' => 63,
                'country_code' => 'SV',
                'country_name' => 'El Salvador',
                'dialCode' => 503,
            ),
            63 => 
            array (
                'id' => 64,
                'country_code' => 'GQ',
                'country_name' => 'Equatorial Guinea',
                'dialCode' => 240,
            ),
            64 => 
            array (
                'id' => 65,
                'country_code' => 'ER',
                'country_name' => 'Eritrea',
                'dialCode' => 291,
            ),
            65 => 
            array (
                'id' => 66,
                'country_code' => 'EE',
                'country_name' => 'Estonia',
                'dialCode' => 372,
            ),
            66 => 
            array (
                'id' => 67,
                'country_code' => 'ET',
                'country_name' => 'Ethiopia',
                'dialCode' => 251,
            ),
            67 => 
            array (
                'id' => 68,
                'country_code' => 'FK',
            'country_name' => 'Falkland Islands (Malvinas)',
                'dialCode' => 500,
            ),
            68 => 
            array (
                'id' => 69,
                'country_code' => 'FO',
                'country_name' => 'Faroe Islands',
                'dialCode' => 298,
            ),
            69 => 
            array (
                'id' => 70,
                'country_code' => 'FJ',
                'country_name' => 'Fiji',
                'dialCode' => 679,
            ),
            70 => 
            array (
                'id' => 71,
                'country_code' => 'FI',
                'country_name' => 'Finland',
                'dialCode' => 358,
            ),
            71 => 
            array (
                'id' => 72,
                'country_code' => 'FR',
                'country_name' => 'France',
                'dialCode' => 33,
            ),
            72 => 
            array (
                'id' => 73,
                'country_code' => 'FX',
                'country_name' => 'France, Metropolitan',
                'dialCode' => 0,
            ),
            73 => 
            array (
                'id' => 74,
                'country_code' => 'GF',
                'country_name' => 'French Guiana',
                'dialCode' => 0,
            ),
            74 => 
            array (
                'id' => 75,
                'country_code' => 'PF',
                'country_name' => 'French Polynesia',
                'dialCode' => 689,
            ),
            75 => 
            array (
                'id' => 76,
                'country_code' => 'TF',
                'country_name' => 'French Southern Territories',
                'dialCode' => 0,
            ),
            76 => 
            array (
                'id' => 77,
                'country_code' => 'GA',
                'country_name' => 'Gabon',
                'dialCode' => 241,
            ),
            77 => 
            array (
                'id' => 78,
                'country_code' => 'GM',
                'country_name' => 'Gambia',
                'dialCode' => 220,
            ),
            78 => 
            array (
                'id' => 79,
                'country_code' => 'GE',
                'country_name' => 'Georgia',
                'dialCode' => 995,
            ),
            79 => 
            array (
                'id' => 80,
                'country_code' => 'DE',
                'country_name' => 'Germany',
                'dialCode' => 49,
            ),
            80 => 
            array (
                'id' => 81,
                'country_code' => 'GH',
                'country_name' => 'Ghana',
                'dialCode' => 233,
            ),
            81 => 
            array (
                'id' => 82,
                'country_code' => 'GI',
                'country_name' => 'Gibraltar',
                'dialCode' => 350,
            ),
            82 => 
            array (
                'id' => 83,
                'country_code' => 'GK',
                'country_name' => 'Guernsey',
                'dialCode' => 0,
            ),
            83 => 
            array (
                'id' => 84,
                'country_code' => 'GR',
                'country_name' => 'Greece',
                'dialCode' => 30,
            ),
            84 => 
            array (
                'id' => 85,
                'country_code' => 'GL',
                'country_name' => 'Greenland',
                'dialCode' => 299,
            ),
            85 => 
            array (
                'id' => 86,
                'country_code' => 'GD',
                'country_name' => 'Grenada',
                'dialCode' => 1473,
            ),
            86 => 
            array (
                'id' => 87,
                'country_code' => 'GP',
                'country_name' => 'Guadeloupe',
                'dialCode' => 0,
            ),
            87 => 
            array (
                'id' => 88,
                'country_code' => 'GU',
                'country_name' => 'Guam',
                'dialCode' => 1671,
            ),
            88 => 
            array (
                'id' => 89,
                'country_code' => 'GT',
                'country_name' => 'Guatemala',
                'dialCode' => 502,
            ),
            89 => 
            array (
                'id' => 90,
                'country_code' => 'GN',
                'country_name' => 'Guinea',
                'dialCode' => 224,
            ),
            90 => 
            array (
                'id' => 91,
                'country_code' => 'GW',
                'country_name' => 'Guinea-Bissau',
                'dialCode' => 245,
            ),
            91 => 
            array (
                'id' => 92,
                'country_code' => 'GY',
                'country_name' => 'Guyana',
                'dialCode' => 592,
            ),
            92 => 
            array (
                'id' => 93,
                'country_code' => 'HT',
                'country_name' => 'Haiti',
                'dialCode' => 509,
            ),
            93 => 
            array (
                'id' => 94,
                'country_code' => 'HM',
                'country_name' => 'Heard and Mc Donald Islands',
                'dialCode' => 0,
            ),
            94 => 
            array (
                'id' => 95,
                'country_code' => 'HN',
                'country_name' => 'Honduras',
                'dialCode' => 504,
            ),
            95 => 
            array (
                'id' => 96,
                'country_code' => 'HK',
                'country_name' => 'Hong Kong',
                'dialCode' => 852,
            ),
            96 => 
            array (
                'id' => 97,
                'country_code' => 'HU',
                'country_name' => 'Hungary',
                'dialCode' => 36,
            ),
            97 => 
            array (
                'id' => 98,
                'country_code' => 'IS',
                'country_name' => 'Iceland',
                'dialCode' => 354,
            ),
            98 => 
            array (
                'id' => 99,
                'country_code' => 'IN',
                'country_name' => 'India',
                'dialCode' => 91,
            ),
            99 => 
            array (
                'id' => 100,
                'country_code' => 'IM',
                'country_name' => 'Isle of Man',
                'dialCode' => 44,
            ),
            100 => 
            array (
                'id' => 101,
                'country_code' => 'ID',
                'country_name' => 'Indonesia',
                'dialCode' => 62,
            ),
            101 => 
            array (
                'id' => 102,
                'country_code' => 'IR',
            'country_name' => 'Iran (Islamic Republic of)',
                'dialCode' => 98,
            ),
            102 => 
            array (
                'id' => 103,
                'country_code' => 'IQ',
                'country_name' => 'Iraq',
                'dialCode' => 964,
            ),
            103 => 
            array (
                'id' => 104,
                'country_code' => 'IE',
                'country_name' => 'Ireland',
                'dialCode' => 353,
            ),
            104 => 
            array (
                'id' => 105,
                'country_code' => 'IL',
                'country_name' => 'Israel',
                'dialCode' => 972,
            ),
            105 => 
            array (
                'id' => 106,
                'country_code' => 'IT',
                'country_name' => 'Italy',
                'dialCode' => 39,
            ),
            106 => 
            array (
                'id' => 107,
                'country_code' => 'CI',
                'country_name' => 'Ivory Coast',
                'dialCode' => 225,
            ),
            107 => 
            array (
                'id' => 108,
                'country_code' => 'JE',
                'country_name' => 'Jersey',
                'dialCode' => 0,
            ),
            108 => 
            array (
                'id' => 109,
                'country_code' => 'JM',
                'country_name' => 'Jamaica',
                'dialCode' => 1876,
            ),
            109 => 
            array (
                'id' => 110,
                'country_code' => 'JP',
                'country_name' => 'Japan',
                'dialCode' => 81,
            ),
            110 => 
            array (
                'id' => 111,
                'country_code' => 'JO',
                'country_name' => 'Jordan',
                'dialCode' => 962,
            ),
            111 => 
            array (
                'id' => 112,
                'country_code' => 'KZ',
                'country_name' => 'Kazakhstan',
                'dialCode' => 7,
            ),
            112 => 
            array (
                'id' => 113,
                'country_code' => 'KE',
                'country_name' => 'Kenya',
                'dialCode' => 254,
            ),
            113 => 
            array (
                'id' => 114,
                'country_code' => 'KI',
                'country_name' => 'Kiribati',
                'dialCode' => 686,
            ),
            114 => 
            array (
                'id' => 115,
                'country_code' => 'KP',
                'country_name' => 'Korea, Democratic People\'s Republic of',
                'dialCode' => 850,
            ),
            115 => 
            array (
                'id' => 116,
                'country_code' => 'KR',
                'country_name' => 'Korea, Republic of',
                'dialCode' => 82,
            ),
            116 => 
            array (
                'id' => 117,
                'country_code' => 'XK',
                'country_name' => 'Kosovo',
                'dialCode' => 381,
            ),
            117 => 
            array (
                'id' => 118,
                'country_code' => 'KW',
                'country_name' => 'Kuwait',
                'dialCode' => 965,
            ),
            118 => 
            array (
                'id' => 119,
                'country_code' => 'KG',
                'country_name' => 'Kyrgyzstan',
                'dialCode' => 996,
            ),
            119 => 
            array (
                'id' => 120,
                'country_code' => 'LA',
                'country_name' => 'Lao People\'s Democratic Republic',
                'dialCode' => 856,
            ),
            120 => 
            array (
                'id' => 121,
                'country_code' => 'LV',
                'country_name' => 'Latvia',
                'dialCode' => 371,
            ),
            121 => 
            array (
                'id' => 122,
                'country_code' => 'LB',
                'country_name' => 'Lebanon',
                'dialCode' => 961,
            ),
            122 => 
            array (
                'id' => 123,
                'country_code' => 'LS',
                'country_name' => 'Lesotho',
                'dialCode' => 266,
            ),
            123 => 
            array (
                'id' => 124,
                'country_code' => 'LR',
                'country_name' => 'Liberia',
                'dialCode' => 231,
            ),
            124 => 
            array (
                'id' => 125,
                'country_code' => 'LY',
                'country_name' => 'Libyan Arab Jamahiriya',
                'dialCode' => 218,
            ),
            125 => 
            array (
                'id' => 126,
                'country_code' => 'LI',
                'country_name' => 'Liechtenstein',
                'dialCode' => 423,
            ),
            126 => 
            array (
                'id' => 127,
                'country_code' => 'LT',
                'country_name' => 'Lithuania',
                'dialCode' => 370,
            ),
            127 => 
            array (
                'id' => 128,
                'country_code' => 'LU',
                'country_name' => 'Luxembourg',
                'dialCode' => 352,
            ),
            128 => 
            array (
                'id' => 129,
                'country_code' => 'MO',
                'country_name' => 'Macau',
                'dialCode' => 853,
            ),
            129 => 
            array (
                'id' => 130,
                'country_code' => 'MK',
                'country_name' => 'Macedonia',
                'dialCode' => 389,
            ),
            130 => 
            array (
                'id' => 131,
                'country_code' => 'MG',
                'country_name' => 'Madagascar',
                'dialCode' => 261,
            ),
            131 => 
            array (
                'id' => 132,
                'country_code' => 'MW',
                'country_name' => 'Malawi',
                'dialCode' => 265,
            ),
            132 => 
            array (
                'id' => 133,
                'country_code' => 'MY',
                'country_name' => 'Malaysia',
                'dialCode' => 60,
            ),
            133 => 
            array (
                'id' => 134,
                'country_code' => 'MV',
                'country_name' => 'Maldives',
                'dialCode' => 960,
            ),
            134 => 
            array (
                'id' => 135,
                'country_code' => 'ML',
                'country_name' => 'Mali',
                'dialCode' => 223,
            ),
            135 => 
            array (
                'id' => 136,
                'country_code' => 'MT',
                'country_name' => 'Malta',
                'dialCode' => 356,
            ),
            136 => 
            array (
                'id' => 137,
                'country_code' => 'MH',
                'country_name' => 'Marshall Islands',
                'dialCode' => 692,
            ),
            137 => 
            array (
                'id' => 138,
                'country_code' => 'MQ',
                'country_name' => 'Martinique',
                'dialCode' => 0,
            ),
            138 => 
            array (
                'id' => 139,
                'country_code' => 'MR',
                'country_name' => 'Mauritania',
                'dialCode' => 222,
            ),
            139 => 
            array (
                'id' => 140,
                'country_code' => 'MU',
                'country_name' => 'Mauritius',
                'dialCode' => 230,
            ),
            140 => 
            array (
                'id' => 141,
                'country_code' => 'TY',
                'country_name' => 'Mayotte',
                'dialCode' => 262,
            ),
            141 => 
            array (
                'id' => 142,
                'country_code' => 'MX',
                'country_name' => 'Mexico',
                'dialCode' => 52,
            ),
            142 => 
            array (
                'id' => 143,
                'country_code' => 'FM',
                'country_name' => 'Micronesia, Federated States of',
                'dialCode' => 691,
            ),
            143 => 
            array (
                'id' => 144,
                'country_code' => 'MD',
                'country_name' => 'Moldova, Republic of',
                'dialCode' => 373,
            ),
            144 => 
            array (
                'id' => 145,
                'country_code' => 'MC',
                'country_name' => 'Monaco',
                'dialCode' => 377,
            ),
            145 => 
            array (
                'id' => 146,
                'country_code' => 'MN',
                'country_name' => 'Mongolia',
                'dialCode' => 976,
            ),
            146 => 
            array (
                'id' => 147,
                'country_code' => 'ME',
                'country_name' => 'Montenegro',
                'dialCode' => 382,
            ),
            147 => 
            array (
                'id' => 148,
                'country_code' => 'MS',
                'country_name' => 'Montserrat',
                'dialCode' => 1664,
            ),
            148 => 
            array (
                'id' => 149,
                'country_code' => 'MA',
                'country_name' => 'Morocco',
                'dialCode' => 212,
            ),
            149 => 
            array (
                'id' => 150,
                'country_code' => 'MZ',
                'country_name' => 'Mozambique',
                'dialCode' => 258,
            ),
            150 => 
            array (
                'id' => 151,
                'country_code' => 'MM',
                'country_name' => 'Myanmar',
                'dialCode' => 95,
            ),
            151 => 
            array (
                'id' => 152,
                'country_code' => 'NA',
                'country_name' => 'Namibia',
                'dialCode' => 264,
            ),
            152 => 
            array (
                'id' => 153,
                'country_code' => 'NR',
                'country_name' => 'Nauru',
                'dialCode' => 674,
            ),
            153 => 
            array (
                'id' => 154,
                'country_code' => 'NP',
                'country_name' => 'Nepal',
                'dialCode' => 977,
            ),
            154 => 
            array (
                'id' => 155,
                'country_code' => 'NL',
                'country_name' => 'Netherlands',
                'dialCode' => 31,
            ),
            155 => 
            array (
                'id' => 156,
                'country_code' => 'AN',
                'country_name' => 'Netherlands Antilles',
                'dialCode' => 599,
            ),
            156 => 
            array (
                'id' => 157,
                'country_code' => 'NC',
                'country_name' => 'New Caledonia',
                'dialCode' => 687,
            ),
            157 => 
            array (
                'id' => 158,
                'country_code' => 'NZ',
                'country_name' => 'New Zealand',
                'dialCode' => 64,
            ),
            158 => 
            array (
                'id' => 159,
                'country_code' => 'NI',
                'country_name' => 'Nicaragua',
                'dialCode' => 505,
            ),
            159 => 
            array (
                'id' => 160,
                'country_code' => 'NE',
                'country_name' => 'Niger',
                'dialCode' => 227,
            ),
            160 => 
            array (
                'id' => 161,
                'country_code' => 'NG',
                'country_name' => 'Nigeria',
                'dialCode' => 234,
            ),
            161 => 
            array (
                'id' => 162,
                'country_code' => 'NU',
                'country_name' => 'Niue',
                'dialCode' => 683,
            ),
            162 => 
            array (
                'id' => 163,
                'country_code' => 'NF',
                'country_name' => 'Norfolk Island',
                'dialCode' => 0,
            ),
            163 => 
            array (
                'id' => 164,
                'country_code' => 'MP',
                'country_name' => 'Northern Mariana Islands',
                'dialCode' => 1670,
            ),
            164 => 
            array (
                'id' => 165,
                'country_code' => 'NO',
                'country_name' => 'Norway',
                'dialCode' => 47,
            ),
            165 => 
            array (
                'id' => 166,
                'country_code' => 'OM',
                'country_name' => 'Oman',
                'dialCode' => 968,
            ),
            166 => 
            array (
                'id' => 167,
                'country_code' => 'PK',
                'country_name' => 'Pakistan',
                'dialCode' => 92,
            ),
            167 => 
            array (
                'id' => 168,
                'country_code' => 'PW',
                'country_name' => 'Palau',
                'dialCode' => 680,
            ),
            168 => 
            array (
                'id' => 169,
                'country_code' => 'PS',
                'country_name' => 'Palestine',
                'dialCode' => 0,
            ),
            169 => 
            array (
                'id' => 170,
                'country_code' => 'PA',
                'country_name' => 'Panama',
                'dialCode' => 507,
            ),
            170 => 
            array (
                'id' => 171,
                'country_code' => 'PG',
                'country_name' => 'Papua New Guinea',
                'dialCode' => 675,
            ),
            171 => 
            array (
                'id' => 172,
                'country_code' => 'PY',
                'country_name' => 'Paraguay',
                'dialCode' => 595,
            ),
            172 => 
            array (
                'id' => 173,
                'country_code' => 'PE',
                'country_name' => 'Peru',
                'dialCode' => 51,
            ),
            173 => 
            array (
                'id' => 174,
                'country_code' => 'PH',
                'country_name' => 'Philippines',
                'dialCode' => 63,
            ),
            174 => 
            array (
                'id' => 175,
                'country_code' => 'PN',
                'country_name' => 'Pitcairn',
                'dialCode' => 870,
            ),
            175 => 
            array (
                'id' => 176,
                'country_code' => 'PL',
                'country_name' => 'Poland',
                'dialCode' => 48,
            ),
            176 => 
            array (
                'id' => 177,
                'country_code' => 'PT',
                'country_name' => 'Portugal',
                'dialCode' => 351,
            ),
            177 => 
            array (
                'id' => 178,
                'country_code' => 'PR',
                'country_name' => 'Puerto Rico',
                'dialCode' => 1,
            ),
            178 => 
            array (
                'id' => 179,
                'country_code' => 'QA',
                'country_name' => 'Qatar',
                'dialCode' => 974,
            ),
            179 => 
            array (
                'id' => 180,
                'country_code' => 'RE',
                'country_name' => 'Reunion',
                'dialCode' => 0,
            ),
            180 => 
            array (
                'id' => 181,
                'country_code' => 'RO',
                'country_name' => 'Romania',
                'dialCode' => 40,
            ),
            181 => 
            array (
                'id' => 182,
                'country_code' => 'RU',
                'country_name' => 'Russian Federation',
                'dialCode' => 7,
            ),
            182 => 
            array (
                'id' => 183,
                'country_code' => 'RW',
                'country_name' => 'Rwanda',
                'dialCode' => 250,
            ),
            183 => 
            array (
                'id' => 184,
                'country_code' => 'KN',
                'country_name' => 'Saint Kitts and Nevis',
                'dialCode' => 1869,
            ),
            184 => 
            array (
                'id' => 185,
                'country_code' => 'LC',
                'country_name' => 'Saint Lucia',
                'dialCode' => 1758,
            ),
            185 => 
            array (
                'id' => 186,
                'country_code' => 'VC',
                'country_name' => 'Saint Vincent and the Grenadines',
                'dialCode' => 1784,
            ),
            186 => 
            array (
                'id' => 187,
                'country_code' => 'WS',
                'country_name' => 'Samoa',
                'dialCode' => 685,
            ),
            187 => 
            array (
                'id' => 188,
                'country_code' => 'SM',
                'country_name' => 'San Marino',
                'dialCode' => 378,
            ),
            188 => 
            array (
                'id' => 189,
                'country_code' => 'ST',
                'country_name' => 'Sao Tome and Principe',
                'dialCode' => 239,
            ),
            189 => 
            array (
                'id' => 190,
                'country_code' => 'SA',
                'country_name' => 'Saudi Arabia',
                'dialCode' => 966,
            ),
            190 => 
            array (
                'id' => 191,
                'country_code' => 'SN',
                'country_name' => 'Senegal',
                'dialCode' => 221,
            ),
            191 => 
            array (
                'id' => 192,
                'country_code' => 'RS',
                'country_name' => 'Serbia',
                'dialCode' => 381,
            ),
            192 => 
            array (
                'id' => 193,
                'country_code' => 'SC',
                'country_name' => 'Seychelles',
                'dialCode' => 248,
            ),
            193 => 
            array (
                'id' => 194,
                'country_code' => 'SL',
                'country_name' => 'Sierra Leone',
                'dialCode' => 232,
            ),
            194 => 
            array (
                'id' => 195,
                'country_code' => 'SG',
                'country_name' => 'Singapore',
                'dialCode' => 65,
            ),
            195 => 
            array (
                'id' => 196,
                'country_code' => 'SK',
                'country_name' => 'Slovakia',
                'dialCode' => 421,
            ),
            196 => 
            array (
                'id' => 197,
                'country_code' => 'SI',
                'country_name' => 'Slovenia',
                'dialCode' => 386,
            ),
            197 => 
            array (
                'id' => 198,
                'country_code' => 'SB',
                'country_name' => 'Solomon Islands',
                'dialCode' => 677,
            ),
            198 => 
            array (
                'id' => 199,
                'country_code' => 'SO',
                'country_name' => 'Somalia',
                'dialCode' => 252,
            ),
            199 => 
            array (
                'id' => 200,
                'country_code' => 'ZA',
                'country_name' => 'South Africa',
                'dialCode' => 27,
            ),
            200 => 
            array (
                'id' => 201,
                'country_code' => 'GS',
                'country_name' => 'South Georgia South Sandwich Islands',
                'dialCode' => 0,
            ),
            201 => 
            array (
                'id' => 202,
                'country_code' => 'SS',
                'country_name' => 'South Sudan',
                'dialCode' => 0,
            ),
            202 => 
            array (
                'id' => 203,
                'country_code' => 'ES',
                'country_name' => 'Spain',
                'dialCode' => 34,
            ),
            203 => 
            array (
                'id' => 204,
                'country_code' => 'LK',
                'country_name' => 'Sri Lanka',
                'dialCode' => 94,
            ),
            204 => 
            array (
                'id' => 205,
                'country_code' => 'SH',
                'country_name' => 'St. Helena',
                'dialCode' => 290,
            ),
            205 => 
            array (
                'id' => 206,
                'country_code' => 'PM',
                'country_name' => 'St. Pierre and Miquelon',
                'dialCode' => 508,
            ),
            206 => 
            array (
                'id' => 207,
                'country_code' => 'SD',
                'country_name' => 'Sudan',
                'dialCode' => 249,
            ),
            207 => 
            array (
                'id' => 208,
                'country_code' => 'SR',
                'country_name' => 'Suriname',
                'dialCode' => 597,
            ),
            208 => 
            array (
                'id' => 209,
                'country_code' => 'SJ',
                'country_name' => 'Svalbard and Jan Mayen Islands',
                'dialCode' => 0,
            ),
            209 => 
            array (
                'id' => 210,
                'country_code' => 'SZ',
                'country_name' => 'Swaziland',
                'dialCode' => 268,
            ),
            210 => 
            array (
                'id' => 211,
                'country_code' => 'SE',
                'country_name' => 'Sweden',
                'dialCode' => 46,
            ),
            211 => 
            array (
                'id' => 212,
                'country_code' => 'CH',
                'country_name' => 'Switzerland',
                'dialCode' => 41,
            ),
            212 => 
            array (
                'id' => 213,
                'country_code' => 'SY',
                'country_name' => 'Syrian Arab Republic',
                'dialCode' => 963,
            ),
            213 => 
            array (
                'id' => 214,
                'country_code' => 'TW',
                'country_name' => 'Taiwan',
                'dialCode' => 886,
            ),
            214 => 
            array (
                'id' => 215,
                'country_code' => 'TJ',
                'country_name' => 'Tajikistan',
                'dialCode' => 992,
            ),
            215 => 
            array (
                'id' => 216,
                'country_code' => 'TZ',
                'country_name' => 'Tanzania, United Republic of',
                'dialCode' => 255,
            ),
            216 => 
            array (
                'id' => 217,
                'country_code' => 'TH',
                'country_name' => 'Thailand',
                'dialCode' => 66,
            ),
            217 => 
            array (
                'id' => 218,
                'country_code' => 'TG',
                'country_name' => 'Togo',
                'dialCode' => 228,
            ),
            218 => 
            array (
                'id' => 219,
                'country_code' => 'TK',
                'country_name' => 'Tokelau',
                'dialCode' => 690,
            ),
            219 => 
            array (
                'id' => 220,
                'country_code' => 'TO',
                'country_name' => 'Tonga',
                'dialCode' => 676,
            ),
            220 => 
            array (
                'id' => 221,
                'country_code' => 'TT',
                'country_name' => 'Trinidad and Tobago',
                'dialCode' => 1868,
            ),
            221 => 
            array (
                'id' => 222,
                'country_code' => 'TN',
                'country_name' => 'Tunisia',
                'dialCode' => 216,
            ),
            222 => 
            array (
                'id' => 223,
                'country_code' => 'TR',
                'country_name' => 'Turkey',
                'dialCode' => 90,
            ),
            223 => 
            array (
                'id' => 224,
                'country_code' => 'TM',
                'country_name' => 'Turkmenistan',
                'dialCode' => 993,
            ),
            224 => 
            array (
                'id' => 225,
                'country_code' => 'TC',
                'country_name' => 'Turks and Caicos Islands',
                'dialCode' => 1649,
            ),
            225 => 
            array (
                'id' => 226,
                'country_code' => 'TV',
                'country_name' => 'Tuvalu',
                'dialCode' => 688,
            ),
            226 => 
            array (
                'id' => 227,
                'country_code' => 'UG',
                'country_name' => 'Uganda',
                'dialCode' => 256,
            ),
            227 => 
            array (
                'id' => 228,
                'country_code' => 'UA',
                'country_name' => 'Ukraine',
                'dialCode' => 380,
            ),
            228 => 
            array (
                'id' => 229,
                'country_code' => 'AE',
                'country_name' => 'United Arab Emirates',
                'dialCode' => 971,
            ),
            229 => 
            array (
                'id' => 230,
                'country_code' => 'GB',
                'country_name' => 'United Kingdom',
                'dialCode' => 44,
            ),
            230 => 
            array (
                'id' => 231,
                'country_code' => 'US',
                'country_name' => 'United States',
                'dialCode' => 1,
            ),
            231 => 
            array (
                'id' => 232,
                'country_code' => 'UM',
                'country_name' => 'United States minor outlying islands',
                'dialCode' => 0,
            ),
            232 => 
            array (
                'id' => 233,
                'country_code' => 'UY',
                'country_name' => 'Uruguay',
                'dialCode' => 598,
            ),
            233 => 
            array (
                'id' => 234,
                'country_code' => 'UZ',
                'country_name' => 'Uzbekistan',
                'dialCode' => 998,
            ),
            234 => 
            array (
                'id' => 235,
                'country_code' => 'VU',
                'country_name' => 'Vanuatu',
                'dialCode' => 678,
            ),
            235 => 
            array (
                'id' => 236,
                'country_code' => 'VA',
                'country_name' => 'Vatican City State',
                'dialCode' => 39,
            ),
            236 => 
            array (
                'id' => 237,
                'country_code' => 'VE',
                'country_name' => 'Venezuela',
                'dialCode' => 58,
            ),
            237 => 
            array (
                'id' => 238,
                'country_code' => 'VN',
                'country_name' => 'Vietnam',
                'dialCode' => 84,
            ),
            238 => 
            array (
                'id' => 239,
                'country_code' => 'VG',
            'country_name' => 'Virgin Islands (British)',
                'dialCode' => 1284,
            ),
            239 => 
            array (
                'id' => 240,
                'country_code' => 'VI',
            'country_name' => 'Virgin Islands (U.S.)',
                'dialCode' => 1340,
            ),
            240 => 
            array (
                'id' => 241,
                'country_code' => 'WF',
                'country_name' => 'Wallis and Futuna Islands',
                'dialCode' => 681,
            ),
            241 => 
            array (
                'id' => 242,
                'country_code' => 'EH',
                'country_name' => 'Western Sahara',
                'dialCode' => 0,
            ),
            242 => 
            array (
                'id' => 243,
                'country_code' => 'YE',
                'country_name' => 'Yemen',
                'dialCode' => 967,
            ),
            243 => 
            array (
                'id' => 244,
                'country_code' => 'ZR',
                'country_name' => 'Zaire',
                'dialCode' => 0,
            ),
            244 => 
            array (
                'id' => 245,
                'country_code' => 'ZM',
                'country_name' => 'Zambia',
                'dialCode' => 260,
            ),
            245 => 
            array (
                'id' => 246,
                'country_code' => 'ZW',
                'country_name' => 'Zimbabwe',
                'dialCode' => 263,
            ),
        ));
        
        
    }
}