<?php

use App\Newplanslimit;
use Illuminate\Database\Seeder;

class PlansDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Newplanslimit::create( [
            'plan_id'=>1,
            'name'=>'TIFFANY',
            'personal_min'=>0,
           // 'personal_max'=>699,
            'personal_max'=>299,
            'structural_min'=>0,
            'structural_max'=>4999
        ] );



        Newplanslimit::create( [
            'plan_id'=>2,
            'name'=>'BLUE MOON',
         //  'personal_min'=>700,
         //   'personal_max'=>3000,
            'personal_min'=>300,
            'personal_max'=>1000,
            'structural_min'=>5000,
            'structural_max'=>30000
        ] );



        Newplanslimit::create( [
            'plan_id'=>3,
            'name'=>'AURORA',
            'personal_min'=>1000,
            'personal_max'=>1500,
            //     'personal_min'=>3000,
      //      'personal_max'=>10000,
            'structural_min'=>30000,
            'structural_max'=>100000
        ] );



        Newplanslimit::create( [
            'plan_id'=>4,
            'name'=>'CULLINAN',
//            'personal_min'=>10000,
//            'personal_max'=>30000,
//            'structural_min'=>100000,
//            'structural_max'=>500000
            'personal_min'=>1500,
            'personal_max'=>4000,
            'structural_min'=>100000,
            'structural_max'=>400000
        ] );



        Newplanslimit::create( [
            'plan_id'=>5,
            'name'=>'SANCY',
//            'personal_min'=>30000,
//            'personal_max'=>50000,
//            'structural_min'=>500000,
//            'structural_max'=>1000000
           'personal_min'=>4000,
            'personal_max'=>6000,
            'structural_min'=>400000,
            'structural_max'=>1000000
        ] );



        Newplanslimit::create( [
            'plan_id'=>6,
            'name'=>'KOH I NOOR',
//            'personal_min'=>50000,
//            'personal_max'=>1500000,
//            'structural_min'=>1000000,
//            'structural_max'=>1000000000
            'personal_min'=>6000,
            'personal_max'=>1500000,
            'structural_min'=>1000000,
            'structural_max'=>1000000000
        ] );
    }
}
