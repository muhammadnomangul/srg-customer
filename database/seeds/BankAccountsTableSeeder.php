<?php

use Illuminate\Database\Seeder;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('bank_accounts')->delete();

        \DB::table('bank_accounts')->insert(array (
            0 =>
            array (
                'id' => 1,
                'bank_name' => 'TransferWise',
                'account_title' => 'B4U Group of Companies, S.L',
                'account_number' => '411006212',
                'account_number1' => NULL,
                'bank_branch_code' => '802-985',
                'currency' => 'AUD',
                'country_id' => 13,
                'address' => 'Plaza de Los Luceros, 17. Piso 9. Pta. 1, 03004 Alicante, Spain.',
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            1 =>
            array (
                'id' => 2,
                'bank_name' => 'YapıKredi BANK',
                'account_title' => 'SAIF UR REHMAN KHAN',
                'account_number' => '22853382904',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'AZN',
                'country_id' => 15,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            2 =>
            array (
                'id' => 3,
                'bank_name' => 'ACLEDA BANK',
                'account_title' => 'B4U EXCHANGE Cambodia',
                'account_number' => '0001-03182763-14 (USD)',
                'account_number1' => '0001-03182763-25 (KHR)',
                'bank_branch_code' => NULL,
                'currency' => 'KHR',
                'country_id' => 36,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            3 =>
            array (
                'id' => 4,
                'bank_name' => 'TD Bank',
                'account_title' => 'B4U Global Group Inc.',
                'account_number' => '15795027660',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'CAD',
                'country_id' => 38,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            4 =>
            array (
                'id' => 5,
                'bank_name' => 'Kapital Bank ASC Şuşa filialı',
                'account_title' => 'SAIF UR REHMAN KHAN',
                'account_number' => 'AZ79AIIB410150A9447000851116',
                'account_number1' => NULL,
                'bank_branch_code' => '200167',
                'currency' => 'EUR',
                'country_id' => NULL,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            5 =>
            array (
                'id' => 6,
                'bank_name' => 'TransferWise',
                'account_title' => 'B4U Group of Companies, S.L',
                'account_number' => 'BE79 9670 5851 7133',
                'account_number1' => NULL,
                'bank_branch_code' => 'TRWIBEB1XXX',
                'currency' => 'EUR',
                'country_id' => NULL,
                'address' => 'Plaza de Los Luceros, 17. Piso 9. Pta. 1, 03004 Alicante, Spain.',
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            6 =>
            array (
                'id' => 7,
                'bank_name' => 'MAY BANK',
                'account_title' => 'BRAVO TECH TRADING',
                'account_number' => '562209657075',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'MYR',
                'country_id' => 133,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

            7 =>
            array (
                'id' => 8,
                'bank_name' => 'AYA BANK',
                'account_title' => 'B4U Co,Ltd',
                'account_number' => '0030103010010688',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'Burma',
                'country_id' => 151,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            8 =>
            array (
                'id' => 9,
                'bank_name' => 'MCB BANK',
                'account_title' => 'FUTURE PROPERTY ASSOCIATES Pvt. Ltd.',
                'account_number' => 'PK05MUCB1181838801000837',
                'account_number1' => '1181838801000837',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            9 =>
            array (
                'id' => 10,
                'bank_name' => 'Soneri BANK',
                'account_title' => 'FUTURE PROPERTY ASSOCIATES Pvt. Ltd.',
                'account_number' => 'PK14SONE0003220006701702',
                'account_number1' => '003220006701702',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            10 =>
            array (
                'id' => 11,
                'bank_name' => 'Habib Metro BANK',
                'account_title' => 'AAS WARYAS TRADERS',
                'account_number' => 'PK04MPBL1201027140185954',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            11 =>
            array (
                'id' => 12,
                'bank_name' => 'Faysal Bank',
                'account_title' => 'AAS WARYAS Global Trading Pvt. Ltd.',
                'account_number' => 'PK48FAYS3316301000000109',
                'account_number1' => '3316301000000109',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            12 =>
            array (
                'id' => 13,
                'bank_name' => 'BANK ISLAMI',
                'account_title' => 'AAS WARYAS TRADERS',
                'account_number' => 'PK19BKIP0209300136500001',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),
            13 =>
            array (
                'id' => 14,
                'bank_name' => 'MCB BANK',
                'account_title' => 'Alpha Advertising (PVT.) LTD.',
                'account_number' => 'PK81MUCB1138940491000701',
                'account_number1' => '1138940491000701',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

             14 =>
            array (
                'id' => 15,
                'bank_name' => 'ASKARI BANK',
                'account_title' => 'AAS WARYAS GLOBAL TRADING PVT. LTD.',
                'account_number' => 'PK85ASCM0000560420001029',
                'account_number1' => '5604-20001029',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

             15 =>
            array (
                'id' => 16,
                'bank_name' => 'ASKARI BANK',
                'account_title' => 'AAS WARYAS LOGISTICS PVT. LTD.',
                'account_number' => 'PK58ASCM0000560420001030',
                'account_number1' => '5604-20001030',
                'bank_branch_code' => NULL,
                'currency' => 'PKR',
                'country_id' => 167,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

            16 =>
            array (
                'id' => 17,
                'bank_name' => 'BDO BANK',
                'account_title' => 'B4U TRADING CO',
                'account_number' => '008630178715',
                'account_number1' => NULL,
                'bank_branch_code' => NULL,
                'currency' => 'PHP',
                'country_id' => 174,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

            17 =>
            array (
                'id' => 18,
                'bank_name' => 'Monzo Bank',
                'account_title' => 'SAIF UR REHMAN KHAN',
                'account_number' => '39556511',
                'account_number1' => NULL,
                'bank_branch_code' => 'MONZGB2L',
                'currency' => 'GBP',
                'country_id' => 230,
                'address' => NULL,
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

            18 =>
            array (
                'id' => 19,
                'bank_name' => 'TransferWise',
                'account_title' => 'B4U Group of Companies, S.L',
                'account_number' => 'GB97 TRWI 2314 7058 7312 76',
                'account_number1' => '58731276',
                'bank_branch_code' => NULL,
                'currency' => 'GBP',
                'country_id' => 230,
                'address' => 'Plaza de Los Luceros, 17. Piso 9. Pta. 1, 03004 Alicante, Spain.',
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

            19 =>
            array (
                'id' => 20,
                'bank_name' => 'TransferWise',
                'account_title' => 'B4U Group of Companies, S.L',
                'account_number' => '8310615550',
                'account_number1' => '02673008',
                'bank_branch_code' => 'CMFGUS33',
                'currency' => 'USD',
                'country_id' => 231,
                'address' => 'Plaza de Los Luceros, 17. Piso 9. Pta. 1, 03004 Alicante, Spain.',
                'created_at' => '2020-07-13 00:00:00',
                'updated_at' => '2020-07-13 00:00:00',
            ),

        ));
    }
}
