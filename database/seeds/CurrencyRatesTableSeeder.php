<?php

use Illuminate\Database\Seeder;

class CurrencyRatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currency_rates')->delete();
        
        \DB::table('currency_rates')->insert(array (
            0 => 
            array (
                'id' => 745,
                'rate_usd' => 1.0,
                'rate_btc' => 7101.75,
                'rate_eth' => 141.33,
                'rate_ltc' => 43.03,
                'rate_bch' => 206.17,
                'rate_xrp' => 0.2127,
                'rate_dash' => 49.34,
                'rate_zec' => 32.27,
                'rate_dsh' => 0.0,
                'today_profit' => 0.34,
                'created_at' => '2020-06-08 00:00:00',
                'updated_at' => '2020-07-02 22:34:51',
            ),
        ));
        
        
    }
}