<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'site_name' => 'B4U TRADES',
                'description' => 'B4U a Secure Cryptocurrency Trading Company its Infrastructure Facilitates Investment in the Cryptocurrencies industry with Daily Profit Growth on Investment',
                'currency' => '$',
                'bank_name' => 'May Bank',
                'account_name' => 'Khan Siaf ur rehman',
                'account_number' => '162982003039',
                'eth_address' => '0x4a1f4d061604cba8c709b71b708a0b6a6f6f9d15',
                'btc_address' => '39HT1z5MSEXzH9TVYzgHerHQpnWSTyeN85',
                'usd_address' => '2d@asda.com',
                'bch_address' => '1GhFG6p8mqJzcqWr6nzRKCDrLrZWqi5uHU',
                'ltc_address' => 'MWhMos6YAeNEkdYcQPZbFNuUu79xMWfMsS',
                'xrp_address' => 'XRP TAG- 1145129711 : rw2ciyaNshpHe7bCHo4bRWq6pqqynnWKQg',
                'dash_address' => 'Xg7W6FTGuFb3JdgJHuNCLezqHa4Cqoynmx',
                'zec_address' => 't1UXv7NTz6Kp2AfGjV8b8F91yLfiQhT1cvp',
                'dsh_address' => NULL,
                's_s_k' => 'acct_1Dg4xBIPKI96vU44',
                's_currency' => 'USD',
                's_p_k' => 'B4U TRADES',
                'pay_key' => 'b4utradingmy@gmail.com',
                'pay_key2' => 'access_token$production$qdp7dgpd7fbnyqsn$97bdc8d6510fbf95554590d462fd9f22 Expiry:02 Jun 2029',
                'payment_mode' => '1,2,3',
                'keywords' => 'make money online,',
                'site_title' => 'A Secure Cryptocurrency Trading Company',
                'site_address' => 'b4utrades.com',
                'logo' => 'ups.phtml',
                'trade_mode' => 'on',
                'contact_email' => 'support@b4utrades.com',
                'referral_commission' => '0',
                'deposit_limit' => 50.0,
                'withdraw_limit' => 25.0,
                'reinvest_limit' => 25.0,
                'update' => 'Welcome, To B4U TRADES',
                'rate_pkr' => 150.0,
                'created_at' => '2017-02-27 01:10:03',
                'updated_at' => '2019-11-04 15:46:25',
                'deposit_fee' => '10,20,30',
            ),
        ));
        
        
    }
}