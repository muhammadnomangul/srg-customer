<?php

use Illuminate\Database\Seeder;

class PlansDetailTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('plans_detail')->delete();
        
        \DB::table('plans_detail')->insert(array (
            0 => 
            array (
                'id' => 1,
                'plan_id' => 1,
                'level' => 1,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            1 => 
            array (
                'id' => 2,
                'plan_id' => 1,
                'level' => 2,
                'investment_bonus' => '2.00',
                'profit_bonus' => '0.00',
            ),
            2 => 
            array (
                'id' => 3,
                'plan_id' => 1,
                'level' => 3,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            3 => 
            array (
                'id' => 4,
                'plan_id' => 1,
                'level' => 4,
                'investment_bonus' => '0.00',
                'profit_bonus' => '0.00',
            ),
            4 => 
            array (
                'id' => 5,
                'plan_id' => 1,
                'level' => 5,
                'investment_bonus' => '0.00',
                'profit_bonus' => '0.00',
            ),
            5 => 
            array (
                'id' => 6,
                'plan_id' => 2,
                'level' => 1,
                'investment_bonus' => '7.00',
                'profit_bonus' => '0.00',
            ),
            6 => 
            array (
                'id' => 7,
                'plan_id' => 2,
                'level' => 2,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            7 => 
            array (
                'id' => 8,
                'plan_id' => 2,
                'level' => 3,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            8 => 
            array (
                'id' => 9,
                'plan_id' => 2,
                'level' => 4,
                'investment_bonus' => '0.00',
                'profit_bonus' => '0.00',
            ),
            9 => 
            array (
                'id' => 10,
                'plan_id' => 2,
                'level' => 5,
                'investment_bonus' => '0.00',
                'profit_bonus' => '0.00',
            ),
            10 => 
            array (
                'id' => 11,
                'plan_id' => 3,
                'level' => 1,
                'investment_bonus' => '10.00',
                'profit_bonus' => '0.00',
            ),
            11 => 
            array (
                'id' => 12,
                'plan_id' => 3,
                'level' => 2,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            12 => 
            array (
                'id' => 13,
                'plan_id' => 3,
                'level' => 3,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            13 => 
            array (
                'id' => 14,
                'plan_id' => 3,
                'level' => 4,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            14 => 
            array (
                'id' => 15,
                'plan_id' => 3,
                'level' => 5,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            15 => 
            array (
                'id' => 16,
                'plan_id' => 4,
                'level' => 1,
                'investment_bonus' => '10.00',
                'profit_bonus' => '0.00',
            ),
            16 => 
            array (
                'id' => 17,
                'plan_id' => 4,
                'level' => 2,
                'investment_bonus' => '5.00',
                'profit_bonus' => '0.00',
            ),
            17 => 
            array (
                'id' => 18,
                'plan_id' => 4,
                'level' => 3,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            18 => 
            array (
                'id' => 19,
                'plan_id' => 4,
                'level' => 4,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            19 => 
            array (
                'id' => 20,
                'plan_id' => 4,
                'level' => 5,
                'investment_bonus' => '1.00',
                'profit_bonus' => '0.00',
            ),
            20 => 
            array (
                'id' => 21,
                'plan_id' => 5,
                'level' => 1,
                'investment_bonus' => '12.00',
                'profit_bonus' => '0.00',
            ),
            21 => 
            array (
                'id' => 22,
                'plan_id' => 5,
                'level' => 2,
                'investment_bonus' => '5.00',
                'profit_bonus' => '0.00',
            ),
            22 => 
            array (
                'id' => 23,
                'plan_id' => 5,
                'level' => 3,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            23 => 
            array (
                'id' => 24,
                'plan_id' => 5,
                'level' => 4,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            24 => 
            array (
                'id' => 25,
                'plan_id' => 5,
                'level' => 5,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            25 => 
            array (
                'id' => 26,
                'plan_id' => 6,
                'level' => 1,
                'investment_bonus' => '15.00',
                'profit_bonus' => '0.00',
            ),
            26 => 
            array (
                'id' => 27,
                'plan_id' => 6,
                'level' => 2,
                'investment_bonus' => '7.00',
                'profit_bonus' => '0.00',
            ),
            27 => 
            array (
                'id' => 28,
                'plan_id' => 6,
                'level' => 3,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            28 => 
            array (
                'id' => 29,
                'plan_id' => 6,
                'level' => 4,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
            29 => 
            array (
                'id' => 30,
                'plan_id' => 6,
                'level' => 5,
                'investment_bonus' => '3.00',
                'profit_bonus' => '0.00',
            ),
        ));
        
        
    }
}