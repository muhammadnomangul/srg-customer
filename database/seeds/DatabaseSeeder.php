<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CountriesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CurrencyRatesTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(PlansDetailTableSeeder::class);
        $this->call(PlansLimitsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
    }
}
