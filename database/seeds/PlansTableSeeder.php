<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('plans')->delete();
        
        \DB::table('plans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'TIFFANY',
                'personel_investment_limit' => 699,
                'structural_investment_limit' => 4999,
                'price' => '700',
                'expected_return' => '400',
                'type' => 'Main',
                'img_url' => 'affiliates/tiffany.png',
                'created_at' => '2018-08-10 21:25:37',
                'updated_at' => '2018-08-16 13:43:55',
                'increment_interval' => 'Two Months',
                'increment_type' => '',
                'increment_amount' => NULL,
                'expiration' => '0',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'BLUE MOON',
                'personel_investment_limit' => 700,
                'structural_investment_limit' => 5000,
                'price' => '1500',
                'expected_return' => '700',
                'type' => 'Main',
                'img_url' => 'affiliates/bluemoon.png',
                'created_at' => '2018-08-11 09:33:37',
                'updated_at' => '2018-08-12 13:26:29',
                'increment_interval' => 'Two Months',
                'increment_type' => '',
                'increment_amount' => NULL,
                'expiration' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'AURORA',
                'personel_investment_limit' => 3000,
                'structural_investment_limit' => 30000,
                'price' => '3500',
                'expected_return' => '1200',
                'type' => 'Main',
                'img_url' => 'affiliates/aurora.png',
                'created_at' => '2018-08-12 10:40:27',
                'updated_at' => '2018-08-13 19:31:50',
                'increment_interval' => 'Two Months',
                'increment_type' => 'Percentage',
                'increment_amount' => '10',
                'expiration' => '',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'CULLINAN',
                'personel_investment_limit' => 10000,
                'structural_investment_limit' => 100000,
                'price' => '7000',
                'expected_return' => '3000',
                'type' => 'Main',
                'img_url' => 'affiliates/cullinan.png',
                'created_at' => '2018-08-13 13:20:51',
                'updated_at' => '2018-08-14 13:26:41',
                'increment_interval' => 'Two Months',
                'increment_type' => '',
                'increment_amount' => NULL,
                'expiration' => '',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'SANCY',
                'personel_investment_limit' => 30000,
                'structural_investment_limit' => 500000,
                'price' => '10000',
                'expected_return' => '4000',
                'type' => 'Main',
                'img_url' => 'affiliates/sancy.png',
                'created_at' => '2018-08-14 15:02:29',
                'updated_at' => '2018-08-15 09:24:28',
                'increment_interval' => 'Two Months',
                'increment_type' => 'Percentage',
                'increment_amount' => '14',
                'expiration' => '',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'KOH I NOOR',
                'personel_investment_limit' => 50000,
                'structural_investment_limit' => 1000000,
                'price' => '100000',
                'expected_return' => '5000',
                'type' => 'Main',
                'img_url' => 'affiliates/koh-e-noor.png',
                'created_at' => '2018-08-15 12:58:40',
                'updated_at' => '2018-08-16 19:31:32',
                'increment_interval' => 'Two Months',
                'increment_type' => 'Fixed',
                'increment_amount' => '20',
                'expiration' => '',
            ),
        ));
        
        
    }
}