<?php

use Illuminate\Database\Seeder;

class PlansLimitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('plans_limits')->delete();
        
        \DB::table('plans_limits')->insert(array (
            0 => 
            array (
                'plan_id' => 1,
                'name' => 'TIFFANY',
                'personal_min' => 0.0,
                'personal_max' => 700.0,
                'structural_min' => 0.0,
                'structural_max' => 5000.0,
            ),
            1 => 
            array (
                'plan_id' => 2,
                'name' => 'BLUE MOON',
                'personal_min' => 700.0,
                'personal_max' => 3000.0,
                'structural_min' => 5000.0,
                'structural_max' => 30000.0,
            ),
            2 => 
            array (
                'plan_id' => 3,
                'name' => 'AURORA',
                'personal_min' => 3000.0,
                'personal_max' => 10000.0,
                'structural_min' => 30000.0,
                'structural_max' => 100000.0,
            ),
            3 => 
            array (
                'plan_id' => 4,
                'name' => 'CULLINAN',
                'personal_min' => 10000.0,
                'personal_max' => 30000.0,
                'structural_min' => 100000.0,
                'structural_max' => 500000.0,
            ),
            4 => 
            array (
                'plan_id' => 5,
                'name' => 'SANCY',
                'personal_min' => 30000.0,
                'personal_max' => 50000.0,
                'structural_min' => 500000.0,
                'structural_max' => 1000000.0,
            ),
            5 => 
            array (
                'plan_id' => 6,
                'name' => 'KOH I NOOR',
                'personal_min' => 50000.0,
                'personal_max' => 1500000.0,
                'structural_min' => 1000000.0,
                'structural_max' => 1000000000.0,
            ),
        ));
        
        
    }
}