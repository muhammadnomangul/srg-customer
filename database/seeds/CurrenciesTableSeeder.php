<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currencies')->delete();
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Dollar',
                'code' => 'USD',
                'small_code' => 'usd',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:01:55',
                'updated_at' => '2018-12-20 20:47:24',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Bitcoin',
                'code' => 'BTC',
                'small_code' => 'btc',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:01:55',
                'updated_at' => '2018-11-22 14:00:17',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Etherum',
                'code' => 'ETH',
                'small_code' => 'eth',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:21',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Bitcash',
                'code' => 'BCH',
                'small_code' => 'bch',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:24',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Litecoin',
                'code' => 'LTC',
                'small_code' => 'ltc',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:27',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Ripple',
                'code' => 'XRP',
                'small_code' => 'xrp',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:30',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Dash',
                'code' => 'DASH',
                'small_code' => 'dash',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:33',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Zeecash',
                'code' => 'ZEC',
                'small_code' => 'zec',
                'status' => 'Active',
                'created_at' => '2018-11-22 11:04:38',
                'updated_at' => '2018-11-22 14:00:40',
            ),
        ));
        
        
    }
}