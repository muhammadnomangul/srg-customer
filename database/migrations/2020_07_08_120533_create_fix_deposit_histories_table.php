<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixDepositHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fix_deposit_histories', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('deposit_id')->index('fix_deposit_histories_deposit_id_foreign');
			$table->integer('user_id')->index('fix_deposit_histories_user_id_foreign');
			$table->float('amount', 10, 0)->nullable();
			$table->float('profit', 10, 0)->nullable();
			$table->float('deduction', 10, 0)->nullable();
			$table->float('new_amount', 10, 0)->nullable();
			$table->timestamps();
			$table->bigInteger('plan_id')->unsigned()->index('fix_deposit_histories_plan_id_foreign');
			$table->dateTime('end_date');
			$table->boolean('is_done');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fix_deposit_histories');
	}

}
