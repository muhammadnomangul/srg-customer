<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSoldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solds', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->string('unique_id', 20)->nullable();
			$table->integer('trade_id');
			$table->string('currency', 10)->nullable();
			$table->float('amount', 10, 0)->default(0);
			$table->string('status', 20);
			$table->string('pre_status', 20)->nullable();
			$table->string('payment_mode', 20);
			$table->float('waiting_profit', 10, 0)->default(0);
			$table->float('sold_profit', 10, 0)->default(0);
			$table->float('pre_amount', 10, 0)->nullable()->default(0);
			$table->float('new_amount', 10, 0)->nullable()->default(0);
			$table->float('sale_deduct_amount', 10, 0)->default(0);
			$table->string('new_type', 100)->nullable();
			$table->integer('paid_flag')->default(0);
			$table->integer('flag_dummy')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solds');
	}

}
