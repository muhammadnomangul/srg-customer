<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyProfitBonusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_profit_bonus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('user_id', 60);
			$table->integer('trade_id')->index();
			$table->string('currency', 25)->nullable();
			$table->float('trade_amount', 10, 0)->nullable()->default(0);
			$table->float('last_profit', 10, 0)->default(0);
			$table->float('today_profit', 10, 0)->default(0);
			$table->float('crypto_amount', 10, 0)->default(0);
			$table->float('crypto_profit', 10, 0)->default(0);
			$table->float('percetage', 10, 0)->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_profit_bonus');
	}

}
