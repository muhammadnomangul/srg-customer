<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoogle2faColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     * @Column 2fa_auth_type will be 0, 1 and 2, 0 for the email 1 for the twilio and 2 for the  google authenticator and 3 for non.
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('google2fa_secret');
            $table->boolean('two_fa_auth_type')->default(0);
            $table->float('sms_payable_amount')->default(0);
            $table->float('sms_paid_amount')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('google2fa_secret');
            $table->dropColumn('two_fa_auth_type');
            $table->dropColumn('sms_payable_amount');
            $table->dropColumn('sms_paid_amount');

        });
    }
}
