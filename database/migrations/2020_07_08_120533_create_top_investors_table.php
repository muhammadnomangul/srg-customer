<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopInvestorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('top_investors', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->string('user_uid', 50)->nullable();
			$table->integer('level')->default(1);
			$table->integer('total_amount')->default(0);
			$table->integer('total_child')->default(0);
			$table->text('child_list', 65535)->nullable();
			$table->date('from_date')->nullable();
			$table->date('to_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('top_investors');
	}

}
