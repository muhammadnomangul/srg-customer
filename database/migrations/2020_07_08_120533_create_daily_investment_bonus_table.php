<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyInvestmentBonusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_investment_bonus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('user_id', 100)->nullable()->index();
			$table->string('parent_id', 100)->nullable()->index();
			$table->integer('parent_user_id')->index();
			$table->integer('trade_id')->index();
			$table->float('bonus', 10, 0)->default(0)->index();
			$table->float('pre_bonus_amt', 10, 0)->default(0);
			$table->float('new_bonus_amt', 10, 0)->default(0);
			$table->string('details');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_investment_bonus');
	}

}
