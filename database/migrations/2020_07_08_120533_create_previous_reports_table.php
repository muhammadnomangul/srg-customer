<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreviousReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('previous_reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('login_id')->nullable();
			$table->string('type')->nullable();
			$table->string('earning')->nullable();
			$table->string('reinvest')->nullable();
			$table->string('deduction')->nullable();
			$table->string('withdrawal')->nullable();
			$table->string('balance')->nullable();
			$table->integer('user_id')->nullable();
			$table->boolean('is_corrected')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('previous_reports');
	}

}
