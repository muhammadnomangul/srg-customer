<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsIntoDeposits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits',function (Blueprint $table){
            $table->float('curr_amount')->default(0)->after('unique_id');
            $table->string('source_currency',5)->nullable()->after('bank_id');
            $table->boolean('is_closed')->default(0);
            $table->date('closed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('curr_amount');
            $table->dropColumn('source_currency');
            $table->dropColumn('is_closed');
            $table->dropColumn('closed_at');
        });
    }
}
