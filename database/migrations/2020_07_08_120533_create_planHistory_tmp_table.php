<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanHistoryTmpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planHistory_tmp', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('num')->nullable();
			$table->string('oldPlanName', 20)->nullable();
			$table->string('newPlanName', 20)->nullable();
			$table->date('changeDate')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planHistory_tmp');
	}

}
