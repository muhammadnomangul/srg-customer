<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ratings', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();

            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('cb_account_id')->index();
            $table->foreign('cb_account_id')->references('id')->on('cb_accounts');

            $table->enum('type',['SellDebit','BuyCredit'])->nullable();
            $table->float('amount');
            $table->string('status','10')->nullable();

           $table->enum('rating',[0,1,2,3,4,5,6]);

           $table->integer('credit_id')->nullable()->index();
            $table->foreign('credit_id')->references('id')->on('cb_credits');
            $table->integer('debit_id')->nullable()->index();
            $table->foreign('debit_id')->references('id')->on('cb_debits');
            $table->text('comments')->nullable();

            $table->integer('rating_by')->index();
            $table->foreign('rating_by')->references('id')->on('users');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_ratings');
    }
}
