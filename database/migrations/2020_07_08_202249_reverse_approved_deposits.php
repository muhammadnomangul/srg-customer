<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReverseApprovedDeposits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared("
        
CREATE  PROCEDURE `reverse_approved_deposits`(deposit_id INT, logged_in_user_id INT)
proc_approve:
       BEGIN

	SET @deposit_id = deposit_id;
	SET @logged_in_user_id = logged_in_user_id;
    
	SET @deposit_status = (SELECT status FROM deposits WHERE id = @deposit_id);
    
    IF @deposit_status <> 'Approved' THEN
		SELECT 'Deposit is not Approved' AS Result;
		LEAVE proc_approve;
	END IF;
    
	SET @child_id = (SELECT user_id FROM deposits WHERE id = @deposit_id);
	SET @child_u_id = (SELECT unique_id FROM deposits WHERE id = @deposit_id);
	SET @converted_amount = (SELECT IFNULL(total_amount, 0) FROM deposits WHERE id = @deposit_id);
	SET @currency = (SELECT currency FROM deposits WHERE id = @deposit_id);
	SET @deposit_amount = (SELECT IFNULL(amount, 0) FROM deposits WHERE id = @deposit_id);
	SET @trans_type = (SELECT trans_type FROM deposits WHERE id = @deposit_id);
     
	CREATE TEMPORARY TABLE IF NOT EXISTS deposit_calculations 
	(
		parent_id INT, 
		parent_u_id VARCHAR(20), 
		level INT, 
		plan_id INT, 
		investment_percentage FLOAT, 
		converted_amount FLOAT, 
		latest_bonus FLOAT, 
		reference_bonus FLOAT, 
		total_bonus FLOAT, 
		predicted_plan INT
	);

	CREATE TEMPORARY TABLE IF NOT EXISTS balances
	(
		user_id INT,
		balance_usd float,
		balance_btc float,
		balance_eth float,
		balance_bch float,
		balance_ltc float,
		balance_xrp float,
		balance_dash float,
		balance_zec float
	);

	DELETE FROM balances;

	-- Minus from total balance
	INSERT INTO balances
	SELECT
		@child_id,
		CASE WHEN @currency = 'USD' THEN IFNULL(balance_usd, 0) - @deposit_amount ELSE balance_usd END balance_usd,
		CASE WHEN @currency = 'BTC' THEN IFNULL(balance_btc, 0) - @deposit_amount ELSE balance_btc END balance_btc,
		CASE WHEN @currency = 'ETH' THEN IFNULL(balance_eth, 0) - @deposit_amount ELSE balance_eth END balance_eth,    
		CASE WHEN @currency = 'BCH' THEN IFNULL(balance_bch, 0) - @deposit_amount ELSE balance_bch END balance_bch,    
		CASE WHEN @currency = 'LTC' THEN IFNULL(balance_ltc, 0) - @deposit_amount ELSE balance_ltc END balance_ltc,
		CASE WHEN @currency = 'XRP' THEN IFNULL(balance_xrp, 0) - @deposit_amount ELSE balance_xrp END balance_xrp,  
		CASE WHEN @currency = 'DASH' THEN IFNULL(balance_dash, 0) - @deposit_amount ELSE balance_dash END balance_dash,    
		CASE WHEN @currency = 'ZEC' THEN IFNULL(balance_zec, 0) - @deposit_amount ELSE balance_zec END balance_zec
	FROM user_accounts
	WHERE user_id = @child_id;

	DELETE FROM deposit_calculations;

	INSERT INTO deposit_calculations
	SELECT r.parent_id, r.parent_u_id, r.level, u.plan, pd.investment_bonus, @converted_amount AS converted_amount, IFNULL((pd.investment_bonus/100)*@converted_amount, 0) * -1 AS latest_bonus, 
	IFNULL(ua.reference_bonus, 0), IFNULL(ua.reference_bonus, 0) - IFNULL((pd.investment_bonus/100)*@converted_amount, 0) total_bonus, 
	decide_plan(r.parent_id, 0, 0) predicted_plan
	FROM referrals r
	INNER JOIN users u ON u.id = r.parent_id
	LEFT JOIN user_accounts ua ON ua.user_id = r.parent_id
	LEFT JOIN plans_detail pd ON pd.plan_id = u.plan AND pd.level = r.level
	WHERE child_id = @child_id AND r.parent_id > 1
	GROUP BY r.parent_id, r.parent_u_id, r.level, u.plan, pd.investment_bonus, ua.reference_bonus;

	-- getting previous balance for user logs
	SET @user_balance = (SELECT CASE 
							WHEN @currency = 'USD' THEN balance_usd
							WHEN @currency = 'BTC' THEN balance_btc
							WHEN @currency = 'ETH' THEN balance_eth 
							WHEN @currency = 'BCH' THEN balance_bch
							WHEN @currency = 'LTC' THEN balance_ltc
							WHEN @currency = 'XRP' THEN balance_xrp
							WHEN @currency = 'DASH' THEN balance_dash
							ELSE balance_zec END
							FROM user_accounts where user_id = @child_id);

    SET @log_detail = CONCAT(@child_id, ' has updated UserBalance of  ', @user_balance, ' by currency (', @currency ,') to ', @user_balance - @deposit_amount);
    
    SELECT decide_plan(@child_id, 0, 0) INTO @user_predicted_plan;
    
	SET autocommit = 0;
	START TRANSACTION;

	-- update child balance in users accounts and approved deposits
    -- pre_status = 'Approved'
    -- status = 'Pending'
    -- approved_at = NULL
	update deposits set pre_status = status, status = 'Cancelled', approved_at = NULL, updated_at = CURRENT_TIMESTAMP() where id = @deposit_id;

	UPDATE user_accounts AS a 
	INNER JOIN balances AS d ON d.user_id = a.user_id
	SET a.balance_usd = d.balance_usd,
	a.balance_btc = d.balance_btc,
	a.balance_eth = d.balance_eth,
	a.balance_bch = d.balance_bch,
	a.balance_ltc = d.balance_ltc,
	a.balance_xrp = d.balance_xrp,
	a.balance_dash = d.balance_dash,
	a.balance_zec = d.balance_zec,
	a.updated_at = CURRENT_TIMESTAMP();

	INSERT INTO logs (title, details, user_id, user_uid, currency, amount, pre_amount, curr_amount, approved_by, updated_at, created_at) 
	VALUES ('UserBalance Reversed', @log_detail, @child_id, @child_u_id, @currency, @deposit_amount, @user_balance, @user_balance + @deposit_amount, @logged_in_user_id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

	INSERT INTO admin_logs (user_id, admin_id, trade_id, event, updated_at, created_at) 
	VALUES (@child_u_id, 'B4U0001', @deposit_id, 'User Deposit Reversed', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

	DELETE FROM daily_investment_bonus WHERE trade_id = @deposit_id;
	DELETE FROM daily_profit_bonus WHERE trade_id = @deposit_id;
    
    IF @trans_type <>  'Reinvestment'
    THEN
		-- update reference_bonus for parents in user accounts
		UPDATE user_accounts AS u 
		INNER JOIN deposit_calculations AS d ON d.parent_id = u.user_id
		SET u.reference_bonus = d.total_bonus,
		u.latest_bonus = d.latest_bonus,
		u.updated_at = CURRENT_TIMESTAMP();

		insert into logs (title, details, user_id, user_uid, currency, amount, pre_amount, curr_amount, approved_by, updated_at, created_at) 
		SELECT 'Reference Bonus Reversed', CONCAT(parent_id, ' has reversed bonus to ', total_bonus), parent_id, parent_u_id, 'USD', latest_bonus, reference_bonus, total_bonus, @logged_in_user_id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()
		FROM deposit_calculations
		WHERE investment_percentage > 0;

		insert into admin_logs (user_id, admin_id, trade_id, event, updated_at, created_at) 
		SELECT parent_u_id, 'B4U0001', @deposit_id, 'User Deposit Reversed', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()
		FROM deposit_calculations
		WHERE investment_percentage > 0;

		-- Minus from attractive funds
		UPDATE attractive_funds AS a 
		INNER JOIN deposit_calculations AS d ON d.parent_id = a.user_id AND d.level = a.level
		SET a.amount = IFNULL(a.amount, 0) - @converted_amount,
		a.max_amount = IFNULL(a.max_amount, 0) - @converted_amount,
		a.updated_at = CURRENT_TIMESTAMP();

		-- update parents' plans
		UPDATE users AS u 
		INNER JOIN deposit_calculations AS d ON d.parent_id = u.id
		SET u.plan = d.predicted_plan;
	
    END IF;
    
	-- update depositor's plan
	UPDATE users SET plan = IFNULL(@user_predicted_plan, plan) WHERE id = @child_id;
    
    -- Deleting gift user deposits
    DELETE FROM fix_deposit_histories WHERE deposit_id = @deposit_id;
    
    CALL reverse_withdrawal (@deposit_id, logged_in_user_id);
	COMMIT;

	CALL calculate_investment_values(@child_id);

	DROP TEMPORARY TABLE IF EXISTS balances;
	DROP TEMPORARY TABLE deposit_calculations;
    
    SELECT 'Done' AS Result;
END ;
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP PROCEDURE IF EXISTS reverse_approved_deposits;');

    }
}
