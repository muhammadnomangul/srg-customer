<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferalInvestmentBonusRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referal_investment_bonus_rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_id');
			$table->integer('first_line');
			$table->integer('second_line');
			$table->integer('third_line');
			$table->integer('fourth_line');
			$table->integer('fifth_line');
			$table->string('created_at', 100);
			$table->string('updated_at', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referal_investment_bonus_rules');
	}

}
