<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 20);
			$table->integer('personel_investment_limit');
			$table->integer('structural_investment_limit');
			$table->string('price', 20);
			$table->string('expected_return', 20);
			$table->string('type', 10)->nullable();
			$table->string('img_url', 200);
			$table->timestamps();
			$table->string('increment_interval', 20);
			$table->string('increment_type', 20);
			$table->string('increment_amount', 20)->nullable();
			$table->string('expiration', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans');
	}

}
