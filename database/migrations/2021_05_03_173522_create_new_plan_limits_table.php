<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPlanLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_plan_limits', function (Blueprint $table) {
            $table->integer('plan_id')->primary();
            $table->string('name', 20);
            $table->float('personal_min', 10, 0);
            $table->float('personal_max', 10, 0);
            $table->float('structural_min', 10, 0);
            $table->float('structural_max', 10, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_plan_limits');
    }
}
