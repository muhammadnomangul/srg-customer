<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepositsTmpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deposits_tmp', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('parent_Id')->nullable();
			$table->decimal('parentAmount', 10)->nullable();
			$table->decimal('childAmount', 10)->nullable();
			$table->date('approved_at')->nullable();
			$table->integer('r_level')->nullable()->default(0);
			$table->integer('sort')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deposits_tmp');
	}

}
