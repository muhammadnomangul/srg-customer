<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FunctionDecidePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \DB::unprepared("
        
CREATE  FUNCTION decide_plan (user_id INT, personal float, structural float) RETURNS int(11)
    DETERMINISTIC
BEGIN 
            DECLARE  p_plan_id int;
            DECLARE  s_plan_id int;
            DECLARE selected_plan_id int;
            DECLARE  local_structural float default 0;
            DECLARE  local_personal float default 0;
            
            SELECT IFNULL(SUM(amount), 0) INTO local_structural 
            FROM attractive_funds AS a WHERE a.user_id = user_id;
            
            SELECT IFNULL(SUM(total_amount), 0) INTO local_personal FROM deposits d 
            WHERE d.user_id = user_id AND status = 'Approved' AND trans_type IN ('NewInvestment', 'Reinvestment');
            
            SET local_personal = local_personal + personal;
            SET local_structural = local_structural + structural;
            
            SELECT plan_id INTO p_plan_Id FROM plans_limits WHERE personal_max > local_personal AND local_personal <= personal_max LIMIT 1;
            
            SELECT plan_id INTO s_plan_Id FROM plans_limits WHERE structural_max > local_structural AND local_structural <= structural_max LIMIT 1;
            
            IF p_plan_id >= s_plan_id
            THEN
                SELECT p_plan_id INTO selected_plan_id;
            ELSE 
                SELECT s_plan_id INTO selected_plan_id;
            END IF;
        RETURN selected_plan_id;
        END;

        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP PROCEDURE IF EXISTS decide_plan;');
    }
}
