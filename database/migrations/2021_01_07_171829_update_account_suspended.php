<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccountSuspended extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $blueprint) {
            $blueprint->dateTime('wrong_attempt_suspended_at')->nullable();
            $blueprint->smallInteger('wrong_attempt_unsuspended_times')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $blueprint) {
            $blueprint->dropColumn(
                [
                    'wrong_attempt_suspended_at',
                    'wrong_attempt_unsuspended_times',
                ]
            );
        });
    }
}
