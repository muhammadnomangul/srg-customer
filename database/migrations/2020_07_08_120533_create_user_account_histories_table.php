<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserAccountHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_account_histories', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('user_id')->index('user_account_histories_user_id_foreign');
			$table->text('bank_acc_info', 65535)->nullable();
			$table->text('kin_bank_info', 65535)->nullable();
			$table->text('bank_name', 65535)->nullable();
			$table->text('account_name', 65535)->nullable();
			$table->text('account_no', 65535)->nullable();
			$table->text('acc_hold_No', 65535)->nullable();
			$table->text('btc_address', 65535)->nullable();
			$table->text('eth_address', 65535)->nullable();
			$table->text('bch_address', 65535)->nullable();
			$table->text('ltc_address', 65535)->nullable();
			$table->text('xrp_address', 65535)->nullable();
			$table->text('dash_address', 65535)->nullable();
			$table->text('zec_address', 65535)->nullable();
			$table->text('dsh_address', 65535)->nullable();
			$table->timestamps();
			$table->integer('updated_by')->nullable()->index('user_account_histories_updated_by_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_account_histories');
	}

}
