<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferalProfitBonusRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referal_profit_bonus_rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_id');
			$table->integer('first_pline');
			$table->integer('second_pline');
			$table->integer('third_pline');
			$table->integer('fourth_pline');
			$table->integer('fifth_pline');
			$table->string('created_at', 100);
			$table->string('updated_at', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referal_profit_bonus_rules');
	}

}
