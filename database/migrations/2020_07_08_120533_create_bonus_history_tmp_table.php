<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBonusHistoryTmpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bonus_history_tmp', function(Blueprint $table)
		{
			$table->integer('parent_id')->nullable();
			$table->string('parent_u_id', 30)->nullable();
			$table->decimal('childAmount', 10)->nullable();
			$table->integer('childLevel')->nullable();
			$table->string('oldPlanName', 20)->nullable();
			$table->string('newPlanName', 20)->nullable();
			$table->date('changeDate')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bonus_history_tmp');
	}

}
