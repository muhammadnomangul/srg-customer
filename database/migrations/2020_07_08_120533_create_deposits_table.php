<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deposits', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index('IX_deposits_user_id');
			$table->string('unique_id', 60)->nullable();
			$table->float('amount', 10, 0)->default(0);
			$table->string('payment_mode', 20);
			$table->integer('bank_id')->nullable();
			$table->string('currency', 20);
			$table->float('rate', 10, 0)->default(0);
			$table->float('total_amount', 10, 0)->default(0);
			$table->string('plan', 20);
			$table->boolean('flag_dummy')->default(0);
			$table->boolean('profit_take')->default(0);
			$table->float('latest_profit', 10, 0)->default(0);
			$table->float('trade_profit', 10, 0)->default(0);
			$table->float('profit_total', 10, 0)->default(0);
			$table->float('latest_crypto_profit', 10, 0)->default(0);
			$table->float('crypto_profit', 10, 0)->default(0);
			$table->float('crypto_profit_total', 10, 0)->default(0);
			$table->float('new_total', 10, 0)->default(0);
			$table->string('pre_status', 20)->nullable();
			$table->string('status', 20);
			$table->string('trans_type', 15)->nullable();
			$table->string('reinvest_type', 60)->nullable();
			$table->text('trans_id', 65535)->nullable();
			$table->string('proof')->nullable();
			$table->dateTime('lastProfit_update_date')->nullable();
			$table->integer('waiting_profit_flag')->default(0);
			$table->date('approved_at')->nullable();
			$table->date('sold_at')->nullable();
			$table->integer('partial_tradeid')->default(0);
			$table->integer('is_partial_sold')->default(0);
			$table->float('total_profit_percetage_new_system', 10, 0)->default(0);
			$table->string('total_profit_new_system')->nullable();
			$table->timestamps();
			$table->decimal('fee_deducted', 5)->nullable();
			$table->integer('withdrawal_id')->nullable()->index('deposits_withdrawal_id_foreign');
			$table->string('notes')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deposits');
	}

}
