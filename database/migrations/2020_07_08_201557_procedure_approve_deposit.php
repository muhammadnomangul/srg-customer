<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProcedureApproveDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \DB::unprepared("
CREATE  PROCEDURE `approve_deposit`(IN `deposit_id` INT, IN `logged_in_user_id` INT, IN `tenure_id` INT, IN `startDate` DATE)
proc_approve:
        BEGIN
        
            SET @deposit_id = deposit_id;
            SET @logged_in_user_id = logged_in_user_id;
            SET @deposit_amount = 0.0;
            SET @child_id = 0;
            SET @child_u_id = '';
        
            SET @deposit_status = (SELECT status FROM deposits WHERE id = @deposit_id);
        
            IF @deposit_status = 'Approved' THEN
                SELECT 'Already Approved' AS Result;
                LEAVE proc_approve;
            END IF;
            
           
            SET @child_id = (SELECT user_id FROM deposits WHERE id = @deposit_id);
            SET @child_u_id = (SELECT unique_id FROM deposits WHERE id = @deposit_id);
            SET @converted_amount = (SELECT IFNULL(total_amount, 0) FROM deposits WHERE id = @deposit_id);
            SET @currency = (SELECT currency FROM deposits WHERE id = @deposit_id);
            SET @deposit_amount = (SELECT IFNULL(amount, 0) FROM deposits WHERE id = @deposit_id);
            SET @trans_type = (SELECT trans_type FROM deposits WHERE id = @deposit_id);
            SET @flag_dummy = (SELECT flag_dummy FROM deposits WHERE id = @deposit_id);
            
            IF (SELECT COUNT(*) FROM referrals WHERE child_id = @child_id) = 0
            THEN
                call generate_referral_tree (@child_id, 0,5);
            END IF;
            
            IF (SELECT COUNT(*) FROM referrals WHERE child_id = @child_id) = 0
            THEN
                SELECT 'Referral tree does not exists' AS Result;
                LEAVE proc_approve;
            END IF;
            
            CREATE TEMPORARY TABLE IF NOT EXISTS deposit_calculations 
            (
                parent_id INT, 
                parent_u_id VARCHAR(20), 
                level INT, 
                plan_id INT, 
                investment_percentage FLOAT, 
                converted_amount FLOAT, 
                latest_bonus FLOAT, 
                reference_bonus FLOAT, 
                total_bonus FLOAT, 
                predicted_plan INT
            );
        
            CREATE TEMPORARY TABLE IF NOT EXISTS balances
            (
                user_id INT,
                balance_usd float,
                balance_btc float,
                balance_eth float,
                balance_bch float,
                balance_ltc float,
                balance_xrp float,
                balance_dash float,
                balance_zec float
            );
        
            DELETE FROM balances;
        
            INSERT INTO balances
            SELECT
                @child_id,
                CASE WHEN @currency = 'USD' THEN IFNULL(balance_usd, 0) + @deposit_amount ELSE balance_usd END balance_usd,
                CASE WHEN @currency = 'BTC' THEN IFNULL(balance_btc, 0) + @deposit_amount ELSE balance_btc END balance_btc,
                CASE WHEN @currency = 'ETH' THEN IFNULL(balance_eth, 0) + @deposit_amount ELSE balance_eth END balance_eth,    
                CASE WHEN @currency = 'BCH' THEN IFNULL(balance_bch, 0) + @deposit_amount ELSE balance_bch END balance_bch,    
                CASE WHEN @currency = 'LTC' THEN IFNULL(balance_ltc, 0) + @deposit_amount ELSE balance_ltc END balance_ltc,
                CASE WHEN @currency = 'XRP' THEN IFNULL(balance_xrp, 0) + @deposit_amount ELSE balance_xrp END balance_xrp,  
                CASE WHEN @currency = 'DASH' THEN IFNULL(balance_dash, 0) + @deposit_amount ELSE balance_dash END balance_dash,    
                CASE WHEN @currency = 'ZEC' THEN IFNULL(balance_zec, 0) + @deposit_amount ELSE balance_zec END balance_zec
            FROM user_accounts
            WHERE user_id = @child_id;
        
        
            DELETE FROM deposit_calculations;
        
            INSERT INTO deposit_calculations
            SELECT r.parent_id, r.parent_u_id, r.level, u.plan, pd.investment_bonus, @converted_amount AS converted_amount, IFNULL((pd.investment_bonus/100)*@converted_amount, 0) AS latest_bonus, 
            IFNULL(ua.reference_bonus, 0), IFNULL(ua.reference_bonus, 0) + IFNULL((pd.investment_bonus/100)*@converted_amount, 0) total_bonus, decide_plan(r.parent_id, 0, @converted_amount) predicted_plan
            FROM referrals r
            INNER JOIN users u ON u.id = r.parent_id
            LEFT JOIN user_accounts ua ON ua.user_id = r.parent_id
            LEFT JOIN plans_detail pd ON pd.plan_id = u.plan AND pd.level = r.level
            WHERE child_id = @child_id AND r.parent_id > 1
            GROUP BY r.parent_id, r.parent_u_id, r.level, u.plan, pd.investment_bonus, ua.reference_bonus;
        
        
            -- getting previous balance for user logs
            SET @user_balance = (SELECT CASE 
                                    WHEN @currency = 'USD' THEN balance_usd
                                    WHEN @currency = 'BTC' THEN balance_btc
                                    WHEN @currency = 'ETH' THEN balance_eth 
                                    WHEN @currency = 'BCH' THEN balance_bch
                                    WHEN @currency = 'LTC' THEN balance_ltc
                                    WHEN @currency = 'XRP' THEN balance_xrp
                                    WHEN @currency = 'DASH' THEN balance_dash
                                    ELSE balance_zec END
                                    FROM user_accounts where user_id = @child_id);
        
            -- is gift users
            SET @tenure_end_date = NOW();
            SET @tenure_start_date = startDate;
            IF IFNULL(tenure_id, 0) > 0
            THEN
                SELECT CASE 
                    WHEN tenure_length = 'Y' THEN 
                        DATE_ADD(@tenure_start_date, INTERVAL tenure YEAR)
                    WHEN tenure_length = 'M' THEN
                        DATE_ADD(@tenure_start_date, INTERVAL tenure MONTH)
                    ELSE
                        DATE_ADD(@tenure_start_date, INTERVAL tenure YEAR)
                    END 
                        INTO @tenure_end_date
                    FROM fix_deposit_tenures
                    WHERE id = tenure_id;
            END IF;
            
            SET autocommit = 0;
            START TRANSACTION;
        
            -- update child balance in users accounts and approved deposits
            update deposits set 
                pre_status = status, 
                status = 'Approved', 
                approved_at = CURRENT_DATE(), 
                updated_at = CURRENT_TIMESTAMP(),
                profit_take = 1
            where id = @deposit_id;
        
            UPDATE user_accounts AS a 
            INNER JOIN balances AS d ON d.user_id = a.user_id
            SET a.balance_usd = d.balance_usd,
            a.balance_btc = d.balance_btc,
            a.balance_eth = d.balance_eth,
            a.balance_bch = d.balance_bch,
            a.balance_ltc = d.balance_ltc,
            a.balance_xrp = d.balance_xrp,
            a.balance_dash = d.balance_dash,
            a.balance_zec = d.balance_zec,
            a.updated_at = CURRENT_TIMESTAMP();
        
            insert into logs (title, details, user_id, user_uid, currency, amount, pre_amount, curr_amount, approved_by, updated_at, created_at) 
            SELECT 'UserBalance Updated', CONCAT(@child_id, ' has updated UserBalance of  ', @user_balance, ' by currency (', @currency ,') to ', @user_balance + @deposit_amount), 
            @child_id, @child_u_id, @currency, @deposit_amount, @user_balance, @user_balance + @deposit_amount, @logged_in_user_id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP();
        
            insert into admin_logs (user_id, admin_id, trade_id, event, updated_at, created_at) 
            Value(@child_u_id, @logged_in_user_id, @deposit_id, 'User Deposit Approved', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
        
            -- Parents will not get following in case of re-investment
            -- > Parents will not get investment bonus
            -- > Parents' reference bonus and latest bonus will not update in user_accounts
            -- > Parents' attractive funds will not update
            -- > Parents' plan will not change
            IF @trans_type <> 'Reinvestment' AND @flag_dummy = 0
            THEN
            
                -- insert into daily_investment_bonus
                INSERT INTO daily_investment_bonus(user_id,parent_id,parent_user_id,trade_id,bonus,pre_bonus_amt,new_bonus_amt,details,created_at,updated_at)
                SELECT @child_u_id, parent_u_id, parent_id, @deposit_id, latest_bonus, reference_bonus, total_bonus, CONCAT('Investment Bonus with percentage of', investment_percentage, ' by user ', @child_id), CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()
                FROM deposit_calculations
                WHERE investment_percentage > 0;
                
                -- update reference_bonus for parents in user accounts
                UPDATE user_accounts AS u 
                INNER JOIN deposit_calculations AS d ON d.parent_id = u.user_id
                SET u.reference_bonus = d.total_bonus,
                u.latest_bonus = d.latest_bonus,
                u.updated_at = CURRENT_TIMESTAMP();
                
                insert into logs (title, details, user_id, user_uid, currency, amount, pre_amount, curr_amount, approved_by, updated_at, created_at) 
                SELECT 'Reference Bonus Updated', CONCAT(parent_id, ' has updated bonus to ', total_bonus), parent_id, parent_u_id, 'USD', latest_bonus, reference_bonus, total_bonus, @logged_in_user_id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()
                FROM deposit_calculations
                WHERE investment_percentage > 0;
    
                
                -- update attractive_funds
                INSERT INTO attractive_funds (user_id, level, amount, max_amount, created_at, updated_at)
                SELECT d.parent_id, d.level, @converted_amount, @converted_amount, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM deposit_calculations AS d
                LEFT OUTER JOIN attractive_funds AS a ON d.parent_id = a.user_id AND d.level = a.level
                WHERE a.id IS NULL;
        
                UPDATE attractive_funds AS a 
                INNER JOIN deposit_calculations AS d ON d.parent_id = a.user_id AND d.level = a.level
                SET a.amount = IFNULL(a.amount, 0) + @converted_amount,
                a.max_amount = IFNULL(a.max_amount, 0) + @converted_amount,
                a.updated_at = CURRENT_TIMESTAMP();
        
                -- update plans
                UPDATE users AS u 
                INNER JOIN deposit_calculations AS d ON d.parent_id = u.id
                SET u.plan = d.predicted_plan;
            END IF;	
        
            UPDATE users SET plan = IFNULL(decide_plan(@child_id, 0, 0), plan) WHERE id = @child_id;
        
            IF IFNULL(tenure_id, 0) > 0 AND @trans_type <> 'Reinvestment'
            THEN
                INSERT INTO fix_deposit_histories
                (deposit_id,user_id,amount,profit,deduction,new_amount,created_at,updated_at,plan_id,end_date,is_done)
                VALUES 
                (@deposit_id, @child_id, @converted_amount, NULL, NULL, NULL, @tenure_start_date, NOW(), tenure_id, @tenure_end_date, 0);
            END IF;
            COMMIT;
            -- update referrals_investments_summation table for depositor
            CALL calculate_investment_values (@child_id);
        
            DROP TEMPORARY TABLE IF EXISTS balances;
            DROP TEMPORARY TABLE deposit_calculations;
            
            SELECT 'Done' AS Result;
        END;

        
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS approve_deposit;');

        //
    }
}
