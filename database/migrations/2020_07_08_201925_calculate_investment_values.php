<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CalculateInvestmentValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared("
      
CREATE  PROCEDURE `calculate_investment_values`(user_id INT)
BEGIN
        
            SET @user_id = user_id;
        
            DROP TABLE IF EXISTS transactions;
            CREATE TEMPORARY TABLE transactions (amount float, currency varchar(20), user_id int, trans_type varchar(50), status varchar(20));
        
            INSERT INTO transactions 
            SELECT sum(d.total_amount) amount, d.currency, d.user_id, d.trans_type, d.status 
            FROM deposits d
            WHERE (@user_id IS NULL OR d.user_id = @user_id) AND d.status NOT IN ('Cancelled', 'Deleted', 'Pending')
            GROUP BY d.currency, d.user_id, d.trans_type, d.status;
        
            INSERT INTO referrals_investments_summation (child_id, child_u_id, balance, currency_id, active_new_investment, active_reinvestment, sold_new_investment, sold)
            SELECT t.user_id, u.u_id, 0 balance, t.currency, t.amount, 0 reinvest, 0 sold_new, 0 sold 
            FROM transactions t
            LEFT OUTER JOIN referrals_investments_summation AS a ON a.child_id = t.user_id AND a.currency_id = t.currency
            LEFT OUTER JOIN users u ON u.id = t.user_id
            WHERE a.id IS NULL AND (t.trans_type = 'NewInvestment' AND t.status = 'Approved')
            ORDER BY t.user_id, t.currency;
        
            INSERT INTO referrals_investments_summation (child_id, child_u_id, balance, currency_id, active_new_investment, active_reinvestment, sold_new_investment, sold)
            SELECT t.user_id, u.u_id, 0 balance, t.currency, 0 new_invest, t.amount, 0 sold_new, 0 sold 
            FROM transactions t
            LEFT OUTER JOIN referrals_investments_summation AS a ON a.child_id = t.user_id AND a.currency_id = t.currency
            LEFT OUTER JOIN users u ON u.id = t.user_id
            WHERE a.id IS NULL AND (t.trans_type = 'Reinvestment' AND t.status = 'Approved')
            ORDER BY t.user_id, t.currency;
        
            INSERT INTO referrals_investments_summation (child_id, child_u_id, balance, currency_id, active_new_investment, active_reinvestment, sold_new_investment, sold)
            SELECT t.user_id, u.u_id, 0 balance, t.currency, 0 new_invest, 0 reinvest, t.amount sold_new, 0 sold 
            FROM transactions t
            LEFT OUTER JOIN referrals_investments_summation AS a ON a.child_id = t.user_id AND a.currency_id = t.currency
            LEFT OUTER JOIN users u ON u.id = t.user_id
            WHERE a.id IS NULL AND (t.trans_type = 'NewInvestment' AND t.status = 'Sold')
            ORDER BY t.user_id, t.currency;
        
            INSERT INTO referrals_investments_summation (child_id, child_u_id, balance, currency_id, active_new_investment, active_reinvestment, sold_new_investment, sold)
            SELECT t.user_id, u.u_id, 0 balance, t.currency, 0 new_invest, 0 reinvest, 0 sold_new, t.amount sold 
            FROM transactions t
            LEFT OUTER JOIN referrals_investments_summation AS a ON a.child_id = t.user_id AND a.currency_id = t.currency
            LEFT OUTER JOIN users u ON u.id = t.user_id
            WHERE a.id IS NULL AND (t.trans_type = 'NewInvestment' AND t.status = 'Sold')
            ORDER BY t.user_id, t.currency;
        
            UPDATE referrals_investments_summation AS a
            INNER JOIN transactions AS t ON a.child_id = t.user_id AND a.currency_id = t.currency
            SET a.active_new_investment = amount
            WHERE (t.trans_type = 'NewInvestment' AND t.status = 'Approved');
        
            UPDATE referrals_investments_summation AS a
            INNER JOIN transactions AS t ON a.child_id = t.user_id AND a.currency_id = t.currency
            SET a.active_reinvestment = amount
            WHERE (t.trans_type = 'Reinvestment' AND t.status = 'Approved');
        
            UPDATE referrals_investments_summation AS a
            INNER JOIN transactions AS t ON a.child_id = t.user_id AND a.currency_id = t.currency
            SET a.sold_new_investment = amount
            WHERE (t.trans_type = 'NewInvestment' AND t.status = 'Sold');
        
            UPDATE referrals_investments_summation AS a
            INNER JOIN transactions AS t ON a.child_id = t.user_id AND a.currency_id = t.currency
            SET a.sold = amount
            WHERE (t.trans_type = 'NewInvestment' AND t.status = 'Sold');
        
            DROP TABLE IF EXISTS transactions;
        END;
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS calculate_investment_values;');

        //
    }
}
