<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBonusHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bonus_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->string('unique_id', 50)->nullable();
			$table->integer('plan_id');
			$table->string('plan_name', 50)->nullable();
			$table->string('new_plan_name', 25)->nullable();
			$table->float('max_total', 10, 0)->default(0);
			$table->float('current_max_total', 10, 0)->default(0);
			$table->float('current_level_bonus', 10, 0)->default(0);
			$table->float('bonus_total', 10, 0)->default(0);
			$table->float('maxlevel1', 10, 0)->default(0);
			$table->float('maxlevel2', 10, 0)->default(0);
			$table->float('maxlevel3', 10, 0)->default(0);
			$table->float('maxlevel4', 10, 0)->default(0);
			$table->float('maxlevel5', 10, 0)->default(0);
			$table->float('current_max1', 10, 0)->default(0);
			$table->float('current_max2', 10, 0)->default(0);
			$table->float('current_max3', 10, 0)->default(0);
			$table->float('current_max4', 10, 0)->default(0);
			$table->float('current_max5', 10, 0)->default(0);
			$table->float('bonus_level1', 10, 0)->nullable()->default(0);
			$table->float('bonus_level2', 10, 0)->nullable()->default(0);
			$table->float('bonus_level3', 10, 0)->nullable()->default(0);
			$table->float('bonus_level4', 10, 0)->nullable()->default(0);
			$table->float('bonus_level5', 10, 0)->nullable()->default(0);
			$table->integer('created_by')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bonus_history');
	}

}
