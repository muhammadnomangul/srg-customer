<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinStrPersonalInvesmentIntoPlanLimits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans_limits',function (Blueprint $table){
            $table->float('structural_personal_min', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans_limits',function (Blueprint $table){
            $table->dropColumn('structural_personal_min');
        });
    }
}
