<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_id')->nullable();
			$table->integer('level')->nullable();
			$table->decimal('investment_bonus', 4)->nullable();
			$table->decimal('profit_bonus', 4)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans_detail');
	}

}
