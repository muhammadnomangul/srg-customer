<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcedureSelfInvestment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::unprepared("
          
    CREATE PROCEDURE `promo_investors_by_user_id`(IN `start_date` DATE, IN `end_date` DATE, IN `user_id` INT)
    BEGIN
        TRUNCATE TABLE top_investors_tmp;
        CREATE TEMPORARY TABLE parentIds (parentId INT, ChildAmount float);
        INSERT INTO parentIds
        SELECT r.parent_id, 
                SUM(d.total_amount) AS ChildAmount
                FROM deposits AS d
                INNER JOIN
                (
                    SELECT DISTINCT child_id, level, parent_Id FROM referrals
                )
                AS r ON d.user_id = r.child_id
                WHERE d.status = 'Approved' AND d.trans_type = 'NewInvestment' AND (approved_at >= start_date AND approved_at <= end_date)
                and r.parent_id = user_id

                GROUP BY  r.parent_id
                ORDER BY ChildAmount DESC LIMIT 100;
        INSERT INTO top_investors_tmp
        SELECT r.parent_id,
                u.u_id,
                u.name,
                u.email,
                u.phone_no,
                u.Country,
                u.plan,
                SUM(d.total_amount) AS ChildAmount,
                r.level
                FROM deposits AS d
                INNER JOIN
                (
                    SELECT DISTINCT child_id, level, parent_Id FROM referrals rr
                    INNER JOIN parentIds pid ON pid.parentId = rr.parent_Id
                )
                AS r ON d.user_id = r.child_id
                INNER JOIN users u ON u.id = r.parent_id
                WHERE d.status = 'Approved' AND d.trans_type = 'NewInvestment' AND (approved_at >= start_date AND approved_at <= end_date) AND u.status = 'active' AND u.type = 0
                GROUP BY r.parent_id, u.u_id, u.name, u.email, u.phone_no, u.Country, u.plan, r.level;
        SELECT
        parent_id AS id,
        u_id AS user_u_id,
        name,
        email,
        phone_no,
        Country,
        plan AS plan_id,
        (SUM(CASE WHEN level = 1 THEN ChildAmount ELSE 0 END)) AS child_amount_level_1,
        (SUM(CASE WHEN level = 2 THEN ChildAmount ELSE 0 END)) AS child_amount_level_2,
        (SUM(CASE WHEN level = 3 THEN ChildAmount ELSE 0 END)) AS child_amount_level_3,
        (SUM(CASE WHEN level = 4 THEN ChildAmount ELSE 0 END)) AS child_amount_level_4,
        (SUM(CASE WHEN level = 5 THEN ChildAmount ELSE 0 END)) AS child_amount_level_5
        
        FROM top_investors_tmp
        
        GROUP BY parent_id, u_id, name, email, phone_no, Country, plan;
        drop table parentIds;

    END;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedure_self_investment');
    }
}
