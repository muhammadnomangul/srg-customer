<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PendingWithDrawals extends Migration
{

    ## made this seprate because it contain two different subqueries other than withdrawals
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement("CREATE VIEW pendingWithdrawalView AS
        
                 SELECT
                `withdrawals`.*, `accounts`.`total_deduct`,`accounts`.`is_manual_verified`, `user`.`awarded_flag`, `user`.`Country`,
                (SELECT admin_logs.admin_id from admin_logs WHERE admin_logs.user_id=withdrawals.unique_id and admin_logs.event='User Verified' ORDER BY admin_logs.id DESC LIMIT 1) as adminid,
                (SELECT name FROM users where u_id = adminid limit 1) as adminusername

                from `withdrawals`
                inner join `user_accounts` as `accounts` on `withdrawals`.`user` = `accounts`.`user_id`
                inner join `users` as `user` on `user`.`id` = `withdrawals`.`user`

                and `user`.`status` = 'active'
                and `withdrawals`.`status` = 'Pending'
                and `withdrawals`.`is_verify` = 0
                        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement("DROP VIEW pendingWithdrawalView");

    }
}
