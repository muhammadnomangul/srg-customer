<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->string('details');
			$table->string('user_id', 50);
			$table->string('user_uid', 50)->nullable();
			$table->string('currency', 100);
			$table->string('amount', 100);
			$table->string('pre_amount', 50);
			$table->string('curr_amount', 50);
			$table->string('approved_by', 50);
			$table->integer('hide_bit')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logs');
	}

}
