<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('country_id')->nullable()->default(0);
			$table->string('country_name', 120)->nullable();
			$table->string('bank_code', 20)->nullable();
			$table->integer('soneri_bank_codes')->nullable();
			$table->integer('finca_bank_codes')->nullable();
			$table->string('bank_name', 200)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banks');
	}

}
