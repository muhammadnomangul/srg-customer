<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referrals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('parent_id')->index('IX_referrals_parent_Id');
			$table->string('parent_u_id', 40)->nullable()->index();
			$table->integer('child_id')->index('IX_referrals_child_id');
			$table->string('child_u_id', 40)->nullable()->index();
			$table->integer('level');
			$table->float('balance', 10, 0)->default(0);
			$table->string('currency_id', 20);
			$table->float('active_new_investment', 10, 0)->default(0);
			$table->float('active_reinvestment', 10, 0)->default(0);
			$table->float('sold_new_investment', 10, 0)->default(0);
			$table->float('sold', 10, 0)->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referrals');
	}

}
