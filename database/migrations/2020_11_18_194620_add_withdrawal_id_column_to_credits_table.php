<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWithdrawalIdColumnToCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cb_credits', function (Blueprint $table) {
            $table->integer('withdrawal_id')->nullable();
            $table->foreign('withdrawal_id')->references('id')->on('withdrawals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cb_credits', function (Blueprint $table) {
            $table->dropColumn('withdrawal_id');
        });
    }
}
