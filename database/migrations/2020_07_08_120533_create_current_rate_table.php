<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrentRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('current_rate', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('rate_usd', 10, 0)->default(1);
			$table->float('rate_btc', 10, 0)->default(0);
			$table->float('rate_eth', 10, 0)->default(0);
			$table->float('rate_ltc', 10, 0)->default(0);
			$table->float('rate_bch', 10, 0)->default(0);
			$table->float('rate_xrp', 10, 0)->default(0);
			$table->float('rate_dash', 10, 0)->default(0);
			$table->float('rate_zec', 10, 0)->default(0);
			$table->float('rate_dsh', 10, 0)->default(0);
			$table->float('today_profit', 10, 0)->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('current_rate');
	}

}
