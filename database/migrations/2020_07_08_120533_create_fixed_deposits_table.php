<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixedDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixed_deposits', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('deposit_id');
			$table->integer('user_id');
			$table->string('user_uid', 100)->nullable();
			$table->float('amount', 10, 0)->default(0);
			$table->float('profit', 10, 0)->default(0);
			$table->string('currency', 15)->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fixed_deposits');
	}

}
