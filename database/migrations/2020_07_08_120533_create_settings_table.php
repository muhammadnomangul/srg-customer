<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('site_name', 50);
			$table->string('description');
			$table->string('currency', 100);
			$table->string('bank_name', 50)->nullable();
			$table->string('account_name', 50)->nullable();
			$table->string('account_number', 20)->nullable();
			$table->string('eth_address', 200)->nullable();
			$table->string('btc_address', 200)->nullable();
			$table->string('usd_address')->nullable();
			$table->string('bch_address', 200)->nullable();
			$table->string('ltc_address', 200)->nullable();
			$table->string('xrp_address', 200)->nullable();
			$table->string('dash_address', 150)->nullable();
			$table->string('zec_address', 150)->nullable();
			$table->string('dsh_address', 150)->nullable();
			$table->string('s_s_k', 200)->nullable();
			$table->string('s_currency', 20)->nullable();
			$table->string('s_p_k', 200)->nullable();
			$table->string('pay_key', 200)->nullable();
			$table->string('pay_key2', 200)->nullable();
			$table->string('payment_mode', 100);
			$table->string('keywords');
			$table->string('site_title', 100);
			$table->string('site_address', 100);
			$table->string('logo', 200);
			$table->string('trade_mode', 20)->nullable();
			$table->string('contact_email', 50)->nullable();
			$table->string('referral_commission', 20)->nullable();
			$table->float('deposit_limit', 10, 0)->default(0);
			$table->float('withdraw_limit', 10, 0)->default(0);
			$table->float('reinvest_limit', 10, 0)->default(0);
			$table->string('update');
			$table->float('rate_pkr', 10, 0);
			$table->timestamps();
			$table->string('deposit_fee')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
