<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DepositView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement("CREATE VIEW depositView AS
                  select `deposits`.*, `users`.`Country`, `users`.`name`, `banks`.`bank_name` from `deposits` 
                  inner join `users` on `users`.`id` = `deposits`.`user_id` 
                  left join `banks` on `deposits`.`bank_id` = `banks`.`id`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement("DROP VIEW depositView");

    }
}
