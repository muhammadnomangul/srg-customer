<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserToDailyProfitBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_profit_bonus', function (Blueprint $table) {
            //
                $table->integer('user')->nullable()->after('user_id')->default(null);
                $table->index('user');
                $table->foreign('user')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_profit_bonus', function (Blueprint $table) {
            //
            $table->dropIndex('user');

            $table->dropColumn('user');


        });
    }
}
