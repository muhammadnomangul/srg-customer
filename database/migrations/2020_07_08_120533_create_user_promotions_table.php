<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserPromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_promotions', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->text('user_id', 65535);
			$table->text('user_u_id', 65535);
			$table->text('name', 65535);
			$table->text('country', 65535);
			$table->text('plan', 65535);
			$table->float('level_1')->nullable();
			$table->float('level_2')->nullable();
			$table->float('level_3')->nullable();
			$table->float('level_4')->nullable();
			$table->float('level_5')->nullable();
			$table->float('total_investment')->nullable();
			$table->date('from');
			$table->date('to');
			$table->text('prize', 65535);
			$table->date('prize_date');
			$table->text('remarks', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_promotions');
	}

}
