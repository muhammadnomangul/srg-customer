<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinStrPersonalInvesmentIntoNewPlanLimits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_plan_limits',function (Blueprint $table){
            $table->float('structural_personal_min', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_plan_limits', function (Blueprint $table) {
            $table->dropColumn('structural_personal_min');
        });
    }
}
