<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('u_id', 60)->unique('u_id');
			$table->string('parent_id', 60)->index('IX_users_parent_id');
			$table->string('name', 50);
			$table->string('email', 50);
			$table->string('phone_no', 25)->nullable();
			$table->string('Country', 100);
			$table->string('photo')->nullable();
			$table->string('old_pass', 50)->nullable();
			$table->string('password');
			$table->string('remember_token', 100)->nullable();
			$table->text('bank_acc_info')->nullable();
			$table->text('kin_bank_info')->nullable();
			$table->string('bank_name', 25)->nullable();
			$table->string('account_name', 50)->nullable();
			$table->string('account_no', 50)->nullable();
			$table->string('acc_hold_No', 50)->nullable();
			$table->string('btc_address')->nullable();
			$table->string('eth_address')->nullable();
			$table->string('bch_address')->nullable();
			$table->string('ltc_address')->nullable();
			$table->string('xrp_address')->nullable();
			$table->string('dash_address', 150)->nullable();
			$table->string('zec_address', 150)->nullable();
			$table->string('dsh_address', 150)->nullable();
			$table->integer('plan')->nullable()->default(1);
			$table->string('rank')->nullable();
			$table->float('last_profit', 10, 0)->default(0);
			$table->float('last_bonus', 10, 0)->nullable()->default(0);
			$table->string('ref_link', 100)->nullable();
			$table->string('status', 25)->nullable()->default('blocked');
			$table->string('status_remarks')->nullable();
			$table->integer('awarded_flag')->default(0);
			$table->string('type', 25)->nullable()->default('0');
			$table->boolean('is_super_admin')->default(0);
			$table->boolean('post_check')->default(0);
			$table->integer('is_subscribe')->default(1);
			$table->boolean('is_email_pin')->default(0);
			$table->string('email_pin')->nullable();
			$table->boolean('is_fixed_deposit')->default(0);
			$table->date('fixed_deposit_date')->nullable();
			$table->string('sms_varify_code', 50);
			$table->text('msg', 65535)->nullable();
			$table->integer('msg_bit')->default(0);
			$table->timestamps();
			$table->boolean('show_donation_popup')->default(0);
			$table->boolean('donate_odp')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
