<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepositIdColumnToDebitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cb_debits', function (Blueprint $table) {
            $table->integer('deposits_id')->nullable();
            $table->foreign('deposits_id')->references('id')->on('deposits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cb_debits', function (Blueprint $table) {
            $table->dropColumn('deposits_id');
        });
    }
}
