<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopInvestorsTmpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('top_investors_tmp', function(Blueprint $table)
		{
			$table->integer('parent_id');
			$table->string('u_id', 60);
			$table->string('name', 50);
			$table->string('email', 50);
			$table->string('phone_no', 25)->nullable();
			$table->string('Country', 100);
			$table->integer('plan')->nullable()->default(1);
			$table->float('ChildAmount', 10, 0)->nullable();
			$table->integer('level')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('top_investors_tmp');
	}

}
