<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOldCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('old_currency', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('dollar_price', 10, 0);
			$table->float('ethereum_price', 10, 0);
			$table->float('bitcoin_price', 10, 0);
			$table->float('daily_profit', 10, 0);
			$table->float('daily_bounce', 10, 0);
			$table->float('profit_percentage1', 10, 0);
			$table->float('profit_percentage2', 10, 0);
			$table->text('payment_account_bitcoin_link', 65535);
			$table->text('payment_account_etherum_link', 65535);
			$table->text('payment_account_dollar_link', 65535);
			$table->string('date_time', 24);
			$table->integer('holiday');
			$table->integer('block');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('old_currency');
	}

}
