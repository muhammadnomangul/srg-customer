<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOtpTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('otp_tokens', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('code');
			$table->integer('user_id')->unsigned();
			$table->boolean('used')->default(0);
			$table->timestamps();
			$table->string('otp_type')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('otp_tokens');
	}

}
