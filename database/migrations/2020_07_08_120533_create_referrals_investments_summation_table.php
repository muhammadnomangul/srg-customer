<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralsInvestmentsSummationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referrals_investments_summation', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('child_id')->index('IX_referrals_investments_summation_child_id');
			$table->string('child_u_id', 40)->nullable()->index('IX_referrals_investments_summation_child_u_id');
			$table->float('balance', 10, 0)->default(0);
			$table->string('currency_id', 20)->nullable();
			$table->float('active_new_investment', 10, 0)->default(0);
			$table->float('active_reinvestment', 10, 0)->default(0);
			$table->float('sold_new_investment', 10, 0)->default(0);
			$table->float('sold', 10, 0)->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referrals_investments_summation');
	}

}
