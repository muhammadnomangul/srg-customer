<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFixDepositHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fix_deposit_histories', function(Blueprint $table)
		{
			$table->foreign('deposit_id')->references('id')->on('deposits')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('plan_id')->references('id')->on('fix_deposit_tenures')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fix_deposit_histories', function(Blueprint $table)
		{
			$table->dropForeign('fix_deposit_histories_deposit_id_foreign');
			$table->dropForeign('fix_deposit_histories_plan_id_foreign');
			$table->dropForeign('fix_deposit_histories_user_id_foreign');
		});
	}

}
