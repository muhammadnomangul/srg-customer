<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReverseWithdrawal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared("
    
CREATE  PROCEDURE `reverse_withdrawal`(deposit_id INT, logged_in_user_id INT)
reverse_withdrawal_proc:
        BEGIN
            SET @logged_in_user_id = logged_in_user_id;
            SET @deposit_id = deposit_id;
            SET @withdraw_id = 0;
            SET @deposit_payment_mode = '';
            
            SELECT withdrawal_id, payment_mode INTO @withdraw_id, @deposit_payment_mode FROM deposits WHERE id = @deposit_id;
            
            -- add condition here if not fund transfer then return
            IF @deposit_payment_mode <> 'FundTransfer' OR IFNULL(@withdraw_id, 0) = 0
            THEN
                LEAVE reverse_withdrawal_proc;
            END IF;
        
            SET @transfer_amount = 0;
            SET @w_payment_mode = ''; -- Withdrawal payment mode
            SET @currency = '';
            SET @w_user_id = 0; -- withdrawal user id
            
            SELECT amount, payment_mode, currency, user INTO @transfer_amount, @w_payment_mode, @currency, @w_user_id
            FROM withdrawals where id = @withdraw_id;
            
            SET @plan = 0;
            SET @user_u_id = '';
        
            SELECT u_id, plan INTO @user_u_id, @plan FROM users WHERE id = @w_user_id;
            
            SET @msg = CONCAT(@user_u_id, ' has reinvested for withdrawal reversal ', @transfer_amount, ' (', @currency, ')');
            
            -- getting previous balance for user logs
            SET @user_balance = (SELECT CASE 
                                    WHEN @currency = 'USD' THEN balance_usd
                                    WHEN @currency = 'BTC' THEN balance_btc
                                    WHEN @currency = 'ETH' THEN balance_eth 
                                    WHEN @currency = 'BCH' THEN balance_bch
                                    WHEN @currency = 'LTC' THEN balance_ltc
                                    WHEN @currency = 'XRP' THEN balance_xrp
                                    WHEN @currency = 'DASH' THEN balance_dash
                                    ELSE balance_zec END
                                    FROM user_accounts where user_id = @w_user_id);
                                    
            CREATE TEMPORARY TABLE IF NOT EXISTS user_balances
            (
                user_id INT,
                bal_usd float,
                bal_btc float,
                bal_eth float,
                bal_bch float,
                bal_ltc float,
                bal_xrp float,
                bal_dash float,
                bal_zec float
            );
            
            SET @Conversion_rate = (SELECT CASE 
                                WHEN @currency = 'USD' THEN rate_usd
                                WHEN @currency = 'BTC' THEN rate_btc
                                WHEN @currency = 'ETH' THEN rate_eth 
                                WHEN @currency = 'BCH' THEN rate_bch
                                WHEN @currency = 'LTC' THEN rate_ltc
                                WHEN @currency = 'XRP' THEN rate_xrp
                                WHEN @currency = 'DASH' THEN rate_dash
                                ELSE rate_zec END
                                FROM currency_rates ORDER BY created_at DESC LIMIT 1);
                                
            SET autocommit = 0;
            START TRANSACTION;
            
            -- update withdrawal to cancel
            UPDATE withdrawals SET pre_status = status, status = 'Cancelled', updated_at = current_timestamp()
            WHERE id = @withdraw_id;
            
            IF @w_payment_mode = 'Bonus'
            THEN
                -- update reference_bonus for parents in user accounts
                UPDATE user_accounts
                SET reference_bonus = reference_bonus + IFNULL(@transfer_amount, 0),
                updated_at = CURRENT_TIMESTAMP()
                WHERE user_id = @w_user_id;
            END IF;
            
            IF @w_payment_mode = 'sold'
            THEN
                
                insert into logs (title, details, user_id, user_uid, currency, amount, pre_amount, curr_amount, approved_by, updated_at, created_at) 
                SELECT 'WithDrawal Reverted & Reinvested', @msg, @w_user_id, @user_u_id, @currency, @transfer_amount, @user_balance, @user_balance + @transfer_amount, @logged_in_user_id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP();
        
                insert into deposits (amount, payment_mode, currency, rate, total_amount, plan, user_id, unique_id, status, pre_status, flag_dummy, profit_take, approved_at, trade_profit, trans_id, trans_type, reinvest_type, updated_at, created_at) 
                values (@transfer_amount, 'Bank transfer', @currency, @Conversion_rate, @transfer_amount * @Conversion_rate, @plan, @w_user_id, @user_u_id, 'New', 'Approved', 0, 0, NULL, 0, 'reinvest', 'Reinvestment', 'Sold', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
                
                CALL approve_deposit (LAST_INSERT_ID(), @logged_in_user_id, 0 , CURRENT_TIMESTAMP());
                                    
            END IF;
            
            IF @w_payment_mode = 'Profit'
            THEN
                
                DELETE FROM user_balances;
                
                INSERT INTO user_balances
                SELECT
                    user_id,
                    CASE WHEN @currency = 'USD' THEN IFNULL(profit_usd, 0) + @transfer_amount ELSE profit_usd END profit_usd,
                    CASE WHEN @currency = 'BTC' THEN IFNULL(profit_btc, 0) + @transfer_amount ELSE profit_btc END profit_btc,
                    CASE WHEN @currency = 'ETH' THEN IFNULL(profit_eth, 0) + @transfer_amount ELSE profit_eth END profit_eth,    
                    CASE WHEN @currency = 'BCH' THEN IFNULL(profit_bch, 0) + @transfer_amount ELSE profit_bch END profit_bch,    
                    CASE WHEN @currency = 'LTC' THEN IFNULL(profit_ltc, 0) + @transfer_amount ELSE profit_ltc END profit_ltc,
                    CASE WHEN @currency = 'XRP' THEN IFNULL(profit_xrp, 0) + @transfer_amount ELSE profit_xrp END profit_xrp,  
                    CASE WHEN @currency = 'DASH' THEN IFNULL(profit_dash, 0) + @transfer_amount ELSE profit_dash END profit_dash,    
                    CASE WHEN @currency = 'ZEC' THEN IFNULL(profit_zec, 0) + @transfer_amount ELSE profit_zec END profit_zec
                FROM user_accounts
                WHERE user_id = @w_user_id;
                
                UPDATE user_accounts AS a 
                INNER JOIN user_balances AS d ON d.user_id = a.user_id
                SET a.profit_usd = d.bal_usd,
                a.profit_btc = d.bal_btc,
                a.profit_eth = d.bal_eth,
                a.profit_bch = d.bal_bch,
                a.profit_ltc = d.bal_ltc,
                a.profit_xrp = d.bal_xrp,
                a.profit_dash = d.bal_dash,
                a.profit_zec = d.bal_zec,
                a.updated_at = CURRENT_TIMESTAMP();
                                    
            END IF;
            
            COMMIT;
        
            DROP TEMPORARY TABLE IF EXISTS user_balances;
            SELECT 'Done' AS Result;
        END;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS reverse_withdrawal;');

        //
    }
}
