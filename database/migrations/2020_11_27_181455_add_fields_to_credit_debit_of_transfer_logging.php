<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToCreditDebitOfTransferLogging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cb_debits', function (Blueprint $table) {
            $table->integer('transfer_to_cash_box_account_id')->nullable();
            $table->foreign('transfer_to_cash_box_account_id')->references('id')->on('cb_accounts');

            $table->integer('transfer_to_user_id')->nullable();
            $table->foreign('transfer_to_user_id')->references('id')->on('users');
        });

        Schema::table('cb_credits', function (Blueprint $table) {
            $table->integer('came_from_cb_account_id')->nullable();
            $table->foreign('came_from_cb_account_id')->references('id')->on('cb_accounts');

            $table->integer('came_from_user_id')->nullable();
            $table->foreign('came_from_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cb_credits', function (Blueprint $table) {
            $table->dropForeign([
                'came_from_cb_account_id',
                'came_from_user_id',
            ]);
            $table->dropColumn([
                'came_from_cb_account_id',
                'came_from_user_id',
            ]);

        });
        Schema::table('cb_debits', function (Blueprint $table) {
            $table->dropForeign([
                'transfer_to_cash_box_account_id',
                'transfer_to_user_id',
            ]);
            $table->dropColumn([
                'transfer_to_cash_box_account_id',
                'transfer_to_user_id',
            ]);

        });
    }
}
