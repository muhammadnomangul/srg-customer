<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_charges_transaction', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('subscription_id')->nullable();
            $table->integer('transaction_media_id')->nullable();
            $table->integer('transaction_media_type')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_charges_transaction', function (Blueprint $table) {
            //
        });
    }
}
