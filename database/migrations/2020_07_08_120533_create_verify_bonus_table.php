<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVerifyBonusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('verify_bonus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('trade_id');
			$table->string('user', 100);
			$table->integer('amount');
			$table->string('type', 100);
			$table->string('status', 50);
			$table->string('currency', 15);
			$table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('verify_bonus');
	}

}
