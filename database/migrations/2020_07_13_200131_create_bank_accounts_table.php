<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('bank_name', 100)->nullable();
            $table->string('account_title', 100)->nullable();
            $table->string('account_number', 100)->nullable();
            $table->string('account_number1', 100)->nullable();
            $table->string('bank_branch_code', 20)->nullable();
            $table->string('currency', 5)->nullable();
            $table->integer('country_id')->nullable();
            $table->string('address')->nullable();
            $table->tinyInteger('is_hide')->default(0);
            $table->timestamps();
        });

        Schema::table('bank_accounts', function (Blueprint $blueprint) {
            $blueprint->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
