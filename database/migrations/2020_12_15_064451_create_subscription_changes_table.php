<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {//cashbox debit id
        // // withdrawal id
        Schema::create('subscription_changes', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('user_id')->nullable()->nullable();
            $table->integer('cashbox_debit_id')->nullable()->nullable();
            $table->integer('withdrawal_id')->nullable()->nullable();
            $table->integer('plan_id')->nullable()->nullable();
            $table->float('amount', 10, 0)->default(0);
            $table->string('currency', 20)->nullable();
            $table->string('payment_mode', 20)->nullable();
            $table->string('package_type', 20)->nullable();
            $table->float('count', 20,0)->default(1);
            $table->boolean('is_exp')->default(0);
            $table->string('remarks')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_changes');
    }
}
