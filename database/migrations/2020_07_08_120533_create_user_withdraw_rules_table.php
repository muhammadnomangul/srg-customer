<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWithdrawRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_withdraw_rules', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('user_id')->index('user_withdraw_rules_user_id_foreign');
			$table->enum('can_take_profit', array('0','1'))->default('0')->comment('0 for can take and 1 for cannot take');
			$table->enum('can_take_bonus', array('0','1'))->default('0')->comment('0 for can take and 1 for cannot take');
			$table->enum('can_take_sold', array('0','1'))->default('0')->comment('0 for can take and 1 for cannot take');
			$table->enum('can_take_withdraw', array('0','1'))->default('0')->comment('0 for can take and 1 for cannot take');
			$table->text('remarks', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_withdraw_rules');
	}

}
