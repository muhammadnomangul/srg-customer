<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CashBoxCredit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cb_credits', function (Blueprint $table) {
            $table->integer('id')->primary()->autoIncrement();

            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('cb_account_id')->index();
            $table->foreign('cb_account_id')->references('id')->on('cb_accounts');

            $table->float('amount');
            $table->float('fee');

            $table->integer('currencies_id')->index();
            $table->foreign('currencies_id')->references('id')->on('currencies');


            $table->smallInteger('category')->nullable();
            $table->smallInteger('type')->nullable();
            $table->string('reason')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cb_credits');
    }
}
