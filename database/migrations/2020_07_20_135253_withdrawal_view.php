<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WithdrawalView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement("CREATE VIEW withdrawalView AS
        
                select `withdrawals`.`id`, `withdrawals`.`unique_id`, `withdrawals`.`amount`, `accounts`.`total_deduct`, `withdrawals`.`status`, `withdrawals`.`payment_mode`,
                 `withdrawals`.`currency`, `withdrawals`.`fund_type` , `withdrawals`.`verified_at`, `withdrawals`.`user`, `users`.`Country`, `withdrawals`.`is_verify`,
                 `withdrawals`.`is_manual_cancel`, `withdrawals`.`created_at`, `withdrawals`.`flag_dummy`,`withdrawals`.`is_paid`, `withdrawals`.`pre_status`,
                 `withdrawals`.`bank_reference_id` , `withdrawals`.`trans_id`
                 from withdrawals
                 left join users on `withdrawals`.`user` = `users`.`id` 
                 left join user_accounts AS accounts on `withdrawals`.`user` = `accounts`.`user_id`
                        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement("DROP VIEW withdrawalView");

    }
}
