<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixDepositTenuresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fix_deposit_tenures', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('tenure');
			$table->timestamps();
			$table->enum('status', array('active','disabled'))->default('active');
			$table->string('tenure_length');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fix_deposit_tenures');
	}

}
