<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CalculateInvestmentBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
    
CREATE  PROCEDURE `calculateInvestmentBonus`( IN date_from Date,IN date_to Date,OUT total_investment_bonus DECIMAL(18,2))
BEGIN  SELECT round(sum(bonus),2)
        INTO total_investment_bonus
        FROM daily_investment_bonus where created_at >= date_from And       
        created_at <= date_to; SELECT total_investment_bonus; 
        END;
        
        ");

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP PROCEDURE IF EXISTS calculateInvestmentBonus;');
    }
}
