<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_accounts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index();
			$table->float('latest_bonus', 10, 0)->default(0);
			$table->float('b_reference_bonus', 10, 0)->default(0);
			$table->float('reference_bonus', 10, 0)->default(0);
			$table->float('ref_bonus2', 10, 0)->nullable()->default(0);
			$table->float('old_ref_bonus', 10, 0)->nullable()->default(0);
			$table->float('latest_profit', 10, 0)->default(0);
			$table->float('balance_usd', 10, 0)->default(0);
			$table->float('b_profit_usd', 10, 0)->nullable();
			$table->float('profit_usd', 10, 0)->default(0);
			$table->float('f_profit_usd', 10, 0)->nullable();
			$table->string('new_profit_usd')->nullable();
			$table->float('new_withdrawal_usd', 10, 0)->nullable();
			$table->float('old_profit_usd', 10, 0)->default(0);
			$table->float('waiting_profit_usd', 10, 0)->default(0);
			$table->float('sold_bal_usd', 10, 0)->default(0);
			$table->float('balance_btc', 10, 0)->default(0);
			$table->float('b_profit_btc', 10, 0)->default(0);
			$table->float('profit_btc', 10, 0)->default(0);
			$table->float('f_profit_btc', 10, 0)->nullable();
			$table->string('new_profit_btc')->nullable();
			$table->float('new_withdrawal_btc', 10, 0)->nullable();
			$table->float('old_profit_btc', 10, 0)->default(0);
			$table->float('waiting_profit_btc', 10, 0)->default(0);
			$table->float('sold_bal_btc', 10, 0)->default(0);
			$table->float('balance_eth', 10, 0)->default(0);
			$table->float('b_profit_eth', 10, 0)->default(0);
			$table->float('profit_eth', 10, 0)->default(0);
			$table->float('f_profit_eth', 10, 0)->nullable();
			$table->float('new_profit_eth', 10, 0)->nullable();
			$table->float('new_withdrawal_eth', 10, 0)->nullable();
			$table->float('old_profit_eth', 10, 0)->default(0);
			$table->float('waiting_profit_eth', 10, 0)->default(0);
			$table->float('sold_bal_eth', 10, 0)->default(0);
			$table->float('balance_bch', 10, 0)->default(0);
			$table->float('profit_bch', 10, 0)->default(0);
			$table->float('f_profit_bch', 10, 0)->nullable();
			$table->float('new_profit_bch', 10, 0)->nullable();
			$table->float('new_withdrawal_bch', 10, 0)->nullable();
			$table->float('waiting_profit_bch', 10, 0)->default(0);
			$table->float('sold_bal_bch', 10, 0)->default(0);
			$table->float('balance_ltc', 10, 0)->default(0);
			$table->float('profit_ltc', 10, 0)->default(0);
			$table->float('f_profit_ltc', 10, 0)->nullable();
			$table->float('new_profit_ltc', 10, 0)->nullable();
			$table->float('new_withdrawal_ltc', 10, 0)->nullable();
			$table->float('waiting_profit_ltc', 10, 0)->default(0);
			$table->float('sold_bal_ltc', 10, 0)->default(0);
			$table->float('balance_xrp', 10, 0)->default(0);
			$table->float('profit_xrp', 10, 0)->default(0);
			$table->float('f_profit_xrp', 10, 0)->nullable();
			$table->float('new_profit_xrp', 10, 0)->nullable();
			$table->float('new_withdrawal_xrp', 10, 0)->nullable();
			$table->float('waiting_profit_xrp', 10, 0)->default(0);
			$table->float('sold_bal_xrp', 10, 0)->default(0);
			$table->float('balance_dash', 10, 0)->default(0);
			$table->float('profit_dash', 10, 0)->default(0);
			$table->float('f_profit_dash', 10, 0)->nullable();
			$table->float('new_profit_dash', 10, 0)->nullable();
			$table->float('new_withdrawal_dash', 10, 0)->nullable();
			$table->float('waiting_profit_dash', 10, 0)->default(0);
			$table->float('sold_bal_dash', 10, 0)->default(0);
			$table->float('balance_zec', 10, 0)->default(0);
			$table->float('profit_zec', 10, 0)->default(0);
			$table->float('f_profit_zec', 10, 0)->nullable();
			$table->float('new_profit_zec', 10, 0)->nullable();
			$table->float('new_withdrawal_zec', 10, 0)->nullable();
			$table->float('waiting_profit_zec', 10, 0)->default(0);
			$table->float('sold_bal_zec', 10, 0)->default(0);
			$table->float('balance_dsh', 10, 0)->default(0);
			$table->float('profit_dsh', 10, 0)->default(0);
			$table->float('waiting_profit_dsh', 10, 0)->default(0);
			$table->float('sold_bal_dsh', 10, 0)->default(0);
			$table->float('total_deduct', 10, 0)->default(0);
			$table->boolean('is_manual_verified')->default(0);
			$table->integer('double_verified')->default(0);
			$table->boolean('is_maual_old_changed')->default(0);
			$table->timestamps();
			$table->boolean('is_report_change')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_accounts');
	}

}
