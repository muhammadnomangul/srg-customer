<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CashBoxAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cb_accounts', function (Blueprint $table) {
            $table->integer('id')->primary()->autoIncrement();

            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('name', 40);

            $table->integer('currencies_id')->index();
            $table->foreign('currencies_id')->references('id')->on('currencies');

            $table->float('current_balance');
            $table->float('locked_balance');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cb_accounts');
    }
}
