<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('withdrawals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('user', 20)->index();
			$table->string('unique_id', 20)->nullable()->index();
			$table->string('amount', 20);
			$table->string('status', 20);
			$table->string('pre_status', 20)->nullable();
			$table->string('payment_mode', 20);
			$table->float('crypto_amount', 10, 0)->nullable()->default(0);
			$table->float('usd_amount', 10, 0)->nullable()->default(0);
			$table->float('pre_amount', 10, 0)->default(0);
			$table->string('currency', 20)->nullable()->index();
			$table->float('withdrawal_fee', 10, 0)->default(0);
			$table->float('donation', 10, 0)->default(0);
			$table->string('new_type', 100)->nullable();
			$table->string('fund_type', 100)->nullable();
			$table->string('fund_receivers_id', 60)->nullable();
			$table->integer('paid_flag')->default(0);
			$table->integer('flag_dummy')->default(0);
			$table->integer('is_verify')->default(0);
			$table->dateTime('verified_at')->nullable();
			$table->string('verified_by', 50)->nullable();
			$table->string('approved_by', 50)->nullable();
			$table->dateTime('approved_at')->nullable();
			$table->string('paid_by', 50)->nullable();
			$table->dateTime('paid_at')->nullable();
			$table->integer('is_paid')->default(0);
			$table->string('bank_reference_id')->nullable();
			$table->string('paid_img')->nullable();
			$table->boolean('is_manual_cancel')->default(0);
			$table->timestamps();
			$table->integer('created_by')->nullable()->index('withdrawals_created_by_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('withdrawals');
	}

}
