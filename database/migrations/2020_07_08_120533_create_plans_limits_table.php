<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansLimitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans_limits', function(Blueprint $table)
		{
			$table->integer('plan_id')->primary();
			$table->string('name', 20);
			$table->float('personal_min', 10, 0);
			$table->float('personal_max', 10, 0);
			$table->float('structural_min', 10, 0);
			$table->float('structural_max', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans_limits');
	}

}
