@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1"> {{$title}} </h3>
                <a class="btn btn-default" href="{{ url('dashboard/emailTemplateList')}}">List All Email Templates</a>
                @include('messages')


				<div class="sign-up-row widget-shadow">
					
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('dashboard/updateEmailTemp') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2 control-label">Email Title</label>

                            <div class="col-md-6">
                                <input id="titleid" type="text" class="form-control" name="title" value="{{ $emailTemplates->title}}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">Message</label>

                            <div class="col-md-6">
                                <input id="message" type="text" class="form-control" name="message" value="{{ $emailTemplates->message}}">

                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('from_email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">From Email</label>

                            <div class="col-md-6">
                                <input id="from_email" type="text" class="form-control" name="from_email" value="{{ $emailTemplates->from_email}}">
                               
                                @if ($errors->has('from_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('from_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="col-md-2 control-label">Email Body</label>

                            <div class="col-md-10">
                               
                                <textarea name="body" id="editor" class="ckeditor" height = "300" >{{html_entity_decode($emailTemplates->body) }}</textarea>
                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      
                      <input type="hidden" name="mail_id" value="{{ $emailTemplates->id}}">
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary submit1">
                                    <i class="fa fa-btn fa-envelope"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>
                    
				</div>
			</div>
		</div>
        <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );

            $(".submit1").click(function(){
                    //	alert(" i am here");
                var conf = confirm("Do you really want to proceed ?");
                if(conf){
                    return true;
                }
                else{
                    return false;
                }
            });

         /*    CKEDITOR.editorConfig = function( config ) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.height = 300;
            config.toolbarCanCollapse = true;
        }; */
     </script>
     
		@include('footer')