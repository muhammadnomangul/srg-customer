@include('header')

		<!-- //header-ends -->
		<!-- main content start-->
<style>

.loader-overlay{
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.90);
    z-index: 999999;
}
.loader-overlay .inner-loader{
    display: table;
    height: 100%;
    width: 100%;
}
.inner-loader > div{
    background-repeat: no-repeat;
    background-position: center;
    display: table-cell;
    vertical-align: middle;
    text-align: center;
}
  /*   .reload_img {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
} */
 </style>  
	<div id="page-wrapper">
            <div id="reload_img" class="loader-overlay" style="display:none;">
                <div class="inner-loader">
                    <div  style="background-image:url({{asset('images/loading-bar.gif')}});"></div>
                </div>
            </div>
          
		<div class="main-page">
            @include('messages')               
			<div class="row widget-shadow">
				{{-- @if(Auth::user()->id =='1' || Auth::user()->id =='1013'|| Auth::user()->id =='5429') --}}
                @if(Auth::user()->is_user(SITE_SUPER_ADMIN))
                <h3 class="title1" align="center">Add Exchange Rates </h3>
               
                    <form class="form-horizontal" id="myform" role="form" method="POST" action="{{ url('dashboard/postrates') }}">
                        {{ csrf_field() }}
						<div class="form-group">
							<label for="bitcoin" class="col-md-4 control-label">Click To Get Latest Curreny Rates</label>
							<div class="col-md-4">
                            <a class="btn btn-primary" style="align:center" href="#" onclick="todayRates();" > Get Exchange Rates </a>
                            </div>
						</div>
                        <div class="form-group">
                            <label for="bitcoin" class="col-md-2 control-label">Bitcoin Rate</label>

                            <div class="col-md-3">
                                <input id="bitcoin" type="text" class="form-control" name="rate_btc" value="{{ old('rate_btc') }}" reqiured>

                                @if ($errors->has('rate_btc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_btc') }}</strong>
                                    </span>
                                @endif
                            </div>
							<label for="ethereum" class="col-md-2 control-label">Ethereum Rate</label>

                            <div class="col-md-3">
                                <input id="ethereum" type="text" class="form-control" name="rate_eth" value="{{ old('rate_eth') }}" reqiured>

                                @if ($errors->has('rate_eth'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_eth') }}</strong>
                                    </span>
                                @endif
                            </div>
                           
                        </div>

                        <div class="form-group">
                           
							<label for="bitcash" class="col-md-2 control-label">Bitcash Rate</label>
                            <div class="col-md-3">
                                <input id="bitcash" type="text" class="form-control" name="rate_bch" value="{{ old('rate_bch') }}" reqiured>

                                @if ($errors->has('rate_bch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_bch') }}</strong>
                                    </span>
                                @endif
                            </div>
							 <label for="litecoin" class="col-md-2 control-label">Litecoin Rate</label>

                            <div class="col-md-3">
                                <input id="litecoin" type="text" class="form-control" name="rate_ltc" value="{{ old('rate_ltc') }}" reqiured>

                                @if ($errors->has('rate_ltc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_ltc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
						
						<div class="form-group">
                           
							<label for="dash" class="col-md-2 control-label">DASH Rate</label>
                            <div class="col-md-3">
                                <input id="dash" type="text" class="form-control" name="rate_dash" value="{{ old('rate_dash') }}" reqiured>

                                @if ($errors->has('rate_dash'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_dash') }}</strong>
                                    </span>
                                @endif
                            </div>
							<label for="ripple" class="col-md-2 control-label">Ripple Rate</label>
                            <div class="col-md-3">
                                <input id="ripple" type="text" class="form-control" name="rate_xrp" value="{{ old('rate_xrp') }}" reqiured>

                                @if ($errors->has('rate_xrp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_xrp') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                        </div>
						
						
						<div class="form-group">
							<label for="profit" class="col-md-2 control-label">Today Profit</label>

                            <div class="col-md-3">
                                <input id="today_profit" name="today_profit" type="text" class="form-control"  value="{{ old('today_profit') }}" reqiured>

                                @if ($errors->has('today_profit'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('today_profit') }}</strong>
                                    </span>
                                @endif
                            </div>
							 <label for="zcash" class="col-md-2 control-label">Zcash Rate</label>

                            <div class="col-md-3">
                                <input id="zcash" type="text" class="form-control" name="rate_zec" value="{{ old('rate_zec') }}" reqiured>

                                @if ($errors->has('rate_zec'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rate_zec') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                        </div>
						<!--<div class="form-group">
							<label for="dashcoin" class="col-md-2 control-label">Dashcoin</label>

                            <div class="col-md-3">
                                <input id="dashcoin" name="dashcoin" type="text" class="form-control"  value="{{ old('dashcoin') }}" reqiured>

                                @if ($errors->has('dashcoin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dashcoin') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                        </div>-->
                        <div class="form-group">
                            <div class="col-md-2">
                               
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
							 <div class="col-md-3">
                               
                            </div>
                            <div class="col-md-3">
                                <button type="reset" class="btn btn-primary" onclick="clear()">
                                Clear
                                </button>
                            </div>
                        </div>
                    </form>
                    <div>
                        <table id="myTable2" class="table table-hover">
                            <thead>
                            <tr>

                                <th>Bitcoin</th>
                                <th>Ethereum</th>
                                <th>Litecoin</th>
                                <th>Bitcash</th>
                                <th>Ripple</th>
                                <th>Dash</th>
                                <th>Zcash</th>
                                <!--<th>Dashcoin</th>-->
                                <th>Profit %</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>


                            <tr>

								@if(isset($currentRates))
                                <td>{{$currentRates->rate_btc}}</td>

                                <td>{{$currentRates->rate_eth}}</td>

                                <td>{{$currentRates->rate_ltc}}</td>

                                <td>{{$currentRates->rate_bch}}</td>

                                <td>{{$currentRates->rate_xrp}}</td>

                                <td>{{$currentRates->rate_dash}}</td>

                                <td>{{$currentRates->rate_zec}}</td>

                                <td>{{$currentRates->today_profit}}%</td>

                                <td>{{date('Y-m-d', strtotime($currentRates->updated_at))}}</td>
								@else
								<td colspan="9" style="text-align:center;">Current Rates Not Found.</td>
								@endif
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div width="80%" class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
						<div width="100%" style="text-align:center;">
							<div><h3 class="title" style="text-align:center;"><font color="red" align="center"> Latest Rates</font></h3><div>
                                    @if(Auth::user()->id =='1' || Auth::user()->id =='1013' )
							<div style="text-align:right;">
    <!--<a  class="btn btn-danger" href="{{ url('dashboard/calculateProfit')}}">Calculate Profit<a>-->
                        <a  class="btn btn-danger" href="{{ url('dashboard/updateRates')}}">Weekend Update Rates<a>
                            </div>
						</div>
                         @endif
                          @endif
							<table id="myTable" class="table table-hover"> 
								<thead> 
									<tr> 
										<th>Sr#</th>
										<th>Bitcoin</th>
										<th>Ethereum</th>
										<th>Litecoin</th> 
										<th>Bitcash</th>
										<th>Ripple</th>
										<th>Dash</th> 
										<th>Zcash</th>
										<!--<th>Dashcoin</th>-->
										<th>Profit %</th>
										<th>Date</th>
									</tr>
								</thead> 
								<tbody> 
								<?php $count = 1; ?>
								@foreach($ratesQueryResults as $ratesQuery)
									<tr>
								
									<td>{{$count}}</td>
									<td>{{$ratesQuery->rate_btc}}</td>
									
									<td>{{$ratesQuery->rate_eth}}</td>
								
									
									<td>{{$ratesQuery->rate_ltc}}</td>
									
									
									<td>{{$ratesQuery->rate_bch}}</td>
									
									<td>{{$ratesQuery->rate_xrp}}</td>
									
									<td>{{$ratesQuery->rate_dash}}</td>
									
									<td>{{$ratesQuery->rate_zec}}</td>
										
									<!--<td>{{$ratesQuery->rate_dsh}}</td>-->
									
									<td>{{$ratesQuery->today_profit}}%</td>
									
									<td>{{date('Y-m-d', strtotime($ratesQuery->created_at))}}</td>
									
									</tr>
								<?php $count++; ?>
								@endforeach
								</tbody> 
							</table>
					   </div>
					</div>
				</div>
			</div>
		</div>

<script>
$(document).ready( function () {
	$('#myTable').DataTable();
    $('#myTable2').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "bFilter": false
    });
} );
function clear() {
    document.getElementById("myform").reset();
}
// Search Today Rates
	function todayRates()
	{
        // var myVar;
        // myVar = setTimeout(showPage, 3000);
      jQuery('#reload_img').show();
	    $.ajax({
			type: "get",
			url: '{{{URL::to("")}}}/dashboard/getRatesOnline',
			//url: 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,BCH,LTC,XRP&tsyms=USD',
			dataType: 'html',
			data: {"_token": "{{ csrf_token() }}",},
			success: function(data) {
               
                jQuery('#reload_img').hide();
             // alert(data);
			 
				var response = jQuery.parseJSON(data);
                if(response.BtcRate != '')
				{
					jQuery('#bitcoin').val(response.BtcRate);	
				}
                if(response.EthRate != '')
				{
					jQuery('#ethereum').val(response.EthRate);	
				}	
				if(response.BchRate != '')
				{
					jQuery('#bitcash').val(response.BchRate);	
				}	
				if(response.LtcRate != '')
				{
					jQuery('#litecoin').val(response.LtcRate);	
				}	
				if(response.XrpRate != '')
				{
					jQuery('#ripple').val(response.XrpRate);	
				}	
				if(response.DashRate != '')
				{
					jQuery('#dash').val(response.DashRate);	
				}	
				
				if(response.ZecRate != '')
				{
					jQuery('#zcash').val(response.ZecRate);	
				}/* if(response.DshRate != '')
				{
					jQuery('#dashcoin').val(response.DshRate);	
				} */		
				//var html="";	
			},error:function(error){ 
                jQuery('#reload_img').hide();
                alert("error!!! No Result Found!");
			}
		});
	}
   
</script>
@include('footer')