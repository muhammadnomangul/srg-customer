@include('header')

 
	<div id="page-wrapper">
           
		<div class="main-page">
            @include('messages')               
			<div class="row widget-shadow">
				
                <h3 class="title1" align="center">Add Currencies </h3>
               
                    <form class="form-horizontal" id="myform" role="form" method="POST" action="{{ url('dashboard/currencysave') }}">
                        {{ csrf_field() }}
						
                        <div class="form-group">
                            <label for="currencyName" class="col-md-2 control-label">Currency Name</label>

                            <div class="col-md-3">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" reqiured>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
							<label for="currency code" class="col-md-2 control-label">Currency Short Code</label>

                            <div class="col-md-3">
                                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}" reqiured>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                           
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                               
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
							 <div class="col-md-3">
                               
                            </div>
                            <div class="col-md-3">
                                <!--<button type="reset" class="btn btn-primary" onclick="clear()">
                                Clear
                                </button>-->
                            </div>
                        </div>
                    </form>
			<style>
				th {
					text-align: center;
				}
			</style>
                    <div width="80%" class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
						<div width="100%" style="text-align:center;">
							<div><h3 class="title" style="text-align:center;"><font color="red" align="center"> List All Currencies</font></h3><div>
						</div>
						
							<table id="myTable" class="table table-hover"> 
								<thead> 
									<tr> 
										<th>Sr#</th>
										<th>Curency Name</th>
										<th>Curency Code</th>
										<th>Staus</th> 
										<th>Action</th>
									</tr>
								</thead> 
								<tbody> 
								<?php $count = 1; ?>
								@foreach($currencies as $currency)
									<tr>
										<td>{{$count}}</td>
										<td>{{$currency->name}}</td>
										<td>{{$currency->code}}</td>
										<td>{{$currency->status}}</td>
										<td><a class="btn btn-default" href="currencyUpdate/{{$currency->id}}" >Update</a></td>
									</tr>
								<?php $count++; ?>
								@endforeach
								</tbody> 
							</table>
					   </div>
					</div>
				</div>
			</div>
		</div>
		

<script>
$(document).ready( function () {
	$('#myTable').DataTable();
} );


</script>
@include('footer')