<style type="text/css">

.style1 {color: #FFFFFF;
font-size:24px;}
.weekendt {font-size: 14px;
color: #FFFFFF;
}
.style3 {color: #FF0000}
.dim {color: #CCCCCC}

.style4 {color: #FFFFFF;
}
.days-td{
border-top:#FFFFFF solid 1px;
font-family:Arial, Helvetica, sans-serif;
    background-color: #660033;
	font-size: 20px;
	line-height:50px;
	color: #FFFFFF;
	border-left:#FFFFFF solid 1px;
	 
	
    
}
.date-td{
font-family:Arial, Helvetica, sans-serif;
    background-color:#A5588E;
	line-height:30px;
	color: #FFFFFF;
	border-top:#FFFFFF solid 1px;
	border-left:#FFFFFF solid 1px;
	
    
}
.profit-td{
font-family:Arial, Helvetica, sans-serif;
	line-height:50px;
	color: #FFFFFF;
	background-color: #660033;
	border-top:#FFFFFF solid 1px;
	border-left:#FFFFFF solid 1px;
	
    
}
.profit-td-weekend{
font-family:Arial, Helvetica, sans-serif;
font-size: 14px;
	line-height:50px;
	color: #FFFFFF;
	background-color: #660033;
	border-top:#FFFFFF solid 1px;
	border-left:#FFFFFF solid 1px;
	
    
}
a:link {
	color: #FFFFFF;
	font-size:15px;
}
a:visited {
	color: #FFFFFF;
}
a:hover {
	color: #FFFFFF;
}
a:active {
	color: #FFFFFF;
}

</style>


	<div class="container">
			<div class="w3-welcome-heading">
				<h3>Test Info</h3>
			</div>
			<div class="w3ls-about-grids">
				<div class="col-md-6 about-left"> 
					<div class="roww">
						<div class="col-md-6 subblockImg3">
							<div class="title">Calculate profit</div>
								<div class="topCalc roww">
									<div class="roww valuteBlock">
										<input type="checkbox" id="selectTra" style="display: none;">
										<label for="selectTra" class="selectTra" toptitle="Standard" bottomtitle="TOP-TRADE"></label>		
										<input class="valute1_1" type="radio" name="valute1" id="valute1_1">
										<label for="valute1_1" class="valute1_1"></label>
										<input class="valute1_2" type="radio" name="valute1" id="valute1_2">
										<label for="valute1_2" class="valute1_2"></label>
										<input class="valute1_3" type="radio" name="valute1" id="valute1_3" checked="">
										<label for="valute1_3" class="valute1_3"></label>
										<div class="reinvest">
											<span>Auto reinvest</span>
											<input type="checkbox" id="reinvestcheckbox">
											<label for="reinvestcheckbox"></label>
											<a href="#" class="info"></a>
										</div>
									</div>
									<div class="roww blockRange blockRange1a">
										<input type="text" id="elRange1" placeholder="Amount">
										<div class="elRange elRange1">
											<input type="range" min="0" max="100" step="1" value="5">
											<div class="running" style="left: 5%;"></div>
											<table class="titl">
												<tbody><tr>
													<td style="width: 10.5%">0.01</td>
													<td style="width: 26.3%">0.1</td>
													<td style="width: 26.3%">5</td>
													<td style="width: 26.3%">25</td>
													<td style="width: 26.3%">100</td>
												</tr>
											</tbody></table>
											<div class="progressLeft" style="width: 5%;"></div>
										</div>
									</div>
									<div class="roww blockRange blockRange1b">
										<input type="text" id="elRange2" placeholder="Amount">
										<div class="elRange elRange1">
											<input type="range" min="0" max="100" step="1" value="5">
											<div class="running" style="left: 5%;"></div>
											<table class="titl">
												<tbody><tr>
													<td style="width: 10.5%">0.01</td>
													<td style="width: 26.3%">50</td>
													<td style="width: 26.3%">300</td>
													<td style="width: 26.3%">1000</td>
													<td style="width: 26.3%">3000</td>
												</tr>
											</tbody></table>
											<div class="progressLeft" style="width: 5%;"></div>
										</div>
									</div>
									<div class="roww blockRange blockRange1c">
										<input type="text" id="elRange3" placeholder="Amount">
										<div class="elRange elRange1">
											<input type="range" min="0" max="100" step="1" value="5">
											<div class="running" style="left: 5%;"></div>
											<table class="titl">
												<tbody><tr>
													<td style="width: 10.5%">100</td>
													<td style="width: 26.3%">1000</td>
													<td style="width: 26.3%">5000</td>
													<td style="width: 26.3%">15000</td>
													<td style="width: 26.3%">50000</td>
												</tr>
											</tbody></table>
											<div class="progressLeft" style="width: 5%;"></div>
										</div>
									</div>
									<div class="roww blockRange blockRange2">
										<input type="text" id="elRange" placeholder="Period">
										<div class="elRange elRange2">
											<input type="range" min="0" max="100" step="1" value="5">
											<div class="running" style="left: 5%;"></div>
											<table class="titl">
												<tbody><tr>
													<td style="width: 10.5%">1</td>
													<td style="width: 15.8%">7</td>
													<td style="width: 15.8%">14</td>
													<td style="width: 15.8%">30</td>
													<td style="width: 15.8%">60</td>
													<td style="width: 15.8%">180</td>
													<td style="width: 10.5%">360</td>
												</tr>
											</tbody></table>
											<div class="progressLeft" style="width: 5%;"></div>
										</div>
									</div>
									<div class="roww pidsum roww">
										<div class="divInput divInputBTC" titl="PROFIT" id="res1">0.000000 <span>BTC</span></div>
										<div class="divInput divInputETH" titl="PROFIT" id="res2">0.000000 <span>ETH</span></div>
										<div class="divInput divInputUSD" titl="PROFIT" id="res3">0.00 <span>USD</span></div>

										<div class="divInput divInputBTC" titl="TOTAL" id="tres1">0.000000 <span>BTC</span></div>
										<div class="divInput divInputETH" titl="TOTAL" id="tres2">0.000000 <span>ETH</span></div>
										<div class="divInput divInputUSD" titl="TOTAL" id="tres3">0.00 <span>USD</span></div>
									</div>
								</div>
					<?php
						 /*  $pages = mysqli_query($conn, "SELECT * FROM  `today_currency` LIMIT 1");
							while ($info = mysqli_fetch_array($pages)) {
								$p1 = $info['profit_percentage1'];
								$p2 = $info['profit_percentage2'];
							} */
							$p1 = 1; $p2 = 2;
						?>
						
							<script>
								function reCalc(v, n) {
									sum = 1*$('#elRange'+v).val();
									if (sum<0) { sum=0; }
									days = 1*$('#elRange').val();
									if (days<0) { days=0; }
									curr = [ ' <span>BTC</span>', ' <span>ETH</span>', ' <span>USD</span>' ];
									sum0 = sum;
									p = pf = 0;
									for (i = 1; i <= days; i++) {
										if ($('#selectTra').prop('checked')) {
											if ($('#reinvestcheckbox').prop('checked')) {
												sum += pf;
											}
											p += pf = sum*<?php echo $p1; ?>/100;
										} else {
											p += pf = sum*<?php echo $p2; ?>/100;
										}
									}
									$('#res'+v).html(p.toFixed(n)+curr[v-1]);
									$('#tres'+v).html((p+sum0).toFixed(n)+curr[v-1]);
								}
								$('#reinvestcheckbox,#selectTra').change(function(){
									reCalc(1, 8);
									reCalc(2, 4);
									reCalc(3, 2);
								});
								$('#elRange1, #elRange').on('input', function(){ reCalc(1, 6); });
								$('#elRange2, #elRange').on('input', function(){ reCalc(2, 4); });
								$('#elRange3, #elRange').on('input', function(){ reCalc(3, 2); });
							</script>
						</div>
					</div>
				</div>
				<div class="col-md-6 about-right">
					<!---img src="{{asset('home/images/9.jpg')}}" alt=""-->
					<table width="100%" align="center" cellpadding="0" cellspacing="0" bordercolor="" bgcolor="#FFFFFF">
					  <tr>
						<td height="55" colspan="7" bgcolor="#660033">
							
							<div align="center">
						  <h2><span class="style1">Daily Profit ( Oct 2018 ) ||</span>  <a href="monthly/september2017.php">Previous Reports</a></h2>
						  </div></td>
					  </tr>
					  
					  <tr>
						<td><div class="days-td" align="center">Mon</div></td>
						<td><div class="days-td" align="center">Tue</div></td>
						<td><div class="days-td" align="center">Wed</div></td>
						<td><div class="days-td" align="center">Thu</div></td>
						<td><div class="days-td" align="center">Fri</div></td>
						<td><div class="days-td" align="center">Sat</div></td>
						<td><div class="days-td" align="center">Sun</div></td>
						</tr>
					  <tr>
						<td><div class="date-td" align="center">01 Oct </div></td>
						<td><div class="date-td" align="center">02 Oct</div></td>
					  <td><div class="date-td" align="center">03 Oct</div></td>
					  <td><div class="date-td" align="center">04 Oct</div></td>
					  <td><div class="date-td" align="center">05 Oct</div></td>
					  <td><div class="date-td" align="center">06 Oct</div></td>
						<td><div class="date-td" align="center">07 Oct</div></td>
					  
					  </tr>
					 
					 <tr>
						 <td><div class="profit-td" align="center"> 0.49%</div></td>
						 <td><div class="profit-td" align="center"> 0.41%</div></td>
						 <td><div class="profit-td" align="center"> 0.39%</div></td>
						 <td><div class="profit-td" align="center"> 0.33%</div></td>
						 <td><div class="profit-td" align="center"> 0.37%</div></td>
						 

							 
						 <td><div class="profit-td-weekend" align="center"> weekend</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
					   </tr>
						<tr>
						<td><div class="date-td" align="center">08 Oct </div></td>
						<td><div class="date-td" align="center">09 Oct</div></td>
					  <td><div class="date-td" align="center">10 Oct</div></td>
					  <td><div class="date-td" align="center">11 Oct</div></td>
					  <td><div class="date-td" align="center">12 Oct</div></td>
					  <td><div class="date-td" align="center">13 Oct</div></td>
						<td><div class="date-td" align="center">14 Oct</div></td>
					  
					  </tr>
					 
					 <tr>
						 <td><div class="profit-td" align="center"> 0.49%</div></td>
						 <td><div class="profit-td" align="center"> 0.41%</div></td>
						 <td><div class="profit-td" align="center"> 0.38%</div></td>
						 <td><div class="profit-td" align="center">0.31%</div></td>
						 <td><div class="profit-td" align="center">0.35%</div></td>
						 

							 
						 <td><div class="profit-td-weekend" align="center"> weekend</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
					   </tr>
						<tr>
						<td><div class="date-td" align="center">15 Oct </div></td>
						<td><div class="date-td" align="center">16 Oct</div></td>
					  <td><div class="date-td" align="center">17 Oct</div></td>
					  <td><div class="date-td" align="center">18 Oct</div></td>
					  <td><div class="date-td" align="center">19 Oct</div></td>
					  <td><div class="date-td" align="center">20 Oct</div></td>
						<td><div class="date-td" align="center">21 Oct</div></td>
					  
					  </tr>
					 
					 <tr>
						 <td><div class="profit-td" align="center">0.51%</div></td>
						<td><div class="profit-td" align="center">0.43%</div></td>
						<td><div class="profit-td" align="center">0.31%</div></td>
						<td><div class="profit-td" align="center">0.35%</div></td>
						<td><div class="profit-td" align="center">0.31%</div></td>
						
						 

							 
						 <td><div class="profit-td-weekend" align="center"> weekend</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
					   </tr>
						<tr>
						<td><div class="date-td" align="center">22 Oct </div></td>
						<td><div class="date-td" align="center">23 Oct</div></td>
					  <td><div class="date-td" align="center">24 Oct</div></td>
					  <td><div class="date-td" align="center">25 Oct</div></td>
					  <td><div class="date-td" align="center">26 Oct</div></td>
					  <td><div class="date-td" align="center">27 Oct</div></td>
						<td><div class="date-td" align="center">28 Oct</div></td>
					  
					  </tr>
					 
					 <tr>
					  <td><div class="profit-td" align="center">0.33%</div></td>
						<td><div class="profit-td" align="center">0.30%</div></td>
						<td><div class="profit-td" align="center">0.35%</div></td>
						<td><div class="profit-td" align="center">0.30%</div></td>
						<td><div class="profit-td" align="center">0.35%</div></td>
							 
						 <td><div class="profit-td-weekend" align="center"> weekend</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
					   </tr>
					   <tr>
					   <td><div class="date-td" align="center">29 Oct </div></td>
					   <td><div class="date-td" align="center">30 Oct </div></td>
					   <td><div class="date-td" align="center">31 Oct </div></td>
					   <td><div class="date-td" align="center">01 Nov </div></td>
					   <td><div class="date-td" align="center">02 Nov </div></td>
					   <td><div class="date-td" align="center">03 Nov </div></td>
					   <td><div class="date-td" align="center">04 Nov </div></td>
						
						
					   
					  </tr>
					 
					 <tr>
						<td><div class="profit-td" align="center">0.30%</div></td>
						<td><div class="profit-td" align="center">0.32%</div></td>
						<td><div class="profit-td" align="center">0.33%</div></td>
						<td><div class="profit-td" align="center"> -</div></td>
						<td><div class="profit-td" align="center"> -</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
						<td><div class="profit-td-weekend" align="center"> weekend</div></td>
					   </tr>
					  
					   
					   
					   </table>

				</div>
				<div class="clearfix"> </div>
			</div>
			
		</div>
		<div>
		
		</div>