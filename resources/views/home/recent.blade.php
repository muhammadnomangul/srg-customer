
					<div class="col-md-4 agileits-services-left">
					
						<div class="services-two-grids">
							<div class="agileinfo-services-right">
								<img src="{{asset('home/images/deposit.png')}}" height="150" alt="" />
								<h6 align="center">Latest Deposits</h6>
							</div>
							<!-- date -->
							<div id="design" class="date1">
								<div id="cycler"> 
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($deposits[0])) Amount:	{{number_format($deposits[0]->amount,4,".",",")."	(".$deposits[0]->currency.")"}} @endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($deposits[1])) Amount:	{{number_format($deposits[1]->amount,4,".",",")."	(".$deposits[1]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($deposits[2])) Amount:	{{number_format($deposits[2]->amount,4,".",",")."	(".$deposits[2]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($deposits[3])) Amount:	{{number_format($deposits[3]->amount,4,".",",")."	(".$deposits[3]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($deposits[4])) Amount:	{{number_format($deposits[4]->amount,4,".",",")."	(".$deposits[4]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i
													class="fa fa-arrow-right"
													aria-hidden="true"></i>@if(isset($deposits[5]))
												Amount:    {{number_format($deposits[5]->amount,4,".",",")."	(".$deposits[5]->currency.")"}}@endif
										</a>
									</div>
								</div>
									
								<script>
									function blinker(){
										$('.blinking').fadeOut(500);
										$('.blinking').fadeIn(500);
									}
									setInterval(blinker, 1000);
									
									function cycle($item, $cycler)
									{
										setTimeout(cycle, 2000, $item.next(), $cycler);				
										$item.slideUp(1000,function(){
											$item.appendTo($cycler).show();        
										});
									}
									cycle($('#cycler div:first'),  $('#cycler'));
								</script>
							</div>
						<!-- //date -->
						</div>
					</div>
					<div class="col-md-4 agileits-services-left">
						<div class="services-two-grids">
							<div class="agileinfo-services-right">
								<img src="{{asset('home/images/withdrawal.jpg')}}" height="150" alt="" />
								<h6 align="center">Latest Withdrawals</h6>
							</div>
							<!-- date -->
							<div id="design2" class="date2">
								<div id="cycler2">   
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($withdrawals[0])) Amount:	{{number_format($withdrawals[0]->amount,4,".",",")."	(".$withdrawals[0]->currency.")"}} @endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($withdrawals[1])) Amount:	{{number_format($withdrawals[1]->amount,4,".",",")."	(".$withdrawals[1]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($withdrawals[2])) Amount:	{{number_format($withdrawals[2]->amount,4,".",",")."	(".$withdrawals[2]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($withdrawals[3])) Amount:	{{number_format($withdrawals[3]->amount,4,".",",")."	(".$withdrawals[3]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($withdrawals[4])) Amount:	{{number_format($withdrawals[4]->amount,4,".",",")."	(".$withdrawals[4]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i
													class="fa fa-arrow-right"
													aria-hidden="true"></i>@if(isset($withdrawals[5]))
												Amount:    {{number_format($withdrawals[5]->amount,4,".",",")."	(".$withdrawals[5]->currency.")"}}@endif
										</a>
									</div>
								</div>
									
								<script>
									function blinker2() {
										$('.blinking2').fadeOut(500);
										$('.blinking2').fadeIn(500);
									}
									setInterval(blinker2, 1000);
								
									function cycle2($item, $cycler2){
										setTimeout(cycle2, 2000, $item.next(), $cycler2);				
										$item.slideUp(1000,function(){
											$item.appendTo($cycler2).show();        
										});
									}
									cycle2($('#cycler2 div:first'),  $('#cycler2'));
								</script>
						
							</div>
						<!-- //date -->
						</div>
					</div>
					<div class="col-md-4 agileits-services-left">
						<div class="services-two-grids">
							<div class="agileinfo-services-right">
								<img src="{{asset('home/images/sold.png')}}" height="150" alt="" />
								<!--<img src="{{asset('home/images/10.jpg')}}" alt="" />-->
								<h6 align="center">Latest Solds</h6>
							</div>
							<!-- date -->
							<div id="design3" class="date3">
								<div id="cycler3"> 
								
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>	@if(isset($solds[0])) Amount:	{{number_format($solds[0]->amount,4,".",",")."	(".$solds[0]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($solds[1])) Amount:	{{number_format($solds[1]->amount,4,".",",")."	(".$solds[1]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($solds[2])) Amount:	{{number_format($solds[2]->amount,4,".",",")."	(".$solds[2]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($solds[3])) Amount:	{{number_format($solds[3]->amount,4,".",",")."	(".$solds[3]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-arrow-right" aria-hidden="true"></i>@if(isset($solds[4])) Amount:	{{number_format($solds[4]->amount,4,".",",")."	(".$solds[4]->currency.")"}}@endif</a>
									</div>
									<div class="date-text">
										<a href="#" data-toggle="modal" data-target="#myModal"><i
													class="fa fa-arrow-right"
													aria-hidden="true"></i>@if(isset($solds[5]))
												Amount:    {{number_format($solds[5]->amount,4,".",",")."	(".$solds[5]->currency.")"}}@endif
										</a>
									</div>
								</div>
								<script>
									function blinker3() {
										$('.blinking3').fadeOut(500);
										$('.blinking3').fadeIn(500);
									}
									setInterval(blinker3, 1000);
								
									function cycle3($item, $cycler3){
										setTimeout(cycle3, 2000, $item.next(), $cycler3);				
										$item.slideUp(1000,function(){
											$item.appendTo($cycler3).show();        
										});
									}
									cycle3($('#cycler3 div:first'),  $('#cycler3'));
								</script>
							</div>
						<!-- //date -->
						</div>
					</div>