
@include('home.assets')
<?php

/* echo "<pre>";
 print_r($pplans);

exit; */
 ?>
		<!-- TradingView Widget BEGIN -->
		<div class="tradingview-widget-container" style="z-index: 12; position: absolute; bottom: 0;">
			<div class="tradingview-widget-container__widget"></div>
			<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
				{
				"symbols": [
					{
					"title": "S&P 500",
					"proName": "INDEX:SPX"
					},
					{
					"title": "Nasdaq 100",
					"proName": "INDEX:IUXX"
					},
					{
					"title": "EUR/USD",
					"proName": "FX_IDC:EURUSD"
					},
					{
					"title": "BTC/USD",
					"proName": "BITFINEX:BTCUSD"
					},
					{
					"title": "ETH/USD",
					"proName": "BITFINEX:ETHUSD"
					}
				],
				"locale": "en"
				}
			</script>
		</div>
		<!-- TradingView Widget END -->
		<div class="w3layouts-banner-info">
			<div class="container">
				<div class="w3layouts-banner-slider">
					<div class="slider">
						<div class="callbacks_container">
							<ul class="rslides callbacks callbacks1" id="slider4">
								<li>
									<div class="agileits-banner-info">
										<h3>WELCOME TO B4U DIGITAL WORLD</h3>
										<p>
										We are experts in the field of trading and developing cryptocurrencies. we are willing to share our experience with EVERYONE. 
										</p>
									</div>
								</li>
								<li>
									<div class="agileits-banner-info">
										<h3>OUR VISION</h3>
										<p>To achieve ultimate Financial Freedom for Everyone  </p>
									</div>
								</li>
								<li>
									<div class="agileits-banner-info">
										<h3>OUR MISSION </h3>
										<p> To reach out to everyone with Love and Sincerity </p>
									</div>
								</li>
								<li>
									<div class="agileits-banner-info">
										<h3>Most Transparent and Secure Investment platform in the world.</h3>
										<p>B4U is registered and approved by the Legal authorities in Malaysia, Indonesia, Cambodia and Pakistan</p>
									</div>
								</li>
							</ul>
						</div>
						<script src="{{asset('home/js/responsiveslides.min.js')}}"></script>
						<script>
							// You can also use "$(window).load(function() {"
							$(function () {
							  // Slideshow 4
							  $("#slider4").responsiveSlides({
								auto: true,
								pager:true,
								nav:false,
								speed: 500,
								namespace: "callbacks",
								before: function () {
								  $('.events').append("<li>before event fired.</li>");
								},
								after: function () {
								  $('.events').append("<li>after event fired.</li>");
								}
							  });
						
							});
						 </script>
						<!--banner Slider starts Here-->
					</div>
				</div>
			</div>
		</div>
		<div class="bounce animated">
			<a href="#welcome" class="scroll">
				<div class="mouse"></div>
			</a>
		</div>
	</div>
	<!-- //banner -->
	<!-- welcome -->
	<div class="welcome" id="welcome">
		<div class="container">
			<div class="w3-welcome-heading">
				<h2>Welcome to B4U family</h2>
			</div>
			<div class="w3l-welcome-info">
				<div class="col-sm-6 welcome-grids">
					<div class="welcome-img">
						<img src="{{asset('home/images/mlaysia-tour.jpg')}}" class="img-responsive zoom-img" alt="">
					</div>
				</div>
				<div class="col-sm-6 welcome-grids">
				    
				    <h2>Amazing Tour Announced 1 Week Malaysia Tour.</h2>
				    <p>
					Personal sales or Group Sales will entitle you to qualify.<br> 
					<ul>
						<li>PERSONAL INVESTMENT 1,000 USD</li>
						<li>DIRECT SALES 5,000 USD</li>
						<li>OVERALL TEAM SALES 20,000 USD, TOTAL L1-L5(25,000 USD)</li>
					</ul>
					For More Deataile Contact Your Leader.<br>
					<i>Terms & Conditions apply</i></p>
					<!---div class="welcome-img">
						<img src="{{asset('home/images/6.jpg')}}" class="img-responsive zoom-img" alt="">
					</div--->
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---div class="w3l-welcome-text">
				<p </p>
			</div--->
		</div>
	</div>
	<!-- //welcome -->
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Good To Know</h3>
			</div>
			<div class="w3ls-about-grids">
				
				<div class="col-md-6 about-left"> 
					<h4>What we do?</h4>
					<p>B4U is group of people who have large experience of trading, investment tricks and market scope. 
					<span>By using their skills and expertise we trade your investment in the market and earn profit. our major investment domains are as following:  </span></p>
					<ul> 
						<li><span class="glyphicon glyphicon-share-alt"></span> Investment in property deals  </li>
						<li><span class="glyphicon glyphicon-share-alt"></span> Investment in Crypto Exchange </li>
						<li><span class="glyphicon glyphicon-share-alt"></span> Investment in Information Technologies</li>
						<li><span class="glyphicon glyphicon-share-alt"></span> Investment in Transport Services </li>
						<li><span class="glyphicon glyphicon-share-alt"></span> Investment in Trading </li> 
					</ul>
				</div>
				<div class="col-md-6 about-right">
					<!---img src="{{asset('home/images/9.jpg')}}" alt=""-->
					<div class="btcwdgt-chart" bw-cash="true"></div>

				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //about -->	
	<!-- Test -->
	<div class="profit" id="profit">

		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Daily Profit Chart</h3>
			</div>
			<div class="w3ls-about-grids">
				
				
					@include('home/calender')
				<div class="clearfix"> </div>
				
			</div>
		</div>
	</div>
	<!-- services -->
	<div class="services" id="services">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Join us! We made it easy</h3>
			</div>
			<div class="agileits-services-grids">
				<div class="col-md-8 agileits-services-left">
					<h4>At a glance</h4>
					<div class="agileits-services-text">
						<p>Very easy way to invest in any local, international or crypto currency </p>
					</div>
					<div class="credit-grids">
						<h5>Pay using Credit Cards:</h5>
						<div class="credit-grid-left">
							<div class="credit-grid">
								<img src="{{asset('home/images/c2.jpg')}}" alt="" />
								<h6>Visa</h6>
								<p>Pay using Visa Card on Paypal, Advance Cash, Skrill </p>
							</div>
							<div class="credit-grid">
								<img src="{{asset('home/images/c3.jpg')}}" alt="" />
								<h6>MasterCard</h6>
								<p>Pay using Visa Card on Paypal, Advance Cash, Skrill</p>
							</div>
							<div class="credit-grid">
								<img src="{{asset('home/images/c4.jpg')}}" alt="" />
								<h6>MasterCard</h6>
								<p>Pay using Visa Card on Paypal, Advance Cash, Skrill</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="credit-grids debit-grids">
						<h5>Debit Cards:</h5>
						<div class="debit-grids-text">
							<p>Morbi nec justo ut ex rhoncus luctus. Duis id ex egestas, tempus lorem sed, porta urna. Duis sodales eleifend laoreet. Vestibulum luctus venenatis massa, in vulputate mi porta ac.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits-services-left">
						@include('home/profitCalculator')
						
				</div>
			</div>	
			<div class="col-md-12 agileits-services-right">
				<div class="w3-services-grids">
						<div class="col-md-4 w3l-services-grid">
							<div class="w3ls-services-img">
								<i class="fa fa-money" aria-hidden="true"></i>
							</div>
							<div class="agileits-services-info">
								<h4>Recent Deposits</h4>
							</div>
						</div>
						<div class="col-md-4 w3l-services-grid">
							<div class="w3ls-services-img">
								<i class="fa fa-credit-card" aria-hidden="true"></i>
							</div>
							<div class="agileits-services-info">
								<h4>Recent withdrawals</h4>
							</div>
						</div>
						<div class="col-md-4 w3l-services-grid">
							<div class="w3ls-services-img">
								<i class="fa fa-line-chart" aria-hidden="true"></i>
							</div>
							<div class="agileits-services-info">
								<h4>Recent Solds</h4>
							</div>
						</div>
						<div class="clearfix"> </div>
				</div>
					@include('home/recent')
				<div class="clearfix"> </div>
				<!--<table id="myTable" class="table table-hover"> 
						<thead> 
						<tr> 
							<th>User</th>
							<th>Amount</th>
						</tr>
						</thead> 
							<tbody> 
							@foreach($deposits as $data)
							<tr>
								<td>{{$data->u_id}}</td>
								<td>{{number_format($data->amount,2,".",",")}}</td>
							</tr>
							@endforeach
						</tbody> 
					</table>-->
			</div>
		</div>
	</div>
	<!-- plans -->
	<div class="plans" id="plans">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Investment Plans</h3>
			</div>
			@foreach($pplans as $plans)
			
			<div class="col-md-4">
				<div class="sign-up-row widget-shadow" style="width:100%; padding:0px;">
					<h3 style="background-color:#e9e9e9; padding:5px;">
						{{$plans->name}}  <img style="width:70px;" src="{{asset('images/'.$plans->img_url)}}">
					</h3>
					<div class="agile-news-info agileinfo-services-left" style="padding:5px; text-align:left;">
						<p><font color="blue"><i class="fa fa-star"></i></font> Criteria For Join This Plan : </p>
						<p><i class="fa fa-"></i>Your Personel Investment Reaised To : {{$settings->currency}}{{$plans->personel_investment_limit}} </p>
						<p> <i class="fa fa-"></i>OR Your Structural Investment Reaised To: {{$settings->currency}}{{$plans->structural_investment_limit}}</p>
						<hr>
						<p><h4><strong>{{$settings->currency}}{{$plans->price}}+</strong></h4>
						<font color="blue"><i class="fa fa-star"></i></font> At least {{$settings->currency}}{{$plans->expected_return}} weekly return</p>
						<hr>
						<strong>Referal Bonuses : <strong>
						<table style="width:100%;">
							<tr>
								<td style="width:50%;">
									<div class="agileits-services-info">
										<h4>Profit Bonus</h4>
									</div>	
									<div>
										<div style="float:left; width:70%;">First Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->first_pline}}%</div>
										<div style="float:left; width:70%;">Second Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->second_pline}}%</div>
										<div style="float:left; width:70%;">Third Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->third_pline}}%</div>
										<div style="float:left; width:70%;">Fourth Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fourth_pline}}%</div>
										<div style="float:left; width:70%;">Fifth Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fifth_pline}}%</div>
									</div>
								</td>
								<td style="width:50%;">
									<div class="agileits-services-info">
										<h4>Investment Bonus</h4>
									</div>
									<div>
										<div style="float:left; width:70%;">First Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->first_line}}%</div>
										<div style="float:left; width:70%;">Second Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->second_line}}%</div>
										<div style="float:left; width:70%;">Third Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->third_line}}%</div>
										<div style="float:left; width:70%;">Fourth Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fourth_line}}%</div>
										<div style="float:left; width:70%;">Fifth Line:</div><div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fifth_line}}%</div>
									</div>	
								</td>
							</tr>
						</table>		
					</div>
				</div>
			</div>
			@endforeach	
		</div>
	</div>
	<!-- news -->
	<div class="news" id="news">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>B4U Events</h3>
			</div>
			<div class="w3ls-news-grids">
				@include('home/gallery')
						
					
					<!--<div class="col-md-4 news-right-grid">
						<div class="agile-news-info">
							<img src="{{asset('home/images/n1.jpg')}}" alt=" " class="img-responsive">
							<h4><a href="#" data-toggle="modal" data-target="#myModal">Sunt in culpa qui officia velit</a></h4>
							<span>19th June | 10:00 - 12:00</span>
							<p> Integer interdum eros vitae sem ultrices, sed eleifend tellus tincidunt. Nam nisl arcu, porttitor sit amet</p>
							<div class="agileinfo-news-button">
								<a href="#" class="hvr-shutter-in-horizontal" data-toggle="modal" data-target="#myModal">More</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 news-right-grid">
						<div class="agile-news-info">
							<img src="{{asset('home/images/n2.jpg')}}" alt=" " class="img-responsive">
							<h4><a href="#" data-toggle="modal" data-target="#myModal">Neque porro quisquam est</a></h4>
							<span>24th Sept | 09:00 - 11:00</span>
							<p> Integer interdum eros vitae sem ultrices, sed eleifend tellus tincidunt. Nam nisl arcu, porttitor sit amet</p>
							<div class="agileinfo-news-button">
								<a href="#" class="hvr-shutter-in-horizontal" data-toggle="modal" data-target="#myModal">More</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 news-right-grid">
						<div class="agile-news-info">
							<img src="{{asset('home/images/n3.jpg')}}" alt=" " class="img-responsive">
							<h4><a href="#" data-toggle="modal" data-target="#myModal">Etiam ut nibh quis magna</a></h4>
							<span>04th Oct | 12:00 - 02:00</span>
							<p> Integer interdum eros vitae sem ultrices, sed eleifend tellus tincidunt. Nam nisl arcu, porttitor sit amet</p>
							<div class="agileinfo-news-button">
								<a href="#" class="hvr-shutter-in-horizontal" data-toggle="modal" data-target="#myModal">More</a>
							</div>
						</div>
					</div>-->
				
				<div class="clearfix"> </div>
			</div>
			<!-- modal 
			<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header"> 
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							<h4 class="modal-title"><span>Banking</span></h4>
						</div> 
						<div class="modal-body">
							<div class="agileits-w3layouts-info">
								<img src="{{asset('home/images/g2.jpg')}}" alt="" />
								<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque ac fermentum diam. Integer eu facilisis nunc, a iaculis felis. Pellentesque pellentesque tempor enim, in dapibus turpis porttitor quis. Suspendisse ultrices hendrerit massa. Nam id metus id tellus ultrices ullamcorper.  Cras tempor massa luctus, varius lacus sit amet, blandit lorem. Duis auctor in tortor sed tristique. Proin sed finibus sem.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //modal --> 
		</div>
	</div>
	<!-- //news -->
	<!-- feedback -->
	<div class="jarallax feedback" id="feedback">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Clients Feedback</h3>
			</div>
			<div class="agileits-feedback-grids">
				<div id="owl-demo" class="owl-carousel owl-theme">
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="{{asset('home/images/f1.jpg')}}" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Mary Jane</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="{{asset('home/images/f2.jpg')}}" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Peter guptill</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="{{asset('home/images/f3.jpg')}}" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Steven Wilson</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="{{asset('home/images/f1.jpg')}}" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Mary Jane</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="images/f2.jpg" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Peter guptill</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Sed semper leo metus, a lacinia eros semper at. Etiam sodales orci sit amet vehicula pellentesque. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="images/f3.jpg" alt="" />
								</div>
								<div class="feedback-img-info">
									<h5>Steven Wilson</h5>
									<p>Vestibulum</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //feedback -->
	<!-- contact -->
	<div class="contact" id="contact">
		<div class="container">
			<div class="w3-welcome-heading">
				<h3>Contact Us</h3>
			</div>
			<div class="agile-contact-grids">
				<div class="col-md-7 contact-form">
					<form action="{{action('UsersController@sendcontact')}}" method="post">
						<input type="text" name="name" placeholder="Enter your full name" required="">
						<input type="email" class="email" name="email" placeholder="Enter your Email" required="">
						<textarea name="nessage" placeholder="Enter Message" required=""></textarea>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" value="SUBMIT">
					</form>
				</div>
				<div class="col-md-5 agileits-w3layouts-address">
					<div class="agileits-w3layouts-address-top">
						<h5>Get in touch</h5>
						<ul>
							<li>+1 234 567 8901</li>
							<li>+1 234 567 8902</li>
						</ul>
					</div>
					<div class="agileits-w3layouts-address-top">
						<h5>Address</h5>
						<ul>
							<li>123 Fourth Avenue,</li>
							<li>lacinia eros 98104,</li>
							<li>New Jersey,</li>
							<li>United States.</li>
						</ul>
					</div>
					<div class="agileits-w3layouts-address-top">
						<h5>Email</h5>
						<ul>
							<li><a href="mailto:info@example.com"> mail@example.com</a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>	
			</div>
		</div>
	</div>
	<!-- //contact -->
	<!-- footer -->
	<div class="jarallax footer">
		<div class="container">
			<div class="footer-logo">
				<h3><a href="/">{{$settings->site_name}}</a></h3>
			</div>
			<div class="agileinfo-social-grids">
				<h4>We are social</h4>
				<div class="border"></div>
				<ul>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-rss"></i></a></li>
					<li><a href="#"><i class="fa fa-vk"></i></a></li>
				</ul>
			</div>
			<div class="copyright">
				<p>© 2018 {{$settings->site_name}}. All rights reserved</p>
			</div>
		</div>
	</div>
	<!-- //copyright -->
	<script src="{{asset('home/js/jarallax.js')}}"></script>
	<script src="{{asset('home/js/SmoothScroll.min.js')}}"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	<script type="text/javascript" src="{{asset('home/js/move-top.js')}}"></script>
	<script type="text/javascript" src="{{asset('home/js/easing.js')}}"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			//$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="{{asset('home/js/owl.carousel.js')}}"></script>  
<script>
  (function(b,i,t,C,O,I,N) {
    window.addEventListener('load',function() {
      if(b.getElementById(C))return;
      I=b.createElement(i),N=b.getElementsByTagName(i)[0];
      I.src=t;I.id=C;N.parentNode.insertBefore(I, N);
    },false)
  })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');
</script>
<script src="{{asset('home/js/bootstrap.js')}}"></script>
<!--<script src="{{ asset('home/js/calculator.js')}}"></script>
<script src="{{ asset('home/js/moment.min.js')}}"></script>
<script src="{{ asset('home/js/fullcalendar.min.js')}}"></script>-->
</body>	
</html>