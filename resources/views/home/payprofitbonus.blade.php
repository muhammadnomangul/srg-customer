@include('header')

<!-- main content start-->

<div id="page-wrapper">

    <div class="main-page signup-page">

        <h3 class="title1">Distribute Bonus</h3>


        @include('messages')



        @if(count($errors) > 0)

            <div class="row">

                <div class="col-lg-12">

                    <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        @foreach ($errors->all() as $error)

                            <i class="fa fa-warning"></i> {{ $error }}

                        @endforeach

                    </div>

                </div>

            </div>

        @endif


        <div class="sign-up-row widget-shadow">


            <form action="#">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <label>Trade ID:</label>
                    <div class="form-group">
                        <input class="form-control" name="trade_id" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <label>Date: </label>
                    <div class="form-group">
                        <input class="form-control" name="date" type="text" id="datepicker2" required>
                    </div>
                </div>
                <div class="row">
                    <label>Percentage Rate</label>
                    <div class="form-group">
                        <input class="form-control" type="text" name="per_rate" required>

                    </div>
                </div>
                <div class="row">
                    <div style="float: right;">
                        <a class="btn btn-info" onclick="viewProfitAmount1();"> Distribute</a>
                    </div>
                </div>


            </form>

        </div>

    </div>

</div>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>

    $(function () {
        $("#datepicker").datepicker();
        $("#datepicker2").datepicker();
    });

    $(document).ready(function () {
        //option A
        $("form").submit(function (e) {
            alert('submit intercepted');
            e.preventDefault(e);
        });
    });

    function postData(url = ``, data = {}) {
        // Default options are marked with *
        return fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
        // parses response to JSON
    }

    function viewProfitAmount1() {
        //	alert(id);
        var trade_id = $("input[name=trade_id]").val();
        var date = $("input[name=date]").val();
        var per_rate = $("input[name=per_rate]").val();
        //alert(approvedAt + soldAt +amount);

        postData('{{{URL::to("")}}}/dashboard/calculateProfit', {
            per_rate: per_rate,
            trade_id: trade_id,
            date: date,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                console.log(response)
                //alert("Profit Amount :"+response);
                return response.text()

            })
            .then(function (text) {
                alert(text);

            }) // JSON-string from `response.json()` call
            .catch(function (error) {
                //console.error(error)
                alert("error!!!!");
            });
    }
</script>
@include('footer')