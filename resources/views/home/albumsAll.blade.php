<!DOCTYPE html>
<html lang="en">
<head>
	@include('home/assetss')
	<style>
	.text-center{text-align:center!important;padding: 20px;}
	 a {color:#22BCB9;text-decoration:none;}
            .cred{margin-top:20px;font-size:11px;}

            /* This rule is read by Galleria to define the gallery height: */
            .galleria{height:450px}
			
		/*.galleria-stage { 
		  height:450px; 
		  position: absolute; 
		  top: 10px; 
		  bottom: 60px; 
		  left: 10px; 
		  right: 10px; 
		  overflow:hidden;
		}*/
	</style>
	<script src="{{asset('home/galleria-files/jquery.js')}}"></script> 
	<link href="{{asset('css/jquery-ui.css')}}" rel="stylesheet">
	<script src="{{asset('js/jquery-ui.js')}}"></script>
	
	<script src="{{asset('home/galleria-files/galleria-1.5.7.min.js')}}"></script> 
	<script src="{{asset('home/galleria-files/galleria.classic.min.js')}}"></script>
	
	<!--
	<script src="{{asset('home/galleria-files/galleria.fullscreen.min.js')}}"></script>-->

</head>

<body class="auth-page" style="background-color:#f8f8f8;">

    <!-- Wrapper Starts -->
    <div class="wrapper">
		<div class="container" style="padding:20px;">
			<div class="row">
			<!-- Logo Starts -->
				<div class="text-center">

				@if(isset($settings->logo))

				<a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
					<img id="logo" class="img-responsive" src="{{ asset('images/b4u-logo.png')}}" alt="logo" style="margin-left: auto; margin-right: auto;">
				</a>
				@else
					<a style="text-align:center; width:100%;" href="{{ url('/') }}">
						<img src="{{ asset('images/b4u-logo.png') }}" style="margin-left: auto; margin-right: auto;">
					</a>
				@endif
				</div>
			</div>
			<div class="row">

				<h3 class="title1">EVENTS Gallery</h3>

                @include('messages')

				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 

					<?php $count = 1; $count2 = 1; ?>
                    <div id="tabs">
						
                        <ul>
							@foreach($AlbumImages as $images)
                            <li><a href="#tabs-{{$count}}">Gallery {{$count}} </a></li>
							<?php $count++; ?>
							@endforeach
                        </ul>
						@foreach($AlbumImages as $images)
                        <div id="tabs-{{$count2}}">
							<div class="galleria">
								@foreach($images->AlbumhasManyImages as $aimages)
								<a href="{{asset('images/gallery/'.$aimages->image_name)}}">
									<img 
										src="{{asset('images/gallery/'.$aimages->image_name)}}",
										data-big="{{asset('images/gallery/'.$aimages->image_name)}}"
										data-title="{{$aimages->title}}"
										data-description="{{$aimages->description}}"
									>
								</a>
								@endforeach
								
							</div>
                        </div>
						<?php $count2++; ?>
						@endforeach
                    </div>
				</div>

			</div>
		</div>
	</div>

	
	
	
	
	
	
 <script>
 
	
    $(function() {
  
		Galleria.run('.galleria');
		
		$( "#tabs" ).tabs();
    });
	

    </script>
</body>

</html>
	
