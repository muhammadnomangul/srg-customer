<!DOCTYPE html>
<html lang="en">
<head>
    <title>@if(isset($settings->site_name)){{$settings->site_name}}@else B4U Investors @endif
        |@if(isset($title)) {{$title}} @endif</title>
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{asset('home/home/images/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="48x48" href="{{asset('home/home/images/favicon-48x48.png')}}">
    <link rel="apple-touch-icon" sizes="96x96" href="{{asset('home/home/images/favicon-96x96.png')}}">


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="p:domain_verify" content="7c2c0d53b8d1c798ad4f872c9f097502"/>

    {{--	<link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet">--}}

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>

    <!-- bootstrap-css -->
    <link href="{{asset('home/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <!--// bootstrap-css -->
    <!-- css -->
    <link rel="stylesheet" href="{{asset('home/css/style.css')}}?123" type="text/css" media="all"/>
    <!--// css -->
    <link rel="stylesheet" href="{{asset('home/css/owl.carousel.css')}}" type="text/css" media="all">
    <link href="{{asset('home/css/owl.theme.css')}}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{asset('home/css/cm-overlay.css')}}"/>
    <!-- font-awesome icons -->
    {{--	<link href="{{asset('home/css/font-awesome.css')}}" rel="stylesheet">--}}

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- //font-awesome icons -->
    <!--// css -->


    <!--//Js -->


    <!-- font -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>

<!-- //font
	<script src="{{asset('home/js/jquery-1.11.1.min.js')}}"></script> -->
    <script src="{{asset('home/js/jquery.min.js')}}"></script>
    <script src="{{asset('home/js/owl.carousel.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142035728-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-142035728-1');
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) 
        {

            $(".scroll").click(function (event) 
            {
                event.preventDefault();
                var urlHash = window.location.href.split("#")[0];
                console.log($(this.hash).offset());
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                //$('html,body').animate({scrollTop: $('#' + urlHash).offset().top}, 1000);

            });



        });


        jQuery(document).ready(function(){
        jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
        || location.hostname == this.hostname) {
        var target = jQuery(this.hash);
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
        if (target.length) {
        jQuery('html, body').animate({
        scrollTop: target.offset().top - 90 }, 1000);
        return false;
        }
        }
        });
        });
        jQuery(window).load(function(){
        function goToByScroll(id){
        jQuery("html, body").animate({scrollTop: jQuery("#"+id).offset().top - 90 }, 1000);
        }
        if(window.location.hash != '') {
        goToByScroll(window.location.hash.substr(1));
        }
        });
    </script>
    <!---translations---->
    <script type="text/javascript">window.liveSettings = {api_key: "ee48175e1b5d40b9827aff45cac7bb4d"}</script>
    <script type="text/javascript" src="{{asset('home/js/translation.js')}}"></script>

    <!-- animation -->
    <link href="{{asset('home/css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
    <script src="{{asset('home/js/wow.min.js')}}"></script>
    <script>
        new WOW().init();
    </script>

    <!-- //animation -->
    <script>

        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
                autoPlay: 3000, //Set AutoPlay to 3 seconds
                autoPlay: true,
                items: 3,
                itemsDesktop: [640, 5],
                itemsDesktopSmall: [414, 4]
            });

        });
    </script>

    <style>
        /*nav {background:#FFF;float:left;}
        nav ul {text-align:center;}*/
        nav ul li {
            float: left;
            display: inline;
        }

        nav ul li:hover {
            background: #E6E6E6;
        }

        nav ul li a {
            display: block;
            padding: 5px 5px;
            color: #444;
        }

        nav ul li ul {
            position: absolute;
            width: 200px;
            background: #FFF;
        }

        nav ul li ul li {
            width: 150px;
        }

        nav ul li ul li a {
            display: block;
            padding: 15px 10px;
            color: #444;
        }

        nav ul li ul li:hover a {
            background: #F7F7F7;
        }

        nav ul li ul.fallback {
            display: none;
        }

        nav ul li:hover ul.fallback {
            display: block;
        }
    </style>
    <script data-ad-client="ca-pub-8618087093269654" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<!-- banner -->
<div class="banner">
    <!--header-->
    <div class="header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="{{ url('/') }}">
                        <img src="{{ asset('home/home/images/logo-light.png') }}">
                    </a>
                @if(isset($settings->site_name))
                    <!--<h1><a  href="/">{{$settings->site_name}}</a></h1>-->
                    @endif
                </div>

                <!--navbar-header-->
                <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false"
                     style="height: 0px;">
                <!--  <a class="btn btn-danger btn-sm" href="{{ url('/home2') }}">Try Our NewLook</a> -->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/') }}" class="active">Home</a></li>
                        <li><a href="{{ url('/') }}#about" >About</a></li><!--class="scroll"-->
                        <li><a href="{{ url('/') }}#profit">Profit</a></li><!--class="scroll"-->
                        <li><a href="{{ url('/') }}#services">Payment</a></li><!--class="scroll"-->
                        <li><a href="{{ url('/') }}#plans">Plans</a></li><!--class="scroll"-->
                        <li><a href="#">TOS </a>
                            <!--<ul class="nav nav-second-level collapse">-->
                            <ul class="fallback">
                                <li>
                                    <a href="{{ url('/terms') }}">Terms And Conditions</a>
                                </li>
                                <li>
                                    <a href="{{ url('/privacy') }}">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="{{ url('/partnership_agreement') }}">Partnership Agreement</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="{{ asset('uploads/fatwa.pdf') }}">Fatwa</a></li>
                        <li><a href="#news" class="scroll">Events</a></li>
                        <li><a href="{{ url('/contactus') }}">Contact</a></li>

                        <!--<li><a href="#feedback" class="scroll">Clients</a></li>
                        <li><a href="#" class="scroll">FAQs</a></li>-->
                        <!--<li><a href="#contact" class="scroll">Contact</a></li>-->
                        @if (Route::has('login'))
                            @auth
                                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ url('logout') }}" onclick="event.preventDefault();
											document.getElementById('logout-form').submit();"> Logout</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                            @else
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                        @endauth
                    @endif
                    <!--<li><a href="{{route('login')}}">Login</a></li>
							<li><a href="{{route('register')}}">Register</ia></li>-->

                    </ul>

                    <div class="clearfix"></div>
                </div>
            </nav>

        </div>
    </div>
    <script>
        $('nav li ul').hide().removeClass('fallback');
        $('nav li').hover(
            function () {
                $('ul', this).stop().slideDown(100);
            },
            function () {
                $('ul', this).stop().slideUp(100);
            }
        );
        $('ul', this).stop().slideDown(500);
    </script>
    <a href="javascirpt:void(0)" data-toggle="modal" id="noticeLink" data-target="#noticeModel"></a>

    <!--//header-->
    <!-- TradingView Widget BEGIN
    <div class="tradingview-widget-container">
        <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
            {
            "symbols": [
                {
                "title": "S&P 500",
                "proName": "INDEX:SPX"
                },
                {
                "title": "Nasdaq 100",
                "proName": "INDEX:IUXX"
                },
                {
                "title": "EUR/USD",
                "proName": "FX_IDC:EURUSD"
                },
                {
                "title": "BTC/USD",
                "proName": "BITFINEX:BTCUSD"
                },
                {
                "title": "ETH/USD",
                "proName": "BITFINEX:ETHUSD"
                }
            ],
            "locale": "en"
            }
            </script>
        </div>
         TradingView Widget END -->