@include('home.assets')
<style>
span {
    display:block;
    font-size: 100%;
    width: 100%;
    line-height: 2rem;
    cursor: pointer;
}
</style>
</div>
<div class="agrementBlock welcome">
			<div class="container">
				<div class="w3-welcome-heading">
					<h2>Contact Us</h2>
				</div><br><br>

				<div class="row">
				<div  class="col-md-5 agileits-w3layouts-address">

					<div class="agileits-w3layouts-address-top">
                      
                      <h3>MANAMA</h3>

					</div>

					<div class="agileits-w3layouts-address-top">

						<h5>Office Address</h5>

						<ul>

							<li>SR DIGITAL WORLD SPC </li>

							<li>Office 2928 Entrance 316 ROAD 4609</li>

							<li>MANAMA.</li>

							

							



							

						</ul>

					</div>

					<div class="agileits-w3layouts-address-top">

						<h5>Email</h5>

						<ul>

							<li><a href="mailto:{{ getSupportMailFromAddress() }}"> {{ getSupportMailFromAddress() }}</a></li>

						</ul>

					</div>

				</div>

				
				<div style="float: right;float: top;" class="col-md-5 agileits-w3layouts-address">

					<div class="agileits-w3layouts-address-top">
                    <h3>Spain</h3>
					</div>

					<div class="agileits-w3layouts-address-top">

						<h5>Office Address</h5>

						<ul>

							<li>Plaza de Los Luceros, 17 Piso 9, </li>

							<li>Pta 1</li>

							<li>03004 Alicante</li>

							<li>Spain</li>

							

							



							

						</ul>

					</div>

					<div class="agileits-w3layouts-address-top">

						<h5>Email</h5>

						<ul>

							<li><a href="mailto:{{ getSupportMailFromAddress() }}"> {{ getSupportMailFromAddress() }}</a></li>

						</ul>

					</div>

				</div>

				<div class="clearfix"> </div>
				</div>
				<br><br>
						<!-- footer -->
			<div class="jarallax footer">
				<div class="container">
					<div class="footer-logo">
						<h3><a href="/">{{$settings->site_name}}</a></h3>
					</div>
					<div class="agileinfo-social-grids">
						<h4>We are social</h4>
						<div class="border"></div>
						<ul>
							<li><a href="https://www.facebook.com/bitcoin4utrading"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
					<div class="copyright">
						<p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>
					</div>
				</div>
			</div>
			<!-- //copyright -->
	</div>
</div>


</body>
</html>