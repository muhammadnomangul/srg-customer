@include('home.assets')
<style>

span {
    display:block;
    font-size: 100%;
    width: 100%;
    line-height: 2rem;
    cursor: pointer;
}
</style>
</div>
	<div class="welcome">
			<div class="container">
				<div class="w3-welcome-heading">
					<h2>Partnership Agreement</h2>
				</div>
				<div class="window roww">
					<p>
						<span>This document is a translation of the original text of the Partnership agreement Terms into English. You agree that this translation is given only for your comfort and your relations with the b4uglobal are regulated in obedience to the English version. The English version of the Partnership agreement Terms is а priority in all possible cases.</span>
						<span>This Partnership agreement (hereinafter- Partnership agreement) is a basis for participating at the b4uglobal Platform. The partnership agreement includes terms of use of the b4uglobal website, terms of use of the b4uglobal services, and regulates relations between the participants of the b4uglobal Platform.</span>
						<span>A present Partnership agreement is signed when a new participant joins the platform after passing the registration. The terms of This Partnership agreement are accepted by the participant fully, without any reservation, in the same format as the present Partnership agreement is expounded on the b4uglobal website and given for an acquaintance and acceptance by the Participant at passing the registration.</span>
						<span>If you (User) do not agree with current rules and terms, do not use the services of this website and do not call on the attended pages of the real website.</span>
					</p>
					<p>
						<span><b>1. Terms and definitions</b></span>
						<span>1.1. <b>Partner, User</b> -  Internet user, including users of the present website, users of a sane mind with the status of full civil capacity according to the legal norms; older than 18 (eighteen) years; and for citizens of the following countries: Canada, British Columbia, Nova Scotia, Nunavut, Saskatchewan, New Brunswick, Newfoundland and Labrador, The Northwest Territories, Yukon; United States: Alabama, Wyoming, Nebraska. South Korea, – older than 19 (nineteen) years of age; for citizens of the following countries: Taiwan, Tunisia, Japan, - older than twenty (20) years of age; for citizens of the following countries: Bahrain, Guinea, Honduras, Egypt, Cameroon, Ivory Coast, Lesotho, Madagascar, Monaco, Singapore, United States: Mississippi, Puerto Rico, New York, Chad - older than 21 (twenty-one) years of age; and acting consciously and willingly using the b4uglobal platform understands the rights and assumes obligations straight implied, and also those stated in the partnership agreement.</span>
						<span>1.2. <b>Company, Platform, b4uglobal</b> - b4uglobal is registered under the number 2488221, at the address SR DIGITAL WORLD SPC Taman Melawati 53100 Kuala Lumpur Malaysia.
						<br>
						The Registrar of Companies for England and Wales, hereby certified that
						<b>B4U Global LTD</b> is this day incorporated under the Companies Act 2006 as a private company, that the company is limited by shares, and the situation of its registered office is in England and Wales
						<br>
						B4U is also registered as B4U GROUP OF COMPANIES, S.L. in Europe - Spain (Mercantile register of Alicante - tomo 4226, folio 216, inscripción 4ª, hoja A-165838) with VAT NUMBER B42646133 and domiciled in Alicante, Plaza de los Luceros 17, 9th floor, 03004.
						</span>

						<span>1.3. <b>Website</b> is the Internet resource, containing information and intellectual property from information system (including computer programs, database, graphic registration of interface (design), etc.), access to which is provided from different user devices connected to the Internet, by means of special software for websites viewing (browser) of www.b4uglobal.com addresses (including domains of the next levels related to these addresses) or Mobile applications.</span>
						<span>1.4. <b>Partnership agreement</b> - this agreement, Terms of placing announcements and other rules and documents, regulating work of Trade Platform or dividing the rules of Service use, published on the website.</span>
						<span>1.5. <b>Services</b> are functional possibilities, services, and instruments accessible for Trade Platforms Partners.</span>
					</p>
					<p>
						<span><b>2. General conditions</b></span>
						<span>2.1. The b4uglobal confides the Partner to carry out advertising, consultative and informative activity, with the purpose of popularization of the b4uglobal and increase sales of products.</span>
						<span>2.2. The partner serves clients and partners of the b4uglobal, which became such as a result of advertising, consultative and informative activity, gets a reward as a honorarium.</span>
						<span>2.3. The partner can create and manage structures, and get a reward for this according to a quarry stair is Appendix 1, being an inalienable part of official agreement.</span>
						<span>2.4. The partner cannot take the means of payment over a client if only he is not authorized for this purpose outside b4uglobal. All payments are done by bank transfer and electronic payment systems – partners of the b4uglobal.</span>
						<span>2.5. The b4uglobal processes all data of clients automatically. The b4uglobal can provide information regarding the Partner and his clients to the third persons only in statutory cases.</span>
						<span>2.6. The partner saves all information, regarding clients and partners of the b4uglobal, in a secret both in the period of action of this agreement and after it ends.</span>
						<span>2.7. The partner has no authority to contract and give statements on behalf of b4uglobal, and also to conclude other transactions with the clients of b4uglobal, without his written consent.</span>
					</p>
					<p>
						<span><b>3. Obligations of the Partner on work with clients</b></span>
						<span>3.1. The partner acquaints the clients of the b4uglobal with the activity of the company and related risks conditioned by vibrations on financial markets, within the framework of individual necessities of client.</span>
						<span>3.2. The partner cannot use the aggressive methods of distribution, and also to give false and inexact information to the clients in regard to b4uglobal.</span>
						<span>3.3. The partner immediately passes to the b4uglobal all important notes of clients that touch the business of the b4uglobal.</span>
						<span>3.4. The Partner must let clients make decisions by themselves.</span>
						<span>3.5. The Partner is forbidden to give promises regarding the revenue.</span>
						<span>The Partner also has:</span>
						<span>3.6. To carry out the activity independently due to the personal funds and on the risk, covering all necessary charges (except for the condition of p.17. 6 in this Agreement).</span>
						<span>3.7. Systematically to conduct work on a selection, educating, preparation of new consultants. To organize for them educating as the realization of the specialized thematic conversations, lectures, seminars, etc. To assist in their internship under the direction of more experienced specialists and consultants, render to them permanent help and support in the development of independent activity.</span>
						<span>3.8. To organize the work of direct lower-level Partners in accordance with their qualifying requirements.</span>
						<span>3.9. In case of occurring of some contingencies negatively influencing on activity or reputation of the b4uglobal, immediately to inform of these facts the direct higher Partner and administration of the b4uglobal in writing.</span>
						<span>3.10. To take part in all obligatory events, organized b4uglobal especially related to in-plant training of the Partner.</span>
						<span>3.11. To pass educating on the specialized and qualifying seminars, organized by The b4uglobal at the advancement of the Partner on a quarry stair. Not to apply in the administration of the b4uglobal on questions of transition from one management in other, or on questions of replacement of direct higher Partner on other.</span>
						<span>3.12. Independently to determine the location, time and volume of the activity envisaged by this Agreement.</span>
						<span>3.13. If necessary, through the leader of the structure, in writing to bring in the suggestion on the increase the b4uglobal's activity efficiency.</span><br>
						<span>Information provided by the User and his actions on the b4uglobal Platform shall not:</span>
						<span>1. be false, inaccurate, or misleading;</span>
						<span>2. Facilitate fraud, deception, or abuse of confidence;</span>
						<span>3. Lead to transactions with stolen or counterfeit items;</span>
						<span>4. Violate or encroach on the property of a third party, his b4uglobal secrets, or his right to privacy of private life;</span>
						<span>5. Contain information insulting someone's honour, dignity, or business reputation;</span>
						<span>6. Contain libel or threats to anyone;</span>
						<span>7. Urge them to commit crimes and incite ethnic hatred;</span>
						<span>8. Promote, maintain, or to call for terrorist and extremist activities;</span>
						<span>9. be obscene, or be in the nature of pornography;</span>
						<span>10. contain computer viruses and other computer programs, in particular, for damage, unauthorized intrusion, secret interception or misappropriation of the data of any system or the system itself, or any part thereof, or personal information or other data (including the b4uglobal Platform data);</span>
						<span>11. Harm the b4uglobal Platform and also to cause a full or partial loss of the b4uglobal Platform services providers, the Internet, or services of any other persons;</span>
						<span>12. Contain advertising of other products;</span>
						<span>13. Infringe rights of third parties, image rights of the citizen, and other rights of third parties;</span>
						<span>14. Otherwise violate the applicable law.</span>
						<span>15. It is forbidden for the user to place Announcements on the b4uglobal Platform, to accomplish or carry out a transaction with the use of Services of the b4uglobal Platform that can result in a violation of the b4uglobal Platform and/or by User of the current legislation of User or Company's current legislation.</span>
						<span>16. The user guarantees independent reimbursement of losses and damage, caused to The b4uglobal Platform because of producing of money or another requirement by the third person, following from the use of services of the b4uglobal Platform, except for the cases when such damage arose up because of the guilty (intentional or careless) act of the b4uglobal Platform.</span>
						<span>17. Duty on a calculation and tax and other payments, following from the use of exchange services of the b4uglobal Platform lies on the User.</span>
						<span>18. A right for the use of services belongs to the User personally. The user is not right to render to the third person services (both on retribution and on the gratuitous basis) in the exchange of units of the electronic payment systems, based on the use of the services of the b4uglobal Platform. The User is obligated not to violate authorial and contiguous rights on the content of the website of service of the b4uglobal Platform acknowledging that such rights are guarded by the law. Users are obligated not to falsify the communication streams used by the b4uglobal Platform services..</span>
						<span>19. The user is obligated by the actions not to violate the legislatively set norms and rules.</span><br>
						<span>The following is strictly forbidden for the User:</span>
						<span>1. Reselling or gratuitous transmission of account to other persons forbidden (the primordial proprietor of account can manage him at any time because an account is tied to the mailing address of holder that cannot be changed).</span>
						<span>2. Bringing in of the falsified Users is forbidden.</span>
						<span>3. Registration of several accounts is strictly forbidden.</span>
						<span>4. Promising or ensuring income only for the fact of its registration is forbidden on the Platform.</span>
						<span>5. Threats, persuasions of persistent character against will man, pursuit are forbidden.</span>
						<span>6. All found out violators of any of the rules of this User Agreement (including accomplishing any attempts, encroachments, encroaching upon violations) will be excluded from the Platform permanently, on all time of existence of the Platform without warning. Thus the structure of the violator will be without a sponsor.</span>
						<span>7. The b4uglobal platform has a right in the one-sided order to give up the execution of this User agreement in regard to the Participant, on default to them, any point of this User agreement.</span>
						<span>8. The reputation of the Platform is the value of Executives and plays a qualificatory role in the Platform's activity, and Executives and Company carry out the maximally loyal activity, for maintenance of the positive reputation of the Platform.</span><br>
						<span>Participants have a right:</span>
						<span>1. To provide information to other people in an order to bring over them to participating in the b4uglobal Platform.</span>
						<span>2. To create the own web sites, pages in social networks, to blog and place on the information about the Platform for bringing in of new members in the Platform, and using for such aims any without limitations resources of network the Internet, if another is not envisaged by the current User's country legislation.</span>
						<span>3. To direct recommendations for the service system improvement in client service.</span>
						<span>4. To produce informing of separate adult persons and groups of persons, including associations of persons on interests, with the purpose of bringing in of them to participating in the Platform.</span>
						<span>5. To direct in the b4uglobal Platform system suggestions, wishes, and reviews with the purpose of improvement of work of service of the system Platform b4uglobal. This possibility is given on the page: reviews.</span>
						<span>6. To get sponsorship from The B4uglobal Company for opening and further development of consultative clubs in a size and on the terms given by the Company, more detailed on the page: club.</span>
					</p>
					<p>
						<span><b>4. Reward and career advancement</b></span>
						<span>4.1. The reward of the Partner depends on his Partner status occupied to them in the quarry stair of the company. Founding for the count of reward it is been the sum of his personal investments in a company or volume of the Partner's sales and his structure.</span>
						<span><b>The reward of the Partner is formed</b></span>
						<span>&mdash; From personal investments of the client invited by the Partner,</span>
						<span>&mdash; From gained profit that is charged extra coming from brief-cases open for the invited clients.</span>
						<span>4.2. Every Partner through the personal cabinet gets information about all b4uglobal products, sold as a result of its advertisement and consultative activity, and also its structure.The partner is right to express objections from given data in a flow 5 calendar days in the b4uglobal address</span>
						<span>4.3. A relation between corresponding Partner and his employees, being under his Executives, is determined in accordance with a quarry stair; ratified by the b4uglobal is an appendix 1 and appendix.</span>
						<span>4.4. Payment of reward of every Partner is produced in a flow not later 3-5 working days, after filing of an application.</span>
						<span>4.5. All rewards of the Partner are counted up in accordance with a quarry stair is an appendix 1 and appendix 2.</span>
						<span>4.6. At the introduction of new products, the Partner of the b4uglobal will be additionally informed in relation to rates in a quarry stair.</span>
						<span>4.7. Rewards, which the Partner gets from the employees, lower-level leaders, make a difference between the coefficient of profitableness of the Partner according to his Partner status and the coefficient of profitableness of his employee, the lower-level leader is an appendix 1.</span>
						<span>4.8. If a lower-level Partner will annul contractual relations with b4uglobal, then the maintenance of the structure created by him, higher corresponding Partner undertakes directly. The conformable re-calculation of rewards is produced.</span>
						<span>4.9. Under reaching status of Metropolis in the quarry stair of b4uglobal, the Partner has a right after to open the b4uglobal club, after the receipt of his written consent and to get the partial refinancing.</span>

					</p>
					<p>
						<span><b>5. Advertisement</b></span>
						<span>The b4uglobal allows conducting advertisement activity with the purpose of the advancement of the company in the press, on radio, television and internet without limitations.</span>
					</p>
					<p>
						<span><b>6. The b4uglobal Representative clubs:</b></span>
						<span>6.1. From the b4uglobal's writing permission Partners have a right to organize regional and structural interregional consultative clubs (The b4uglobal clubs).</span>
						<span>6.2. The b4uglobal does not carry to property and legal accountability for work of consultative clubs on places and does not take part in-process with local administrative authorities.</span>
					</p>
					<p>
						<span><b>7. Bonus program</b></span>
						<span>7.1. The b4uglobal partner has a right to get additional money bonuses from the b4uglobal according to the bonus program is an appendix 2. According to the terms of the program of bonuses, Partner must personally or together with the structure to execute the volumes of sales necessary for the implementation of terms of the bonus program. The b4uglobal bonus program is appointed on the achievement of status subject to the condition that all data are given in a profile.</span>
						<span>7.2. Partner of the b4uglobal on the achievement of TIFFANY status gets a bonus reward in $100 and brand name t-shirt.</span>
						<span>7.3. Partner of the b4uglobal on the achievement of BLUE MOON status from the description page of the agent gets a bonus reward in $650 or iPhone 6s</span>
						<span>7.4. Partner of the b4uglobal on achievement on the terms of AURORA status from the description page of the agent gets a bonus reward in $1000 or MacBook Air</span>
						<span>7.5. Partner has a right to express objections on the bonus reward given to him in a flow 5 calendar days in an address b4uglobal.</span>
						<span>7.6. At the introduction of new bonus rewards, the b4uglobal Partner will be additionally informed of all innovations, and also all changes of touching the bonus program.</span>

					</p>
					<p>
						<span><b>8. Terms of agreement</b></span>
						<span>8.1. This agreement is established for an indefinite term.</span>
						<span>8.2. This agreement can be stopped on a mutual agreement on the basis of preliminary notification month prior to its.</span>
						<span>8.3. The b4uglobal has a right to terminate the agreement without any notification in advance due to the following reasons:</span>
						<span>8.4. The Partner violates current local and international laws.</span>
						<span>8.5. The Partner compromises name of the b4uglobal by his actions.</span>
						<span>8.6. The Partner violates the terms of the agreement.</span>
						<span>8.7. The Partner makes public and unpublicized announcements in front of other partners, influencing the b4uglobal's image of.</span>
						<span>8.8. There are no sales from the partner or his structures for more than 2 months.</span>
						<span>8.9. If the Partner doesn't give exact information regarding the level of risks of the b4uglobal product or gives promises and predictions about any guaranteed income.</span>
					</p>
					<p>
						<span><b>9. Moving away from User from the Platform can be caused:</b></span>
						<span>9.1. By the cases of swindle at the use of the Platform software;</span>
						<span>9.2. By well-proven attempts of the breaking of program code or database of the Platform;</span>
						<span>9.3. In case of violation of this User agreement, especially in case of the use of the Platform's website or separate elements of the Platform's website for other purposes, besides envisaged by this User Agreement, access to the Platform's website can be limited or blocked.</span>
					</p>
					<p>
						<span><b>10. Responsibility of parties</b></span>
						<span>10.1. The company in no measure does not bear responsibility for financial losses, and any other types of Platform Participant's losses, constrained with the use of information presented on a web-site and participating in the b4uglobal Platform.</span>
						<span>10.2. Responsibility for failures and losses constrained with the use of information presented on a web-site and participating in the b4uglobal Platform, Participant realizes and undertakes to a full degree.</span>
						<span>10.3. The b4uglobal Platform bears responsibility for the exceptionally functioning of the Personal Cabinet of Participant of the Platform and execution of commissions of Participants within the framework of conditions of this Agreement.</span>
						<span>10.4. TThe b4uglobal Platform does not bear the responsibility for any violations, or limitations in the work of payment systems of the partners used for the direction of monetary resources on the Personal Accounts of Participants.</span>
						<span>10.5. The b4uglobal Platform does not bear responsibility for the change of cost of assets because of the change of courses of currencies that takes place during the implementation of transaction.</span>
						<span>10.6. The b4uglobal Platform does not bear responsibility for delays or failures in the process of operations, arising up because of an act of providence, and also any case of defects in telecommunication, computer, electric and other contiguous systems.</span>
					</p>
					<p>
						<span><b>11. Safety of monetary resources and safety of operations</b></span>
						<span>11.1. Monetary resources can be translated from the account only at the presence of order (requests) from the User. Monetary resources of Users passed in the b4uglobal Platform cannot be used in other aims.</span>
						<span>11.2. The b4uglobal platform reserves a right at any moment to check the solvency of the User (and in case of necessity - and his personality) by means of the data given during registration.</span>
						<span>11.3. The b4uglobal Platform does not bear responsibility for any of the services of giving services of automatic exchange of units of the electronic payment systems, additions to the accounts and payment of commodity on merchant websites, electronic payment systems.</span>
						<span>11.4. The b4uglobal platform doesn't check competence and legality of possession by the User of the facilities offered by the User to the exchange and does not carry out supervision after the operations of the User at any electronic payment systems.</span>
						<span>11.5. The b4uglobal Platform supports the electronic payment systems only, operations that are irrevocable in accordance with the terms of maintenance of the corresponding system.</span>
						<span>11.6. Any and every operation with participation the electronic payment systems is not subject to abolition from the moment of her completion are receipted by the User of being due to him on the before accepted terms of transaction.</span>
						<span>11.7. The e-Payments systems bear the exceptional responsibility for facilities entrusted to them by Users; Company is not a side in an agreement between the system of e-Payments and its User and in no case does not carry to responsibility for the wrong or illegal use of the corresponding system, and also for abuse of functionality of this system. A mutual right and duties of e-Payments systems and the User are regulated by the rules and agreements, accepted in the corresponding system.</span>
						<span>11.8. Return of facilities to the User or other Participant or person regardless of his role in the Platform is impossible; Participants understand the essence and terms of this Agreement.</span>
						<span>11.9. The platform and Company do not bear responsibility for receipt or no receipt by the User of the expected profit.</span>
						<span>11.10. The company and executives of the Platform do not bear responsibility for problems that can arise up in the used authorized payment systems, because is not a proprietor or joint owner of this payment system.</span>
						<span>11.11. The courses of exchange are not published on the service website and not reported to the User in the moment of acceptance to the terms of exchange operation, that set accepted service from the User and given out by service to the User volumes of assets in the systems of e-Payments. The expenses constrained with the use of the electronic system of payments and envisaged by a corresponding bilateral agreement between the User and system are paid by the User.</span>
					</p>
					<p>
						<span><b>12. Acceptance of Agreement by the User</b></span>
						<span>12.1. This User Agreement inures from the moment of acceptance by the User, by means of mark about the acceptance of terms of Agreement, during registration, when a person ticks off under this User Agreement during registration on the Website. A person takes part in the Platform understanding risks of losses and possible profits.</span>
						<span>12.2. The Platform is opened for any person including advising, but not limited to the requirements of this User Agreement, regardless of the location of person or nationality, if participation is not forbidden by the laws of the country of person.</span>
						<span>12.3. The platform gives no guarantees of profit or assertions, obvious or implied, in regard to this software, vehicle facilities, or documentation, including their quality, efficiency, commercial value, benefit or fitness for a certain aim.</span>
						<span>12.4. The user agreement sets certain legal rights the stated in this Agreement. The user can have additional rights in accordance with the laws of the state or country of the User. This User Agreement does not change the rights envisaged by the laws of the state or country of the User, if it is shut out by the laws of the state or country of the User.</span>
						<span>12.5. Putting a mark about the acceptance of this User Agreement, Participant confirms voluntarily registration in the b4uglobal Platform and gratuitous gift of facilities to the Platform, as monetary resources, in chosen independently by Participant sums.</span>
						<span>12.6. According to this User agreement, Gifts in a money equivalent, Participants which joined Platform with the same aims and understanding are given to other Participants; without some terms and obligations from the side of the Company.</span>
						<span>12.7. The Participant renounces this User agreement in the future any claims on the pursuit in the area of civil, administrative, or criminal law against the Company, its Organizers and Participants, and also any other persons involved in the realization of the b4uglobal Platform.</span>
						<span>12.8. The user agreement guarantees that between Participant of Platform and b4uglobal Platform Company, there exist no other Agreements, Contracts, or by another character concerted obligations, except this User agreement.</span>
					</p>
					<p>
						<span><b>13. Other additionals</b></span>
						<span>13.1. If at the input of information to the database an error is admitted through the fault of the b4uglobal, the Partner has a right during 15 calendar days after the establishment of this fact to demand to correct. For this aim, the Partner directs a statement in an electronic kind on the mail of b4uglobal through</span>
						<span>13.2. If the Partner possesses work experience in other companies, using the principle of the network marketing for the sale of financial products and services, then he can, from a consent b4uglobal, to occupy that position in a career that he attained in other company. Before beginning the activity as the b4uglobal Partner, it is necessary preliminary to give an extract (calculation sheet) containing information on the level attained in other company, whereupon to confirm:</span>
						<span>13.3. Aurora in the flow of 1st month from the beginning of Partner's work.</span>
						<span>13.4. Serenity status in the flow of 3rd months from the beginning of Partner's work.</span>
						<span>13.5. After the expiration of foregoing terms of confirmation, the position is counted concordantly to the actually attained results.</span>
						<span>13.6. The partner bears the personal responsibility before tax authorities of that country the citizen of that he is, and be under an obligation independently to pay income taxes, got because of participating in Platform from units, in case of presence and according to the requirements of the current tax legislation of the country in that Participant of Platform is registered as a taxpayer, or must be registered as a taxpayer.</span>
						<span>13.7. The partner is under an obligation to decide all questions related to payment of all being due from his insurance, pension, medical, and other social payments in accordance with the legislation of his country. B4uglobal does not have some obligations underpayment of all enumerated obligatory payments.</span>
						<span>13.8. In case of the death of Partner, if his quarry status is higher than Serenity, during 3rd from the day of his death part of the access of the structure of Partner is paid his legal heirs on a next chart: the first year - 75% of profit, second year - 50% of profit and the third year - 25% of profit, or, on the preliminary desire of the Partner, the attained position and structure in the career of the b4uglobal get to his heir-at-laws or on a testament, co-ordinating preliminary with the b4uglobal</span>
						<span>13.9. All Applications indicated in this agreement, and also Appendixes of relatively future b4uglobal products, are an inalienable part of this agreement. All new amendments and supplements are published on the b4uglobal website 30 days prior to their inuring.</span>
					</p>
					<p>
						<span><b>14. Disputes and legal relationships</b></span>
						<span>14.1. In dispute resolution within this Partnership agreement, the current Legislation of Italy must be in force.</span>
						<span>14.2. The company aims to settle any vexed questions or disagreements with Users by negotiations. Consequently, any arguments and disagreements will be first examined by the representatives of Platform or Executives that will try to decide a question straight with the User. Any dispute that cannot be decided by negotiations will be passed to the referenda appointed by both parties in writing. This Agreement is regulated by the legislation of Italy.</span>
						<span>14.3. Confession of separate parts of this User Agreement does not abolish the action of other provisions of this User agreement invalid.</span>
						<span>14.4. Parties agreed, that present of the Partnership agreement is not an agent agreement, by the agreement of commission or guarantee on the legislation of the country of any of Parties and does not entail the obligations, conditioned by the legislation of relatively agent agreement, agreement of commission or guarantee.</span>
					</p>
					<p>
						<span><b>15. Personal data</b></span>
						<span>15.1. The User's personal data, the provision of which is required from the User during the interaction, is securely stored in the electronic system of the Company and shall be treated as confidential and protected. In accordance with the policy of the Company, customer information is disclosed to third parties not affiliated with and only with the consent of the User, or in accordance with the normative legal regulation. Personal data may include, but is not limited, a name, date of birth, e-mail, address, bank details, information about financial status, trading activity, past transactions, account balance, as well as the forms of data collection of the client information ("know your client").</span>
						<span>15.2. Use of Personal Information of the Service Participants.</span>
						<span>15.3. The User agrees that the personal data given during his/her registration (phone, e-mail, country, etc.) will be processed by the system of the b4uglobal Service.</span>
						<span>15.4. The b4uglobal Service has the right to inform users on mentioned contacts (e-mail mailing, SMS, SMS-mailing, etc.).</span>
						<span>15.5. B4uglobal Service has the right to put comments on the b4uglobal Website indicating contact details - username, country, name, last name, after the User´s comment is received by the b4uglobal Service system.</span>
						<span>15.6. The company protects user data in confidentiality.</span>
					</p>
					<p>
						<span><b>Appendix 1.</b></span>
						<span><b>Statuses The company provides 6 statuses for investors:</b></span>
						<span></span></p><h3><b>TIFFANY</b></h3>
						<span><b>This status is given you as soon as you register an account.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>First line 3%</td>
								<td>First line 3%</td>
						   </tr>
						   <tr>
								<td>Second line 2%</td>
								<td>Second line 1%</td>
						   </tr>
						   <tr>
								<td>Third line 1%</td>
								<td>Third  line 1%</td>
						   </tr>
						   <tr>
								<td colspan="2">The weekly limit for investment is $700 / Weekly withdrawal $400</td>
						   </tr>
						</tbody></table>
						<span><h3><b>BLUE MOON</b></h3>
						</span>
						<span><b>This status is given when the turnover of your structure has reached $5,000 or the amount of your personal investment portfolio is $700.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>first line 7%</td>
								<td>first line 5%</td>
						   </tr>
						   <tr>
								<td>second line 3%</td>
								<td>second line 3%</td>
						   </tr>
						   <tr>
								<td>third line 1%</td>
								<td>third  line 1%</td>
						   </tr>
						   <tr>
								<td colspan="2">Weekly limit for investment is $1500 /  Weekly withdrawal $700</td>
						   </tr>
						</tbody></table>
						<span><h3><b>AURORA</b></h3></span>
						<span><b>This status is given when the turnover of your structure has reached $30,000 or the amount of your personal investment portfolio is $3,000.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>first line 10%</td>
								<td>first line 7%</td>
						   </tr>
						   <tr>
								<td>second line 3%</td>
								<td>second line 3%</td>
						   </tr>
						   <tr>
								<td>third line 1%</td>
								<td>third line 1%</td>
						   </tr>
						   <tr>
								<td>fourth line 1%</td>
								<td>fourth line 1%</td>
						   </tr>
						   <tr>
								<td>fifth line 1%</td>
								<td>fifth line 1%</td>
						   </tr>
						   <tr>
								<td colspan="2">Weekly limit for investment is $3500 /  Weekly withdrawal $1200</td>
						   </tr>
						</tbody></table>
						<span><h3><b>CULLINAN</b></h3></span>
						<span><b>This status is given when the turnover of your structure has reached $100,000 or the amount of your personal investment portfolio is $10,000.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>first line 10%</td>
								<td>first line 8%</td>
						   </tr>
						   <tr>
								<td>second line 5%</td>
								<td>second line 5%</td>
						   </tr>
						   <tr>
								<td>third line 3%</td>
								<td>third line 3%</td>
						   </tr>
						   <tr>
								<td>fourth line 1%</td>
								<td>fourth line 1%</td>
						   </tr>
						   <tr>
								<td>fifth line 1%</td>
								<td>fifth line 1%</td>
						   </tr>
						   	<tr>
								<td colspan="2">Weekly limit for investment is $7000 /  Weekly withdrawal $3000</td>
						   </tr>
						</tbody></table>
						<span><h3><b>SANCY</b></h3></span>
						<span><b>This status is given when the turnover of your structure has reached $500,000 or the amount of your personal investment portfolio is $30,000.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>first line 12%</td>
								<td>first line 10%</td>
						   </tr>
						   <tr>
								<td>second line 5%</td>
								<td>second line 5%</td>
						   </tr>
						   <tr>
								<td>third line 3%</td>
								<td>third line 3%</td>
						   </tr>
						   <tr>
								<td>fourth line 3%</td>
								<td>fourth line 3%</td>
						   </tr>
						   <tr>
								<td>fifth line 3%</td>
								<td>fifth line 3%</td>
						   </tr>
						   <tr>
								<td colspan="2">Weekly limit for investment is $10000 /  Weekly withdrawal $4000</td>								
						   </tr>
						</tbody></table>
						<span><h3><b>KOH I NOOR</b></h3></span>
				  <span><b>The status is given when turnover of your structure has reached $1 million <br>or amount of your personal invesment portfolio is $50,000.</b></span>
						<table border="1">
						   <tbody><tr>
								<td>Affiliate bonus from a deposit of investor which was invited by you:</td>
								<td>Affiliate bonus from the profit of investor which was invited by you:</td>
						   </tr>
						   <tr>
								<td>first line 15%</td>
								<td>first line 12%</td>
						   </tr>
						   <tr>
								<td>second line 7%</td>
								<td>second line 5%</td>
						   </tr>
						   <tr>
								<td>third line 3%</td>
								<td>third line 3%</td>
						   </tr>
						   <tr>
								<td>fourth line 3%</td>
								<td>fourth line 3%</td>
						   </tr>
						   <tr>
								<td>fifth line 3%</td>
								<td>fifth line 3%</td>
						   </tr>
						   <tr>
								<td colspan="2">Weekly limit for investment is $100000 /  Weekly withdrawal $5000</td>								
						   </tr>
						</tbody></table><br><br>
						<span><b>Appendix 2. Bonuses</b></span><br>
						<span>The b4uglobal partner has a right to get additional money bonuses from the b4uglobal according to the bonus program. According to the terms of the bonus program, the Partner must personally or together with the structure to execute the volumes of sales necessary for the implementation of terms of the bonus program. The b4uglobal bonus program is appointed on the achievement of status subject to the condition that all data are given in a profile.</span><br>
						<span>1. The b4uglobal Partner on the achievement of TIFFANY status, gets a bonus reward in $100 and a brand name t-shirt.</span><br>
						<!-- <span>2. The b4uglobal Partner on the achievement of BLUE MOON status from the page of description of <a href="https://b4uglobal.com/agent.php" style="color: #68afbd;">agent</a>, gets a bonus reward in  $650 or iPhone 6s</span><br> -->
						<span>2. The b4uglobal Partner on the achievement of AURORA status, from the page of description of an agent, gets a bonus reward in $1000 or MacBook Air</span>
					    <p></p>
				</div>
				<!-- footer -->
				<div class="jarallax footer">
					<div class="container">
						<div class="footer-logo">
							<h3><a href="/">{{$settings->site_name}}</a></h3>
						</div>
						<div class="agileinfo-social-grids">
							<h4>We are social</h4>
							<div class="border"></div>
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
								<li><a href="#"><i class="fa fa-vk"></i></a></li>
							</ul>
						</div>
						<div class="copyright">
							<p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>
						</div>
					</div>
				</div>
				<!-- //copyright -->
			</div>
		</div>


</body>
</html>