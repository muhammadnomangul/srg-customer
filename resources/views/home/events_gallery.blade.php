 @include('home.assets')

 

 
 <link rel="stylesheet" href="{{ asset('home/gallery/css/unite-gallery.css')}}">
	<!-- <script type='text/javascript' src="{{asset('home/gallery/js/jquery-11.0.min.js')}}"></script>   -->
	<script type='text/javascript' src="{{asset('home/gallery/js/unitegallery.min.js')}}"></script>	
	<script type='text/javascript' src="{{asset('home/gallery/js/ug-theme-grid.js')}}"></script>

<!-- //header-ends -->
	<!-- main content start-->
	<div class="container">
	<div id="page-wrapper">
		<div class="main-page signup-page">
			<div style="text-align: center; color:white;">
			<h3 class="title1">Album Images</h3>
		</div>
                @include('messages')
				<div class="sign-up-row widget-shadow">
                  
				
					<div class="row">
						<div id="content" class='list-group gallery'>

							@if($images->count())

								@foreach($images as $image)
								
							
								<div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>

									<a class="thumbnail fancybox" rel="ligthbox1" href="{{url('events_gallery1/')}}/{{$image->id}}">

										<img class="img-responsive" alt="" src="{{asset('/images/gallery/'.$image->cover_image)}}" />

										<div class='text-center'>
                                              
                                            <p>{{$image->name}}</p>  
							          

										</div> <!-- text-center / end -->

									</a>

								
								</div> <!-- col-6 / end -->

								@endforeach

							@endif

						</div> <!-- list-group / end -->

					</div> <!-- row / end -->
                
				</div>

			 

	
		</div>
	</div>

	
<!-- footer -->



	<div class="jarallax footer" style="margin-top:200px;">

		<div class="container">

			<div class="footer-logo">

				<h3><a href="/">{{$settings->site_name}}</a></h3>

			</div>

			<div class="agileinfo-social-grids">

				<h4>We are social</h4>

				<div class="border"></div>

				<ul>

					<li><a href="https://web.facebook.com/bitcoin4utrading/"><i class="fa fa-facebook"></i></a></li>

					<li><a href="#"><i class="fa fa-twitter"></i></a></li>

					<li><a href="#"><i class="fa fa-rss"></i></a></li>

					<li><a href="#"><i class="fa fa-vk"></i></a></li>

				</ul>

			</div>

			<div class="copyright">

				<p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>

			</div>

		</div>

	</div>

	</div>

	<!-- //copyright -->
 

	

    <script src="{{ asset('js/multi-fileinput.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/multi-theme.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/multi-popper.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/fancybox/jquery.mousewheel-3.0.4.pack.js')}}" type="text/javascript"></script>
	<!--<script src="{{ asset('js/fancybox/jquery.fancybox-1.3.4.pack.js')}}" type="text/javascript"></script>-->
	<script src="{{ asset('js/jquery.fancybox.min.js')}}"></script>
	<script type="text/javascript">
  
		$(document).ready(function(){
			$("a[rel=ligthbox1]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
		});
	</script>

	<script type="text/javascript">

		/* init Jarallax */

		$('.jarallax').jarallax({

			speed: 0.5,

			imgWidth: 1366,

			imgHeight: 768

		})

	</script>

 </body>

</html>
	 

