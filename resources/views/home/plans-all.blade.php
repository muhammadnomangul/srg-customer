<style>
    hr {
        margin: 0px !important;

    }

    h3 {
        margin: 0px !important;

    }

    <!--
    .agileits-services-info {
        border-bottom: 1px solid #000;
        padding-bottom: 2px;
    }

    -->
    .services-info {
        border-bottom: 1px solid #000;
        padding-bottom: 22px;
    }

    .textfont {
        font-size: 13px;
        margin: 0 auto;
    }
</style>

<div class="container">
    <div class="w3-welcome-heading">
        <h3>Investment Plans</h3>
    </div>

    <div class="col-lg-12">
        <div class="alert alert-danger">
            <b>Only for Pakistan / Pakistani National</b>
            <p>Pakistani national, must be deposit more than <b>$710 </b>, Otherwise your deposit/portfolio in banks will not approve and consider as dummy</p>
        </div>
    </div>


    @foreach($pplans as $plans)

        <div class="col-md-4">
            <div class="sign-up-row agile-news-info" style="width:100%; margin-bottom:10px; text-align:left;">
                <h3><img style="width:70px;" src="{{asset('images/'.$plans->img_url)}}"> {{$plans->name}} </h3>
                <hr>
                <p><font color="blue"><i class="fa fa-star"></i></font> Criteria For Join This Plan : </p>
                @if($plans->personel_investment_limit == 699)
                    <p class="textfont"><i class="fa fa-"></i>Personel Investment Min $50 And Max
                        : {{$settings->currency}}{{$plans->personel_investment_limit}} </p>
                @else
                    <p class="textfont"><i class="fa fa-"></i>Personel Investment
                        : {{$settings->currency}}{{$plans->personel_investment_limit}} </p>
                @endif
                <p class="textfont">OR</p>
                @if($plans->personel_investment_limit == 699)
                    <p class="textfont"><i class="fa fa-"></i>Structural Investment Min $50 And Max
                        : {{$settings->currency}}{{$plans->structural_investment_limit}} </p>
                @else
                    <p class="textfont"><i class="fa fa-"></i>Structural Investment
                        : {{$settings->currency}}{{$plans->structural_investment_limit}} with </p>
                    <p class="textfont"><i class="fa fa-"></i>Personal Investment
                        : {{$settings->currency}}{{$plans->structural_personal_min}} </p>
                @endif
                <hr>
                <strong>Referal Bonuses : </strong>
                <table style="width:100%;">
                    <tr>
                        <td style="width:50%;">

                            <div class="agileits-services-info">
                                <h6>Investment Bonus</h6>
                                <hr>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px;   width:70%;">First Line:</div>
                                    <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->first_line}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px;   width:70%;">Second Line:</div>
                                    <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->second_line}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px;   width:70%;">Third Line:</div>
                                    <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->third_line}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px;   width:70%;">Fourth Line:</div>
                                    <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fourth_line}}
                                        %
                                    </div>
                                </div>
                                <div class="">
                                    <div style="float:left; width:70%;">Fifth Line:</div>
                                    <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fifth_line}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">

                                </div>
                            </div>
                        </td>
                        <td style="width:50%;">
                            <div class="agileits-services-info">
                                <h6>Profit Bonus</h6>
                                <hr>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px; width:70%;">First Line:</div>
                                    <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->first_pline}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left;  font-size: 14px; width:70%;">Second Line:</div>
                                    <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->second_pline}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px; width:70%;">Third Line:</div>
                                    <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->third_pline}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">
                                    <div style="float:left; font-size: 14px; width:70%;">Fourth Line:</div>
                                    <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->fourth_pline}}
                                        %
                                    </div>
                                </div>
                                <div class="">
                                    <div style="float:left; font-size: 14px; width:70%;">Fifth Line:</div>
                                    <div style="float:right; font-size: 14px; width:30%; color:red; text-align:left;">{{$plans->fifth_pline}}
                                        %
                                    </div>
                                </div>
                                <div class="services-info">

                                </div>
                            </div>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    @endforeach
</div>