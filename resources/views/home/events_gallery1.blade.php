@include('home.assets')

<link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet">
<link href="{{ asset('css/jquery.fancybox.min.css')}}" rel="stylesheet">


<script src="{{ asset('js/jquery-1.12.4.js')}}"></script>

<script src="{{ asset('js/jquery-1.11.1.min.js')}}"></script>
<link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet">


<!-- //header-ends -->
<!-- main content start-->
<div class="container"
     style="background-color: white ;padding-bottom: 500px; margin-top: 30px;padding-top: 20px; border-radius: 10px; padding: 30px 40px 300px 40px; ">
    <div id="page-wrapper">
    <!-- <small><a class="btn btn-danger btn-sm" href="{{ url('events_gallery') }}"><i class="fa fa-arrow-left"></i>
                Galleries</a></small> -->
        <div>

            <h3 style="text-align: left; "> {{isset($images3->name) ? $images3->name : ''}}</h3>
            <small>Dated: {{ empty($images3->created_at)? '-'  :$images3->created_at->format('d-m-Y')}}</small>
            <p style="text-align: left; ">{{isset($images3->description) ?  $images3->description : ''}}</p>

        </div>
        @include('messages')
        <div class="sign-up-row widget-shadow">


            <div class="row">
                <div id="content" class='list-group gallery'>

                    @if($images1->count())

                        @foreach($images1 as $image)


                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>

                                <a class="thumbnail fancybox" rel="ligthbox"
                                   href="{{url('/images/gallery/'.$image->image_name)}}">

                                    <img class="img-responsive" alt=""
                                         src="{{asset('/images/gallery/'.$image->image_name)}}"/>

                                    <div class='text-center'>

                                        <small class='text-muted'>{{ $image->title }}</small>

                                    </div> <!-- text-center / end -->

                                </a>


                            </div> <!-- col-6 / end -->
                        @endforeach

                    @endif

                </div> <!-- list-group / end -->

            </div> <!-- row / end -->

        </div>


    </div>
</div>


<!-- footer -->


<div class="container">
    <div class="jarallax footer" style="background-image: none !important ;margin-top:200px;">


        <div class="footer-logo">

            <h3><a href="/">{{$settings->site_name}}</a></h3>

        </div>

        <div class="agileinfo-social-grids">

            <h4>We are social</h4>

            <div class="border"></div>

            <ul>

                <li><a href="https://web.facebook.com/bitcoin4utrading/"><i class="fa fa-facebook"></i></a></li>

                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                <li><a href="#"><i class="fa fa-rss"></i></a></li>

                <li><a href="#"><i class="fa fa-vk"></i></a></li>

            </ul>

        </div>

        <div class="copyright">

            <p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>

        </div>


    </div>
</div>


<!-- //copyright -->


<script src="{{ asset('js/multi-popper.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/fancybox/jquery.mousewheel-3.0.4.pack.js')}}" type="text/javascript"></script>
<!--<script src="{{ asset('js/fancybox/jquery.fancybox-1.3.4.pack.js')}}" type="text/javascript"></script>-->
<script src="{{ asset('js/jquery.fancybox.min.js')}}"></script>


<script type="text/javascript">


    $(document).ready(function () {
        $("a[rel=ligthbox]").fancybox({
            'transitionIn': 'none',
            'transitionOut': 'none',
            'titlePosition': 'over',
            'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    });
</script>
</body>

</html>