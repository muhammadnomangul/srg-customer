<!DOCTYPE html>
<html lang="en">
<head>
	@include('home/assetss')
	<style>
	.text-center{text-align:center!important;padding: 20px;}
	
	</style>
	
	<!--<link href="{{asset('css/jquery-ui.css')}}" rel="stylesheet">
	<script src="{{asset('js/jquery-ui.js')}}"></script>-->
	 <link rel="stylesheet" href="{{ asset('home/gallery/css/unite-gallery.css')}}">
	<script type='text/javascript' src="{{asset('home/gallery/js/jquery-11.0.min.js')}}"></script> 
	<script type='text/javascript' src="{{asset('home/gallery/js/unitegallery.min.js')}}"></script> 
	<script type='text/javascript' src="{{asset('home/gallery/js/ug-theme-grid.js')}}"></script> 

</head>

<body class="auth-page" style="background-color:#f8f8f8;">

    <!-- Wrapper Starts -->
    <div class="wrapper">
		<div class="container user-auth" style="padding:20px;">
			<div class="row">
			<!-- Logo Starts -->
				<div class="text-center">
				@if(isset($settings->logo))
					<a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
						<img id="logo" class="img-responsive" src="{{ asset('images/b4u-logo.png')}}" alt="logo" style="margin-left: auto; margin-right: auto;">
					</a>
				@else
					<a style="text-align:center; width:100%;" href="{{ url('/') }}">
						<img src="{{ asset('images/b4u-logo.png') }}" style="margin-left: auto; margin-right: auto;">
					</a>
				@endif
				</div>
			</div>
			<div class="row">
				<h3 class="title1">EVENTS Gallery</h3>
				<?php $count = 1; ?>
				@foreach($AlbumImages as $images)
				<div id="gallery{{$count}}" style="display:none;">
					@foreach($images->AlbumhasManyImages as $aimages)
					<img style="width: 100%; height: 100%;"  
						src="{{asset('images/gallery/'.$aimages->image_name)}}"
						data-image="{{asset('images/gallery/'.$aimages->image_name)}}"
						data-description="Preview {{$aimages->title}}"
					"> 
					@endforeach  
				</div>
				<?php $count++; ?>
				@endforeach  
			</div>
		</div>
	</div>
	<script type="text/javascript">

		jQuery(document).ready(function(){
			var total = {{$count}};
			
			for(var i=1; i<total; i++)
			{
				jQuery("#gallery"+i).unitegallery({
					theme_panel_position: "bottom"		
				});
			}
			
		});
		
	</script>
</body>

</html>
	