@include('home.assets')
<style>
span {
    display:block;
    font-size: 100%;
    width: 100%;
    line-height: 2rem;
    /*cursor: pointer;*/
}
</style>
</div>
<div class="agrementBlock welcome">
	<div class="container">
		<div class="w3-welcome-heading" style="padding-bottom: 10px;">
			<h2>Terms and Conditions</h2>
		</div>
		<div class="window row">
			<!-- <p>
				<span>The present Agreement is concluded between management of b4uglobal.com platform in the person of b4uglobal company (hereinafter – Service) and users of company’s services (hereinafter – Users) registered in b4uglobal.com system, and it defines the principles of interaction, as well as the extent of the parties’ liability.</span>
				<span>The present Agreement is binding upon both Service and Users.</span>
			</p> -->
			<p style="text-align:justify;">
				<span><strong>DECLAMATION! DON'T PAY CASH TO ANY ONE, ONLY PAY ONLINE VIA WEBSITE MATHODES.</strong></span>
				<span><b>1. User agreement</b></span>
				<span>1.1. The given document presents an English version of the original text of the Terms of the User agreement. The User accepts that this version is for convenience in reading only. Relations between the User and the B4uglobal platform are regulated according to the English version. In all cases, the English version of terms of the User Agreement is а priority.</span>
				<span>1.2. The User Agreement is signed between the B4uglobal platform Representative and the Users of the company services and is a condition for participation in the B4uglobal platform. The User Agreement includes terms of use of the B4uglobal platform website, terms of use of B4uglobal services, regulates relations between the B4uglobal participants and is a degree on parties’ responsibilities.</span>
				<span>1.3. The present User agreement is considered signed by a new participant to join the B4uglobal platform after his registration. The terms of this User Agreement are to be accepted by a participant in full, without any reservation and in the same format as set out on the B4uglobal website provided for information and recognition by the participant during registration.</span>
				<span>In case the User does not agree with the presented terms and conditions, he should not use the services of this website and should not go to the associated pages of this website.</span>
			</p>
			
			<p style="text-align:justify;">
				<span><b>2. Terms and definitions</b></span>
				<span>2.1. <b>User</b> -  Internet user, including users of the website, users of a sane mind with the status of full civil capacity according to the legal norms; older than 18 (eighteen) years; and for citizens of the following countries: Malaysia, Singapore, Indonesia, Cambodia, Thailand, Burma, Turki, Spain, Kazakhstan and Pakistan. Consciously and willingly using the B4uglobal platform understands the rights and assumes obligations straight implied, and also those stated in the User agreement.</span>
				<span>2.2. <b>Company, Platform, b4uglobal</b> - B4uglobal is registered under the number 2488221, at the address SR DIGITAL WORLD SPC Taman Melawati 53100 Kuala Lumpur Malaysia.
					<br>
					The Registrar of companies for England and Wales, hereby certified that
					<b>B4U Global LTD</b> is this day incorporated under the Companies Act 2006 as a private company, that the company is limited by shares, and the situation of its registered office is in England and Wales
					<br>
					B4U is also registered as B4U GROUP OF COMPANIES, S.L. in Europe - Spain (Mercantile register of Alicante - tomo 4226, folio 216, inscripción 4ª, hoja A-165838) with VAT NUMBER B42646133 and domiciled in Alicante, Plaza de los Luceros 17, 9th floor, 03004.
				</span>
				<span>2.3. <b>Website</b> - is the internet resource, containing the totality of contained information  and intellectual property (including computer programs, database, graphic registration of interface (design), etc) in information system and objects of intellectual property, access to which is provided from different user devices connected to the Internet by means of special software for websites viewing (browser) of www.b4uglobal.com (including the domains of the next levels related to these addresses).</span>
				<span>2.4. <b>User agreement</b> – is the presented agreement, terms of placing announcements and other rules and documents, regulating work of the b4uglobal platform or dividing the rules of the Service use, published on the website.</span>
				<span>2.5. <b>Services</b> - are functional possibilities, services and facilities accessible on the b4uglobal platforms for Users.</span>
				<span>2.6. <b>An account</b> - is registration data, unique login (address of e-mail) and password, created independently by the User in the process of Registration on the Website or changed in future by the User through the Account Settings, used for access in the Account Settings after authorizing of the User on the b4uglobal platform.</span>
				<span>2.7. <b>A login of the User</b> - is an unchangeable internal electronic code, that is appropriated to every User by the b4uglobal platform passing registration on the Company's website www.b4uglobal.com. After the approval of login and password, the User actually gets access to the services rendered by the b4uglobal platform. A login is the programmatic means used by the b4uglobal platform, for the account of mutual rights and duties of Platform Users, and system of the b4uglobal platform. A login is required for User identification for determination, in what size and to what bank account in the payment system it is necessary to transfer sums.</span>
				<span>2.8. <b>A password of User</b> - is a changeable internal electronic code that is chosen by the User and appropriated by the b4uglobal platform to every User passed registration on the Company's website www.b4uglobal.com. After the appropriation of password and login, the Participant actually gets access to the services rendered by the b4uglobal. Password of User is the programmatic means used by the b4uglobal platform for User identification for his access to Account Settings.</span>
				<span>2.9. <b>Registration</b> is totality of User activities, corresponding to indicated b4uglobal platform instructions, and also corresponding to the User agreement, that includes the grant for Registration data and other information, accomplished by User with the use of unique function of user interface of the b4uglobal Project to form the Personal account and to access to separate b4uglobal Project Services.</span>
				<span>2.10. <b>Data</b> is any material and information, which the User has delivered to the b4uglobal platform in connection with the b4uglobal platform. </span>
			</p>
			<p style="text-align:justify;">
				<span><b>3. General Terms and Conditions</b></span>
				<span>3.1. Interaction between Service and Users is effected via b4uglobal platform — an online resource located at www.b4uglobal.com.</span>
				<span>3.2. To use services provided by the Platform, each User must be registered on the Platform website and logged in.</span>
				<span>3.3. A decision on registration the website is made exclusively by the User.</span>
				<span>3.4. It is forbidden to sign various accounts for one person, including accounts with various email addresses. Each User may be registered only once and must use only one user account (otherwise, his account will be locked out with no chance for recovery). </span>
				<span>3.5. By being registered on the B4uglobal platform website users accept terms of Service which are stipulated in this Agreement, and they are obliged to comply with the rules of operation on Platform.</span>
				<span>3.6. The User should realize that all photo-, video- and text information at the B4uglobal platform website is a subject of copyright matter; it is provided exclusively for introductory and recommendation purposes.</span>
				<span>3.7. The User bears responsibility for using information received on the B4uglobal platform website.</span>
				<span>3.8. The minimal amount of investments - $50, the maximum amount of investments is $1,000,000 or equal currency per month depending on the status of the investor.* the minimum profit to withdraw - $25 currency.</span>
				<span>3.9. Investors’ income is from 7% to 20% per month of the investment portfolio extent.</span>
				<span>3.10. Daily accrual of profit follows the B4uglobal results (except Saturday, Sunday and holidays).</span>
				<span>3.11. The recommended investment term is 6 months and more. Considering the fee for pre-term withdrawal, we recommend investing funds within this minimum term or withdrawing a maximum of 30% of the portfolio with no fee.</span>
				<span>3.12. <b>Fee</b></span>
				<span>
					<table class="table table-bordered">
					  <thead>
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">Minimum Amount</th>
					      <th scope="col">Maximum Amount</th>
					      <th scope="col">Fee</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <th scope="row">1</th>
					      <td>$50.00</td>
					      <td>$300.00</td>
					      <td>$0.00</td>
					    </tr>
					    <tr>
					      <th scope="row">2</th>
					      <td>$301.00</td>
					      <td>$1000.00</td>
					      <td>$10.00</td>
					    </tr>
					    <tr>
					      <th scope="row">3</th>
					      <td>$1001.00</td>
					      <td>$1 Million</td>
					      <td>$30.00</td>
					    </tr>
					    <tr>
					      <td colspan="4">
					      	* If more than one deposit of the same currency, the sum of all depicted amount meet the fee criteria, the deposit fee will be deducted accordingly.<br/>
					      	*No fee will be deducted on different currency deposits if each currency's deposit is less than $301 in one day.
					      </td>
					    </tr>
					  </tbody>
					</table>

				</span>
						
				<span>- Fee for profit withdrawal upto 500USD - 5USD;</span>
				<span>- Fee for profit withdrawal more than 500USD - 15USD;</span>
				<span>Requirements for applications handling for withdrawal of funds or profit – from 3 to 5 working days. Application for withdrawal may be executed on any day.</span>
				<span>b4uglobal operating schedule – daily, from Monday to Friday.</span>

					<span>3.13. Affiliate status program</span>
				<span><b>TIFFANY</b> – this status is credited immediately after you have registered your account.</span>
				<span>The affiliate bonus from deposit of the investor you invited will make:</span>

				<span>First line - 3%, Second line - 2%, Third line - 1%, fourth line -0%,fifth line - 0%
				.</span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>first line - 3%, second line - 1%, third line - 1%, fourth line - 0%, fifth line - 0%.</span>
				<span><b>BLUE MOON</b> - this status is credited when the turnover of your venture has reached $5,000, or the volume of personal investment portfolio has reached $700.</span>
				<span>The affiliate bonus from deposit the investor you invited will make:</span>
				<span>first line - 7%, second line - 3%, third line - 1%, fourth line - 0%, fifth line - 0%.</span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>first line 5%, second line 3%,third line 1%,fourth line 0%,fifth line 0%.</span>
						
				<span><b>AURORA</b> – this status is credited when the turnover of your venture has reached $30,000 or the volume of personal investment portfolio has reached $3,000.</span>
				<span>The affiliate bonus from deposit of the investor you invited will make:</span>
				<span>first line 10%, second line 3%, third line 1%,fourth line 1%, fifth line 1%,						    </span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>
				    first line 7%, second line 3%, third line 1%,fourth line 1%,fifth line 1%.</span>
				<span><b>CULLINAN</b> – this status is given when turnover of your structure has reached $100,000 or amount of your personal investment portfolio is $10,000.</span>
				<span>The affiliate bonus from deposit of the investor you invited will make:</span>
				<span>first line 10%, second line 5%, third line 3%,fourth line 1%, fifth line 1%</span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>first line 8%, second line 5%, third line 3%,fourth line 1%,fifth line 1%</span>
				<span><b>SANCY</b> – this status is given when turnover of your structure has reached $500,000 or amount of your personal investment portfolio is $30,000.</span>
				<span>The affiliate bonus from deposit of the investor you invited will make:</span>
				<span>first line 12%, second line 5%,third line 3%, fourth line 3%, fifth line 3%</span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>first line 10%, second line 5%, third line 3%,  fourth line 3%, fifth line 3%</span>
				<span><b>KOH I NOOR</b> – the status is assigned when the turnover of your structure reaches $1 million.<br>Co-founder+ allows you to receive a one-time bonus of $3,000 for every next $100,000* attracted into your structure.</span>
				<span>The affiliate bonus from deposit of the investor you invited will make:</span>
				<span>first line 15%, second line 7%, third line 3%,fourth line 3%, fifth line 3%</span>
				<span>The affiliate bonus from profit received by the investor you invited will make:</span>
				<span>first line 12%, second line 5%, third line 3%,fourth line 3%, fifth line 3%</span>
				<span>* To calculate round of $100,000 taken into account the total amount of funds attracted by your partners from 1st to 5th lines. Once someone from your partners reaches Co-founder+ status, financial turnover of his structure is not included into calculation of the next round.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>4. User's personal account</b></span>
				<span>4.1. Use of the Service implies the use of a personal account that contains a real E-mail address, unique login and payment details.</span>
				<span>4.2. During account registration, the User is recommended to use unique data: login and password.</span>
				<span>4.3. The account is the User’s main tool to receive information and financial benefits.</span>
				<span>4.4. The account may be blocked temporarily or permanently if the User breaches this Agreement.</span>
				<span>4.5. Account details may be changed after the Service receives a corresponding request from the User.</span>
				<span>4.6. Personal account data is under reliable technical protection and it is not subject to disclosure or transfer to a third party.</span>
				<span>4.7. The account may be deleted in case there is no activity from the User within 6 months.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>5. User’s rights and obligations</b></span>
				<span>5.1. User is entitled to:</span>
				<span>- register and use one personal account;</span>
				<span>- use information obtained on the B4uglobal platform Website to gain financial profit for personal use and under personal responsibility;</span>
				<span>- use marketing tools to receive extra compensation according to the affiliate program terms;</span>
				<span>- introduce any required changes into personal account settings by filing a corresponding application;</span>
				<span>- obtain credible and prompt information.</span>
				<span>5.1.1. Provide information to other people in an order to bring over them to participating in the B4uglobal platform;</span>
				<span>5.1.2. Create own websites and provide information about the Platform to affiliate new members, and to use for this purpose any resources of the Internet without limitations, if another is envisaged by the User's country current legislation;</span>
				<span>5.1.3. Inform other adult persons and groups of persons, including associations of persons on interests to further affiliate them to the B4uglobal platform;</span>
				<span>5.1.4. Create own blogs and other web-resources, including personally-oriented, and to place on their information about the B4uglobal platform for the purpose to affiliate new Users to the B4uglobal platform;</span>
				<span>5.1.5. Deliver proposals and reviews to the B4uglobal Company for improvement of the B4uglobal platform service work;</span>
				<span>5.1.6.To require from the B4uglobal Company implementation of the Agreement terms.</span>
				<span>5.2. The user of the B4uglobal platform is obliged to:</span>
				<span>5.2.1. To register a personal account to use the B4uglobal platform;</span>
				<span>5.2.2. To provide safety of login, password and financial password for access to the Account Settings and the Personal Account in the B4uglobal platform, not to divulge and not to hand them over to third parties;</span>
				<span>5.2.3. To adhere to the rules of work with the B4uglobal platform Website and this Agreement.</span>
				<span>5.2.4. To use anti-virus software for prevention of unauthorized division to his account;</span>
				<span>5.2.5. To report to the Service in case of loss of password or in case third parties received an access to data of the account.</span>
				<span>5.2.6. To make deposit payments by conscientious transaction;</span>
				<span>5.2.7. Not to distribute slanderous and negative statements relating to the Company, the B4uglobal platform, its Organizers and Users, and also any other persons involved in the B4uglobal platform work: verbally, in written form, and also in mass media, including the Internet Otherwise the account will be blocked without possibility of the renewal.</span>
				<span>5.2.8. Not to pretend to be organizer, proprietor of the B4uglobal platform (if is not one), any presentation must be limited to the name "Participant", "User";</span>
				<span>5.2.9. Not to use status of the organizer, proprietor, or authorized representative of the B4uglobal platform during realization of simple standard actions equal to the actions for realization of rights and plenary powers of the User, any presentation must be limited to the name "Participant" or "User";</span>
				<span>5.2.10. To independently pay income taxes, got from participating in the Platform, in case of presence and according to the requirements of the current tax legislation of the country where User is registered as a taxpayer, or must be registered as a taxpayer.</span>
				<span>5.2.11. Information given by User and his operations B4uglobal platform must not:</span>
				<span>-	Be false, inexact or entering in an error;</span>
				<span>-	Assist a swindle, deception or breach of trust;</span>
				<span>-	Conduce to the finance of transactions with the stolen or counterfeit objects;</span>
				<span>-	Violate or trench upon property of the third party, their commercial secrets, or their right on inviolability of private life;</span>
				<span>-	Contain information, offending somebody's honour, dignity or business reputation;</span>
				<span>-	Contain slander or threats to anyone;</span>
				<span>-	Call for crimes and to provoke international conflict;</span>
				<span>-	Promote, support or call for terrorist and extremist activity;</span>
				<span>-	Be obscene, or have pornography character;</span>
				<span>-	Contain computer viruses, and also another computer programs directed, in particular, on harming, unauthorized encroachment, secret intercept is or an appropriation of data of any system of either the system, either its part or personal information or another data (including these Platforms of  B4uglobal);</span>
				<span>-	Cause harm to the B4uglobal platform, and also become a reason of complete or partial loss of the B4uglobal platform services of the Internet network providers or services of any another parties;</span>
				<span>-	Contain materials of advertising character;</span>
				<span>-	Violate intellectual rights for the third party, right on the image of citizen, and other rights for the third parties.</span>
				<span>5.2.12. The User is notified that in case of violation of this Agreement Service he has a right to apply the measures of influence as blocking of the account to finding out of circumstances.</span>
				<span>5.2.13. It is forbidden for the User to accomplish or carry out a transaction with the use of the B4uglobal platform Services, which can result in the violation of the B4uglobal platform and/or by User of User's or Company's current legislation.</span>
				<span>5.2.14. The User guarantees independent reimbursement of losses and damage, caused to the B4uglobal platform because of money production or another requirement by the third party, following from the use of the B4uglobal platform services.</span>
				<span>5.2.15. A right for the services´ use belongs to the User personally. The User has no right to render services to the third parties (both on retribution and gratuitous basis) in the exchange of units of the electronic payment systems, based on the use of the services of the B4uglobal platform. The User is obligated not to violate authorial and contiguous rights on the content of the B4uglobal platform website acknowledging that such rights are guarded by the law. The Users are obliged not to falsify the communication streams used by the B4uglobal services.</span>
				<span>5.2.16. The User is obligated not to violate the legally set norms and rules in his activities.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>6. Rights and obligations of Company</b></span>
				<span>Account blocking may occur in one of the following cases:</span>
				<span>- Transfer of access to other users.</span>
				<span>- Multiple accounts registration.</span>
				<span>- Use of supplementary software and bots by the Service.</span>
				<span>- Attempts of willful misrepresentation and deceiving of other users.</span>
				<span>- Insults of users, employees or any negative impairment towards the Service.</span>
				<span>- Distribution of negative information about the Service, as well as in any other cases when the User comprises a threat to the Service and/or to other users.</span>
				<span>6.1. The company is responsible for technical maintenance of the Website and provides round-the-clock access to Users’ accounts.</span>
				<span>6.2. Company is entitled to:</span>
				<span>-	Require the User to undergo complete identification if his account is blocked in the result of non-compliance with the present Agreement;</span>
				<span>-	Use the e-mail address of the User to send system notifications, newsletters, as well as marketing information; </span>
				<span>-	Deny the User in services providing without giving a reason;</span>
				<span>-	Change the terms of cooperation at its discretion upon Users notice.</span>

				<span>6.3. Company is obliged to:</span>
                <span>-	provide round-the-clock access to the Website;</span>
				<span>-	ensure Users’ personal data safety and not disclose it to any third parties;</span>
				<span>-	timely and in full inform Users about all changes and developments;</span>
				<span>-	Accrue financial remuneration to Users as a result of cooperation with the B4uglobal platform website (regular accruals of financial profit and remuneration for participation in the affiliate program) and process Users’ applications for deposits and withdrawals of funds.</span>
				<span>
               B4UGLOBAL is obliged, in the application of the provisions of data protection and anti-money laundering regulations to request additional information to learn more about you as a User. In this regard, depending on the type of user, it will be entitled to require information such as ID, residence card, foreign identity card or passport, deeds, shareholding structure, accreditation of business activity, etc. If the User denies providing the required information, B4UGLOBAL is entitled to suspend the account until that information is provided.
                </span>
			</p>
			<p style="text-align:justify;">
				<span><b>7.	The account can be blocked in the following cases:</b></span>
				<span>-	Transfer of access to other users.</span>
				<span>-	Registration  of multiple accounts.</span>
				<span>-	In case of family members accounts managed from the same IP (same location),the main account, the 1st one on the network tree, must be the one with the highest amount invested. Failure to comply with this rule will result in the blocking of all affected accounts.</span>
		    	<span>-	Use of the additional programs imitating activity of registration record.</span>
				<span>-	Intentional attempts of deception and introduction in an error of other users.</span>
				<span>-	Insults of Users, employees or causing of any negative toward the Service.</span>
				<span>-	Distribution of negative information about the Service.</span>
				<span>-	Similarly in any other cases,  where the decision of the Service defines that the user carries  threat for the Service and/or for other users.</span>
						
			</p>
			<p style="text-align:justify;">
				<span><b>8.	Financial mutual settlements</b></span>
				<span>8.1. Acceptance of the  present Agreement terms implies possible participation in an investment process and transfer of financial funds to discretionary management of the Service.</span>
				<span>8.2. Discretionary management is performed by acceptance of financial funds. It is implemented via electronic payment systems.</span>
				<span>8.3. Deposit and subsequent withdrawal of financial funds is implemented by the User himself via suggested payment systems: Ethereum, Bitcoin and others.</span>
				<span>8.4. Additional expenses in the form of fee for deposit and subsequent withdrawal are limited by the User. </span>
				<span>8.5. Financial profit as a result of the Service’s actions within the framework of discretionary management is automatically accrued to the User’s balance in the amount declared in «For investors» Section on this Website.</span>
				<span>8.6. Withdrawal of funds is made according to a manually processed User’s application.</span>
				<span>8.7. Withdrawal waiting time may take 3-5 days after application for funds withdrawal is created and confirmed by the User.</span>
				<span>8.8. User is notified and agrees that waiting period for withdrawal declared in the present Agreement may be prolonged due to payment processors operation failure, as well as in case of force-major. The User acknowledges that these factors are beyond competence and responsibility of the Service.</span>
				<span>8.9. The User is warned and agrees that an index of profit can be negative.  The User acknowledges that these factors are beyond scope and sphere of the Service responsibility.</span>
				<span>8.10. Parties agree that this Agreement was not the agreement of confidence management by property, agency service, or guarantee on the legislation of the country of any of Parties and does not entail the obligations conditioned by the legislation of relatively confidence management property, agency service, or guarantee.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>9.	Responsibility of Parties</b></span>
				<span>9.1. The system does not carry responsibility in any case for Users' success or Users' failure of actions, constrained with the use of the Platform on the website. The project and the Company do not bear responsibility for receipt or non-receipt by the User of the expected profit.</span>
				<span>9.2. A company in no way bear responsibility for financial losses, and any other types of User's losses, constrained with the use of information presented on the Website and participating in the Company activity.</span>
				<span>9.3. The User, realizing all risks in full, takes responsibility for failures and losses constrained with the use of information presented on the website and participating in the B4uglobal Project. </span>
				<span>9.4. The B4uglobal Company bears responsibility for exclusive functioning of the User Account Settings of the Platform and exact execution of commissions of Users within the framework of terms of this Agreement.</span>
				<span>9.5. The B4uglobal Company does not bear responsibility for any violations, or limitations in-process the payment systems used for the direction of monetary resources to the Personal Accounts of Users.</span>
				<span>9.6. The B4uglobal Company does not bear responsibility for the change of cost of assets because of the change of crypto-currencies courses, that take place during the implementation of the transaction, but similarly on the whole at the market.</span>
				<span>9.7. The B4uglobal Company does not bear responsibility for delays or failures in the process of finance operations, arising up because of an act of providence, and also any case of defects in telecommunication, computer, electric and other contiguous systems.</span>
				<span>9.8. Monetary resources can be transferred from the account only at the presence of order (requests) from the User.</span>
				<span>9.9. The B4uglobal Project has reserved the right at any moment to check the solvency of the User (and in case of necessity - and his personality) by means of the data given during registration.</span>
				<span>9.10. The B4uglobal Project does not carry responsibility for any of the services of automatic exchange of units of the electronic payment systems, additions to the accounts and payment of commodity on merchant sites, electronic payment systems.</span>
				<span>9.11. The B4uglobal Project doesn't check a competence and legality of User's possession of the facilities offered by the User to the exchange and does not carry out supervision after the operations of the User in none of the electronic payment systems.</span>
				<span>9.12. The B4uglobal Project supports only those electronic payment systems, where operations are irrevocable in accordance with the terms of the corresponding system maintenance.</span>
				<span>9.13. Any and every operation with participation in the electronic payment systems is not subject to abolition from the moment of its completion - from receiving by User of what being due to him before accepted terms of transaction.</span>
				<span>9.14. The systems of e-payments bear the exceptional responsibility for facilities entrusted to them by the Users The Company is not a party in an agreement between the system of e-payments and its User and no case bears responsibility for the wrong or illegal use of the corresponding system, and also for abuse of functionality of this system. The mutual right and duties of the User and systems of e-payments are regulated by the rules and agreements, accepted in the corresponding system.</span>
				<span>9.15. Return of facilities to the User, or other User, or person regardless of his role in the Project is impossible. Participants understand the essence and terms of this Agreement. </span>
				<span>9.16. The company and executives of the Project do not bear responsibility for problems that can arise up in the used authorized payment systems, because the Company is not a proprietor or joint owner of payment systems.</span>

				<span>9.17. The company and executives of the Project do not bear responsibility for problems, breaking, scums, that can rise exchange course, on which the B4uglobal is based, and that can cause the loss of facilities (altcoins). The Company is not a proprietor or joint owner of this payment system.</span>

				<span>9.18. Exchange courses are not published on the Service Website and not reported to the User in the moment of acceptance by him of exchange operation terms, which set accepted service from the User and given out by the service to the User volumes of assets in the systems of e-payments. The expenses constrained with the use of the electronic system of payments and envisaged by a corresponding bilateral agreement between the User and system are paid by the User.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>10. Force-majeur</b></span>
				<span>10.1. In case of origin of some circumstances impedimental to complete or partial implementation of any of parties of its obligations under this Contract, namely, natural calamities, war, military operations of any character, blockade, loss by a person carrying out the transactions of a considerable part of the capital, the decline of course of exchange of one currency concerning other is considerably different from a middle index, closing of exchange or other circumstances, being beyond control outside, each of parties has a right to annul further implementation of obligations following from this Agreement, and in this case, none of the parties has no authority to claim compensation from other parties.</span>
				<span>10.2. Confirmation about the offensive of these circumstances as notification passed through electronic connection will be the sufficient founding of offensive of the indicated circumstances and their duration.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>11. Validity of Agreement</b></span>
				<span>11.1. The present Agreement becomes effective after registration of personal account by the User on the B4uglobal platform website, and it remains valid during the whole period of cooperation between the User and the Service. </span>
				<span>11.2. The Service reserves the right to introduce changes and additions into the text of the present Agreement at its discretion without any notice.</span>
				<span>11.3. In case of disagreement with changes in the User agreement, a person has a right to give up the execution of this User agreement and stop to use services given by the Company. Sums that were sent by the User before, providing voluntary payments are not returned. Any Participant understands and is conscious that has permanent open access to this Agreement.</span>
				<span>11.4. Users are strongly recommended to check the content of the Agreement from time to time with respect to changes and additions.</span>
			</p>
			<p style="text-align:justify;">
				<span><b>12. Service Contact details</b></span>
				<span>12.1. If required, the User can contact the Service representatives via the following channels of communication:</span>
				<span>- <a href="https://b4uglobal.com/#contact"> Contact form</a> on the b4uglobal platform website;</span>
				<span>- Service’s e-mail address in <a href="https://b4uglobal.com/#contact">«Contacts»</a> section on the website;</span>
				<span>- Live Chat service</a>, which is available for prompt support at B4uglobal website.</span>
			</p>

		</div>
		<!-- footer -->
		<div class="jarallax footer">
			<div class="container">
				<div class="footer-logo">
					<h3><a href="/">{{$settings->site_name}}</a></h3>
				</div>
				<div class="agileinfo-social-grids">
					<h4>We are social</h4>
					<div class="border"></div>
					<ul>
						<li><a href="https://www.facebook.com/bitcoin4utrading"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-rss"></i></a></li>
						<li><a href="#"><i class="fa fa-vk"></i></a></li>
					</ul>
				</div>
				<div class="copyright">
					<p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>
				</div>
			</div>
		</div>
		<!-- //copyright -->
	</div>
</div>


</body>
</html>