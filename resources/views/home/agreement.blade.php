@include('home.assets')
<style>
span {
    display:block;
    font-size: 100%;
    width: 100%;
    line-height: 2rem;
    cursor: pointer;
}
</style>
</div>
		<div class="agrementBlock welcome">
			<div class="container">
				<div class="w3-welcome-heading">
					<h2>Privacy policy</h2>
				</div>
				<div class="window roww">
					
					<p>
						<span><b>General</b></span>
						<span>One of the main objectives of the B4uglobal platform (hereinafter – the Service, the Company) is the protection of confidential and personal data of its clients (hereinafter the Users) – visitors of the website www.b4uglobal.com (hereinafter – the Site) and the Users of the asset management service and the affiliate program. Any changes to this Privacy Policy will be published in this section. User Registration automatically means that he has read this policy and unconditionally accepts it.</span>
						<span>All information provided on the website of B4uglobal is stored on secure servers in compliance with international privacy standards and fully encrypted. One of the main priorities of the B4uglobal is to ensure the security of the personal data of the User.</span>
					</p>
					<p>
						<span><b>1. Obtaining, storage and processing of personal data</b></span>
						<span>1.1. This Privacy Policy regulates all aspects of the interaction between the Service and the Users in issues of personal data transfer and processing. All the data that the User transmits through the Site shall be subject to processing and storage.</span>
						<span>1.2. This Privacy Policy comes into force from the date of registration of the User and his/her agreement with the Cooperation Agreement, through the registration process.</span>
						<span>1.3. The Service reserves the exclusive right to change this Privacy Policy at any time without notice to Users. The changes take effect from the date of publication on the Site. If the User continues to use the services, it is considered that he has read the current content of the Privacy Policy and accepts the changes.</span>
						<span>1.4. When registering an account on the Site, the User is obliged to prove that he is familiar with the terms of cooperation. With account registration, the User confirms the decision of providing personal data and consent to the collection, processing, storage and use as part of this Privacy Policy.</span>
						<span>1.5. The User is solely responsible for the correctness of the provided personal data.</span>
						<span>1.6. The Service uses the personal data of the Users for:</span>
						<span>- provision of high-quality asset management services and /or the participation in the Service affiliate program;</span>
						<span>- unhindered access of the User to the Site;</span>
						<span>- timely inform the User about the services;</span>
						<span>- registration of a personal account and access to all services;</span>
						<span>- notifications about the balance changes of the personal account, the connection of services;</span>
						<span>- recovery access if the password is lost;</span>
						<span>- sociological research for revealing needs of Users, search of technical problems and implementation of marketing activities;</span>
						<span>- feedback to the Service;</span>
						<span>- work improvement, evaluation of the level of service.</span>
						<span>1.7. Personal User data can be used for marketing activities: e-mailing the news from the Service, offers of new services.</span>
						<span>1.8. Personal data may be used to inform the Users about the activity of the personal account: the flow of funds, report about the implementation of the entry and withdrawal requests, as well as the affiliate activity.</span>
						<span>1.9. Service shall ensure the confidentiality of personal data of the Users when collecting, processing and storage.</span>		
						<span>1.10 The User can´t refuse the transfer and use of personal information.</span>
					</p>
					<p>
						<span><b>2. Agreement to collection, storage and use of personal information</b></span>
						<span>2.1. When registering an account on the Site, Service asks the User to provide personal information that will be used for account and service access. The e-mail of the User, as well as contact and billing information may be required for initial access to a personal account and service.</span>
						<span>2.2. During registration, the User will be given a personal password, which will become part of his/her personal account.</span>
						<span>2.3. By providing personal information to the Service, the User voluntarily agrees to its use and handling.</span>
						<span>2.4. The Service can notify Users about new services and latest updates. The User may refuse these mailings by the Service Notification.</span>
					</p>
					<p>
						<span><b>3. Age of Users</b></span>
						<span>3.1. Account registration and the Service usage is allowed for Users of a sane mind with the status of full civil capacity according to the legal norms; older than 18 (eighteen) years of age; and for citizens of All over the World.</span>

					</p>
					<p>
						<span><b>4. Changes to Personal Information</b></span>
						<span>4.1. The User has access to edit the personal information in his/her account through the website interface.</span>

					</p>
					<p>
						<span><b>5. Security of the User's account</b></span>
						<span>5.1. As long as the User is not registered or not logged into the system under his/her username and password, all movements on the Site remain completely anonymous, that does not preclude the obligations of the User to comply with the terms of this Agreement.</span>
						<span>5.2. Access to the User account can only be obtained if there is a User ID ("Login"), a unique and secret password ("Password"), and additional data (login and password as "Personal Data"). The User must choose his/her own unique "Username" and "Password."</span>
						<span>5.3. It is forbidden to publish information provided by Users in the public domain, or disclose it to third parties without the written consent of the Company.</span>
						<span>5.4. The User is responsible for any unauthorized use of the "Personal data", as this will be considered as an act committed by him. To avoid problems the User must keep his/her personal data secret, and make every effort to respect its privacy.</span>
					</p>
					<p>
						<span><b>6. Personal data</b></span>
						<span>6.1. Personal data of the User, the provision of which is required from the User during an interaction, is securely stored in the electronic system of the Company and shall be treated as confidential and protected. Following the policy of the Company, customer information is disclosed to third parties not affiliated with and only with the consent of the User, or in accordance with the normative legal regulation. Personal data may include, but is not limited to name, date of birth, e-mail, address, bank details, information about financial status, trading activity, past transactions, account balance, as well as the forms of data collection of the client information ("know your client").</span>
						<span>6.2. Use of Personal Information of the Service Participants.</span>
						<span>6.3. The User agrees that the personal data given during his/her registration (phone, e-mail, country, etc.) will be processed by the system of B4uglobal Service.</span>
						<span>6.4. B4uglobal Service has the right to inform users on mentioned contacts (e-mail mailing, sms-mailing, etc.).</span>
						<span>6.5.B4uglobal Service has the right to put comments on the B4uglobal Site indicating contact details - username, country, name, last name, after a User´s comment is received by Service B4uglobal system.</span>
						<span>6.6. The Company has no right to disclose any information except for cases where it may be necessary for the carrying out of operations or other activities for managing the account (s) of the client and is only subject to common obligations in respect of confidentiality and when required in accordance with laws. Access to such information is provided only by way of an official need to parties, such as employees of the Company and / or third parties, who need this information to provide services to clients and / or companies. In all cases, to protect such information, physical, electronic, and procedural control means are used. These control means, in a reasonable degree, are intended to: ensure the security and confidentiality of customer documentation and information; protection against any anticipated threats or hazards to the security or integrity of the document or customer information; protection against unauthorized access or use of customer documents or information, which can lead to substantial harm or inconvenience to the customer.</span>
					</p>
					<p>
						<span><b>7. Cookies</b></span>
						<span>7.1. The Service uses the cookies on the Site to track information about Users and services used. Cookies are data that a Web browser sends when the User visits the Site and it is stored on a User´s personal computer. The Service uses cookies to track User visits of any Site section by counting transitions on internal links and traffic monitoring.</span>

					</p>
					<p>
						<span><b>8. Personal security</b></span>
						<span>8.1. The Service is making every effort to prevent unauthorized access to the User´s personal account and disclosure of his/her personal information. However, the data transmission via the Internet and Web services do not guarantee the absolute protection and require special attention from the User.</span>
						<span>8.2. The User is obliged to keep his/her password in secret and not pass it on to third parties.</span>
						<span>8.3. If the User lost a password (access) to a personal account, he can request its restoration by the automated service on the Site using a personal email address. A password will be contained in a letter that will be sent from the Service to this address.</span>
						<span>8.4. After receiving the temporary password, it is recommended the User creates a new password.</span>
						<span>8.5. The User controls the flow of personal information when using the Site.</span>
						<span>8.6. B4uglobal Service reserves the right, to in its discretion terminate the access of the User who violates this User Agreement, to the Service as a whole and, in particular, including terminating or temporarily suspending a User's access to the Personal Area. The User whose access to the service has been terminated, or whose information ceases to be valid, has no right to create a new account again (including using the email address used earlier in the B4uglobal Service) without authorization from the B4uglobal Service, as well as no right to use the Personal data of another user for access to the B4uglobal Service.</span>
						<span>8.7. During the use the B4uglobal Services (including the Registration, interacting with other Users through the interface of the B4uglobal Service, advertising, browsing of the web pages of B4uglobal Service and so on), the User self voluntarily decides to provide to B4uglobal Service his/her personal data and other information about the User (last name, name, middle name, email address, mobile phone number, as well as any other information provided by the User (including those contained in the messages sent to other Users via the contact form at the interface of the Service B4uglobal), information about the actions of the User to B4uglobal Service, etc.) for the purposes of execution of the User Agreement, and hereby declares his/her consent to the processing of the B4uglobal Service and its affiliates of personal and other data entities, his/her transfer (including cross-border transfer of the territory of foreign states, providing adequate protection for the rights of subjects of personal data) for the treatment of other Users and / or third parties acting on behalf of the B4uglobal Service, including for the purposes of: providing consulting support to the Users, obtaining statistical and analytical data to improve the functioning of B4uglobal Service and/or Services, expanding the range of services, receiving information and / or promotional messages from B4uglobal Service or third parties, prevention and suppression of illegal and/or unauthorized actions of the Users or third parties, ensuring compliance with current legislation. The B4uglobal Service takes all necessary measures to protect the personal data of the User from unauthorized access by third parties.</span>

					</p>
					<p>
						<span><b>9. Contact information</b></span>
						<span>9.1. The User can contact with the representatives of the Service concerning this Privacy Policy by writing a request e-mail to <a href="mailto:{{ getSupportMailFromAddress() }}">{{ getSupportMailFromAddress() }}</a> or create a ticket in the customer service (customer support service is available daily, Monday through Friday, from 10:00 am to 05:00 pm (GMT+4:00).</span>

					</p>

				</div>
				<!-- footer -->
				<div class="jarallax footer">
					<div class="container">
						<div class="footer-logo">
							<h3><a href="/">{{$settings->site_name}}</a></h3>
						</div>
						<div class="agileinfo-social-grids">
							<h4>We are social</h4>
							<div class="border"></div>
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
								<li><a href="#"><i class="fa fa-vk"></i></a></li>
							</ul>
						</div>
						<div class="copyright">
							<p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>
						</div>
					</div>
				</div>
				<!-- //copyright -->

			</div>
		</div>

</body>
</html>