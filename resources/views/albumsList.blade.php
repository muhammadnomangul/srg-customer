@include('header')
<!-- //header-ends -->
<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Albums </h3>
        @include('messages')
        <div class="sign-up-row widget-shadow">

            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data"
                  action="{{ url('dashboard/savealbum') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Album Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-4 control-label">Album Details</label>

                    <div class="col-md-6">
                        <input id="description" type="text" class="form-control" name="description"
                               value="{{ old('description') }}">

                        @if ($errors->has('description'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('cover_image') ? ' has-error' : '' }}">
                    <label for="Picture" class="col-md-4 control-label">Album Picture</label>

                    <div class="col-md-6">
                    <!--        <img  style="width:100px; height:100px; float: right; " class="img-responsive" alt="" src="{{asset('/images/'.Auth::user()->photo)}}" />
							-->
                        <input type="file" value="fileupload">
                        @if ($errors->has('cover_image'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('cover_image') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Save
                        </button>
                    </div>
                </div>
            </form>

            <div width="80%" class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
                <div width="100%" style="text-align:center;">
                    <div><h3 class="title" style="text-align:center;"><font color="red" align="center">Albums
                                List </font></h3>
                        <div>

                        </div>

                        <table id="myTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Sr#</th>
                                <th>Name</th>
                                <!--<th>Descriptions</th>-->
                                <th>Creation Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($albumsAll as $album)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$album->name}}</td>
                                <!--<td>{{$album->description}}</td>-->
                                    <td>{{$album->created_at}}</td>
                                    <td><a href="#" data-toggle="modal" data-target="#updatealbumdetails"
                                           onclick="viewModelFunc10({{$album->id}});" class="btn btn-default">Edit</a><a
                                                class="btn btn-primary"
                                                href="{{url('dashboard/gallery')}}/{{$album->id}}">Add Images</a><a
                                                class="btn btn-danger"
                                                href="{{url('dashboard/deleteAlbum')}}/{{$album->id}}"> X </a></td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Update Album Details -->

<div id="updatealbumdetails" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title" style="text-align:center;">Update Album Details</h4>

            </div>

            <div class="modal-body">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
                      action="{{ url('dashboard/updateAlbmdetails') }}">
                    {{ csrf_field() }}

                    <div id="bodycontent1">
                        <!-- display Ajax Models For Admin Users here -->
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>

<!-- /Update Album Details -->

<script>

    function viewModelFunc10(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/albmdetails',
            data: {id: id, "_token": "{{ csrf_token() }}",},
            success: function (response) {
                $("#bodycontent1").html(response);
            }, error: function (response) {
                // alert('Be patient, System is under maintenance');
                //$("#CallSubModal").modal('show');
            }
        });
    }
</script>


@include('admin.modals_admin')
@include('footer')