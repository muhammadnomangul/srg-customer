@include('header')
<!-- //header-ends -->
<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Manage All Sold Trades</h3>
        @include('messages')

        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
            <table id="myTable" class="table table-hover">
                <thead>
                <tr>
                    <th>Sr.#</th>
                    <th>Portfolio#</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Amount Deduct</th>
                    <th>Trade Amount</th>
                    <th>Profit</th>
                    <th>Payment mode</th>
                    <th>Status</th>
                    <th>Date created</th>
                </tr>
                </thead>
                <?php $count = 1;?>
                <tbody>
                @foreach($solds as $data)
                    <tr>
                        <th scope="row">{{$count}}</th>
                        <td>S-{{$data->id}}</td>
                        <td>{{$data->currency}}</td>
                        <td>{{number_format($data->amount, 4)}}</td>
                        <td>{{number_format($data->sale_deduct_amount, 4)}}</td>
                        <td>{{number_format($data->pre_amount, 4)}}</td>
                        {{--                                <td>{{number_format($data->waiting_profit, 4)}}</td> --}}
                        <td>{{number_format($data->sold_profit, 4)}}</td>
                        <td>{{$data->payment_mode}}</td>
                        <td>{{$data->status}}</td>
                        <td>{{$data->created_at}}</td>
                    </tr>
                    <?php $count++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "pagingType": "full_numbers"
        });
    });

    $(".submit1").click(function () {
        return confirm("Do you really want to proceed ?");
    });

    function viewDetailsFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewDetailsPost',
            data: {id: id, type: "sold", "_token": "{{ csrf_token() }}",},
            success: function (response) {
                $("#scontent").html(response);
            }, error: function (response) {
                // alert("Something went wrong. Please try again later.");
                //$("#CallSubModal").modal('show');
            }
        });
    }
</script>
@include('modals')
@include('footer')