@include('header')

<?php

if (isset($accountsInfo->reference_bonus)) 
{
    $reference_bonus = $accountsInfo->reference_bonus;
} 
else 
{
    $reference_bonus = 0;
}

?>
<!-- //header-ends -->
<style> 
    .pading10 {
        padding-left: 5px;
    }
</style>
<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Your Donations List</h3>
        @include('messages')


        <a class="btn btn-default" href="#" data-toggle="modal" data-target="#donationModal"><i class="fa fa-plus"></i>Donate</a>

        <div style="text-align: right;">
            @if(isset($userdonation))
                <p>Your Donation : <label>${{number_format($userdonation, 2)}}</label></p>
            @endif
        </div>
        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
           

            <table id="myTablelg" class="table table-hover">
                
            </table>
            
        </div>
    </div>
</div>
<!-- Withdrawal Modal -->
<div id="donationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Payment will be sent to B4U Global account.</h4>
            </div> 
            <div class="modal-body">

                <strong style='color:red;' class="errors"></strong>

                <form style="padding:3px;" data-toggle="validator" role="form" method="post"
                      action="{{ route('donations.add') }}" autocomplete="off">
                    <div class="form-group">
                        <label for="select" class="control-label">Select Payment Mode * : </label>
                        <div class="form-input">
                            <select name="payment_mode" id="payment_mode" class="form-control amount_type payment_mode"
                                    required>
                                <option value="">--Select--</option>
                                @foreach(\App\Donation::$payment_modes as $index => $payment_mode)
                                    <option value="{{ $payment_mode }}">{{ $payment_mode }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="currency">
                        <label for="select" id="label_id" class="control-label"></label><br/>
                        <label for="select" class="control-label">Select Donation Currency*:</label>
                        <div class="form-input">
                            <select name="currency" id="currency" class="form-control amount_type currencyVal" required>
                                @foreach($currencies as $currency)
                                    <option value="{{$currency->code}}">{{$currency->code}} ({{$currency->name}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="donation_type">
                        <label for="select" id="Fundlabel" class="control-label">Donation Type : </label>
                        <div class="form-input">
                            <select name="donation_type" id="donation_type" class="form-control fundtransfer_type">
                                @foreach(\App\Donation::$donation_types as $index => $donation_type)
                                    <option value="{{ $index }}">{{ $donation_type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group" id="amount_with">
                        <label for="select" id="amt_convert" class="control-label"></label><br/>
                        <label for="select" id="amountid" class="control-label label_id">Amount* :</label>
                        <div class="form-input">
                            <input type="text" name="amount" id="with_amt" class="form-control"
                                   placeholder="Enter amount here" required  
                                   onkeypress="return isNumberKey(this, event);"  maxlength="7">
                        </div>
                    </div>


                    <div class="form-group">
                        <input type="hidden" name="_key" value="{{ csrf_token() }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" id="submit1" class="btn btn-default" value="Submit">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () 
    {

        //$('#donationModal').modal('show');
        $('#myTable').DataTable({
            "pagingType": "full_numbers"
        });

        var donation_type = $("#donation_type").val();
        if (donation_type == "fundtransfer") {
            $("#fundreciever").show();
            $("#fund_receiver_id").attr("required", "true");

        } else {

            $("#fundreciever").hide();
            $('#fund_receiver_id').removeAttr('required');
            $("#fundreciever2").hide();
            $('#fundreciever_id2').removeAttr('required');
        }
        var fund_id = $("#fund_receiver_id").val();
        if (fund_id == "notexist") {
            $("#fundreciever2").show();
            $("#fund_receiver_id2").attr("required", "true");
        } else {
            $("#fundreciever2").hide();
            $('#fundreciever_id2').removeAttr('required');
        }

        //On change of Modes Withdrawa
        $(document).on('change', "#payment_mode", function () {
            if ($(this).val() == "Bonus") {
                //$("#donation_type").hide();
                $('.currencyVal option[value!="USD"]').hide();
                var currencyValue = $('#currency option:selected').text();
                if (currencyValue != "USD") {
                    $('.currencyVal option:selected').val("USD");
                    //alert(currencyValue);
                    $("#currency option:selected").prop("selected", false);
                }

            } else if ($(this).val() == "Profit") {
                //alert($(this).val());
                //$("#donation_type").hide();
                $('.currencyVal option[value!=""]').show();
            } else if ($(this).val() == "Sold") {
                //alert($(this).val());
                //$("#donation_type").hide();
                $('.currencyVal option[value!=""]').show();
            }

        }).trigger("change");
        $(document).on('change', '#donation_type', function () {
            if ($(this).val() == "fundtransfer") {
                $("#fundreciever").show();
                $("#fund_receiver_id").attr("required", "true");

            } else if ($(this).val() == "") {
                //alert($(this).val());
                $("#fundreciever").hide();
                $('#fund_receiver_id').removeAttr('required');
                $("#fundreciever2").hide();
                $('#fundreciever_id2').removeAttr('required');
            }
            /* else{
                $("#fundreciever").hide();
                $('#fundreciever_id').removeAttr('required');
            } */

        }).trigger("change");
        //On change of amount_type
        $(document).on('change', '.amount_type', function () 
        { 
            $('#submit1').attr('disabled', false);
            var selectedVal = $('#payment_mode option:selected').val();
            $("strong").hide();
            var curr = $('#currency option:selected').val();
            var amtval=$('#with_amt').val();
            if (selectedVal != "" && curr != "") 
            {
                //alert(selectedVal + curr);
                $("strong").hide();
                jQuery.ajax({
                    type: "POST",
                    url: '{{{URL::to("")}}}/dashboard/getAccountInfoPostDonation',
                    data: {
                        curr: curr,
                        type: selectedVal,
                        "_token": "{{ csrf_token() }}",
                    },
                    //	cache: false,
                    success: function (response) {
                        console.log(response);
                        $("#label_id").html("");
                        $("#label_id").append(response);

                        if ($('#error').val() == 1) 
                        {
                            $('#submit1').attr('disabled', true);
                        } 
                        else 
                        {
                            $('#submit1').attr('disabled', false);
                        }


                       
                        //	$( "#ajaxval strong" ).html(response);
                    },
                    error: function (response) {
                        //$("#CallSubModal").modal('show');
                        console.log(response);
                        // alert("error!!!!" + response);

                    }
                });
                amountconverstion(curr,amtval);

            }
        });
        $(document).on('change', '#fund_receiver_id', function () {
            if ($(this).val() == "notexist") 
            {

                $("#fundreciever2").show();
                $("#fundreciever_id2").attr("required", "true");

            } else { //alert($(this).val());
                $("#fundreciever2").hide();
                $('#fundreciever_id2').removeAttr('required');
            }

        }).trigger("change");
        document.getElementById("fundreciever_id2").addEventListener("focusout", searchBaneficiary);

    });

    function viewProofFunc1(id) {
        //alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewProofPost1',
            data: {
                id: id,
                type: "deposit",
                "_token": "{{ csrf_token() }}",
            },
            //	cache: false,
            success: function (response) {
                console.log(response);
                $("#pcontent1").html("");
                $("#pcontent1").html(response);
                //	$( "#ajaxval p" ).append(response);
            },
            error: function (response) {
                console.log(response);
                // alert("error!!!!");
                //$("#CallSubModal").modal('show');
            }
        });
    }


    function viewDetailsFunc(id) {
        //	alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewWithdrawDetailsPost',
            data: {
                id: id,
                type: "withdraw",
                "_token": "{{ csrf_token() }}",
            },
            //	cache: false,
            success: function (response) {
                console.log(response);
                $("#wcontent").html("");
                $("#wcontent").html(response);
                //	$( "#ajaxval p" ).append(response);
            },
            error: function (response) {
                console.log(response);
                //$("#CallSubModal").modal('show');
                // alert("error!!!!");
            }
        });
    }

    function viewModelFuncUWD(id) {
    }

    function searchBaneficiary() {
        var fundreciever_id2 = $("#fundreciever_id2").val();

        if (fundreciever_id2 != '') {
            //alert(fundreciever_id2);
            $.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/beneficiaryDetails',
                dataType: 'html',
                data: {
                    benficiaryid: fundreciever_id2,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    $("#ben_id").append(response);
                },
                error: function (response) {
                    //$("#CallSubModal").modal('show');
                    // alert(response)
                }
            });
        }
    }
    ///////////////
     function isNumberKey(txt, evt) {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode == 46) 
      {
        //Check if the text already contains the . character
        if (txt.value.indexOf('.') === -1) {
          return true;
        } else {
          return false;
        }
      } else {
        if (charCode > 31 &&
          (charCode < 48 || charCode > 57))
          return false;
      }
      return true;
    }
    ////////////////
    $('a').off('click').click(function() {
    });


    $(document).ready(function () 
    {
        $("#submit1").click(function () 
        {
            var withdraw_mode=$('#payment_mode').val();
            var amountid=$('#with_amt').val();
            var donation_amt=10;

            var number = $('#label_id').text();
            var sd = number.replace(/[^0-9]/gi, ''); 
            var currconamt=$('.currconamt').val();

            var currencytype=$('.currencyVal').val();

            if(withdraw_mode=='')
            {
                $(".errors").show();
                $(".errors").html("Payment mode must be selected");
                return false; 
            }
            else if(amountid=='')
            {
                $(".errors").show();
                $(".errors").html("Amount must be required");
                return false; 
            }
            else if(currencytype!='USD' && parseFloat(currconamt)<donation_amt.toFixed(2))
            {
            	//$(".errors").show();
                //$(".errors").html(amountid+" Amount Must be less than or equal to "+sd +"Amount");
                //"Min Amount can not be less than "+donation_amt
                $(".errors").html("Min Amount can not be less than "+donation_amt);
                return false; 
            }
            else if(parseFloat(amountid)<donation_amt && currencytype=='USD')
            {
                $(".errors").show();
                $(".errors").html("Min Amount can not be less than "+donation_amt);
                return false; 
            }

            
            
        });

        ///////////data table////////////////


        var i = 1;
        $('#myTablelg').DataTable({
            processing: true,
            serverSide: true,
            "bLengthChange": false,
            "ajax": {
                "url": "{{url('dashboard/donations/json')}}",
                "type": "POST"
            },
            // drawCallback: function () {
            // var sum = $('#myTablelg').DataTable().column(2).data().sum();
    
            // $('#total').html(sum);
            // },
           


            "deferRender": true,
            "columns": [
            /* title will auto-generate th columns */
            {
                "data": "id", "title": "Sr.#", "searchable": true,
                "render": function(data, type, full, meta) 
                {
                    return i++;
                }
            },
            {
                    "data": "id", "title": "D-ID", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {
                        var itemID = data;
                        return 'D-' + itemID + '';
                    }
            },

            {
                "data": "amount", "title": "Amount", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {
                        return row.amount;
                    }
            },

            {
                "data": "usd_amount", "title": "USD Amount", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {
                        return row.usd_amount;
                    }
            },

            {
                "data": "donation_type", "title": "Donation Type", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {   
                         var donationtype='';
                        @foreach(\App\Donation::$donation_types as $index => $donation_type)
                            var index="{{$index}}";
                            if(index==row.donation_type)
                            {
                                var donationtype="{{ $donation_type }}";
                            }
                        @endforeach
                        
                        return donationtype;
                    }
            },

            {
                "data": "currency", "title": "Currency", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {   
                        return row.currency;
                    }
            },

            {
                "data": "payment_mode", "title": "Payment Mode", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {
                        return row.payment_mode;
                    }
            },

            {
                "data": "status", "title": "Status", "searchable": true,
                    "render": function (data, type, row, meta) 
                    {
                        return row.status;
                    }
            },
            {
                "data": "created_at", "title": "Created", "orderable": true, "searchable": true,
                'render': function (data, type, row) 
                {
                    var created_date = new Date(row.created_at);
                    var curr_hour = created_date.getHours();
                    var curr_min = created_date.getMinutes();
                    var curr_sec = created_date.getSeconds();

                    var datestring = created_date.getFullYear() + "-" + ("0"+(created_date.getMonth()+1)).slice(-2) +"-"+("0" + created_date.getDate()).slice(-2);


                    return datestring+" "+curr_hour+":"+curr_min+":"+curr_sec;

                    //return created_date.toISOString().slice(0, 10);
                }
            },
        ],
            "order": [8, "desc"]
        });

        ////////////////////////////////////
    });
    /////////////////////
    //// 
    $('body').on('hidden.bs.modal', '.modal', function () { 
        // $(this).find('input[type="text"],input[type="email"],textarea,select').each(function() { 
        // if (this.defaultValue != '' || this.value != this.defaultValue) {
        // this.value = this.defaultValue; 
        // } else { this.value = ''; }
        // }); 
        // $('#label_id').html("");
        // $('#submit1').attr('disabled',false);  

        $('#payment_mode').prop('selectedIndex',0);
        $("#currency").prop('selectedIndex',0);
        $(".fundtransfer_type").prop('selectedIndex',0);
        $("#with_amt").val("");

        $('#label_id').html("");
        $('#submit1').attr('disabled',false);   

    });

$(document).on('input','#with_amt',function () 
{ 
    var currencytype = $('#currency option:selected').val();
    var x = document.getElementById("with_amt").value;
    if(x!='')
    {
        amountconverstion(currencytype,x);
    }
    else
    {
        $('#amt_convert').html("");
    }
});

var input = document.getElementById('with_amt');
 input.onkeydown = function() {
        var key = event.keyCode || event.charCode;
        if( key == 8 ){
         $('#amt_convert').html("");
        }
    };

function amountconverstion(currencytype,amountval)
{
    if(amountval!='')
    {
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/currenconvert',
            data: {currencytype:currencytype,amount: amountval,"_token": "{{ csrf_token() }}",},
            //cache: false,
            success: function (response) 
            {
               $('#amt_convert').html(response);
                //  $( "#ajaxval p" ).append(response);
            }, error: function (response) 
            {
                console.log(response);
                //alert("error!!!!");
                //$("#CallSubModal").modal('show');
            }
        });
    }
    else
    {
        $('#amt_convert').html("");
    }
}




</script>
@include('modals')
@include('footer')