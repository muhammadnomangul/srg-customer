@include('header')

 
	<div id="page-wrapper">
           
		<div class="main-page">
            @include('messages')               
			<div class="row widget-shadow">
                <h3 class="title1" align="center">Update Currencies </h3>
               
                   <form class="form-horizontal" id="myform" role="form" method="POST" action="{{ url('dashboard/currencyupdatePost') }}">
                        {{ csrf_field() }}
						
                        <div class="form-group">
							<div class="col-md-2">
                               
                            </div>
							<div class="col-md-2">
								<label for="currencyName" class="control-label" style="text-align:right;">Currency Name:</label>
							</div>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$curr->name}}" reqiured>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
							<div class="col-md-2">
                               
                            </div>
						</div>
						<div class="form-group">
							<div class="col-md-2">
                               
                            </div>
							<div class="col-md-2">
								<label for="currency code" class="control-label" style="text-align:right;">Currency Code *:</label>
							</div>
                            <div class="col-md-6 form-input">
                                <input id="code" type="text" class="form-control" name="code" value="{{$curr->code}}" reqiured>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
							<div class="col-md-2">
                               
                            </div>
                        </div>
						<div class="form-group">
							<div class="col-md-2">
                               
                            </div>
							<div class="col-md-2">
								<label for="select" class="control-label" style="align:right;">Select Status * : </label>
							</div>
							<div class="col-md-6  form-select">
								<select name="status" id="status" class="form-control" required>
									<option value="">--Select--</option>
									<option @if($curr->status == "Active") selected="selected" @endif value="Active">Active</option>
									<option @if($curr->status == "Disabled") selected="selected" @endif value="Disabled">Disabled</option>
								</select>
							</div>
							<div class="col-md-2">
                               
                            </div>
						</div>	
                        <div class="form-group">
							<div class="col-md-2">
								   
							</div>
							<div class="col-md-2">
                               <input type="hidden" name="cid" value="{{$curr->id}}">
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Update
                                </button>
                            </div>
							<div class="col-md-2">
                               
                            </div>
                            
                        </div>
                </form>
                   
			</div>
		</div>
	</div>

<script>

	
</script>
@include('footer')