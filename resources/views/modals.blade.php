<!--<link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet"> -->
<style>
    .custom-combobox {
        position: relative;
        display: inline-block;
    }

    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }

    .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
    }

    .loader-overlay {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.60);
        z-index: 999999;
    }

    .loader-overlay .inner-loader {
        display: table;
        height: 100%;
        width: 100%;
    }

    .inner-loader > div {
        background-repeat: no-repeat;
        background-position: center;
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }


</style>

<div id="DDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="dcontent">
        </div>
    </div>

</div>

<!-- Deposit Proof Model -->
<div id="popImageModel" class="modal fade" role="dialog" style="width:100%">
    <div class="modal-dialog" style="width:95%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;"><strong>Proof Image:</strong></h4>
            </div>
            <div class="modal-body" id="pcontent">
                <!--//  image display here -->
            </div>
        </div>
    </div>
</div>
<!-- /Deposit Proof Model -->

<!-- /Deposits Details Modal -->
<div id="WDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="wcontent">
        </div>
    </div>
</div>
<!-- /Deposits Details Modal -->
<!-- Sale Modal -->
<div id="saleModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Sale Deposit</h4> <br>
                <div><b>Sales comission Criteria</b><br>
                    <p>(i) Up to 60 days(2 months) - 35%</p><br>
                    <p>(ii) From 2 months to 4 months - 20% </p><br>
                    <p>(iii)From 4 months to 6 months - 10% </p><br>
                    <p>(iv) After 6 months - 0% </p><br>
                </div>
            </div>
            <div id="ajaxval">
                <!-- display msg here -->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="select" class="control-label">If you want to Sale this Deposit, than click on Continue
                        Sale Button : </label>
                </div>
                <?php    $uid = "A";  $date = "B";?>
                <p style="color:red;"><img src="{{asset('images/sale-alert.gif')}}"> Be alert Sold Deposit Never Revert.
                    <a id="submit1" class="btn btn-default" href="#" onclick="confirmSaleFunc(this);">Continue</a></p>
            </div>
        </div>
    </div>
</div>
<div id="imageiban" class="modal fade" role="dialog" style="width:100%">
    <div class="modal-dialog" style="width:95%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;"><strong>IBAN Example:</strong></h4>
            </div>
            <div class="modal-body" id="pcontent123">
            </div>
        </div>
    </div>
</div>

<script>
    function confirmSaleFunc(link) {
        //alert(uid + date);
        //, id : uid,created : date
        link.onclick = function (event) {
            event.preventDefault();
        }
        var conf = confirm("Do you really want to proceed ?");
        if (conf) {
            var str = $("p.d-mub").attr('d-mub');
            $('#reload_img').show();
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/saleTradeSave',
                data: {val: str, "_token": "{{ csrf_token() }}",},
                success: function (response) {
                    $('#reload_img').hide();
                    if (response == "Success") {
                        alert("Your Deposit is sold successfully.");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        // When the user clicks on <span> (x), close the modal
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    } else if (response == "Invalid Amount") {
                        alert("Your Sold Amount is not valid, Please contact with Admin!!");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                    } else if (response == "User Balance Less Than Sold Amount") {
                        alert("Your Balance is Less than your sold amount, Please contact with Admin!!");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                    } else if (response == "Deposit Already Sold") {
                        alert("Deposit Already Sold");
                        var span = document.getElementsByClassName("close")[0];
                        window.location.replace('{{ url("dashboard/deposits")}}');
                    } else if (response == "User Balance Amount is less than 0") {
                        alert("Error..!! Sorry Your Balance is Less than 0, Please contact support team!!");
                        // Get the <span> element that closes the modal
                        {{--var span = document.getElementsByClassName("close")[0];--}}
                        {{--window.location.replace('{{ url("dashboard/deposits")}}');--}}
                    } else if (response == "Already Sold") {
                        alert("Your Deposits Already Sold!!");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    } else {
                //        alert("Your Deposits Sold!");
                        console.log(response);
                        alert("Error !!Something went wrong. Please try again");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    }
                }, error: function (response, response2, response3) {
                    $('#reload_img').hide();
                    //$("#CallSubModal").modal('show');
                    // alert('Be patient, System is under maintenance');
                }
            });
        } else {
            $('#saleModal').modal('hide');
            location.reload();
            return false;
        }

    }

    // call on deposits page
    function confirmPartialSaleFunc() {

        var conf = confirm("Do you really want to proceed partial sale ?");
        if (conf) {
            var str = $("#ajaxval p").text();
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/partialSaleTrade',
                data: {val: str, "_token": "{{ csrf_token() }}",},
                success: function (response) {
                    if (response == "Success") {
                        alert("Your Deposit is sold successfully.");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        // When the user clicks on <span> (x), close the modal
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    } else if (response == "Already Partial Sold") {
                        alert("Your Deposits Already Sold!!");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    } else {
                        alert("Your Deposit is sold successfully.");
                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];
                        span.onclick = function () {
                            modal.style.display = "none";
                        }
                        window.location.replace('{{ url("dashboard/sold")}}');
                    }
                }, error: function (response) {
                    // alert('Be patient, System is under maintenance');
                    //$("#CallSubModal").modal('show');
                }
            });
        } else {
            return false;
        }

    }


    $(document).ready(function () {
        $('#rtype').hide();
        $('#reinvest_type').attr('required', 'false');
        $('#fundUser').hide();
        $('#fund_user').attr('required', 'false');
        $('.dropdown').click(function (e) {
            $(this).next().slideToggle();
            $(this).toggleClass('active');
            $(this).next().toggleClass('open-menu');
            $(this).prev().toggleClass('active-menu');
        });

    });


    // Search User Info

    function searchUsers(txt) {
        if (txt.length >= 1) {
            //alert(txt);
            var userName = '';
            $.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/relatedUsers',
                dataType: 'html',
                data: {username: userName, search: txt, "_token": "{{ csrf_token() }}",},
                success: function (data) {
                    concole.log(data);
                    var object = jQuery.parseJSON(data);
                    var html = "";
                    if (object != "") {
                        $('#livesearch').addClass("searhShow");
                        for (var j = 0; j < object.length; j++) {
                            html = "<li class='myLi'>" + object[j].name + " [ " + object[j].u_id + " ]</li>";
                            $('#livesearch ul').append(html);
                        }

                    } else {
                        $('#livesearch').removeClass("searhShow");
                    }

                }
            });
        }
    }

    $(document).on('click', '.myLi', function () {
        //alert("success");
        var divSelect = $(this).html();
        var arrDivValue = divSelect.split('[');
        var arrDivText = arrDivValue[0];
        //alert(arrDivText);
        $('#fund_user').val(arrDivText);
        $('#livesearch').removeClass("searhShow");
        $("#livesearch ul").empty();
    });


</script>
