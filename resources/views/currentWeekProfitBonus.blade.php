@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Last Week Profit</h3>
				@include('messages')
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-default submit1" href="{{ url('dashboard/daily_profit') }}">All Profit List</a>
                        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
                            <table id="myTable" class="table table-hover"> 
                                <thead> 
                                    <tr> 
                                        <th>Sr.#</th> 
                                        <th>Trade ID</th> 										<th>User ID</th>
                                        <th>Amount</th>
                                        <th>Today Profit</th>
                                        <th>Crypto Profit</th> 
                                        <th>Date created</th>
                                    </tr> 
                                </thead>
                                <?php $count = 1;?> 
                                <tbody> 
                                    @foreach($profit_list as $profit)
                                    <tr> 
                                        <th scope="row">{{$count}}</th>
                                        <td>D-{{$profit->trade_id}}</td>										<td>{{$profit->user_id}}</td>
                                        <td>${{number_format($profit->trade_amount, 2)}}</td> 
                                        <td>${{number_format($profit->today_profit, 6)}} </td> 
                                        @if($profit->currency != "USD")
                                        <td>({{$profit->currency}}) {{number_format($profit->crypto_profit, 6)}}</td> 
                                        @else										<td>0</td> 									   @endif
                                        <td>{{$profit->created_at}}</td>
                                    </tr> 
                                    <?php $count++; ?>
                                    @endforeach
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                <h3 class="title1">Last Week Bonuses</h3>
                    <div class="col-md-12">
                         <a class="btn btn-default submit1" href="{{ url('dashboard/daily_bonus') }}">All Bonus List</a>
                        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
                        <table id="myTable2" class="table table-hover"> 
                            <thead> 
                                <tr> 
                                    <th>Sr.#</th> 
                                    <th>Trade ID</th> 
                                    <th>Trade User ID</th>
                                    <th>Referal Parent ID</th>
                                    <th>Parent Plan #</th> 
                                    <th>Bonus</th>
                                    <th>Details</th>
                                    <th>Date created</th>
                                </tr> 
                            </thead>
                            <?php $count = 1;?> 
                            <tbody> 
                                @foreach($bonus_list as $bonus)
                                <tr> 
                                    <th scope="row">{{$count}}</th>
                                    <td>D-{{$bonus->trade_id}}</td>
                                    <td>{{$bonus->user_id}}</td> 
                                    <td>{{$bonus->parent_id}} </td>
                                    <td>{{$bonus->parent->dplan->name}} </td> 
                                    <td>${{number_format($bonus->bonus, 6)}}</td>
                                    <td>{{$bonus->details}}</td>
                                    <td>{{$bonus->created_at}}</td> 
                                </tr> 
                                <?php $count++; ?>
                                @endforeach
                            </tbody> 
                        </table>
                    </div>
                    </div>
                </div>
			</div>
		</div>
	<script>
	$(document).ready( function (){
		$('#myTable').DataTable( {
			"pagingType": "full_numbers"
    	});
		$('#myTable2').DataTable( {
			"pagingType": "full_numbers"
    	});
	});
	$(".submit1").click(function(){
			//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");
		if(conf){
			return true;
		}
		else{
			return false;
		}
	});
	</script>
		@include('footer')