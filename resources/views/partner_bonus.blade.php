		<div class="row">
			@if(\Auth::user()->plan >= 3)

			    <div class="row-one" style="margin-top:10px;">
					<!--	// Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
						<!--Total Sub Users -->
					<div class="col-md-2 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 1st Level Total </sup><br>
						<b style="color:#490342; margin-bottom:10px;">${{number_format($maxfirstTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 1st </sup><br>
						<b style="color:#490342;">${{number_format($bonusFirst, 2)}}</b>
					</div>
					<div class="col-md-2 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 2nd Level Total</sup>	<br>
						<b style="color:#490342;">${{number_format($maxsecondTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 2nd </sup><br>
						<b style="color:#490342;">${{number_format($bonusSecond, 2)}}</b>
					</div>
					
					<div class="col-md-2 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 3rd Level Total </sup><br>
						<b style="color:#490342;">${{number_format($maxthirdTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 3rd </sup><br>
						<b style="color:#490342;">${{number_format($bonusThird, 2)}}</b>
						<br>
					</div>
					<!--$last_profit-Div -->
					<div class="col-md-2 col-sm-6 rp t-b">
						
						<sup style="color:black;"> Max 4th Level Total </sup><br>
					
						<b style="color:#490342;">${{number_format($maxfourthTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 4th </sup><br>
						<b style="color:#490342;">${{number_format($bonusFourth, 2)}}</b>
						
					</div>
					<div class="col-md-2 col-sm-6 rp t-b">
						
							<sup style="color:black;"> Max 5th Level Total</sup><br>
							<b style="color:#490342;">${{number_format($maxfifthTotal, 2)}}</b>
							<br><br>
							<sup style="color:black;">Bonus 5th </sup><br>
							<b style="color:#490342;">${{number_format($bonusFifth, 2)}}</b>
							
					</div>
					<div class="col-md-2 col-sm-6 rp t-b">
							<sup style="color:black;"> Max Final Total</sup><br>
							<b style="color:#490342;">${{number_format($maxTotal, 2)}}</b>
							<br><br>
							<sup style="color:black;">Bonus Total Earnings </sup><br>
							<b style="color:#490342;">${{number_format($bonusTotal, 2)}}</b>
						
					</div>
					<!--$last_bonus-->
				</div>

			@else
				

			    <div class="row-one" style="margin-top:10px;">
					<!--	// Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
						<!--Total Sub Users -->
					<div class="col-md-3 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 1st Level Total </sup><br>
						<b style="color:#490342; margin-bottom:10px;">${{number_format($maxfirstTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 1st </sup><br>
						<b style="color:#490342;">${{number_format($bonusFirst, 2)}}</b>
					</div>
					<div class="col-md-3 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 2nd Level Total</sup>	<br>
						<b style="color:#490342;">${{number_format($maxsecondTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 2nd </sup><br>
						<b style="color:#490342;">${{number_format($bonusSecond, 2)}}</b>
					</div>
					
					<div class="col-md-3 col-sm-6 rp t-b">
						<sup style="color:black;"> Max 3rd Level Total </sup><br>
						<b style="color:#490342;">${{number_format($maxthirdTotal, 2)}}</b>
						<br><br>
						<sup style="color:black;">Bonus 3rd </sup><br>
						<b style="color:#490342;">${{number_format($bonusThird, 2)}}</b>
						<br>
					</div>
					
					<div class="col-md-3 col-sm-6 rp t-b">
							<sup style="color:black;"> Max Final Total</sup><br>
							<b style="color:#490342;">${{number_format($maxTotal, 2)}}</b>
							<br><br>
							<sup style="color:black;">Bonus Total Earnings </sup><br>
							<b style="color:#490342;">${{number_format($bonusTotal, 2)}}</b>
						
					</div>
					<!--$last_bonus-->
				</div>

			@endif
				<div class="clearfix"> </div>

				<div class="col-lg-4 col-md-4" >

				</div>
				<div class="col-lg-4 col-md-4 col-sm-5" style="margin-bottom:5px; padding:0px;">

				</div>
				<div class="col-lg-4 col-md-4 col-sm-4" style="margin-bottom:5px; padding:0px;">

				</div>
		</div>
		<div class="clearfix" ></div>
		<div class="row-one" style="margin-top:10px;">
			<div class="col-md-3 col-sm-6 rp t-b">
				<h4 style="margin-top:5px; text-align:center;"><label>Total Earnings</label> <br> ${{number_format($bonusTotal, 2)}}</h4>
			</div>
			 
			<div class="col-md-2 col-sm-6 rp t-b">
				<h4 style="margin-top:5px; text-align:center;"><label> Profit Bonus </label> <br> ${{number_format($profitbonus, 2)}}</h4>
			</div>
			<div class="col-md-2 col-sm-6 rp t-b">
				<h4 style="margin-top:5px; text-align:center;"><label> Bonus ReInvest </label> <br> ${{number_format($bonusreinvestment, 2)}}<br></h4>
			</div>
			<div class="col-md-2 col-sm-6 rp t-b">
				<h4 style="margin-top:5px; text-align:center;"><label> Bonus Withdrawal </label><br> ${{number_format($bonuswithdrawal, 2)}}<br></h4>

			</div>
			<div class="col-md-3 col-sm-6 rp t-b">
				<h4 style="margin-top:5px; text-align:center;"><label>Final Bonus</label><br> ${{number_format($finaltotalbonus, 2)}}<br></h4>

			</div>

		</div>
		<div class="clearfix" ></div>
		<!--<table id="myTable" class="table table-hover">
            <thead>
				<tr>
					<th>Max 1st Level Total</th>
					<th>Max 2nd Level Total</th>
					<th>Max 3rd Level Total</th>
					@if(\Auth::user()->plan >= 3)
					<th>Max 4th Level Total</th>
					<th>Max 5th Level Total</th>
					@endif
					<th>Max Final Total</th>
				</tr>
            </thead>
            <tbody>
    				<tr>
    					<th scope="row">${{number_format($maxfirstTotal, 2)}}</th>
    					<td>${{number_format($maxsecondTotal, 2)}}</td>
    					<td>${{number_format($maxthirdTotal, 2)}}</td>
    					@if(\Auth::user()->plan >= 3) 
    					<td>${{number_format($maxfourthTotal, 2)}}</td>
    					<td>${{number_format($maxfifthTotal, 2)}}</td>
    					@endif
    					<td><strong>${{number_format($maxTotal, 2)}}</strong></td>
    				<tr>
                    <tr>
                            <th >Bonus First</th>
                            <th>Bonus Second</th>
                            <th>Bonus Third</th>
                            @if(\Auth::user()->plan >= 3)
                            <th>Bonus Fourth</th>
                            <th>Bonus Fifth</th>
                            @endif
                            <th>Bonus Total Earning</th>
                    </tr>
                    <tr>

                            <th scope="row">${{number_format($bonusFirst, 2)}}</th>
                            <td>${{number_format($bonusSecond, 2)}}</td>
                            <td>${{number_format($bonusThird, 2)}}</td>
                            @if(\Auth::user()->plan >= 3) 
                            <td>${{number_format($bonusFourth, 2)}}</td>
                            <td>${{number_format($bonusFifth, 2)}}</td>
                            @endif
                            <td><strong>${{number_format($bonusTotal, 2)}}</strong></td>
                    </tr>
                    <tr>    
                            <th>Profit Bonus</th>
                            <th>Bonus ReInvestment</th>
                            <th>Bonus Withdrawal</th>
                            @if(\Auth::user()->plan >= 3)
                            <th></th>
                            @endif
                            <th>Bonus Final Earnings</th>
                    </tr>

                    <tr>
                            <th scope="row">${{number_format($profitbonus, 2)}}</th>
                            <td>${{number_format($bonusreinvestment, 2)}}</td>
                            <td>${{number_format($bonuswithdrawal, 2)}}</td>
                            @if(\Auth::user()->plan >= 3) 
                            <td> </td>
                            <td> </td>
                            @endif
                            <td><strong>${{number_format($finaltotalbonus, 2)}}</strong></td>
                   </tr>
    				
				
            </tbody>

        </table>-->
	 
   