@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Manage Clients Sold</h3>
				@include('messages')
				<a class="btn btn-default" href="{{ url('dashboard/sold')}}">View Personal Solds</a>
				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
					<table id="myTable" class="table table-hover"> 
						<thead> 
							<tr> 
								<th>Sr.#</th> 
								<th>Portfolio#</th>
								<th>User</th>
                                <th>Amount</th>
								<th>Amount Deduct</th>
                                <th>Payment mode</th>
								<th>Status</th>
                                <th>Date created</th>
							</tr> 
						</thead> 
						<?php $count=1;?>
						<tbody> 
							@foreach($solds as $data)
							<tr> 
								<th scope="row">{{$count}}</th>
								<td>S-{{$data->id}}</td>
								<td>{{$data->u_id}}</td>
								<td>{{number_format($data->amount, 4)}} ({{$data->currency}})</td> 
								<td>{{number_format($data->sale_deduct_amount, 4)}} ({{$data->currency}})</td> 
								<td>{{$data->payment_mode}}</td> 
                                <td>{{$data->status}}</td>
								<td>{{$data->created_at}}</td> 
							</tr> 
							<?php $count++; ?>
							@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	<script>
	$(document).ready( function () {
		$('#myTable').DataTable( {
        "pagingType": "full_numbers"
    	} );
	} );

	$(".submit1").click(function(){
			//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");
		if(conf){
			return true;
		}
		else{
			return false;
		}
	});
	
	function viewDetailsFunc(id)
	{
	//	alert(id);
		jQuery.ajax({
			type: "POST",
			url: '{{{URL::to("")}}}/dashboard/viewDetailsPost',
			data: {id : id,type:"sold","_token": "{{ csrf_token() }}",},
		//	cache: false,
			success: function(response){
     			$("#scontent").html(response);

			},error:function(response){
				alert('Be patient, System is under maintenance');
			}
		});
	}
	</script>
        @include('modals_admin')
		@include('footer')