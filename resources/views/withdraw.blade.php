@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Admin Withdraw Funds</h3>
				
				@include('messages')

				<div class="sign-up-row widget-shadow">
					<form method="post" action="{{action('PaymentController@withdraw')}}" autocomplete="off">
						<!--<div class="sign-u">
							<div class="sign-up1">
								<h4>Select Type withdrawal* : </h4>
							</div>
							<div class="sign-up2">
								<select name="withdraw_mode" style="height:40px;">
									<option>Bonus</option>
									<option>Profit</option>
									<option>Sale</option>
								</select>
							</div>
							<div class="clearfix"> </div>
						</div>-->
						<div class="sign-u">
							<div class="sign-up1">
								<h4>Amount to withdraw* : <small>Withdrawal Min Amount $25</small></h4>
							</div>
							<div class="sign-up2">
								<input type="text" name="amount" value="N20000" disabled>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="sub_home">
							<input type="submit" value="Submit">
							<div class="clearfix"> </div>
						</div>
						<input type="hidden" name="right" value="{{Auth::user()->right}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
					</form>
				</div>
			</div>
		</div>
		@include('footer')