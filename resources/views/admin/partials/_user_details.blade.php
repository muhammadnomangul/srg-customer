<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="text-align:center;">User Details</h4>
</div>
<div class="modal-body">
    <div class="col-sm-6"><label class="modal-title" style="color:red;">User Info :</label></div>
    <table class="table table-responsive">
        <tbody>
        @if ($verify_by != '')
            <tr>
                <th><label>Verify by </label></th>
                <th>{{ $verify_by }}</th>
            </tr>
        @endif
        <tr>
            <th><label>User ID</label></th>
            <td>{{ $userUID }}</td>
            <th><label><label>Name</label> </label></th>
            <td>{{ $userinfo[0]->name }}</td>
        </tr>
        <tr>
            <th><label>Parent Id </label></th>
            <td>{{ $userinfo[0]->parent_id }}</td>
            <th><label>User Plan </label></th>
            <td>{{ $planname }}</td>
        </tr>
        <tr>
            <th><label>Email </label></th>
            <td>{{ $userinfo[0]->email }}</td>
        </tr>
        </tbody>
    </table>
    <table width="90%" align="center">
        <tr>
            <td colspan="4"></td>
        </tr>
        @if ($type == "partners" || $partner == 1)
            <tr style="margin-top:50px;">
                <td colspan="4"></td>
            </tr>
            <tr class="modal-header" style="color:red;padding-bottom: 25px !important; ">
                <td colspan="4">
                    <h4 class="modal-title">Deposits Balance Info:</h4>
                </td>
            </tr>

            <tr style="padding-bottom: 25px !important;">
                <td>{{ number_format($userinfo[0]->balance_usd, 2, '.', '') }} (USD)</td>
                <td>{{ number_format($userinfo[0]->balance_btc, 6, '.', '') }} (BTC)</td>
                <td>{{ number_format($userinfo[0]->balance_eth, 6, '.', '') }} (ETH)</td>
                <td>{{ number_format($userinfo[0]->balance_bch, 6, '.', '') }} (BCH)</td>
            </tr>
            <tr style="padding-bottom: 25px !important;">
                <td>{{ number_format($userinfo[0]->balance_ltc, 6, '.', '') }} (LTC)</td>
                <td>{{ number_format($userinfo[0]->balance_dash, 6, '.', '') }} (DASH)</td>
                <td>{{ number_format($userinfo[0]->balance_xrp, 6, '.', '') }} (XRP)</td>
                <td>{{ number_format($userinfo[0]->balance_zec, 6, '.', '') }} (ZEC)</td>
            </tr>
    </table>
    @else
        <div class="col-sm-12">
            <h4>Accounts Balance Info : </h4>
        </div>
        <div class="col-sm-12">
            <label class="modal-title" style="color:red;">Bonus: </label>
        </div>
        <div class="">
            <div class="col-sm-3">
                <label>Bonus : </label>
            </div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->reference_bonus, 2, '.', '') }}
            </div>
            <div class="col-sm-3">
                <label>Latest Bonus: </label>
            </div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->latest_bonus, 2, '.', '') }}
            </div>
        </div>

        {{--/*    if(Auth::user()->type == 1 || Auth::user()->type == 2)
        {
        <tr style="margin-top:50px;"><td><strong>Old Bonus : </strong></td><td>'.number_format($userinfo[0]->old_ref_bonus, 2, '.', '').'</td></tr>
        }
        */--}}

        <div class="col-sm-12">
            <label class="modal-title" style="color:red;">Profit Info: </label>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_usd, 6, '.', '') }} (USD)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_btc, 6, '.', '') }} (BTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_eth, 6, '.', '') }} (ETH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_bch, 6, '.', '') }} (BCH)</div>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_ltc, 6, '.', '') }} (LTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_dash, 6, '.', '') }} (DASH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_xrp, 6, '.', '') }} (XRP)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->profit_zec, 6, '.', '') }} (ZEC)</div>
        </div>
        <div class="">
            <div class="col-md-6">{{ number_format($userinfo[0]->profit_rsc, 6, '.', '') }} (RSC)</div>
            <div class="col-md-6">{{ number_format($userinfo[0]->profit_usdt, 6, '.', '') }} (USDT)</div>
        </div>
        {{--/*    if(Auth::user()->type == 1 || Auth::user()->type == 2)
        {


        <tr class="modal-header" style="color:red"><td colspan="4"><h4 class="modal-title">Old Profit Info: </h4></td></tr>

        <tr style="margin-top:50px;"><td>'.number_format($userinfo[0]->old_profit_usd, 2, '.', '').'(USD)</td><td>'.number_format($userinfo[0]->old_profit_btc, 6, '.', '').'(BTC)</td><td>'.number_format($userinfo[0]->old_profit_eth, 6, '.', '').'(ETH)</td></tr>

        } */--}}

        <div class="col-sm-12">
            <label class="modal-title" style="color:red;">Deposits Balance Info: </label>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_usd, 6, '.', '') }} (USD)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_btc, 6, '.', '') }} (BTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_eth, 6, '.', '') }} (ETH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_bch, 6, '.', '') }} (BCH)</div>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_ltc, 6, '.', '') }} (LTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_dash, 6, '.', '') }} (DASH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_xrp, 6, '.', '') }} (XRP)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->balance_zec, 6, '.', '') }} (ZEC)</div>
        </div>
        <div class="">
            <div class="col-md-6">{{ number_format($userinfo[0]->balance_rsc, 6, '.', '') }} (RSC)</div>
            <div class="col-md-6">{{ number_format($userinfo[0]->balance_usdt, 6, '.', '') }} (USDT)</div>
        </div>

        <div class="col-sm-12">
            <label class="modal-title" style="color:red;">Sold Balance Info: </label>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_usd, 6, '.', '') }} (USD)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_btc, 6, '.', '') }} (BTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_eth, 6, '.', '') }} (ETH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_bch, 6, '.', '') }} (BCH)</div>
        </div>
        <div class="">
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_ltc, 6, '.', '') }} (LTC)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_dash, 6, '.', '') }} (DASH)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_xrp, 6, '.', '') }} (XRP)</div>
            <div class="col-sm-3">{{ number_format($userinfo[0]->sold_bal_zec, 6, '.', '') }} (ZEC)</div>
        </div>
        <div class="">
            <div class="col-md-6">{{ number_format($userinfo[0]->sold_bal_rsc, 6, '.', '') }} (RSC)</div>
            <div class="col-md-6">{{ number_format($userinfo[0]->sold_bal_usdt, 6, '.', '') }} (USDT)</div>
        </div>
    @endif
</div>
