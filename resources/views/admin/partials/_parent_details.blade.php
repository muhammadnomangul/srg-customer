<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="text-align:center;">User Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <label class="col-md-4" style="color:red;">User Info: </label>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>User ID : </label>{{ $userUID }}
        </div>
        <div class="col-md-6">
            <label>Name : </label>{{ $userName }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>User Plan : </label>{{ $planname }}
        </div>

        <div class="col-md-6">
            <label>Email ID : </label>{{ $email }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Phone : </label>{{ $userPhoneNo }}
        </div>
    </div>
</div>