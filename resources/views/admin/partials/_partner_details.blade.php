<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="text-align:center;">User Details</h4>
</div>
<div class="modal-body">
    <div class="row">
        <label class="col-md-4" style="color:red;">User Info: </label>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>User ID : </label>{{ $userUID }}
        </div>
        <div class="col-md-6">
            <label>Name : </label>{{ $userName }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>Parent ID : </label>{{ $userParentId }}
        </div>
        <div class="col-md-6">
            <label>User Plan : </label>{{ $planname }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Email ID : </label>{{ $email }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Phone : </label>{{ $userPhoneNo }}
        </div>
    </div>
    <div class="row col-md-12">
        <label class="modal-title" style="color:red;">Deposits Balance Info: </label>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>USD : </label>{{ number_format($userinfo[0]->balance_usd, 2, '.', '') }}
        </div>
        <div class="col-md-4">
            <label>BTC : </label>{{ number_format($userinfo[0]->balance_btc, 6, '.', '') }}
        </div>
        <div class="col-md-4">
            <label>ETH : </label>{{ number_format($userinfo[0]->balance_eth, 6, '.', '') }}
        </div>
    </div>
    <div class="row">

        <div class="col-md-4">
            <label>XRP : </label>{{ number_format($userinfo[0]->balance_xrp, 4, '.', '') }}
        </div>
        <div class="col-md-4">
            <label>BCH : </label>{{ number_format($userinfo[0]->balance_bch, 4, '.', '') }}
        </div>
        <div class="col-md-4">
            <label>LTC : </label>{{ number_format($userinfo[0]->balance_ltc, 4, '.', '') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label>DASH : </label>{{ number_format($userinfo[0]->balance_dash, 4, '.', '') }}
        </div>
        <div class="col-md-4">
            <label>ZEC : </label>{{ number_format($userinfo[0]->balance_zec, 4, '.', '') }}
        </div>
</div>
</div>