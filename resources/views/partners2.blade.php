@include('header')

<!-- //header-ends -->

<!-- main content start-->
<?php

$count1 = count($firstLine);
$count2 = count($secondLine);
$count3 = count($thirdLine);
$count4 = count($fourthLine);
$count5 = count($fifthLine);

$counter = 1;
$row2 = 1; $counter2 = 0;
$row3 = 1; $counter3 = 0;
$row4 = 1; $counter4 = 0;
$row5 = 1; $counter5 = 0;

?>
<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>
<div id="page-wrapper">
    <div class="main-page">

        <h3 class="title1">Your Referrel Partners</h3>

        @include('messages')
        @if(app('request')->session()->get('back_to_admin'))

            @include('partner_bonus')

        @endif
        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">


            <h3 style="margin-top:5px; text-align:center;">Your All Referrals Details and Total Invetments</h3><br>

            <table id="myTable" class="table table-hover">

                <thead>

                <tr>

                    <th>First Level Total</th>
                    <th>Second Level Total</th>
                    <th>Third Level Total</th>
                    @if(\Auth::user()->plan >= 3)
                        <th>Fourth Level Total</th>

                        <th>Fifth Level Total</th>
                    @endif
                    <th>Final Total</th>

                </tr>

                </thead>

                <tbody>
                <?php

                /*  $account_balance 		=	$firstTotal	+	$secondTotal +	$thirdTotal;
                 if(\Auth::user()->plan >= 3){
                     $account_balance 	= 	$account_balance + $fourthTotal + $fifthTotal;
                 }

                 */

                //$account_balance =$firstTotal+$secondTotal+$thirdTotal+$fourthTotal+$fifthTotal;

                ?>

                <tr>

                    <th scope="row">${{number_format($firstTotal, 2)}}</th>

                    <td>${{number_format($secondTotal, 2)}}</td>

                    <td>${{number_format($thirdTotal, 2)}}</td>
                    @if(\Auth::user()->plan >= 3)
                        <td>${{number_format($fourthTotal, 2)}}</td>

                        <td>${{number_format($fifthTotal, 2)}}</td>
                    @endif
                    <td><strong>${{number_format($account_balance, 2)}}</strong></td>

                <tr>

                </tbody>

            </table>

            <div id="tabs">

                <ul>

                    <li><a href="#tabs-1">1st Level ({{$userlevel1}})</a></li>

                    <li><a href="#tabs-2">2nd Level ({{$userlevel2}})</a></li>

                    <li><a href="#tabs-3">3rd Level ({{$userlevel3}})</a></li>
                    @if(\Auth::user()->plan >= 3)
                        <li><a href="#tabs-4">4th Level ({{$userlevel4}})</a></li>

                        <li><a href="#tabs-5">5th Level ({{$userlevel5}})</a></li>
                    @endif
                </ul>

                <div id="tabs-1">

                    <table id="myTable1" class="table table-hover">

                        <thead>

                        <tr>
                            <th>Sr.#</th>
                            <th>User Name</th>
                            <th>User ID</th>
                            <th>Parent ID</th>
                            <th>Total USD</th>
                            <th>Total Reinvest</th>
                            <th>Total Sold</th>
                            <th>Created At</th>

                        </tr>

                        </thead>

                        <?php //$counter = 1;?>

                        <tbody>

                        @if(isset($firstLine))

                            @foreach($firstLine as $lines)


                                <?php \App\Model\Referral::sync($lines->id); ?>
                                @if(isset($lines->u_id))

                                    <tr @if($lines->total == 0) style="color:red;" @endif >

                                        <th scope="row">{{$counter}}</th>

                                        <td>{{$lines->name}}</td>

                                        <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal"
                                                                href="#"
                                                                onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                        </td>

                                        <td>{{$lines->parent_id}} </td>

                                        <td> ${{number_format($lines->total, 2)}}</td>
                                        <td> ${{number_format($lines->totalReinvest, 2)}}</td>
                                        <td> ${{number_format($lines->totalSold, 2)}}</td>
                                        <td>{{date("Y-m-d",strtotime($lines->created_at))}}</td>
                                    </tr>

                                @endif

                                <?php $counter++; ?>

                            @endforeach

                        @endif

                        </tbody>

                    </table>

                </div>

                <div id="tabs-2">

                    <table id="myTable2" class="table table-hover">

                        <thead>

                        <tr>

                            <th>Sr.#</th>
                            <th>User Name</th>
                            <th>User ID</th>
                            <th>Parent ID</th>
                            <th>Total USD</th>
                            <th>Total Reinvest</th>
                            <th>Total Sold</th>
                            <th>Created At</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php // $row2 = 1; $counter2 = 0; ?>
                        @if(isset($secondLine))

                            @for($i=0; $i < $count2; $i++)

                                @foreach($secondLine[$counter2] as $lines)
                                    <?php \App\Model\Referral::sync($lines->id); ?>
                                    <tr @if($lines->total == 0) style="color:red;" @endif >

                                        <th scope="row">{{$row2}}</th>

                                        <td>{{$lines->name}}</td>

                                        <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal"
                                                                href="#"
                                                                onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                        </td>

                                        <td>{{$lines->parent_id}} </td>

                                        <td> ${{number_format($lines->total, 2)}}</td>
                                        <td> ${{number_format($lines->totalReinvest, 2)}}</td>
                                        <td> ${{number_format($lines->totalSold, 2)}}</td>
                                        <td>{{date("Y-m-d",strtotime($lines->created_at))}}</td>
                                    </tr>

                                    <?php $row2++;?>

                                @endforeach

                                <?php $counter2++;?>

                            @endfor

                        @endif

                        </tbody>

                    </table>

                </div>

                <div id="tabs-3">

                    <table id="myTable3" class="table table-hover">

                        <thead>

                        <tr>
                            <th>Sr.#</th>
                            <th>User Name</th>
                            <th>User ID</th>
                            <th>Parent ID</th>
                            <th>Total USD</th>
                            <th>Total Reinvest</th>
                            <th>Total Sold</th>
                            <th>Created At</th>
                        </tr>

                        </thead>


                        <tbody>

                        @if(isset($thirdLine))

                            @for($i=0; $i < $count3; $i++)

                                @foreach($thirdLine[$counter3] as $lines)

                                    <?php \App\Model\Referral::sync($lines->id); ?>
                                    <tr @if($lines->total == 0) style="color:red;" @endif >

                                        <th scope="row">{{$row3}}</th>

                                        <td>{{$lines->name}}</td>

                                        <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal"
                                                                href="#"
                                                                onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                        </td>

                                        <td>{{$lines->parent_id}} </td>

                                        <td> ${{number_format($lines->total, 2)}}</td>
                                        <td> ${{number_format($lines->totalReinvest, 2)}}</td>
                                        <td> ${{number_format($lines->totalSold, 2)}}</td>
                                        <td>{{date("Y-m-d",strtotime($lines->created_at))}}</td>
                                    </tr>

                                    <?php $row3++;?>

                                @endforeach

                                <?php $counter3++;?>

                            @endfor

                        @endif

                        </tbody>

                    </table>

                </div>
                @if(\Auth::user()->plan >= 3)
                    <div id="tabs-4">

                        <table id="myTable4" class="table table-hover">

                            <thead>

                            <tr>

                                <th>Sr.#</th>
                                <th>User Name</th>
                                <th>User ID</th>
                                <th>Parent ID</th>
                                <th>Total USD</th>
                                <th>Total Reinvest</th>
                                <th>Total Sold</th>
                                <th>Created At</th>

                            </tr>

                            </thead>

                            <tbody>

                            @if(isset($fourthLine))


                                @for($i=0; $i < $count4; $i++)

                                    @foreach($fourthLine[$counter4] as $lines)

                                        <?php \App\Model\Referral::sync($lines->id); ?>
                                        <tr @if($lines->total == 0) style="color:red;" @endif >

                                            <th scope="row">{{$row4}}</th>

                                            <td>{{$lines->name}}</td>

                                            <td class="linksAll"><a data-toggle="modal"
                                                                    data-target="#PartnersDetailsModal" href="#"
                                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                            </td>

                                            <td>{{$lines->parent_id}} </td>

                                            <td> ${{number_format($lines->total, 2)}}</td>
                                            <td> ${{number_format($lines->totalReinvest, 2)}}</td>
                                            <td> ${{number_format($lines->totalSold, 2)}}</td>
                                            <td> {{date("Y-m-d",strtotime($lines->created_at))}}</td>

                                        </tr>

                                        <?php $row4++;?>

                                    @endforeach

                                    <?php $counter4++;?>

                                @endfor

                            @endif

                            </tbody>

                        </table>

                    </div>

                    <div id="tabs-5">

                        <table id="myTable5" class="table table-hover">

                            <thead>

                            <tr>
                                <th>Sr.#</th>
                                <th>User Name</th>
                                <th>User ID</th>
                                <th>Parent ID</th>
                                <th>Total USD</th>
                                <th>Total Reinvest</th>
                                <th>Total Sold</th>
                                <th>Created At</th>
                            </tr>

                            </thead>

                            <tbody>

                            @if(isset($fifthLine))

                                @for($i=0; $i < $count5; $i++)

                                    @foreach($fifthLine[$counter5] as $lines)
                                        <?php \App\Model\Referral::sync($lines->id); ?>
                                        <tr @if($lines->total == 0) style="color:red;" @endif >

                                            <th scope="row">{{$row5}}</th>

                                            <td>{{$lines->name}}</td>

                                            <td class="linksAll"><a data-toggle="modal"
                                                                    data-target="#PartnersDetailsModal" href="#"
                                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                            </td>

                                            <td>{{$lines->parent_id}} </td>

                                            <td> ${{number_format($lines->total, 2)}}</td>
                                            <td> ${{number_format($lines->totalReinvest, 2)}}</td>
                                            <td> ${{number_format($lines->totalSold, 2)}}</td>
                                            <td>{{date("Y-m-d",strtotime($lines->created_at))}}</td>

                                        </tr>

                                        <?php $row5++;?>

                                    @endforeach

                                    <?php $counter5++;?>

                                @endfor

                            @endif

                            </tbody>

                        </table>

                    </div>
                @endif
            </div>

        </div>

    </div>

</div>

<!-- Partners Details Modal -->
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ucontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /Partners Details Modal -->


<script>
    /* function viewDetailsFunc(id)
      {
          var type = "partners";
          jQuery.ajax({
              type: "POST",
              url: '{!!URL::to("/")!!}/dashboard/viewUserDetails',
            data: {id : id,type:type,"_	token": "{{ csrf_token() }}",},
            success: function(response)
            {
                console.log(response);
                $("#pcontent").html("");
                $("#pcontent").html(response);
            },error:function(response)
            {
                console.log(response);
                alert("error!!!!");
            }
        });
    }


    function postData(url = ``, data = {}) {
        // Default options are marked with *
        return fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
        // parses response to JSON
    }
 */
    function viewDetailsFunc(id) {
        var partner = '1';
        postData('{{{URL::to("")}}}/dashboard/view-partner-details', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                //console.log(response)
                return response.text()
            })
            .then(function (html) {
                html = JSON.parse(html);
                $(".ucontent").html("");
                $(".ucontent").html(html);

            }) // JSON-string from `response.json()` call
            .catch(function (error) {
                //console.error(error)
                alert("error!!!ddd!");
            });
    }

    $(document).ready(function () {

        $('#myTable1').DataTable({
            "pagingType": "full_numbers"
        });

        $('#myTable2').DataTable({
            "pagingType": "full_numbers"
        });

        $('#myTable3').DataTable({
            "pagingType": "full_numbers"
        });

        $('#myTable4').DataTable({
            "pagingType": "full_numbers"
        });

        $('#myTable5').DataTable({
            "pagingType": "full_numbers"
        });

    });

    $(function () {
        $("#tabs").tabs();
    });

</script>
@include('footer')
