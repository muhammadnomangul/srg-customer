@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				
			@include('messages')

				<div class="sign-up-row widget-shadow">
					<form method="post" action="{{action('UsersController@dnate')}}">
					<div class="sign-u">
						<div class="sign-up1">
							<h4>Amount to donate* : <small>min {{$settings->currency}} {{$settings->min_donation}} and max {{$settings->currency}} {{$settings->max_donation}}.</h4>
						</div>
						<div class="sign-up2">
							<input type="text" placeholder="Amount to donate" name="amount" required>
						</div>
						<div class="clearfix"> </div>
					</div>

					<div class="sub_home">
						<input type="submit" value="Submit">
						<div class="clearfix"> </div>
					</div>
					<input type="hidden" name="donated_by" value="{{Auth::user()->username}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
				</form>
				</div>
			</div>
		</div>
		@include('footer')