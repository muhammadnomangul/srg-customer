<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>White List IPs</h1>
</div>
  
<div class="container">
  <div class="row">
   <h2>Admin</h2>
   <form class='form' action="{{URL::to('/')}}/dashboard/whitelist/new">
       {!! csrf_field() !!}
       <input type='hidden' name='type' value='admin' />
       <input name='ip' value=''  require/>
       <input type='submit'  value='add admin ip'/>  
   </form>
   <table class="table table-striped">
       <thead>
           <tr>
               <th>ip</th>

               <th>actions</th>

           </tr>
       </thead>
       
       <tbody>
           
           @foreach($whitelist['admin'] as $ip)
           <tr>
               <td>{{$ip}}</td>
               <td><a href="{{URL::to('/')}}/dashboard/whitelist/delete?ip={{$ip}}&type=admin">delete</a></td>
           
           </tr>
           @endforeach
           
       </tbody>
   </table>
   <h2>Manager</h2>
   <form class='form' action="{{URL::to('/')}}/dashboard/whitelist/new">
       {!! csrf_field() !!}
       <input type='hidden' name='type' value='manager' />
       <input name='ip' value=''  require/>
       <input type='submit'  value='add manager ip'/>
       
       
   </form>
   <table class="table table-striped">
       <thead>
           <tr>
               <th>ip</th>

               <th>actions</th>

           </tr>
       </thead>
       
       <tbody>
           @foreach($whitelist['manager'] as $ip)
           <tr>
               <td>{{$ip}}</td>
               <td><a href="{{URL::to('/')}}/dashboard/whitelist/delete?ip={{$ip}}&type=manager">delete</a></td>
           
           </tr>
           @endforeach
           
       </tbody>
   </table>
  </div>
</div>

</body>
</html>
