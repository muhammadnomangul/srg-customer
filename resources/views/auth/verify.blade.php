<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

        * {
            color: black;
        }

        .termss1 input checkbox {
            width: 0px !important;
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('home/home/js/jquery.disableAutoFill.min.js') }}"></script>
    <script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
</head>
<?php

$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if (strpos($url, '?') !== false) {
    $ur2 = "exist";
} else {
    $ur2 = "notexist";
}

if ($ur2 == "exist") {
    $urlArr = explode("?", $url);
    if (isset($urlArr) && isset($urlArr[1])) {
        $parentId = trim($urlArr[1]);
    } else {
        $parentId = "B4U0001";
    }
} else {
    $parentId = "B4U0001";
}
//echo $parent_id;
//exit;
//$validURL = str_replace("&", "&amp", $url);
?>
<body class="auth-page" style="background-color:#f8f8f8;">
<style>
    .red-border {
        border: 1px solid red;
    }
</style>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->

                <div class="text-center mb-4">
                    @if(isset($settings->logo))
                        <a href="{{url('/')}}" style="text-align:center; width:100%; color:#555;">

                            <img id="logo" class="img-responsive"
                                 src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <img src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>

                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                @endif
                @if ($errors->has('g-recaptcha-response') )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                            </div>
                        </div>
                    </div>

            @endif
            <!-- Logo Ends -->
                <div class="form-container">
                    <div>
                        <!-- Section Title Starts -->
                        <div class="row text-center">
                            <h3 class="title-head" style="font-size:1.5em; color:#555;">Member Verification</h3>
                            <div class="alert alert-success" role="alert">
                                <h6 style="margin: 0;
    font-weight: 800;
    font-size: 16px;
    line-height: 45px;
    color: #0e0e0ea1;
    padding: 10px 0 20px;
    position: relative;
    text-transform: uppercase;
    margin: 0 15px;">
                                    Your B4U U ID is <b
                                            style="font-size: 30px">{{ \Illuminate\Support\Facades\Auth::user()->u_id }}</b>.
                                    <br>
                                    Please keep it secure, it will use for the Login into B4U Global
                                </h6>
                            </div>
                            <span id="error_msg" style="color: red;"></span>
                        </div>

                        <div class="form-container">

                            @if(Session::has('errormsg'))
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                &times;
                                            </button>
                                            <i class="fa fa-ban"></i> {{ Session::get('errormsg') }}
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('An email has sent to your mentioned Email Address with instructions, Please check Inbox, Spam, Junk, Promotion Sections  ') }}
                                </div>
                            @else
                                <div class="alert alert-info anEmailSentPara" role="alert">
                                    Please Click the Below <b>Click To Send Verification Email</b> Button, to verify
                                    your email address.
                                </div>
                            @endif

                            <div class="alert alert-info anEmailSentWaitPara hide" role="alert">
                                An email has sent to your Given Email address, You have to wait till 2:00 seconds to
                                resend an Email Address
                            </div>

                            <div>
                                <form class="d-inline" method="POST"
                                      action="{{ route('verification.resend') }}">
                                    @csrf
                                    <input type="email" name="user_email"
                                           class="form-control"
                                           value="{{ \Illuminate\Support\Facades\Auth::user()->email }}"
                                           style="background-color: white; color: black">

                                    <div class="form-group" style="margin-top: 20px">
                                        <button class="btn btn-lg btn-primary btn-block sendEmailVerificationButton"
                                                type="submit">
                                            Click to Send Verification Email
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-center copyright-text">Copyright © {{date('Y')}} {{$settings->site_name}} All Rights
                    Reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->


<script type="text/javascript">
    let IsEmailSent = "{{  session('resent') }}";
    let lastMailSendingTime = "{{   \Carbon\Carbon::createFromTimestamp(strtotime(\Illuminate\Support\Facades\Auth::user()->last_email_send_at))->timezone('UTC')->toDateTimeString()  }}";
    let timerCountDown = '2:00';
    let timerCountDownDefined = null;

    function getDateTime() {
        let dateObject = new Date();
        return dateObject.getUTCFullYear() + '-' + parseInt(dateObject.getUTCMonth() + 1) + '-' + dateObject.getUTCDate() + ' ' + dateObject.getUTCHours() + ':' + dateObject.getUTCMinutes() + ':' + dateObject.getUTCSeconds();
    }

    function msToTime(s) {
        // Pad to 2 or 3 digits, default is 2
        function pad(n, z) {
            z = z || 2;
            return ('00' + n).slice(-z);
        }

        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        // return pad(hrs) + ':' + pad(mins) + ':' + pad(secs) + '.' + pad(ms, 3);
        return pad(mins) + ':' + pad(secs);
    }

    $(function () {

        if (lastMailSendingTime) {
            let emailVerificationBtn = $('.sendEmailVerificationButton');
            let emailVerificationBtnText = $(emailVerificationBtn).text().trim();
            timeCount();
            $(emailVerificationBtn).html("<i style='color:white' class=\'fa fa-spinner fa-spin\'></i> Loading ");
            $(emailVerificationBtn).attr('disabled', true);
            // disable verification button
            let setIntervalFunc = setInterval(function () {

                let diff = Math.abs(new Date(getDateTime()) - new Date(lastMailSendingTime));
                // console.log(getDateTime());
                // console.log(lastMailSendingTime);

                let seconds = Math.floor(diff / 1000); //ignore any left over units smaller than a second
                let minutes = Math.floor(seconds / 60);
                seconds = seconds % 60;
                let hours = Math.floor(minutes / 60);
                minutes = minutes % 60;

                if (!timerCountDownDefined) {
                    timerCountDown = minutes + ':' + seconds;
                    console.log(timerCountDown);
                    timerCountDownDefined = true;
                }

                if ((hours >= 0) && (minutes >= 2 && seconds > 0)) {
                    $(emailVerificationBtn).attr('disabled', false);
                    $(emailVerificationBtn).text(emailVerificationBtnText);
                    $('.anEmailSentPara').show();
                    $('.anEmailSentWaitPara').addClass('hide');
                    clearInterval(setIntervalFunc);
                } else {
                    // timerCountDown = minutes + ":" + seconds;
                    $(emailVerificationBtn).html("<i class=\'fa fa-spinner fa-spin\'></i> Retry in " + timerCountDown + "  ");
                    $('.anEmailSentPara').hide();
                    $('.anEmailSentWaitPara').removeClass('hide');
                }

            }, 1000);

            function timeCount() {
                let interval = setInterval(function () {
                    var timer = timerCountDown.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    timerCountDown = minutes + ':' + seconds;
                }, 1000);
            }
        }


    });
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172512767-1"></script>

</body>

</html>

