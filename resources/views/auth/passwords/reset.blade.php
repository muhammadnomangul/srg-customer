<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

    </style>
</head>

<body class="auth-page" style="background-color:#f8f8f8;">

<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->
                <div class="text-center">
                    @if(!(isset($settings->logo)))
                        <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
                            <img id="logo" class="img-responsive"
                                 src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <image src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                   style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>
                <!-- Logo Ends -->
                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif
                <div class="form-container">
                    <div>
                        <!-- Section Title Starts -->
                        <div class="row text-center">
                            <h2 class="title-head" style="font-size:1.5em; color:#555;">Reset Password</h2>
                        </div>
                        <!-- Section Title Ends -->
                        <!-- Form Starts -->

                        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-8">
                                    <input style="background:transparent; color:#555;" class="form-control" id="email"
                                           type="email" name="email" value="{{ isset($email) ? $email : old('email')}}"
                                           required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-8">
                                    <input style="background:transparent; color:#555;" id="password" type="password"
                                           class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-8">
                                    <input style="background:transparent; color:#555;" id="password-confirm"
                                           type="password" class="form-control" name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>

                        <!-- Form Ends -->
                    </div>
                </div>

            </div>
        </div>
        <!-- Copyright Text Starts -->
        <p class="text-center copyright-text">Copyright
            © {{date('Y')}} @if(isset($settings->site_name)){{$settings->site_name}} @else B4U Global @endif All Rights
            Reserved</p>
        <!-- Copyright Text Ends -->
    </div>
</div>
</div>
</div>
<!-- Wrapper Ends -->
</body>

</html>