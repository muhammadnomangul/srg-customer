<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

    </style>
</head>

<body class="auth-page" style="background-color:#f8f8f8;">
<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->
                <div class="text-center">
                    @if(isset($settings->logo))
                        <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">

                            <img id="logo" class="img-responsive" 
                            src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <img src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                   style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>
                <!-- Logo Ends -->
                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($rmessage))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-ok"></i> {{ $rmessage }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif
                <div class="form-container">
                    <div>
                        <!-- Section Title Starts -->
                        <div class="row text-center">
                            <h2 class="title-head" style="font-size:1.5em; color:#555;">Reset Password</h2>
                        </div>
                        <!-- Section Title Ends -->
                        <!-- Form Starts -->
                        <div class="card-content">
                            @if (session('status'))
                                <div class="notification">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="forgot-password-form" method="POST" action="{{ route('password.email') }}">

                                {{ csrf_field() }}

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <label class="label" style="color:black">E-Mail Address</label>
                                    </div>

                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                <input style="background:transparent; color:#555;" class="form-control"
                                                       id="email" type="email" name="email"
                                                       maxlength="50"
                                                       placeholder="ENTER YOUR EMAIL"
                                                       value="{{ old('email') }}" required autofocus pattern="(?![^@]{65})[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*">
                                            </p>

                                            @if ($errors->has('email'))
                                                <p class="help is-danger" style="color:red;">
                                                    {{ $errors->first('email') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label"></div>

                                    <div class="field-body">
                                        <div class="field is-grouped">
                                            <div class="control">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block">Send
                                                    Password Reset Link
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group">
                                <p class="text-center">Don't have an account? <a href="{{route('register')}}">register
                                        now</a></p>
                            </div>
                            <!-- Form Ends -->
                        </div>
                    </div>
                    <!-- Copyright Text Starts -->
                    <p class="text-center copyright-text">Copyright
                        © {{date('Y')}} @if(isset($settings->site_name)){{$settings->site_name}} @else B4U Global @endif
                        All Rights Reserved</p>
                    <!-- Copyright Text Ends -->
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
$(function()
{
    $(document).on('click', 'button[type="submit"]', function(e)
    {
        $('button').prop('disabled', true);
        $(this).closest('form').submit();
      //  $('input, a').prop('readonly', true);
        var buttonText = $(this).html();
        $(this).html('<i class="fa fa-spinner spin"></i> Processing...');
        $(this).html(buttonText);
    });
});

</script>
</body>

</html>