<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

        .red-border {
            border: 1px solid red;
        }
    </style>

</head>

<body class="auth-page" style="background-color:#f8f8f8;">

<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->
                <div class="text-center">

                    @if(isset($settings->logo))

                        <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
                            <img id="logo" class="img-responsive"
                                 src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <img src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>
                <!-- Logo Ends -->
                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                @endif
                @if ($errors->has('g-recaptcha-response') )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                            </div>
                        </div>
                    </div>

                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif
                <div class="form-container">
                    <div>
                        <div class="row text-center">
                            <h2 class="title-head" style="font-size:1.5em; color:#555;">member login</h2>
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{csrf_field()}}
                            <div class="form-group">

                                <input id="login" type="text" name="login" placeholder="ENTER YOUR B4U ID OR Email"
                                       style="background:transparent; color:#555;"
                                       class="form-control {{ $errors->has('u_id') || $errors->has('email') || $errors->has('login') ? ' red-border' : '' }}"
                                       value="{{ old('u_id') ?: old('email') }}" required autofocus>

                                @if ($errors->has('u_id') || $errors->has('email') || $errors->has('login'))
                                    <span class="help-block" style="color:red">
                                        <strong>{{$errors->first('u_id') ?: $errors->first('email')  }}</strong>
                                        <strong>{{$errors->first('login')  }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <input style="background:transparent; color:#555;"
                                       class="form-control {{ $errors->has('password') ? 'red-border' :'' }}"
                                       id="password" name="password" id="password" placeholder="ENTER PASSWORD"
                                       type="password" required>
                                <input name="status" type="hidden" value="active">
                                @if ($errors->has('password'))
                                    <span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Captcha::display([]) !!}
                            </div>

                            <!-- Input Field Ends -->
                            <!-- Submit Form Button Starts -->
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">login</button>
                                <p class="text-center">Don't have an account? <a href="{{route('register')}}">Register
                                        Now</a></p>
                                <p class="text-center">Forgot Your Password? <a href="{{ route('password.request') }}">Forget
                                        Password</a></p>
                            </div>

                            <!-- Submit Form Button Ends -->
                        </form>

                        <!-- Form Ends -->
                    </div>
                </div>
                <!-- Copyright Text Starts -->
                <p class="text-center copyright-text">Copyright
                    © {{date('Y')}} @if(isset($settings->site_name)){{$settings->site_name}} @else B4U Global @endif All
                    Rights Reserved</p>
                <!-- Copyright Text Ends -->
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<!-- <script src="{{ asset('js/jquery-ui.js')}}"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $(document).on('click', 'button[type="submit"]', function (e) {
            $('button').prop('disabled', true);
            $(this).closest('form').submit();
            //$('input, a').prop('readonly', true);
            var buttonText = $(this).html();
            $(this).html('<i class="fa fa-spinner spin"></i> Processing...');
            $(this).html(buttonText);
        });
    });

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172512767-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-172512767-1');

</script>
<!--  LiveChat Widget-->
@include('liveChatScript')
</body>

</html>
