<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

        .termss1 input checkbox {
            width: 0px !important;
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('home/home/js/jquery.disableAutoFill.min.js') }}"></script>
    <script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
</head>
<?php

$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if (strpos($url, '?') !== false) {
    $ur2 = "exist";
} else {
    $ur2 = "notexist";
}

if ($ur2 == "exist") {
    $urlArr = explode("?", $url);
    if (isset($urlArr) && isset($urlArr[1])) {
        $parentId = trim($urlArr[1]);
    } else {
        $parentId = "B4U0001";
    }
} else {
    $parentId = "B4U0001";
}
//echo $parent_id;
//exit;
//$validURL = str_replace("&", "&amp", $url);
?>
<body class="auth-page" style="background-color:#f8f8f8;">
<style>
    .red-border {
        border: 1px solid red;
    }
</style>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->

                <div class="text-center mb-4">
                    @if(isset($settings->logo))
                        <a href="{{url('/')}}" style="text-align:center; width:100%; color:#555;">

                            <img id="logo" class="img-responsive"
                                 src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <img src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>

                @include('messages')
                {{--                @if(Session::has('message'))--}}
                {{--                    <div class="row">--}}
                {{--                        <div class="col-lg-12">--}}
                {{--                            <div class="alert alert-danger alert-dismissable">--}}
                {{--                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;--}}
                {{--                                </button>--}}
                {{--                                <i class="fa fa-warning"></i> {{ Session::get('message') }}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                @endif--}}
                @if ($errors->has('g-recaptcha-response') )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                            </div>
                        </div>
                    </div>

            @endif
            <!-- Logo Ends -->
                <div class="form-container">
                    <div>
                        <!-- Section Title Starts -->
                        <div class="row text-center">
                            <h3 class="title-head" style="font-size:1.5em; color:#555;">member registration</h3>
                            <span id="error_msg" style="color: red;"></span>
                        </div>
                        <!-- Section Title Ends -->
                        <!-- Form Starts -->
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}" autocomplete="off"
                              id="register_form">
                        {{csrf_field()}}
                        <!-- Input Field Starts -->
                            <div class="form-group">
                                <input style="background:transparent; color:#555;"
                                       class="form-control @if ($errors->has('name')) red-border @endif" id="name"
                                       placeholder="ENTER YOUR FULL NAME" name="name" type="text"
                                       value="{{ old('name') }}"
                                       required autofocus
                                >
                                @if ($errors->has('name'))
                                    <span class="help-block" style="color:red">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
                                @endif
                            </div>
                            <!-- Input Field Ends -->

                            <!-- Input Field Starts -->
                            <div class="form-group">
                                <input style="background:transparent; color:#555;"
                                       class="form-control @if ($errors->has('email')) red-border @endif" id="email"

                                       placeholder="ENTER YOUR EMAIL" name="email" type="email"
                                       value="{{ old('email') }}" required="required" maxlength="42">
                                @if ($errors->has('email'))
                                    <span class="help-block" style="color:red">
                        											<strong>{{ $errors->first('email') }}</strong>
                        										</span>
                                @endif
                            </div>
                            <!-- Input Field Ends -->
                            <!-- Input Field Starts -->
                            <div class="form-group">
                                <input style="background:transparent; color:#555;"
                                       class="form-control @if ($errors->has('password')) red-border @endif"
                                       name="password"
                                       placeholder="ENTER PASSWORD" type="password" required autocomplete="off">

                                <span toggle="#password-field"
                                      class="fa fa-fw fa-eye field-icon toggle-password"></span>

                                @if ($errors->has('password'))
                                    <span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        <p> <strong>Example:: </strong> Password@@0123 </p>
                                    </span>
                                @endif
                            </div>
                            <!-- Input Field Ends -->
                            <!-- Input Field Starts -->
                            <div class="form-group">
                                <input style="background:transparent; color:#555;"
                                       class="form-control @if ($errors->has('password_confirmation')) red-border @endif"
                                       placeholder="RE-ENTER YOUR PASSWORD" name="password_confirmation" type="password"
                                       required autocomplete="off">

                                <span toggle="#password-field"
                                      class="fa fa-fw fa-eye field-icon toggle-password"></span>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block" style="color:red">
											<strong>{{ $errors->first('password_confirmation') }}</strong>
										</span>
                                @endif

                            </div>
                            <!-- Input Field Ends -->

                            <!-- Input Field Starts -->
                            <div class="form-group">

                                <input id="parent_id_input"
                                       class="form-control @if ($errors->has('parent_id')) red-border @endif"
                                       @if(isset($parentId)) value="{{$parentId}}" @else  value="{{old('parent_id')}}"
                                       @endif name="parent_id" type="text"
                                       placeholder="ENTER YOUR Parent ID, If Don't Know Any Use Our Default 'B4U0001'"
                                       style="background:transparent; color:#555;" maxlength="30">
                                <span id="invalid-prnt"></span>

                                @if ($errors->has('parent_id'))
                                    <span class="help-block" style="color:red">
											<strong>{{ $errors->first('parent_id') }}</strong>
										</span>
                                @endif

                            </div>
                            <!-- Input Field Ends -->
                            <!-- Input Field Starts -->
                            <div class="form-group">

                                <input style="background:transparent; color:#555;" type="tel"
                                       class="form-control @if ($errors->has('phone_no')) red-border @endif"
                                       id="phone_no" placeholder="" name="phone_no"
                                       value="{{ old('phone_no') }}"
                                       required autocomplete="disabled">
                                <input name="country" id="country_name" type="hidden" autocomplete="off">
                                <input name="country_coe" id="country_coe" type="hidden" autocomplete="off">
                                <span id="valid-msg" class="hide" style="color:green">✓ Valid</span>
                                <span id="error-msg" class="hide" style="color:red"></span>

                                @if ($errors->has('phone_no'))
                                    <span class="help-block" style="color:red">
											<strong>{{ $errors->first('phone_no') }}</strong>
										</span>
                                @endif
                            </div>
                            <!-- Input Field Ends -->
                            <!-- Input Field Starts -->
                            <div class="form-group">
                                {!! Captcha::display([]) !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block" style="color:red">
											<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
										</span>
                                @endif
                            </div>
                            <!-- Input Field Ends -->

                        <!-- Input Field Starts
								<div class="form-group">
									<input class="form-control" value="{{1}}" name="country" type="text" placeholder="ENTER YOUR COUNTRY"style="background:transparent; color:#555;">
								</div>-->
                            <!-- Input FiEnds -->

                            <!-- Input Field Starts -->
                            <div class="form-group">
                                <div style="float: left;"><input style="width: 20px;" name="sign" type="checkbox"
                                                                 {{ old('sign') ? "checked='checked'" : '' }}
                                                                 required="required"></div>
                                <div style="float: left; margin: 4px 0px 0px 4px;"><p> Accept <a
                                                href="{{ route('terms') }}">Terms and Conditions</a> and
                                        <a href="{{ route('privacy') }}">Privacy Policy</a></p></div>
                            </div>

                            @if ($errors->has('sign'))
                                <span class="help-block" style="color:red">
											<strong>{{ $errors->first('sign') }}</strong>
										</span>
                        @endif

                        <!-- Input Field Ends -->
                            <!-- Submit Form Button Starts -->
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit"
                                        id="register_btn">Create account
                                </button>

                                <p class="text-center">Already a member? <a href="{{route('login')}}">Login Now</a></p>
                            </div>
                            <!-- Submit Form Button Ends -->
                        </form>
{{--                        <p class="text-center agreement-text"><a href="{{ url('/terms') }}">Terms and Conditions</a></p>--}}
                        <!-- Form Ends -->
                    </div>
                </div>
                <!-- Copyright Text Starts -->
                <p class="text-center copyright-text">Copyright © {{date('Y')}} {{$settings->site_name}} All Rights
                    Reserved</p>
                <!-- Copyright Text Ends -->

            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#name").keypress(function (e) {
            $("#error_msg").html("");
            var key = e.keyCode;
            $return = (key == 8 || key == 32 || (key >= 48 && key <= 57) || (key > 64 && key < 91) || (key > 96 && key < 123));
            if (!$return) {
                $("#error_msg").html("No special characters Please!");
                return false;
            }
        });
        $('#name').bind("cut copy paste", function (e) {
            e.preventDefault();
        });
        $("#register_form").submit(function (e) {
            //disable the submit button
            $("#register_btn").attr("disabled", true);
            return true;


        });
    });
</script>
<script>

    $(document).ready(function () {

        //$('input.makeReadOnly').parent().addClass("makeReadOnlyouter");
        //$('input.makeReadOnly').keydown(function() { return false; });


        $(".toggle-password").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if ($(this).prev('input').attr('type') === "password") {
                $(this).prev('input').attr("type", "text");
            } else {
                $(this).prev('input').attr("type", "password");
            }
        })

        // disable auto fill input fileds.
        $("#register_form").disableAutoFill();
        var input = document.querySelector("#phone_no");

        var errorMsg = document.querySelector("#error-msg");
        var validMsg = document.querySelector("#valid-msg");
        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];


        var iti = window.intlTelInput(input, {
            nationalMode: false,
            autoHideDialCode: true,
            autoPlaceholder: "polite",
            formatOnDisplay: true,
            placeholderNumberType: "MOBILE",
            preferredCountries: ['us', 'pk', 'my', 'gb'],
            utilsScript: "{{ asset('home/flag_phone_input/js/utils.js')}}",
        });
        let countryRecord = iti.getSelectedCountryData();
        var countryData = iti.getSelectedCountryData().name;
        input.addEventListener('countrychange', function () {
            let countryRecord = iti.getSelectedCountryData();
            var countryData = countryRecord.name;
            document.querySelector("#country_name").value = countryData.split("(")[0];
            document.querySelector("#country_coe").value = countryRecord.iso2;
        });
        document.querySelector("#country_name").value = countryData.split("(")[0];
        document.querySelector("#country_coe").value = countryRecord.iso2;


        var reset = function () {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };
        input.addEventListener('blur', function () {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    validMsg.classList.remove("hide");
                    input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    if (errorMap[errorCode]) {
                        errorMsg.innerHTML = errorMap[errorCode];
                        errorMsg.classList.remove("hide");
                    } else {
                        errorMsg.innerHTML = errorMap[0];
                        errorMsg.classList.remove("hide");
                    }

                    input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                }
            }
        });
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);


        $("#parent_id_input").inputmask({
            regex: "^([B]{1}[4]{1}[U]{1}[0]{1}[0-9]{100})$",
            greedy: false,
            placeholder: ''
        });

    });
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-172512767-1');
</script>
<!--  LiveChat Widget-->
@include('liveChatScript')
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172512767-1"></script>

</body>

</html>

