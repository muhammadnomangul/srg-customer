<!DOCTYPE html>
<html lang="en">
<head>
  <title>Validate Admin Pin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Pin Verification</h1>
  <p>Your ip address is {{$client_ip}}</p>
</div>
  
<div class="container">
  <div class="row">
    <div class="col-sm-12">
        @if ($errors->has('g-recaptcha-response') )
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                    </div>
                </div>
            </div>


        @endif
        <form action="{{route('postIp')}}" method='POST'>

          {!! csrf_field() !!}
		  <div class="form-group">
			<label for="pin">PIN:</label>
			<input type="password" class="form-control" name='pin' id="pin">
		  </div>
         <!-- <div class="form-group">
              {!! Captcha::display([]) !!}


          </div>-->

          <button type="submit" class="btn btn-default">Submit</button>
</form> 
    </div>
    
  </div>
</div>

</body>
</html>
