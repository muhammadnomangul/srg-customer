@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				@include('messages')
				<div class="sign-up-row widget-shadow">
					<form method="post" action="{{action('UsersController@updatepass')}}">
						<h5>Change Password :</h5>
						<div class="sign-u">
							<div class="sign-up1">
								<h4>Old Password* :</h4>
							</div>
							<div class="sign-up2">
								<input type="password" name="old_password" required>
							</div>
							<div class="clearfix"> </div>

							@if ($errors->has('old_password'))
								<span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
							@endif
						</div>
						<div class="sign-u">
							<div class="sign-up1">
								<h4>New Password* :</h4>
							</div>
							<div class="sign-up2">
								<input type="password" name="password" required>
							</div>
							<div class="clearfix"> </div>

							@if ($errors->has('password'))
								<span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        <p> <strong>Example:: </strong> Password@@0123 </p>
                                    </span>
							@endif
						</div>
						
						<div class="sign-u">
							<div class="sign-up1">
								<h4>Confirm* :</small></h4>
							</div>
							<div class="sign-up2">
								<input type="password" name="password_confirmation" required>
							</div>
							<div class="clearfix"> </div>
						</div>
						
						<div class="sub_home">
							<input type="submit" value="Submit">
							<div class="clearfix"> </div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
					</form>
				</div>
			</div>
		</div>
		@include('footer')