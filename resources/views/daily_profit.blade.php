@include('header')
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Daily Profit's List</h3>
        @include('messages')
        <div class="row">
            <div style="float:left;">
                @if(app('request')->session()->get('back_to_admin'))
                    Total Earnings In USD :
                    @if(isset($total_USD))
                        ${{number_format($total_USD, 2)}}
                    @else
                        $0.00
                    @endif
                @endif
            </div>
            @if(app('request')->session()->get('back_to_admin'))
                <div style="float:right; text-align:right;"><a class="btn btn-default" href="#" data-toggle="modal"
                                                               data-target="#profitDetailsModal">
                        View Profit Details</a></div>
            @endif
        </div>
        <div class="row">
            <!---->
            <div class="col-xs-12">
                <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
                    <table id="myTable" class="table table-hover">

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Deposit Modal -->
<div id="profitDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Profit Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <h4 class="modal-title" style="color:red;"><strong>Current Profit :</strong></h4>
                    <div class="col-sm-4">
                        @if(isset($totalProfitBonusUSD))

                            USD : {{number_format($totalProfitBonusUSD, 2)}}
                        @endif
                    </div>

                    <div class="col-sm-4">
                        @if(isset($totalProfitBonusBTC))

                            BTC : {{number_format($totalProfitBonusBTC, 8)}}
                        @endif
                    </div>
                    <div class="col-sm-4">
                        @if(isset($totalProfitBonusETH))

                            ETH : {{number_format($totalProfitBonusETH, 6)}}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <h4 class="modal-title" style="color:red;"><strong>Previous Profit :</strong></h4>
                    <div class="col-sm-4">
                        @if(isset($old_profit_usd))

                            USD : {{number_format($old_profit_usd, 2)}}
                        @endif
                    </div>

                    <div class="col-sm-4">
                        @if(isset($old_profit_btc))

                            BTC : {{number_format($old_profit_btc, 8)}}
                        @endif
                    </div>
                    <div class="col-sm-4">
                        @if(isset($old_profit_eth))

                            ETH : {{number_format($old_profit_eth, 6)}}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <h4 class="modal-title" style="color:red;"><strong>Current Profit Withdrawn :</strong></h4>
                    <div class="col-sm-4">
                        @if(isset($withdrawn_usd))

                            USD  : {{number_format($withdrawn_usd, 2)}}
                        @endif
                    </div>

                    <div class="col-sm-4">
                        @if(isset($withdrawn_btc))

                            BTC  : {{number_format($withdrawn_btc, 8)}}
                        @endif
                    </div>
                    <div class="col-sm-4">
                        @if(isset($withdrawn_eth))

                            ETH  : {{number_format($withdrawn_eth, 6)}}
                        @endif
                    </div>
                </div>
                <div class="row">
                    <h4 class="modal-title" style="color:red;"><strong>Previous Profit Withdrawn :</strong></h4>
                    <div class="col-sm-4">
                        @if(isset($pre_withdrawn_usd))

                            USD : {{number_format($pre_withdrawn_usd, 2)}}
                        @endif
                    </div>

                    <div class="col-sm-4">
                        @if(isset($pre_withdrawn_btc))

                            BTC : {{number_format($pre_withdrawn_btc, 8)}}
                        @endif
                    </div>
                    <div class="col-sm-4">
                        @if(isset($pre_withdrawn_eth))
                            ETH : {{number_format($pre_withdrawn_eth, 6)}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    $(document).ready(function () {

        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            "bLengthChange": false,
            "ajax": {
                "url": "{{url('dashboard/daily_profit/json')}}",
                "type": "POST"
            },
            "deferRender": true,
            "columns": [
                /* title will auto-generate th columns */
                {
                    "data": "id", "title": "Sr.#", "orderable": true, "searchable": true,
                },
                {
                    "data": "trade_id", "title": "Trade ID", "orderable": true, "searchable": true
                },
                {
                    "data": "user_id", "title": "User ID", "orderable": true, "searchable": true
                },
                {
                    "data": "currency", "title": "Currency", "orderable": true, "searchable": true
                },
                {
                    "data": "trade_amount", "title": "Amount", "orderable": true, "searchable": true,

                    "render": function (data, type, row) {
                        if (row.currency === 'USD') {
                            return row.trade_amount.toFixed(2);
                        } else {
                            return row.crypto_amount.toFixed(6);
                        }
                    }
                },
                {
                    "data": "today_profit", "title": "Profit Amount", "orderable": true, "searchable": true,

                    "render": function (data, type, row) {
                        if (row.currency === 'USD') {
                            return row.today_profit.toFixed(6);
                        } else {
                            return row.crypto_profit.toFixed(6);
                        }
                    }
                },
                {
                    "data": "created_at",
                    "title": "Date Created",
                    "orderable": true,
                    "searchable": true,
                    // type: 'date-m-d-Y',
                    // targets: 0
                    "render": function (data, type, row) {


                        var day = moment(row.created_at).format('DD');
                        var year = moment(row.created_at).format('YYYY');
                        // if(day > 10 && year > 2020){
                            return moment(row.created_at).format('YYYY-MM-DD');
                        // }else{
                        //     return moment(row.created_at).add(1, 'days').format('YYYY-MM-DD');
                        // }
                        // console.log(day);
                        //
                        // var created_date = new Date(row.created_at);
                        // var curr_hour = created_date.getHours();
                        // var curr_min = created_date.getMinutes();
                        // var curr_sec = created_date.getSeconds();
                        // let currentDate = created_date.getDate();
                        //
                        // if (row.id < 48714445) {
                        //     created_date.addDays(1)
                        // }
                        //
                        // var datestring = created_date.getFullYear() + "-" + ("0" + (created_date.getMonth() + 1)).slice(-2) + "-" + ("0" + (currentDate)).slice(-2);
                        // return datestring + " " + curr_hour + ":" + curr_min + ":" + curr_sec;


                        // return data;
                    }
                },
            ],
            "order": [6, "desc"]
        });

        $(".submit1").click(function () {
            return confirm("Do you really want to proceed ?");
        });
    });


</script>
@include('footer')
