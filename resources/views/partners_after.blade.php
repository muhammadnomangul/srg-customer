<!-- main content start-->
<?php
if (isset($ratesQuery)) {
    $bitcoinRate     =  $ratesQuery->rate_btc;
    $bitcashRate     =  $ratesQuery->rate_bch;
    $ethereumRate     =  $ratesQuery->rate_eth;
    $litecoinRate     =  $ratesQuery->rate_ltc;
    $rippleRate     =  $ratesQuery->rate_xrp;
    $dashRate         =  $ratesQuery->rate_dash;
    $zcashRate         =  $ratesQuery->rate_zec;
} else {
    $bitcoinRate     = 1;
    $bitcashRate     = 1;
    $ethereumRate     = 1;
    $litecoinRate     = 1;
    $rippleRate     = 1;
    $dashRate         = 1;
    $zcashRate         = 1;
}

$count1 = count($firstLine);
$count2 = count($secondLine);
$count3 = count($thirdLine);
$count4 = count($fourthLine);
$count5 = count($fifthLine);

$counter = 1;
$row2 = 1;
$counter2 = 0;
$row3 = 1;
$counter3 = 0;
$row4 = 1;
$counter4 = 0;
$row5 = 1;
$counter5 = 0;

?>
<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>

<?php //echo " 1- ".$users1." 2- ".$users2." 3- ". $users3." 4- ".$users4." 5- ".$users5;
?>

@include('messages')

<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
    <table id="myTable" class="table table-hover">
        <thead>
            <tr>
                <th>First Level Total</th>
                <th>Second Level Total</th>
                <th>Third Level Total</th>
                @if(\Auth::user()->plan >= 3)
                <th>Fourth Level Total</th>
                <th>Fifth Level Total</th>
                @endif
                <th>Final Total</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $account_balance = $firstTotal + $secondTotal + $thirdTotal;
            if (\Auth::user()->plan >= 3) {
                $account_balance = $account_balance + $fourthTotal + $fifthTotal;
            }
            ?>
            <tr>
                <th scope="row">${{number_format($firstTotal, 2)}}</th>
                <td>${{number_format($secondTotal, 2)}}</td>
                <td>${{number_format($thirdTotal, 2)}}</td>
                @if(\Auth::user()->plan >= 3)
                <td>${{number_format($fourthTotal, 2)}}</td>
                <td>${{number_format($fifthTotal, 2)}}</td>
                @endif
                <td><strong>${{number_format($account_balance, 2)}}</strong></td>
            <tr>
        </tbody>
    </table>

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">1st Level </a></li>
            <li><a href="#tabs-2">2nd Level </a></li>
            <li><a href="#tabs-3">3rd Level </a></li>
            @if(\Auth::user()->plan >= 3)
            <li><a href="#tabs-4">4th Level </a></li>
            <li><a href="#tabs-5">5th Level </a></li>
            @endif
        </ul>

        <div id="tabs-1">
            <table id="myTable1" class="table table-hover">
                <thead>
                    <tr>
                        <th>Sr.#</th>
                        <th>User Name</th>
                        <th>User ID</th>
                        <th>Parent ID</th>
                        <th>Total USD</th>
                    </tr>
                </thead>

                <tbody>
                    @if(isset($firstLine))
                    @foreach($firstLine as $lines)
                    <?php $totalBalUsd = $lines->usd + $lines->btc * $bitcoinRate + $lines->eth * $ethereumRate + $lines->bch * $bitcashRate + $lines->ltc * $litecoinRate + $lines->xrp * $rippleRate + $lines->dash * $dashRate + $lines->zec * $zcashRate;
                    ?>
                    @if(isset($lines->u_id))

                    @if($totalBalUsd == 0)
                    <tr style="color:red;">
                        @else
                    <tr>
                        @endif
                        <th scope="row">{{$counter}}</th>
                        <td>{{$lines->name}}</td>
                        <td class="linksAll">
                            <a data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                        </td>
                        <td>{{$lines->parent_id}} </td>
                        <td> ${{number_format($totalBalUsd, 2)}}</td>
                    </tr>
                    @endif
                    <?php $counter++; ?>
                    @endforeach

                    @endif

                </tbody>

            </table>

        </div>

        <div id="tabs-2">

            <table id="myTable2" class="table table-hover">

                <thead>

                    <tr>

                        <th>Sr.#</th>
                        <th>User Name</th>
                        <th>User ID</th>
                        <th>Parent ID</th>
                        <th>Total USD</th>
                    </tr>
                </thead>
                <tbody>
                    <?php // $row2 = 1; $counter2 = 0;
                    ?>
                    @if(isset($secondLine))

                    @for($i=0; $i < $count2; $i++) @foreach($secondLine[$counter2] as $lines) <?php
                                                                                                $totalBalUsd = $lines->usd + $lines->btc * $bitcoinRate + $lines->eth * $ethereumRate + $lines->bch * $bitcashRate + $lines->ltc * $litecoinRate + $lines->xrp * $rippleRate + $lines->dash * $dashRate + $lines->zec * $zcashRate;
                                                                                                ?> @if($totalBalUsd==0) <tr style="color:red;">

                        @else

                        <tr>

                            @endif

                            <th scope="row">{{$row2}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($totalBalUsd, 2)}}</td>
                        </tr>



                        <?php $row2++; ?>

                        @endforeach

                        <?php $counter2++; ?>

                        @endfor

                        @endif

                </tbody>

            </table>

        </div>

        <div id="tabs-3">

            <table id="myTable3" class="table table-hover">

                <thead>

                    <tr>
                        <th>Sr.#</th>
                        <th>User Name</th>
                        <th>User ID</th>
                        <th>Parent ID</th>
                        <th>Total USD</th>
                    </tr>

                </thead>



                <tbody>

                    @if(isset($thirdLine))

                    <?php //$row3 = 1; $counter3 = 0;
                    ?>

                    @for($i=0; $i < $count3; $i++) @foreach($thirdLine[$counter3] as $lines) <?php
                                                                                                $totalBalUsd = $lines->usd + $lines->btc * $bitcoinRate + $lines->eth * $ethereumRate + $lines->bch * $bitcashRate + $lines->ltc * $litecoinRate + $lines->xrp * $rippleRate + $lines->dash * $dashRate + $lines->zec * $zcashRate;
                                                                                                ?> @if($totalBalUsd==0) <tr style="color:red;">

                        @else

                        <tr>

                            @endif

                            <th scope="row">{{$row3}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($totalBalUsd, 2)}}</td>
                        </tr>

                        <?php $row3++; ?>

                        @endforeach

                        <?php $counter3++; ?>

                        @endfor

                        @endif

                </tbody>

            </table>

        </div>
        @if(\Auth::user()->plan >= 3)
        <div id="tabs-4">

            <table id="myTable4" class="table table-hover">

                <thead>

                    <tr>

                        <th>Sr.#</th>

                        <th>User Name</th>

                        <th>User ID</th>

                        <th>Parent ID</th>

                        <th>Total USD</th>

                    </tr>

                </thead>

                <tbody>

                    @if(isset($fourthLine))

                    <?php //$row4 = 1; $counter4 = 0;

                    //$count4 = count($fourthLine);

                    ?>

                    @for($i=0; $i < $count4; $i++) @foreach($fourthLine[$counter4] as $lines) <?php

                                                                                                $totalBalUsd = $lines->usd + $lines->btc * $bitcoinRate + $lines->eth * $ethereumRate + $lines->bch * $bitcashRate + $lines->ltc * $litecoinRate + $lines->xrp * $rippleRate + $lines->dash * $dashRate + $lines->zec * $zcashRate;

                                                                                                ?> @if($totalBalUsd==0) <tr style="color:red;">

                        @else

                        <tr>

                            @endif

                            <th scope="row">{{$row4}}</th>

                            <td>{{$lines->name}}</td>
                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>


                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($totalBalUsd, 2)}}</td>

                        </tr>



                        <?php $row4++; ?>

                        @endforeach

                        <?php $counter4++; ?>

                        @endfor

                        @endif

                </tbody>

            </table>

        </div>

        <div id="tabs-5">

            <table id="myTable5" class="table table-hover">

                <thead>

                    <tr>

                        <th>Sr.#</th>

                        <th>User Name</th>

                        <th>User ID</th>

                        <th>Parent ID</th>

                        <th>Total USD</th>

                    </tr>

                </thead>

                <tbody>

                    @if(isset($fifthLine))

                    <?php //$row5 = 1; $counter5 = 0;
                    ?>

                    @for($i=0; $i < $count5; $i++) @foreach($fifthLine[$counter5] as $lines) <?php
                                                                                                $totalBalUsd = $lines->usd + $lines->btc * $bitcoinRate + $lines->eth * $ethereumRate + $lines->bch * $bitcashRate + $lines->ltc * $litecoinRate + $lines->xrp * $rippleRate + $lines->dash * $dashRate + $lines->zec * $zcashRate;
                                                                                                ?> @if($totalBalUsd==0) <tr style="color:red;">

                        @else

                        <tr>

                            @endif

                            <th scope="row">{{$row5}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($totalBalUsd, 2)}}</td>

                        </tr>

                        <?php $row5++; ?>

                        @endforeach

                        <?php $counter5++; ?>

                        @endfor

                        @endif

                </tbody>

            </table>

        </div>
        @endif
    </div>

</div>





<!-- Partners Details Modal -->
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="pcontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /Partners Details Modal -->


<script>
    function viewDetailsFunc(id) {
        var type = "partners";
        jQuery.ajax({
            type: "POST",
            url: '{!!URL::to("/")!!}/dashboard/viewUserDetails',
            data: {
                id: id,
                type: type,
                "_	token": "{{ csrf_token() }}",
            },
            success: function (response) {
                response = JSON.parse(response);
                $("#pcontent").html(response);
            },
            error: function (response) {
                // alert('Be patient, System is under maintenance');
                //$("#CallSubModal").modal('show');
            }
        });
    }


    // $(document).ready( function () {



    $('#myTable1').DataTable({

        "pagingType": "full_numbers"

    });

    $('#myTable2').DataTable({

        "pagingType": "full_numbers"

    });

    $('#myTable3').DataTable({

        "pagingType": "full_numbers"

    });

    $('#myTable4').DataTable({

        "pagingType": "full_numbers"

    });

    $('#myTable5').DataTable({

        "pagingType": "full_numbers"

    });

    //});



    //  $( function() {

    $("#tabs").tabs();

    //} );
</script>