<a href="{{ route('clear_cache_awarded_partners') }}">Click for latest results</a>


<ul style="display: flex;
justify-content: space-between;
list-style-type: none;
margin: 12px 0 12px 0">
    @for($x=1; $x<21; $x++)
        <li style="background-color: white;padding: 10px"><a
                    href="{{ url("dashboard/awarded_partners/{$x}") }}">Level-{{$x}}
                @if($level == $x)
                    ({{$user_level_count}})
                    <br><small>${{number_format($total, 2)}}</small>
                @endif
            </a>
        </li>
    @endfor
</ul>
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content ucontent">
        </div>
    </div>
</div>
