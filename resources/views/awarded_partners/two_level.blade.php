<?php $counter = 0; ?>
@include('header')

<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>

<div id="page-wrapper">
    <div class="main-page">
        <h3 class="title1">Distributor Referrals Partners</h3>
        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
            <h3 style="margin-top:5px; text-align:center;">Distributor Referrals Details </h3><br>
        </div>
        <div id="tabs">
            @include('awarded_partners.tabs')
        </div>

        <div id="tabs-2" class="widget-shadow">
            <table id="myTable2" class="table table-hover table-borderd">

                <thead>
                <tr>
                    <th>Sr.#</th>
                    <th>User Name</th>
                    <th>User ID</th>
                    <th>Parent ID</th>
                    <th>Total USD</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>

                @if(isset($linee))

                    @for($i=0; $i < count($linee); $i++)

                        @foreach($linee[$counter] as $key => $lines)

                            <tr @if($lines->total == 0) style="color:red;" @endif >

                                <th scope="row">{{$key}}</th>

                                <td>{{$lines->name}}</td>

                                <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                        onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                                </td>

                                <td>{{$lines->parent_id}} </td>

                                <td> ${{number_format($lines->total, 2)}}</td>
                                <td>{{$lines->created_at}}</td>
                            </tr>

                        @endforeach

                    @endfor

                @endif

                </tbody>

            </table>
        </div>

    </div>
</div>
@include('footer')
<script>

    // function viewUserDetailsFunc(id) {
    function viewDetailsFunc(id) {
        var partner = 1;
        postData('{{{\Illuminate\Support\Facades\URL::to("dashboard/view-partner-details")}}}', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (html) {
                // console.log(html);
                $(".ucontent").html(html);
            })
            .catch(function (error) {
                $('#PartnersDetailsModal').modal('hide');
                alert('Invalid user details');
            });
    }


    $(document).ready(function () {

        $('.table').DataTable({
            "pagingType": "full_numbers",
            "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
            buttons: ['excel'],
            "lengthMenu": [[10, 100, 500, 1000, -1], [10, 100, 500, 1000, "All"]]
        });


    });

    $(function () {
        // $("#tabs").tabs();
    });

</script>
