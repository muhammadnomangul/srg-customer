@include('newHome.header')
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Hero Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="hero-block bg-primary" style="background-image: url('public/home/images/shape/shape-dot5.png')">
            <div class="element-effect">
                <img src="{{asset('public/home/images/vactor/line3.png')}}" alt="Line">
                <img src="{{asset('public/home/images/vactor/line3.png')}}" alt="Line">
                <img src="{{asset('public/home/images/vactor/line3.png')}}" alt="Line">
                <img src="{{asset('public/home/images/vactor/line3.png')}}" alt="Line">
                <img src="{{asset('public/home/images/vactor/line3.png')}}" alt="Line">
            </div>
            <div class="hero-shape-top" style="background-image: url('public/home/images/shape/hero-shape-top.png')"></div>
            <div class="hero-shape-bottom" style="background-image: url('public/home/images/shape/header-shape.png')"></div>
            <div class="hero-block-inner">
                <div class="container">
                    <div class="row hero-content-info-area justify-content-end">
                        <div class="col-lg-9">
                            <div class="hero-mockup-thumb-area">
                                <div class="hero-info-list text-white">
                                    <div class="line-shape">
                                        <img src="{{asset('public/home/images/shape/line-shape.png')}}" alt="Icon">
                                    </div><!-- /.line-shape -->
                                    <div class="hero-info-list-inner">
                                        <div class="hero-info">
                                            <div class="text">Get Profit</div>
                                            <div class="icon">
                                                <img src="{{asset('public/home/images/icon/Icon-4.png')}}" alt="Icon">
                                            </div>
                                        </div><!-- /.hero-info -->
                                        <div class="hero-info">
                                            <div class="icon">
                                                <img src="{{asset('public/home/images/icon/Icon-2.png')}}" alt="Icon">
                                            </div>
                                            <div class="text">Investment</div>
                                        </div><!-- /.hero-info -->
                                        <div class="hero-info">
                                            <div class="icon">
                                                <img src="{{asset('public/home/images/icon/Icon.png')}}" alt="Icon">
                                            </div>
                                            <div class="text">Choose Plan</div>
                                        </div><!-- /.hero-info -->
                                        <div class="hero-info">
                                            <div class="icon">
                                                <img src="{{asset('public/home/images/icon/Icon-3.png')}}" alt="Icon">
                                            </div>
                                            <div class="text">Create account</div>
                                        </div><!-- /.hero-info -->
                                    </div>
                                </div><!-- /.hero-info-list -->
                                <div class="hero-thumb-area">
                                    <div class="hero-thumb-inc">
                                        <img src="{{asset('public/home/images/others/profit-inc.png')}}" alt="Inc">
                                    </div><!-- /.hero-thumb-inc -->
                                    <div class="hero-thumb">
                                        <img src="{{asset('public/home/images/others/hero-thumb.png')}}" alt="Thumbnail">
                                    </div><!-- /.hero-thumb -->
                                </div>
                            </div><!-- /.hero-mockup-thumb-area -->
                        </div>
                    </div><!-- /.hero-content-info-area -->

                    <div class="row hero-content-area align-content-center">
                        <div class="col-lg-6">
                            <div class="hero-content">
                                <div class="hero-content-shape">
                                    <img src="{{asset('public/home/images/shape/shape-hero.png')}}" alt="Shape">
                                </div>
                                <div class="hero-subtitle">Double Your Investments</div>
                                <h2 class="hero-title">B4U Global – A Cryptocurrency Platform</h2><!-- /.hero-title -->
                                <div class="form-group-btn">
                                    <a class="btn btn-default btn-primary" href="{{ route('register') }}">Open An Account</a>
                                    <!-- <a href="https://www.youtube.com/embed/GT6-H4BRyqQ" data-rel="lightcase" class="video-popup btn-video">
                                        <img src="{{asset('public/home/images/icon/video.png')}}" alt="icon" />
                                        <span class="fa fa-play"></span>
                                    </a> -->
                                </div><!-- /.form-group-btn -->
                            </div><!-- /.hero-content -->
                        </div><!-- /.col-lg-6 -->
                    </div>
                </div><!--  /.container -->
            </div>
        </div><!-- /.hero-block -->
     
       <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Features Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        
         @include('newHome.indexSections.featuresSection')

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Discount Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="discount-block ptb-120 bg-image bg-overlay-primary" id="about2">
            <div class="container">
                
                    <div class="section-title ">
                                <div class="subtitle">Good to know</div>
                                <h2 class="title-main" data-animate="hg-fadeInUp">What we do?</h2><!-- /.title-main -->
                    </div><!-- /.section-title -->
                <div class="row justify-content-center">
                   <div class="col-md-6 about-left discount-text text-white">

                    <p> B4U a Secure Cryptocurrency Trading Company its Infrastructure Facilitates Investment in the Cryptocurrencies industry with Daily Profit Growth on Investment.<br><br> B4U is group of people who have large experience of trading, investment tricks and market scope.

                    <span>By using their skills and expertise we trade your investment in the market and earn profit. our major investment domains are as following:  </span></p>

                    <ul>

                        <li><span class="glyphicon glyphicon-share-alt"></span> Investment in property deals  </li>

                        <li><span class="glyphicon glyphicon-share-alt"></span> Investment in Crypto Exchange </li>

                        <li><span class="glyphicon glyphicon-share-alt"></span> Investment in Information Technologies</li>

                        <li><span class="glyphicon glyphicon-share-alt"></span> Investment in Transport Services </li>

                        <li><span class="glyphicon glyphicon-share-alt"></span> Investment in Trading </li>

                    </ul>

                </div>

                <div class="col-md-6 about-right">


                    <div class="btcwdgt-chart" bw-cur="btc"></div>



                </div>

                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end discount block ~~-->

       <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Pricing Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.newPlansSection')

         <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Profit Table
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~--> 
         
                
         @include('newHome.indexSections.calenderSection')
              

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Profit Calculation Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.profitCalculationSection') 

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Our Mission Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.ourMissionSection')

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Work Process Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.investmentProcessSection')
        
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Deposit Withdraw Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
     
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Fan Fact Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
       
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Faqs Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.faqsSection')

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Our Vision Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         @include('newHome.indexSections.ourVisionSection')

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Testimonial Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
          
      

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Work brand Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="work-brand-block style-one pd-b-120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="brand-list" data-animate="hg-fadeInLeft">
                            <div class="brands-link">
                                <img src="{{asset('public/home/images/brand/bitcon.png')}}" alt="logo">
                            </div><!-- /.brands-link -->
                            <div class="brands-link">
                                <img src="{{asset('public/home/images/brand/wallet.png')}}" alt="logo">
                            </div><!-- /.brands-link -->
                        </div><!-- /.brand-list -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="section-title">
                            <div class="subtitle"></div>
                            <div class="subtitle" data-animate="hg-fadeInUp">Our trust partners</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Become A Partner</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                It’s the best time to join hand with cryptocurrency trading experts. Become our partner and let’s build a community of expert sellers and traders.
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                        <div class="work-brand-content" data-animate="hg-fadeInUp">
                            <a class="btn btn-default btn-primary" href="#">Become A Partner</a>
                        </div><!-- /.work-brand-content -->
                    </div><!-- /.col-lg-6 -->
                </div>
            </div>
        </div><!--~./ end work brand block ~--> 

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Latest News Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

         
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Site Footer
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->  
  
@include('newHome.footer')