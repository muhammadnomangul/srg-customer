
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Site Footer
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <footer class="site-footer bg-primary pd-t-120" style="background-image: url('public/home/images/shape/cloud-star.png')">
            <div class="footer-cloud-bg" style="background-image: url('public/home/images/shape/cloud.png')"></div>
            <div class="footer-bottom-shape" style="background-image: url('public/home/images/shape/footer-bottom-shape.png')"></div>

            <div class="man-coin">
                <img src="{{asset('public/home/images/vactor/man.png')}}" alt="Man Coin">
            </div><!-- /.man-coin -->

            <div class="star-group">
                <img src="{{asset('public/home/images/vactor/star.png')}}" alt="Star">
                <img src="{{asset('public/home/images/vactor/star.png')}}" alt="Star">
                <img src="{{asset('public/home/images/vactor/star.png')}}" alt="Star">
                <img src="{{asset('public/home/images/vactor/star.png')}}" alt="Star">
                <img src="{{asset('public/home/images/vactor/star.png')}}" alt="Star">
            </div><!-- /.star-group -->
            <div class="tree-group">
                <div class="tree-item-left" data-animate="hg-fadeInLeft">
                    <img src="{{asset('public/home/images/vactor/tree1.png')}}" alt="Tree">
                </div>
                <div class="tree-item-right" data-animate="hg-fadeInRight">
                    <img src="{{asset('public/home/images/vactor/tree2.png')}}" alt="Tree">
                </div>
            </div><!-- /.star-group -->

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                Start Footer Widget Area
            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="footer-widget-area">
                <div class="container">
                  <div class="row justify-content-center">
                    <div  class="col-lg-10"  >
                        <div class="section-title text-center">
                          
                                <div class="justify-content-center">
                                    <div class="about-loga">
                                        <img src="{{ url('images/b4u-investment1.png') }}" alt="Site Logo" />
                                    </div>
                                    <br>
                                    <h2 >We are social</h2>
                                    
                                    <hr style="width: 150px;">   
                                    <div class="about-loga" style="padding-right: 20px;" >
                                        <ul id="socialmedia">

                                            <li><a href="https://web.facebook.com/bitcoin4utrading/"><i class="fab fa-facebook-f"></i></a></li>

                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>

                                            <li><a href="#"><i class="fa fa-rss"></i></a></li>

                                            <li><a href="#"><i class="fab fa-vk"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                          
                        </div><!--~./ end about us widget ~-->
                        
                    </div>
                </div>
            </div><!--~./ end footer widgets area ~-->

            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                Start Footer Bottom Area
            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="copyright-text text-center">
                               <p>© {{date('Y')}} {{$settings->site_name}}. All rights reserved</p>
                            </div><!--~./ end copyright text ~-->
                        </div><!--~./ col-12 ~-->
                    </div>
                </div>
            </div><!--~./ end footer bottom area ~-->
        </footer><!--~./ end site footer ~-->
    </div><!--~~./ end site content ~~-->


    <div class="Whatsapp" style="position: fixed; bottom: 0px; right: 0px; margin-right: 400px; z-index: 2;"  >
                <a href="https://wa.me/+923093331478"><img width="150" height="50" src="{{asset('images/whatsapp-icon.png')}}"></a>
    </div>
        <!--  LiveChat Widget-->
        @include('liveChatScript')
    <!-- All The JS Files
    ================================================== --> 
    <script>

              (function(b,i,t,C,O,I,N) {

                window.addEventListener('load',function() {

                  if(b.getElementById(C))return;

                  I=b.createElement(i),N=b.getElementsByTagName(i)[0];

                  I.src=t;I.id=C;N.parentNode.insertBefore(I, N);

                },false)

              })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');

    </script>
  <!--  <script src="{{asset('public/home/js/jquery.js')}}"></script> -->
    <script src="{{asset('public/home/js/popper.min.js')}}"></script>
    <script src="{{asset('public/home/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/home/js/plugins.js')}}"></script>
    <script src="{{asset('public/home/js/meanmenu.min.js')}}"></script>
    <script src="{{asset('public/home/js/imagesloaded.pkgd.min.js')}}"></script> 
    <script src="{{asset('public/home/js/jquery.nice-select.min.js')}}"></script> 
    <script src="{{asset('public/home/js/theia-sticky-sidebar.min.js')}}"></script>
    <script src="{{asset('public/home/js/ResizeSensor.min.js')}}"></script>
    <script src="{{asset('public/home/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('public/home/js/lightcase.js')}}"></script>
    <script src="{{asset('public/home/js/simple-scrollbar.min.js')}}"></script>
    <script src="{{asset('public/home/js/scrolla.jquery.min.js')}}"></script>
    <script src="{{asset('public/home/js/odometer.min.js')}}"></script>
    <script src="{{asset('public/home/js/isInViewport.jquery.js')}}"></script>
    <script src="{{asset('public/home/js/circle-progress.min.js')}}"></script>
    <script src="{{asset('public/home/js/chartist.min.js')}}"></script>
    <script src="{{asset('public/home/js/chartistPoint.js')}}"></script>
    <script src="{{asset('public/home/js/main.js')}}"></script><!-- main-js -->
   
</body>
</html>