
@include('newHome.header')
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Page Title Area
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="page-title-area bg-primary" style="background-image: url('public/assets/images/shape/shape-dot1.png')">
            <div class="shape-group">
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
            </div><!-- /.shape-group -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="page-header-content text-center">
                            <div class="page-header-caption">
                                <h2 class="page-title">About Us</h2>
                            </div><!--~~./ page-header-caption ~~-->
                            <div class="breadcrumb-area">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>  
                                    <li class="breadcrumb-item active">About</li>
                                </ol>
                            </div><!--~~./ breadcrumb-area ~~-->
                        </div><!--~~./ page-header-content ~~-->
                    </div>
                </div>
            </div><!--~~./ end container ~~-->
        </div><!--~~./ end page title area ~~--> 

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start About Us Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="about-us-block ptb-120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="about-mock-up-thumb">
                            <div class="mock-up-thumb" data-animate="hg-fadeInLeft">
                                <img src="../../../public/assets/images/about/about1.jpg" alt="Thumbnail">
                            </div><!-- /.mock-up-thumb -->
                            <div class="mock-up-thumb" data-animate="hg-fadeInLeft">
                                <img src="../../../public/assets/images/about/about2.jpg" alt="Thumbnail">
                            </div><!-- /.mock-up-thumb -->
                        </div><!-- /.about-mock-up-thumb -->
                        <div class="experience-info-area bg-image bg-overlay-primary" style="background-image: url('public/assets/images/bg/experiance-bg.jpg')" data-animate="hg-fadeInUp">
                            <div class="experience-info">
                                24+ Years <span>experience</span>
                            </div><!-- /.experience-info -->
                        </div><!-- /.experience-info-area -->
                    </div><!-- /.col-lg-6 -->

                    <div class="col-lg-6">
                        <div class="about-us-content md-mrt-60">
                            <div class="section-title">
                                <div class="subtitle" data-animate="hg-fadeInUp">About Our Company</div>
                                <h2 class="title-main" data-animate="hg-fadeInUp">About Company</h2><!-- /.title-main -->
                                <div class="title-text" data-animate="hg-fadeInUp">
                                    Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                                </div><!-- /.title-text -->
                            </div><!-- /.section-title -->
                            <div class="about-info-list">
                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="about-icon icon-small icon-red">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-scroll"></span>
                                            </div>
                                        </div><!-- /.about-icon-->
                                        <div class="info-title">
                                            <h3 class="heading">Our History</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->

                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="about-icon icon-small icon-green">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-target-1"></span>
                                            </div>
                                        </div><!-- /.about-icon-->
                                        <div class="info-title">
                                            <h3 class="heading">Our Mission</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->

                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="about-icon icon-small">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-telephone"></span>
                                            </div>
                                        </div><!-- /.about-icon-->
                                        <div class="info-title">
                                            <h3 class="heading">Best Support</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->
                            </div>
                        </div><!-- /.about-us-content -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end about us block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Our Vision Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="our-vision-block bg-primary pd-t-120">
            <div class="shape-group">
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
            </div>

            <div class="section-vertical-title-area">
                <h2 class="vertical-title"><span>our</span> vision</h2>
            </div><!-- /.section-vertical-title-area -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="our-vision-content-area">
                            <div class="our-vision-content bg-image" style="background-image: url('public/assets/images/shape/shape-dot2.png')">
                                <div class="element-effect">
                                    <img class="line" src="../../../public/assets/images/vactor/line2.png" alt="Icon">
                                    <img class="triangle" src="../../../public/assets/images/vactor/triangle2.png" alt="Icon">
                                    <img class="rectangle" src="../../../public/assets/images/vactor/rectangle.png" alt="Icon">
                                    <img class="circle" src="../../../public/assets/images/vactor/circle.png" alt="Icon">
                                </div><!-- /.element-effect -->
                                <div class="vision-info-list">
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-green">
                                            <span class="flaticon-employee-1"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">Customer Relationship</h3>
                                            <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-red">
                                            <span class="flaticon-bar-chart"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">Our Company Growth</h3>
                                            <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-turquoise">
                                            <span class="flaticon-employee"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">100M Members</h3>
                                            <p>Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo felis</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                </div>
                            </div><!-- /.our-vision-content -->
                            <div class="vision-thumb-area" data-animate="hg-fadeInLeft">
                                <img src="../../../public/assets/images/others/vision-thumb.png" alt="Thumb">
                            </div>
                        </div><!-- /.our-vision-content-area -->
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!--~~./ end our vision block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Fan Fact Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="fanfact-block ptb-120">
            <div class="container ml-b-50">
                <div class="row fanfact-promo-numbers">
                    <div class="col-lg-3 col-md-6" data-animate="hg-fadeInUp">
                        <div class="promo-number">
                            <div class="odometer-wrap"><div class="odometer" data-odometer-final="58">0</div><sub>K</sub></div><!-- /.odometer-wrap -->
                            <h4 class="promo-title">Active Members</h4><!--  /.promo-title -->
                        </div><!-- /.promo-number -->
                    </div><!-- /.col-lg-3 -->  
                    <div class="col-lg-3 col-md-6" data-animate="hg-fadeInUp">
                        <div class="promo-number number-turquoise">
                            <div class="odometer-wrap"><div class="odometer" data-odometer-final="259">0</div></div><!-- /.odometer-wrap -->
                            <h4 class="promo-title">Running Days</h4><!--  /.promo-title -->
                        </div><!-- /.promo-number -->
                    </div><!-- /.col-lg-3 -->  
                    <div class="col-lg-3 col-md-6" data-animate="hg-fadeInUp">
                        <div class="promo-number number-green">
                            <div class="odometer-wrap"><div class="odometer" data-odometer-final="89">0</div><sub>M</sub></div><!-- /.odometer-wrap -->
                            <h4 class="promo-title">Total Deposit</h4><!--  /.promo-title -->
                        </div><!-- /.promo-number -->
                    </div><!-- /.col-lg-3 -->  
                    <div class="col-lg-3 col-md-6" data-animate="hg-fadeInUp">
                        <div class="promo-number number-red">
                            <div class="odometer-wrap"><div class="odometer" data-odometer-final="62">0</div><sub>K</sub></div><!-- /.odometer-wrap -->
                            <h4 class="promo-title">Total Members</h4><!--  /.promo-title -->
                        </div><!-- /.promo-number -->
                    </div><!-- /.col-lg-3 -->   
                </div><!-- /.fanfact-promo-numbers -->
            </div><!-- /.container -->
        </div><!--~~./ end fanfact block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Investor Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="investor-block pd-b-120">
            <div class="container ml-b-5">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="section-title">
                            <div class="subtitle" data-animate="hg-fadeInUp">36k Investors in here</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Our Top Investor</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div>
                    <div class="col-lg-6">
                        <div class="btn-links-area text-right">
                            <button class="btn-links btn-prev">
                                <span class="fa fa-angle-left"></span>
                            </button>
                            <button class="btn-links btn-next">
                                <span class="fa fa-angle-right"></span>
                            </button>
                        </div><!-- /.btn-links-area -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="investor-carousel owl-carousel" data-animate="hg-fadeInUp">
                            <div class="investor-item">
                                <figure class="investor-thumb">
                                    <img src="../../../public/assets/images/investor/1.jpg" alt="Sport Thumb">
                                    <div class="invest-amount">$20000.00 USD</div>
                                </figure><!-- /.investor-thumb --> 
                                <div class="investor-info">
                                    <h3 class="investor-name"><a href="single-investor.html">Nir Proman</a></h3>
                                    <div class="designation">Business man</div>
                                </div><!-- /.investor-info -->   
                            </div><!-- /.investor-item -->
                            <div class="investor-item">
                                <figure class="investor-thumb">
                                    <img src="../../../public/assets/images/investor/2.jpg" alt="Sport Thumb">
                                    <div class="invest-amount">$15000.00 USD</div>
                                </figure><!-- /.investor-thumb --> 
                                <div class="investor-info">
                                    <h3 class="investor-name"><a href="single-investor.html">Mark Henri</a></h3>
                                    <div class="designation">Business man</div>
                                </div><!-- /.investor-info -->   
                            </div><!-- /.investor-item -->
                            <div class="investor-item">
                                <figure class="investor-thumb">
                                    <img src="../../../public/assets/images/investor/3.jpg" alt="Sport Thumb">
                                    <div class="invest-amount">$10000.00 USD</div>
                                </figure><!-- /.investor-thumb --> 
                                <div class="investor-info">
                                    <h3 class="investor-name"><a href="single-investor.html">Jonathon Doe</a></h3>
                                    <div class="designation">Business man</div>
                                </div><!-- /.investor-info -->   
                            </div><!-- /.investor-item -->
                        </div><!-- /.investor-carousel -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end investor block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Discount Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="discount-block ptb-120 bg-image bg-overlay-primary" style="background-image: url('public/assets/images/bg/discount-bg.jpg')">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="discount-content text-white text-center">
                            <h2 class="discount-title" data-animate="hg-fadeInUp">45% Referral Commission</h2><!-- /.discount-title -->
                            <div class="discount-text" data-animate="hg-fadeInUp">
                                Regular members who have an active deposit can participate in our affilire program which givesfor their active referrals
                            </div><!-- /.title-text -->
                            <div class="discount-btn-group" data-animate="hg-fadeInUp">
                                <a href="contact.html" class="btn btn-default btn-white">Get Start Now</a>
                            </div><!-- /.action-btn-group -->
                        </div><!-- /.discount-content-->
                    </div><!-- /.col-lg-8 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end discount block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Pricing Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="pricing-block ptb-120">
            <div class="container ml-b-30">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <div class="subtitle" data-animate="hg-fadeInUp">Not hidden charge</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Choose you Investment plan</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

                <div class="row pricing-table-list">
                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box style-two pricing-green" data-animate="hg-fadeInUp">
                            <div class="pricing-icon">
                                <span class="flaticon-bars"></span>
                            </div><!-- /.pricing-icon-->
                            <div class="package-price">
                                <div class="pricing">1.75%</div>
                                Daily For Lifetime
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->

                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box style-two pricing-red" data-animate="hg-fadeInUp">
                            <div class="pricing-icon">
                                <span class="flaticon-piggy-bank"></span>
                            </div><!-- /.pricing-icon-->
                            
                            <div class="package-price">
                                <div class="pricing">5%</div>
                                Daily For 40 Days
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->

                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box style-two" data-animate="hg-fadeInUp">
                            <div class="pricing-icon">
                                <span class="flaticon-analytics"></span>
                            </div><!-- /.pricing-icon-->
                            
                            <div class="package-price">
                                <div class="pricing">10%</div>
                                Daily For 90 Days
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->
                </div><!--~./ row ~-->
            </div><!-- /.container -->
        </div><!--~~./ end pricing block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Testimonial Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="testimonial-block pd-t-120 bg-gradient">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <div class="subtitle" data-animate="hg-fadeInUp">Client Feedback</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">What’s Say Our Client</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="testimonail-carousel-main" data-animate="hg-fadeInUp">
                            <div id="testimonail-carousel" class="owl-carousel">
                                <div class="testimonial-item">
                                    <div class="client-area">
                                        <div class="thumbnails">
                                            <img src="../../../public/assets/images/testimonials/one/1.png" alt="img">
                                        </div><!-- /.thumbnails -->
                                        <div class="info">
                                            <h4 class="client-name">Bilsans Max</h4><!--  /.client-name -->
                                            <p class="client-designation">Business man</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.client-area -->
                                    <div class="details">
                                        <p>Pretium, porta donec, aliqumtes justo ipsum ulibepede urna maecequet. oisl purus suscipit Neque porro.</p>
                                        <div class="total-reviews">
                                            <ul>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.details -->
                                </div><!-- /.testimonial-item --> 
                                <div class="testimonial-item">
                                    <div class="client-area">
                                        <div class="thumbnails">
                                            <img src="../../../public/assets/images/testimonials/one/2.png" alt="img">
                                        </div><!-- /.thumbnails -->
                                        <div class="info">
                                            <h4 class="client-name">Jordan Weyker</h4><!--  /.client-name -->
                                            <p class="client-designation">Business man</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.client-area -->
                                    <div class="details">
                                        <p>Pretium, porta donec, aliqumtes justo ipsum ulibepede urna maecequet. oisl purus suscipit Neque porro.</p>
                                        <div class="total-reviews">
                                            <ul>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.details -->
                                </div><!-- /.testimonial-item --> 
                                <div class="testimonial-item">
                                    <div class="client-area">
                                        <div class="thumbnails">
                                            <img src="../../../public/assets/images/testimonials/one/3.png" alt="img">
                                        </div><!-- /.thumbnails -->
                                        <div class="info">
                                            <h4 class="client-name">Maktrek Moe</h4><!--  /.client-name -->
                                            <p class="client-designation">Business man</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.client-area -->
                                    <div class="details">
                                        <p>Pretium, porta donec, aliqumtes justo ipsum ulibepede urna maecequet. oisl purus suscipit Neque porro.</p>
                                        <div class="total-reviews">
                                            <ul>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.details -->
                                </div><!-- /.testimonial-item --> 
                                <div class="testimonial-item">
                                    <div class="client-area">
                                        <div class="thumbnails">
                                            <img src="../../../public/assets/images/testimonials/one/4.png" alt="img">
                                        </div><!-- /.thumbnails -->
                                        <div class="info">
                                            <h4 class="client-name">Jordan Weyker</h4><!--  /.client-name -->
                                            <p class="client-designation">Business man</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.client-area -->
                                    <div class="details">
                                        <p>Pretium, porta donec, aliqumtes justo ipsum ulibepede urna maecequet. oisl purus suscipit Neque porro.</p>
                                        <div class="total-reviews">
                                            <ul>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li class="color"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.details -->
                                </div><!-- /.testimonial-item --> 
                            </div><!-- /#testimonail-carousel -->
                        </div><!-- /.testimonail-carousel-main -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end testimonial block ~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Work brand Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="work-brand-block ptb-120">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!--~~ Start Brands Carousel ~~-->
                        <div class="brands-carousel-main" data-animate="hg-fadeInUp">
                            <div class="brands-carousel owl-carousel">
                                <div class="brands-link">
                                    <img src="../../../public/assets/images/brand/1.png" alt="logo">
                                </div>
                                <div class="brands-link">
                                    <img src="../../../public/assets/images/brand/2.png" alt="logo">
                                </div>
                                <div class="brands-link">
                                    <img src="../../../public/assets/images/brand/3.png" alt="logo">
                                </div>
                                <div class="brands-link">
                                    <img src="../../../public/assets/images/brand/4.png" alt="logo">
                                </div>
                            </div>
                        </div><!--~./ end brands carousel ~-->
                    </div>
                </div>
            </div>
        </div><!--~./ end popular brands block ~--> 
    
 @include('newHome.footer')