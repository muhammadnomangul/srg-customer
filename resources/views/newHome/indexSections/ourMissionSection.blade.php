  <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Our Mission Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
 <div class="our-mission-block pd-t-120">
            <div class="container-fluid">
                <div class="row justify-content-end no-gutters">
                    <div class="col-lg-6">
                        <div class="our-mission-left-area bg-image" style="background-image: url('public/home/images/shape/shape-dot3.png')">
                            <div class="mock-up-thumb video-abs-center md-mrt-60" data-animate="hg-fadeInLeft">
                                <img src="{{asset('public/home/images/others/video-thumb.jpg')}}" alt="Thumbnail" />
                                <div class="video-btn-area">
                                    <a href="#" class="video-popup btn-video">
                                        <img src="{{asset('public/home/images/icon/video.png')}}" alt="icon" />
                                        <span class="fa fa-play"></span>
                                    </a>
                                </div><!-- /.video-btn-area -->
                            </div><!-- /.mock-up-block -->
                            <div class="section-vertical-title-area" data-animate="hg-fadeInUp">
                                <h2 class="vertical-title"><span>our</span> mission</h2>
                            </div><!-- /.section-vertical-title-area -->
                        </div>
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="features-info-list">
                            <div class="single-info" data-animate="hg-fadeInUp">
                                <div class="info-header">
                                    <div class="icon-area icon-small icon-green">
                                        <div class="shape-icon"></div>
                                        <div class="icon">
                                            <span class="flaticon-shield-1"></span>
                                        </div>
                                    </div><!-- /.icon-area-->
                                    <div class="info-title">
                                        <h3 class="heading">Excellent Services</h3>
                                    </div><!-- /.info-icon -->
                                </div><!-- /.info-title -->
                                <div class="info">
                                    <p>We always want to stay one step ahead in market. This is the only way we can double our clients’ investment in shortest span of time.</p>
                                </div><!-- /.info -->
                            </div><!-- /.single-info -->

                            <div class="single-info" data-animate="hg-fadeInUp">
                                <div class="info-header">
                                    <div class="icon-area icon-small icon-red">
                                        <div class="shape-icon"></div>
                                        <div class="icon">
                                            <span class="flaticon-profits"></span>
                                        </div>
                                    </div><!-- /.icon-area-->
                                    <div class="info-title">
                                        <h3 class="heading">Build Professional Relationship</h3>
                                    </div><!-- /.info-icon -->
                                </div><!-- /.info-title -->
                                <div class="info">
                                    <p>We want to build a professional relationship with our clients. We are friendly but our nature of work demands professionalism and serious observation on client investment.</p>
                                </div><!-- /.info -->
                            </div><!-- /.single-info -->

                            <div class="single-info" data-animate="hg-fadeInUp">
                                <div class="info-header">
                                    <div class="icon-area icon-small">
                                        <div class="shape-icon"></div>
                                        <div class="icon">
                                            <span class="flaticon-employee"></span>
                                        </div>
                                    </div><!-- /.icon-area-->
                                    <div class="info-title">
                                        <h3 class="heading">Transparency</h3>
                                    </div><!-- /.info-icon -->
                                </div><!-- /.info-title -->
                                <div class="info">
                                    <p>Our cryptocurrency experts make sure that we are earning and delivering profits direct into clients account. We offer 100% transparent profit delivery system.</p>
                                </div><!-- /.info -->
                            </div><!-- /.single-info -->
                        </div>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end our mission block ~~-->
