  <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Faqs Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
  <div class="faqs-block style-one pd-b-120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="faq-thumb" data-animate="hg-fadeInLeft">
                            <img src="{{asset('public/home/images/others/faq1.png')}}" alt="Thumbnail">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="section-title">
                            <div class="subtitle" data-animate="hg-fadeInUp">frequently asked questions</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Ask Your Question</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                What people are asking from B4U Global?
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                        <div class="faq-wrapper" data-animate="hg-fadeInUp">
                            <div class="faq-item open active">
                                <div class="faq-title">
                                    <h4 class="title">What is the minimum amount to open an account?</h4>
                                </div>
                                <div class="faq-content">
                                    <p>The minimum amount varies with the plan. Please scroll up to see our plan details.</p>
                                </div>
                            </div><!-- /.faq-item -->
                            <div class="faq-item">
                                <div class="faq-title">
                                    <h4 class="title">Is it possible to have multiple accounts?</h4>
                                </div>
                                <div class="faq-content">
                                    <p>It is not possible to open more than one account. We only offer one account per person.</p>
                                </div>
                            </div><!-- /.faq-item -->
                            <div class="faq-item">
                                <div class="faq-title">
                                    <h4 class="title">How to get refund if I change my mind?</h4>
                                </div>
                                <div class="faq-content">
                                    <p>You can apply for the withdrawal at any time if there is the sufficient fund available in your account. However, funds can be held for some duration as per the investment plan.</p>
                                </div>
                            </div><!-- /.faq-item -->
                           <!--  <div class="faq-item">
                                <div class="faq-title">
                                    <h4 class="title">Adipiscing, feugiat ornare pretium luctus ?</h4>
                                </div>
                                <div class="faq-content">
                                    <p>Fusce nulla mus quam proin, eget vestibulum elementum potenti mus ipsum. Phasellus nulla inceptos. Et vivamus magna ac, amet quisque tortor.</p>
                                </div>
                            </div> --><!-- /.faq-item -->
                        </div><!-- /.faq-wrapper -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end faqs block ~~-->
