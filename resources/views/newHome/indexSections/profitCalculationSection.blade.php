 <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Profit Calculation Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="profit-calculation-block pd-b-120">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="features-content md-mrt-60">
                            <div class="section-title">
                                <div class="subtitle"></div>
                                <div class="subtitle">Know Your Profit</div>
                                <h2 class="title-main" data-animate="hg-fadeInUp">Profit calculation</h2><!-- /.title-main -->
                                <div class="title-text" data-animate="hg-fadeInUp">
                                    Want to know average profit ? Here is the profit calculator, go ahead select the amount and know the profit you can make daily.
                                </div><!-- /.title-text -->
                            </div><!-- /.section-title -->
                      <iframe src="{{URL::to('/')}}/pcalculator" width="100%" height="380"></iframe>
                        </div><!-- /.features-content -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="profit-thumb-group-area" data-animate="hg-fadeInRight">
                            <div class="animation-circle-inverse">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div><!-- /.animation-circle-inverse -->

                            <div class="profit-thumb-group">
                                <img src="{{asset('public/home/images/others/profit.png')}}" alt="Thumbnail">
                            </div>
                        </div>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end profit calculation block ~~-->
