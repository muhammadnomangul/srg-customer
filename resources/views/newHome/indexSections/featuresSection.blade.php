
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Features Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="features-block ptb-120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="feature-thumb-area" data-animate="hg-fadeInLeft">
                            <div class="feature-thumb">
                                <img src="{{asset('public/home/images/others/feature.png')}}" alt="Shape Thumbnail">
                            </div><!-- /.feature-thumb -->
                            <div class="animated-element">
                                <div class="element">
                                    <img src="{{asset('public/home/images/vactor/element1.png')}}" alt="Element">
                                </div>
                                <div class="element">
                                    <img src="{{asset('public/home/images/vactor/element2.png')}}" alt="Element">
                                </div>
                                <div class="element">
                                    <img src="{{asset('public/home/images/vactor/element3.png')}}" alt="Element">
                                </div>
                                <div class="element">
                                    <img src="{{asset('public/home/images/vactor/element4.png')}}" alt="Element">
                                </div>
                            </div><!-- /.animated-element -->
                        </div><!-- /.feature-thumb-area -->
                    </div><!-- /.col-lg-5 -->

                    <div class="col-lg-7">
                        <div class="features-content">
                            <div class="section-title">
                                <div class="subtitle" data-animate="hg-fadeInUp">About Our Company</div>
                                <h2 class="title-main" data-animate="hg-fadeInUp">Why B4U Global</h2><!-- /.title-main -->
                                <!-- <div class="title-text" data-animate="hg-fadeInUp">
                                    Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                                </div> --><!-- /.title-text -->
                            </div><!-- /.section-title -->
                            <div class="features-info-list">
                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="icon-area icon-small icon-red">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-save-money"></span>
                                            </div>
                                        </div><!-- /.icon-area-->
                                        <div class="info-title">
                                            <h3 class="heading">How We Provide Daily Profit</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>B4U Global is the bitcoin industry’s leading cryptocurrency trading currency. Our experienced traders make sure that each trade earns a healthy profit.</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->

                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="icon-area icon-small icon-green">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-shield-1"></span>
                                            </div>
                                        </div><!-- /.icon-area-->
                                        <div class="info-title">
                                            <h3 class="heading">We Are Leaders</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>We are in the bitcoin trading industry since the beginning and have produced leaders since then who are working with us.</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->

                                <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="icon-area icon-small icon-red">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-profits"></span>
                                            </div>
                                        </div><!-- /.icon-area-->
                                        <div class="info-title">
                                            <h3 class="heading">Highest ROI Guaranteed</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>Our clients earn the highest ROI, and that’s because our experts always makes analysis and research based trade.</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->

                                 <div class="single-info" data-animate="hg-fadeInUp">
                                    <div class="info-header">
                                        <div class="icon-area icon-small ">
                                            <div class="shape-icon"></div>
                                            <div class="icon">
                                                <span class="flaticon-employee"></span>
                                            </div>
                                        </div><!-- /.icon-area-->
                                        <div class="info-title">
                                            <h3 class="heading">24/7 Customer Support</h3>
                                        </div><!-- /.info-icon -->
                                    </div><!-- /.info-title -->
                                    <div class="info">
                                        <p>We provide round the clock customer support to our clients. Contact us anytime for queries and updates on your accounts.</p>
                                    </div><!-- /.info -->
                                </div><!-- /.single-info -->
                            </div>
                        </div><!-- /.features-content -->
                    </div><!-- /.col-lg-7 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end features block ~~-->