    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Deposit Withdraw Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

   <div class="deposit-withdraw-block bg-image bg-primary ptb-120" style="background-image: url('public/home/images/shape/shape01.png')">
            <div class="coin-thumb" data-animate="hg-fadeInRight">
                <img src="{{asset('public/home/images/others/conis.png')}}" alt="Thumbnail">
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center text-white">
                            <div class="subtitle" data-animate="hg-fadeInUp">Not hidden charge</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Latest Deposits & Withdrawals</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="deposit-withdraw-content" data-animate="hg-fadeInUp">
                            <div class="table-responsive deposit-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Latest Deposits</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/1.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maxlis Jon</a></div>
                                            </td>
                                            <td class="amount">$150</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/2.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Wagase</a></div>
                                            </td>
                                            <td class="amount">$869</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/3.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maktrek</a></div>
                                            </td>
                                            <td class="amount">$206</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/2.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Natallia Moe</a></div>
                                            </td>
                                            <td class="amount">$893</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/1.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">David James</a></div>
                                            </td>
                                            <td class="amount">$789</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/3.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maxlis Jon</a></div>
                                            </td>
                                            <td class="amount">$369</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->

                            <div class="table-responsive withdraw-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Latest Withdrawals</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/3.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maxlis Jon</a></div>
                                            </td>
                                            <td class="amount">$150</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/2.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Wagase</a></div>
                                            </td>
                                            <td class="amount">$869</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/1.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maktrek</a></div>
                                            </td>
                                            <td class="amount">$206</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/2.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Natallia Moe</a></div>
                                            </td>
                                            <td class="amount">$893</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/3.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">David James</a></div>
                                            </td>
                                            <td class="amount">$789</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                        <tr>
                                            <td class="investor">
                                                <div class="thumb"><img src="{{asset('public/home/images/author/1.png')}}" alt="Author"></div>
                                                <div class="name"><a href="#">Maxlis Jon</a></div>
                                            </td>
                                            <td class="amount">$369</td>
                                            <td class="date">2019 - 04 - 21 04:38:50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /.deposit-withdraw-content -->
                    </div>
                </div>
            </div>
        </div><!--~./ end deposit withdraw block ~--> 
