  <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Work Process Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
  <div id="work-process-block" class="work-process-block ptb-120">
            <div class="element-effect">
                <img class="star" src="{{asset('public/home/images/vactor/star2.png')}}" alt="Icon">
                <img class="line" src="{{asset('public/home/images/vactor/line2.png')}}" alt="Icon">
                <img class="triangle" src="{{asset('public/home/images/vactor/triangle2.png')}}" alt="Icon">
                <img class="rectangle" src="{{asset('public/home/images/vactor/rectangle.png')}}" alt="Icon">
                <img class="circle" src="{{asset('public/home/images/vactor/circle.png')}}" alt="Icon">
                <img class="circle2" src="{{asset('public/home/images/vactor/circle2.png')}}" alt="Icon">
            </div><!-- /.element-effect -->
            <div class="container ml-b-45">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <h4 class="subtitle" data-animate="hg-fadeInUp">Fast & Easy  Process.</h4><!--  /.subtitle -->
                            <h2 class="title-main" data-animate="hg-fadeInUp">Easy Investment Process.</h2><!-- /.title-main -->
                            <p class="title-text" data-animate="hg-fadeInUp">We provide easy options to invest your money. Your personal investment plan account is just few steps away.</p>
                        </div><!-- /.section-title -->
                    </div>
                </div>

                <div class="row process-list">
                    <div class="bg-line" style="background-image:url('public/home/images/others/linearrow.png')"></div><!-- /.bg-line -->
                    
                    <!--~./ Start Single Process ~-->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-process" data-animate="hg-fadeInUp">
                            <div class="icon color-red">
                                <span class="flaticon-profile"></span>
                            </div>
                            <h2 class="process-step">Creat Account</h2>
                            <h3 class="process-no">Step 01</h3>
                        </div>
                    </div><!--~./ end single process ~-->

                    <!--~./ Start Single Process ~-->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-process" data-animate="hg-fadeInUp">
                            <div class="icon color-blue">
                                <span class="flaticon-click"></span>
                            </div>
                            <h2 class="process-step">Choose plan</h2>
                            <h3 class="process-no">Step 02</h3>
                        </div>
                    </div><!--~./ end single process ~-->

                    <!--~./ Start Single Process ~-->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-process" data-animate="hg-fadeInUp">
                            <div class="icon color-green">
                                <span class="flaticon-debit-card"></span>
                            </div>
                            <h2 class="process-step">Investment</h2>
                            <h3 class="process-no">Step 03</h3>
                        </div>
                    </div><!--~./ end single process ~-->

                    <!--~./ Start Single Process ~-->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-process" data-animate="hg-fadeInUp">
                            <div class="icon color-yellow">
                                <span class="flaticon-bars"></span>
                            </div>
                            <h2 class="process-step">Get Profit</h2>
                            <h3 class="process-no">Step 04</h3>
                        </div>
                    </div><!--~./ end single process ~-->
                </div>
            </div><!-- /.container -->
        </div><!--~~./ end work process block ~~-->