 <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Pricing Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="pricing-block ptb-120" id="plans2">
            <div class="container ml-b-30">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <div class="subtitle" data-animate="hg-fadeInUp">We Hide Nothing</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Choose your Investment plan</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                We are offering best cryptocurrency investment plan in the market. Choose any investment plan and start earning profit from today.
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

                <div class="row pricing-table-list">
                    @foreach($pplans as $plans)
                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                      <div class="pricing-box pricing-green" data-animate="hg-fadeInUp">    
                        <h3><img style="width:70px;" src="{{asset('images/'.$plans->img_url)}}"> {{$plans->name}} </h3>
                    <hr>
                    <p><font color="blue"><i class="fa fa-star"></i></font> Criteria For Join This Plan : </p>
                    @if($plans->personel_investment_limit == 699)
                        <p class="textfont"><i class="fa fa-"></i>Personel Investment ($50 - {{$settings->currency}}{{$plans->personel_investment_limit}}) </p>
                    @else
                        <p class="textfont"><i class="fa fa-"></i>Personel Investment : {{$settings->currency}}{{$plans->personel_investment_limit}} </p>
                    @endif
                    <p class="textfont">OR</p>
                    @if($plans->personel_investment_limit == 699)
                        <p class="textfont"><i class="fa fa-"></i>Structural Investment ($50 - {{$settings->currency}}{{$plans->structural_investment_limit}}) </p>
                    @else
                        <p class="textfont"><i class="fa fa-"></i>Structural Investment : {{$settings->currency}}{{$plans->structural_investment_limit}} </p>
                    @endif
                    <hr>
                    <strong>Referal Bonuses : </strong>
                    <table style="width:100%;">
                        <tr>
                            <td style="width:50%;">
                                
                                <div class="agileits-services-info">
                                    <h6>Investment Bonus</h6>
                                    <hr>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px;   width:70%;">First Line:</div>
                                        <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->first_line}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px;   width:70%;">Second Line:</div>
                                        <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->second_line}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px;   width:70%;">Third Line:</div>
                                        <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->third_line}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px;   width:70%;">Fourth Line:</div>
                                        <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fourth_line}}%</div>
                                    </div>
                                    <div class="">
                                        <div style="float:left; width:70%;">Fifth Line:</div>
                                        <div style="float:right; width:30%; color:red; text-align:left;">{{$plans->fifth_line}}%</div>
                                    </div>
                                    <div class="services-info">
                                        
                                    </div>
                                </div>
                            </td>
                            <td style="width:50%;">
                                <div class="agileits-services-info">
                                    <h6>Profit Bonus</h6>
                                    <hr>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px; width:70%;">First Line:</div>
                                        <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->first_pline}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left;  font-size: 14px; width:70%;">Second Line:</div>
                                        <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->second_pline}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px; width:70%;">Third Line:</div>
                                        <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->third_pline}}%</div>
                                    </div>
                                    <div class="services-info">
                                        <div style="float:left; font-size: 14px; width:70%;">Fourth Line:</div>
                                        <div style="float:right; font-size: 14px;  width:30%; color:red; text-align:left;">{{$plans->fourth_pline}}%</div>
                                    </div>
                                    <div class="">
                                        <div style="float:left; font-size: 14px; width:70%;">Fifth Line:</div>
                                        <div style="float:right; font-size: 14px; width:30%; color:red; text-align:left;">{{$plans->fifth_pline}}%</div>
                                    </div>
                                    <div class="services-info">
                                        
                                    </div>
                                </div>
                            </td>
                            
                            </tr>
                        </table>
                    </div>
                    </div><!--~./ col-lg-4 ~-->
                @endforeach
                   
                </div><!--~./ row ~-->
            </div><!-- /.container -->
        </div><!--~~./ end pricing block ~~-->
