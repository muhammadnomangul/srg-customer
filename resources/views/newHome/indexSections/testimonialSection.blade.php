<div class="testimonial-block style-two ptb-120">
            <div class="element-effect">
                <img class="star" src="{{asset('public/home/images/vactor/star2.png')}}" alt="Icon">
                <img class="line" src="{{asset('public/home/images/vactor/line.png')}}" alt="Icon">
                <img class="triangle" src="{{asset('public/home/images/vactor/triangle.png')}}" alt="Icon">
                <img class="rectangle" src="{{asset('public/home/images/vactor/rectangle.png')}}" alt="Icon">
                <img class="circle" src="{{asset('public/home/images/vactor/circle.png')}}" alt="Icon">
            </div><!-- /.element-effect -->
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-9">
                        <div class="section-title">
                            <div class="subtitle" data-animate="hg-fadeInUp">Client Feedback</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">What’s Say Our Client</h2><!-- /.title-main -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-9 -->
                    <div class="col-lg-3">
                        <!-- side thumbnail -->
                        <div class="testimonail-carousel-thumb" data-animate="hg-fadeInLeft">
                            <div class="thumb-prev">
                                <img src="#')}}" alt="Customer Feedback">
                            </div><!-- /.thumb-prev -->
                            <div class="thumb-active">
                                <img src="#')}}" alt="Customer Feedback">
                            </div><!-- /.thumb-active -->
                            <div class="thumb-next">
                                <img src="#')}}" alt="Customer Feedback">
                            </div><!-- /.thumb-next -->
                        </div><!-- /.testimonail-carousel-thumb -->
                    </div>
                </div><!-- /.row -->

                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="testimonail-carousel-area" data-animate="hg-fadeInUp">
                            <div id="testimonail-carousel2" class="owl-carousel">
                                <div class="testimonial-item">
                                    <div class="thumbnails shape-thumb-area">
                                        <div class="shape-thumb">
                                            <img src="{{asset('public/home/images/testimonials/1.jpg')}}" alt="img">
                                        </div>
                                        <div class="shape-border"></div>
                                    </div><!-- /.thumbnails -->
                                    <div class="testimonial-info">
                                        <div class="details">
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit. Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consecteturLorem </p>
                                        </div><!-- /.details -->
                                        <div class="client-area">
                                            <div class="info">
                                                <div class="sing">
                                                    <img src="{{asset('public/home/images/others/sign.png')}}" alt="Thumb">
                                                </div>
                                                <h4 class="company-name">Thesoftking</h4><!-- /.client-name -->
                                                <p class="client-name">Julia Morthin (CEO & Founder )</p>
                                                <div class="total-reviews">
                                                    <ul>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.info -->
                                        </div><!-- /.client-area -->
                                    </div><!-- /.testimonial-info -->
                                </div><!-- /.testimonial-item --> 

                                <div class="testimonial-item">
                                    <div class="thumbnails shape-thumb-area">
                                        <div class="shape-thumb">
                                            <img src="{{asset('public/home/images/testimonials/2.jpg')}}" alt="img">
                                        </div>
                                        <div class="shape-border"></div>
                                    </div><!-- /.thumbnails -->
                                    <div class="testimonial-info">
                                        <div class="details">
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit. Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consecteturLorem </p>
                                        </div><!-- /.details -->
                                        <div class="client-area">
                                            <div class="info">
                                                <div class="sing">
                                                    <img src="{{asset('public/home/images/others/sign.png')}}" alt="Thumb">
                                                </div>
                                                <h4 class="company-name">BrothersLab</h4><!-- /.client-name -->
                                                <p class="client-name">Julia Morthin (CEO & Founder )</p>
                                                <div class="total-reviews">
                                                    <ul>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.info -->
                                        </div><!-- /.client-area -->
                                    </div><!-- /.testimonial-info -->
                                </div><!-- /.testimonial-item --> 

                                <div class="testimonial-item">
                                    <div class="thumbnails shape-thumb-area">
                                        <div class="shape-thumb">
                                            <img src="{{asset('public/home/images/testimonials/3.jpg')}}" alt="img">
                                        </div>
                                        <div class="shape-border"></div>
                                    </div><!-- /.thumbnails -->
                                    <div class="testimonial-info">
                                        <div class="details">
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit. Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consecteturLorem </p>
                                        </div><!-- /.details -->
                                        <div class="client-area">
                                            <div class="info">
                                                <div class="sing">
                                                    <img src="{{asset('public/home/images/others/sign.png')}}" alt="Thumb">
                                                </div>
                                                <h4 class="company-name">IdealBrothers</h4><!-- /.client-name -->
                                                <p class="client-name">Julia Morthin (CEO & Founder )</p>
                                                <div class="total-reviews">
                                                    <ul>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.info -->
                                        </div><!-- /.client-area -->
                                    </div><!-- /.testimonial-info -->
                                </div><!-- /.testimonial-item --> 

                                <div class="testimonial-item">
                                    <div class="thumbnails shape-thumb-area">
                                        <div class="shape-thumb">
                                            <img src="{{asset('public/home/images/testimonials/4.jpg')}}" alt="img">
                                        </div>
                                        <div class="shape-border"></div>
                                    </div><!-- /.thumbnails -->
                                    <div class="testimonial-info">
                                        <div class="details">
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit sed quia non qui dolorem ipsum quia dolor sit amet, consectetur, adipis civelit. Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consecteturLorem </p>
                                        </div><!-- /.details -->
                                        <div class="client-area">
                                            <div class="info">
                                                <div class="sing">
                                                    <img src="{{asset('public/home/images/others/sign.png')}}" alt="Thumb">
                                                </div>
                                                <h4 class="company-name">Thesoftking</h4><!-- /.client-name -->
                                                <p class="client-name">Julia Morthin (CEO & Founder )</p>
                                                <div class="total-reviews">
                                                    <ul>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li class="color"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.info -->
                                        </div><!-- /.client-area -->
                                    </div><!-- /.testimonial-info -->
                                </div><!-- /.testimonial-item --> 
                            </div><!-- /#testimonail-carousel -->
                            <div class="dots-thumb">
                                <img src="{{asset('public/home/images/shape/shape-dot-small.png')}}" alt="Dots">
                            </div>
                        </div>
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end testimonial block ~~-->