       <div class="latest-news-block bg-gradient ptb-120">
            <div class="container ml-b-30">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <div class="subtitle" data-animate="hg-fadeInUp">Latest News Post</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Our Company News</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                These are the latest news and event updates from us, for our clients
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

                <div class="row">
                    @foreach($album_gallery as $album)
                    <div class="col-lg-6 col-md-6">
                        <article class="post post-grid" data-animate="hg-fadeInUp">
                            <div class="entry-meta-content">
                             
                                <div class="entry-date"><span>{{ date('j F, Y', strtotime($album->created_at)) }}</span></div>
                            </div><!-- /.entry-meta-content -->

                            <figure class="post-thumb">
                                <a href="{{url('events_gallery1/')}}/{{$album->id}}">
                                <img style="height: 300px; " 
                                src="{{asset('images/gallery/'.$album->cover_image)}}"
                                data-image="{{asset('images/gallery/'.$album->cover_image)}}"
                                data-description="Preview {{$album->name}}">
                            </figure><!--  /.post-thumb -->
                            <div class="post-details">                    
                                <h2 class="entry-title"><a href="{{url('events_gallery1/')}}/{{$album->id}}">{{$album->name}}</a></h2><!-- /.entry-title -->
                                <div class="entry-content">
                                    <p>{{ \Illuminate\Support\Str::limit($album->description, 150, $end='...') }}</p>
                                </div><!-- /.entry-content -->
                            </div><!-- /.post-details -->
                        </article><!-- /.post -->
                    </div><!-- /.col-lg-4 -->
                    @endforeach
                    
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end latest news block ~~-->