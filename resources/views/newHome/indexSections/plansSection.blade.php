 <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Pricing Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="pricing-block ptb-120" id="plans2">
            <div class="container ml-b-30">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="section-title text-center">
                            <div class="subtitle" data-animate="hg-fadeInUp">Not hidden charge</div>
                            <h2 class="title-main" data-animate="hg-fadeInUp">Choose you Investment plan</h2><!-- /.title-main -->
                            <div class="title-text" data-animate="hg-fadeInUp">
                                Blandit rutrum auctor eu quis euismod, eget porta feugiat, ut nec sit. Est fusce enim dapibus commodo  pulvinar con
                            </div><!-- /.title-text -->
                        </div><!-- /.section-title -->
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

                <div class="row pricing-table-list">
                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box pricing-green" data-animate="hg-fadeInUp">
                            <div class="pricing-icon icon-default icon-green">
                                <div class="shape-icon"></div>
                                <div class="icon">
                                    <span class="flaticon-profit"></span>
                                </div>
                            </div><!-- /.pricing-icon-->
                            <div class="package-price">
                                <div class="pricing">25%</div>
                                Daily For 5 Days
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->

                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box pricing-red" data-animate="hg-fadeInUp">
                            <div class="pricing-icon icon-default icon-red">
                                <div class="shape-icon"></div>
                                <div class="icon">
                                    <span class="flaticon-startup"></span>
                                </div>
                            </div><!-- /.pricing-icon-->
                            
                            <div class="package-price">
                                <div class="pricing">30%</div>
                                Daily For 5 Days
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->

                    <div class="col-lg-4 col-md-6">
                        <!--~~~~~ Start Pricing Box ~~~~~--> 
                        <div class="pricing-box" data-animate="hg-fadeInUp">
                            <div class="pricing-icon icon-default">
                                <div class="shape-icon"></div>
                                <div class="icon">
                                    <span class="flaticon-business-and-finance"></span>
                                </div>
                            </div><!-- /.pricing-icon-->
                            
                            <div class="package-price">
                                <div class="pricing">45%</div>
                                Daily For 5 Days
                                <div class="divider"></div>
                            </div><!-- /.pricing-price -->
                            <div class="pricing-content">
                                <ul class="list">
                                    <li>Earning On Mon - Fri</li>
                                    <li>Min Invest $25.00 usd</li>
                                    <li>Max Invest $40,000.00 usd</li>
                                    <li>Principal Return</li>
                                    <li>Compound Available</li>
                                </ul>
                            </div><!-- /.pricing-content -->
                            <div class="pricing-footer text-center">
                                <a href="#" class="btn btn-default">Invest Now</a>
                            </div><!-- /.pricing-footer -->
                        </div><!--~./ end pricing box ~-->
                    </div><!--~./ col-lg-4 ~-->
                </div><!--~./ row ~-->
            </div><!-- /.container -->
        </div><!--~~./ end pricing block ~~-->
