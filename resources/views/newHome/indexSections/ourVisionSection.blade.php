<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Our Vision Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div class="our-vision-block bg-primary pd-t-120">
            <div class="shape-group">
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
            </div>

            <div class="section-vertical-title-area">
                <h2 class="vertical-title"><span>our</span> vision</h2>
            </div><!-- /.section-vertical-title-area -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="our-vision-content-area">
                            <div class="our-vision-content bg-image" style="background-image: url('public/home/images/shape/shape-dot2.png')">
                                <div class="element-effect">
                                    <img class="line" src="{{asset('public/home/images/vactor/line2.png')}}" alt="Icon">
                                    <img class="triangle" src="{{asset('public/home/images/vactor/triangle2.png')}}" alt="Icon">
                                    <img class="rectangle" src="{{asset('public/home/images/vactor/rectangle.png')}}" alt="Icon">
                                    <img class="circle" src="{{asset('public/home/images/vactor/circle.png')}}" alt="Icon">
                                </div><!-- /.element-effect -->
                                <div class="vision-info-list">
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-green">
                                            <span class="flaticon-employee-1"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">Security & Privacy</h3>
                                            <p>We never compromise on security and privacy of our clients. We do not share the clients’ details with anyone.</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-red">
                                            <span class="flaticon-bar-chart"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">Maximize the Profit Margin</h3>
                                            <p>We always are in pursue of maximizing our profit, so we can provide best return on investment to our clients.</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                    <div class="card-info-one" data-animate="hg-fadeInUp">
                                        <div class="icon bg-turquoise">
                                            <span class="flaticon-employee"></span>
                                        </div><!-- /.icon-->
                                        <div class="info">
                                            <h3 class="heading">Easy To Use Platform</h3>
                                            <p>We do not like to complicate things, we have developed best user friendly platform, which enables the customers to have a complete control on their accounts.</p>
                                        </div><!-- /.info -->
                                    </div><!-- /.card-info-one -->
                                </div>
                            </div><!-- /.our-vision-content -->
                            <div class="vision-thumb-area" data-animate="hg-fadeInLeft">
                                <img src="{{asset('public/home/images/others/vision-thumb.png')}}" alt="Thumb">
                            </div>
                        </div><!-- /.our-vision-content-area -->
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!--~~./ end our vision block ~~-->
