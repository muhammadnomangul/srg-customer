@include('header')

		<!-- //header-ends -->

		<!-- main content start-->

		<div id="page-wrapper">

			<div class="main-page signup-page">

				<h3 class="title1">Daily Bonus List</h3>
				@if(app('request')->session()->get('back_to_admin'))
					<div class="row">
						@include('messages')
							@php
								$investmentBonus = investmentBonus(Auth::user()->u_id);
								$profitBonus = profitBonus(Auth::user()->u_id);
								$totalInvestmentBonus = totalInvestmentBonus(Auth::user()->u_id);
							@endphp
						@if($investmentBonus)	
						INVESTMENT BONUS : {{number_format($investmentBonus, 2)}}
						@endif
						@if($profitBonus)	
						+ PROFIT BONUS : {{number_format($profitBonus, 2)}}
						@endif
						@if($totalInvestmentBonus)	
						=   TOTAL BONUS : {{number_format($totalInvestmentBonus, 2)}}
						@endif
					</div>
				@endif
				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 

					<table id="myTable" class="table table-hover"> 

					 
					</table>
					 
				</div>

			</div>

		</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
	<script>

   $(document).ready( function ()
	{
        var table = $('#myTable').DataTable( 
        {
           	processing: true,
            serverSide: true,
            pageLength: 10,
            "ajax": {
                "url": "{{url('dashboard/daily_bonus/json')}}",
                "type": "GET",
				error: function (xhr, error, thrown) {
					//$("#CallSubModal").model('show')
				}
            },
            columns:
                [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', title : 'Sr#', orderable: true,searchable: false,
                      
                        'render': function (data, type, row)
                        {
                            var color = "font-weight: bold;";
                            
                            return '<span style="'+color+'">'+row.DT_RowIndex+'</span>'
                        }
                        
                	},
                    { data : 'trade_id', name: 'trade_id', title : 'Invoice ID', orderable: true, searchable: true},
                    { data : 'user_id', name: 'user_id', title : 'User ID', orderable: true, searchable: true},
                    { data : 'parent_id', name: 'parent_id', title : 'Parent ID', orderable: true, searchable: true},
                    { data : 'bonus', name: 'bonus', title : 'Bonus', orderable: true, searchable: true},
                    { data : 'details', name: 'details', title : 'Details', orderable: true, searchable: true},
                    // { data : 'created_at', name: 'created_at', title : 'Date created', orderable: true, searchable: true}
					{
						"data": "created_at",
						"title": "Date Created",
						"orderable": false,
						"searchable": true,
						// type: 'date-m-d-Y',
						// targets: 0
						"render": function (data, type, row) {

							var day = moment(row.created_at).format('DD');
							var hour = moment(row.created_at).format('HH');
							console.log('day',day)
							console.log('day',hour)
							var year = moment(row.created_at).format('YYYY');
							console.log('year',year)
							var bonus_type = row.details;
							if(bonus_type == "Profit Bonus"){
								if(day > 10 && year > 2020){
									return moment(row.created_at).add(1, 'days').format('YYYY-MM-DD');

								}else if(day < 10 && year > 2020){
									return moment(row.created_at).add(1, 'days').format('YYYY-MM-DD');

								}else{
									return moment(row.created_at).format('YYYY-MM-DD');
								}
							}
							else{
								console.log('investment ',bonus_type)
								if(hour > 21){
									return moment(row.created_at).add(1, 'days').format('YYYY-MM-DD');
								}else{

									return moment(row.created_at).format('YYYY-MM-DD');
								}
							}
						}
					},

				],

          	order:[6,'desc']

        });
	});		

/*	$(document).ready( function () {

		$('#myTable').DataTable( {

        "pagingType": "full_numbers"

    	});

	});*/



	$(".submit1").click(function(){
			//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");

		if(conf){
			return true;
		}
		else{
			return false;
		}

	});

	</script>

		@include('footer')