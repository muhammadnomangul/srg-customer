<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
@include('mails.head')
<body>
<h1>Dear Team!</h1>
<p>{{ $msg }}</p>

<p>Thank you</p>
@include('mails.footer')
</body>
</html>