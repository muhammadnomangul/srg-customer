<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="'https://fonts.googleapis.com/css2?family=PT+Sans:wght@700&display=swap">
</head>

<body>
<div class="container" style="display: block;">
    <div class="maincontainer" style="width: 600px;
    align-items: center;margin: 0 auto;
    height: 528px;left:0;text-align:center;border: gainsboro 1px solid  ; justify-content: center; align-items: center; flex-direction: column;   background: #F4F4F4 0% 0% no-repeat padding-box;">
        <div class="scontainer" style="width: 480px;
    height: 430px;background: #FFFFFF 0% 0% no-repeat padding-box;
    box-shadow:  0px 3px 6px #00000029;
    border-radius: 5px;
    padding-top: 2px;
    margin-left: 55px;
    opacity: 1;background-color: white; border: 2px solid white; box-shadow: darkgray 1px 1px 1px 1px ; border-radius: 5px;margin: 0 auto; ">
            <table style="margin: 0 auto;">
                <thead>
                <div class="logo" style="top: 54px;

                width: 140px;
                height: 71px;background: transparent url('img/Logo - Future Car Planning a-01.png') 0% 0% no-repeat padding-box;
    opacity: 1;padding-bottom: -px;"><th colspan="4">
                        <img src="./Logo.png " style="margin-top:-20px"></th>

                </thead>
                <div><tr>
                        <td style="text-align: center" colspan="4"><h1 style="top: 144px;
                left: 167px;
                width: 267px;
                height: 32px;

                display: block;
                font: normal normal bold  PT Sans;font-size: 21px;
                letter-spacing: 0px;
                color: #000000;
                opacity: 1;font-family: 'PT Sans', sans-serif;margin-bottom: 1px; margin: 0 auto;">Verify your email address</h1></td>
                    </tr></div>
                <tr>
                    <td style="text-align: center" colspan="4"><h2 style="top: 192px;
                left: 159px;
                width: 283px;
                height: 36px;display: block;
                text-align: center;
                font: normal normal normal  PT Sans;
                font-size: 12px;
                letter-spacing: 0px;
                color: #000000;margin: 0 auto;
                opacity: 0.54;font-family: 'PT Sans', sans-serif;  margin-bottom: 25px; " >To verify you email address, enter this code in your browser.</h2></td>
                </tr>

                <!-- <td style="text-align: center" colspan="4"><div class="code-bg" style="top: 8px;
                    left: 2px;margin: 0 auto;
                    width: 180px;display: block;
                    height: 67px;
                   margin: 0 auto;
                    background: #F0F1F7 0% 0% no-repeat padding-box;
                    border-radius: 5px;
                    opacity: 1;margin: 0 auto;"></td> -->
                <td><div class="code" input type="text" id="0000" name="0000"
                         style="top: 276px;

                        width: 150px;display: block;
                        background: #F0F1F7 0% 0% no-repeat padding-box;
                border-radius: 5px;

                        text-align: center;
                        font: normal normal normal 36px/48px Segoe UI;
                        letter-spacing: 7.2px;padding-left: 15px;padding-right: 15px; padding-bottom: 10px; padding-top: 5px;
                        color: #090808;margin: 0 auto;
                        opacity: 1;justify-content:center;text-align: center;align-items: center;">
                         {{$user_otp}}

                    </div><br>

                </td>


                <tr>
                    <td style="text-align: center" colspan="4"> <h2 style="top: 355px;
                left: 159px;
                text-align: center;display: block;
                justify-content: center;
                width: 283px;
                height: 36px;margin:0 auto;
                text-align: center;
                font: normal normal normal 14px/18px PT Sans;
                letter-spacing: 0px;
                flex-direction: column;
                color: #000000;
                opacity: 0.54;font-family: 'PT Sans', sans-serif ">If you didn't request a code, you can safely ignore this
                            email.</h2>
                    </td>

                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td colspan="4" style="text-align: center; vertical-align: middle;"> <p style="top: 486px;
                    left: 209px;
                    margin-left: 200px;
                    margin-top: 15px;
                    width: 192px;
                    height: 13px;
                    text-align: center;
                    font: normal normal normal  PT Sans;font-size: 9.1px;
                    letter-spacing: 0px;
                    color: #908F94;
                    opacity: 1;font-family: 'PT Sans', sans-serif;padding-top:22px;">© 2021 Future Car Finance,
                        All rights reserved.
                    </p></td>
            </tr> </table>
        </table>
    </div>
</div>
</body>
</html>