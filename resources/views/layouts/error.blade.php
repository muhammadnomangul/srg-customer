<!-- Call Subscription Modal -->
<div id="CallSubModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="sub_content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Be Patience !!</h4>
            </div>
            <div class="modal-body" style="text-align: center; margin-bottom: 30px;">
                {{--   <div class="col-sm-8 col-offset-sm-4"><label class="modal-title"></label></div>--}}
                <form method="post" action="{{route('subcribeCallPckg')}}">
                    @csrf
                    <p>It Seems Your Internet Connection is slow ... </p>
                    <small style="color: red">(Please Try Again later .. .)</small>
                    <br><br>
{{--                    <input type="button" class="close btn btn-primary btn-sm" value="Got it " style="float: right">--}}
                </form>
            </div>
            <div class="clearfix"></div>
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>
