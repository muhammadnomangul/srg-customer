<!DOCTYPE HTML>
<html>
<head>

    <title>@if(isset($settings->site_name)){{$settings->site_name}}@else B4U Global @endif |@if(isset($title)) {{$title}} @endif</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('home/home/images/favicon.ico')}}">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Bootstrap Core CSS -->
    <!-- <link href="{{ asset('css/bootstrap.css')}}" rel='stylesheet' type='text/css' /> -->

    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet" type='text/css'> 

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet"> 
    <link href="{{ asset('css/jquery.fancybox.min.css')}}" rel="stylesheet"> 
    <!--<link href="{{ asset('js/fancybox/jquery.fancybox-1.3.4.css')}}" type="text/css" rel="stylesheet" media="screen"> -->
    <link href="{{ asset('css/multi-fileinput.css')}}" rel="stylesheet"  type="text/css"> 
    <link href="{{ asset('css/jquery.dataTables.min.css')}}" rel="stylesheet"> 

    <link href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" rel="stylesheet"  type="text/css"> 
    <!-- //font-awesome icons -->
    
    <!-- js ---->
    <script src="{{ asset('js/jquery-1.12.4.js')}}"> </script>
    <script src="{{ asset('js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{ asset('jquery.copy-to-clipboard.js')}}"></script>
    <script src="{{ asset('js/modernizr.custom.js')}}"></script>
    <!-- <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/jquery-datatable-3.3.1.js')}}"></script>-->


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
      
    <script type="text/javascript" src="{{asset('js/ckeditor5/ckeditor.js')}}"></script>

    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts--> 

    <!--animate-->
    <link href="{{ asset('css/animate.css')}}" rel="stylesheet" type="text/css" media="all">

    <script src="{{ asset('js/wow.min.js')}}"></script>

    <script>
        new WOW().init();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <!--//end-animate-->

    <!-- Alert Message -->
    <header>
        <meta content="text/html; charset=utf-8" http-equiv=Content-Type>
        <link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notonastaliqurdudraft.css">

        <style>
            .urdu_warning {
                font-family: 'Noto Nastaliq Urdu Draft', serif;
            }
        </style>
    </header>


    <!-- Metis Menu -->
    <script src="{{ asset('js/metisMenu.min.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}"></script>
    <link href="{{ asset('css/custom.css')}}" rel="stylesheet">
    @yield('style')
    <!--//Metis Menu -->
</head> 

<body class="cbp-spmenu-push">

@include('layouts.header')

        @yield('content')


@include('modals')
@include('footer')

