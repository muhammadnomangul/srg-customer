<!-- facebook sdk -->
<div id="fb-root"></div>

<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];

        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=642791809168417";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /facebook sdk -->

<style type="text/css">
    /* <!--.nav{
        overflow-y: scroll;
    }

    .navbar-collapse{
        overflow-y: scroll;
    }--> */

    .navbar2 {
        overflow-y: scroll;
        margin-top: 13px;
        height: 90%;
    }

    .navbar2::-webkit-scrollbar {
        background: transparent; /* make scrollbar transparent */
    }

    .loader1 {
        background-color: #000;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0.5;
        filter: alpha(opacity=50);
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1020;
    }

    .loader1 img {
        vertical-align: middle;
        left: 50%;
        top: 50%;
        position: fixed;
    }

    .loader1 .x16 span {
        line-height: 16px;
        font-size: 12px;
        margin-left: 6px;

    }

    .loader1 .x32 img {
        width: 75px;
        height: 75px;
    }

    .loader {
        left: 50%;
        top: 50%;

        margin: 1px auto auto 91px;
        border: 1px solid #f3f3f3;
        border-radius: 50%;
        border-top: 1px solid #3498db;
        width: 18px;
        height: 18px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>

<div class="main-content">
    <div class="loader1" style="display: none;">
        <div class="loader1 x32"><img src="{{ asset('/images/loading32x32.gif')}}"><span
                    style="color: rgb(0, 0, 0);"></span></div>
    </div>
    <!--left-fixed -navigation-->
    <div class="sidebar" role="navigation">

        <div class="navbar-collapse">
            <nav class="navbar2 cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <ul class="nav" id="side-menu">

                    @if(app('request')->session()->get('back_to_admin'))
                        <li><a href="{{ route('login_to_admin_account') }}" class="active"><i
                                        class="fa fa-dashboard nav_icon"></i>Back TO Admin Panel</a></li>
                        <li><a href="{{ url('dashboard/myreport') }}" class="active"><i
                                        class="fa fa-dashboard nav_icon"></i>Report</a></li>
                        <li><a href="#" class="active" data-toggle="modal" data-target="#sndmsg"
                               onclick="viewModelFuncM({{Auth::user()->id}})"><i
                                        class="fa fa-envelope nav_icon"></i>Send Notification</a></li>
                    @endif
                    <li><a href="{{ url('/dashboard') }}"><i
                                    class="fa fa-dashboard nav_icon"></i>@lang('home.Dashboard')</a></li>

                <!--<li class=""><a href="#"><i class="fa fa-user nav_icon"></i>Manage Profile <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="{{ url('dashboard/changepassword') }}">Change Password</a>
									<a href="{{ url('dashboard/accountdetails') }}">Update Account</a>
								</li>
							</ul>
						</li> -->
                    <!-- /nsdsv-second-level  -->

                    @if(Auth::user()->type =='0' || Auth::user()->type =='3' )
                        <li><a href="{{ url('https://support.b4uglobal.com/') }}">
                                <i class="far fa-life-ring" style="color:yellow;"></i>@lang('home.Support')</a>
                        </li>
                        <li><a href="{{ url('dashboard/deposits') }}">
                                <i class="fa fa-money nav_icon"></i>@lang('home.Deposits')</a>
                        </li>

                        <li><a href="{{ url('dashboard/withdrawals') }}">
                                <i class="fa fa-dollar nav_icon"></i>@lang('home.Withdrawals')</a>
                        </li>

                        <li><a href="{{ url('dashboard/sold') }}">
                                <i class="fa fa-dollar nav_icon"></i>@lang('home.Sold')</a>
                        </li>

                        <li><a href="{{ url('dashboard/plans') }}">
                                <i class="fa fa-cog nav_icon"></i>@lang('home.Investment plans')</a>
                        </li>

                        <li><a href="{{ url('dashboard/profitCalculator') }}">
                                <i class="fa fa-cog nav_icon"></i>Profit Calculator</a>
                        </li>

                        <li><a href="{{ url('dashboard/referuser') }}">
                                <i class="fa fa-users nav_icon"></i>@lang('home.Refer users')</a>
                        </li>

                    <!-- <li><a href="{{ url('dashboard/dailyuserLogs') }}">
								<i class="fa fa-cog nav_icon"></i>Daily Logs</a>
							</li--->

                        <li><a href="{{ url('dashboard/partners') }}">
                                <i class="fa fa-users nav_icon"></i>@lang('home.Your Partners')</a>
                        </li>

                    @endif

                    @if(Auth::user()->type =='1')
                        <li><a href="{{ url('dashboard/rates') }}">
                                <i class="fa fa-cog nav_icon"></i>Daily Exchange Rates</a>
                        </li>
                    @endif

                    @if(Auth::user()->type =='1' || Auth::user()->type =='2')
                        <li><a href="{{ url('dashboard/searchUser') }}">
                                <i class="fa fa-users nav_icon"></i>Search User</a>
                        </li>
                    @endif

                    @if(Auth::user()->type =='1' || Auth::user()->type =='2' || Auth::user()->type =='5')
                        <li><a href="{{ url('dashboard/manageusers') }}">
                                <i class="fa fa-users nav_icon"></i>Manage Users</a>
                        </li>

                        <li>
                            <a href="{{ url('dashboard/mpdeposits') }}"><i class="fa fa-th-list nav_icon"></i>Manage
                                Deposits</a>
                        </li>
                    @endif

                    @if(Auth::user()->type =='1' || Auth::user()->type =='2' || Auth::user()->type =='5')
                        <li><a href="{{ url('dashboard/pwithdrawals') }}">
                                <i class="fa fa-th-list nav_icon"></i>Manage Withdrawals</a>
                        </li>

                        <li><a href="{{ url('dashboard/msold') }}">
                                <i class="fa fa-dollar nav_icon"></i>Manage Solds</a>
                        </li>

                        @if(Auth::user()->id =='1' || Auth::user()->id =='1013' )
                            <li><a href="{{ url('dashboard/settings') }}">
                                    <i class="fa fa-gear nav_icon"></i>Settings</a>
                            </li>
                        @endif
                    @endif

                    @if(Auth::user()->awarded_flag =='1')
                        <li><a href="{{ url('dashboard/awarded_partners') }}">
                                <i class="fa fa fa-image nav_icon"></i>Distributor Partners </a>
                        </li>
                    @endif

                    @if(Auth::user()->type =='1' || Auth::user()->type =='2')
                        <li><a href="{{ url('dashboard/plans') }}">
                                <i class="fa fa-cog nav_icon"></i>Investment plans</a>
                        <!--<a href="{{ url('dashboard/mplans') }}"><i class="fa fa-cog nav_icon"></i>Investment plans</a>-->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file nav_icon"></i>Reporting <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                                <li><a href="{{ url('dashboard/topinvestors') }}">
                                        <i class="fa fa-money nav_icon"></i>Top Investors</a>
                                </li>
                                <li><a href="{{ url('dashboard/lastWeekProfitBonus') }}">
                                        <i class="fa fa fa-users nav_icon"></i>Last Week Profit And Bonus</a>
                                </li>

                                <li><a href="{{ url('dashboard/partners') }}">
                                        <i class="fa fa-users nav_icon"></i>Your Partners</a>
                                </li>


                                <li><a href="{{ url('dashboard/payprofitbonus') }}">
                                        <i class="fa fa fa-image nav_icon"></i>Send Profit/Bonus as CronJob </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-plus nav_icon"></i>Others <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                                <li><a href="{{ url('dashboard/albumsList') }}">
                                        <i class="fa fa-image nav_icon"></i>Albums List</a>
                                </li>
                                <li><a href="{{ url('dashboard/referuser') }}">
                                        <i class="fa fa-users nav_icon"></i>Refer users</a>
                                </li>
                                <li><a href="{{ url('dashboard/dailyLogs') }}">
                                        <i class="fa fa-cog nav_icon"></i>Daily Logs</a>
                                </li>
                                <li><a href="{{ url('dashboard/adminlogs') }}">
                                        <i class="fa fa-cog nav_icon"></i>Admin Logs</a>
                                </li>
                            </ul>
                        </li>

                        <li><a href="{{ url('user/logout') }}">
                                <i style="margin-right: 10px;" class="fas fa-sign-out-alt"></i> New Logout</a>
                        </li>
                    @endif
                </ul>
                <!-- //sidebar-collapse -->
            </nav>
        </div>
    </div>
    <!--left-fixed -navigation-->

    <!-- header-starts -->
    <div class="sticky-header header-section ">

        <div class="header-left">
            <!--toggle button start-->
            <button id="showLeftPush"><i class="fa fa-bars"></i></button>
            <!--toggle button end-->

            <!--logo -->
            <div class="logo" style="padding:6px; width:100px; float:left;">
                <a href="{{ url('/') }}" style="padding-left:15px !important;">
                    <image src="{{ url('images/b4u-investment1.png') }}">
                </a>
            </div>

            @if(Auth::user()->u_id =='B4U00016447')
                <div style="  margin-top: 40px;  float: right;">
                    <a href="{{ url('locale/en') }}">English</a>
                    | <a href="{{ url('locale/ma') }}">Malaysia</a>
                </div>
            @endif

            <div class="clearfix"></div>
        </div>

        <div class="header-right">

            <div class="profile_details" style="padding:8px; margin-top:15px;">
                <?php
                $name = '';
                if (isset(Auth::user()->name)) {
                    $name = Auth::user()->name;
                    if (strlen($name) > 10) {
                        $name = substr($name, 0, 10) . '...';
                    }
                }
                //$disk = Storage::disk('gcs');
                //$file = $disk->get('test-public.png');
                //echo $file;
                //exit;
                ?>

                @if(Auth::user()->photo == "")
                    <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                    style="width:40px; height:40px; border-radius:100%; border:3px solid #fff;"
                                    src="{{ asset('/images/noimage-round.jpg') }}" alt=""> </span> {{ $name }} </a>
                @else
                    <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                    style="width:40px; height:40px; border-radius:100%; border:3px solid #fff;"
                                    src="<?='https://storage.googleapis.com/b4ufiles/profile_Img/' . Auth::user()->photo; ?>"
                                    alt=""> </span> {{ $name }} </a>
                @endif

                | <a href="{{ url('dashboard/accountdetails') }}">{{Auth::user()->u_id}}</a>

                @if(Auth::user()->id !='1')
                    @if(Auth::user()->msg_bit == "1" )
                        |    <a href="#" data-toggle="modal" data-target="#showmsg1"
                                onclick="viewModelFuncS({{Auth::user()->id}})">
                            <img style="padding-bottom:10px; width:25px; height:35px;"
                                 src="{{ URL::to('/images/bell-new.png') }}" alt="Bell"></a>
                    @else
                        |    <img style="padding-bottom:10px; width:25px; height:35px; border-radius:100%; "
                                  src="{{asset('/images/bell-no-new.png')}}" alt="Bell">
                    @endif
                @endif
                | <a href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    <img style="padding-bottom:10px;" src="{{asset('/images/logout.png')}}" alt="Logout">
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Show Msg -->
    <div id="showmsg1" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Message</h4>
                </div>

                <div class="modal-body">
                    <div id="bodycontent3">
                        <!-- display Ajax Models For Admin Users here -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /Show Msg -->
    <script type="text/javascript">
        function viewModelFuncS(id) {
            //alert(id);
            // var  mtype = "accsount";
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/showmsg',
                data: {id: id, "_token": "{{ csrf_token() }}",},
                //	cache: false,
                success: function (response) {
                    console.log(response);
                    $("#bodycontent3").html("");
                    $("#bodycontent3").html(response);
                    //	$( "#ajaxval p" ).appenda(response);
                }, error: function (response) {
                    console.log(response);
                    //alert("error!!!!");
                    //$("#CallSubModal").modal('show');
                }
            });
        }
    </script>

    <!-- //header-ends -->
@include('admin.modals_admin')