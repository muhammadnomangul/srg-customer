@include('header')
<!-- //header-ends -->
<?php


$availableReinvestment = 0;
//$pmodes = str_split($settings->payment_mode);
$pmodes = explode(",", $settings->payment_mode);
//print_r($pmodes);
//exit;w
$amountusd = "";
$port = 0;
$userid = \Auth::user()->id;

?>
<!-- main content start-->

<div id="page-wrapper">
    <div class="main-page signup-page">
        @include('messages')
        <div class="sign-u" style="background-color:#fff; padding:20px;">


            <div class="sign-up1">
                @if($deposit_mode != "new")
                    <h4>You are to make {{$deposit_mode}} deposit of <strong>${{$amount}} {{$currency}}</strong> using
                        your preferred mode of payment below.</h4><br>
                    <div>
                        @if($availableReinvestment > 0)
                            Your Available {{$reinvest_type}} reinvestment :
                            ${{number_format($availableReinvestment,4)}}
                        @endif
                    </div>

                @elseif($deposit_mode=="new")
                    <h4>You are to make new deposit of <strong> {{$amount}} {{$currency}}</strong> using your preferred
                        mode of payment below.</h4><br>

                    @if($currency=="USD")
                        @foreach($pmodes as $pmode)
                            @if($pmode==1)
                                <div class="panel panel-default" style="border:0px solid #fff;">

                                    <!-- Panel Heading Starts -->
                                    <h3>Bank Deposit/Transfer </h3>
                                    <?php
                                    $banklist = \App\BankAccountModel::getAllBankAccounts();
                                    $banklistPak = \App\BankAccountModel::getPakBankAccounts();

                                    ?>

                                    @foreach($banklist as $keyIt => $account)


                                        <h4>
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                               href="#{{$keyIt}}">
                                                <strong>
                                                    @if(isset($account[0]))
                                                        {{ $account[0]->country_name }} ({{ $account[0]->currency }})
                                                    @endif
                                                </strong>&nbsp;
                                                <img src="{{ asset('home/images/bank-transfer.png')}}" width="70px;"
                                                     height="40px;">
                                            </a>
                                        </h4>

                                        <div id="{{$keyIt}}" class="panel-collapse collapse">
                                          @if($account[0]->country_name == 'Pakistan')
                                                <div class="alert alert-danger">
                                              Please contact to your Parent-ID or CEO.
                                                </div>
                                            @endif
                                            @foreach($account as $key => $subDetails)
                                                <div class="alert alert-success">
                                                    @if($subDetails->bank_name == 'TransferWise')
                                                        <h4><strong>Bank Name:</strong> {{ $subDetails->bank_name }}
                                                        </h4>
                                                        <h4><strong>Account
                                                                Title:</strong> {{ $subDetails->account_title }}</h4>
                                                        <h4><strong>Account# /
                                                                IBAN:</strong> {{ $subDetails->account_number }}</h4>
                                                        <h4><strong>Bank Code
                                                                (SWIFT/BIC):</strong> {{$subDetails->bank_branch_code}}
                                                        </h4>
                                                        <h4><strong>Wire Transfer
                                                                Number:</strong> {{ $subDetails->account_number1 }}</h4>
                                                        @if($subDetails->country_id == 231)
                                                            <h4><strong>Routing Number (ACH or ABA):</strong> <!--026073150--> 084009519
                                                            </h4>
                                                        @endif
                                                        @if($subDetails->country_id == 230)
                                                            <h4><strong>UK sort code:</strong> 23-14-70</h4>
                                                        @endif
                                                        <h4><strong>Company Address:</strong> {{ $subDetails->address }}
                                                        </h4>

                                                    @else
                                                        <h4><strong>Bank Name:</strong> {{ $subDetails->bank_name }}
                                                        </h4>
                                                        <h4><strong>Account
                                                                Title:</strong> {{ $subDetails->account_title }}</h4>
                                                        <h4><strong>Account# /
                                                                IBAN:</strong> {{ $subDetails->account_number }}
                                                            / {{ $subDetails->account_number1 }} </h4>

                                                        @if($subDetails->bank_branch_code)
                                                            <h4><strong>Bank Code
                                                                    (BSB):</strong>{{$subDetails->bank_branch_code}}
                                                            </h4>
                                                        @endif
                                                        @if($subDetails->address)
                                                            <h4><strong>Company
                                                                    Address:</strong> {{ $subDetails->address }}
                                                            </h4>
                                                        @endif
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>




                                    @endforeach


                                </div>
                            @endif

                            @if($pmode==2)
                                <div class="panel panel-default" style="border:0px solid #fff;">
                                    <h4>
                                        <a href="{{ url('dashboard/paywithcard') }}/{{$amount}}">
                                            <strong>Credit card
                                                <img src="{{ asset('home/images/c3.jpg')}}" width="70px;"
                                                     height="40px;">
                                                <img src="{{ asset('home/images/c2.jpg')}}" width="70px;"
                                                     height="40px;">
                                            </strong>
                                        </a>
                                    </h4>
                                </div>
                            @endif

                            @if($pmode==3)
                                <div class="panel panel-default" style="border:0px solid #fff;">
                                    <h4>
                                        <div style="float:left;">
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="business" value="{{$settings->pay_key}}">
                                                <input type="hidden" name="hosted_button_id"
                                                       value="{{$settings->pay_key2}}">
                                                <input type="hidden" name="cmd" value="_xclick">
                                                <input type="hidden" name="item_name" value="Investment">
                                                <input type="hidden" name="amount" value="{{$amount}}">
                                                <input type="hidden" name="currency_code" value="USD">
                                                <input id="invoice" type="hidden" value="" name="invoice">
                                                <input type="image" src="{{ asset('images/paypal5.PNG')}}" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                            </form>

                                        </div>
                                        <!-- Perfect Money Integration   -->

                                    <!-- 	<form action="https://perfectmoney.is/api/step1.asp" method="POST">

					    <input type="hidden" name="PAYEE_ACCOUNT" value="U21469008">
					    <input type="hidden" name="PAYEE_NAME" value="Test Account1">
					    <div class="sign-up">
					    <input type="hidden"  name="PAYMENT_AMOUNT" value="0.001">
					    </div>
					    <input type="hidden" name="PAYMENT_UNITS" value="USD">
					    <input type="hidden" name="STATUS_URL" value="https://b4uglobal.com/dashboard/PF_status">
					    <input type="hidden" name="PAYMENT_URL" value="https://b4uglobal.com/dashboard/PF_success">
					    <input type="hidden" name="NOPAYMENT_URL" value="https://b4uglobal.com/dashboard/PF_failed">
					    <input type="hidden" name="BAGGAGE_FIELDS" value="ORDER_NUM CUST_NUM">
					    <input type="hidden" name="PAYMENT_ID" value="{{$userid}}">
					    <input type="hidden" name="CUST_NUM" value="2067609">


					    <span style="float: right;"><input class="btn btn-success btn-sm" type="submit" name="PAYMENT_METHOD" value="Perfect-Money">
					  	</span>

				</form> -->

                                        <!--//PerfectMoney -->

                                        <div class="input" style="width: 100%, float: right;">
                                            <div class="btnn" id="copyButtonContainer5" style="">
                                                <!--ac_account_email,ac_sci_name,ac_amount,ac_currency,ac_order_id-->
                                                <form name="send_dollar1" id="send_dollar1" method="post" role="form"
                                                      action="https://wallet.advcash.com/sci/" target="_blank">
                                                    <input type="hidden" name="action" value="send_dollar2">
                                                    <input type="hidden" name="login_id" value="{{$userid}}">

                                                    <input type="hidden" name="ac_account_email"
                                                           value="bitcoins4umy@gmail.com"/>
                                                    <input type="hidden" name="ac_sci_name" value="Bitcoins4u"/>
                                                    <input type="hidden" name="ac_amount" value="{{$amount}}"/>
                                                    <input type="hidden" name="ac_currency" value="USD"/>
                                                    <input type="hidden" name="ac_order_id"
                                                           value="<?php echo(rand(100, 100000)); ?>"/>
                                                    <input type="hidden" name="ac_sign"
                                                           value="1PMiMo7nKz6cp6eAYwX7ZYtcVCdfpkMo1p"/>
                                                    <!-- Optional Fields -->
                                                    <input type="hidden" name="ac_success_url"
                                                           value="https://b4uglobal.com/dashboard"/>
                                                    <input type="hidden" name="ac_success_url_method" value="POST"/>
                                                    <input type="hidden" name="ac_fail_url"
                                                           value="https://b4uglobal.com/dashboard"/>
                                                    <input type="hidden" name="ac_fail_url_method" value="POST"/>
                                                    <input type="hidden" name="ac_status_url"
                                                           value="https://b4uglobal.com/dashboard"/>
                                                    <input type="hidden" name="ac_status_url_method" value="POST"/>
                                                    <!-- <input type="hidden" name="ac_comments" value="Comment" /> -->
                                                    <input type="submit" name="btn_submit" value=""
                                                           style="width: 30%; padding 10px 10px; border-radius: 5px;box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.2);height: 35px;font-size: 14px;line-height: 35px;cursor: pointer;margin-bottom: 3%;margin-top: 1px;color: black;background-color: orange;background-image: url({{ asset('images/advcashlogo.jpg')}});background-size: 100% 100%;background-repeat: no-repeat;">
                                                </form>
                                            </div>
                                        </div>

                                    </h4>
                                </div>
                            @endif
                        @endforeach
                    @elseif($currency != "USD")
                        @include('cryptoAddress')
                    @endif

                @endif
                <div class="clearfix"></div>
                <div class="alert-dismissable">
                    <h4>Contact management at <strong>{{$settings->contact_email}}</strong> for other payment methods.
                    </h4>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
        <script>//document.frmTransaction.submit();</script>
        <form method="post" role="form" id="formABC"
              action="{{ $deposit_mode == "new" ?  route('save_deposit') :  route('reinvest_deposit') }}"
              style="background-color:#fff; padding:20px; margin-top:10px;"
              autocomplete="off" mod="adep">
            <div class="form-content">
                @if($deposit_mode == "new")
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Upload payment proof (Receipt) after payment:</label>
                            <span style="color: red;" class="error"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="file" required class="file-upload-aws" accept="image/*">
                            <input type="hidden" name="fileurl" value="{{ old('fileurl') }}">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if(Auth::user()->Country == 'Pakistan' && $currency=="USD")
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Select the Bank:</label><br>
                                <small>(Where payment deposited)</small>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" name="bank_id" required>
                                    <option value="">Select</option>

                                    @foreach($banklistPak as  $key => $banks)
                                        <option {{ old('bank_id') == $banks->bank_id ? 'selected="selected"' : '' }} value="{{$banks->bank_id}}">{{$banks->bank_name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    <div class="form-group">

                        <div class="col-md-4">
                            <label for="currencyName" data-toggle="tooltip" title="Bank transaction# OR receipt#"
                                   class="control-label">Transaction ID:</label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" id="trans_id" name="trans_id" required
                                   value="{{ old('trans_id') }}"
                                   onkeypress="return isNumberKey(this, event);">
                        </div>
                        <div class="col-md-2">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-4">
                            <label for="select" class="control-label">Payment Mode:</label>
                        </div>

                        <div class="col-md-8">
                            @if($currency=="USD")
                                @foreach($pmodes as $pmode)
                                    @if($pmode==1)
                                        <label class="radio">
                                            <input type="radio" class="radio" name="payment_mode" value="Bank transfer"
                                                   {{ old('payment_mode') == 'Bank transfer' ? 'selected="selected"' : '' }}
                                                   @if($currency=="USD")checked @endif>
                                            <img src="{{ asset('home/images/bank-transfer.png')}}" width="50px;">
                                        </label>
                                    @endif
                                    @if($pmode==2)
                                        <label class="radio">
                                            <input type="radio" class="radio" name="payment_mode" value="Credit card"
                                                    {{ old('payment_mode') == 'Credit card' ? 'selected="selected"' : '' }}

                                            >
                                            <img src="{{ asset('home/images/c3.jpg')}}" width="50px;">
                                        </label>
                                    @endif
                                    @if($pmode==3)
                                        <label class="radio">
                                            <input type="radio" class="radio" name="payment_mode" value="Paypal"
                                                    {{ old('payment_mode') == 'Paypal' ? 'selected="selected"' : '' }}

                                            >
                                            <img src="{{ asset('images/paypal1.png')}}" width="50px;">
                                        </label>
                                    @endif
                                @endforeach
                            @elseif($currency != "USD")
                                <?php $counter1 = 0; ?>
                                @foreach($currencies as $currencyval)

                                    @if($currency == $currencyval->code && $counter1 == 0)
                                        <label class="radio">
                                            <input type="radio" class="radio" name="payment_mode"
                                                   value="{{$currencyval->name}}"
                                                   {{ old('payment_mode') == $currencyval->name ? 'selected="selected"' : '' }}

                                                   @if($currency == $currencyval->code) checked @endif>
                                            <img src="{{asset('images/'.$currencyval->small_code.'.png')}}"
                                                 width="50px;">
                                        </label>
                                        <?php $counter1++; ?>
                                    @else
                                        <?php continue; ?>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-4">

                        </div>
                        <div class="col-md-8">
                            <button type="submit" id="btnSubmit" class="col-md-6 btn btn-default">Submit payment
                            </button>
                            <div class="clearfix"></div>
                            <input type="hidden" name="amount" value="{{$amount}}">

                        <!--<input type="hidden" name="amountusd" value="{{$amountusd}}">-->
                            {{--                    <input type="hidden" name="pay_type" value="{{$pay_type}}">--}}
                            <input type="hidden" name="plan_id" value="{{$plan_id}}">
                            <input type="hidden" name="deposit_mode" value="{{$deposit_mode}}">
                            <input type="hidden" name="currency" value="{{$currency}}">
                            <?php if ($reinvest_type != "") { ?>
                            <input type="hidden" name="reinvest_type" value="{{$reinvest_type}}">
                            <?php } ?>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @endif
                @if($deposit_mode != "new")
                    @if($currency=="USD")
                        <input type="hidden" name="payment_mode" value="Bank transfer">
                    @elseif($currency != "USD")
                        <?php $counter1 = 0; ?>
                        @foreach($currencies as $currencyval)

                            @if($currency == $currencyval->code && $counter1 == 0)

                                <input type="hidden"
                                       {{ old('payment_mode') == $currencyval->name ? 'selected="selected"' : '' }} name="payment_mode"
                                       value="{{$currencyval->name}}">

                                <?php $counter1++; ?>
                            @else
                                <?php continue; ?>
                            @endif
                        @endforeach
                    @endif
                    <div class="form-group">

                        <div class="col-md-4">

                        </div>
                        <div class="col-md-8">
                            <button type="button" id="btnSubmit" class="col-md-6 btn btn-primary o-s">Submit payment
                            </button>

                            <div class="clearfix"></div>

                            <input type="hidden" name="amount" value="{{$amount}}">

                        <!--<input type="hidden" name="amountusd" value="{{$amountusd}}">-->
                            {{--                    <input type="hidden" name="pay_type" value="{{$pay_type}}">--}}
                            <input type="hidden" name="plan_id" value="{{$plan_id}}">
                            <input type="hidden" name="deposit_mode" value="{{$deposit_mode}}">
                            <input type="hidden" name="currency" value="{{$currency}}">
                            <?php if ($reinvest_type != "") { ?>
                            <input type="hidden" name="reinvest_type" value="{{$reinvest_type}}">
                            <?php } ?>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @endif

            </div>
        </form>
    </div>
</div>

@include('footer')

<script>
    // A $( document ).ready() block.
    $(document).ready(function () {
        $("#formABC").submit(function (e) {

            //disable the submit button
            $("#btnSubmit").attr("disabled", true);
            return true;
        });
        let depositmode = "{{$deposit_mode}}";
        if (depositmode != 'new') {
            $("#trans_id").rules("remove", "required");
            $("#payment_mode").rules("remove", "required");
        }
        $('[data-toggle="tooltip"]').tooltip();
        $('#btnSubmit').click(function () {
            let fileInput = $(".file-upload-aws");
            if ($(fileInput).length && !$(fileInput).val()) {
                alert('Choose Deposit Slip');
                return false;
            }
            return true;
        });

    });


    function isNumberKey(txt, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            //Check if the text already contains the . character
            if (txt.value.indexOf('.') === -1) {
                return true;
            } else {
                return false;
            }
        } else {
            // if (charCode > 31 &&
            //     (charCode < 48 || charCode > 57))
            //     return false;
        }
        return true;
    }


</script>
