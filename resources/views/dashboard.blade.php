@include('header')

<style>
    .icon1-background {
        /*color: #ff7676;*/
        color: #ececec;
    }

    .icon2-background {
        /*color: #f8af3c;
            color: #ff00bf;*/
        color: #ececec;
    }

    .icon3-background {
        /*color: #c0ffff; color: #ff00bf; */
        color: #ececec;
    }

    .icon4-background {
        /*color: #2cabe3;color: #ff00bf;*/
        color: #ececec;
    }

    .icon5-background {
        /*color: #40c040;color: #ff00bf;*/
        color: #ececec;
    }

    .icon6-background {
        /*color: #c0c0ff;color: #ff00bf;*/
        color: #ececec;
    }

    .circle-icon2 {
        background: #ffc0c0;
        padding: 30px;
        border-radius: 50%;
    }

    .circle-icon {
        background: #ffc0c0;
        width: 100px;
        height: 100px;
        border-radius: 50%;
        text-align: center;
        line-height: 100px;
        vertical-align: middle;
        padding: 30px;
    }
</style>

ADMIN
<?php
if (isset($ex_rates) && isset($accounts)) {
    $total_usd = $accounts->balance_usd;
    $total_profit_usd = $accounts->profit_usd;
    $total_waiting_profit_usd = $accounts->waiting_profit_usd;
    $total_sold_usd = $accounts->sold_bal_usd;

    $total_btc = $accounts->balance_btc * $ex_rates->rate_btc;
    $total_profit_btc = $accounts->profit_btc * $ex_rates->rate_btc;
    $total_waiting_profit_btc = $accounts->waiting_profit_btc * $ex_rates->rate_btc;
    $total_sold_btc = $accounts->sold_bal_btc * $ex_rates->rate_btc;

    $total_rsc = $accounts->balance_rsc * $ex_rates->rate_rsc;
    $total_profit_rsc = $accounts->profit_rsc * $ex_rates->rate_rsc;
    $total_waiting_profit_rsc = $accounts->waiting_profit_rsc * $ex_rates->rate_rsc;
    $total_sold_rsc = $accounts->sold_bal_rsc * $ex_rates->rate_rsc;

    $total_usdt = $accounts->balance_usdt * $ex_rates->rate_usdt;
    $total_profit_usdt = $accounts->profit_usdt * $ex_rates->rate_usdt;
    $total_waiting_profit_usdt = $accounts->waiting_profit_usdt * $ex_rates->rate_usdt;
    $total_sold_usdt = $accounts->sold_bal_usdt * $ex_rates->rate_usdt;

    $total_eth = $accounts->balance_eth * $ex_rates->rate_eth;
    $total_profit_eth = $accounts->profit_eth * $ex_rates->rate_eth;
    $total_waiting_profit_eth = $accounts->waiting_profit_eth * $ex_rates->rate_eth;
    $total_sold_eth = $accounts->sold_bal_eth * $ex_rates->rate_eth;

    $total_bch = $accounts->balance_bch * $ex_rates->rate_bch;
    $total_profit_bch = $accounts->profit_bch * $ex_rates->rate_bch;
    $total_waiting_profit_bch = $accounts->waiting_profit_bch * $ex_rates->rate_bch;
    $total_sold_bch = $accounts->sold_bal_bch * $ex_rates->rate_bch;

    $total_ltc = $accounts->balance_ltc * $ex_rates->rate_ltc;
    $total_profit_ltc = $accounts->profit_ltc * $ex_rates->rate_ltc;
    $total_waiting_profit_ltc = $accounts->waiting_profit_ltc * $ex_rates->rate_ltc;
    $total_sold_ltc = $accounts->sold_bal_ltc * $ex_rates->rate_ltc;

    $total_xrp = $accounts->balance_xrp * $ex_rates->rate_xrp;
    $total_profit_xrp = $accounts->profit_xrp * $ex_rates->rate_xrp;
    $total_waiting_profit_xrp = $accounts->waiting_profit_xrp * $ex_rates->rate_xrp;
    $total_sold_xrp = $accounts->sold_bal_xrp * $ex_rates->rate_xrp;

    $total_dash = $accounts->balance_dash * $ex_rates->rate_dash;
    $total_profit_dash = $accounts->profit_dash * $ex_rates->rate_dash;
    $total_waiting_profit_dash = $accounts->waiting_profit_dash * $ex_rates->rate_dash;
    $total_sold_dash = $accounts->sold_bal_dash * $ex_rates->rate_dash;

    $total_zec = $accounts->balance_zec * $ex_rates->rate_zec;
    $total_profit_zec = $accounts->profit_zec * $ex_rates->rate_zec;
    $total_waiting_profit_zec = $accounts->waiting_profit_zec * $ex_rates->rate_zec;
    $total_sold_zec = $accounts->sold_bal_zec * $ex_rates->rate_zec;

    /* $totle_dsh 				= $accounts->balance_dsh * $ex_rates->rate_dsh;
    $totle_profit_dsh 			= $accounts->profit_dsh * $ex_rates->rate_dsh;
    $totle_waiting_profit_dsh 	= $accounts->waiting_profit_dsh * $ex_rates->rate_dsh;
    $totle_sold_dsh 			= $accounts->sold_bal_dsh * $ex_rates->rate_dsh; */

    $totalDeposits = $total_usd + $total_btc + $total_rsc + $total_eth + $total_bch + $total_ltc + $total_xrp + $total_dash + $total_zec + $total_usdt;  //+ $totle_dsh
    $totalProfit = $total_profit_usd + $total_profit_btc + $total_profit_rsc + $total_profit_eth + $total_profit_bch + $total_profit_ltc + $total_profit_xrp + $total_profit_dash + $total_profit_zec + $total_profit_usdt; //+ $total_profit_dsh
    $totalWaitProfit = $total_waiting_profit_usd + $total_waiting_profit_btc + $total_waiting_profit_rsc + $total_waiting_profit_eth + $total_waiting_profit_bch + $total_waiting_profit_ltc + $total_waiting_profit_xrp + $total_waiting_profit_dash + $total_waiting_profit_zec + $total_waiting_profit_usdt; // + $totle_waiting_profit_dsh
    $totalSolds = $total_sold_usd + $total_sold_btc + $total_sold_rsc + $total_sold_eth + $total_sold_bch + $total_sold_ltc + $total_sold_xrp + $total_sold_dash + $total_sold_zec + $total_sold_usdt; //+ $total_sold_dsh
    $final_Profit = $totalProfit + $totalWaitProfit;
    $total_deduct = $accounts->total_deduct;
    if ($total_deduct > 0) {
        $final_Profit = $final_Profit - $total_deduct;
    }
} else {
    $totalDeposits = 0;
    $totalProfit = 0;
    $totalWaitProfit = 0;
    $totalSolds = 0;
    $final_Profit = 0;
}

$headers = apache_request_headers();
$headers = $headers['Host'];
//echo $totalSolds 	;
?>
<!-- /User Details Modal -->
<a href="javascirpt:void(0)" data-toggle="modal" id="noticeLink" data-target="#noticeModel"></a>
<!--Notice Modal -->
@if($user->show_donation_popup == 0)
    <div id="noticeModel" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Please download the app by clicking on picture.</h4> <br>
                </div>

                <div class="modal-body">
                    <a href="https://play.google.com/store/apps/details?id=io.rscoins.network"><img style="width: 100%;"  src="{{url('images/rsc_gold.jpg')}}"></a>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('hidePopUp') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">Don't show this again.</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
<!-- /Notice Modal -->
<!-- main content start-->
<div id="page-wrapper" style="padding-left: 5px;">
    <div class="main-page">

    {{-- @if($user->show_donation_popup == 0)
    <div class="row-one" style="margin-top:10px;">
        <div class="alert alert-warning alert-dismissible">
            <form action="{{ route('donations.donate_odp.cancel') }}" method="POST">
                @csrf
                <button type="submit" class="close" >×</button>
            </form>
            <form action="{{ route('donations.donate_odp') }}" method="POST">
                @csrf
                <button type="submit" style="background: none; border: none">Do you want to donate your one day profit to COVID-19 peoples?</button>
            </form>
        </div>
    </div>
    @endif --}}

    <!-- Currency Rates -->
        @if (env('is_allowed_for_long_ip', '0') <> 1)
            @if(!app('request')->session()->get('back_to_admin') && $settings->show_ad == 0 && $headers == 'b4uglobal.com' && \Illuminate\Support\Facades\Auth::user()->type == 0)
                <div id="adsense" class="row-one" style="height:100px;  max-height:100px; background-color: #fff;">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- dashboard -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-8618087093269654"
                         data-ad-slot="1063636867"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            @endif
        @endif

        <div class="row-one">

            <div class="col-md-2 col-sm-6 rp t-b" style="font-size:18px;">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>Today's Exchange Rates</label></small>
                    <br>
                    <small>{{ \Carbon\Carbon::now()->format('j F Y') }}</small>
                </h4>
            </div>
             <div class="col-md-2 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_rsc}}</label></small>
                    <br>
                    <small>RSCoin</small>
                </h4>
                <!---h4 style="margin-top:5px; text-align:center;"><div class="loader"></div><br><small>Attracted Funds</small></h4--->
            </div>
            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_usdt}}</label></small>
                    <br>
                    <small>Tether</small>
                </h4>
                <!---h4 style="margin-top:5px; text-align:center;"><div class="loader"></div><br><small>Attracted Funds</small></h4--->
            </div>
            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_btc}}</label></small>
                    <br>
                    <small>Bitcoin</small>
                </h4>
                <!---h4 style="margin-top:5px; text-align:center;"><div class="loader"></div><br><small>Attracted Funds</small></h4--->
            </div>

           
            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_bch}}</label></small>
                    <br><small>Bitcoin Cash</small>
                </h4>
            </div>

            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_eth}}</label></small>
                    <br><small>Ethereum</small>
                </h4>
            </div>

            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_ltc}}</label></small>
                    <br><small>Litecoin</small>
                </h4>
            </div>

            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_xrp}}</label></small>
                    <br><small>Ripple</small>
                </h4>
            </div>

            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_dash}}</label></small>
                    <br><small>Dash</small>
                </h4>
            </div>

            <div class="col-md-1 col-sm-3 rp t-b">
                <h4 style="margin-top:5px; text-align:center;">
                    <small><label>$ {{$ex_rates->rate_zec}}</label></small>
                    <br><small>Zcash</small>
                </h4>
            </div>

        </div>

        <div class="clearfix"></div>


        @if(Session::has('status'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i> {{ Session::get('status') }}
                    </div>
                </div>
            </div>
        @endif

        <?php /* if(isset($history))
        {   ?>
        @if( Auth::user()->u_id  != "B4U0001")

        <div>
            <p>Last Login:  {{$history->ip}} || {{$history->location}} || {{$history->created_at->diffForHumans()}}</p>
            </div>

            @endif
        <?php } */?>


        <!-- <div class="row">
            <a class="btn btn-success" style="float: right;" href="{{ route('donations') }}" style="color: #FFF">Donate
                in COVID-19 Relief Fund</a>
        </div> -->

        <div class="row">
           


            <div class="row">
                     {{--  @if(Auth::user()->plan >= 2)
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span style="text-align: center">  <i class="fa fa-info-circle"></i> <b >Important Alert! </b><br>It is to notify you that we are changing our policy for plan limits.  Please fulfill the following conditions before <b>(30-May-2021)</b> to keep your account plan to {{ucfirst(strtolower($uplan->name))}}. <br>
                            Otherwise your account would be blocked or downgraded. <br></span><br>
                        <p style="text-align: left;">
                            <b>Conditions for  {{ucfirst(strtolower($uplan->name))}}:</b><br>
                            1.  Your minimum personal investment should be equal to: <b>{{$newPlanDetails->personal_min}}</b><br>
                            2. Your minimum structural investment (Attracted Fund) should be equal to:  <b>{{$newPlanDetails->structural_min}}</b> with minimum Personal Investment of:  <b>{{$newPlanDetails->structural_personal_min}}</b> <br>
                            <small>(Note: You have to fulfill any one of the above conditions)</small>
                         </p>
                        </div>
                      @endif--}}

                @if( Auth::user()->msg_bit  == 1)
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p>{{Auth::user()->msg}}</p>
                    </div>
                @endif

                @if( Auth::user()->bank_name  == "" || Auth::user()->account_name  == "" || Auth::user()->account_no  == "" )

                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p>Some details are missing in your bank details. Please Update your profile</p>
                    </div>
                @endif
                    @include('messages')

                @if( Auth::user()->phone_no  == "")
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p>Your Phone No. is still missing. Please Update your profile</p>
                    </div>
                @endif

                @if(Auth::user()->plan >= 2 && Auth::user()->is_call_sub == 0 )
                    <a href="#"   data-toggle="modal" data-target="#CallSubModal1"  class="btn btn-sm btn-primary" style="float: right">Subscribe for call package</a>
                @elseif(Auth::user()->is_call_sub == 1)
                    <p style="float: right">Your call package is valid till: <b><u>{{date('F d, Y', strtotime(Auth::user()->call_sub_at . '+1 Month'))}}</u></b></p>
                @endif
                 <br>
            <!--   {{-- @if( Auth::user()->is_email_pin  == 0) --}}
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>Your account is not secure. Please active 2FA.</p>
				</div>
            {{-- @endif --}} -->


            <!--  @if(!is_null(Auth::user()->rank) && Auth::user()->rank != 'NULL')
                <div class="row" style="margin-left: 10px; margin-bottom: 10px;">
                    <div class="col-md-12 col-sm-6">
                        <p>Your Rank</p>
                        <div class="">
                            <img style="width:50px; margin-left: 15px;"
                                 src="{{ asset('/images/rank_'. Auth::user()->rank .'.png') }}">
                            </div>
                            <div>
                                <strong style="margin-top:10px;"><p>B4U {{Auth::user()->rank}}</p></strong>
                            </div>
                        </div>
                    </div>
                @endif -->
                @if(app('request')->session()->get('back_to_admin'))
                    <a href="{{route('upPlanAF')}}" class="btn btn-default" onclick="clickAndDisable(this)">Update Plan and Attracted Funds</a>
                @endif
                <br>
                <div class="row-one" style="margin-top:10px; ">
                    <!--	// Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
                    <!--Total Sub Users -->
                    <div class="col-md-3 col-sm-6 rp t-b ">
                        <sup style="color:black;">Your ID
                            @if(app('request')->session()->get('back_to_admin'))
                                ({{ Auth::user()->id }})
                            @endif
                        </sup>
                        <h4 style="color:#490342;font-weight:bold;">
                            <a data-toggle="modal" data-target="#UserDetailsModal" href="#"
                               onclick="viewUserDetailsFunc({{Auth::user()->id}})">{{Auth::user()->u_id}}</a>
                        </h4>
                    </div>

                    <div class="col-md-3 col-sm-6 rp t-b">
                        <sup style="color:black;"> Investment Plan </sup>
                        <h4><a href="#" id="current_plan">
                                <div class="loader"></div>
                            </a></h4>
                    </div>
                <!---div class="col-md-3 col-sm-6 rp t-b">
				<h4><div style="backgound-img;"url{{asset('images/'.$uplan->img_url)}}"";><a href="{{url('dashboard/plans')}}">{{$uplan->name}}</a></h4><br>
				</div--->
                    <!--$last_profit-Div -->
                    <div class="col-md-3 col-sm-6 rp t-b" style="min-height: 74px">

                        @if(Auth::user()->type == 1)
                            <sup style="color:black;">Your Role </sup>
                            <br>
                            <h4 style="color:black;">Supper Admin</h4>
                        @else
                            <sup style="color:black;">Parent ID </sup>
                            <br>
                            <h4 style="color:black;">
                                <a data-toggle="modal" data-target="#ParentDetailsModal" href="#"
                                   onclick="viewUserDetailsFuncParent({{Auth::user()->id}})">{{Auth::user()->parent_id}}</a>
                            </h4>
                        @endif
                    </div>

                    <div class="col-md-3 col-sm-6 rp t-b">
                        @if(Auth::user()->type == 1)
                            <sup style="color:black;"><a href="{{url('dashboard/partners')}}"> Your Partners </a></sup>
                        @else
                            <sup style="color:black;"><a href="{{url('dashboard/partners')}}"> Your Partners </a></sup>
                        @endif
                        <br>
                        <h4 style="color:green;" id='mtotal_sub_users'>
                            <div class="loader"></div>
                        </h4>
                    </div>
                    <!--$last_bonus-->
                </div>

                <div class="clearfix"></div>

            <!-- <div class="col-lg-4 col-md-4" >
				 <h4><i class="fa fa-bell"></i> {{$settings->update}} </h4>
			</div> -->

                <div class="col-lg-4 col-md-4 col-sm-5" style="margin-bottom:5px; padding:0px;"></div>
                <div class="col-lg-4 col-md-4 col-sm-4" style="margin-bottom:5px; padding:0px;"></div>
            </div>




            <div class="row-one" style="margin-top:5px; text-align:center;">

                <!-- Deposit Icon -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    <div>
                        <h4>
						<span class="fa-stack">  <!--fa-lg  fa-2x fa-3x fa-4x-->
							<i class="fa fa-circle fa-stack-2x icon1-background"></i>
							<i class="fa-stack-1x fa">&#xf0d6;</i>
                            <!-- <i class="fa fa-bicycle fa-5x circle-icon"/>-->
						</span><br>

                            <strong style="margin-top:4px;">
                                @if(Auth::user()->type == 1)
                                    <a href="{{url('dashboard/mdeposits')}}"> DEPOSITED </a>
                                @else
                                    <a href="{{ url('dashboard/deposits')}}"> DEPOSITED </a>
                                @endif
                            </strong>
                        </h4>
                    </div>

                    <div>
                        <h3 style="margin-top:8px;" title="Your account balance">
                            @if(isset($totalDeposits) && $totalDeposits > 0)
                                {{$settings->currency}}{{ number_format($totalDeposits, 2, '.', ',')}} <i
                                        class="fa fa-arrow-up" style="color:green; font-size:12px;"></i>
                            @else
                                {{$settings->currency}}0
                            @endif
                        </h3>
                    </div>
                </div>

                <!-- Profit Icon -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    <div>
                        <h4>
						<span class="fa-stack">  <!--fa-lg  fa-2x fa-3x fa-4x-->
							<i class="fa fa-circle fa-stack-2x icon2-background"></i>
							<i class="fas fa-chart-line fa-stack-1x"></i>
						</span><br>

                            <strong style="margin-top:4px;">
                                <a href="{{url('dashboard/daily_profit')}}">PROFIT</a>

                                @if($accounts->is_manual_verified == 1 && $accounts->double_verified == 0)
                                    <i style="color: green;" class="fa fa-check" aria-hidden="true"></i>
                                @elseif($accounts->double_verified == 1)
                                    <i style="color: green;" class="fas fa-check-double"></i>
                                @endif
                            </strong>
                        </h4>
                    </div>

                    <?php
                    if (isset($accounts->profit_usd) || isset($accounts->waiting_profit_usd)) {
                        $usdProfit = $accounts->profit_usd + $accounts->waiting_profit_usd;
                    } else {
                        $usdProfit = 0;
                    }
                     if (isset($accounts->profit_rsc) || isset($accounts->waiting_profit_rsc)) {
                        $rscProfit = $accounts->profit_rsc + $accounts->waiting_profit_rsc;
                    } else {
                        $rscProfit = 0;
                    }
                    if (isset($accounts->profit_btc) || isset($accounts->waiting_profit_btc)) {
                        $btcProfit = $accounts->profit_btc + $accounts->waiting_profit_btc;
                    } else {
                        $btcProfit = 0;
                    }
                    if (isset($accounts->profit_eth) || isset($accounts->waiting_profit_eth)) {
                        $ethProfit = $accounts->profit_eth + $accounts->waiting_profit_eth;
                    } else {
                        $ethProfit = 0;
                    }
                    if (isset($accounts->profit_bch) || isset($accounts->waiting_profit_bch)) {
                        $bchProfit = $accounts->profit_bch + $accounts->waiting_profit_bch;
                    } else {
                        $bchProfit = 0;
                    }
                    if (isset($accounts->profit_ltc) || isset($accounts->waiting_profit_ltc)) {
                        $ltcProfit = $accounts->profit_ltc + $accounts->waiting_profit_ltc;
                    } else {
                        $ltcProfit = 0;
                    }
                    if (isset($accounts->profit_xrp) || isset($accounts->waiting_profit_xrp)) {
                        $xrpProfit = $accounts->profit_xrp + $accounts->waiting_profit_xrp;
                    } else {
                        $xrpProfit = 0;
                    }

                    if (isset($accounts->profit_dash) || isset($accounts->waiting_profit_dash)) {
                        $dashProfit = $accounts->profit_dash + $accounts->waiting_profit_dash;
                    } else {
                        $dashProfit = 0;
                    }

                    if (isset($accounts->profit_zec) || isset($accounts->waiting_profit_zec)) {
                        $zecProfit = $accounts->profit_zec + $accounts->waiting_profit_zec;
                    } else {
                        $zecProfit = 0;
                    }
                    /* if(isset($accounts->profit_dsh) || isset($accounts->waiting_profit_dsh))
                    {
                        $dshProfit =  $accounts->profit_dsh + $accounts->waiting_profit_dsh;
                    }else{
                        $dshProfit = 0;
                    } */
                    ?>

                    <div>
                        <h3 style="margin-top:8px; text-align:center;" title="Your profit">
                            @if(isset($final_Profit))

                                {{$settings->currency}}{{number_format($final_Profit , 2, '.', ',')}} <i
                                        class="fa fa-arrow-up" style="color:green; font-size:12px;"></i>
                            @else
                                {{$settings->currency}}0
                            @endif
                        </h3>
                    </div>
                </div>

                <!-- Ref. Bonus Icon -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    <div>
                        <h4>
						<span class="fa-stack">  <!--fa-lg  fa-2x fa-3x fa-4x-->
						<i class="fa fa-circle fa-stack-2x icon3-background"></i>
						<i class="fa fa-sitemap fa-stack-1x"></i>
						</span><br>
                            <strong style="margin-top:4px;">
                                <a href="{{url('dashboard/daily_bonus')}}"> REF. BONUS</a>
                                @if($accounts->is_manual_verified == 1 && $accounts->double_verified == 0)
                                    <i style="color: green;" class="fa fa-check" aria-hidden="true"></i>
                                @elseif($accounts->double_verified == 1)
                                    <i style="color: green;" class="fas fa-check-double"></i>
                                @endif
                            </strong>
                        </h4>
                    </div>
                    <div>
                        <h3 style="margin-top:8px; text-align:center;" title="Your reference bonus">

                            {{$settings->currency}} {{number_format($accounts->reference_bonus , 2, '.', ',')}}

                        </h3>
                    </div>
                </div>

                <!-- Withdrawal Icon -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    <div>
                        <h4>
						<span class="fa-stack">
						<i class="fa fa-circle fa-stack-2x icon3-background"></i>
						<i class="fas fa-wallet fa-stack-1x"></i>
						</span><br>
                            <strong style="margin-top:4px;">
                                <a href="{{url('dashboard/withdrawals')}}">Withdrawals</a>
                            </strong>
                        </h4>
                    </div>
                    <div>
                        <h3 style="margin-top:8px; text-align:center;" title="Your reference bonus"
                            id="user_withdrawals">
                            <div class="loader"></div>
                        <!-- @if(isset($wAmount))
                            {{$settings->currency}} {{number_format($wAmount , 2, '.', ',')}} <i class="fa fa-arrow-up" style="color:green; font-size:12px;"></i>
						@else
                            {{$settings->currency}} 0
						@endif -->
                        </h3>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row-one" style="margin-top:10px;">
                <!--	// Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
                <!--Total Sub Users -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    <!---h4 style="color:green;"><div class="loader"></div><small>Attracted Funds</small></h4--->
                    <h4 style="margin-top:5px; text-align:center;">
                        <label id="attractive_funds">
                            <div class="loader" style="margin: 1px auto auto 1px;"></div>
                        </label>
                        <br><a href="{{url('dashboard/partners')}}"><small>Attracted Funds</small></a>
                    </h4>
                    <!---h4 style="margin-top:5px; text-align:center;"><div class="loader"></div><br><small>Attracted Funds</small></h4--->
                </div>

                <!--$last_profit-Div -->
                <div class="col-md-3 col-sm-6 rp t-b">
                    @if(isset($accounts->latest_profit))
                        <h4 style="margin-top:5px; text-align:center;">
                            @if($latestprofit_new[0]->latestprofit!='')
                                {{$settings->currency}}{{number_format($latestprofit_new[0]->latestprofit , 4, '.', ',')}}
                            @else
                                {{$settings->currency}}{{number_format($accounts->latest_profit , 4, '.', ',')}}
                            @endif

                            <i class="fa fa-arrow-up" style="color:green; font-size:12px;"></i><br><small>Last
                                Profit</small></h4>
                    @else
                        <h4 style="margin-top:5px; text-align:center;">{{$settings->currency}}0 <i
                                    class="fa fa-arrow-up"
                                    style="color:green; font-size:12px;"></i><br><small>Last
                                Profit</small></h4>
                    @endif
                </div>

                <!--$last_bonus-->
                <div class="col-md-3 col-sm-6 rp t-b">
                    @if(isset($accounts->latest_bonus))
                        <h4 style="margin-top:5px; text-align:center;">{{$settings->currency}}{{number_format($accounts->latest_bonus , 4, '.', ',')}}
                            <i class="fa fa-arrow-up" style="color:green; font-size:12px;"></i><br><small> Last
                                Bonus</small></h4>
                    @else
                        <h4 style="margin-top:5px; text-align:center;">{{$settings->currency}}0<i class="fa fa-arrow-up"
                                                                                                  style="color:green; font-size:12px;"></i><br><small>Last
                                Bonus</small></h4>
                    @endif
                </div>

                <div class="col-md-3 col-sm-6 rp t-b">
                    <h4 style="margin-top:5px; text-align:center;">
                        @if(isset($totalSolds) && $totalSolds > 0)
                            {{$settings->currency}}{{ number_format($totalSolds, 2, '.', ',')}}
                        @else
                            {{$settings->currency}} 0
                        @endif
                        <br><small> Sold Balance </small>
                    </h4>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row-one" style="margin-top:0px;">
                @include('tables')
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div
<!-- User Details Modal -->
<div id="UserDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="ucontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>
<!-- Call Subscription Modal -->
<div id="CallSubModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="sub_content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Subscribe</h4>
            </div>
            <div class="modal-body" style="text-align: center; margin-bottom: 30px;">
             {{--   <div class="col-sm-8 col-offset-sm-4"><label class="modal-title"></label></div>--}}
            <form method="post" action="{{route('subcribeCallPckg')}}">
                @csrf
                <p>You are going to subscribe call package worth $50/month. Click on proceed to continue. </p>
                <small style="color: red">($50 will be deducted immediately from you Profit or Bonus.)</small>
                <br><br>
                <input type="submit" class="btn btn-primary btn-sm" value="Proceed" style="float: right">
            </form>
            </div>
            <div class="clearfix"></div>
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<div id="ParentDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ucontent1">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /User Details Modal -->
<a href="javascirpt:void(0)" data-toggle="modal" id="noticeLink" data-target="#noticeModel"></a>
<!--Notice Modal -->

<!-- /Notice Modal -->
<script>

    $(document).ready(function () {


        function clickAndDisable(link) {

            link.onclick = function (event) {
                event.preventDefault();
            }
        }


        $("#formABC").submit(function (e) {

            //disable the submit button
            $("#btnSubmit").attr("disabled", true);
            return true;

        });

        $("#noticeLink").trigger("click");

        fetch("{{route('plan_json2')}}")
            .then(function (response) {
                return response.json()
            }).then(function (data) {
            $("#mtotal_sub_users").html(data.count_total_partners);
            $("#attractive_funds").html(data.total_attractive_funds);
            $("#current_plan").html(data.plan_name);
        });

        // get withdrawls
        $.get("{{route('withdrawals.ajax')}}", function (data, status) {
            $("#user_withdrawals").html(data)
        });


    });

    function viewUserDetailsFuncParent(id) {
        //alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewUserDetailsParent',
            data: {id: id, "_token": "{{ csrf_token() }}",},
            //  cache: false,
            success: function (response) {
                response = JSON.parse(response);
                $(".ucontent1").html(response);
            }, error: function (response) {
                // alert('Be patient, System is under maintenance');
                //$("#CallSubModal").modal('show');
            }
        });
    }

    function viewUserDetailsFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{ route('view_user_details') }}',
            data: {id: id, "_token": "{{ csrf_token() }}",},
            success: function (response) {
                response = JSON.parse(response);
                $("body #ucontent").html(response);
            }, error: function (response) {
                // alert('Be patient, System is under maintenance');
                //$("#CallSubModal").modal('show');
            }
        });
    }

</script>

@include('footer')
