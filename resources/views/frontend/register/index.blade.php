@extends('frontend.layout.app')

<!-- @section('title', 'Page Title') -->

@section('content')
<?php

    $url  = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    
    if (strpos($url, '?') !== false) {
        $ur2 = "exist";
    } else {
        $ur2 = "notexist";
    }
    
    if ($ur2 == "exist") {
        $urlArr = explode("?", $url);
        if (isset($urlArr) && isset($urlArr[1])) {
            $parentId = trim($urlArr[1]);
        } else {
            $parentId = "B4U0001";
        }
    } else {
        $parentId = "B4U0001";
    }
?>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Page Title Area
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="page-title-area bg-primary" style="background-image: url('public/home/images/shape/shape-dot1.png')">
            <div class="shape-group">
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
            </div><!-- /.shape-group -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="page-header-content text-center">
                            <div class="page-header-caption">
                                <h2 class="page-title">My Account</h2>
                            </div><!--~~./ page-header-caption ~~-->
                            <div class="breadcrumb-area">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>  
                                    <li class="breadcrumb-item active">Sign Up</li>
                                </ol>
                            </div><!--~~./ breadcrumb-area ~~-->
                        </div><!--~~./ page-header-content ~~-->
                    </div>
                </div>
            </div><!--~~./ end container ~~-->
        </div><!--~~./ end page title area ~~--> 

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            User Signin Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="user-signup-block ptb-120">
            <div class="bg-left bg-image" style="background-image: url('public/home/images/shape/signup-shape.png')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="sing-up-mockup">
                            <img src="{{asset('public/home/images/others/signup-mockup.png')}}" alt="Mockup">
                        </div>
                    </div><!--/.col-lg-2-->
                    <div class="col-lg-6">
                        <div class="user-register-area">

                    @if(Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-warning"></i> {{ Session::get('message') }}
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($errors->has('g-recaptcha-response') )
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                                </div>
                            </div>
                        </div>

                    @endif
                            <div class="form-content">
                                <div class="form-header">
                                    <h4 class="form-subheading">Sign Up here</h4>
                                    <h2 class="heading">Welcome To B4U Global</h2>
                                </div>
                                <form class="default-form signup-form" method="POST" action="{{ route('register') }}" autocomplete="off" id="register_form">
                                {{csrf_field()}}    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">

                                                @if ($errors->has('name'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="name">Name</label>
                                                <input style="background:transparent; color:#555;" class="form-controller" id="name" placeholder="ENTER YOUR FULL NAME" name="name" type="text" value="{{ old('name') }}" required autofocus>
                                    
                                            </div><!--/.form-group-->
                                        </div>
                                       
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                @if ($errors->has('email'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="email">Email Address</label>
                                                <input style="background:transparent; color:#555;" class="form-controller" id="email" placeholder="ENTER YOUR EMAIL" name="email" type="email" value="{{ old('email') }}" required>
                                            </div><!--/.form-group-->
                                        </div>
                                         <div class="col-lg-6">
                                            <div class="form-group">
                                                @if ($errors->has('phone_no'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('phone_no') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="phone">Phone</label>
                                                <input style="background:transparent; color:#555;" type="tel" class="form-controller" id="phone_no" placeholder="" name="phone_no" type="text" value="{{ old('phone_no') }}" required autocomplete="disabled">
                                                <input name="country" id="country_name" type="hidden" autocomplete="off" >
                                               
                                                <span id="error-msg" class="hide" style="color:red"></span>
                                            </div><!--/.form-group-->
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                @if ($errors->has('password'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="pass">Password</label>
                                                <input style="background:transparent; color:#555;" class="form-controller"  name="password" placeholder="ENTER PASSWORD" type="password" required autocomplete="off">
                                            </div><!--/.form-group-->
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="con_pass"> Confirm Password</label>
                                                <input style="background:transparent; color:#555;" class="form-controller"  placeholder="RE-ENTER YOUR PASSWORD" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" required autocomplete="off">
                                            </div><!--/.form-group-->
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">

                                                @if ($errors->has('parent_id'))
                                                    <span class="help-block" style="color:red">
                                                        <strong>{{ $errors->first('parent_id') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="parent_id"> Parent ID</label>
                                                <input id="parent_id_input"  class="form-controller" @if(isset($parentId)) value="{{$parentId}}" @else  value="{{old('parent_id')}}" @endif name="parent_id" type="text" placeholder="ENTER YOUR Parent ID, If Don't Know Any Use Our Default 'B4U0001'"style="background:transparent; color:#555;">
                                                <span id="invalid-prnt"></span>
                                    
                                            </div><!--/.form-group-->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block" style="color:red">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                    {!! Captcha::display([]) !!}
                                    </div>

                                    <div class="login-form-remember"> 
                                        <label><input name="sign" type="checkbox" required><span><p> Accept <a href="{{ route('terms2') }}">Terms and Conditions</a> and 
                                    <a href="{{ route('privacy2') }}">Privacy Policy</a></p></span></label>
                                    </div>
                                   
    
                                    <div class="form-btn-group">
                                        <div class="form-login-area">
                                            <button type="submit" class="btn btn-default btn-primary">
                                                Sign Up
                                            </button>
                                        </div>
                                        <div class="login-form-register-now">
                                            Already have an account ? <a class="btn-register-now" href="{{ url('/signin') }}">Sign In</a>
                                        </div>
                                    </div>
                                  
                                </form>  
                            </div>
                        </div><!--~./ end user register area ~--> 
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end user signup block ~~-->
<script>
   
    $("#register_form").disableAutoFill();
    var input = document.querySelector("#phone_no");
    var errorMsg = document.querySelector("#error-msg");
    var validMsg = document.querySelector("#valid-msg");
      // here, the index maps to the error code returned from getValidationError - see readme
var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

    var iti =window.intlTelInput(input, {
        nationalMode: false,
        autoHideDialCode: true,
      // autoPlaceholder: "off",
        autoPlaceholder: "polite",
        formatOnDisplay: true,
        placeholderNumberType: "MOBILE",
        preferredCountries: ['us', 'pk', 'my', 'gb'],
        utilsScript: "{{ asset('home/flag_phone_input/js/utils.js')}}",
    });  

    var countryData = iti.getSelectedCountryData().name;
    // console.log(iti.getSelectedCountryData());
    // listen to the phone dropdown for changes
    input.addEventListener('countrychange', function() {
        var countryData = iti.getSelectedCountryData().name;
        document.querySelector("#country_name").value= countryData.split("(")[0];
    });
    document.querySelector("#country_name").value= countryData.split("(")[0];

    var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");

        // $("#register_btn").attr("disabled",false);
    };
    // on blur: validate
    input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                // validMsg.classList.remove("hide");
            } else {
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
              
            }
        }
    });
    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);
    var parent_input = document.querySelector("#parent_id_input");
    parent_input.addEventListener('blur', parentvalidate);
        function parentvalidate(){
           
            if(parent_input.value != '' && parent_input.value != "B4U0001")
            {
                jQuery.ajax({
                    type: "POST",
                    url: '{{ \Illuminate\Support\Facades\URL::to('validate/parentid') }}',
                    data: {b4uid: $("#parent_id_input").val(), "_token": "{{ csrf_token() }}",},
                    //  cache: false,
                    dataType: "json",
                    success: function (response) {
                        if(!response.exists){
                            $("#invalid-prnt").html("You entered invalid parent id. Default parent id has been set.");
                            $("#invalid-prnt").css("color","red");
                            $("#parent_id_input").val("B4U0001");
                        }else
                        {
                            $("#invalid-prnt").html("✓ Valid");
                            $("#invalid-prnt").css("color","green");
                        }
                    }, error: function (response) {
                        $("#invalid-prnt").html("Invalid Parent ID");
                    }
                });
            }
            
        }
    $("#parent_id_input").inputmask({
            regex: "^([B]{1}[4]{1}[U]{1}[0]{1}[0-9]{100})$",
            greedy : false,
            placeholder : ''
    });
    
  </script>        
@stop