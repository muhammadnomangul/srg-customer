@extends('frontend.layout.app')

<!-- @section('title', 'Page Title') -->

@section('content')
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Page Title Area
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="page-title-area bg-primary" style="background-image: url('public/home/images/shape/shape-dot1.png')">
            <div class="shape-group">
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
                <div class="shape"></div>
            </div><!-- /.shape-group -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="page-header-content text-center">
                            <div class="page-header-caption">
                                <h2 class="page-title">My Account</h2>
                            </div><!--~~./ page-header-caption ~~-->
                            <div class="breadcrumb-area">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>  
                                    <li class="breadcrumb-item active">Sign In</li>
                                </ol>
                            </div><!--~~./ breadcrumb-area ~~-->
                        </div><!--~~./ page-header-content ~~-->
                    </div>
                </div>
            </div><!--~~./ end container ~~-->
        </div><!--~~./ end page title area ~~--> 

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            User Signin Block
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="user-signin-block ptb-120">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="sing-in-mockup md-mrb-60">
                            <img src="{{asset('public/home/images/others/signin-mockup.png')}}" alt="Mockup">
                        </div>
                    </div><!--/.col-lg-2-->
                    <div class="col-lg-6">
                        <div class="user-register-area">
                            <div class="form-content">
                                <div class="form-header">
                                    <h4 class="form-subheading">Sign in here</h4>
                                    <h2 class="heading">Welcome To B4U Global</h2>
                                </div>
                                <!-- Logo Starts -->
                                <div class="text-center">

                                <!-- @if(isset($settings->logo))

                                <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
                                    <img id="logo" class="img-responsive" src="{{ asset('images/b4u-logo.png')}}" alt="logo" style="margin-left: auto; margin-right: auto;">
                                </a>
                                @else
                                    <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                                        <img src="{{ asset('images/b4u-logo.png') }}" style="margin-left: auto; margin-right: auto;">
                                    </a>
                                @endif -->
                                </div>
                                <!-- Logo Ends -->
                                @if(Session::has('message'))
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <i class="fa fa-warning"></i> {{ Session::get('message') }}
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if ($errors->has('g-recaptcha-response') )
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-warning"></i> {{$errors->first('g-recaptcha-response') ?: $errors->first('g-recaptcha-response') }}
                                            </div>
                                        </div>
                                    </div>

                                @endif
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                @if (session('warning'))
                                    <div class="alert alert-warning">
                                        {{ session('warning') }}
                                    </div>
                                @endif
                                <form  class="default-form signin-form" method="POST" action="{{ route('login') }}">
                                {{csrf_field()}}    
                                <div class="form-group">
                                        <label for="email">ENTER YOUR B4U ID OR Email</label>
                                        @if ($errors->has('u_id') || $errors->has('email'))
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <strong>Sorry!</strong> {{$errors->first('u_id') ?: $errors->first('email') }}.
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        </div>
                                        @endif
                                        <input id="login" type="text"  name="login" class="form-control {{ $errors->has('u_id') || $errors->has('email') ? ' is-invalid' : '' }}"   value="{{ old('u_id') ?: old('email') }}" required autofocus>
                                    </div><!--/.form-group-->
                                    
                                    <div class="form-group">
                                        <label for="email">Password</label>
                                        @if ($errors->has('password'))
                                            <span class="help-block"  style="color:red">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        <input id="password" name="password" id="password" type="password" required class="form-controller">
                                    </div><!--/.form-group-->
                                    <div class="remember-and-password">
                                        <!-- <div class="login-form-remember"> 
                                            <label><input id="remembermesignin" value="" type="checkbox"><span>Keep me looged in</span></label>
                                        </div> -->
                                        <div class="forget-pass">
                                            <a class="btn-password" href="{{ route('password.request') }}">Forget Password</a>
                                        </div>
                                    </div><!--/.remember-and-password-->
                                    <div class="form-group">
                                        {!! Captcha::display([]) !!}
                                    </div>
    
                                    <div class="form-btn-group">
                                        <div class="form-login-area">
                                            <button type="submit" class="btn btn-default btn-primary">
                                                Sign In
                                            </button>
                                        </div>
                                        <div class="login-form-register-now">
                                            You have no account ? <a class="btn-register-now" href="{{ url('/signup') }}">Sign Up</a>
                                        </div>
                                    </div>
                                    <!-- <div class="reg-others-media">
                                        <div class="text">Or, User social media to sign in</div>
                                        <div class="social-media-icons">
                                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </div>
                                    </div> -->
                                </form>  
                            </div>
                        </div><!--~./ end user register area ~--> 
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!--~~./ end user signin block ~~-->
@stop