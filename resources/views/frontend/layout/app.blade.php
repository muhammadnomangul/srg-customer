<!doctype html>
<html lang="zxx">

<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="HYIP Investment is a modern presentation HTML5 Business HTML Template.">
    <meta name="keywords" content="HTML5, Template, Design, Business, HYIP, HYIP Html, Hyip manager, bitcoin doubler, hyip builder, hyip design, hyip template, investment,money, money script, p2p, ponzi template, profit" />
    <meta name="author" content="rifat636">

    <!-- Titles
    ================================================== -->

    <title>@if(isset($settings->site_name)){{$settings->site_name}}@else B4U Investors @endif |@if(isset($title)) {{$title}} @endif</title>

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{asset('public/home/images/favicon.ico')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/home/images/android-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/home/images/apple-icon-144x144.png')}}">

    <!-- Custom Font
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700%7COpen+Sans:300,400,600,700&display=swap" rel="stylesheet">  

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('public/home/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/meanmenu.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/simple-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/odometer-theme-default.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/fontawesome.all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/lightcase.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/chartist.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('public/home/css/style.css')}}">

    <script src="{{asset('home/js/jquery.min.js')}}"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('home/home/js/jquery.disableAutoFill.min.js') }}"></script>
    <script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
    <!-- Phone Input CSS-->
    <link rel="stylesheet" href="{{ asset('home/flag_phone_input/css/intlTelInput.css')}}">
    <!--<link rel="stylesheet" href="{{ asset('home/flag_phone_input/css/demo.css')}}">-->
    <!-- Phone Input JS-->
    <script src="{{ asset('home/flag_phone_input/js/intlTelInput.js')}}"></script> 
    <!-- Template JS Files -->
    <script src="{{ asset('home/home/js/modernizr.js')}}"></script>
    
    <style>
    ul#socialmedia li {
      display:inline;
      text-align: center;
      padding:5px 5px;

    }
    </style>
    
</head>

<body>
    @include('frontend.layout.header')
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Start Preloader
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~--> 
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div><!-- /preloader-icon -->
        </div><!-- /preloader-inner -->
    </div><!-- /preloader -->
    <!--********************* SITE CONTENT *********************-->
    <div class="site-content">
            @yield('content')
            @include('frontend.layout.footer')
    </div><!--~~./ end site content ~~-->
    </body>
</html>