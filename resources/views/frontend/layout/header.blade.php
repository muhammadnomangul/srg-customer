       <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Site Header
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <header class="site-header header-style-one">
            <div class="site-navigation style-one">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <div class="navbar navbar-expand-lg navigation-area">
                                <!-- Site Branding -->
                                <div class="site-branding">
                                    <a class="site-logo" href="{{ url('/home2') }}">
                                        <img src="{{ url('images/b4u-investment1.png') }}" alt="Site Logo" />
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="{{ url('/') }}">Back to OldSite</a>
                                </div><!-- /.site-branding -->

                                <div class="mainmenu-area">
                                    <nav class="menu">
                                        <ul id="nav">                  
                                            <li><a class="active" href="{{ url('/home2') }}">Home</a></li>      
                                            <li><a href="#about2">About</a></li>
                                            <li>
                                                <a href="#profit2">Profit</a>
                                            </li> 
                                            <li>
                                                <a href="#plans2">Plans</a>
                                            </li>
                                                                                
                                           <!--  <li class="dropdown-trigger">
                                                <a href="#">Invest</a>
                                                <ul class="dropdown-content">
                                                    <li><a href="investor.html">Investor</a></li>
                                                    <li><a href="investment-plan.html">Investment Plan</a></li>
                                                </ul>
                                            </li>      -->
                                            <li class="dropdown-trigger">
                                                <a href="#">Pages</a>
                                                <ul class="dropdown-content">
                                                    <li><a href="{{ url('uploads/fatwa.pdf') }}">Fatwa</a></li>
                                                    <li><a href="{{ url('/privacy2') }}">Privacy Policy</a></li>
                                                    <li><a href="{{ url('/terms2') }}">Terms & conditions</a></li>
                                                    <li><a href="{{ url('/partnership_agreement2') }}">Partnership Agreement</a></li>
                                                    <li><a href="#">Contact</a></li>
                                             
                                                    <!-- <li><a href="404.html">404</a></li> -->
                                                </ul>
                                            </li>
                                           @if (Route::has('login'))
                                             @auth
                                                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                                                <li><a href="{{ url('logout') }}"
                                                 onclick="event.preventDefault(); 
                                                 document.getElementById('logout-form').submit();"> Logout</a></li>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                            
                                            @else 
                                                <li><a href="{{ url('/signin') }}">Sign In</a></li>
                                                <li><a href="{{ url('/signup') }}">Sign Up</a></li>
                                              @endauth
                                            @endif
                                          <!--   <li class="dropdown-trigger">
                                                <a href="#">Blog</a>
                                                <ul class="dropdown-content">
                                                    <li><a href="blog.html">Blog Classic</a></li>
                                                    <li><a href="blog-grid.html">Blog Grid</a></li>
                                                    <li><a href="blog-single.html">Blog Details</a></li>
                                                </ul>
                                            </li> -->
                                            
                                        </ul> <!-- /.menu-list -->
                                    </nav><!--/.menu-->
                                </div><!--~./ mainmenu-wrap ~-->
                            </div><!--~./ navigation-area ~-->
                        </div><!--  /.col-12 -->
                    </div><!--  /.row -->
                </div><!-- /.container -->
            </div><!-- /.site-navigation -->

            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!--~~~~~~~ Start Mobile Menu ~~~~~~~~-->
                            <div class="mobile-menu">
                                <a class="mobile-logo" href="{{ url('/home2') }}">
                                        <img  src="{{ asset('images/b4u-logo.png') }}" alt="Site Logo" />
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="{{ url('/') }}">Back to OldSite</a>
                            </div><!--~~./ end mobile menu ~~-->
                        </div><!--  /.col-12 -->
                    </div><!--  /.row -->
                </div><!-- /.container -->
            </div><!-- /.mobile-menu-area -->
        </header><!--~~./ end site header ~~-->
          <!--~~~ Sticky Header ~~~-->
        <div id="sticky-header"></div><!--~./ end sticky header ~-->