<?php
	//Crypto Transections Temporary Blocked
	// $eth_address 	= "0x4a1f4d061604cba8c709b71b708a0b6a6f6f9d15";
	//$btc_address 	= "39HT1z5MSEXzH9TVYzgHerHQpnWSTyeN85";
	// $bch_address 	= "1GhFG6p8mqJzcqWr6nzRKCDrLrZWqi5uHU";
	// $ltc_address 	= "MWhMos6YAeNEkdYcQPZbFNuUu79xMWfMsS";
	// $xrp_address 	= "XRP TAQ- 77431070 : rPVMhWBsfF9iMXYj3aAzJVkPDTFNSyWdKy";
	// $dash_address = "Xg7W6FTGuFb3JdgJHuNCLezqHa4Cqoynmx";
	// $zec_address 	= "t1WjfzcVpuQGNwcwrTHBEBMNtEzsf3ruev1";

	$eth_address 	= $settings->eth_address;
	$btc_address 	= $settings->btc_address;
	$bch_address 	= $settings->bch_address;
	$ltc_address 	= $settings->ltc_address;
	$xrp_address 	= $settings->xrp_address;
	$dash_address   = $settings->dash_address;
	$zec_address 	= $settings->zec_address;
	$rsc_address 	= $settings->rsc_address;
?>
    @if($currency == "RSC")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#btc">
				<strong>RS Coin</strong>
				<img src="{{ asset('images/rsc.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="btc">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>RSC Address :</strong> {{$rsc_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
	@if($currency == "USDT")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#btc">
				<strong>Tether</strong>
				<img src="{{ url('images/usdt.jpg')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="usdt">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>USDT Address :</strong> {{$settings->usdt_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---BTC ADDRESS--->
	@if($currency == "BTC")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#btc">
				<strong>Bitcoin </strong>
				<img src="{{ asset('images/btc.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="btc">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>BTC Address :</strong> {{$btc_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---ETH ADDRESS--->
	@if($currency == "ETH")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#eth">
				<strong>Etherum</strong>
				<img src="{{ asset('images/eth.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="eth">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>ETH Address :</strong> {{$eth_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---LTC ADDRESS--->
	@if($currency == "LTC")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#ltc">
				<strong>Litecoin</strong>
				<img src="{{ asset('images/ltc.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="ltc">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>LTC Address :</strong> {{$ltc_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---BCH ADDRESS--->
	@if($currency == "BCH")
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#bch">
				<strong>Bitcash</strong>
				<img src="{{ asset('images/bch.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="bch">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>BCH Address :</strong> {{$bch_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---DASH ADDRESS--->
	@if($currency == "DASH" )
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#dash">
				<strong>DASH</strong>
				<img src="{{ asset('images/dash.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="dash">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>DASH Address :</strong> {{$dash_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---XRP ADDRESS--->
	@if($currency == "XRP" )
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#xrp">
				<strong>Ripple</strong>
				<img src="{{ asset('images/xrp.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="xrp">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>XRP Address :</strong> {{$xrp_address}}
					<strong>Destination Tag :</strong> 12345678

				</h4>
			</div>
		</div>
	</div>
	@endif
							
	<!---ZCH ADDRESS--->
	@if($currency == "ZEC" )
	<div class="panel panel-default" style="border:0px solid #fff;">
		<h4>
			<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zec">
				<strong>Zeecash</strong>
				<img src="{{ asset('images/zec.png')}}" width="60px;" height="60px;">
			</a>
		</h4>
		<div id="zec">
			<div class="alert alert-success alert-dismissable">
				<h4>
					<strong>ZEC Address :</strong> {{$zec_address}}
				</h4>
			</div>
		</div>
	</div>
	@endif	
			