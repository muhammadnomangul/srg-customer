@include('header')
		<!-- //header-ends -->
		<!-- main content   strt-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Login History</h3>
				@include('messages')
				
				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
					<table id="myTable" class="table table-hover"> 
						<thead> 
							<tr> 
								<th>Sr.#</th> 
								<th>Ip</th>
								<th>Location</th>
								<th>Date</th>
							</tr> 
						</thead> 
						<?php $count=1;?>
						<tbody> 
							@foreach($history2 as $data)
							<tr> 
								<th scope="row">{{$count}}</th>
						        <td>{{$data->ip}}</td>
						        <td>{{$data->location}}</td>
						        <td>{{$data->created_at}}</td>
							</tr> 
							<?php $count++; ?>
							@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	<script>
	$(document).ready( function () {
		$('#myTable').DataTable( {
        "pagingType": "full_numbers"
    	} );
	});

	$(".submit1").click(function(){
			//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");
		if(conf){
			return true;
		}
		else{
			return false;
		}
	});
	
	function viewDetailsFunc(id)
	{
	//	alert(id);
		jQuery.ajax({
			type: "POST",
			url: '{{{URL::to("")}}}/dashboard/viewDetailsPost',
			data: {id : id,type:"sold","_token": "{{ csrf_token() }}",},
		//	cache: false,
			success: function(response){
     			$("#scontent").html(response);
			},error:function(response){
				alert('Be patient, System is under maintenance');
			}
		});
	}
	</script>
@include('modals')
@include('footer')