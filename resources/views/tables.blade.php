<div class="row col-lg-12 widget-shadow" style="margin-top: 10px;">
    <div class="col-lg-6" style="float:left; margin-right:0px;  background-color:white;">
			<div width="100%" style="text-align:center; ">
				<div style="float:left; padding:5px; width:65%;">
					<h4 class="title"><font align="left">Latest Deposits</font></h4>
				</div>
				<div style="float:right; padding:10px; width:35%;">
				@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
					<a class="btn btn-sm btn-default" href="{{ url('dashboard/mdeposits')}}">View All Deposits</a>
				@else
					<a class="btn btn btn-sm btn-default" href="{{ url('dashboard/deposits')}}">View All Deposits</a>	
				@endif
				</div>
				
			</div>	
			<table id="myTable" class="table table-hover"> 
				<thead> 
					<tr> 
						<th>ID</th>
						<th>Amount</th>
						@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
							<th>User</th>
						@endif
						<!--<th>Trans No/Type</th>-->
						<th>Date</th>
					</tr>
				</thead> 
				<tbody> 
					@foreach($deposits as $data)
					<tr>
						
						<td>D-{{$data->id}}</td>
						@if($data->currency == "USD")
							<td>{{number_format($data->amount,2,".",",")}} ({{$data->currency}})</td>
						@else
							<td>{{number_format($data->amount,4,".",",")}} ({{$data->currency}})</td>
						@endif
						@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
						<td>{{$data->u_id}}</td>
						@endif
						@if($data->status == "Approved")
						<td>{{date('Y-m-d', strtotime($data->approved_at))}}</td>
						@else
						<td>{{date('Y-m-d', strtotime($data->created_at))}}</td>
						@endif
					</tr>
					@endforeach
				</tbody> 
			</table>
		</div>
		<div class="col-lg-6" style="float:right; margin-left="10px"">
			<div width="100%" >
			    <div style="float:left; padding:5px; width:65%;">
					<h4 class="title"><font align="left"> Latest Withdrawals</font></h4>
				</div>
				<div style="float:right; padding:10px; width:35%;">
					@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
					<a class="btn btn btn-sm btn-default" href="{{ url('dashboard/mwithdrawals')}}">View All Withdrawals</a>
					@else
					<a class="btn btn btn-sm btn-default" href="{{ url('dashboard/withdrawals')}}">View All Withdrawals</a>	
					@endif
				</div>
				
			</div>
			<table id="myTable" class="table table-hover" style="text-align:center;"> 
				<thead> 
					<tr> 
						<th>ID</th>
						<th>Amount</th>
						@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
						<th>User</th>
						@endif
						<th>Type</th>
						<th>Date</th>
					</tr>
				</thead> 
				<tbody> 
					@foreach($withdrawals as $data)
					<tr>
						<td>W-{{$data->id}}</td>
						@if($data->currency == "USD")
							<td>{{number_format($data->amount,2,".",",")}} ({{$data->currency}})</td>
						@else
							<td>{{number_format($data->amount,4,".",",")}} ({{$data->currency}})</td>
						@endif
						@if(Auth::user()->type == 1  ||  Auth::user()->type == "1")
						<td>{{$data->u_id}}</td>
						@endif
						<td>{{$data->payment_mode}}</td>
						
						<td>{{date('Y-m-d', strtotime($data->created_at))}}</td>
					</tr>
					@endforeach
				</tbody> 
			</table>
		</div>
	</div>
 
				
					
			

		<div class="col-lg-12 widget-shadow" style="margin-right:0px;margin-top:10px;  background-color:white;"> 
	 
			<div width="100%" style="text-align:center;">
				<div style="float:left; padding:5px; width:65%;">
					<h4 class="title"><font align="left">Latest Login History</font></h4>
				</div>
				<div style="float:right; padding:10px; width:35%;">
				 
					<a class="btn btn btn-sm btn-default" href="{{ url('dashboard/loginhistory')}}">View All History</a>	
				 
				</div>
				
			</div>	
			<table id="myTable" class="table table-hover"> 
				<thead> 
					<tr> 
						<th>Sr.#</th>
						<th>Ip</th>
						<th>Location</th>
						<th>Date</th>
					</tr>
				</thead>
				<?php $count=1;?> 
				<tbody> 
					@if(isset($history1))
					@foreach($history1 as $logi)
					<tr>
						
						<th scope="row">{{$count}}</th>
				        <td>{{$logi->ip}}</td>
				        <td>{{$logi->location}}</td>
				        <td>{{$logi->created_at}}</td>
					</tr>
					<?php $count++; ?>
					@endforeach
					@endif
				</tbody> 
			</table>
	</div>
	 