@include('header')

<style>
    .btn-custom {
        border: 1px solid black;
        margin: 5px;
    }

    label {
        margin-bottom: 10px;
    }

    .btn-active {
        background-color: #9e2187 !important;
        border-color: white !important;
        color: white !important;
    }

    .required {
        border: 1px solid red;
    }

    /* Rating Classes */
    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }

</style>
<div id="page-wrapper">

    {{-- Transfer to Different User--}}
    <div id="withdrawalModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">
                        Transfer to B4U User
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="errors alert alert-danger hide"></div>

                    <form id="formABC" style="padding:3px;" data-toggle="validator" role="form" method="post"
                          action="{{ route('cash_box_transfer')  }}" autocomplete="off" mod="cbTrans">
                        <div class="form-content">
                            <div class="form-group" ¬>
                                <label for="select" id="label_id" class="control-label">Select Cash Box Account *
                                    : </label>
                                <div class="form-input">
                                    <select name="cb_account" id="currency"
                                            class="form-control amount_type currencyValCB select2"
                                            required="required">
                                        @foreach($users_cash_box_accounts as $cba)
                                            @if($cba->currencies()->code == 'USD' || $cba->currencies()->code == 'RSC' || $cba->currencies()->code == 'USDT')
                                            <option {{ $current_cash_box_account->id == $cba->id ? 'selected="selected"' : null }} data-ab="{{ $cba->current_balance }}"
                                                    value="{{$cba->currencies()->code}}">
                                                {{ $cba->name }}
                                                ({{ number_format($cba->current_balance, $cba->curr_precision ) }}</span> {{$cba->currencies()->code}}
                                                )
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="fundreciever">
                                <label for="select"  class="control-label">Select Beneficiary * : </label>
                                <label id="benId" class="control-label"  style="float: right; color: maroon" ></label>
                                <div class="form-input">
                                    <select name="fund_receivers_id" id="fund_receiver_id" class="form-control select2"
                                            required="required">
                                        <option selected="selected" value="">--Select Beneficiary--</option>
                                        @foreach($beneficiaries as $beneficiary)
                                            @if(isset($beneficiary->beneficiary_uid))
                                                <option {{ old('fund_receivers_id') == $beneficiary->beneficiary_uid ? 'selected' : '' }}  value="{{$beneficiary->beneficiary_uid}}">{{$beneficiary->beneficiary_uid}}</option>
                                            @endif
                                        @endforeach
                                        <option {{ old('fund_receivers_id') == 'notexist' ? 'selected' : '' }} value="notexist">
                                            Add New Beneficiary
                                        </option>
                                    </select>
                                </div>
                                @error('fund_receivers_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" style="display: none" id="fundreciever2">
                                <label for="text" id="ben_id" class="control-label fund2">New beneficiary B4U-ID *
                                    : </label>
                                <div class="form-input">
                                    <input type="text" name="fund_receivers_id2" id="fundreciever_id2"
                                           class="form-control"
                                           placeholder="Enter B4U-ID (B4U0001) Of Fund Reciever User."
                                           value="{{ old('fund_receivers_id2') }}">
                                </div>

                                @error('fund_receivers_id2')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="amount_with">
                                <label for="select" id="amountid" class="control-label label_id">Amount* :</label>
                                <div class="form-input">
                                    <input type="number" name="amount" id="with_amt" class="form-control"
                                           placeholder="Enter amount here" required="required"
                                           min="0"
                                           max="{{ $current_cash_box_account->current_balance }}"
                                           value="{{ old('amount') }}">
                                </div>

                                @error('amount')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                            </div>
                            <div class="form-group">
                                @csrf
                                <button type="button" id="submit12" class="btn btn-primary t-s">Transfer Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="cbRatingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">
                        Rating to B4U User
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="errors alert alert-danger hide"></div>

                    <form id="formABC" style="padding:3px;" data-toggle="validator" role="form" method="post"
                          action="{{ route('cb_rating_user')  }}" autocomplete="off">
                        <div class="form-content">
                            <div class="form-group">
                                <label for="text"  class="control-label">Rating* : </label>
                                <div class="form-input">
                                    <div class="rate col-md-7">
                                        <input type="radio" id="star5" name="rate" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" id="star4" name="rate" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" name="rate" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" name="rate" value="2" />
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" name="rate" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group" >
                                <label for="text"  class="control-label">Comments* : </label>
                                <div class="form-input">
                                    <input type="text" name="comment" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                @csrf
                                <input type="hidden" class="cbRatingUser" name="id" value="">
                                <input type="hidden" class="transType" name="transType" value="">
                                <button type="button" id="submit12" class="btn btn-primary t-s">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--     Top Up Model--}}
    <div id="topUpModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">
                        Cash Out
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="errors alert alert-danger hide"></div>
                    <form id="formABCTopUp" style="padding:3px;" data-toggle="validator" role="form" method="post"
                          action="{{ route('cash_box_topUp')  }}" autocomplete="off" mod="cbTopup">
                        <div class="form-content">
                            <div class="form-group" ¬>
                                <label for="select" id="label_id" class="control-label">Select Cash Box Account *
                                    : </label>
                                <div class="form-input">
                                    <select name="cb_account" id="currency"
                                            class="form-control amount_type currencyValCB cashboxcbaccount select2"
                                            required="required">
                                        @foreach($users_cash_box_accounts as $cba)
                                            @if($cba->currencies()->code == 'USD' || $cba->currencies()->code == 'RSC' || $cba->currencies()->code == 'USDT')
                                            <option {{ $current_cash_box_account->id == $cba->id ? 'selected="selected"' : null }} data-ab="{{ $cba->current_balance }}"
                                                    value="{{$cba->currencies()->code}}">
                                                {{ $cba->name }}
                                                ({{ number_format($cba->current_balance, $cba->curr_precision ) }}</span> {{$cba->currencies()->code}}
                                                )
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--                            <div class="form-group" id="transfer_address">--}}
                            {{--                                <label for="select" id="transfer_address_label" class="control-label label_id">Transfer To* :</label>--}}
                            {{--                                <div class="form-input">--}}
                            {{--                                    <input type="text" name="amount" id="with_amt" class="form-control"--}}
                            {{--                                           placeholder="Enter amount here" required="required"--}}
                            {{--                                           min="0"--}}
                            {{--                                           max="{{ $current_cash_box_account->current_balance }}"--}}
                            {{--                                           value="{{ old('amount') }}">--}}
                            {{--                                </div>--}}

                            {{--                                @error('amount')--}}
                            {{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
                            {{--                                @enderror--}}

                            {{--                            </div>--}}

                            <div class="form-group" id="amount_with">
                                <label for="select" id="amountid" class="control-label label_id">Amount* :</label>
                                <div class="form-input">
                                    <input type="number" name="amount" id="with_amt" class="form-control cashoutamount"
                                           placeholder="Enter amount here" required="required"
                                           min="0"
                                           max="{{ $current_cash_box_account->current_balance }}"
                                           value="{{ old('amount') }}">
                                </div>

                                @error('amount')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                            </div>

                            <div class="responseCashOut"></div>

                            <div class="form-group">
                                @csrf
                                <button type="button" id="submit12" class="btn btn-primary cashoutbtn t-s">Cash Out
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Cash box account   --}}
    <div class="main-page signup-page">

        @include('messages')

        <div class="row">


            <div style="display: flex; flex-direction: row;justify-content: space-between">

                {{--     User Current Accounts          --}}
                <div class="rp t-b" style="width: 25%">
                    <div class="tab" style="display: flex;flex-direction: column">

                        <h3 style="margin-left : 10px; color: black">Cash Box</h3>
                        <hr style="border: 1px solid black; width: 95%">


                        @foreach($users_cash_box_accounts as $cba)
                            <a href="{{ route('cash-box.show', [$cba->id]) }}"
                               class="btn btn-custom
{{ $cba->id == $current_cash_box_account->id ? 'btn-active' : '' }}">
                                <span style="float: left"> {{ $cba->currencies()->code }} </span>
                                <span style="float: right">
                                    {{ number_format($cba->current_balance, $cba->curr_precision) }} </span>
                            </a>
                        @endforeach
                    </div>
                </div>


                <div style="width: 73%">


                    <div class="rp t-b" style="display: flex; justify-content: space-between">

                        <div class="col-md-12" style=" text-align: center">
                            <h3>
                                {{ number_format($current_cash_box_account->current_balance, $current_cash_box_account->curr_precision) }}
                                ({{ $current_cash_box_account->currencies() instanceof \App\Currencies ?  $current_cash_box_account->currencies()->code : null }}
                                )
                                <small>Current balance</small>
                            </h3>

                            <h3 style="padding-top:20px">

                                <small>{{ number_format($current_cash_box_account->locked_balance, $current_cash_box_account->curr_precision) }}
                                    ({{ $current_cash_box_account->currencies() instanceof \App\Currencies ?  $current_cash_box_account->currencies()->code : null }})
                                    Balance Under Process
                                </small>
                            </h3>

                        </div>

                        <div class="col-md-4">
                            <div style="display: flex;flex-direction: column; justify-content: space-between">
                                @if($current_cash_box_account->currencies()->code == 'USD' || $current_cash_box_account->currencies()->code == 'RSC' || $current_cash_box_account->currencies()->code == 'USDT')
                                <a class="btn btn-default" href="#" data-toggle="modal" data-target="#withdrawalModal">
                                    <svg style="width: 20px" class="svg-inline--fa fa-exchange-alt fa-w-16"
                                         aria-hidden="true"
                                         focusable="false" data-prefix="fas" data-icon="exchange-alt" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                        <path fill="currentColor"
                                              d="M0 168v-16c0-13.255 10.745-24 24-24h360V80c0-21.367 25.899-32.042 40.971-16.971l80 80c9.372 9.373 9.372 24.569 0 33.941l-80 80C409.956 271.982 384 261.456 384 240v-48H24c-13.255 0-24-10.745-24-24zm488 152H128v-48c0-21.314-25.862-32.08-40.971-16.971l-80 80c-9.372 9.373-9.372 24.569 0 33.941l80 80C102.057 463.997 128 453.437 128 432v-48h360c13.255 0 24-10.745 24-24v-16c0-13.255-10.745-24-24-24z"></path>
                                    </svg>

                                    &nbsp; Transfer</a>

                                @endif
                                <a style="margin: 10px 0 0 0" class="btn btn-default" href="#" data-toggle="modal"
                                   data-target="#topUpModal">
                                    <i class="fas fa-arrow-down"></i>&nbsp; Cash Out</a>


                                {{--                                <div style="margin: 10px 0 0 0;">--}}
                                {{--                                    <div class="dropdown">--}}
                                {{--                                        <button class="btn btn-default dropdown-toggle" type="button"--}}
                                {{--                                                data-toggle="dropdown" style="width: 100%">Top Up<span class="caret"></span></button>--}}
                                {{--                                        <ul class="dropdown-menu">--}}
                                {{--                                            <li><a href="#">B4U Wallet</a></li>--}}
                                {{--                                        </ul>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}


                            </div>


                        </div>

                    </div>

                    <div class="rp t-b" style="margin-top: 20px;">
                        <ul class="nav nav-tabs">
                            <li class="active debit-tab"><a data-toggle="tab" href="#debit">Debit</a></li>
                            <li class="credit-tab"><a data-toggle="tab" href="#credit">Credit</a></li>
                        </ul>
                        <div class="tab-content" style="margin-top: 20px;">
                            <div id="credit" class="table-responsive tab-pane fade">
                                <table id="creditTable" class="table table-hover" style="width: 100%">
                                </table>
                            </div>
                            <div id="debit" class="table-responsive tab-pane fade in active">
                                <table id="debitTable" class="table table-hover" style="width: 100%">
                                </table>
                            </div>
                        </div>


                        {{--                        <div class="col-md-6" style="border-right:2px solid black">--}}
                        {{--                            <h5 style="font-weight: bold">Credits</h5>--}}
                        {{--                            <hr style="border: 1px solid black">--}}
                        {{--                            <table class="table table-bordered">--}}
                        {{--                                <tr>--}}
                        {{--                                    <th>h1</th>--}}
                        {{--                                    <th>h2</th>--}}
                        {{--                                </tr>--}}

                        {{--                                <tr>--}}
                        {{--                                    <td>d1</td>--}}
                        {{--                                    <td>d2</td>--}}
                        {{--                                </tr>--}}
                        {{--                            </table>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-6">--}}
                        {{--                            <h5 style="font-weight: bold">Debits</h5>--}}
                        {{--                            <hr style="border: 1px solid black">--}}
                        {{--                            <table class="table table-bordered">--}}
                        {{--                                <tr>--}}
                        {{--                                    <th>h1</th>--}}
                        {{--                                    <th>h2</th>--}}
                        {{--                                </tr>--}}

                        {{--                                <tr>--}}
                        {{--                                    <td>d1</td>--}}
                        {{--                                    <td>d2</td>--}}
                        {{--                                </tr>--}}
                        {{--                            </table>--}}

                        {{--                        </div>--}}

                    </div>


                </div>

            </div>

        </div>
    </div>
</div>

@include('modals')
@include('footer')
<script>
    let accId = "<?php echo $current_cash_box_account->id ?>";
    let currentAccCurrency = "<?php echo $current_cash_box_account->currencies() instanceof \App\Currencies ? $current_cash_box_account->currencies()->code : null ?>";
    let currentAccName = "<?php echo $current_cash_box_account->name ?>";
    let addNewBenificiaryInput = $('input[name=fund_receivers_id2]');
    let amountEntered = $('input[name=amount]')
    let searchBenficiary = "{{ url('dashboard/beneficiaryDetails') }}";
    let creditURI = "{{ url('cash-box/credits/json') }}";
    let debitURI = "{{ url('cash-box/debits/json') }}";
    let csrfToken = "{{ csrf_token() }}";
    let isAllowedTopDownURL = "{{ route('is_allowed_top_down') }}";
    let approveCbTransferURL = "{{ url("cash-box/cbTransfer/approve") }}";
    let userRatingInfoURL = "{{ url("userRatingInfo") }}";

    $(function () {
        // load Credit and Debit
        loadDebitRecords(debitURI);
        loadCreditRecords(creditURI);
    });
</script>
<script src="{{ url('cb.js?2.6.3') }}"></script>
