<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
@include('header')

<div id="page-wrapper">
    <div class="main-page signup-page">
        <br>
        <h3 class="title1">{{getUserDetailsById($userId)->u_id}} Reviews</h3>
        <div class="row">

            <div class="col-lg-3 col-sm-1" style="text-align: left;">
                No of deals done: {{$userRatings->count()}}
            </div>
            <div class=" col-lg-7 col-sm-1  text-right RatingMobileView" >
              Rating:
            </div>

            <div class="col-lg-1  col-sm-12 ">

                <input style="text-align: left !important;" id="input-1" name="input-1" class="title1 rating rating-loading " data-min="0" data-max="5" data-step="0.1" value="{{$rating}}" data-size="s" disabled="">



            </div>
            <div class="col-lg-1 col-sm-12  text-sm-left MobileViewRating ">({{$rating}})</div>

        </div>


        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
            <table id="myTable" class="table table-hover">
                <thead>
                <tr>
                    <th>Sr.</th>
                    <th>Comments</th>
                    <th>Rating</th>
                    <th>Rating By</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody style="color: black !important;" >
                <?php $c = 1; ?>
                @foreach($userRatings as $userRating)
                    <tr>
                        <td>{{ $c++ }}</td>
                        <td>{{ $userRating->comments }}</td>
{{--
                        <td>{{ $userRating->ratng }}</td>
--}}
                        <td><span style="display: none">{{ $userRating->rating }}</span><input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="{{ $userRating->rating }}" data-size="s" disabled=""></td>
                        <td>{{ getUserDetailsById($userRating->rating_by)->u_id }}</td>
                        <td>{{ date_format($userRating->created_at,"d-m-Y") }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "pagingType": "full_numbers",
            "bLengthChange": false,
        });
    });
    $("#input-id").rating();
</script>
@include('footer')