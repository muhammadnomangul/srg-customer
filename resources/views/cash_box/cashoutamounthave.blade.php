<table class="table table-bordered">
    <thead>
    <tr>
        <th>Can Cash Out?</th>
        <th>Cash Out</th>
        <th>Deduction fee</th>
        <th>USD Amount</th>
        <th>Total amount</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>
            @if($should_transfer)
                <b style="color: green" class="shouldtransfer isyes">Yes</b>
            @else
                <b style="color: red" class="shouldtransfer isno">No</b>
            @endif
        </td>
        <td>{{ $amount_to_cash_out }} {{ strtoupper($currency) }}</td>
        <td>{{ $deduction_fee }} {{ strtoupper($currency) }}</td>
        <td>{{ $usd_amount }} </td>
        <td>{{ $deduction_fee + $amount_to_cash_out  }} {{ strtoupper($currency) }}</td>
    </tr>

    <tr>
        <th colspan="5">Message</th>
    </tr>
    <tr>
        <td colspan="5">{{ $message }}</td>
    </tr>

    </tbody>

</table>