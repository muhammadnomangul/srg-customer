<!DOCTYPE html>
<html lang="en">
<head>
    <title>Validate OTP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        body {
            font-family: Tahoma;
        }

        .text-center {
            text-align: center !important;
            padding: 20px;
        }

        #myProgress {
            width: 100%;
            background-color: #ddd;
        }

        #myBar {
            width: 100%;
            height: 30px;
            background-color: #428bca;

            padding: 7px 20px 0 0;
            font-weight: bold;
            color: white;
        }


    </style>
</head>
<body class="auth-page" style="background-color:#f8f8f8;">
<div class="wrapper">
    <div class="container user-auth" style="padding:20px; margin-top: 80px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-md-8 col-lg-8">
                <!-- Logo Starts -->
                <div class="text-center">

                    <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
                        <img id="logo" class="img-responsive"
                             src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                             style="margin-left: auto; margin-right: auto; margin-top: auto;">
                    </a>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @include("messages")
                        <p> Please enter the OTP that you have received on your phone number or Email </p>
                        <form id="formABC" action="{{route('postOtp')}}" method='POST'>
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="pin">PIN:</label>
                                <input type="number" class="form-control" name='pin' id="pin">
                            </div>
                            <div style="text-align: right;">
                                @if($token->otp_type=="withdrawal")
                                    <a href="/dashboard/withdrawals" class="btn btn-default">Back</a>
                                @elseif($token->otp_type=="account" || $token->otp_type=="personal_info")
                                    <a href="/dashboard/accountdetails" class="btn btn-default">Back</a>
                                @elseif($token->otp_type=="re-invest")
                                    <a href="/dashboard/deposits" class="btn btn-default">Back</a>
                                @elseif($token->otp_type=="register")
                                    <a href="/register" class="btn btn-default">Back</a>
                                @elseif($token->otp_type=="login")
                                    <a href="/user/logout" class="btn btn-default">Back</a>
                                @elseif($token->otp_type=="site_settings")
                                    <a href="/dashboard/settings" class="btn btn-default">Back</a>
                                @else
                                    <a href="{{ back() }}" class="btn btn-default">Back</a>
                                @endif
                                <button type="submit" id="submit1" class="btn btn-success">Submit</button>


                                <a id="resend" style="color:red; float: left;" onclick="clickAndDisable(this)"
                                   href="/otp/resend/{{$token->otp_type}}">Resend Code?</a>

                                <br>
                                <!--  <div id="myProgress">
                                   <div id="myBar">100%</div>
                                 </div> -->

                            </div>
                        </form>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

    let timeCountDown = 100;
    let elem = document.getElementById("myBar");

    $(function () {

        showHideTimer = (showTimer = true) => {
            if (showTimer) {
                // $('#resend').hide();
                var TimeInterval = setInterval(function () {
                    timeCountDown = timeCountDown - 1;
                    //$('#resendp').html(timeCountDown);
                    // $('#fileProgress').attr('value', timeCountDown);
                    elem.style.width = timeCountDown + "%";
                    $('#myBar').html(timeCountDown + "%");


                    if (timeCountDown == 0) {
                        $('#resend').show();
                        //$('#resendp').hide();
                        clearInterval(TimeInterval);
                        $('#myProgress').hide();
                    }

                }, 1000);
            }
        }
        showHideTimer(true);
    });

    $("#formABC").submit(function (e) {

        //disable the submit button
        $("#submit1").attr("disabled", true);
        return true;
    });

    function clickAndDisable(link) {
        // disable subsequent clicks
        /* $('#resend').hide();
           $('#resendp').html('OTP has been sent.Please wait!');*/
        link.onclick = function (event) {
            event.preventDefault();
        }
    }
</script>

</body>
</html>
