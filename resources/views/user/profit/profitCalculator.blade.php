@include('header')

<!-- main content start-->

<div id="page-wrapper">

    <div class="main-page signup-page">

        <h3 class="title1">Profit Calculator</h3>


        @include('messages')



        @if(count($errors) > 0)

            <div class="row">

                <div class="col-lg-12">

                    <div class="alert alert-danger alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        @foreach ($errors->all() as $error)

                            <i class="fa fa-warning"></i> {{ $error }}

                        @endforeach

                    </div>

                </div>

            </div>

        @endif

        <div class="row">

            <div class="sign-up-row widget-shadow col-lg-12">

                <div class="col-lg-6">
                    <form action="#">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <label>Deposit Approved Date:</label>
                            <div class="form-group">
                                <input class="form-control" name="approvedAt" type="text" id="datepicker" required>
                            </div>
                        </div>
                        <div class="row">
                            <label>Deposit Sold/Current Date: </label>
                            <div class="form-group">
                                <input class="form-control" name="soldAt" type="text" id="datepicker2" required>
                            </div>
                        </div>
                        <div class="row">
                            <label>Deposit Amount: </label>
                            <div class="form-group">
                                <input class="form-control" type="number" min="0" pattern="/^-?\d+\.?\d*$/"
{{--                                       onKeyPress="if(this.value.length==6)return false;" --}}
                                       name="amount" required>
                            </div>
                        </div>
                        <div class="row">
                            <div style="float:left;">
                                <a class="btn btn-default" id="viewProfitButton" onclick="viewProfitAmount();">
                                    Calculate Profit </a>
                            </div>
                            <div style="float:right;">
                                <button class="btn btn-danger" type="reset" value="Reset" id="reset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <div class="modal-content ucontent">

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>

    $(function () {
        $("#datepicker").datepicker({
            maxDate: -1,
        });
        $("#datepicker2").datepicker({
            maxDate: -1,
        });
    });

    $(document).ready(function () {
        //option A
        $("form").submit(function (e) {
            alert('submit intercepted');
            e.preventDefault(e);
        });
    });

    function postData(url = ``, data = {}) {
        // Default options are marked with *
        return fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
        // parses response to JSON
    }

    function viewProfitAmount() {
        let processing = false;

        if (processing) {
            return false;
        }

        processing = true;
        var approvedAt = $("input[name=approvedAt]").val();
        var soldAt = $("input[name=soldAt]").val();
        var amount = $("input[name=amount]").val();

        if (!approvedAt || !soldAt || !amount) {
            return alert('Please fill out all fields before requesting for profit.');
        }

        fetch('{{{URL::to("")}}}/dashboard/findProfit', {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                amount: amount,
                approvedAt: approvedAt,
                soldAt: soldAt,
                "_token": '{{ csrf_token() }}',
            })
        })
            .then(function (response) {
                console.log(response)
                //alert("Profit Amount :"+response);
                return response.text()

            })
            .then(function (text) {
                $(".ucontent").html(text);

            }) // JSON-string from `response.json()` call
            .catch(function (error) {
                //console.error(error)
                alert("error!!!!");
            }).finally(res => {
            processing = false;
        });
    }

    ///
    $("#reset").on('click', function () {
        $(".ucontent").html("");
    });

</script>
@include('footer')
