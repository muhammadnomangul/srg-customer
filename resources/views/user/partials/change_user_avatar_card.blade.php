<label>Change Avatar</label>
<hr>
<div class="card-body">
    <form method="post" action="{{action('UsersController@updatephoto')}}">
        <input type="hidden" name="_key" value="{{csrf_token()}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="file" required class="file-upload-aws" accept="image/*">
        <input type="hidden" name="fileurl">
        <br>
        <button type="submit" class="mt-3 btn btn-sm btn-primary">Upload</button>
    </form>
</div>
     

