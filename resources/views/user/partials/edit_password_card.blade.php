    <label>Change Password</label>
    <hr>
    <div class="card-body">
        <form method="post" action="{{action('UsersController@updatepass')}}" mod="up">
            <div class="form-content">
                <div class="form-group">
                    <label>Password</label>
                    <br>
                    <input class="form-control" type="password" name="old_password"  placeholder="Old Password" required >
                    <br>
                    <input class="form-control" type="password"   name="password" placeholder="New Password*" required  >
                    <br>
                    <input class="form-control" type="password"   name="password_confirmation" placeholder="Confirm Password*" required>
                </div>
                <input type="hidden" name="current_password" value="{{Auth::user()->password}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="o-s btn btn-sm btn-primary float-right">Change Password</button>
            </div>
        </form>
    </div>
 
