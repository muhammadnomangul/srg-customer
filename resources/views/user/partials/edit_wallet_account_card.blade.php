<div style="text-align: center;"><h4><label>Edit Wallet Account Information</label></h4></div>
<hr>
<div class="card-body">
    <form method="post" action="{{action('UsersController@updateacct')}}" mod="ewai">
       <div class="form-content">
           <div class="form-group">
               <label>BTC Address</label>
               <input class="form-control" type="text" name="btc_address"
                      value="{{Auth::user()->btc_address}}" {{empty(Auth::user()->btc_address) ?  '' : '' }}>
           </div>
           <div class="form-group">
               <label>Eth Address</label>

               <input class="form-control" type="text" name="eth_address"
                      value="{{Auth::user()->eth_address}}" {{empty(Auth::user()->eth_address) ?  '' : '' }}>
           </div>
           <div class="form-group">
               <label>BCH Address</label>
               <input class="form-control" type="text" name="bch_address"
                      value="{{Auth::user()->bch_address}}">
           </div>
           <div class="form-group">
               <label>LTC Address</label>
               <input class="form-control" type="text" name="ltc_address"
                      value="{{Auth::user()->ltc_address}}" {{empty(Auth::user()->ltc_address) ?  '' : '' }}>
           </div>
           <div class="form-group">
               <label>XRP Address | XRP-Tag</label><br>
               <input class="form-control" style="width: 70%; display: inline-block;" type="text"
                      {{empty(Auth::user()->xrp_address) ?  '' : '' }}
                      name="xrp_address1"
                      value="{{$xrp_address[0]}}"><input style="width: 30%; display: inline-block;" class="form-control" type="text" {{empty(Auth::user()->xrp_address) ?  '' : '' }} name="xrp_address2" value="@if(isset($xrp_address[1])){{$xrp_address[1]}}@endif" placeholder="XRP-TAG">
           </div>
           <div class="form-group">
               <label>DASH Address</label>
               <input class="form-control" type="text" name="dash_address"
                      value="{{Auth::user()->dash_address}}" {{empty(Auth::user()->dash_address) ?  '' : '' }}>
           </div>
           <div class="form-group">
               <label>ZEC Address</label>
               <input class="form-control" type="text" name="zec_address"
                      value="{{Auth::user()->zec_address}}" {{empty(Auth::user()->zec_address) ?  '' : '' }}>
           </div>

           <div class="form-group">
               <label>RSC Address</label>
               <input class="form-control" type="text" name="rsc_address"
                      value="{{Auth::user()->rsc_address}}" {{empty(Auth::user()->rsc_address) ?  '' : '' }}>
           </div>
           <div class="form-group">
               <label>USDT Address</label>
               <input class="form-control" type="text" name="usdt_address"
                      value="{{Auth::user()->usdt_address}}" {{empty(Auth::user()->usdt_address) ?  '' : '' }}>
           </div>
           <input type="hidden" name="_key" value="{{csrf_token()}}">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <button type="button" class="btn btn-sm btn-primary float-right o-s" style="float: right;">Update Information
           </button>
       </div>
    </form>
</div>
 
