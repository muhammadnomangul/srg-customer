<div style="text-align: center;"><h4><label>Edit Account Information</label></h4></div>
<hr>
<form class="form-horizontal" method="post" action="{{action('UsersController@updateprofile')}}"
      enctype="multipart/form-data" mod="eai">
    <div class="form-content">
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Name:</label>
            <div class="col-sm-9">
                <input class="form-control" type="text" name="name" required
                       value="{{ old('name') ? old('name') :Auth::user()->name }}" {{empty(Auth::user()->name) ?  '' : '' }}>
            </div>
        </div>

        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Email:</label>
            <div class="col-sm-9" style="display: flex">
                <input class="form-control" type="email" name="email" required
                       value="{{ old('email') ? old('email') :Auth::user()->email }}" {{empty(Auth::user()->email) ?  '' : 'disabled=true' }}>

                @if(\Illuminate\Support\Facades\Auth::user()->email_verified_at)
                    <button type="button" class="btn btn-success">Email Verified</button>
                @else
                    <a href="{{ route('send_verification_email_') }}" class="btn btn-danger">Click to Verify an
                        Email</a>
                @endif

            </div>
        </div>

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Phone No :</label>--}}
{{--            <div class="col-sm-9">--}}
{{--                <input class="form-control" id="phone_no" name="phone_no"--}}
{{--                       value="{{ old('phone_no') ? old('phone_no'): Auth::user()->phone_no}}" type="tel"--}}
{{--                       placeholder="" {{empty(Auth::user()->phone_no) ?  '' : '' }}>--}}

{{--                <input name="country_coe" id="country_coe" type="hidden" autocomplete="off">--}}

{{--                <span id="valid-msg" class="hide" style="color:green">✓ Valid</span>--}}
{{--                <span id="error-msg" class="hide" style="color:red"></span>--}}

{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Phone No :</label>
            <div class="col-sm-9"  style="display: flex">
                <input class="form-control" disabled
                       value="{{ old('phone_no') ? old('phone_no'): Auth::user()->phone_no}}" type="tel"
                       placeholder="" {{empty(Auth::user()->phone_no) ?  '' : '' }} >
                <a href="{{ route('update_profile_phone') }}" class="btn btn-success">Update Phone Number </a>
                <input name="country_coe" id="country_coe" type="hidden" autocomplete="off">

                <span id="valid-msg" class="hide" style="color:green">✓ Valid</span>
                <span id="error-msg" class="hide" style="color:red"></span>

            </div>
        </div>


        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Country :</label>
            <div class="col-sm-9">
                <select class="form-control select2" name="Country" id="countryid"
                        onChange="searchBanks();" {{empty(Auth::user()->Country) ?  '' : '' }}>
                    <option value="">Select Country</option>
                    @foreach($countries as $country)
                        <option {{ old('Country')  == $country->country_name ? 'selected' : '' }} value="{{$country->country_name}}"
                                @if(Auth::user()->Country == "$country->country_name" )selected @endif>{{$country->country_name}} </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Bank Name:</label>
            <div class="col-sm-9">
                <select class="form-control select2" name="bank_name" id="bank_nameId"
                        onChange="selectBank();" {{empty(Auth::user()->bank_name) ?  '' : '' }}>
                    <option value="">Select Bank</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Account Title:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="account_name"
                       value="{{ old('account_name') ? old('account_name') : Auth::user()->account_name}}" {{empty(Auth::user()->account_name) ?  '' : '' }}>


            </div>
        </div>
        @if(
     Auth::user()->Country == "Europe"
    || Auth::user()->Country == "France"
     || Auth::user()->Country == "Canada"
    || Auth::user()->Country == "France, Metropolitan"
    || Auth::user()->Country == "French Guiana"
    || Auth::user()->Country == "French Polynesia"
    || Auth::user()->Country == "French Southern Territories"
    || Auth::user()->Country == "Georgia"
    || Auth::user()->Country == "Germany"
    || Auth::user()->Country == "Greece"
    || Auth::user()->Country == "Greenland"
    || Auth::user()->Country == "Italy"
    || Auth::user()->Country == "United Kingdom"
    || Auth::user()->Country == "United States"
    || Auth::user()->Country == "United States minor outlying islands"




)
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Account Holder Name:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="usa_account_holder_name"
                       value="{{ old('usa_account_holder_name') ? old('usa_account_holder_name') : Auth::user()->usa_account_holder_name}}" {{empty(Auth::user()->usa_account_holder_name) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
    Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Date of birth:</label>
            <div class="col-sm-9">

                <input class="form-control" type="date" name="date_of_birth"
                       value="{{ old('date_of_birth') ? old('date_of_birth') : Auth::user()->date_of_birth}}" {{empty(Auth::user()->date_of_birth) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
   Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Full home address:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="full_address"
                       value="{{ old('full_address') ? old('full_address') : Auth::user()->full_address}}" {{empty(Auth::user()->full_address) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
    Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Bank name:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="full_bank_name"
                       value="{{ old('full_bank_name') ? old('full_bank_name') : Auth::user()->full_bank_name}}" {{empty(Auth::user()->full_bank_name) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
            Auth::user()->Country == "Canada"
        )
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Institution Number (of your bank )</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="institute_bank_number"
                       value="{{ old('institute_bank_number') ? old('institute_bank_number') : Auth::user()->institute_bank_number}}" {{empty(Auth::user()->institute_bank_number) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
            Auth::user()->Country == "Canada"
        )
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Transit Number</label>
            <div class="col-sm-9">
                <input class="form-control" type="text" name="transit_number"
                       value="{{ old('transit_number') ? old('transit_number') : Auth::user()->transit_number}}" {{empty(Auth::user()->transit_number) ?  '' : '' }}>
            </div>
        </div>
        @endif
        @if(
    Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"

)
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Routing number:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="routing_number"
                       value="{{ old('routing_number') ? old('routing_number') : Auth::user()->routing_number}}" {{empty(Auth::user()->routing_number) ?  '' : '' }}>


            </div>
        </div>
        @endif
        @if(
   Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"




)
            <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">
                Account Type</label>
            <div class="col-sm-9">
{{--                <input class="form-control" type="radio" name="bank_account_type"--}}
{{--                       value="{{ old('bank_account_type') ? old('bank_account_type') : Auth::user()->bank_account_type}}" {{empty(Auth::user()->bank_account_type) ?  '' : '' }}>--}}
{{--                <input class="form-control" type="radio" name="bank_account_type"--}}
{{--                       value="{{ old('bank_account_type') ? old('bank_account_type') : Auth::user()->bank_account_type}}" {{empty(Auth::user()->bank_account_type) ?  '' : '' }}>--}}
                <div class="row">
                    <div class="col-sm-5">
                        <input type="radio" id="customRadioInline1" {{(Auth::user()->bank_account_type) == 'checking' ?  'checked' : '' }} value="checking" name="bank_account_type" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Checking Account</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" id="customRadioInline2" {{(Auth::user()->bank_account_type) == 'saving' ?  'checked' : '' }} value="saving" name="bank_account_type" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">Saving Account </label>
                    </div>
                </div>
{{--                <div class="custom-control custom-radio custom-control-inline">--}}
{{--                    <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">--}}
{{--                    <label class="custom-control-label" for="customRadioInline1">Normal Account</label>--}}
{{--                </div>--}}
{{--                <div class="custom-control custom-radio custom-control-inline">--}}
{{--                    <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">--}}
{{--                    <label class="custom-control-label" for="customRadioInline2">Saving Account </label>--}}
{{--                </div>--}}

            </div>
        </div>
        @endif
        @if(
     Auth::user()->Country == "Europe"
    || Auth::user()->Country == "France"
    || Auth::user()->Country == "France, Metropolitan"
    || Auth::user()->Country == "French Guiana"
    || Auth::user()->Country == "French Polynesia"
    || Auth::user()->Country == "French Southern Territories"
    || Auth::user()->Country == "Georgia"
    || Auth::user()->Country == "Germany"
    || Auth::user()->Country == "Greece"
    || Auth::user()->Country == "Greenland"
    || Auth::user()->Country == "Italy"
    || Auth::user()->Country == "United Kingdom"

)
            <div class="form-group">
                <label style="text-align:left;" class="control-label col-sm-3">Short Code / IBAN</label>
                <div class="col-sm-9">

                    <input class="form-control" type="text" name="iban_number"
                           value="{{ old('iban_number') ? old('iban_number') : Auth::user()->iban_number}}" {{empty(Auth::user()->iban_number) ?  '' : '' }}>


                </div>
            </div>
        @endif
{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Account number:</label>--}}
{{--            <div class="col-sm-9">--}}

{{--                <input class="form-control" type="text" name="account_name"--}}
{{--                       value="{{ old('account_name') ? old('account_name') : Auth::user()->account_name}}" {{empty(Auth::user()->account_name) ?  '' : '' }}>--}}


{{--            </div>--}}
{{--        </div>--}}


        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">@if(Auth::user()->Country == 'Pakistan') IBAN
                : <small><a href="#" onclick="Ibanimg();" data-target="#imageiban">(What is this?)</a></small> @else
                    Account No: @endif</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="account_no"
                       {{empty(Auth::user()->account_no) ?  '' : '' }}
                       value="{{ old('account_no') ? old('account_no') : Auth::user()->account_no}}"
                       {{empty(Auth::user()->account_no) ?  '' : '' }}
                       placeholder="Example: PK12ABCD1234567891234567" style="text-transform:uppercase;"
                       oninput="this.value = this.value.toUpperCase();">


            </div>
        </div>

        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Account holder phone no:</label>
            <div class="col-sm-9">

                <input class="form-control" type="text" name="acc_hold_No" placeholder="Example: 03331234567"
                       {{empty(Auth::user()->acc_hold_No) ?  '' : '' }}
                       value="{{ old('acc_hold_No') ? old('acc_hold_No') : Auth::user()->acc_hold_No}}">


            </div>
        </div>


{{--        r--}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
                <button type="button" class="btn btn-sm btn-primary o-s">Update Information</button>
                <a href="{{ route('user.kyc.get') }}" class="btn btn-sm btn-info">Update KYC</a>
            </div>
        </div>
    </div>
</form>




