<style>
    .securePinDiv input {
        padding: 13px;
        font-size: 16px;
    }
</style>

<label> 2Fa Authentication Process By</label>
<hr>
<div class="card-body">
    <form method="post" action="{{route('update_google_2fa')}}" mod="ctf">
        <div class="form-content">
            <input type="hidden" name="_key" value="{{csrf_token()}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">



            @foreach( \App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade::b4uAllowed2FaMethod() as $key => $methods)
                <div>
                    <label>
                        <input type="radio" class="form-check-input"
                               {{ \Illuminate\Support\Facades\Auth::user()->two_fa_auth_type == $methods['value'] ? 'checked': '' }}  value="{{ $methods['value'] }}"
                               name="auth_type"> {{ $methods['name'] }}

                    </label>

                    @if(!empty($methods['extraInfo']))
                        {!!  $methods['extraInfo']  !!}
                    @endif

                    <span style="float: right; font-size: 14px; letter-spacing: 2px"
                          class="badge badge-{{$methods['badge_class']}}">{{ $methods['cost'] }}</span>
                </div>
            @endforeach


            <div class="securePinDiv-dddd hide" style="margin: 10px 0 0 0;display: flex;justify-content: space-around;">

                <input type="text" class="inputs-unsp firstInput" name="fP" maxLength="1" size="1" min="0" max="9"
                       autofocus
                       pattern="[0-9]{1}"/>
                <input type="text" class="inputs-unsp" name="sP" maxLength="1" size="1" min="0" max="9"
                       pattern="[0-9]{1}"/>
                <input type="text" class="inputs-unsp" name="tP" maxLength="1" size="1" min="0" max="9"
                       pattern="[0-9]{1}"/>
                <input type="text" class="inputs-unsp" name="fuP" maxLength="1" size="1" min="0" max="9"
                       pattern="[0-9]{1}"/>
                <input type="text" class="inputs-unsp" name="fvP" maxLength="1" size="1" min="0" max="9"
                       pattern="[0-9]{1}"/>
                <input type="text" class="inputs-unsp" name="sxP" maxLength="1" size="1" min="0" max="9"
                       pattern="[0-9]{1}"/>

            </div>


            <hr>
            <button type="button" class="mt-3 btn btn-sm btn-primary o-s">Update</button>
        </div>
    </form>
</div>

<script>


    /**
     * @Purpose :: Auto focusing inputs when user tries to type OTP
     * */
    function focusAutoInputs() {
        $(document).on('keyup', '.inputs-unsp', async function () {
            if (parseInt(this.value.length) === parseInt(this.maxLength)) {
                $(this).next('input').focus();
            }
            // await isValidOtpEntered();
        });
    }

    /**
     * @Purpose :: Verify, Is valid otp entered or not.
     * */
    async function isValidOtpEntered() {
        getOTP(true).then(res => {
            res === 6 ? $('button').prop('disabled', true) : $('button').prop('disabled', false);
        });
    }

    // 3 and not verified email..


    /**
     * @Purpose :: Get One time password.
     * */
    async function getOTP(returnLength = false) {
        let enteredOtpInputBlock = $(closestForm).find('input[name=etp]');
        await $('input.inputs-unsp').each((index, values) => {
            if (index === 0) {
                enteredOtp = $(values).val();
            } else {
                enteredOtp += $(values).val();
            }
        });

        $(enteredOtpInputBlock).val(enteredOtp);

        if (returnLength) {
            return $(enteredOtpInputBlock).val().length;
        } else {
            return $(enteredOtpInputBlock).val();
        }

    }


    $(function () {
        focusAutoInputs();
        let securePinInput = $('div.securePinDiv');
        $(document).on('change', 'input[name=auth_type]', function () {
            if ($('input[name=auth_type]:checked').val() == 4) {
                $(securePinInput).removeClass('hide');
                $('button').prop('disabled', true);
            } else {
                $('button').prop('disabled', false);
                $(securePinInput).addClass('hide');
            }
        });
    })
</script>

