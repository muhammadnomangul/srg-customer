<div style="text-align: center;"><h4><label>Edit KYC Information</label></h4></div>
<hr>
<div class="card-body">
        <div class="form-group row">
            <label for="mother_name">Mother Name:</label>
            <input disabled type="text" class="form-control" placeholder="Mother Name"
                   value="{{ old('mother_name',$kyc->mother_name) }}">
        </div>
        <div class="form-group row">
            <label for="dob">Date Of Birth:</label>
            <input disabled type="text" class="form-control"
                   value="{{ old('dob',date('d-m-Y',strtotime($kyc->dob))) }}">
        </div>
        <div class="form-group row">
            @if($kyc->cnic)
                <img class="preview-img img-responsive user-image-default"
                     src="{{ $kyc->cnic_image }}">
            @endif

        </div>
        <div class="form-group row">
            @if($kyc->passport)
                <img class="preview-img img-responsive user-image-default"
                     src="{{ $kyc->passport_image }}">
            @endif

        </div>

</div>
 
