 
    <div class="card-body">
        <div class="text-center" style="width: 40%;
    margin: 0 auto;">
            @if(null !== Auth::user()->photo)
                <div class="avatar avatar-md">
                    <img style="width:150px; height:150px;" src="{{'https://storage.googleapis.com/b4ufiles/profile_Img/'.Auth::user()->photo }}" alt="..." class="avatar-img rounded-circle">
                </div>
                @else
                <div class="avatar avatar-md">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 20 20" class="avatar-img rounded-circle fill-current text-muted">
                        <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zM7 6v2a3 3 0 1 0 6 0V6a3 3 0 1 0-6 0zm-3.65 8.44a8 8 0 0 0 13.3 0 15.94 15.94 0 0 0-13.3 0z"/>
                    </svg>
                </div>
            @endif
        </div>
        <hr>

        <div class="mb-3">
           
            <div class="small text-muted text-uppercase" style="float: right;">
            @if(Auth::user()->type == 1)
                <span class="badge badge-danger">Admin</span>
            @endif

            @if(Auth::user()->type == 2)
                <span class="badge badge-success">Employee</span>
            @endif

            @if(Auth::user()->type == 0)
                <span class="badge badge-primary">Customer</span>
            @endif
            </div><br/>
            <div class="small text-muted text-uppercase"><strong>User Id: </strong><span>{{ Auth::user()->u_id }}</span></div>
          
       
        </div>

        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Parent Id: </strong><span>{{ Auth::user()->parent_id }}</span></div>
           
        </div>
        @if(Auth::user()->Country == 'United States' || Auth::user()->Country == 'United States')









        @endif
        @if(
Auth::user()->Country == "Europe"
|| Auth::user()->Country == "France"
|| Auth::user()->Country == "Canada"
|| Auth::user()->Country == "France, Metropolitan"
|| Auth::user()->Country == "French Guiana"
|| Auth::user()->Country == "French Polynesia"
|| Auth::user()->Country == "French Southern Territories"
|| Auth::user()->Country == "Georgia"
|| Auth::user()->Country == "Germany"
|| Auth::user()->Country == "Greece"
|| Auth::user()->Country == "Greenland"
|| Auth::user()->Country == "Italy"
|| Auth::user()->Country == "United Kingdom"
|| Auth::user()->Country == "United States"
|| Auth::user()->Country == "United States minor outlying islands"




)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong>Account Holder Name </strong><span>{{ Auth::user()->usa_account_holder_name }}</span></div>

            </div>
        @endif
        @if(
    Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong>Date Of Birth</strong><span>{{ Auth::user()->date_of_birth }}</span></div>

            </div>
        @endif
        @if(
   Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong>Address </strong><span>{{ Auth::user()->full_address }}</span></div>

            </div>
        @endif
        @if(
    Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"
)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Bank Name </strong><span>{{ Auth::user()->full_bank_name }}</span></div>

            </div>
        @endif
        @if(
            Auth::user()->Country == "Canada"
        )
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Institute Bank Number </strong><span>{{ Auth::user()->institute_bank_number }}</span></div>

            </div>
        @endif
        @if(
            Auth::user()->Country == "Canada"
        )
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Transit Number </strong><span>{{ Auth::user()->transit_number }}</span></div>

            </div>
        @endif
        @if(
    Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"

)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Routing Number  </strong><span>{{ Auth::user()->routing_number }}</span></div>

            </div>
        @endif
        @if(
   Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"




)
            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Bank Account Type </strong><span>{{ Auth::user()->bank_account_type }}</span></div>

            </div>
        @endif
        @if(
     Auth::user()->Country == "Europe"
    || Auth::user()->Country == "France"
    || Auth::user()->Country == "France, Metropolitan"
    || Auth::user()->Country == "French Guiana"
    || Auth::user()->Country == "French Polynesia"
    || Auth::user()->Country == "French Southern Territories"
    || Auth::user()->Country == "Georgia"
    || Auth::user()->Country == "Germany"
    || Auth::user()->Country == "Greece"
    || Auth::user()->Country == "Greenland"
    || Auth::user()->Country == "Italy"
    || Auth::user()->Country == "United Kingdom"

)


            <div class="mb-3">
                <div class="small text-muted text-uppercase"><strong> Iban Number </strong><span>{{ Auth::user()->iban_number }}</span></div>

            </div>
        @endif

        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Name: </strong><span>{{ Auth::user()->name }}</span></div>
           
        </div>

        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Email:</strong><span>{{ Auth::user()->email }}</span></div>
          
        </div>

        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Contact:</strong><span>{{ Auth::user()->phone_no }}</span></div>
         
        </div>

        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Account Details:</strong></div>
            <span>Bank: {{ Auth::user()->bank_name }}</span><br>
            <span>Account Title: {{ Auth::user()->account_name }}</span><br>
            <span>Account Number: {{ Auth::user()->account_no }}</span>
        </div>


        <div class="mb-3">
            <div class="small text-muted text-uppercase"><strong>Status</strong>  
            
            <span>
                @if(Auth::user()->status == 'active')
                    <span class="text-success">●</span>
                    <span>Active</span>
                @else
                    <span class="text-danger">●</span>
                    <span>In active</span>
                @endif
            </span>

        </div>
          
        </div>

        <div class="mb-3">
            <div class="small text-muted text-uppercase">Country<strong>  <span>{{ Auth::user()->Country }}</span></strong></div>
          
        </div>
    </div>
 
