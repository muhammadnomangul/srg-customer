<div class=" col-md-7  col-xs-12">
    <div class="sign-up  widget-shadow">
        <div style="text-align: center;padding: 20px">
            <h4><label>Edit KYC Information</label></h4>
        </div>
        <hr>
        <div class="card-body" style="padding: 20px">
            <form class="user" action="{{ route('user.kyc.update') }}" class="form-horizontal" role="form"
                  method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="form" value="kyc">
                <div class="form-group row {{ $errors->has('mother_name') ? ' has-error' : '' }}">
                    <label for="mother_name">Mother Name:</label>
                    <input type="text" class="form-control" placeholder="Mother Name"
                           value="{{ old('mother_name',$kyc->mother_name) }}" name="mother_name">
                    @if ($errors->has('mother_name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('mother_name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group row">
                    <label for="dob">Date Of Birth:</label>
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" name="dob" data-date-format="dd-mm-yyyy"
{{--                               placeholder="DD-MM-YYYY"--}}
                            @if(!empty($kyc->dob)) value="{{ old('dob',date('d-m-Y',strtotime($kyc->dob)))  }}" @endif>
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                    @if ($errors->has('dob'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group row">
                    <label for="dob">CNIC Image:</label>
                    <div id="dropzone" class="dropzone {{ $kyc->cnic ? 'dropped' : '' }}">
                        <div class="dropify-message {{ $kyc->cnic ? 'hidden' : '' }}">
                            <span class="file-icon"></span>
                            <img src="{{ asset('malik/img/cloud-upload.png') }}" width="100px">

                            <p>Drag and drop a file here or click</p>
                        </div>
                        <div class="img-thumbnail">
                            @if($kyc->cnic)
                                <img class="preview-img img-responsive user-image-default"
                                     src="{{ $kyc->cnic_image }}">
                            @endif
                        </div>

                        <div class="label">
                            <div class="dropify-infos">
                                <div class="dropzone-text">
                                    <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                </div>
                            </div>
                        </div>

                        <input type="file" class="file-upload-aws" data-name="cnic" accept="image/*"
                               data-attr="multiple">
                        <input type="hidden" name="cnic_fileurl">

                    </div>

                </div>
                <div class="form-group row">
                    <label for="dob">Passport Image:</label>
                    <div id="dropzone" class="dropzone {{ $kyc->passport ? 'dropped' : '' }}">
                        <div class="dropify-message {{ $kyc->passport ? 'hidden' : '' }}">
                            <span class="file-icon"></span>
                            <img src="{{ asset('malik/img/cloud-upload.png') }}" width="100px">

                            <p>Drag and drop a file here or click</p>
                        </div>
                        <div class="img-thumbnail">
                            @if($kyc->passport)
                                <img class="preview-img img-responsive user-image-default"
                                     src="{{ $kyc->passport_image }}">
                            @endif
                        </div>

                        <div class="label">
                            <div class="dropify-infos">
                                <div class="dropzone-text">
                                    <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                </div>
                            </div>
                        </div>

                        <input type="file" class="file-upload-aws" accept="image/*" data-name="passport"
                               data-attr="multiple">
                        <input type="hidden" name="passport_fileurl">

                    </div>

                </div>
                <div class="row" style="text-align : end;">

                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i> Save
                    </button>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-5 col-xs-12">
    <div class="sign-up  widget-shadow">
        <div style="text-align: center; padding: 20px">
            <h4><label>Next Of Kin</label></h4>
        </div>
        <div class="card-body" style="padding: 20px">
            <form class="user" action="{{ route('user.kyc.update') }}" class="form-horizontal" role="form"
                  method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="form" value="nok">
                <div class="form-group row {{ $errors->has('nok_name') ? ' has-error' : '' }}">
                    <label for="nok_name">Name:</label>

                    <input type="text" class="form-control" placeholder="Ali..."
                           value="{{ old('nok_name',$kyc->nok_name) }}" name="nok_name">
                    @if ($errors->has('nok_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nok_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row {{ $errors->has('nok_relation') ? ' has-error' : '' }}">
                    <label for="nok_relation">Relation:</label>
{{--                    <input type="text" class="form-control" placeholder="Father, Brother, Friend etc..."--}}
{{--                           value="{{ old('nok_relation',$kyc->nok_relation) }}" name="nok_relation">--}}

{{--                    <select class="form-control select2" name="Country" id="countryid"--}}
{{--                            onChange="searchBanks();" {{empty(Auth::user()->Country) ?  '' : '' }}>--}}
{{--                        <option value="">Select Country</option>--}}
{{--                        @foreach($countries as $country)--}}
{{--                            <option {{ old('Country')  == $country->country_name ? 'selected' : '' }} value="{{$country->country_name}}"--}}
{{--                                    @if(Auth::user()->Country == "$country->country_name" )selected @endif>{{$country->country_name}} </option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}

                    <select class="form-control select2" required name="nok_relation" id="bank_nameId"
                            {{empty($kyc->nok_relation) ?  '' : '' }} value="{{ old('nok_relation',$kyc->nok_relation) }}">
                        <option value="">Select Relation </option>

                        @foreach($user_kyc_relations->sortBy('name') as $relation)
                        <option {{ old('nok_relation')  == $relation->name ? 'selected' : '' }} value="{{$relation->name}}"
                                @if(isset(Auth::user()->kyc->nok_relation))
                                @if(Auth::user()->kyc->nok_relation == "$relation->name" )
                                selected
                                @endif
                                @endif
                        >
                            {{$relation->name}}
                        </option>
                        @endforeach



{{--                          @foreach($user_kyc_relations as $relation)--}}

{{--                            @if(!empty($kyc->nok_relation) && $kyc->nok_relation == $relation->name)--}}
{{--                                <option value="{{$kyc->nok_relation}}" selected>{{$kyc->nok_relation}}</option>--}}
{{--                            @endif--}}
{{--                            <option value="{{$relation->name}}" >{{$relation->name}}</option>--}}

{{--                          @endforeach--}}
{{--                            @if(!empty($kyc->nok_relation) )--}}
{{--                                <option value="{{$kyc->nok_relation}}" selected>{{$kyc->nok_relation}}</option>--}}
{{--                            @endif--}}



{{--                        @foreach($relations as $relation)--}}
{{--                            <option {{ old('nok_relation')  == $kyc->nok_relation ? 'selected' : '' }} value="{{$kyc->nok_relation}}"--}}
{{--                                    @if($kyc->nok_relation == "$kyc->nok_relation" )selected @endif>{{$kyc->nok_relation}} </option>--}}
{{--                        @endforeach--}}
{{--                        <option value="Wife">Wife / Spouse</option>--}}
{{--                        <option value="Son">Son</option>--}}
{{--                        <option value="Father">Father</option>--}}
{{--                        <option value="Brother">Brother</option>--}}
{{--                        <option value="Husband">Husband</option>--}}
{{--                        <option value="Mother">Mother</option>--}}
{{--                        <option value="Daughter">Daughter</option>--}}
                    </select>
                    @if ($errors->has('nok_relation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nok_relation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row {{ $errors->has('nok_contact_num') ? ' has-error' : '' }}">
                    <label for="nok_contact_num">Contact #:</label>
                    <input type="text" class="form-control" placeholder="Mobile Number"
                           value="{{ old('nok_contact_num',$kyc->nok_contact_num) }}" name="nok_contact_num">
                    @if ($errors->has('nok_contact_num'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nok_contact_num') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row" style="text-align : end;">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
