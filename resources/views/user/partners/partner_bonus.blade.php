    <style>
      .model{
            width:500px;
        }
        
        #box-detail > .panel.col-md-6 {
          padding-left: 0;
          padding-right: 0;
        }

    </style>
    <?php
    if (1 == \Auth::user()->plan) {
        $plan_name = 'TIFFANY';
    }
    if (2 == \Auth::user()->plan) {
        $plan_name = 'BLUE MOON';
    }
    if (3 == \Auth::user()->plan) {
        $plan_name = 'AURORA';
    }
    if (4 == \Auth::user()->plan) {
        $plan_name = 'CULLINAN';
    }
    if (5 == \Auth::user()->plan) {
        $plan_name = 'SANCY';
    }
    if (6 == \Auth::user()->plan) {
        $plan_name = 'KOH I NOOR';
    }
    ?>

    

    <div class="row">
        <div class="row-one" style="margin-top:10px;">  
            @if(app('request')->session()->get('back_to_admin'))
                <div style="float:left;">
                    <a class="btn btn-default" href="{{ url('partners_history/store') }}">Generate Bonus History</a> 
                </div>

                <div style="float:left;margin-left:20px;">
                    <a class="btn btn-default clearHist" href="{{ url('partners_history/delete') }}" >Clear Bonus History</a> 
                </div>
            @endif

            <div style="float:left;">
                <a style="margin-left:20px;" href="#" data-toggle="modal" data-target="#bonusHistoryModal" onclick="viewModelFunc({{\Auth::user()->id}})" class="btn btn-default">View Bonus History</a> 
            </div>
            <div>
                <a style="margin-left:20px;" href="#" onclick="updateRefferals({{\Auth::user()->id}})" class="btn btn-default">Update All Refferals and Plan</a>
                <h4 style="float:right;">User Plan: {{$plan_name}}</h4>
            </div>

        </div>
        <div class="clearfix" ></div>
        
        
        @if(\Auth::user()->plan >= 3)
            
            <div class="row-one" style="margin-top:10px;">
                <!--    // Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
                    <!--Total Sub Users -->
                <div class="col-md-2 col-sm-6 rp t-b">
                    <sup style="color:black;"> Max 1st Level Total </sup><br>
                    <b style="color:#490342; margin-bottom:10px;">${{number_format($maxfirstTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus 1st </sup><br>
                    <b style="color:#490342;">${{number_format($bonusFirst, 2)}}</b>
                </div>
                <div class="col-md-2 col-sm-6 rp t-b">
                    <sup style="color:black;"> Max 2nd Level Total</sup>    <br>
                    <b style="color:#490342;">${{number_format($maxsecondTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus 2nd </sup><br>
                    <b style="color:#490342;">${{number_format($bonusSecond, 2)}}</b>
                </div>
                
                <div class="col-md-2 col-sm-6 rp t-b">
                    <sup style="color:black;"> Max 3rd Level Total </sup><br>
                    <b style="color:#490342;">${{number_format($maxthirdTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus 3rd </sup><br>
                    <b style="color:#490342;">${{number_format($bonusThird, 2)}}</b>
                    <br>
                </div>
                <!--$last_profit-Div -->
                <div class="col-md-2 col-sm-6 rp t-b">
                    
                    <sup style="color:black;"> Max 4th Level Total </sup><br>
                
                    <b style="color:#490342;">${{number_format($maxfourthTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus 4th </sup><br>
                    <b style="color:#490342;">${{number_format($bonusFourth, 2)}}</b>
                    
                </div>
                <div class="col-md-2 col-sm-6 rp t-b">
                    
                    <sup style="color:black;"> Max 5th Level Total</sup><br>
                    <b style="color:#490342;">${{number_format($maxfifthTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus 5th </sup><br>
                    <b style="color:#490342;">${{number_format($bonusFifth, 2)}}</b>
                        
                </div>
                <div class="col-md-2 col-sm-6 rp t-b">
                    <sup style="color:black;"> Max Final Total</sup><br>
                    <b style="color:#490342;">${{number_format($maxTotal, 2)}}</b>
                    <br><br>
                    <sup style="color:black;">Bonus Total Earnings </sup><br>
                    <b style="color:#490342;">${{number_format($bonusTotal, 2)}}</b>
                    
                </div>
                <!--$last_bonus-->
            </div>

        @else
                

                <div class="row-one" style="margin-top:10px;">
                    <!--    // Ref Bonus Div  col-md-3 col-sm-6 rp t-b -->
                        <!--Total Sub Users -->
                    <div class="col-md-3 col-sm-6 rp t-b">
                        <sup style="color:black;"> Max 1st Level Total </sup><br>
                        <b style="color:#490342; margin-bottom:10px;">${{number_format($maxfirstTotal, 2)}}</b>
                        <br><br>
                        <sup style="color:black;">Bonus 1st </sup><br>
                        <b style="color:#490342;">${{number_format($bonusFirst, 2)}}</b>
                    </div>
                    <div class="col-md-3 col-sm-6 rp t-b">
                        <sup style="color:black;"> Max 2nd Level Total</sup>    <br>
                        <b style="color:#490342;">${{number_format($maxsecondTotal, 2)}}</b>
                        <br><br>
                        <sup style="color:black;">Bonus 2nd </sup><br>
                        <b style="color:#490342;">${{number_format($bonusSecond, 2)}}</b>
                    </div>
                    
                    <div class="col-md-3 col-sm-6 rp t-b">
                        <sup style="color:black;"> Max 3rd Level Total </sup><br>
                        <b style="color:#490342;">${{number_format($maxthirdTotal, 2)}}</b>
                        <br><br>
                        <sup style="color:black;">Bonus 3rd </sup><br>
                        <b style="color:#490342;">${{number_format($bonusThird, 2)}}</b>
                        <br>
                    </div>
                    
                    <div class="col-md-3 col-sm-6 rp t-b">
                            <sup style="color:black;"> Max Final Total</sup><br>
                            <b style="color:#490342;">${{number_format($maxTotal, 2)}}</b>
                            <br><br>
                            <sup style="color:black;">Bonus Total Earnings </sup><br>
                            <b style="color:#490342;">${{number_format($bonusTotal, 2)}}</b>
                        
                    </div>
                    <!--$last_bonus-->
                </div>

            @endif
                <div class="clearfix"> </div>

                <div class="col-lg-4 col-md-4" >

                </div>
                <div class="col-lg-4 col-md-4 col-sm-5" style="margin-bottom:5px; padding:0px;">

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4" style="margin-bottom:5px; padding:0px;">

                </div>
        </div>
        <div class="clearfix" ></div>
        @if(isset($bonusbonushistory))
        
        <div class="row-one" style="margin-top:10px;">
            <div class="col-md-2 col-sm-6 rp t-b">
                <sup style="color:black;"> Old Max 1st</sup><br>
                <b style="color:#490342; margin-bottom:10px;">${{number_format($bonushistory->maxlevel1, 2)}}</b>
                <br><br>
                <sup style="color:black;"> Current 1st </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->current_max1, 2)}}</b>
            </div>
            <div class="col-md-2 col-sm-6 rp t-b">
                <sup style="color:black;"> Old Max 2nd </sup>   <br>
                <b style="color:#490342;">${{number_format($bonushistory->maxlevel2, 2)}}</b>
                <br><br>
                <sup style="color:black;"> Current 2nd </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->current_max2, 2)}}</b>
            </div>
                    
            <div class="col-md-2 col-sm-6 rp t-b">
                <sup style="color:black;"> Old Max 3rd </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->maxlevel3, 2)}}</b>
                <br><br>
                <sup style="color:black;"> Current 3rd </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->current_max3, 2)}}</b>
            </div>
                    <!--$last_profit-Div -->
            <div class="col-md-2 col-sm-6 rp t-b">  
                <sup style="color:black;"> Old Max 4th</sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->maxlevel4, 2)}}</b>
                <br><br>
                <sup style="color:black;"> Current 4th </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->current_max4, 2)}}</b>    
            </div>
            <div class="col-md-2 col-sm-6 rp t-b">  
                <sup style="color:black;"> Old Max 5th</sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->maxlevel5, 2)}}</b>
                <br><br>
                <sup style="color:black;">Current 5th </sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->current_max5, 2)}}</b>        
            </div>
            <div class="col-md-2 col-sm-6 rp t-b">
                <sup style="color:black;">Total Bonus</sup><br>
                <b style="color:#490342;">${{number_format($bonushistory->bonus_total, 2)}}</b>
                <br><br>
                <sup style="color:black;">Current Bonus</sup><br>
                <b style="color:#490342;">${{number_format($current_level_bonus, 2)}}</b>
            </div>
        </div>
        
        <div class="clearfix" ></div>
        @endif
        <div class="row-one" style="margin-top:10px;">
            <div class="col-md-3 col-sm-6 rp t-b">
                <h4 style="margin-top:5px; text-align:center;"><label>Total Earnings</label> <br> ${{number_format($bonusTotal, 2)}}</h4>
            </div>
             
            <div class="col-md-2 col-sm-6 rp t-b">
                <h4 style="margin-top:5px; text-align:center;"><label> Profit Bonus </label> <br> ${{number_format($profitbonus, 2)}}</h4>
            </div>
            <div class="col-md-2 col-sm-6 rp t-b">
                <h4 style="margin-top:5px; text-align:center;"><label> Bonus ReInvest </label> <br> ${{number_format($bonusreinvestment, 2)}}<br></h4>
            </div>
            <div class="col-md-2 col-sm-6 rp t-b">
                <h4 style="margin-top:5px; text-align:center;"><label> Bonus Withdrawal </label><br> ${{number_format($bonuswithdrawal, 2)}}<br></h4>

            </div>
            <div class="col-md-3 col-sm-6 rp t-b">
                <h4 style="margin-top:5px; text-align:center;"><label>Final Bonus</label><br> ${{number_format($finaltotalbonus, 2)}}<br></h4>

            </div>

        </div>
        <div class="clearfix" ></div>
        
        <!-- /Deposits Details Modal -->
        <div id="bonusHistoryModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" id="bonusContent">
                </div>
            </div>
        </div>
        
        
        <script>


        $(".clearHist").click(function(){
            //  alert(" i am here");
        var conf = confirm("Warning! Do you really want to DELETE the history ??");
        if(conf){
            return true;
        }
        else{
            return false;
        }
    }); 
        
        function viewModelFunc(id)
        {
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/getBonusHistory',
                data: {id : id,"_token": "{{ csrf_token() }}",},
                //  cache: false,
                success: function(response){
                    console.log(response);
                    $("#bonusContent").html("");
                    $("#bonusContent").html(response);
                    //  $( "#ajaxval p" ).append(response);
                },error:function(response){
                    console.log(response);
                    //alert("error!!!!");
                    alert(response);
                }
            });
        }
        
        function updateRefferals(id)
        {
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/updateParnersPlans',
                data: {id : id,"_token": "{{ csrf_token() }}",},
                //  cache: false,
                success: function(response){
                    console.log(response);
                    if(response == "unsuccessful")
                    {
                        alert(" User Id is not found! Please try again!");
                    }else{
                        alert("Action Successfull,\n Please wait for the system to update Plan and Refferals.");
                    }
                    //  $( "#ajaxval p" ).append(response);
                },error:function(response){
                    console.log(response);
                    //alert("error!!!!");
                    alert(response);
                }
            });
        }
        
        
        
        </script>
        <!--<table id="myTable" class="table table-hover">
            <thead>
                <tr>
                    <th>Max 1st Level Total</th>
                    <th>Max 2nd Level Total</th>
                    <th>Max 3rd Level Total</th>
                    @if(\Auth::user()->plan >= 3)
                    <th>Max 4th Level Total</th>
                    <th>Max 5th Level Total</th>
                    @endif
                    <th>Max Final Total</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <th scope="row">${{number_format($maxfirstTotal, 2)}}</th>
                        <td>${{number_format($maxsecondTotal, 2)}}</td>
                        <td>${{number_format($maxthirdTotal, 2)}}</td>
                        @if(\Auth::user()->plan >= 3) 
                        <td>${{number_format($maxfourthTotal, 2)}}</td>
                        <td>${{number_format($maxfifthTotal, 2)}}</td>
                        @endif
                        <td><strong>${{number_format($maxTotal, 2)}}</strong></td>
                    <tr>
                    <tr>
                            <th >Bonus First</th>
                            <th>Bonus Second</th>
                            <th>Bonus Third</th>
                            @if(\Auth::user()->plan >= 3)
                            <th>Bonus Fourth</th>
                            <th>Bonus Fifth</th>
                            @endif
                            <th>Bonus Total Earning</th>
                    </tr>
                    <tr>

                            <th scope="row">${{number_format($bonusFirst, 2)}}</th>
                            <td>${{number_format($bonusSecond, 2)}}</td>
                            <td>${{number_format($bonusThird, 2)}}</td>
                            @if(\Auth::user()->plan >= 3) 
                            <td>${{number_format($bonusFourth, 2)}}</td>
                            <td>${{number_format($bonusFifth, 2)}}</td>
                            @endif
                            <td><strong>${{number_format($bonusTotal, 2)}}</strong></td>
                    </tr>
                    <tr>    
                            <th>Profit Bonus</th>
                            <th>Bonus ReInvestment</th>
                            <th>Bonus Withdrawal</th>
                            @if(\Auth::user()->plan >= 3)
                            <th></th>
                            @endif
                            <th>Bonus Final Earnings</th>
                    </tr>

                    <tr>
                            <th scope="row">${{number_format($profitbonus, 2)}}</th>
                            <td>${{number_format($bonusreinvestment, 2)}}</td>
                            <td>${{number_format($bonuswithdrawal, 2)}}</td>
                            @if(\Auth::user()->plan >= 3) 
                            <td> </td>
                            <td> </td>
                            @endif
                            <td><strong>${{number_format($finaltotalbonus, 2)}}</strong></td>
                   </tr>
                    
                
            </tbody>

        </table>-->
     
   
