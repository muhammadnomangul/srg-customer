<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>

{{--@include('messages')--}}

{{--@if(app('request')->session()->get('back_to_admin'))--}}

{{--    @include('user.partners.partner_bonus')--}}

{{--@endif--}}

<div class="clearfix"></div>
<div class="row-one" style="margin-top:10px;">
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($BlueMoonPlanChildsList as $user){{$user->child_u_id }}&#10;@endforeach">
                <label>BlueMoon Childs</label><br>{{count($BlueMoonPlanChildsList)}}</p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($SilverRankChildsList as $user){{$user->child_u_id }}&#10;@endforeach">
                <label>SilverRank Childs</label><br>{{count($SilverRankChildsList)}}</p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($AuroraPlanChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>Aurora Childs</label><br>{{count($AuroraPlanChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($CoordinatorRankChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>CoordinatorRank Childs</label><br>{{count($CoordinatorRankChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($CullinanPlanChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>Cullinan Childs</label><br>{{count($CullinanPlanChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($DiamondRankChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>DiamondRank Childs</label><br>{{count($DiamondRankChildsList)}}<br></p></h4>
    </div>
</div>
<div class="clearfix"></div>

<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">

    <table id="myTable" class="table table-hover">
        <thead>
        <tr>
            <th>First Level Total</th>
            <th>Second Level Total</th>
            <th>Third Level Total</th>
            @if(\Auth::user()->plan >= 3)
                <th>Fourth Level Total</th>
                <th>Fifth Level Total</th>
            @endif
            <th>Final Total</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <th scope="row">${{number_format($firstTotal, 2)}}</th>
            <td>${{number_format($secondTotal, 2)}}</td>
            <td>${{number_format($thirdTotal, 2)}}</td>
            @if(\Auth::user()->plan >= 3)
                <td>${{number_format($fourthTotal, 2)}}</td>
                <td>${{number_format($fifthTotal, 2)}}</td>
            @endif
            <td><strong>${{number_format($totalAttractiveFund, 2)}}</strong></td>
        <tr>
        </tbody>
    </table>


    <div id="tabs">

        <ul>
            <li><a href="#tabs-1">1st Level Referrals({{count( $levelReferrals[1] )}})</a></li>
            <li><a href="#tabs-2">2nd Level Referrals({{count( $levelReferrals[2] )}}) </a></li>
            <li><a href="#tabs-3">3rd Level Referrals({{count( $levelReferrals[3])}})</a></li>
            @if(\Auth::user()->plan >= 3)
                <li><a href="#tabs-4">4th Level ({{count( $levelReferrals[4])}})</a></li>
                <li><a href="#tabs-5">5th Level ({{count( $levelReferrals[5])}})</a></li>
            @endif

            {{--            @if(app('request')->session()->get('back_to_admin'))--}}
            {{--                <li><a href="#Deposit-1">1st Level Deposit ({{count( $levelReferralDeposit[1] )}})</a></li>--}}
            {{--                <li><a href="#Deposit-2">2nd Level Deposit ({{count( $levelReferralDeposit[2] )}}) </a></li>--}}
            {{--                <li><a href="#Deposit-3">3rd Level Deposit ({{count( $levelReferralDeposit[3])}})</a></li>--}}
            {{--                @if(\Auth::user()->plan >= 3)--}}
            {{--                    <li><a href="#Deposit-4">4th Level Deposit({{count( $levelReferralDeposit[4])}})</a></li>--}}
            {{--                    <li><a href="#Deposit-5">5th Level Deposit({{count( $levelReferralDeposit[5])}})</a></li>--}}
            {{--                @endif--}}
            {{--            @endif--}}
        </ul>

        @foreach($levelReferrals as $level=>$referrals)
            @include('user.partners._table',compact('level','referrals'))
        @endforeach

        {{--        @if(app('request')->session()->get('back_to_admin'))--}}
        {{--            @foreach($levelReferralDeposit as $level=>$referralDeposit)--}}
        {{--                @include('user.partners._deposit_table',compact('level','referralDeposit'))--}}
        {{--            @endforeach--}}
        {{--        @endif--}}


    </div>

</div>
<!-- Partners Details Modal -->
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ucontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /Partners Details Modal -->


<script>
    function viewUserDetailsFunc(id) {
        //alert(id);
        var partner = 1;
        postData('{{{URL::to("")}}}/dashboard/view-partner-details', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                return response.text()
            })
            .then(function (html) {
                $(".ucontent").html(JSON.parse(html));
            })
            .catch(function (error) {
                $('#PartnersDetailsModal').modal('hide');
                alert('Invalid user reference');
            });

    }


    $('#myTable1').DataTable({
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#myTable2').DataTable({
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#myTable3').DataTable({
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#myTable4').DataTable({
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#myTable5').DataTable({
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#deposit-1').DataTable({
        "pagingType": "full_numbers"
    });

    $('#deposit-2').DataTable({
        "pagingType": "full_numbers"
    });

    $('#deposit-3').DataTable({
        "pagingType": "full_numbers"
    });

    $('#deposit-4').DataTable({
        "pagingType": "full_numbers"
    });

    $('#deposit-5').DataTable({
        "pagingType": "full_numbers"
    });

    //});


    //  $( function() {

    $("#tabs").tabs();
    $("#Deposit-").tabs();

    //} );
</script>
