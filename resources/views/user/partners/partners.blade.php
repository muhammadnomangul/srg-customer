@include('header')
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Referral Partners</h3>
        <div id="contento">
            <div class="loader" style="width: 200px ; height: 200px; margin: auto;"></div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        fetch("{{route('partners_after2')}}")
            .then(function (response) {
                return response.text()
            }).then(function (res) {
             // console.log(res);

            $("#contento").html(res);

        });
    });

</script>


@include('footer')
