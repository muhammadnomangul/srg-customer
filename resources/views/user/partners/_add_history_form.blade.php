<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="plan">Chose Plan</label>
		    <select class="form-control" name="plan" id="plan" required>
		      <option selected>Select Plan</option>
		      <option value="1">TIFFANY</option>
		      <option value="2">BLUE MOON</option>
		      <option value="3">AURORA</option>
		      <option value="4">CULLINAN</option>
		      <option value="5">SANCY</option>
		      <option value="6">KOH I NOOR</option>
		    </select>
	  	</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    <label for="total_bonus">Bonus Total</label>
		    <input type="text" class="form-control" id="total_bonus" name="total_bonus" placeholder="0" required>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    <label for="maxlevel1">Attractive Funds (Max Level 1)</label>
		    <input type="text" class="form-control" id="maxlevel1" name="maxlevel1" placeholder="0">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		    <label for="maxlevel2">AttractiveFunds (Max Level 2)</label>
		    <input type="text" class="form-control" id="maxlevel2" name="maxlevel2" placeholder="0">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    <label for="maxlevel3">AttractiveFunds (Max Level 3)</label>
		    <input type="text" class="form-control" id="maxlevel3" name="maxlevel3" placeholder="0">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    <label for="maxlevel4">AttractiveFunds (Max Level 4)</label>
		    <input type="text" class="form-control" id="maxlevel4" name="maxlevel4" placeholder="0">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    <label for="maxlevel5">AttractiveFunds (Max Level 5)</label>
		    <input type="text" class="form-control" id="maxlevel5" name="maxlevel5" placeholder="0">
		</div>
	</div>

	<div class='col-md-6'>
	    <div class="form-group">
	        <label for="created_at">Created At</label>
	        <input type='text' class="form-control" id="created_at" name="created_at" required>
	    </div>
	</div>
	
</div>

		
{!! Form::submit('Create History',['class' => 'btn btn-primary pull-right']) !!}


<script type="text/javascript">

	$(function() {
	  $('#created_at').datepicker({
	  	maxDate: -1,
	  });
	});
	
</script>