@include('header')
<style>
    .play {
        background: red;
        border-radius: 50% / 10%;
        color: #FFFFFF;
        font-size: 2em; /* change this to change size */
        height: 3em;
        margin: 20px auto;
        padding: 0;
        position: relative;
        text-align: center;
        text-indent: 0.1em;
        transition: all 150ms ease-out;
        width: 4em;
    }

    .play:hover {
        background: darkorange;
    }

    .play::before {
        background: inherit;
        border-radius: 5% / 50%;
        bottom: 9%;
        content: "";
        left: -5%;
        position: absolute;
        right: -5%;
        top: 9%;
    }

    .play::after {
        border-style: solid;
        border-width: 1em 0 1em 1.732em;
        border-color: transparent transparent transparent rgba(255, 255, 255, 0.75);
        content: ' ';
        font-size: 0.75em;
        height: 0;
        margin: -1em 0 0 -0.75em;
        top: 50%;
        position: absolute;
        width: 0;
    }

</style>
<div id="page-wrapper">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Set up Pin</div>

                        <div class="panel-body" style="text-align: center;">
                            <p>Set up your two factor authentication by entering six digit Pin, Beware, this PIN will
                                use in 2FA, don't share with anyone
                                otherwise, you might be loss</p>
                            <p>
                                چھ عدد پن داخل کرکے اپنی دو عنصر کی توثیق کریں ، خبردار ، یہ پن 2 ایف اے میں استعمال
                                ہوگا ، کسی کے ساتھ اشتراک نہیں کریں گے۔
                                بصورت دیگر ، آپ کو نقصان ہوسکتا ہے
                            </p>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Authenticate</div>
                        @include('messages')
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{ route('setup_saved_pin_auth') }}"
                            >
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="one_time_password" class="col-md-4 control-label">One Time Password
                                    </label>

                                    <div class="col-md-6">
                                        <div class="verificationInputBlock">
                                            <input type="text" class="inputs" name="one" maxLength="1" size="1" min="0"
                                                   max="9" autofocus pattern="[0-9]{1}" required="required"/>
                                            <input type="text" class="inputs" name="two" maxLength="1" size="1" min="0"
                                                   max="9" pattern="[0-9]{1}" required="required"/>
                                            <input type="text" class="inputs" name="three" maxLength="1" size="1"
                                                   min="0" max="9" pattern="[0-9]{1}" required="required"/>
                                            <input type="text" class="inputs" name="four" maxLength="1" size="1" min="0"
                                                   max="9" pattern="[0-9]{1}" required="required"/>
                                            <input type="text" class="inputs" name="five" maxLength="1" size="1" min="0"
                                                   max="9" pattern="[0-9]{1}" required="required"/>
                                            <input type="text" class="inputs" name="six" maxLength="1" size="1" min="0"
                                                   max="9" pattern="[0-9]{1}" required="required"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Proceed
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</div>
@include('modals')
@include('footer')
