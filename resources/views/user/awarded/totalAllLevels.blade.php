<table id="myTable" class="table table-hover">

    <thead>
    <tr>
        <th>L-1Total</th>
        <th>L-2 Total</th>
        <th>L-3 Total</th>
        <th>L-4 Total</th>
        <th>L-5 Total</th>
        <th>L-6 Total</th>
        <th>L-7 Total</th>
        <th>L-8 Total</th>
        <th>L-9 Total</th>
        <th>L-10 Total</th>


    </tr>

    </thead>

    <tbody>

    <tr>

        <th scope="row">${{number_format($firstTotal, 2)}}</th>

        <td>${{number_format($secondTotal, 2)}}</td>

        <td>${{number_format($thirdTotal, 2)}}</td>

        <td>${{number_format($fourthTotal, 2)}}</td>

        <td>${{number_format($fifthTotal, 2)}}</td>

        <td>${{number_format($sixthTotal, 2)}}</td>

        <td>${{number_format($seventhTotal, 2)}}</td>

        <td>${{number_format($eighthTotal, 2)}}</td>

        <td>${{number_format($ninthTotal, 2)}}</td>

        <td>${{number_format($tenthTotal, 2)}}</td>

    <tr>

    </tbody>

</table>

<table id="myTable2" class="table table-hover">

    <thead>
    <tr>

        <th>L-11 Total</th>
        <th>L-12 Total</th>
        <th>L-13 Total</th>
        <th>L-14 Total</th>
        <th>L-15 Total</th>
        <th>L-16 Total</th>
        <th>L-17 Total</th>
        <th>L-18 Total</th>
        <th>L-19 Total</th>
        <th>L-20 Total</th>
        <th>Final Total</th>

    </tr>

    </thead>

    <tbody>

    <tr>

        <th scope="row">$
        {{number_format($eleventhTotal, 2)}}</td>

        <td>${{number_format($twelvethTotal, 2)}}</td>

        <td>${{number_format($thirteenthTotal, 2)}}</td>

        <td>${{number_format($fourteenthTotal, 2)}}</td>

        <td>${{number_format($fifteenthTotal, 2)}}</td>

        <td>${{number_format($sixteenthTotal, 2)}}</td>

        <td>${{number_format($seventeenthTotal, 2)}}</td>

        <td>${{number_format($eighteenthTotal, 2)}}</td>

        <td>${{number_format($ninteenthTotal, 2)}}</td>

        <td>${{number_format($twentiethTotal, 2)}}</td>

        <td><strong>${{number_format($account_balance, 2)}}</strong></td>

    <tr>

    </tbody>

</table>