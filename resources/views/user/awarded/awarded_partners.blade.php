@include('header')

<!-- //header-ends -->

<!-- main content start-->
<?php


?>
<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>
<div id="page-wrapper">
    <div class="main-page">

        <h3 class="title1">Distributor Referrals Partners</h3>

        @include('messages')

        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">

            {{--        <!--@include('user.awarded.totalAllLevels')-->--}}

            <h3 style="margin-top:5px; text-align:center;">Distributor Referrals Details </h3><br>
            {{--            <h3 style="margin-top:5px; text-align:center;"> Total Investments :--}}
            {{--                ${{number_format($account_balance, 2)}}--}}
            {{--            </h3>--}}
            <br>
            <div id="tabs">
                <ul>

                    @for($x=1; $x<21; $x++)
                        <li><a href="{{ url("dashboard/awarded_partners/{$x}") }}">Level-{{$x}} ({{$user_level_count}})
                                @if($level == $x)
                                    <br><small>${{number_format($total, 2)}}</small>
                                @endif
                            </a>
                        </li>
                    @endfor

                </ul>

                @include('user.awarded.table')
                @include('user.awarded.table2')
            </div>
        </div>

    </div>

</div>

<!-- Partners Details Modal -->
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ucontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /Partners Details Modal -->

@include('footer')

<script>

    // function viewUserDetailsFunc(id) {
    function viewDetailsFunc(id) {
        var partner = 1;
        postData('{{{URL::to("")}}}/dashboard/view-partner-details', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                //console.log(response)
                return response.text()
            })
            .then(function (html) {
                html = JSON.parse(html);
                $(".ucontent").html("");
                $(".ucontent").html(html);
            }) // JSON-string from `response.json()` call
            .catch(function (error) {
                //console.error(error)
                alert("error!!!!");
            });
    }


    $(document).ready(function () {

        $('.table').DataTable({
            "pagingType": "full_numbers",
            "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
            buttons: ['excel'],
            "lengthMenu": [[10, 100, 500, 1000, -1], [10, 100, 500, 1000, "All"]]
        });


    });

    $(function () {
        $("#tabs").tabs();
    });

</script>