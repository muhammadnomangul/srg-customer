<?php

$row11 = 1; $counter11 = 0;
$row12 = 1; $counter12 = 0;
$row13 = 1; $counter13 = 0;
$row14 = 1; $counter14 = 0;
$row15 = 1; $counter15 = 0;
$row16 = 1; $counter16 = 0;
$row17 = 1; $counter17 = 0;
$row18 = 1; $counter18 = 0;
$row19 = 1; $counter19 = 0;
$row20 = 1; $counter20 = 0;

?>

<!-- TAB 11 -->


@if($level == 11)
    <div id="tabs-11">

        <table id="myTable11" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>
            </tr>

            </thead>

            <tbody>

            @if(isset($eleventhLine))

                @for($i=0; $i < count($eleventhLine); $i++)

                    @foreach($eleventhLine[$counter11] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row11}}</th>
                            <td>{{$lines->name}}</td>
                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>
                            <td>{{$lines->parent_id}} </td>
                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row11++;?>

                    @endforeach

                    <?php $counter11++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 12)
    <!-- TAB 12 -->
    <div id="tabs-12">

        <table id="myTable12" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>
            </tr>

            </thead>

            <tbody>

            @if(isset($twelvethLine))

                @for($i=0; $i < count($twelvethLine); $i++)

                    @foreach($twelvethLine[$counter12] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row12}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row12++;?>

                    @endforeach

                    <?php $counter12++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 13)
    <!-- TAB 13 -->
    <div id="tabs-13">

        <table id="myTable13" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total Investment USD</th>
                <th>Created At</th>
            </tr>

            </thead>


            <tbody>

            @if(isset($thirteenthLine))

                @for($i=0; $i < count($thirteenthLine); $i++)

                    @foreach($thirteenthLine[$counter13] as $lines)


                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row13}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal"
                                                    href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>
                        </tr>

                        <?php $row13++;?>

                    @endforeach

                    <?php $counter13++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 14)
    <!-- TAB 14 -->
    <div id="tabs-14">

        <table id="myTable14" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($fourteenthLine))


                @for($i=0; $i < count($fourteenthLine); $i++)

                    @foreach($fourteenthLine[$counter14] as $lines)


                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row14}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal"
                                                    href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row14++;?>

                    @endforeach

                    <?php $counter14++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 15)
    <!-- TAB 15 -->
    <div id="tabs-15">

        <table id="myTable15" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($fifteenthLine))

                @for($i=0; $i < count($fifteenthLine); $i++)

                    @foreach($fifteenthLine[$counter15] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row15}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row15++;?>

                    @endforeach

                    <?php $counter15++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 16)
    <!-- TAB 16 -->
    <div id="tabs-16">

        <table id="myTable16" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($sixteenthLine))

                @for($i=0; $i < count($sixteenthLine); $i++)

                    @foreach($sixteenthLine[$counter16] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row16}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row16++;?>

                    @endforeach

                    <?php $counter16++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 17)
    <!-- TAB 17 -->
    <div id="tabs-17">

        <table id="myTable17" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($seventeenthLine))

                @for($i=0; $i < count($seventeenthLine); $i++)

                    @foreach($seventeenthLine[$counter17] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row17}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row17++;?>

                    @endforeach

                    <?php $counter17++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 18)
    <!-- TAB 18 -->
    <div id="tabs-18">

        <table id="myTable18" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($eighteenthLine))

                @for($i=0; $i < count($eighteenthLine); $i++)

                    @foreach($eighteenthLine[$counter18] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row18}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal"
                                                    href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row18++;?>

                    @endforeach

                    <?php $counter18++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 19)

    <!-- TAB 19 -->
    <div id="tabs-19">

        <table id="myTable19" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($ninteenthLine))

                @for($i=0; $i < count($ninteenthLine); $i++)

                    @foreach($ninteenthLine[$counter19] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row19}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal"
                                                    href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row19++;?>

                    @endforeach

                    <?php $counter19++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 20)
    <!-- TAB 20 -->
    <div id="tabs-20">

        <table id="myTable20" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>
            </tr>

            </thead>

            <tbody>

            @if(isset($twentiethLine))

                @for($i=0; $i < count($twentiethLine); $i++)

                    @foreach($twentiethLine[$counter20] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row20}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal"
                                                    data-target="#PartnersDetailsModal"
                                                    href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a>
                            </td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row20++;?>

                    @endforeach

                    <?php $counter20++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
    <!-- TAB 20 END -->
@endif









