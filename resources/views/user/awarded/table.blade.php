<?php

$counter = 1;
$row2 = 1; $counter2 = 0;
$row3 = 1; $counter3 = 0;
$row4 = 1; $counter4 = 0;
$row5 = 1; $counter5 = 0;
$row6 = 1; $counter6 = 0;
$row7 = 1; $counter7 = 0;
$row8 = 1; $counter8 = 0;
$row9 = 1; $counter9 = 0;
$row10 = 1; $counter10 = 0;

?>


@if($level == 1)
    <div id="tabs-1">
        <table id="myTable1" class="table table-hover">
            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>
            <tbody>
            @if(isset($firstLine))

                @foreach($firstLine as $lines)



                    @if(isset($lines->u_id))

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$counter}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>
                        </tr>

                    @endif

                    <?php $counter++; ?>

                @endforeach

            @endif
            </tbody>
        </table>
    </div>
@elseif($level == 2)
    <div id="tabs-2">
        <table id="myTable2" class="table table-hover">

            <thead>
            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>
            </tr>
            </thead>
            <tbody>

            @if(isset($secondLine))

                @for($i=0; $i < count($secondLine); $i++)

                    @foreach($secondLine[$counter2] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row2}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>
                        </tr>

                        <?php $row2++;?>

                    @endforeach

                    <?php $counter2++;?>

                @endfor

            @endif

            </tbody>

        </table>
    </div>
@elseif($level == 3)
    <!-- TAB 3 -->
    <div id="tabs-3">

        <table id="myTable3" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total Investment USD</th>
                <th>Created At</th>
            </tr>

            </thead>


            <tbody>

            @if(isset($thirdLine))


                @for($i=0; $i < count($thirdLine); $i++)

                    @foreach($thirdLine[$counter3] as $lines)


                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row3}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>
                        </tr>

                        <?php $row3++;?>

                    @endforeach

                    <?php $counter3++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 4)
    <!-- TAB 4 -->
    <div id="tabs-4">
        <table id="myTable4" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($fourthLine))


                @for($i=0; $i < count($fourthLine); $i++)

                    @foreach($fourthLine[$counter4] as $lines)


                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row4}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row4++;?>

                    @endforeach

                    <?php $counter4++;?>

                @endfor

            @endif

            </tbody>

        </table>
    </div>
@elseif($level == 5)
    <!-- TAB 5 -->
    <div id="tabs-5">

        <table id="myTable5" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($fifthLine))

                @for($i=0; $i < count($fifthLine); $i++)

                    @foreach($fifthLine[$counter5] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row5}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row5++;?>

                    @endforeach

                    <?php $counter5++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 6)
    <!-- TAB 6 -->
    <div id="tabs-6">

        <table id="myTable6" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($sixthLine))

                @for($i=0; $i<count($sixthLine); $i++)


                    @foreach($sixthLine[$counter6] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row6}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row6++;?>

                    @endforeach

                    <?php $counter6++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 7)
    <!-- TAB 7 -->
    <div id="tabs-7">

        <table id="myTable7" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($seventhLine))

                @for($i=0; $i < count($seventhLine); $i++)

                    @foreach($seventhLine[$counter7] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row7}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row7++;?>

                    @endforeach

                    <?php $counter7++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 8)
    <!-- TAB 8 -->
    <div id="tabs-8">

        <table id="myTable8" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($eighthLine))

                @for($i=0; $i < count($eighthLine); $i++)

                    @foreach($eighthLine[$counter8] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row8}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row8++;?>

                    @endforeach

                    <?php $counter8++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 9)
    <!-- TAB 9 -->
    <div id="tabs-9">

        <table id="myTable9" class="table table-hover">

            <thead>

            <tr>

                <th>Sr.#</th>

                <th>User Name</th>

                <th>User ID</th>

                <th>Parent ID</th>

                <th>Total USD</th>
                <th>Created At</th>

            </tr>

            </thead>

            <tbody>

            @if(isset($ninthLine))

                @for($i=0; $i < count($ninthLine); $i++)

                    @foreach($ninthLine[$counter9] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row9}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row9++;?>

                    @endforeach

                    <?php $counter9++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@elseif($level == 10)
    <!-- TAB 10 -->
    <div id="tabs-10">

        <table id="myTable10" class="table table-hover">

            <thead>

            <tr>
                <th>Sr.#</th>
                <th>User Name</th>
                <th>User ID</th>
                <th>Parent ID</th>
                <th>Total USD</th>
                <th>Created At</th>
            </tr>

            </thead>

            <tbody>

            @if(isset($tenthLine))

                @for($i=0; $i < count($tenthLine); $i++)

                    @foreach($tenthLine[$counter10] as $lines)

                        <tr @if($lines->total == 0) style="color:red;" @endif >

                            <th scope="row">{{$row10}}</th>

                            <td>{{$lines->name}}</td>

                            <td class="linksAll"><a data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                                                    onclick="viewDetailsFunc({{$lines->id}})">{{$lines->u_id}}</a></td>

                            <td>{{$lines->parent_id}} </td>

                            <td> ${{number_format($lines->total, 2)}}</td>
                            <td>{{$lines->created_at}}</td>

                        </tr>

                        <?php $row10++;?>

                    @endforeach

                    <?php $counter10++;?>

                @endfor

            @endif

            </tbody>

        </table>

    </div>
@endif

						
					