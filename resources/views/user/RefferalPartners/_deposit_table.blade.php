
<div id="Deposit-{{$level}}">

    <table id="deposit-{{$level}}" class="table table-hover">
        <thead>
            <tr>
                <th>Sr.#</th>
                {{-- <th>User Name </th> --}}
                <th>Deposit ID </th>
                <th>B4U ID </th>
                <th>Currency </th>
                <th>Payment Type </th>
                <th>Amount USD </th>
    			<th>Status </th>
    			<!--<th>Total Sold </th>-->
                <th>Approved Date</th>
                <th>Sold Date</th>
            </tr>
        </thead>

        <tbody>
            @foreach($referralDeposit as $key => $deposit)
                <tr
		  	        @if ($deposit->total_amount == 0)
            	       style="color:red"; 
				    @endif 
			     >

                    <th scope="row">{{$key+1}}</th>
                    <td>{{'D-'.$deposit->id}}</td>
                    {{-- <td>{{$deposit->duser->name}}</td> --}}
                    <td >
                        <a class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                            onclick="viewUserDetailsFunc({{$deposit->unique_id}})">{{$deposit->unique_id }}
                        </a>
                    </td>
                    <td>{{$deposit->currency}} </td>
                    <td>{{$deposit->trans_type}} </td>
                    <td> $ {{number_format($deposit->total_amount, 2)}}</td>
                    <td>{{$deposit->status}} </td>
    				{{-- <td> ${{number_format(@$deposit->child->sold_new_investment, 2)}}</td> --}}
                    <td>{{date("Y-m-d",strtotime($deposit->approved_at))}}</td>
                    <td>
                        @if($deposit->pre_status == 'Approved' && $deposit->status == 'Sold')
                            {{date("Y-m-d",strtotime($deposit->sold_at))}}
                        @else
                            {!!'Not Sold Yet.'!!}
                        @endif
                    </td>
                </tr>

            @endforeach

        </tbody>
    </table>

</div>
	
