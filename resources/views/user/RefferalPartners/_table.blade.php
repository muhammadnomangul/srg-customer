
<div id="tabs-{{$level}}" style="overflow:scroll;">

    <table id="myTable{{$level}}" class="table table-hover">

        <thead>
            <tr>
                <th>Sr. #</th>
                <th>User Name </th>
                <th>Mobile No </th>
                <th>Email</th>
                <th>B4U ID </th>
                <th>Parent ID </th>
                <th>Total USD </th>
    			<th>Total Reinvest </th>
    			<!--<th>Total Sold </th>-->
                <th>Created At </th>
            </tr>
        </thead>

        <tbody>
            @foreach($referrals as $referral)
                <tr @if ($referral->attractive_funds == 0) style="color:red"; @endif>

                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{@@$referral->child->name}}</td>
                    <td>{{@@$referral->child->phone_no}}</td>
                    <td>{{@@$referral->child->email}}</td>
                    <td >
                        <a class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#"
                            onclick="viewUserDetailsFunc({{@$referral->child->id}})">{{@$referral->child->u_id}}
                        </a>
                    </td>
                    <td>{{@$referral->child->parent_id}} </td>
                    <td> ${{number_format($referral->attractive_funds, 2)}}</td>
    				<td> ${{number_format($referral->active_reinvestment, 2)}}</td>
    				{{-- <td> ${{number_format(@$referral->child->sold_new_investment, 2)}}</td> --}}
                    <td>{{date("Y-m-d",strtotime(@$referral->child->created_at))}}</td>
                </tr>

            @endforeach

        </tbody>

    </table>
</div>