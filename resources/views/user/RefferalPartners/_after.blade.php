<style>
    .linksAll a:link {
        color: #2c699e !important;
    }
</style>

{{--@include('messages')--}}

{{--@if(app('request')->session()->get('back_to_admin'))--}}

{{--    @include('user.partners.partner_bonus')--}}

{{--@endif--}}

<div class="clearfix"></div>
<div class="row-one" style="margin-top:10px;">
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($BlueMoonPlanChildsList as $user){{$user->child_u_id }}&#10;@endforeach">
                <label>BlueMoon Childs</label><br>{{count($BlueMoonPlanChildsList)}}</p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($SilverRankChildsList as $user){{$user->child_u_id }}&#10;@endforeach">
                <label>SilverRank Childs</label><br>{{count($SilverRankChildsList)}}</p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($AuroraPlanChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>Aurora Childs</label><br>{{count($AuroraPlanChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($CoordinatorRankChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>CoordinatorRank Childs</label><br>{{count($CoordinatorRankChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($CullinanPlanChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>Cullinan Childs</label><br>{{count($CullinanPlanChildsList)}}<br></p></h4>
    </div>
    <div class="col-md-2 col-sm-6 rp t-b">
        <h4 style="margin-top:5px; text-align:center;">
            <p title="@foreach($DiamondRankChildsList as $user){{$user->child_u_id}}&#10;@endforeach">
                <label>DiamondRank Childs</label><br>{{count($DiamondRankChildsList)}}<br></p></h4>
    </div>
</div>
<div class="clearfix"></div>

<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">

    <table id="myTable" class="table table-hover">
        <thead>
        <tr>
            <th>First Level Total</th>
            <th>Second Level Total</th>
            <th>Third Level Total</th>
            @if(\Auth::user()->plan >= 3)
                <th>Fourth Level Total</th>
                <th>Fifth Level Total</th>
            @endif
            <th>Final Total</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <th scope="row">${{number_format($firstTotal, 2)}}</th>
            <td>${{number_format($secondTotal, 2)}}</td>
            <td>${{number_format($thirdTotal, 2)}}</td>
            @if(\Auth::user()->plan >= 3)
                <td>${{number_format($fourthTotal, 2)}}</td>
                <td>${{number_format($fifthTotal, 2)}}</td>
            @endif
            <td><strong>${{number_format($totalAttractiveFund, 2)}}</strong></td>
        <tr>
        </tbody>
    </table>


    <div id="tabs">

        <ul>
            <li><a href="#tabs-1">1st Level Referrals({{count( $levelReferrals[1] )}})</a></li>
            <li><a href="#tabs-2">2nd Level Referrals({{count( $levelReferrals[2] )}}) </a></li>
            <li><a href="#tabs-3">3rd Level Referrals({{count( $levelReferrals[3])}})</a></li>
            @if(\Auth::user()->plan >= 3)
                <li><a href="#tabs-4">4th Level ({{count( $levelReferrals[4])}})</a></li>
                <li><a href="#tabs-5">5th Level ({{count( $levelReferrals[5])}})</a></li>
            @endif

            {{--            @if(app('request')->session()->get('back_to_admin'))--}}
            {{--                <li><a href="#Deposit-1">1st Level Deposit ({{count( $levelReferralDeposit[1] )}})</a></li>--}}
            {{--                <li><a href="#Deposit-2">2nd Level Deposit ({{count( $levelReferralDeposit[2] )}}) </a></li>--}}
            {{--                <li><a href="#Deposit-3">3rd Level Deposit ({{count( $levelReferralDeposit[3])}})</a></li>--}}
            {{--                @if(\Auth::user()->plan >= 3)--}}
            {{--                    <li><a href="#Deposit-4">4th Level Deposit({{count( $levelReferralDeposit[4])}})</a></li>--}}
            {{--                    <li><a href="#Deposit-5">5th Level Deposit({{count( $levelReferralDeposit[5])}})</a></li>--}}
            {{--                @endif--}}
            {{--            @endif--}}
        </ul>



            <div id="tabs-1" style="overflow:scroll;">
                <table id="myTable1" style="width: 100%" class="table table-hover">
                </table>
            </div>
            <div id="tabs-2" style="overflow:scroll;">
                <table id="myTable2" style="width: 100%" class="table table-hover">
                </table>
            </div><div id="tabs-3" style="overflow:scroll;">
                <table id="myTable3" style="width: 100%" class="table table-hover">
                </table>
            </div>
        @if(\Auth::user()->plan >= 3)
            <div id="tabs-4" style="overflow:scroll;">
                <table id="myTable4" style="width: 100%" class="table table-hover">
                </table>
            </div>
            <div id="tabs-5" style="overflow:scroll;">
                <table id="myTable5" style="width: 100%"   class="table table-hover">
                </table>
            </div>
        @endif
{{--        @endforeach--}}

        {{--        @if(app('request')->session()->get('back_to_admin'))--}}
        {{--            @foreach($levelReferralDeposit as $level=>$referralDeposit)--}}
        {{--                @include('user.partners._deposit_table',compact('level','referralDeposit'))--}}
        {{--            @endforeach--}}
        {{--        @endif--}}


    </div>

</div>
<!-- Partners Details Modal -->
<div id="PartnersDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ucontent">
            <!-- The User details-->
            <!-- Trigger the Modal -->
        </div>
    </div>
</div>

<!-- /Partners Details Modal -->


<script>
    function viewUserDetailsFunc(id) {
        //alert(id);
        var partner = 1;
        postData('{{{URL::to("")}}}/dashboard/view-partner-details', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                return response.text()
            })
            .then(function (html) {
                $(".ucontent").html(JSON.parse(html));
            })
            .catch(function (error) {
                $('#PartnersDetailsModal').modal('hide');
                alert('Invalid user reference');
            });

    }

    var tab1 = 0;
    var tab2 = 0;
    var tab3 = 0;
    var tab4 = 0;
    var tab5 = 0;
    $('#myTable1').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": true,
        "ajax": {
            "url": "{{url('dashboard/refferal_partner_level_1/json')}}",
            "type": "POST"
        },
        "deferRender": true,
        "responsive": true,
        "columns": [
            /* title will auto-generate th columns
            *
            * users.name as user_name,
            users.phone_no as user_phone_no,
            users.email as user_email,
            users.u_id as user_u_id,
            users.parent_id as user_parent_id,
            *
            *
            * */
            {
                "data": "user_id", "title": "Sr.#", "searchable": true,
                'render': function (data, type, row) {
                    tab1++;
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_id;
                    }else{
                        return '<span style="color: red" >' + row.user_id + '</span>';

                    }
                }
            },
            {
                "data": "user_name", "title": "User Name ", "searchable": true,
                "render": function (data, type, row, meta) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_name;
                    }else {
                        return '<span style="color: red" >' + row.user_name + '</span>';

                    }
                }
            },
            {
                "data": "user_phone_no", "title": "Mobile No", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_phone_no;
                    }else {
                        return '<span style="color: red" >' + row.user_phone_no + '</span>';

                    }

                }
            },
            {
                "data": "user_email", "title": "Email", "orderable": true, "searchable": true,

                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0   || row.attractive_funds > 0){
                        return row.user_email;
                    }else {
                        return '<span style="color: red" >' + row.user_email + '</span>';

                    }
                }
            },
            {
                "data": "user_u_id", "title": "B4U ID","orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        // return '<span style="color: red" >' + row.user_u_id + '</span>';
                        return '<a  class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc('+ row.user_id + ')">' + row.user_u_id +'</a> ';
                    }else {
                        return '<a style="color: red" class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc(' + row.user_id + ')">' + row.user_u_id +'</a> ';

                    }
                }
            },
            {
                "data": "user_parent_id", "title": "Parent ID", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_parent_id;
                    }else {
                        return '<span style="color: red" >' + row.user_parent_id + '</span>';
                    }
                }
            },
            {
                "data": 'attractive_funds', "title": "Total USD", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.attractive_funds;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){
                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.attractive_funds + '</span>';
                        }

                    }
                }
            },
            {
                "data": "active_reinvestment", "title": "Total Reinvest", "orderable": true, "searchable": true,
                'render': function (data, type, row) {

                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.active_reinvestment;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){

                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.active_reinvestment + '</span>';
                        }

                    }
                }
            },

            {
                "data": "user_created_at", "title": "Created At", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_created_at;
                    }else {
                        return '<span style="color: red" >' + row.user_created_at + '</span>';

                    }
                }
            },
        ],
        "order": [7, "desc"],

        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });
    $('#myTable2').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": true,
        "ajax": {
            "url": "{{url('dashboard/refferal_partner_level_2/json')}}",
            {{--"url": "{{url('dashboard/deposits/json')}}",--}}
            "type": "POST"
        },
        "deferRender": true,
        "responsive": true,
        "columns": [
            /* title will auto-generate th columns
            *
            * users.name as user_name,
            users.phone_no as user_phone_no,
            users.email as user_email,
            users.u_id as user_u_id,
            users.parent_id as user_parent_id,
            *
            *
            * */
            {
                "data": "user_id", "title": "Sr.#", "searchable": true,
                'render': function (data, type, row) {
                    tab2++;
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return '<span style="color: red" >' + row.user_id + '</span>';
                    }else{
                        return row.user_id;

                    }
                }
            },
            {
                "data": "user_name", "title": "User Name ", "searchable": true,
                "render": function (data, type, row, meta) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_name;
                    }else {
                        return '<span style="color: red" >' + row.user_name + '</span>';
                    }
                }
            },
            {
                "data": "user_phone_no", "title": "Mobile No", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_phone_no;
                    }else {
                        return '<span style="color: red" >' + row.user_phone_no + '</span>';
                    }

                }
            },
            {
                "data": "user_email", "title": "Email", "orderable": true, "searchable": true,

                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_email;
                    }else {
                        return '<span style="color: red" >' + row.user_email + '</span>';
                    }
                }
            },
            {
                "data": "user_u_id", "title": "B4U ID","orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return '<a style="color: red" class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc(' + row.user_id + ')">' + row.user_u_id +'</a> ';
                    }else {
                        return '<a  class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc('+ row.user_id + ')">' + row.user_u_id +'</a> ';

                    }
                }
            },
            {
                "data": "user_parent_id", "title": "Parent ID", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_parent_id;
                    }else {
                        return '<span style="color: red" >' + row.user_parent_id + '</span>';
                    }
                }
            },
            {
                "data": 'attractive_funds', "title": "Total USD", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.attractive_funds;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){
                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.attractive_funds + '</span>';
                        }
                    }
                }
            },
            {
                "data": "active_reinvestment", "title": "Total Reinvest", "orderable": true, "searchable": true,
                'render': function (data, type, row) {

                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.active_reinvestment;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){

                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.active_reinvestment + '</span>';
                        }

                    }
                }
            },

            {
                "data": "user_created_at", "title": "Created At", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_created_at;
                    }else {
                        return '<span style="color: red" >' + row.user_created_at + '</span>';
                    }
                }
            },
        ],
        "order": [7, "desc"],
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });
    $('#myTable3').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": true,
        "ajax": {
            "url": "{{url('dashboard/refferal_partner_level_3/json')}}",
            {{--"url": "{{url('dashboard/deposits/json')}}",--}}
            "type": "POST"
        },
        "deferRender": true,
        "responsive": true,
        "columns": [
            /* title will auto-generate th columns
            *
            * users.name as user_name,
            users.phone_no as user_phone_no,
            users.email as user_email,
            users.u_id as user_u_id,
            users.parent_id as user_parent_id,
            *
            *
            * */
            {
                "data": "user_id", "title": "Sr.#", "searchable": true,
                'render': function (data, type, row) {
                    tab3++;
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_id;
                    }else{
                        return '<span style="color: red" >' + row.user_id + '</span>';
                    }
                }
            },

            {
                "data": "user_name", "title": "User Name ", "searchable": true,
                "render": function (data, type, row, meta) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_name;
                    }else {
                        return '<span style="color: red" >' + row.user_name + '</span>';
                    }
                }
            },
            {
                "data": "user_phone_no", "title": "Mobile No", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_phone_no;
                    }else {
                        return '<span style="color: red" >' + row.user_phone_no + '</span>';
                    }

                }
            },
            {
                "data": "user_email", "title": "Email", "orderable": true, "searchable": true,

                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_email;
                    }else {
                        return '<span style="color: red" >' + row.user_email + '</span>';
                    }
                }
            },
            {
                "data": "user_u_id", "title": "B4U ID","orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return '<a  class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc('+ row.user_id + ')">' + row.user_u_id +'</a> ';
                    }else {
                        return '<a style="color: red" class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc(' + row.user_id + ')">' + row.user_u_id +'</a> ';

                    }
                }
            },
            {
                "data": "user_parent_id", "title": "Parent ID", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_parent_id;
                    }else {
                        return '<span style="color: red" >' + row.user_parent_id + '</span>';
                    }
                }
            },
            {
                "data": 'attractive_funds', "title": "Total USD", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.attractive_funds;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){
                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.attractive_funds + '</span>';
                        }

                    }
                }
            },
            {
                "data": "active_reinvestment", "title": "Total Reinvest", "orderable": true, "searchable": true,
                'render': function (data, type, row) {

                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.active_reinvestment;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){

                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.active_reinvestment + '</span>';
                        }

                    }
                }
            },

            {
                "data": "user_created_at", "title": "Created At", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_created_at;
                    }else {
                        return '<span style="color: red" >' + row.user_created_at + '</span>';
                    }
                }
            },
        ],
        "order": [7, "desc"],
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });
    $('#myTable4').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": true,
        "ajax": {
            "url": "{{url('dashboard/refferal_partner_level_4/json')}}",
            {{--"url": "{{url('dashboard/deposits/json')}}",--}}
            "type": "POST"
        },
        "deferRender": true,
        "responsive": true,
        "columns": [
            /* title will auto-generate th columns
            *
            * users.name as user_name,
            users.phone_no as user_phone_no,
            users.email as user_email,
            users.u_id as user_u_id,
            users.parent_id as user_parent_id,
            *
            *
            * */
            {
                "data": "user_id", "title": "Sr.#", "searchable": true,
                'render': function (data, type, row) {
                    tab4++;
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_id;


                    }else{
                        return '<span style="color: red" >' + row.user_id + '</span>';
                    }
                }
            },

            {
                "data": "user_name", "title": "User Name ", "searchable": true,
                "render": function (data, type, row, meta) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_name;
                    }else {
                        return '<span style="color: red" >' + row.user_name + '</span>';
                    }
                }
            },
            {
                "data": "user_phone_no", "title": "Mobile No", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_phone_no;
                    }else {
                        return '<span style="color: red" >' + row.user_phone_no + '</span>';
                    }

                }
            },
            {
                "data": "user_email", "title": "Email", "orderable": true, "searchable": true,

                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0   || row.attractive_funds > 0){
                        return row.user_email;
                    }else {
                        return '<span style="color: red" >' + row.user_email + '</span>';
                    }
                }
            },
            {
                "data": "user_u_id", "title": "B4U ID","orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return '<a  class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc('+ row.user_id + ')">' + row.user_u_id +'</a> ';
                    }else {
                        return '<a style="color: red" class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc(' + row.user_id + ')">' + row.user_u_id +'</a> ';

                    }
                }
            },
            {
                "data": "user_parent_id", "title": "Parent ID", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_parent_id;
                    }else {
                        return '<span style="color: red" >' + row.user_parent_id + '</span>';
                    }
                }
            },
            {
                "data": 'attractive_funds', "title": "Total USD", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.attractive_funds;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){
                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.attractive_funds + '</span>';
                        }

                    }
                }
            },
            {
                "data": "active_reinvestment", "title": "Total Reinvest", "orderable": true, "searchable": true,
                'render': function (data, type, row) {

                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.active_reinvestment;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){

                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.active_reinvestment + '</span>';
                        }

                    }
                }
            },

            {
                "data": "user_created_at", "title": "Created At", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0   || row.attractive_funds > 0){
                        return row.user_created_at;
                    }else {
                        return '<span style="color: red" >' + row.user_created_at + '</span>';
                    }
                }
            },
        ],
        "order": [7, "desc"],
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });
    $('#myTable5').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": true,
        "ajax": {
            "url": "{{url('dashboard/refferal_partner_level_5/json')}}",
            {{--"url": "{{url('dashboard/deposits/json')}}",--}}
            "type": "POST"
        },
        "deferRender": true,
        "responsive": true,
        "columns": [
            /* title will auto-generate th columns
            *
            * users.name as user_name,
            users.phone_no as user_phone_no,
            users.email as user_email,
            users.u_id as user_u_id,
            users.parent_id as user_parent_id,
            *
            *
            * */
            {
                "data": "user_id", "title": "Sr.#", "searchable": true,
                'render': function (data, type, row) {
                    tab5++;
                    if(row.active_reinvestment > 0   || row.attractive_funds > 0){
                        return row.user_id;
                    }else{
                        return '<span style="color: red" >' + row.user_id + '</span>';
                    }
                }
            },

            {
                "data": "user_name", "title": "User Name ", "searchable": true,
                "render": function (data, type, row, meta) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_name;
                    }else {
                        return '<span style="color: red" >' + row.user_name + '</span>';
                    }
                }
            },
            {
                "data": "user_phone_no", "title": "Mobile No", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_phone_no;
                    }else {
                        return '<span style="color: red" >' + row.user_phone_no + '</span>';
                    }

                }
            },
            {
                "data": "user_email", "title": "Email", "orderable": true, "searchable": true,

                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.user_email;
                    }else {
                        return '<span style="color: red" >' + row.user_email + '</span>';
                    }
                }
            },
            {
                "data": "user_u_id", "title": "B4U ID","orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return '<a  class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc('+ row.user_id + ')">' + row.user_u_id +'</a> ';
                    }else {
                        return '<a style="color: red" class="linksAll" data-toggle="modal" data-target="#PartnersDetailsModal" href="#" onclick="viewUserDetailsFunc(' + row.user_id + ')">' + row.user_u_id +'</a> ';

                    }
                }
            },
            {
                "data": "user_parent_id", "title": "Parent ID", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_parent_id;
                    }else {
                        return '<span style="color: red" >' + row.user_parent_id + '</span>';
                    }
                }
            },
            {
                "data": 'attractive_funds', "title": "Total USD", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.attractive_funds;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){
                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.attractive_funds + '</span>';
                        }

                    }
                }
            },
            {
                "data": "active_reinvestment", "title": "Total Reinvest", "orderable": true, "searchable": true,
                'render': function (data, type, row) {

                    if(row.active_reinvestment > 0  ||  row.attractive_funds > 0){
                        return row.active_reinvestment;
                    }else {
                        if(row.active_reinvestment == null  || row.attractive_funds == null){

                            return '<span style="color: red" >' + 0 + '</span>';
                        }else{

                            return '<span style="color: red" >' + row.active_reinvestment + '</span>';
                        }

                    }
                }
            },

            {
                "data": "user_created_at", "title": "Created At", "orderable": true, "searchable": true,
                'render': function (data, type, row) {
                    if(row.active_reinvestment > 0  || row.attractive_funds > 0){
                        return row.user_created_at;
                    }else {
                        return '<span style="color: red" >' + row.user_created_at + '</span>';
                    }
                }
            },
        ],
        "order": [7, "desc"],
        "pagingType": "full_numbers",
        "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
        buttons: [
            'excel', 'pdf'
        ]
    });
    //
    // $('#myTable2').DataTable({
    //     "pagingType": "full_numbers",
    //     "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
    //     buttons: [
    //         'excel', 'pdf'
    //     ]
    // });
    //
    // $('#myTable3').DataTable({
    //     "pagingType": "full_numbers",
    //     "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
    //     buttons: [
    //         'excel', 'pdf'
    //     ]
    // });
    //
    // $('#myTable4').DataTable({
    //     "pagingType": "full_numbers",
    //     "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
    //     buttons: [
    //         'excel', 'pdf'
    //     ]
    // });
    //
    // $('#myTable5').DataTable({
    //     "pagingType": "full_numbers",
    //     "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
    //     buttons: [
    //         'excel', 'pdf'
    //     ]
    // });
    //
    // $('#deposit-1').DataTable({
    //     "pagingType": "full_numbers"
    // });
    //
    // $('#deposit-2').DataTable({
    //     "pagingType": "full_numbers"
    // });
    //
    // $('#deposit-3').DataTable({
    //     "pagingType": "full_numbers"
    // });
    //
    // $('#deposit-4').DataTable({
    //     "pagingType": "full_numbers"
    // });
    //
    // $('#deposit-5').DataTable({
    //     "pagingType": "full_numbers"
    // });

    //});


    //  $( function() {

    $("#tabs").tabs();
    $("#Deposit-").tabs();
    function viewUserDetailsFunc(id) {
        //alert(id);
        var partner = 1;
        postData('{{{URL::to("")}}}/dashboard/view-partner-details', {
            id: id,
            partner: partner,
            "_token": "{{ csrf_token() }}",
        })
            .then(function (response) {
                return response.text()
            })
            .then(function (html) {
                $(".ucontent").html(JSON.parse(html));
            })
            .catch(function (error) {
                $('#PartnersDetailsModal').modal('hide');
                alert('Invalid user reference');
            });

    }
    //} );
</script>
