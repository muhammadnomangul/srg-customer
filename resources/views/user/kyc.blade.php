@include('header')
<!-- Phone Input CSS-->
<link rel="stylesheet" href="{{ asset('home/flag_phone_input/css/intlTelInput.css')}}">

<!-- Phone Input JS-->
<script src="{{ asset('home/flag_phone_input/js/intlTelInput.js')}}"></script>
<!-- //header-ends -->

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">KYC Information</h3>

        @include('messages')

        <div class="row">

                @if($view == 'show')
                    @include('user.partials.kyc_show')
                @else
                    @include('user.partials.kyc_card')
                @endif
        </div>

    </div>

</div>


<script>

</script>

@include('footer')

