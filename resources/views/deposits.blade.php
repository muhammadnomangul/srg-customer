@include('header')
<style>
    label {
        margin: 10px;
        margin-left: 0;
    }

    input.required {
        border: 1px solid red !important;
    }
</style>
<?php
$amountusd = ""; ?>
<!-- //header-ends -->
<!-- main content start-->
<div id="page-wrapper">
    <div id="reload_img" class="loader-overlay" style="display:none;">
        <div class="inner-loader">
            <div style="background-image:url({{asset('images/loading-bar.gif')}});">
                <h3 style="color: white; text-align: center; padding-top: 100px;">Please wait! System is processing you
                    request..</h3>
            </div>
        </div>
    </div>
    <div class="main-page signup-page">
        <h3 class="title1">Your deposits</h3>
        @include('messages')

        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger">
                    <b>Only for Pakistan / Pakistani National</b>
                    <p>Pakistani national, must be deposit more than <b>$710 </b>, Otherwise your deposit/portfolio in
                        Banks will
                        not approve and consider as dummy</p>
                </div>
            </div>
        </div>
        <div class="alert alert-warning alert-dismissable">
            <i class="fa fa-info-circle"></i> <b >Deposit Important Alert! </b><br>It is to notify you that we are changing our policy for crypto deposits.
              Your all previous crypto deposits will be converted to USD. In addition to this, if you will create any new crypto deposit it will be automatically converted to USD.<br>
             <small>(Note: RSC deposits will not changed)</small>
        </div>

        <a class="btn btn-default" href="#" data-toggle="modal" data-target="#depositModal"><i class="fa fa-plus"></i>New
            deposit</a>
        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#depositFromCashBoxModal"><i
                    class="fa fa-plus"></i> &nbsp;Deposit from Cash Box</a>
        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
            <table id="myTablelg" class="table table-hover">
            </table>
        </div>
    </div>
</div>

<!-- Deposit Modal -->
<div id="depositModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Make deposit </h4>
            </div>
            <div class="modal-body">
                <span id="errors"></span>

                @if(\Illuminate\Support\Facades\Auth::user() && \App\users::getUserDetails(\Illuminate\Support\Facades\Auth::user()->id)->Country)
                    <form style="padding:3px;" id="form1" role="form" method="post" action="/dashboard/deposit"
                          autocomplete="off">
                        <div class="form-group">
                            <label for="select" class="control-label">Select Deposit Mode * : </label>
                            <div class="form-input">
                                <select name="deposit_mode" id="deposit_mode" class="form-control select2" required>
                                    <option value="">--Select--</option>
                                    <option value="new">New-Investment</option>
                                    <option value="reinvest">Re-Investment</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select" class="control-label">Select Currency * : </label>
                            <div class="form-input">
                                <select name="currency" id="currency" class="form-control currencyVal amount_type select2"
                                        required>
                                    <!--<option value="">--Select--</option>-->
                                    @foreach($currencies as $currency)
                                        <option value="{{$currency->code}}">{{$currency->code}} ({{$currency->name}})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="rtype">
                            <label for="select" class="control-label">Select Reinvest Type * : </label>
                            <div class="form-input">
                                <select name="reinvest_type" id="reinvest_type" class="form-control amount_type"
                                        required>
                                    <option value="">--Select--</option>
                                    <option value="Bonus">Bonus</option>
                                    <option value="Profit">Profit</option>
                                    <option value="Sold">Sold Balance</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="amount_w">

                            <label for="select" id="amt_convert" class="control-label"></label><br/>
                            <label for="select" id="label_id" class="control-label label_id">Amount* :</label>
                            <div class="form-input">
                                <input style="padding:5px;" id="amountVal" class="form-control"
                                       placeholder="Enter amount here" type="text" name="amount"
                                       onkeypress="return isNumberKey(this, event);" maxlength="10" min="1"
                                       required><br/>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_key" value="{{ csrf_token() }}">
                        <!--<input type="hidden" id="amount_usd" name="amountusd" value="">-->
                        <input type="submit" class="btn btn-default" value="Continue" id="sbm_btn">
                        <span style=" margin-left:10px; color:red;"> Note : You can only 1 Profit Reinvest against a Currency within One Month. </span>
                    </form>

                <!--   <div class="form-group" id="amount_w">
                            <label for="select" id="label_id" class="control-label label_id"></label><br/>
                            <label for="select" class="control-label">Amount* :</label>
                            <div class="form-input">
                                <input style="padding:5px;" id="amountVal" class="form-control"
                                       placeholder="Enter amount here" type="text" name="amount"
                                       required onkeypress="return isNumberKey(this, event);" maxlength="7" min="1"><br/>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_key" value="{{ csrf_token() }}">
                        <!-<input type="hidden" id="amount_usd" name="amountusd" value="">->
                        <input type="submit" class="btn btn-default" value="Continue" id="sbm_btn">
                        <span style=" margin-left:10px; color:red;"> Note : You can only 1 Profit Reinvest against a Currency within One Month. </span>
                    </form> -->
                @else
                    <h2 class="danger" style="margin-bottom: 20px">Warning!</h2>
                    <p class="danger">It seems you havn't select your country in your profile, To make deposit, <a
                                target="_blank" href="{{ url('dashboard/accountdetails') }}">Click here to select your
                            country</a></p>

                @endif

            </div>
        </div>
    </div>
</div>
<div id="depositFromCashBoxModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">Make deposit from CashBox </h4>
            </div>
            <div class="modal-body">
                <div class="errors alert alert-danger hide"></div>
                <form style="padding:3px;" id="form12" role="form" method="post"
                      action="{{ route('cash_box_deposit') }}"
                      autocomplete="off" mod="cbDeposit">
                    @csrf
                    <div class="form-content">
                        <div class="form-group">
                            <label for="select" class="control-label">Select CashBox Account * : </label>
                            <div class="form-input">
                                <select name="cb_account" class="form-control currencyValCB select2"
                                        required="required">
                                    @foreach($cashBoxAccounts as $cashBoxAccount)
                                        @if($cashBoxAccount->currencies()->code == 'USD' || $cashBoxAccount->currencies()->code == 'RSC' || $cashBoxAccount->currencies()->code == 'USDT')
                                        <option data-ab="{{ $cashBoxAccount->current_balance }}"
                                                value="{{$cashBoxAccount->currencies()->code}}">
                                            {{ $cashBoxAccount->name }}
                                            ({{ number_format($cashBoxAccount->current_balance, $cashBoxAccount->currencies()->code === 'USD'? 2:  8) }}</span> {{$cashBoxAccount->currencies()->code}}
                                            )
                                        </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select" class="control-label label_id">Amount* :</label>
                            <div class="form-input">
                                <input style="padding:5px;" class="form-control"
                                       placeholder="Enter amount here" type="text" name="cb_amount"
                                       maxlength="10" min="1"
                                       required="required">
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary t-s">Deposit Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    let depositSubmitEndpoint = '<?php echo url('/dashboard/deposit')?>';
    let cashBoxDeposit = '<?php echo route('cash_box_deposit')?>';
    let amountEntered = $('input[name=cb_amount]')
    let addNewBenificiaryInput = null;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $('#myTable').DataTable({
            "pagingType": "full_numbers"
        });

        $("#sbm_btn").click(function () {
            /* var conf = confirm("Do you really want to proceed ?");
            if(conf)
            { */
            var mode = $('#deposit_mode option:selected').val();
            var curr = $('#currency option:selected').val();
            var amount = $('#amountVal').val();
            //alert(curr + amount + mode);

            var limit = 0;
            if (mode == "new") {
                limit = 50;
            } else if (mode == "reinvest") {
                limit = 25;
            }
            limit = parseInt(limit);
            amount = parseFloat(amount);
            var avlblance = parseFloat($("#availablemt").val());
            //$("#label_id").html("");

            if (amount > avlblance) {
                // $("#errors").html('<strong style="color:red;">Amount can not be greater than Available balance</strong>');
                $("#amt_convert").html('<strong style="color:red;">Amount can not be greater than Available balance</strong>');
                return false;
            }
            if (curr == "USD") {
                if (amount >= limit) {
                    $("#formId").submit();
                    return true;
                } else {
                    // $("#errors").html('<strong style="color:red;"> Error! Invested amount is less than minimum allowed  limit $' + limit + '. </strong>');
                    // $("#errors").html('<strong style="color:red;"> Amount is less than allowed  limit $' + limit + '. </strong>');
                    $("#amt_convert").html('<strong style="color:red;"> Error! Invested amount is less than minimum allowed  limit $' + limit + '. </strong>');
                    $("#amt_convert").html('<strong style="color:red;"> Amount is less than allowed  limit $' + limit + '. </strong>');

                    return false;
                }
            } else {
                jQuery.ajax({
                    type: "POST",
                    url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/amountLimitCheck') }}',
                    data: {amount: amount, curr: curr, "_token": "{{ csrf_token() }}",},
                    success: function (response) {
                        var result = parseInt(response);
                        if (result >= limit) {
                            return true;
                        } else {
                            $("#label_id").append('<strong style="color:red;"> Error! Invested amount is $' + result + ' less than minimum allowed  limit $' + limit + '. </strong>');
                            return false;
                            //alert("Error! Invested amount is $"+result+" less than minimum allowed  limit $"+limit);
                        }
                    }, error: function (response) {
                        //$("#CallSubModal").modal('show');
                        // alert('Be patient, System is under maintenance');
                    }
                });
            }
        });
        //////////////////////////////////

        var i = 1;
        $('#myTablelg').DataTable({
            processing: true,
            serverSide: true,
            "bLengthChange": false,
            "ajax": {
                "url": "{{url('dashboard/deposits/json')}}",
                "type": "POST"
            },
            "deferRender": true,
            "columns": [
                /* title will auto-generate th columns */
                {
                    "data": "id", "title": "Sr.#", "searchable": true,
                    "render": function (data, type, full, meta) {
                        return i++;
                    }
                },

                {
                    "data": "id", "title": "Portfolio#", "searchable": true,
                    "render": function (data, type, row, meta) {
                        var itemID = data;
                        return '<a data-toggle="modal" data-target="#DDetailsModal" href="#" onclick="viewDetailsFunc(' + row.id + ')">D-' + itemID + '</a>';
                    }
                },
                {
                    "data": "trans_type", "title": "Payment Type", "orderable": true, "searchable": true,
                    'render': function (data, type, row) {

                        if (row.trans_type == "Reinvestment") {
                            return row.trans_type + ' (' + row.reinvest_type + ')';
                        } else if (row.payment_mode == "FundTransfer") {
                            return row.trans_type + '<p style="color:red;">(' + row.payment_mode + ')</p>';
                        } else {
                            if (row.payment_mode == 'cashbox') {
                                return row.trans_type + ' <span class="label label-success" style="background-color: #9e2187">Cash Box</span>\n'
                            } else if (row.payment_mode == 'cashbox-failed') {
                                return row.trans_type + ' <span class="label label-danger">Cash Box Failed</span>\n'
                            } else {
                                return data;
                            }

                        }
                    }
                },
                {
                    "data": "amount", "title": "Amount", "orderable": true, "searchable": true,

                    'render': function (data, type, row) {

                        if (row.currency == "USD") {
                            return row.amount.toFixed(3) + ' (' + row.currency + ')';
                        } else {
                            return row.amount.toFixed(3) + ' (' + row.currency + ')';
                        }
                    }

                },

                {
                    "data": "rate", "title": "Rate", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        return "$ " + (+row.rate).toFixed(2);
                    }
                },
                {
                    "data": "fee_deducted", "title": "Deposit Fee", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        if (row.fee_deducted == null) {
                            return "$ " + 0;
                        } else {
                            return "$ " + data;
                        }
                    }
                },
                {
                    "data": 'total_amount', "title": "Total Deposit", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        return "$ " + (+row.total_amount + +row.fee_deducted).toFixed(3);
                    }
                },
                {
                    "data": "status", "title": "Status", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        if (row.status == "Approved") {
                            return '<font color="green">Approved</font>';
                        } else {
                            return data;
                        }
                    }
                },

                {
                    "data": "status", "title": "Status Date", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        if (row.status == "Sold") {
                            return row.sold_at;
                        } else if (row.status == "Approved") {
                            return row.approved_at;
                        } else {
                            return "--";
                        }
                    }
                },
                {


                    "data": "created_at", "title": "Created", "orderable": true, "searchable": true,
                    'render': function (data, type, row) {


                        var created_date = new Date(row.created_at);
                        // var curr_day =created_date.getDate();
                        // var curr_month = created_date.getMonth();
                        // var curr_year = created_date.getFullYear();
                        var curr_hour = created_date.getHours();
                        var curr_min = created_date.getMinutes();
                        var curr_sec = created_date.getSeconds();


                        var datestring = created_date.getFullYear() + "-" + ("0" + (created_date.getMonth() + 1)).slice(-2) + "-" + ("0" + created_date.getDate()).slice(-2);


                        return datestring + " " + curr_hour + ":" + curr_min + ":" + curr_sec;
                        //alert(now);
                        //return created_date.toISOString().slice(0, 10);
                    }
                },
                {
                    'data': 'id', "title": "Actions", "searchable": false,
                    'render': function (data, type, row) {
                        let row_html = '<a href="#" onclick="viewProofFunc(' + row.id + ');" class="btn btn-default btn-sm" data-toggle="modal" data-target="#popImageModel" title="View payment proof"> <i class="fa fa-eye"></i></a>';
                        var sessionvar = "{{app('request')->session()->get('back_to_admin')}}";

                        var notfixeddeposit = "{{notFixedDeposit("+row.id+")}}";


                        if (row.status == "Approved" && notfixeddeposit) {
                            row_html += ' <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#saleModal" onclick="saleFunc(' + row.id + ')">Sale</a>'

                            if (row.is_partial_sold != 1 && sessionvar) {
                                row_html += '<a class="btn btn-info" href="#" data-toggle="modal" data-target="#partialSaleModal" onclick="partialSaleFunc(' + row.id + ','
                                    + row.currency + "," + row.amount + ')' + '>Partial Sale By Admin</a>';
                            }
                        } else if (row.is_partial_sold == 1 || row.is_partial_sold == '1') {
                            row_html += '<font color="red"> Partial Sold </font>';
                        }

                        if (row.status == "Sold") {
                            row_html += '<font color="red">';

                            if (row.is_partial_sold == 1) {
                                row_html += '( Partialy sold with D-' + row.partial_tradeid + ')';
                            }

                            row_html += '</font>';
                        }


                        //  return '<a class="btn-sm btn-danger submit1" href="{{ url('dashboard/updateStatus')}}/'+row.id+'/Cancelled/deposit">Trash</a>'

                        return row_html;
                    }
                },


                // {
                //     "data": "currency", "title": "Currency", "orderable": true, "searchable": true
                // },
                // {
                //     "data": "trade_amount", "title": "Amount", "orderable": true, "searchable": true,

                //        "render": function (data, type, row) {
                //             if(row.currency === 'USD'){
                //                 return row.trade_amount.toFixed(2);
                //             }else{
                //                 return row.crypto_amount.toFixed(6);
                //             }
                //         }
                // },
                // {
                //     "data": "today_profit", "title": "Profit Amount", "orderable": true, "searchable": true,

                //        "render": function (data, type, row) {
                //             if(row.currency === 'USD'){
                //                 return row.today_profit.toFixed(6);
                //             }else{
                //                 return row.crypto_profit.toFixed(6);
                //             }
                //         }
                // },

            ],
            "order": [9, "desc"]
        });
        ///////////////////////////////////////


    });


    function saleFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/saleTradePost',
            data: {id: id, "_token": "{{ csrf_token() }}",},
            //  cache: false,
            success: function (response) {
                $("#ajaxval").html(response);
            }, error: function (response) {
                //$("#CallSubModal").modal('show');
                // alert('Be patient, System is under maintenance');
            }
        });
    }

    function partialSaleFunc(id, currency, amount) {
        $("#investment_id").html("");
        $("#deposit_id").val("");
        $("#curr_id").val("");
        $("#depositAmount").val("");
        var investment = "";
        var withdraw_amount = (amount / 100) * 30;
        if (id != "" && currency != "" && amount != "") {
            investment = "<h3> Deposit: D-" + id + " and Amount: " + amount + " " + currency + " Maximum partial allowed withdraw amount " + withdraw_amount + " " + currency + " </h3>";
            //alert(investment);
            $("#investment_id").append(investment);
            $("#deposit_id").val(id);
            $("#curr_id").val(currency);
            $("#depositAmount").val(amount);
            $("#withdrawalAmount").val(withdraw_amount);
            //amount=""; currency=""; id="";
        }
    }


    $(".submit1").click(function () {
        var conf = confirm("Do you really want to proceed ?");
        if (conf) {
            return true;
        } else {
            return false;
        }
    });


    function viewDetailsFunc(id) {
        //alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewDetailsPost',
            data: {id: id, type: "deposit", "_token": "{{ csrf_token() }}",},
            //	cache: false,
            success: function (response) {
                $("#dcontent").html(response);
            }, error: function (response) {
                //$("#CallSubModal").modal('show');
                // alert('Be patient, System is under maintenance');
            }
        });
    }

    function viewProofFunc(id) {
        //alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewProofPost',
            data: {id: id, type: "deposit", "_token": "{{ csrf_token() }}",},
            //	cache: false,
            success: function (response) {
                $("#pcontent").html(response);
            }, error: function (response) {
                //$("#CallSubModal").modal('show');
                // alert('Be patient, System is under maintenance');
            }
        });
    }

    $("#reinvest_type").change(function () {
        if ($(this).val() == "Bonus") {

            $('.currencyVal option[value!="USD"]').hide();
            var currencyValue = $('#currency option:selected').text();
            // if (currencyValue != "USD") {
            //     $('.currencyVal option:selected').val("USD");
            //     alert(currencyValue);
            //     $("#currency option:selected").prop("selected", false);
            // }

        } else if ($(this).val() == "Profit") {
            $('.currencyVal option[value!=""]').show();

        } else if ($(this).val() == "Sold") {
            $('.currencyVal option[value!=""]').show();
        }
    }).trigger("change");

    $("#deposit_mode").change(function () {
        if ($(this).val() == "reinvest") {
            $("#rtype").show();
            $('#reinvest_type').attr('required', 'true');
        } else {
            $("#rtype").hide();
            $('#reinvest_type').removeAttr('required');
            $('.currencyVal option[value!=""]').show();
            $("strong").hide();
        }
    }).trigger("change");

    //On change of amount_type
    $(".amount_type").change(function () {
        var selectedMode = $('#deposit_mode option:selected').val();
        // alert(selectedMode);
        var selectedType = $('#reinvest_type option:selected').val();
        var curr = $('#currency option:selected').val();
        var x = document.getElementById("amountVal").value;

        if (selectedMode == "reinvest") {
            if (curr != "" && selectedType != "") {
                $("strong").hide();
                jQuery.ajax({
                    type: "POST",
                    url: '{{{URL::to("")}}}/dashboard/getAccountInfoPost',
                    data: {curr: curr, type: selectedType, "_token": "{{ csrf_token() }}",},
                    success: function (response) {
                        $("#label_id").append(response);
                    }, error: function (response) {
                        //$("#CallSubModal").modal('show');
                        // alert('Be patient, System is under maintenance');
                    }
                });
            }
        }

        amountconverstion(curr, x,selectedType);


    });

    function isNumberKey(txt, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            //Check if the text already contains the . character
            if (txt.value.indexOf('.') === -1) {
                return true;
            } else {
                return false;
            }
        } else {
            if (charCode > 31 &&
                (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    }

    ///////////////////
    $(document).on('input', '#amountVal', function () {
        var currencytype = $('#currency option:selected').val();
        var x = document.getElementById("amountVal").value;
        var selectedType = $('#reinvest_type option:selected').val();
        var depositMode = $('#deposit_mode option:selected').val();
        if (x != '') {
            console.log('in amount change event ');
            console.log('amount entered ',x);
            console.log('currencytype',currencytype);
            console.log('depositMode',depositMode);
            console.log('selectedType',selectedType);
            console.log(' input change event  ');
            amountconverstion(currencytype, x,selectedType);
            $('#amt_convert').html("");
            $("#errors").html("");
            jQuery.ajax({
                type: "POST",
                url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/depositCheck') }}',
                data: {currencytype: currencytype, amount: x,selectedType: selectedType,depositMode: depositMode, "_token": "{{ csrf_token() }}",},
                //cache: false,
                success: function (response) {
                    console.log('success response ',response)
                    $('#amt_convert').html(response);
                }, error: function (response) {
                    console.log('error response',response)
                    //$("#CallSubModal").modal('show');
                }
            });
            {{--jQuery.ajax({--}}
            {{--    type: "POST",--}}
            {{--    url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/currenconvert') }}',--}}
            {{--    data: {currencytype: currencytype, amount: x, "_token": "{{ csrf_token() }}",},--}}
            {{--    //cache: false,--}}
            {{--    success: function (response) {--}}
            {{--        $('#amt_convert').html(response);--}}
            {{--    }, error: function (response) {--}}
            {{--        //$("#CallSubModal").modal('show');--}}
            {{--    }--}}
            {{--});--}}


        } else {
            $('#amt_convert').html("");
        }
    });

    var input = document.getElementById('amountVal');
    input.onkeydown = function () {
        var key = event.keyCode || event.charCode;
        if (key == 8) {
            $('#amt_convert').html("");
        }
    };

    function amountconverstion(currencytype, amountval,selectedType) {
        if (amountval != '') {
            jQuery.ajax({
                type: "POST",
                url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/currenconvert') }}',
                data: {currencytype: currencytype, amount: amountval, selectedType: selectedType, "_token": "{{ csrf_token() }}",},
                //cache: false,
                success: function (response) {
                    $('#amt_convert').html(response);
                }, error: function (response) {
                    //$("#CallSubModal").modal('show');

                }
            });
            console.log('hy in conversion')
            console.log('currencytype',currencytype)
            console.log('amount',amountval)
            console.log('selectedType',selectedType)
        } else {
            $('#amt_convert').html("");
        }
    }
</script>

@include('modals')
@include('footer')

<script src="{{ url('cb.js?1.3.3') }}"></script>
