<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="margin-top: 5%">
    <h2>B4U Global Version Details</h2>
    <table class="table">
        <thead>
        </thead>
        <tbody>

        @foreach( $versionDesc as $versionItems => $versionValues)
            <tr>
                <th>
                    {{ \Illuminate\Support\Str::upper(
                    \Illuminate\Support\Str::replaceArray('_', [ ' '], $versionItems))
                     }}
                </th>
                <td>
                    @if(!is_array($versionValues))
                        {{ $versionValues }}
                    @else
                        @foreach($versionValues as $key => $releaseNotes)
                            @if(is_numeric($key))
                                <p>{{$releaseNotes}}</p>
            @else
                <tr>
                    <th>{{$key}}</th>
                    <td>{{$releaseNotes}}</td>
                </tr>
                @endif
                @endforeach
                @endif
                </td>
                </tr>
                @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
