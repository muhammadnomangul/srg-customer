<!DOCTYPE html>
<html lang="en">
<head>
    @include('home/assetss')
    <style>
        .text-center {
            text-align: center !important;
            padding: 20px;
        }

        .btn-primary {
            background-color: #6c175c;
        }

        .btn-primary:hover {
            background-color: #5e0750 !important;
        }

        .red-border {
            border: 1px solid red;
        }
    </style>

</head>

<body class="auth-page" style="background-color:#f8f8f8;">

<!-- Wrapper Starts -->
<div class="wrapper">
    <div class="container user-auth" style="padding:20px;">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-md-5 col-lg-5">
                <!-- Logo Starts -->
                <div class="text-center">

                    @if(isset($settings->logo))

                        <a href="{{url('/')}}" style="text-align:center; color:#555; width:100%;">
                            <img id="logo" class="img-responsive"
                                 src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png" alt="logo"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @else
                        <a style="text-align:center; width:100%;" href="{{ url('/') }}">
                            <img src="https://b4uglobal.s3-ap-southeast-1.amazonaws.com/b4u-logo.png"
                                 style="margin-left: auto; margin-right: auto;">
                        </a>
                    @endif
                </div>


                <div class="form-container">
                    <div>
                        <div class="row text-center">
                            <h2 class="title-head" style="font-size:1.5em; color:#555;">Account Suspended</h2>
                        </div>

                        @if(empty(\Illuminate\Support\Facades\Auth::user()) || \Illuminate\Support\Facades\Auth::user()->wrong_attempt_unsuspended_times > \App\User::unBlockWrongAttemptUnsuspendedTimes)
                            <div class="alert alert-danger">
                                Your account has been suspended due to Wrong Attempts five times with password,
                                Please contact to Support Admin to make your account enable.
                                <br>
                            </div>

                            {{--                            Email us: <a href="mailto:support@b4uglobal.com">support@b4uglobal.com</a>--}}
                            {{--                            <br>--}}
                            {{--                            Call Us <a href="tel:04232503445">042-32503445</a>--}}

                            {{--                            <div style="background: lightgray; margin-top: 10px">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="reason" style="padding: 10px; font-weight: bold; color: black">Please--}}
                            {{--                                        tell us, why your account has been suspended?</label>--}}
                            {{--                                    <textarea class="form-control" style="background-color: white; color: black"--}}
                            {{--                                              id="reason" rows="5" required="required"></textarea>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <button type="submit" style="width: 100%" class="btn btn-primary">Request to Un-Block Account</button>--}}

                        @else
                            <div class="alert alert-danger">
                                Your account has been suspended due to Wrong Attempts with password.
                            </div>

                            <div class="alert alert-success">
                                Your account will be automatically unsuspend
                                at
                                <br>
                                <b>{{ \Illuminate\Support\Facades\Auth::user()->userAccountWillUnBlockAt() }}</b>
                            </div>
                    @endif


                    <!-- Form Ends -->
                    </div>
                </div>
                <!-- Copyright Text Starts -->
                <p class="text-center copyright-text">Copyright
                    © {{date('Y')}} @if(isset($settings->site_name)){{$settings->site_name}} @else B4U Global @endif All
                    Rights Reserved</p>
                <!-- Copyright Text Ends -->
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<!-- <script src="{{ asset('js/jquery-ui.js')}}"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $(document).on('click', 'button[type="submit"]', function (e) {
            $('button').prop('disabled', true);
            $(this).closest('form').submit();
            //$('input, a').prop('readonly', true);
            var buttonText = $(this).html();
            $(this).html('<i class="fa fa-spinner spin"></i> Processing...');
            $(this).html(buttonText);
        });
    });

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172512767-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-172512767-1');
</script>
<!--  LiveChat Widget-->
@include('liveChatScript')
</body>

</html>
