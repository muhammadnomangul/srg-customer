@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Manage clients withdrawals</h3>
				
				@include('messages')

				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
					<table class="table table-hover"> 
						<thead> 
							<tr> 
								<th>Agent name</th>
                                <th>Clients referred</th>
                                <th>Clients activated</th>
								<th>Earnings</th> 
							</tr> 
						</thead> 
						<tbody> 
							@foreach($agents as $agent)
							<tr> 
								 <td>{{$agent->duser->name}}</td> 
								 <td>{{$agent->total_refered}}</td> 
                                 <td>{{$agent->total_activated}}</td> 
								 <td>{{$agent->earnings}}</td> 
							</tr> 
							@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
        @include('modals')
		@include('footer')