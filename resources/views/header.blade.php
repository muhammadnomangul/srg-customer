@php
    if(\Illuminate\Support\Facades\Auth::guest()){
        return redirect(url('/'));
    }

    $headers = apache_request_headers();
    $headers = $headers['Host'];
@endphp
        <!DOCTYPE HTML>
<html>
<head>

    <title>@if(isset($settings->site_name)){{$settings->site_name}}@else B4U Global @endif
        |@if(isset($title)) {{$title}} @endif</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
        function closeReviewModal(){
            $("#reviewDetailsModal").modal('toggle');
        }
    </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('home/home/images/favicon.ico')}}">

    <!-- Bootstrap Core CSS -->
   <link href="{{ asset('css/bootstrap.css')}}" rel='stylesheet' type='text/css'/>

    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css')}}" rel='stylesheet' type='text/css'/>
    <link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet" type='text/css'>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- font CSS -->
    <!-- font-awesome icons -->
    {{--    <link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet">--}}
   {{-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet">
<!--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

    <link href="{{ asset('css/preview.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.fancybox.min.css')}}" rel="stylesheet">
<!--<link href="{{ asset('js/fancybox/jquery.fancybox-1.3.4.css')}}" type="text/css" rel="stylesheet" media="screen"> -->
    <link href="{{ asset('css/multi-fileinput.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.dataTables.min.css')}}" rel="stylesheet">

    <link href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>

    <!-- //font-awesome icons -->

    <!-- js ---->
    <script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
    <script src="{{ asset('js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{ asset('jquery.copy-to-clipboard.js')}}"></script>
    <script src="{{ asset('js/modernizr.custom.js')}}"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <script type="text/javascript" src="{{asset('js/ckeditor5/ckeditor.js')}}"></script>
    <link href="{{ 'http://127.0.0.1:8000/editor.dataTables.css' }}" rel="stylesheet"
          type="text/css">
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
    <script src="{{ asset('js/wow.min.js')}}"></script>

    <script>
        new WOW().init();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <link href="{{ url('style2.css')}}" rel='stylesheet' type='text/css'/>

    <header>
        <meta content="text/html; charset=utf-8" http-equiv=Content-Type>
        <link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notonastaliqurdudraft.css">
        <style>
            .urdu_warning {
                font-family: 'Noto Nastaliq Urdu Draft', serif;
            }
           .clear-rating {
               display: none !important;
           }
            .caption {
                display: none !important;
            }
            .select2, select {
                width: 100% !important;
            }

            .verificationInputBlock {
                display: flex;
                justify-content: center;
            }

            .verificationInputBlock input {
                text-align: center;
                line-height: 34px;
                width: 32px;
                margin-right: 2.5px;
            }

            .svg-inline--fa.fa-stack-2x {
                height: 0;
                width: 0;
            }

            .btn-primary {
                color: #fff;
                background-color: #9e2187 !important;
                border-color: #9e2187 !important;
                letter-spacing: 2px;
                font-size: 14px;
            }

            .btn {
                letter-spacing: 2px;
                font-size: 14px;

            }

        </style>
    </header>
    <script src="{{ asset('js/metisMenu.min.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}"></script>
    <link href="{{ asset('css/custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("public/malik/moon-dropzone.css") }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"
            type="text/javascript"></script>
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>

</head>

<body class="cbp-spmenu-push">

<div class="hide verificationCodeBlock">

    <div class="verificationBlock">
        <p class="info-message" style="margin: 10px;
    font-size: 12px;
    color: black;
        text-align: center;
    letter-spacing: 2px;">
            {{ \App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade::getAnOtpMessage(\Illuminate\Support\Facades\Auth::user()->two_fa_auth_type ) }}
        </p>
        <div class="verificationInputBlock">
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" autofocus
                   pattern="[0-9]{1}"/>
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}"/>
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}"/>
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}"/>
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}"/>
            <input type="password" class="inputs-un" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}"/>
        </div>

        <div>
            <hr>
            <button type="button" class="proceed-2f btn btn-sm btn-primary">Proceed</button>
            &nbsp;
            <button type="button" class="resend-2f btn btn-sm btn-info">Resend</button>
        </div>
    </div>

</div>
@include('layouts.error')

<script src="{{ url('fileUpload.js?1.9') }}"></script>
@if(\Illuminate\Support\Facades\Auth::user()->two_fa_auth_type <> 3)
    <script src="{{ url('ot.js?1.3') }}"></script>
@else
    <script src="{{ url('non-ot.js?1.3') }}"></script>
@endif

<script>
    (function (d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0];

        if (d.getElementById(id)) return;

        js = d.createElement(s);
        js.id = id;

        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=642791809168417";

        fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /facebook sdk -->

<style type="text/css">
    /* <!--.nav{
    overflow-y: scroll;
}

.navbar-collapse{
    overflow-y: scroll;
}--> */

    .navbar2 {
        overflow-y: scroll;
        margin-top: 30px;
        height: 90%;
        background-color: #580949;
    }
    /*@media screen and (max-width: 1200px) {*/
    /*    .MobileView*/
    /*    {*/
    /*        text-align: right !important;*/
    /*    }*/
    /*}*/
    @media screen and (min-width: 1200px) {
        .MobileView
        {
            text-align: right !important;
        }
    }
    @media screen and (max-width: 992px) {
        .navbar2 {
            overflow-y: scroll;
            margin-top: -5px;
            height: 90%;
            background-color: #580949;
        }
        .MobileViewRating
        {
            text-align: left !important;

        }
        .RatingMobileView{
            text-align: left !important;

        }

    }


    @media screen and (min-width: 992px) {
        .MobileViewRating
        {
          text-align:right !important;

        }
        .RatingMobileView{
            padding:0;
        }
    }


    .navbar2::-webkit-scrollbar {
        background: transparent;
        /* make scrollbar transparent */
    }

    .loader1 {
        background-color: #000;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0.5;
        filter: alpha(opacity=50);
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1020;
    }

    .loader1 img {
        vertical-align: middle;
        left: 50%;
        top: 50%;
        position: fixed;
    }

    .loader1 .x16 span {
        line-height: 16px;
        font-size: 12px;
        margin-left: 6px;

    }

    .loader1 .x32 img {
        width: 75px;
        height: 75px;
    }

    .loader {
        left: 50%;
        top: 50%;

        margin: 1px auto auto 91px;
        border: 1px solid #f3f3f3;
        border-radius: 50%;
        border-top: 1px solid #3498db;
        width: 18px;
        height: 18px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }


    .headerAD, .desktopView, #desktop-logo {
        display: block;
    }

    #showLeftPush, .mobileView, #mobile-logo {
        display: none;
    }

    @media screen and (max-width: 992px) {
        .headerAD, .desktopView, #desktop-logo {
            display: none;
        }

        #showLeftPush, .mobileView, #mobile-logo {
            display: block;
        }
    }


    @media screen and (max-width: 320px) {
        .profile_details {
            margin-top: -5px;
        }

        .profile_details .uid {
            position: relative;
            top: 6px;
        }

        /*.uid {
          position: relative;
          top: 2% !important;
          background-color: red;
        }*/
    }

</style>
<div class="main-content">
    <div class="loader1" style="display: none;">
        <div class="loader1 x32"><img src="{{ asset('/images/loading32x32.gif')}}"><span
                    style="color: rgb(0, 0, 0);"></span></div>
    </div>
    <!--left-fixed -navigation-->
    <div class="sidebar" role="navigation">

        <div class="navbar-collapse">

            <nav class="navbar2 cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

                <ul class="nav" id="side-menu">

                    @if(\Illuminate\Support\Facades\Auth::user() && in_array(\Illuminate\Support\Facades\Auth::user()->id , config('b4uglobal.onlyShowVWithdrawalPage')))

                        <li>
                            <a href="{{ url('dashboard/withdrawals') }}">
                                <i class="fas fa-money-check nav_icon"></i> &nbsp;@lang('home.Withdrawals')</a>
                        </li>

                    @else

                        @if(app('request')->session()->get('back_to_admin'))
                            <li>
                                <a href="{{ route('login_to_admin_account') }}" class="active"><i
                                            class="fa fa-arrow-left nav_icon"></i> &nbsp;Back TO Admin Panel</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/myreport') }}" class="active">
                                    <i class="fas fa-file-prescription"></i> &nbsp;Report</a>
                            </li>
                            <li>
                                <a href="#" class="active" data-toggle="modal" data-target="#sndmsg"
                                   onclick="viewModelFuncM({{Auth::user()->id}})">
                                    <i class="fa fa-envelope nav_icon"></i> &nbsp;Send Notification</a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ url('/dashboard') }}"><i class="fas fa-tachometer-alt"></i>
                                &nbsp;@lang('home.Dashboard')
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('cash-box.index') }}"><i class="fas fa-wallet"></i>
                                &nbsp;Cash Box
                            </a>
                        </li>

                        @if(Auth::user()->is_user(SITE_NORMAL_USER) || Auth::user()->is_user(SITE_COMMITTEE) )
                            <li>
                                <a href="{{ url('https://support.b4uglobal.com/') }}">
                                    <i class="far fa-life-ring"
                                    ></i> &nbsp;@lang('home.Support')
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('dashboard/deposits') }}">
                                    <i class="fas fa-money-check-alt nav_icon"></i> &nbsp;@lang('home.Deposits')</a>
                            </li>

                            <li>
                                <a href="{{ route('donations') }}">
                                    <i class="fa fa-th-list nav_icon"></i> &nbsp;@lang('home.Donations')</a>
                            </li>

                            <li>
                                <a href="{{ url('dashboard/withdrawals') }}">
                                    <i class="fas fa-money-check nav_icon"></i> &nbsp;@lang('home.Withdrawals')</a>
                            </li>

                            <li>
                                <a href="{{ url('dashboard/sold') }}">
                                    <i class="fas fa-coins"></i> &nbsp;@lang('home.Sold')</a>
                            </li>

                            {{--  <li>
                                  <a href="{{ url('dashboard/plans') }}">
                                      <i class="fa fa-cog nav_icon"></i> &nbsp;@lang('home.Investment plans')</a>
                              </li>
                            --}}
                            <li>
                                <a href="{{ url('dashboard/profitCalculator') }}"><i class="fa fa-cog nav_icon"></i>
                                    &nbsp;Profit
                                    Calculator</a>
                            </li>

                            <li>
                                <a href="{{ url('dashboard/referuser') }}">
                                    <i class="fa fa-users nav_icon"></i> &nbsp;@lang('home.Refer users')</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/RefferalPartners') }}">
                                    <i class="fa fa-users nav_icon"></i> &nbsp;@lang('home.Your Partners')</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/accountdetails') }}">
                                    <i class="fa fa-user nav_icon"></i> &nbsp;Update Profile</a>
                            </li>
                        @endif
                        @can('viewAny',\App\currency_rates::class)
                            <li>
                                <a href="{{ url('dashboard/rates') }}"><i class="fas fa-chart-line nav_icon"></i> &nbsp;Daily
                                    Exchange Rates</a>
                            </li>
                        @endif
                        @can('search-user')
                            <li>
                                <a href="{{ url('dashboard/searchUser') }}">
                                    <i class="fa fa-search nav_icon"></i> &nbsp;Search User</a>
                            </li>
                        @endif
                        @can('viewAny',\App\User::class)
                            <li>
                                <a href="{{ url('dashboard/manageusers') }}">
                                    <i class="fa fa-users nav_icon"></i> &nbsp;Manage Users</a>
                            </li>
                        @endcan
                        @can('viewAny', \App\Model\Deposit::class)
                            <li>
                                <a href="{{ url('dashboard/mpdeposits') }}">
                                    <i class="fas fa-money-check-alt nav_icon"></i> &nbsp;Manage Deposits</a>
                            </li>
                        @endcan
                        @can('viewAny', App\withdrawals::class)
                            <li>
                                <a href="{{ url('dashboard/pwithdrawals') }}"><i
                                            class="fas fa-money-check nav_icon"></i>
                                    &nbsp;Manage
                                    Withdrawals</a>
                            </li>
                        @endcan
                        @can('viewAny', \App\solds::class)
                            <li><a href="{{ url('dashboard/msold') }}"><i class="fas fa-coins"></i> &nbsp;Manage
                                    Solds</a>
                            </li>
                        @endcan
                        @can('viewAny',\App\Donation::class)
                            <li>
                                <a href="{{ url('dashboard/mdonations') }}">
                                    <i class="fa fa-th-list nav_icon"></i> &nbsp;Manage Donations</a>
                            </li>
                        @endcan
                        @can('viewAny',\App\Promotion::class)
                            <li><a href="{{ route('promotions.index') }}"><i class="fas fa-bullhorn nav_icon"></i>
                                    &nbsp;
                                    Manage Promos</a></li>
                        @endif
                        @can('viewAny',\App\plans::class)
                            <li>

                                <a href="{{ url('dashboard/mplans') }}"><i class="fa fa-cog nav_icon"></i>
                                    &nbsp; Investment plans</a>
                            </li>
                        @endcan
                        <li>
                            <a href="#">
                                <i class="fas fa-file-signature nav_icon"></i> &nbsp; Promotions
                                <span style="float: right;"><i class="fas fa-caret-down"></i></span>
                            </a>
                            <ul class="nav nav-second-level collapse" aria-expanded="false">
                                <li>
                                    <a href="{{ url('dashboard/promo-investors/2') }}" onclick="clickAndDisable(this)">
                                        <i class="fas fa-donate"></i> &nbsp; SixMonth Promo Invesment</a>
                                </li>
                                <li>
                                    <a href="{{ url('dashboard/oneMonthPromo') }}" onclick="clickAndDisable(this)">
                                        <i class="fas fa-donate"></i> &nbsp; OneMonth Promo Invesment</a>
                                </li>
                            </ul>
                        </li>
                        @if(Auth::user()->awarded_flag =='1')
                            <li>
                                <a href="{{ url('dashboard/awarded_partners') }}"><i
                                            class="fa fa fa-image nav_icon"></i>
                                    &nbsp; Distributor
                                    Partners </a>
                            </li>
                        @endif
                        @if(Auth::user()->can('viewAny',\App\Logs::class))
                            <li>
                                <a href="#"><i class="fas fa-file-signature nav_icon"></i> &nbsp;System Logs<span
                                            style="float: right;"><i class="fas fa-caret-down"></i> </span></a>
                                <ul class="nav nav-second-level collapse" aria-expanded="false">
                                    <li>
                                        <a href="{{ url('dashboard/adminlogs') }}">
                                            <i class="fas fa-file-signature nav_icon"></i> &nbsp;Admin Logs</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('dashboard/dailyLogs') }}">
                                            <i class="fas fa-file-signature nav_icon"></i> &nbsp;Daily Logs</a>
                                    </li>
                                    @if(Auth::user()->is_user(SITE_ADMIN))
                                        <li>
                                            <a href="{{ url('dashboard/laravel-logs') }}">
                                                <i class="fas fa-file-signature nav_icon"></i> &nbsp;Laravel Logs</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Auth::user()->is_user(SITE_SUPER_ADMIN) || Auth::user()->is_user(SITE_MANAGER) || Auth::user()->is_user(SITE_ADMIN))
                            <li>
                                <a href="#"><i class="fa fa-file nav_icon"></i> &nbsp;Reporting
                                    <span style="float: right;"><i class="fas fa-caret-down"></i> </span></a>
                                <ul class="nav nav-second-level collapse" aria-expanded="false">
                                    <li>
                                        <a href="{{ url('dashboard/topinvestors') }}">
                                            <i class="fas fa-receipt"></i> &nbsp;Top Investors</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('dashboard/lastWeekProfitBonus') }}">
                                            <i class="fa fa fa-users nav_icon"></i> &nbsp;Last Week Profit And Bonus</a>
                                    </li>

                                    <li>
                                        <a href="{{ url('dashboard/RefferalPartners') }}">
                                            <i class="fa fa-users nav_icon"></i> &nbsp;Your Partners</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('dashboard/payprofitbonus') }}"><i
                                                    class="fa fa fa-image nav_icon"></i> &nbsp;Send
                                            Profit/Bonus as CronJob </a>
                                    </li>
                                </ul>
                            </li>
                            @if(Auth::user()->is_user(SITE_SUPER_ADMIN))
                                <li>
                                    <a href="#"><i class="fa fa-cog nav_icon"></i> &nbsp;Site Settings
                                        <span style="float: right;"><i class="fas fa-caret-down"></i> </span></a>

                                    <ul class="nav nav-second-level collapse" aria-expanded="false">
                                        <li>
                                            <a href="{{ url('dashboard/settings') }}">
                                                <i class="fas fa-user-cog"></i> &nbsp;Settings</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('configs.index') }}">
                                                <i class="fa fa-cogs nav_icon"></i> &nbsp;Site Configs</a>
                                        </li>

                                    </ul>
                                </li>
                            @endif
                            <li>
                                <a href="#"><i class="fa fa-plus nav_icon"></i> &nbsp;Others
                                    <span style="float: right;"><i class="fas fa-caret-down"></i> </span></a>
                                <ul class="nav nav-second-level collapse" aria-expanded="false">
                                    <li>
                                        <a href="{{ url('dashboard/albumsList') }}">
                                            <i class="fa fa-image nav_icon"></i> &nbsp;Albums List</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('dashboard/referuser') }}"><i class="fa fa-users nav_icon"></i>
                                            &nbsp;Refer
                                            users</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li>
                            <a href="{{ url('user/logout') }}">
                                <i style="margin-right: 10px;" class="fas fa-sign-out-alt"></i>&nbsp; Logout</a>
                        </li>


                    @endif
                </ul>

                <!-- //sidebar-collapse -->

            </nav>

        </div>

    </div>


<!--  <li>
        <a href="{{ route('promo_investor', [true]) }}" onclick="clickAndDisable(this)">
                <i class="fas fa-donate"></i> &nbsp; OneMonth Promo Invesment</a>
    </li> -->

    <!--left-fixed -navigation-->

    <!-- header-starts -->
    <div class="sticky-header header-section desktopView"
         style="background-color: white; border-bottom: 3px solid #580949;">
        @if(!app('request')->session()->get('back_to_admin') && $settings->show_ad == 0 && env('HEADER_AD') && Auth::user()->type == 0 && $headers == 'b4uglobal.com')
            <div class="col-md-3 col-sm-6 col-xs-12">
                @else
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    @endif

                    <!--logo -->
                        <div class="logo" style="padding:6px; float:left; background-color: white;">
                            <a href="{{ url('/') }}">
                                @include('layouts.logo')
                            </a>
                        </div>

                        @if(Auth::user()->u_id =='B4U00016447')
                            <div style="  margin-top: 40px;  float: right;">
                                <a href="{{ url('locale/en') }}">English</a>
                                | <a href="{{ url('locale/ma') }}">Malaysia</a>
                            </div>
                        @endif
                    </div>
                    @if(!app('request')->session()->get('back_to_admin') && $settings->show_ad == 0 && env('HEADER_AD') && Auth::user()->type == 0 && $headers == 'b4uglobal.com')
                        <div class="headerAD col-md-6" id="adsense" style="margin-top: 10px;">
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- header -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:600px;height:80px"
                                 data-ad-client="ca-pub-8618087093269654"
                                 data-ad-slot="4460302842"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                        </div>
                    @endif

                    @if(!app('request')->session()->get('back_to_admin') && $settings->show_ad == 0 && env('HEADER_AD') && Auth::user()->type == 0 && $headers == 'b4uglobal.com')
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            @else
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    @endif

                                    <div class="profile_details" style="padding:8px; margin-top:15px; color: black;">
                                        <?php
                                        $name = '';
                                        if (isset(Auth::user()->name)) {
                                            $name = Auth::user()->name;
                                            if (strlen($name) > 6) {
                                                $name = substr($name, 0, 6) . '..';
                                            }
                                        }
                                        //$disk = Storage::disk('gcs');
                                        //$file = $disk->get('test-public.png');
                                        //echo $file;
                                        //exit;
                                        ?>

                                        @if(!is_null(Auth::user()->rank) && Auth::user()->rank != 'NULL')
                                            <img style="width:50px;"
                                                 src="{{ asset('/images/rank_'. Auth::user()->rank .'.png') }}"> |
                                        @else                <!-- <strong><p>B4U {{Auth::user()->rank}}</p></strong> -->
                                        @if(Auth::user()->photo == "")
                                            <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                                            style="width:40px; height:40px; border-radius:100%; border:3px solid black;"
                                                            src="{{ asset('/images/noimage-round.jpg') }}" alt=""> </a>
                                        @else
                                            <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                                            style="width:40px; height:40px; border-radius:100%; border:3px solid black;"
                                                            src="<?= 'https://storage.googleapis.com/b4ufiles/profile_Img/' . Auth::user()->photo; ?>"
                                                            alt=""> </span></a>
                                        @endif

                                        @endif
                                        <a href="{{ url('dashboard/accountdetails') }}"
                                           style="color: black;">{{ $name }} | {{Auth::user()->u_id}}</a>

                                        @if(Auth::user()->msg_bit == "1" )
                                            | <a href="#" data-toggle="modal" data-target="#showmsg1"
                                                 onclick="viewModelFuncS({{Auth::user()->id}})">
                                                <img style="padding-bottom:10px; width:25px; height:35px;"
                                                     src="{{asset('/images/bell-new.png') }}" alt="Bell"></a>
                                        @else
                                            |
                                            <img style="padding-bottom:10px; width:25px; height:35px; border-radius:100%; "
                                                 src="{{asset('/images/bell-no-new.png')}}" alt="Bell">
                                        @endif

                                        |
                                        <a href="{{ url('user/logout') }}">

                                            <img style="padding-bottom:10px;" src="{{asset('/images/logout.png')}}"
                                                 alt="Logout">
                                        </a>


                                    </div>

                                    <div class="clearfix"></div>

                                </div>

                                <div class="clearfix"></div>

                        </div>

                        <!-- ##MOBILE HEADER## -->
                        <div class="sticky-header header-section mobileView"
                             style="background-color: white; border-bottom: 3px solid #580949; ">
                            <div class="">
                                <button id="showLeftPush" class="btn btn-sm"
                                        style="background-color: #580949; color: white; float: left;"><i
                                            class="fa fa-bars"></i></button>

                                <!--logo  style="padding:6px; width:100px; float:left; background-color: white;" -->
                                <div class="logo"
                                     style="background-color: white; margin-top: -15px; width: 20%; float: left; ">
                                    <a href="{{ url('/') }}"><!-- style="padding-left:15px !important;" -->
                                        @include('layouts.logo')
                                    </a>
                                </div>

                                <!-- </div>

                                <div class="">
                         -->
                                <div class="profile_details" style=" color: black; padding-right: 5px; float: right;">


                                    @if(!is_null(Auth::user()->rank) && Auth::user()->rank != 'NULL')
                                        <img style="width:50px;"
                                             src="{{ asset('/images/rank_'. Auth::user()->rank .'.png') }}"> |
                                    @else                <!-- <strong><p>B4U {{Auth::user()->rank}}</p></strong> -->
                                    @if(Auth::user()->photo == "")
                                        <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                                        style="width:40px; height:40px; border-radius:100%; border:3px solid black;"
                                                        src="{{ asset('/images/noimage-round.jpg') }}" alt=""> </a>
                                    @else
                                        <a href="{{ url('dashboard/accountdetails') }}"><span class="prfil-img"><img
                                                        style="width:40px; height:40px; border-radius:100%; border:3px solid black;"
                                                        src="<?= 'https://storage.googleapis.com/b4ufiles/profile_Img/' . Auth::user()->photo; ?>"
                                                        alt=""> </span></a>
                                    @endif

                                    @endif
                                    <span class="uid"><a href="{{ url('dashboard/accountdetails') }}"
                                                         style="color: black;">{{Auth::user()->u_id}}</a>
                </span>
                                </div>

                            </div>

                        </div>
                        <!-- ##MOBILE HEADER## -->

                        <!-- Show Msg -->
                        <div id="showmsg1" class="modal fade" role="dialog">

                            <div class="modal-dialog">

                                <!-- Modal content-->

                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        <h4 class="modal-title" style="text-align:center;">Message</h4>

                                    </div>

                                    <div class="modal-body">

                                        <div id="bodycontent3">
                                            <!-- display Ajax Models For Admin Users here -->
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /Show Msg -->
                        <script type="text/javascript">
                            function clickAndDisable(link) {

                                link.onclick = function (event) {
                                    event.preventDefault();
                                }
                            }


                            function getDateInString(date) {

                                var created_date = new Date(date);
                                // var curr_day =created_date.getDate();
                                // var curr_month = created_date.getMonth();
                                // var curr_year = created_date.getFullYear();
                                var curr_hour = created_date.getHours();
                                var curr_min = created_date.getMinutes();
                                var curr_sec = created_date.getSeconds();
                                var datestring = created_date.getFullYear() + "-" + ("0" + (created_date.getMonth() + 1)).slice(-2) + "-" + ("0" + created_date.getDate()).slice(-2);
                                return datestring + " " + curr_hour + ":" + curr_min + ":" + curr_sec;
                            }

                            function viewModelFuncS(id) {
                                //alert(id);
                                // var  mtype = "accsount";
                                jQuery.ajax({
                                    type: "POST",
                                    url: '{{{URL::to("")}}}/dashboard/showmsg',
                                    data: {
                                        id: id,
                                        "_token": "{{ csrf_token() }}",
                                    },
                                    //	cache: false,
                                    success: function (response) {
                                        $("#bodycontent3").html(response);
                                    },
                                    error: function (response) {
                                    }
                                });
                            }
                        </script>
                        <!--  LiveChat Widget-->
                    @include('liveChatScript')
                        <!-- //header-ends -->
{{--@include('admin.modals_admin')--}}
