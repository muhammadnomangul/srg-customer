@include('header')

<!-- //header-ends -->

<!-- main content start-->

<div id="page-wrapper">

	<div class="main-page signup-page">

        <h3 class="title1"> User Logs</h3>

		@include('messages')
		<div class="row">
			
			<div class="col-xs-12">
				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">

					<table id="myTable" class="table table-hover">

						<thead>

						<tr>
                            <th>Id</th>

                            <th>Statement</th>

                            <th>Amount</th>

							<th>Approved By</th>

                            <th>Date</th>

                        </tr>

						</thead>

						

						<tbody>
						
						@foreach($daily_logs_list as $logs)

							<tr>
                                
                                 <td>{{$logs->id}}</td>
                                <td><b>{{$logs->title}}</b>

                                    <p>{{$logs->details}}</p></td>


                                <td style="text-align: center;">{{$logs->amount}}
                                    &nbsp;<sub>{{$logs->currency}}<sub></td>

                                <td>{{$logs->approved_by}}</td>

                                <td>{{$logs->created_at}} </td>

							</tr>

                           

						@endforeach

						</tbody>

					</table>

				</div>

			</div>
		</div>
		<div class="row">

		</div>

	</div>

</div>



<script>

	$(document).ready( function () {

		$('#myTable').DataTable( {

			"pagingType": "full_numbers"

		});

	});



	$(".submit1").click(function(){
		//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");

		if(conf){
			return true;
		}
		else{
			return false;
		}

	});

</script>

@include('footer')