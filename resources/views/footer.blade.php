<style type="text/css">
    #footer {
        position: fixed;
        bottom: 0;
        width: 100%;
        left: 0;
        height: 80px;
        margin-bottom: 30px;
    }

    .footerAD {
        display: none;
    }

    P

    @media screen and (max-width: 992px) {

        .footerAD {
            display: block;
            text-align: center;
        }
    }
</style>

@if(!app('request')->session()->get('back_to_admin') && $settings->show_ad == 0  && env('FOOTER_AD'))
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <div class="footerAD" id="footer">
                    <div class="row">
                        <div class="col-md-12 col-12 col-sm-12">
                            <div id="adsense">
                                <script async
                                        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- header -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:600px;height:80px"
                                     data-ad-client="ca-pub-8618087093269654"
                                     data-ad-slot="4460302842"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            <!-- <span>Copyright &copy;<script>document.write(new Date().getFullYear());</script><a class="purple" href="{{ url('/member/area') }}"> Cosplay-Exchange</a> All rights reserved <small style="font-size: 20%;">| This system is made with <i class="fas fa-heart" style="color: #FB2224" aria-hidden="true"></i> by <a class="purple" href="javascript:void(0);">TheWebIcon</a></small>
                    </span> -->

            </div>
        </div>
    </footer>
@endif
<div class="footer">
    <p>All Rights Reserved &copy; {{$settings->site_name}} {{date('Y')}}</p>
</div>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script type="text/javascript">
    function postData(url = ``, data = {}) {
        // Default options are marked with *
        return fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
    }

    function disableOther(button) {
        if (button !== 'showLeftPush') {
            classie.toggle(showLeftPush, 'disabled');
        }
    }

    function filePreview(input, id) {
        id = '#' + id + '-preview';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(id + ' img').remove();
                $(id).append('<img class="preview-img img-responsive" src="' + e.target.result + '"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function showPopup(status, title, message, timer) {
        var icon = 'success';
        if (status == false) {
            icon = 'error';
        }
        Swal.fire({
            icon: icon,
            title: title,
            text: message,
            timer: timer
        })
    }

    function ajx(url, type, data, successCallback, dataType) {
        if (dataType == '' || dataType == undefined)
            dataType = 'json';
        $.ajax({
            url: url,
            type: type,
            data: data,
            dataType: dataType,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
            .done(successCallback)
            .fail(function (res) {
                //(res);
            });
    }

    $(function () {

        $('.select2').select2({
            placeholder: 'Select an option'
        });

        $(document).on('click', 'button[type="submit"],input[type="submit"]', function (e) {
            $('button').prop('disabled', true);
            $('input[type="submit"]').prop('disabled', true);
            $(this).closest('form').submit();
            var buttonText = $(this).html();
            $(this).html('<i class="fa fa-spinner spin"></i> Processing...');
            $(this).html(buttonText);
        });
        $(document).on('click', 'button[type="submit"]', function (e) {
            $('button').prop('disabled', true);
            $(this).closest('form').submit();
            $('input, a').prop('disabled', true);
            var buttonText = $(this).html();
            $(this).html('<i class="fa fa-spinner spin"></i> Processing...');
            $(this).html(buttonText);
        });
        $('.datepicker').datepicker({
            "setDate": new Date(),
            "autoclose": true,
            "dateFormat": 'dd-mm-yy'
        });
        $("input[type=file]").change(function () {
            filePreview(this, $(this).attr('id'));
        });
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;
        showLeftPush.onclick = function () {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-172512767-1');

    });


</script>
<script src="{{ asset('js/classie.js')}}"></script>
<script src="{{ asset('js/bootstrap.js')}}"></script>
<script src="{{ asset('js/jquery-ui.js')}}"></script>
<script src="{{ asset("public/malik/moon-dropzone.js") }}"></script>
<script src="{{ asset('public/sweetalert/sweetalert2@9.js') }}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172512767-1"></script>

</body>
</html>
