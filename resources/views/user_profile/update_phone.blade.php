<div style="text-align: center;"><h4><label>Edit Phone Information</label></h4></div>
<hr>
<form class="form-horizontal" method="post" action="{{action('UsersController@updateprofilePhone')}}"
      enctype="multipart/form-data" mod="eai">
    <div class="form-content">
{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Name:</label>--}}
{{--            <div class="col-sm-9">--}}
{{--                <input class="form-control" type="text" name="name" required--}}
{{--                       value="{{ old('name') ? old('name') :Auth::user()->name }}" {{empty(Auth::user()->name) ?  '' : '' }}>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Email:</label>--}}
{{--            <div class="col-sm-9" style="display: flex">--}}
{{--                <input class="form-control" type="email" name="email" required--}}
{{--                       value="{{ old('email') ? old('email') :Auth::user()->email }}" {{empty(Auth::user()->email) ?  '' : 'disabled=true' }}>--}}

{{--                @if(\Illuminate\Support\Facades\Auth::user()->email_verified_at)--}}
{{--                    <button type="button" class="btn btn-success">Email Verified</button>--}}
{{--                @else--}}
{{--                    <a href="{{ route('send_verification_email_') }}" class="btn btn-danger">Click to Verify an--}}
{{--                        Email</a>--}}
{{--                @endif--}}

{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group">
            <label style="text-align:left;" class="control-label col-sm-3">Phone No :</label>
            <div class="col-sm-9">
                <input class="form-control" id="phone_no" name="phone_no"
                       value="{{ old('phone_no') ? old('phone_no'): Auth::user()->phone_no}}" type="tel"
                       placeholder="" {{empty(Auth::user()->phone_no) ?  '' : '' }}>

                <input name="country_coe" id="country_coe" type="hidden" autocomplete="off">

                <span id="valid-msg" class="hide" style="color:green">✓ Valid</span>
                <span id="error-msg" class="hide" style="color:red"></span>

            </div>
        </div>

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Country :</label>--}}
{{--            <div class="col-sm-9">--}}
{{--                <select class="form-control select2" name="Country" id="countryid"--}}
{{--                        onChange="searchBanks();" {{empty(Auth::user()->Country) ?  '' : '' }}>--}}
{{--                    <option value="">Select Country</option>--}}
{{--                    @foreach($countries as $country)--}}
{{--                        <option {{ old('Country')  == $country->country_name ? 'selected' : '' }} value="{{$country->country_name}}"--}}
{{--                                @if(Auth::user()->Country == "$country->country_name" )selected @endif>{{$country->country_name}} </option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Bank Name:</label>--}}
{{--            <div class="col-sm-9">--}}
{{--                <select class="form-control select2" name="bank_name" id="bank_nameId"--}}
{{--                        onChange="selectBank();" {{empty(Auth::user()->bank_name) ?  '' : '' }}>--}}
{{--                    <option value="">Select Bank</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Account Title:</label>--}}
{{--            <div class="col-sm-9">--}}

{{--                <input class="form-control" type="text" name="account_name"--}}
{{--                       value="{{ old('account_name') ? old('account_name') : Auth::user()->account_name}}" {{empty(Auth::user()->account_name) ?  '' : '' }}>--}}


{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">@if(Auth::user()->Country == 'Pakistan') IBAN--}}
{{--                : <small><a href="#" onclick="Ibanimg();" data-target="#imageiban">(What is this?)</a></small> @else--}}
{{--                    Account No: @endif</label>--}}
{{--            <div class="col-sm-9">--}}

{{--                <input class="form-control" type="text" name="account_no"--}}
{{--                       {{empty(Auth::user()->account_no) ?  '' : '' }}--}}
{{--                       value="{{ old('account_no') ? old('account_no') : Auth::user()->account_no}}"--}}
{{--                       {{empty(Auth::user()->account_no) ?  '' : '' }}--}}
{{--                       placeholder="Example: PK12ABCD1234567891234567" style="text-transform:uppercase;"--}}
{{--                       oninput="this.value = this.value.toUpperCase();">--}}


{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label style="text-align:left;" class="control-label col-sm-3">Acc. Holder Ph#:</label>--}}
{{--            <div class="col-sm-9">--}}

{{--                <input class="form-control" type="text" name="acc_hold_No" placeholder="Example: 03331234567"--}}
{{--                       {{empty(Auth::user()->acc_hold_No) ?  '' : '' }}--}}
{{--                       value="{{ old('acc_hold_No') ? old('acc_hold_No') : Auth::user()->acc_hold_No}}">--}}


{{--            </div>--}}
{{--        </div>--}}


{{--        <div class="form-group">--}}

{{--            <label style="text-align:left;" class="control-label col-sm-3">Next of kin <a href=""--}}
{{--                                                                                          title="Next of kin refers to a person's closest living blood relative. The next-of-kin relationship is important in determining inheritance rights if a person dies without a will and has no spouse and/or children. State law establishes next-of-kin relationships and inheritance priorities. The deceased's estate becomes state property if no legal heir can be identified.">(?)</a>--}}
{{--                :</label>--}}
{{--            <div class="col-sm-9">--}}

{{--            <textarea class="form-control" name="kin_bank_info"--}}
{{--                      {{empty(Auth::user()->kin_bank_info) ?  '' : '' }}--}}
{{--                      rows="5">{{ old('kin_bank_info') ? old('kin_bank_info') :html_entity_decode(Auth::user()->kin_bank_info) }}</textarea>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
                <button type="button" id="update_phone_button" class="btn btn-sm btn-primary o-s">Update Information</button>
{{--                <a href="{{ route('user.kyc.get') }}" class="btn btn-sm btn-info">Update KYC</a>--}}
            </div>
        </div>
    </div>
</form>




