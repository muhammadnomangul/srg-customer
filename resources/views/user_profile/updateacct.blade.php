@include('header')
<!-- Phone Input CSS-->
<link rel="stylesheet" href="{{ asset('home/flag_phone_input/css/intlTelInput.css')}}">

<!-- Phone Input JS-->
<script src="{{ asset('home/flag_phone_input/js/intlTelInput.js')}}"></script>
<!-- //header-ends -->

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">


{{--        <div class="alert alert-danger alert-dismissible">--}}
{{--            <button type="button" class="close" data-dismiss="alert">&times;</button>--}}
{{--            <p><b>Important Notice!</b>--}}
{{--                <br/>--}}
{{--                <br> Dear User:--}}
{{--                <br/>Kindly check your information and make sure everything is correct. B4U Global is implementing 2FA--}}
{{--                security on the website. After security implementation, you will need an OTP(One time password) on your--}}
{{--                mobile and email to make changes, perform withdrawals and fund transfers. The OTP will be sent to the--}}
{{--                following number and email, so make sure it's correct. Kindly contact our support if there is something--}}
{{--                missing or incorrect.--}}
{{--                <br/>Thank you.--}}
{{--                <br/><br/>Regards--}}
{{--                <br/>B4U Global Team.--}}
{{--            </p>--}}
{{--        </div>--}}

{{--        <h5 class="alert alert-info">SMS Payable Amount--}}
{{--            : {{ \Illuminate\Support\Facades\Auth::user()->sms_payable_amount }}$</h5>--}}
{{--        <h5 class="alert alert-danger">Paid Amount : {{ \Illuminate\Support\Facades\Auth::user()->sms_paid_amount }}--}}
{{--            $</h5>--}}


        <h3 class="title1">Profile</h3>

        @include('messages')


{{--        <div class="row">--}}
{{--            <div class="sign-up  widget-shadow col-md-8 " style="padding: 20px">--}}
{{--                @include('user.partials.edit_wallet_account_card')--}}
{{--            </div>--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="sign-up  widget-shadow col-md-10">--}}
{{--                    @include('user.partials.profile_card')--}}
{{--                </div>--}}

{{--                <div class="sign-up widget-shadow col-md-10" style="padding: 30px;   margin-top: 10px;  ">--}}
{{--                    @include('user.partials.change_2fa_authentication')--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}


        <div class="row" style=" margin-top: 20px; ">
            <div class="sign-up  widget-shadow col-md-8" style="padding: 20px; ">
                @include('user_profile.update_phone')
            </div>
{{--            <div class="sign-up col-md-4">--}}

{{--                <div class="sign-up widget-shadow col-md-10" style="padding: 20px;">--}}
{{--                    @include('user.partials.change_user_avatar_card')--}}
{{--                </div>--}}

{{--                <div class="sign-up  widget-shadow col-md-10" style="padding: 30px;   margin-top: 10px;  ">--}}
{{--                    @include('user.partials.edit_password_card')--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>


    </div>

</div>


<script>

    function Ibanimg() {
        //alert(id);
        jQuery.ajax({
            type: "get",
            url: '{{{URL::to("")}}}/dashboard/viewibanimage',
            data: {"_token": "{{ csrf_token() }}"},
            //	cache: false,
            success: function (response) {
                $("#pcontent123").html(response);
            }, error: function (response) {
                // alert('Be patient, System is under maintenance');
                //$("#CallSubModal").modal('show');
            }
        });
    }

    function selectBank() {
        var bankname = $("#bank_nameId").val();

        if (bankname == '') {
            $("#bankname2").show();
        } else {
            $("#bankname2").hide();
        }
    }

    function searchBanks() {
        var countryname = $("#countryid").val();
        if (countryname == '') {
            alert("Please select country first!");
        } else if (countryname != '') {
            var oldBankName = "{{Auth::user()->bank_name}}";
            $.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/banksDetails',
                dataType: 'html',
                data: {country: countryname, "_token": "{{ csrf_token() }}",},
                success: function (data) {
                    //	concole.log(data);
                    var object = jQuery.parseJSON(data);
                    // var html = "";

                    var html = "<option value='B4U Wallet'>B4U Wallet</option>";

                    if (object != "" && object != "Bank Not Found") {
                        $("#bank_nameId").html("");
                        //$("#bankname2").hide();
                        for (var j = 0; j < object.length; j++) {
                            //console.log(object[j].bank_name + "--" + object[j].id);
                            var selected = '';
                            if (oldBankName == object[j].bank_name) {

                                selected = 'selected';

                            }

                            html += "<option value='" + object[j].bank_name + "'" +
                                selected + "> " + object[j].bank_name + "</option>";
                        }


                        /* html +="<option value='B4U Wallet' */
                        //html = "<option value=''> Bank Name Not Found </option>";
                        //$('#bank_nameId').append(html);
                        $('#bank_nameId').html(html);


                    } else if (object != "Bank Not Found") {
                        $("#bankname2").show();
                        // html = "<option value=''> Bank Name Not Found </option>";
                        $('#bank_nameId').empty().append(html);
                    }
                }
            });
        }
    }
    $( "#update_phone_button" ).prop( "disabled", true );
    var input = document.querySelector("#phone_no");
    var errorMsg = document.querySelector("#error-msg");
    var validMsg = document.querySelector("#valid-msg");
    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
    var iti = window.intlTelInput(input, {
        nationalMode: false,
        autoHideDialCode: true,
        // autoPlaceholder: "off",
        autoPlaceholder: "polite",
        formatOnDisplay: true,
        placeholderNumberType: "MOBILE",
        preferredCountries: ['us', 'pk', 'my', 'gb'],
        utilsScript: "{{ asset('home/flag_phone_input/js/utils.js')}}",
    });


    let countryRecord = iti.getSelectedCountryData();
    input.addEventListener('countrychange', function () {


        let countryRecord = iti.getSelectedCountryData();
        document.querySelector("#country_coe").value = countryRecord.iso2;
    });
    document.querySelector("#country_coe").value = countryRecord.iso2;

    var reset = function () {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
        // $( "#update_phone_button" ).prop( "disabled", false );
    };
    var release = function () {

        $( "#update_phone_button" ).prop( "disabled", false );
        $( "#valid-msg" ).show();
    };
    var freeze = function () {

        $( "#update_phone_button" ).prop( "disabled", true );
    };

    input.addEventListener('blur', function () {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                validMsg.classList.remove("hide");
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                release();
            } else {
                input.classList.add("error");

                var errorCode = iti.getValidationError();
                if (errorMap[errorCode]) {
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                    freeze();
                } else {
                    errorMsg.innerHTML = errorMap[0];
                    errorMsg.classList.remove("hide");
                    freeze();
                }
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
            }
        }
    });
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);

    $(document).ready(function () {
        $( "#update_phone_button" ).prop( "disabled", true );
        $("#bankname2").hide();
        searchBanks();
    });
</script>
@include('modals')
@include('footer')
