@include('header')

<!-- main content start-->

<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">TOP Promo Achievers</h3>
        @include('messages')

        @if(count($errors) > 0)
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        @foreach ($errors->all() as $error)
                            <i class="fa fa-warning"></i> {{ $error }}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif


        <div class="row">
            <div width="80%" class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
                <div width="100%" style="text-align:center;">
                    <div><h3 class="title" style="text-align:center;"><font color="red" align="center"> Top
                                Investors</font></h3></div>
                </div>


                <table id="myTable" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>UserID</th>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Plan</th>
                        <th>Self Deposit</th>
                        <th>Level_1</th>
                        <th>Level_2</th>
                        <th>Level_3</th>
                        <th>Level_4</th>
                        <th>Level_5</th>
                        <th>Total Downline Investment</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1; ?>
                    @if(isset($promo_investors))
                        @foreach($promo_investors as $top)

                            <?php $total_amount = (isset($top->child_amount_level_1) ? $top->child_amount_level_1 : 0)
                                + (isset($top->child_amount_level_2) ? $top->child_amount_level_2 : 0)
                                + (isset($top->child_amount_level_3) ? $top->child_amount_level_3 : 0)
                                + (isset($top->child_amount_level_4) ? $top->child_amount_level_4 : 0)
                                + (isset($top->child_amount_level_5) ? $top->child_amount_level_5 : 0);   ?>

                            @if($total_amount >= 5000 || $OMP == 1 || $OMP == 2)
                                 
                                    <tr>

                                        <td>{{$count}}</td>
                                        <td>{{$top->user_u_id}}</td>
                                        <td>{{$top->name}}</td>
                                        <td>{{$top->Country}}</td>
                                        <td>{{$top->plan_id}}</td>
                                        <td>
                                            ${{ isset($top->self_deposit) ?  number_format($top->self_deposit, 2) : 0}}</td>
                                        <td>
                                            ${{ isset($top->child_amount_level_1) ?  number_format($top->child_amount_level_1, 2) : 0 }}</td>
                                        <td>
                                            ${{ isset($top->child_amount_level_2) ? number_format($top->child_amount_level_2, 2) : 0 }}</td>
                                        <td>
                                            ${{ isset($top->child_amount_level_3) ? number_format($top->child_amount_level_3, 2) : 0  }}</td>
                                        <td>
                                            ${{ isset($top->child_amount_level_4) ? number_format($top->child_amount_level_4, 2) : 0 }}</td>
                                        <td>
                                            ${{ isset($top->child_amount_level_4) ?  number_format($top->child_amount_level_5, 2) : 0 }}</td>
                                        <td>${{ isset($total_amount) ? number_format($total_amount,2) : 0}}</td>
                                        <td>{{$from}}</td>
                                        <td>{{$to}}</td>
                                    </tr>
                                
                            @endif

                            <?php $count++; ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

<script>

    $(function () {
        $("#datepicker").datepicker();
        $("#datepicker2").datepicker();
    });

    $(document).ready(function () {
        $('#myTable').DataTable({

            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]

        });

    });

</script>
@include('footer')