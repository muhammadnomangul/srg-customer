<script>

    function viewModelFuncDP(id) {
        $('input.withdrawal-ref').val(id);
    }

    function viewDetailsFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/viewWithdrawDetailsPost') }}',
            data: {id: id, type: "withdraw", "_token": "{{ csrf_token() }}",},
            success: function (response) {
                $("#wcontent").html(response);
            }, error: function (response) {
                //$("#CallSubModal").modal('show');
                // alert('System is under maintenance, we will be back in few hours');
            }
        });
    }

</script>