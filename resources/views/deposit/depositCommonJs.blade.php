<script>

    {{-- View deposit details function  --}}
    function viewDetailsFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/viewDetailsPost') }}',
            data: {id: id, type: "deposit", "_token": "{{ csrf_token() }}",},
            success: function (response) {
                $("#dcontent").html(response);
            }, error: function (response) {
                // alert("Something went wrong. Please try again");
                //$("#CallSubModal").modal('show');
            }
        });
    }

    // View deposit proof
    function viewProofFunc(id) {
        jQuery.ajax({
            type: "POST",
            url: '{{ \Illuminate\Support\Facades\URL::to('dashboard/viewProofPost') }}',
            data: {id: id, type: "deposit", "_token": "{{ csrf_token() }}",},
            success: function (response) {
                $("#pcontent").html(response);
            }, error: function (response) {
                // alert("Something went wrong. Please try again");
                //$("#CallSubModal").modal('show');
            }
        });
    }

</script>