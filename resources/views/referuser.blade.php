@include('header')

<style>
    .require-asterisks{
        color: red;
        padding: 0 0 0 10px;
    }
</style>
<link rel="stylesheet" href="{{ asset('home/flag_phone_input/css/intlTelInput.css')}}">

<!-- Phone Input JS-->
<script src="{{ asset('home/flag_phone_input/js/intlTelInput.js')}}"></script>
<!-- //header-ends -->
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">Refer users to {{$settings->site_name}}</h3>
                @include('messages')
				<div class="sign-up-row widget-shadow">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('dashboard/saveuser') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Full Name<span class="require-asterisks">*</span></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required="required" autofocus="autofocus">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address<span class="require-asterisks">*</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="required">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password<span class="require-asterisks">*</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required="required">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password<span class="require-asterisks">*</span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required="required">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Input Field Starts -->
                        <div class="form-group{{ $errors->has('phone_no') ? ' has-error' : '' }}">
                            <label for="phone_no" class="col-md-4 control-label">Phone<span class="require-asterisks">*</span></label>

                       <div class="col-md-6">
                        <input type="tel" class="form-control" id="phone_no" placeholder="" name="phone_no"
                               value="{{ old('phone_no') }}" required>
                        <input name="country_coe" id="country_coe" type="hidden" autocomplete="off">
                        <input name="country_name" id="country_name" type="hidden" autocomplete="off">
                        <span id="valid-msg" class="hide" style="color:green">✓ Valid</span>
                        <span id="error-msg" class="hide" style="color:red"></span>


                                @if ($errors->has('phone_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_no') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <!-- Input Field Ends -->
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary referUserRegisterBtn">
                            <i class="fa fa-btn fa-user"></i> Register
                        </button>
                    </div>
                </div>
            </form>
            <strong>You can also do this by sharing your referral link:
                <button class="btn btn-primary"
                        data-clipboard-text="{{url('/')}}/register?{{Auth::user()->u_id}}" style="float: right;"
                        onclick="CopyToClipboard('div1')">copy
                </button>
                </br>
                <div class="col-md-8 col-sm-6" id="div1">
                    {{url('/')}}/register?{{Auth::user()->u_id}}
                </div>
                <div class="col-md-2 col-sm-6">

                </div>


            <!--<a href="{{url('/')}}/register?{{Auth::user()->u_id}}">Invite Referral Partner</a>-->
            </strong>

        </div>
    </div>
</div>
<script>

    var input = document.querySelector("#phone_no");
    var errorMsg = document.querySelector("#error-msg");
    var validMsg = document.querySelector("#valid-msg");
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
    var iti = window.intlTelInput(input, {
        nationalMode: false,
        autoHideDialCode: true,
        // autoPlaceholder: "off",
        autoPlaceholder: "polite",
        formatOnDisplay: true,
        placeholderNumberType: "MOBILE",
        preferredCountries: ['us', 'pk', 'my', 'gb'],
        utilsScript: "{{ asset('home/flag_phone_input/js/utils.js')}}",
    });

    let countryRecord = iti.getSelectedCountryData();
    var countryData = iti.getSelectedCountryData().name;
    input.addEventListener('countrychange', function () {
        let countryRecord = iti.getSelectedCountryData();
        var countryData = countryRecord.name;
        document.querySelector("#country_coe").value = countryRecord.iso2;
        document.querySelector("#country_name").value = countryData.split("(")[0];
    });
    document.querySelector("#country_coe").value = countryRecord.iso2;
    document.querySelector("#country_name").value = countryData.split("(")[0];
    var reset = function () {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };

    input.addEventListener('blur', function () {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                validMsg.classList.remove("hide");
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                $('.referUserRegisterBtn').prop('disabled', false)
            } else {
                input.classList.add("error");
                var errorCode = iti.getValidationError();
                if (errorMap[errorCode]) {
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                } else {
                    errorMsg.innerHTML = errorMap[0];
                    errorMsg.classList.remove("hide");
                }
                input.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                $('.referUserRegisterBtn').prop('disabled', true)
            }
        }
    });
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);

    function CopyToClipboard(containerid) {
        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
            document.execCommand("copy");
            alert("Refer Link has been copied")
        }
    }

</script>
@include('footer')
