@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h3 class="title1">GH user</h3>
				
				@include('messages')

				<div class="sign-up-row widget-shadow">
					<form method="post" action="{{action('HomeController@withdraw')}}">
					<div class="sign-u">
						<div class="sign-up1">
							<h4>Select user :</h4>
						</div>
						<div class="sign-up2">
							<select class="form-control" style="height:50px;" name="user">
								<option value="">Select user</option>
								@foreach($users as $user)
								<option value="{{$user->id}}">{{$user->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="clearfix"> </div>
					</div>
					
					<div class="sign-u">
						<div class="sign-up1">
							<h4>Amount :</h4>
						</div>
						<div class="sign-up2">
							<input type="text" name="plan" required>
						</div>
						<div class="clearfix"> </div>
					</div>

					<div class="sub_home">
						<input type="submit" value="Submit">
						<div class="clearfix"> </div>
					</div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
				</form>
				</div>
			</div>
		</div>
		@include('footer')