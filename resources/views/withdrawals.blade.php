@include('header')

<?php
$blockWithdrawals = false;
if (date('Y-m-d') > '2021-07-20' && date('Y-m-d H:i:s') < '2021-07-24 03:00:00'){
    $blockWithdrawals = true;
}
if (isset($accountsInfo->reference_bonus)) {
    $reference_bonus = $accountsInfo->reference_bonus;
} else {
    $reference_bonus = 0;
}

function calculate_profit($currency)
{
    if ($currency != "") {
        $curr = strtolower($currency);
        $pval = "profit_" . $curr;

        if (isset($accountsInfo->$pval)) {
            $amount = $accountsInfo->$pval;
            $profit = $amount;
        } else {
            $profit = 1;
        }
        return $profit;
    }
}

function calculate_profit_usd($currency)
{
    if ($currency != "") {
        $curr = strtolower($currency);
        $pval = "profit_" . $curr;
        $rate = "rate_" . $curr;
        if (isset($accountsInfo->$pval) && isset($ratesQuery->$rate)) {
            $amount = $accountsInfo->$pval;
            $rate = $ratesQuery->$rate;
            $profit_usd = $amount * $rate;
        } else {
            $profit_usd = 1;
        }

        return $profit_usd;
    }
}
function calculate_sold_bal($currency)
{
    if ($currency != "") {
        $curr = strtolower($currency);
        $sval = "sold_bal_" . $curr;
        if (isset($accountsInfo->$sval)) {
            $amount = $accountsInfo->$sval;
            $sold = $amount;
        } else {
            $sold = 1;
        }
        return $sold;
    }
}
function calculate_sold_bal_usd($currency)
{
    if ($currency != "") {
        $curr = strtolower($currency);
        $sval = "sold_bal_" . $curr;
        $rate = "rate_" . $curr;
        if (isset($accountsInfo->$sval) && isset($ratesQuery->$rate)) {
            $amount = $accountsInfo->$sval;
            $rate = $ratesQuery->$rate;
            $sold_usd = $amount * $rate;
        } else {
            $sold_usd = 1;
        }
        return $sold_usd;
    }
}

?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page signup-page">
        <h3 class="title1">Your Withdraw Funds List</h3>
        @include('messages')
        <br>
        <a class="btn btn-default" href="#" data-toggle="modal" data-target="#withdrawalModal">
            <i class="fa fa-plus"></i> Request withdrawal</a>
        <div style="text-align: right;">
            {{-- @if(isset($userdonation))
                 <p>Your Donation : <label>${{number_format($userdonation, 2)}}</label></p>
             @endif--}}
            @if(Auth::user()->is_call_sub == 1 && $accountsInfo->charges > 0)
                {{--                <p>Your Pending Call Subscriction Charges : <label>${{$accountsInfo->charges}}</label></p>--}}
            @endif
        </div>

        <div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table">
            <table id="myTablelg" class="table table-hover">
            </table>
        </div>
    </div>
</div>

<div id="withdrawalModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center;">
                    @if($blockWithdrawals)
                        Request Withdrawals stopped, We apologise for inconvience
                    @else
                        Payment will be sent to your receiving address.
                    @endif
                </h4>
            </div>
            <div class="modal-body">
                @if($blockWithdrawals)
                    <p style="text-align: center; color: red;"><b>Alert!</b> Unable to process withdrawal due to Eid Holidays. Kindly try after 23-July-2021.</p>
                @else
                    <span class="errors" style="color: red"></span>
                    <form id="formABC" style="padding:3px;" data-toggle="validator" role="form" method="post"
                          action="{{ url('dashboard/withdrawal') }}" autocomplete="off" mod="dwith">
                        <div class="form-content">
                            <div class="form-group">
                                <label for="select" class="control-label">Select Withdrawal Mode * : </label>
                                <div class="form-input">
                                    <select name="withdraw_mode" id="withdraw_mode"
                                            class="form-control amount_type withdrawmode" required>
                                        <option value="">--Select--</option>
                                        <option {{ old('withdraw_mode')  == 'Bonus'? 'selected' : '' }} value="Bonus">
                                            Bonus
                                        </option>
                                        <option {{ old('withdraw_mode') == 'Profit' ? 'selected' : '' }} value="Profit">
                                            Profit
                                        </option>
                                        @if(Auth::user()->u_id != 'B4U0720')
                                            <option {{ old('withdraw_mode') == 'Sold' ? 'selected' : '' }} value="Sold">
                                                Sold Balance
                                            </option>
                                        @endif
                                        {{--                                        <option value="FundTransfer">Fund Transfer</option>--}}
                                        {{--                                        <option></option>--}}
                                    </select>
                                </div>
                                @error('withdraw_mode')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="currency">
                                <label for="select" id="label_id" class="control-label">Select Withdrawal Currency *
                                    : </label>
                                <div class="form-input">
                                    <select name="currency" id="currency" class="form-control amount_type currencyVal"
                                            required>
                                        @foreach($currencies as $currency)
                                            <option {{ old('currency') == $currency->code ? 'selected' : '' }} value="{{$currency->code}}">{{$currency->code}}
                                                ({{$currency->name}})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('currency')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="fundtype">
                                <label for="select" id="Fundlabel" class="control-label">Fund Type : </label>
                                {{--                                <small style="color: red">(Note: FundTransfer is not allowed for one week. Sorry for the--}}
                                {{--                                    inconvenience.)</small>--}}
                                <div class="form-input">
                                    <select name="fund_type" id="fund_type" class="form-control fundtransfer_type">
                                        <option value="#">Select Fund Type</option>
                                        <option class="hideSold hideBonus" value="">Cash Withdrawal</option>
                                        <option class="hideSold" value="cashbox">Transfer To My Cash Box</option>
                                        <option {{ old('fund_type') == 'fundtransfer' ? 'selected' : '' }} value="fundtransfer">
                                            Fund Transfer to Account
                                        </option>
                                    </select>
                                </div>
                                @error('fund_type')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="fundreciever" style="display: none;">
                                <label for="select" class="control-label">Select Fund Beneficiary * : </label>
                                <div class="form-input">
                                    <select name="fund_receivers_id" id="fund_receiver_id" class="form-control select2"
                                            required >
                                        <option value="">--Select Beneficiary--</option>
                                        @foreach($beneficiaries as $beneficiary)
                                            @if(isset($beneficiary->beneficiary_uid))
                                                <option {{ old('fund_receivers_id') == $beneficiary->beneficiary_uid ? 'selected' : '' }}  value="{{$beneficiary->beneficiary_uid}}">{{$beneficiary->beneficiary_uid}}</option>
                                            @endif
                                        @endforeach
                                        <option {{ old('fund_receivers_id') == 'notexist' ? 'selected' : '' }} value="notexist">
                                            Add New Beneficiary
                                        </option>
                                    </select>
                                </div>
                                @error('fund_receivers_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="fundreciever2"
                                 style="{{ old('fund_receivers_id') == 'notexist' ? '' : 'display: none' }}">
                                <label for="text" id="ben_id" class="control-label fund2">New beneficiary B4U-ID *
                                    : </label>
                                <div class="form-input">
                                    <input type="text" name="fund_receivers_id2" id="fundreciever_id2"
                                           class="form-control"
                                           placeholder="Enter B4U-ID (B4U0001) Of Fund Reciever User."
                                           value="{{ old('fund_receivers_id2') }}" required />
                                    <!--</br> <p id="fund2" style="color:red;"></p>-->
                                </div>
                                @error('fund_receivers_id2')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group" id="amount_with">
                                <label for="select" id="amt_convert" class="control-label"></label><br/>
                                <label for="select" id="amountid" class="control-label label_id">Amount* :</label>
                                <div class="form-input">
                                    <input type="number" min="40" name="amount" id="with_amt" class="form-control"
                                           placeholder="Enter amount here"
                                           value="{{ old('amount') }}" required />
                                </div>
                                @error('amount')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            @if(Auth::user()->Country == "Europe"
                                    || Auth::user()->Country == "France"
                                    || Auth::user()->Country == "Canada"
                                    || Auth::user()->Country == "France, Metropolitan"
                                    || Auth::user()->Country == "French Guiana"
                                    || Auth::user()->Country == "French Polynesia"
                                    || Auth::user()->Country == "French Southern Territories"
                                    || Auth::user()->Country == "Georgia"
                                    || Auth::user()->Country == "Germany"
                                    || Auth::user()->Country == "Greece"
                                    || Auth::user()->Country == "Greenland"
                                    || Auth::user()->Country == "Italy"
                                    || Auth::user()->Country == "United Kingdom"
                                    || Auth::user()->Country == "United States"
                                    || Auth::user()->Country == "United States minor outlying islands"
)
                                @if(Auth::user()->usa_account_holder_name == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Account Holder Name:</label>
                                    <div class="form-input">
                                            <input class="form-control" type="text" name="usa_account_holder_name"
                                                   value="{{ old('usa_account_holder_name') ? old('usa_account_holder_name') : Auth::user()->usa_account_holder_name}}" {{empty(Auth::user()->usa_account_holder_name) ?  '' : '' }} required />
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "Canada"
                               || Auth::user()->Country == "United States"
                               || Auth::user()->Country == "United States minor outlying islands" )
                                @if(Auth::user()->date_of_birth == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Date of birth:</label>
                                    <div class="form-input">
                                            <input class="form-control" type="date" name="date_of_birth"
                                                   value="{{ old('date_of_birth') ? old('date_of_birth') : Auth::user()->date_of_birth}}" {{empty(Auth::user()->date_of_birth) ?  '' : '' }} required />
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "Canada"
                               || Auth::user()->Country == "United States"
                               || Auth::user()->Country == "United States minor outlying islands")
                                @if(Auth::user()->full_address == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Full Address:</label>
                                    <div class="form-input">
                                            <input class="form-control" type="text" name="full_address" required
                                                   value="{{ old('full_address') ? old('full_address') : Auth::user()->full_address}} {{empty(Auth::user()->full_address) ?  '' : '' }}">
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "Canada" )
                                @if(Auth::user()->institute_bank_number == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Institution Number (of your bank )</label>
                                    <div class="form-input">
                                            <input class="form-control" type="text" name="institute_bank_number" required
                                                   value="{{ old('institute_bank_number') ? old('institute_bank_number') : Auth::user()->institute_bank_number}}" {{empty(Auth::user()->institute_bank_number) ?  '' : '' }}>
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "Canada" && Auth::user()->transit_number == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Transit Number </label>
                                    <div class="form-input">
                                            <input class="form-control" type="text" name="transit_number" required
                                                   value="{{ old('transit_number') ? old('transit_number') : Auth::user()->transit_number}}" {{empty(Auth::user()->transit_number) ?  '' : '' }}>
                                    </div>
                                </div>
                            @endif
                            @if(Auth::user()->Country == "Canada"
                               || Auth::user()->Country == "United States"
                               || Auth::user()->Country == "United States minor outlying islands" )
                                @if( Auth::user()->full_bank_name == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Full Bank Name:</label>
                                    <div class="form-input">
                                            <input class="form-control" type="text" name="full_bank_name" required />
{{--                                                   value="{{ old('full_bank_name') ? old('full_bank_name') : Auth::user()->full_bank_name}}" {{empty(Auth::user()->full_bank_name) ?  '' : '' }}>--}}
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "United Kingdom"
                               || Auth::user()->Country == "Europe"
                                    || Auth::user()->Country == "France"
                                    || Auth::user()->Country == "France, Metropolitan"
                                    || Auth::user()->Country == "French Guiana"
                                    || Auth::user()->Country == "French Polynesia"
                                    || Auth::user()->Country == "French Southern Territories"
                                    || Auth::user()->Country == "Georgia"
                                    || Auth::user()->Country == "Germany"
                                    || Auth::user()->Country == "Greece"
                                    || Auth::user()->Country == "Greenland"
                                    || Auth::user()->Country == "Italy"
                                     )
                                @if(Auth::user()->iban_number == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Short Number / IBAN:</label>
                                    <div class="form-input">
                                        <input class="form-control" type="text" name="iban_number" required
                                               value="{{ old('iban_number') ? old('iban_number') : Auth::user()->iban_number}}" {{empty(Auth::user()->iban_number) ?  '' : '' }}>
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(Auth::user()->Country == "United States"
                               || Auth::user()->Country == "United States minor outlying islands"
                                     )
                                @if(Auth::user()->routing_number == '')
                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label ">Routing Number:</label>
                                    <div class="form-input">
                                        <input class="form-control" type="text" name="routing_number" required
                                               value="{{ old('routing_number') ? old('routing_number') : Auth::user()->routing_number}}" {{empty(Auth::user()->routing_number) ?  '' : '' }}>
                                    </div>
                                </div>
                                @endif
                            @endif
                            @if(
   Auth::user()->Country == "Canada"
   || Auth::user()->Country == "United States"
   || Auth::user()->Country == "United States minor outlying islands"

)
                                @if( (Auth::user()->bank_account_type) == 'saving'
     || (Auth::user()->bank_account_type) == "checking")

                                @else

                                <div class="form-group">
                                    <label style="text-align:left;" class="control-label col-sm-6">
                                        Account  Type</label>

                                    <div class="col-sm-12">
                                        {{--                <input class="form-control" type="radio" name="bank_account_type"--}}
                                        {{--                       value="{{ old('bank_account_type') ? old('bank_account_type') : Auth::user()->bank_account_type}}" {{empty(Auth::user()->bank_account_type) ?  '' : '' }}>--}}
                                        {{--                <input class="form-control" type="radio" name="bank_account_type"--}}
                                        {{--                       value="{{ old('bank_account_type') ? old('bank_account_type') : Auth::user()->bank_account_type}}" {{empty(Auth::user()->bank_account_type) ?  '' : '' }}>--}}
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="radio" id="customRadioInline1" {{(Auth::user()->bank_account_type) == 'checking' ?  'checked' : '' }} value="checking" name="bank_account_type"  required class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Checking Account</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="radio" id="customRadioInline2" {{(Auth::user()->bank_account_type) == 'saving' ?  'checked' : '' }} value="saving" name="bank_account_type" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2">Saving Account </label>
                                            </div>
                                        </div>
                                        {{--                <div class="custom-control custom-radio custom-control-inline">--}}
                                        {{--                    <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">--}}
                                        {{--                    <label class="custom-control-label" for="customRadioInline1">Normal Account</label>--}}
                                        {{--                </div>--}}
                                        {{--                <div class="custom-control custom-radio custom-control-inline">--}}
                                        {{--                    <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">--}}
                                        {{--                    <label class="custom-control-label" for="customRadioInline2">Saving Account </label>--}}
                                        {{--                </div>--}}

                                    </div>
                                </div>
                                @endif
                            @endif
                            <br>
                            <div class="form-group">
                                <label for="" id="don_amt_convert" style="color: red"></label>
                                <br>
                                <label for="select" id="donationid" class="control-label label_id">Donation for B4U
                                    Foundation<a
                                            href="http://b4ufoundation.com/"> (what is this?)</a>
                                    :</label>


                                <div class="form-input">
                                    <input type="number" name="donation" id="don_amt" class="form-control" min="0"
                                           placeholder="Enter donations amount here"
                                           value="{{ old('donation') ? old('donation')  : 0 }}">
                                </div>

                                @error('donation')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">

                                <label for="" id="donationid_amt_convert" style="color: red">
                                </label>
                                <br>

                                <label for="select" id="donationid" class="control-label label_id">Donate in B4U
                                    Coronavirus
                                    Relief Fund
                                    <!-- <a href="http://b4ufoundation.com/"> (what is this?)</a>: -->
                                </label>

                                <div class="form-input">
                                    <input type="number" name="donation2" id="don_amt2" class="form-control" min="0"
                                           placeholder="Enter COVID-19 Relief Fund donations here"
                                           value="{{ old('donation2') ? old('donation2') : 0 }}">
                                </div>

                                @error('donation2')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="_key" value="{{ csrf_token() }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="button" id="submit12" class="btn btn-primary o-s">Submit</button>
                                <span style=" margin-left:10px; color:red;"> Note : You can request only 1 Profit withdrawal within One Month. </span>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>


<script>
    var Errors = '<?php echo(count($errors) > 0) ?>';
    $(document).ready(function () {
        console.log('hejknfklsn');
        //$("#CallSubModal").modal('show');
        $("#formABC").submit(function (e) {

            //disable the submit button
            $("#submit1").attr("disabled", true);
            return true;
        });
        if (parseFloat(Errors) === 1) {
            $('#withdrawalModal').modal('show');
        }
        $('#myTable').DataTable({
            "pagingType": "full_numbers"
        });
        let fundtype = $("#fund_type").val();
        let fund_id = $("#fund_receiver_id").val();

        if (fundtype == "fundtransfer") {
            $("#fundreciever").show();
            $("#fund_receiver_id").attr("required", "true");
        } else {

            $("#fundreciever").hide();
            $('#fund_receiver_id').removeAttr('required');
            $("#fundreciever2").hide();
            $('#fundreciever_id2').removeAttr('required');
        }
        if (fund_id == "notexist") {
            $("#fundreciever2").show();
            $("#fund_receiver_id2").attr("required", "true");
        } else {
            $("#fundreciever2").hide();
            $('#fundreciever_id2').removeAttr('required');
        }
        $("#withdraw_mode").change(function () {
            if ($(this).val() == "Bonus") {
                $('.currencyVal option[value!="USD"]').hide();
                $('.currencyVal').val('');
                $('.currencyVal').val('USD');
                $('.hideSold').removeClass("hide");
                $(".hideBonus").prop("selected", false);
                $('.hideBonus').addClass("hide");
            } else if ($(this).val() == "Profit") {
                $('.currencyVal option[value!=""]').show();
                $('.hideSold').removeClass("hide");
            } else if ($(this).val() == "Sold") {
                $('.currencyVal option[value!=""]').show();
                $(".hideSold").prop("selected", false);
                $('.hideSold').addClass("hide");
            }

        }).trigger("change");

        $("#fund_type").change(function () {
            if ($(this).val() == "fundtransfer") {
                $("#fundreciever").show();
                $("#fund_receiver_id").attr("required", "true");
            } else if ($(this).val() == "" || $(this).val() == "cashbox") {
                //alert($(this).val());
                $("#fund_receiver_id").val('');
                $("#fundreciever_id2").val('');
                $("#fundreciever").hide();
                $('#fund_receiver_id').removeAttr('required');
                $("#fundreciever2").hide();
                $('#fundreciever_id2').removeAttr('required');
            }
        }).trigger("change");

        $(".amount_type").change(function () {
            var selectedVal = $('#withdraw_mode option:selected').val();

            $("strong").hide();
            var curr = $('#currency option:selected').val();
            var x = document.getElementById("with_amt").value;
            if (selectedVal != "" && curr != "") {
                //alert(selectedVal + curr);
                $("strong").hide();
                jQuery.ajax({
                    type: "POST",
                    url: '{{{URL::to("")}}}/dashboard/getAccountInfoPost',
                    data: {
                        curr: curr,
                        type: selectedVal,
                        "_token": "{{ csrf_token() }}",
                    },
                    //	cache: false,
                    success: function (response) {
                        $("#label_id").html(response);

                        if ($('#error').val() == 1) {
                            $('#submit1').attr('disabled', true);
                        } else {
                            $('#submit1').attr('disabled', false);
                        }
                    },
                    error: function (response) {
                        alert("System is under maintenance, please wait for some hours.");
                    }
                });
                amountconverstion(curr, x);
            }
        });
        $("#fund_receiver_id").change(function () {
            if ($(this).val() == "notexist") {
                $("#fundreciever2").show();
                $("#fundreciever_id2").attr("required", "true");

            } else {
                $("#fundreciever2").hide();
                $('#fundreciever_id2').removeAttr('required');
            }

        }).trigger("change");
        document.getElementById("fundreciever_id2").addEventListener("focusout", searchBaneficiary);
        $(document).on('click', '#submit1', function () {

            var actualamount = $('#availablemt').val();
            var inputamount = parseFloat($('#with_amt').val()) || 0;
            var donationamt = parseFloat($('#don_amt').val()) || 0;
            var coronadonationamt = parseFloat($('#don_amt2').val()) || 0;
            var sumall = (inputamount + donationamt + coronadonationamt);
            var withdraw_mode = $('#withdraw_mode').val();

            //var currency=$('#currency').val();
            var amountid = $('#with_amt').val();
            if (withdraw_mode == '') {
                $(".errors").html("Withdrawal mode must be selected");
            } else if (!amountid) {
                $(".errors").html("Amount must be require");
            } else if (sumall > actualamount) {
                $('.errors').html("You have insufficient Balance for this Transaction");
            } else if ($("#fund_receiver_id").is(":visible") && $('#fund_receiver_id').val() === '') {
                $(".errors").html("Fund Receiver mode must be selected");
            } else if ($("#fundreciever_id2").is(":visible") && $("#fundreciever_id2").val() === '') {
                $('.errors').html("beneficiary B4U-ID must be required");
            } else {
                $('#submit1').closest('form').submit();
            }
        });

    });

    function viewProofFunc1(id) {
        //alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{{URL::to("")}}}/dashboard/viewProofPost1',
            data: {
                id: id,
                type: "deposit",
                "_token": "{{ csrf_token() }}",
            },
            //	cache: false,
            success: function (response) {
                $("#pcontent1").html(response);
            },
            error: function (response) {
                alert("System is under maintenance, please wait for some hours.");
            }
        });
    }

    function viewDetailsFunc(id) {
        //	alert(id);
        jQuery.ajax({
            type: "POST",
            url: '{{ route('view.withdrawal.details') }}',
            data: {
                id: id,
                type: "withdraw",
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {
                $("#wcontent").html(response);
            },
            error: function () {
                alert("System is under maintenance, please wait for some hours.");
            }
        });
    }

    function viewModelFuncUWD(id) {
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('span');
        var text = document.createTextNode(dirtyString);
        container.appendChild(text);
        return container.innerHTML; // innerHTML will be a xss safe string
    }

    function searchBaneficiary() {
        let fundReceiver = $("#fundreciever_id2").val();

        if (fundReceiver) {
            $.ajax({
                type: "POST",
                url: '{{ url('dashboard/beneficiaryDetails') }}',
                data: {
                    benficiaryid: fundReceiver,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    $("#ben_id").html(response);
                },
                error: function (response) {
                    if (response.status === 422) {
                        $("#ben_id").html('<strong style="color:red;">Beneficiary id not valid. Please add correct Beneficiary !(Note: You cannot enter your own User_id) </strong>')
                    }
                }
            });
        }

    }

    function isNumberKey(txt, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            //Check if the text already contains the . character
            if (txt.value.indexOf('.') === -1) {
                return true;
            } else {
                return false;
            }
        } else {
            if (charCode > 31 &&
                (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    }

    /////////////////////////////
    $(document).ready(function () {

        var i = 1;
        $('#myTablelg').DataTable({
            processing: true,
            serverSide: true,
            "bLengthChange": false,
            "ajax": {
                "url": "{{url('dashboard/withdrawals/json')}}",
                "type": "POST"
            },
            "deferRender": true,
            "columns": [
                /* title will auto-generate th columns */
                {
                    "data": "id", "title": "Sr.#", "searchable": true,
                    "render": function (data, type, full, meta) {
                        return i++;
                    }
                },

                {
                    "data": "id", "title": "Portfolio#", "searchable": true,
                    "render": function (data, type, row, meta) {
                        var itemID = data;
                        return '<a data-toggle="modal" data-target="#WDetailsModal" href="#" onclick="viewDetailsFunc(' + row.id + ')">W-' + itemID + '</a>';
                    }
                },

                {
                    "data": "rate", "title": "Amount", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        if (row.currency == 'USD') {
                            return " " + (+row.amount).toFixed(2);
                        } else {
                            return " " + (+row.amount).toFixed(5);
                        }
                    }
                },

                {
                    "data": "currency", "title": "Currency", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        return row.currency;
                    }
                },


                {
                    "data": "withdrawal_fee", "title": "Transfer Fee", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        return row.withdrawal_fee;
                    }
                },

                {
                    "data": "donation", "title": "Donation", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {
                        return row.donation;
                    }
                },

                {
                    "data": "payment_mode", "title": "Withdrawal Mode", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {

                        var returnValue = row.payment_mode;

                        if (row.fund_type == "fundtransfer") {
                            returnValue += '<span style="color:red;"> (FundTransfer) </span>';

                        }

                        if (row.fund_type == "cashbox") {
                            return returnValue + ' <span class="label label-success" style="background-color: #9e2187">Cash Box</span>'

                        }
                        if (row.fund_type == "callcharges") {
                            return returnValue + ' <span class="label label-success" style="background-color: #9e2187">Call Charges</span>'

                        }

                        return returnValue

                    }
                },
                {
                    "data": "status", "title": "Status", "orderable": true, "searchable": false,
                    'render': function (data, type, row) {

                        var returnValue = '';

                        if (row.is_verify == 1 && row.status == "Pending") {
                            returnValue = '<p style="color: orange;"><strong>Verified</strong></p>';
                        } else if (row.is_paid == 1) {
                            returnValue = '<p style="color: green;"><strong>Deposited</strong></p>';
                        } else {
                            returnValue = row.status;
                        }

                        if (row.flag_dummy == 1) {
                            returnValue += "<font color='red'>(Dummy)</font>";
                        }
                        return returnValue;

                    }
                },


                {
                    "data": "created_at", "title": "Created", "orderable": true, "searchable": true,
                    'render': function (data, type, row) {
                        var created_date = new Date(row.created_at);

                        var curr_hour = created_date.getHours();
                        var curr_min = created_date.getMinutes();
                        var curr_sec = created_date.getSeconds();

                        var datestring = created_date.getFullYear() + "-" + ("0" + (created_date.getMonth() + 1)).slice(-2) + "-" + ("0" + created_date.getDate()).slice(-2);

                        return datestring + " " + curr_hour + ":" + curr_min + ":" + curr_sec;

                        //return created_date.toISOString().slice(0, 10);
                    }
                },
                {
                    "data": "fund_receivers_id", "title": "Fund Receiver Id", "orderable": true, "searchable": true
                },
                // {
                //     "data": "trade_amount", "title": "Amount", "orderable": true, "searchable": true,

                //        "render": function (data, type, row) {
                //             if(row.currency === 'USD'){
                //                 return row.trade_amount.toFixed(2);
                //             }else{
                //                 return row.crypto_amount.toFixed(6);
                //             }
                //         }
                // },
                // {
                //     "data": "today_profit", "title": "Profit Amount", "orderable": true, "searchable": true,

                //        "render": function (data, type, row) {
                //             if(row.currency === 'USD'){
                //                 return row.today_profit.toFixed(6);
                //             }else{
                //                 return row.crypto_profit.toFixed(6);
                //             }
                //         }
                // },

            ],
            "order": [8, "desc"]
        });
    });


    let lastEnteredAmount = '';
    let DonationLastEnteredAmount = '';
    let DonationCovidLastEnteredAmount = '';

    // $(document).on('keyup', '#with_amt', function (e) {
    //     let validationResponse = validateAmountField(e, $('#with_amt'), $('#amt_convert'));
    //     if (validationResponse) {
    //         lastEnteredAmount = $(this).val();
    //         var currencytype = $('#currency option:selected').val();
    //         amountconverstion(currencytype, $(this).val());
    //     } else {
    //         $(this).val(lastEnteredAmount);
    //     }
    // });

    $(document).on('input', '#with_amt', function () {
        var currencytype = $('#currency option:selected').val();
        var x = document.getElementById("with_amt").value;
        if (x != '') {
            amountconverstion(currencytype, x);
        } else {
            $('#amt_convert').html("");
        }
    });

    // $(document).on('keyup', '#don_amt', function (e) {
    //     let validationResponse = validateAmountField(e, $('#don_amt'), $('#don_amt_convert'));
    //     if (validationResponse) {
    //         DonationLastEnteredAmount = $(this).val();
    //         $('#don_amt_convert').html('');
    //     } else {
    //         $(this).val(DonationLastEnteredAmount);
    //     }
    // });

    // $(document).on('keyup', '#don_amt2', function (e) {
    //     let validationResponse = validateAmountField(e, $('#don_amt2'), $('#donationid_amt_convert'));
    //     if (validationResponse) {
    //         DonationCovidLastEnteredAmount = $(this).val();
    //         $('#donationid_amt_convert').html('');
    //     } else {
    //         $(this).val(DonationCovidLastEnteredAmount);
    //     }
    // });


    function validateAmountField(ElementEventObject, TargetElement, ErrorElement) {
        let KeyNumber = ((ElementEventObject.which) ? ElementEventObject.which : ElementEventObject.keyCode);
        let x = $(TargetElement).val();
        let validationTest = true;
        if (KeyNumber === 8) {
            return true;
        }
        if ((KeyNumber > 47 && KeyNumber < 58) || KeyNumber === 110) {
            if (x.indexOf('.') === -1 && x.length > 8) {
                $(ErrorElement).html('<strong style="color: red">Only 8 digits allowed before (.)</strong>');
                validationTest = false;
            }
            if (x.indexOf('.') !== -1) {
                // check after . only 8 numbers allowed.
                let NumbersInArray = x.split('.');
                if (NumbersInArray[1].length > 8) {
                    $(ErrorElement).html('<strong style="color: red">Only 8 digits allowed after (.)</strong>');
                    validationTest = false;
                }
            } else if (x.indexOf('.') > 0) {
                $(ErrorElement).html('<strong style="color: red">Amount is too long, Maximum 16 digits allowed for amount</strong>');
                validationTest = false;
            }
            if (x.length > 17) {
                $(ErrorElement).html('<strong style="color: red">Amount is too long, Maximum 16 digits allowed for amount</strong>');
                validationTest = false;
            }
            return validationTest;
        } else {
            return false;
        }
    }


    var input = document.getElementById('with_amt');
    input.onkeydown = function () {
        var key = event.keyCode || event.charCode;
        if (key == 8) {
            $('#amt_convert').html("");
        }
    };

    function amountconverstion(currencytype, amountval) {

        let amountRegex = /^\d+(\.\d{1,9})?$/;
        if (amountval && amountRegex.test(amountval)) {
            jQuery.ajax({
                type: "POST",
                url: '{{{URL::to("")}}}/dashboard/currenconvert',
                data: {currencytype: currencytype, amount: amountval, "_token": "{{ csrf_token() }}",},
                //cache: false,
                success: function (response) {
                    $('#amt_convert').html(response);
                }, error: function (response) {
                    alert('Too many tries.');
                }
            });
        } else {
            $('#amt_convert').html("");
        }
    }

    //////////////////////////////////////
</script>
@include('modals')
@include('footer')
