@include('header')

 

<!-- //header-ends -->
	<!-- main content start-->
	<div id="page-wrapper">
		<div class="main-page signup-page">
			<h3 class="title1">Gallery </h3>
                @include('messages')
				<div class="sign-up-row widget-shadow">
              
					 <h3>Uploads Images</h3>

					<form action="{{ url('dashboard/uploadimage') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
						{!! csrf_field() !!}

						@if (count($errors) > 0)

							<div class="alert alert-danger">

								<strong>Whoops!</strong> There were some problems with your input.<br><br>

								<ul>

									@foreach ($errors->all() as $error)

										<li>{{ $error }}</li>

									@endforeach

								</ul>

							</div>

						@endif


						@if ($message = Session::get('success'))

						<div class="alert alert-success alert-block">

							<button type="button" class="close" data-dismiss="alert">×</button>

								<strong>{{ $message }}</strong>

						</div>

						@endif


						<div class="row">
							<div class="form-group">
								<div class="file-loading">
									<input id="image-file" type="file"  accept="image/*" data-min-file-count="1" multiple class="form-control">
								</div>
							
							</div>    
							
						</div>

					</form> 
					<div class="row">
						<div id="content" class='list-group gallery'>

							@if($images->count())

								@foreach($images as $image)
								
							
								<div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>

									<a class="thumbnail fancybox" rel="ligthbox" href="{{url('/images/gallery/'.$image->image_name)}}">

										<img class="img-responsive" alt="" src="{{asset('/images/gallery/'.$image->image_name)}}" />

										<div class='text-center'>

											<small class='text-muted'>{{ $image->title }}</small>

										</div> <!-- text-center / end -->

									</a>

									<form action="{{ url('dashboard/deleteimage/'.$image->id.'/'.$image->image_name) }}" method="POST">

									<input type="hidden" name="_method" value="delete">

									{!! csrf_field() !!}

									<button type="submit" class="close-icon btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>

									</form>

								</div> <!-- col-6 / end -->

								@endforeach

							@endif

						</div> <!-- list-group / end -->

					</div> <!-- row / end -->
                
				</div>
		</div>
	</div>
    <script src="{{ asset('js/multi-fileinput.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/multi-theme.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/multi-popper.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/fancybox/jquery.mousewheel-3.0.4.pack.js')}}" type="text/javascript"></script>
	<!--<script src="{{ asset('js/fancybox/jquery.fancybox-1.3.4.pack.js')}}" type="text/javascript"></script>-->
	<script src="{{ asset('js/jquery.fancybox.min.js')}}"></script>
	<script type="text/javascript">
        $("#image-file").fileinput({
            theme: 'fa',
			uploadUrl: '{{{URL::to("")}}}/dashboard/uploadImages',
            uploadExtraData: function() {
                return {
                    albumId : {{$albumid}} ,_token: "{{ csrf_token() }}",
                };
				location.reload();
            },
            allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
            overwriteInitial: false,
            maxFileSize:2048,
            maxFilesNum: 10
        }); 
  
		$(document).ready(function(){
			$("a[rel=ligthbox]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
		});
	</script>
	@include('footer')