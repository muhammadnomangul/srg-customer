@include('header')
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
			
				<h3 class="title1">{{$title}}</h3>
				<!--<a class="btn btn-default" href="{{ url('dashboard/mpdeposits')}}">View Pending Deposits</a>-->
				 <a class="btn btn-default" href="{{ url('dashboard/emailTemplate')}}">Add New Email Templates</a>
				 @include('messages')
				<div class="bs-example widget-shadow table-responsive" data-example-id="hoverable-table"> 
					<table id="myTable" class="table table-hover"> 
						<thead> 
							<tr> 
								<th>Sr#</th> 
								<th>Title</th>
								<th>Message</th>
								<th>Email Body</th>
                                <th>Date created</th>
                                <th>Options</th>
							</tr> 
						</thead> 
						<?php $count=1;
							
						?>
						<tbody> 
							@foreach($emailTemplates as $email)
							
							<tr>
							
								<th scope="row">{{$count}}</th>
                                <td>{{$email->title}}</td>
                                 <td>{{$email->message}}</td>
                                <td>{{ str_limit($email->body, $limit = 100, $end = '...') }}</td>
                                 <td>{{$email->created_at}}</td>
								<td>
									<a class="btn btn-primary submit1" href="{{ url('dashboard/emailTemplateEdit') }}/{{$email->id}}">Update</a> 
									
								 </td>
							</tr> 
							<?php $count++;?>
							@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
		<script>
	$(document).ready( function () {
		$('#myTable').DataTable();
	} );

	$(".submit1").click(function(){
			//	alert(" i am here");
		var conf = confirm("Do you really want to proceed ?");
		if(conf){
			return true;
		}
		else{
			return false;
		}
	});
	</script>
        @include('modals')
		@include('footer')