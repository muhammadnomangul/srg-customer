<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    //Side Bar//
    'Dashboard' => 'Papan Pemuka',
    'Support' => 'Sokongan',
    'Deposits' => 'Deposits',
    'Withdrawals' => 'Pengeluaran',
    'Sold' => 'Dijual',
    'Investment plans' => 'Pelan Pelaburan',
    'Refer users' => 'Rujuk Pengguna',
    'Your Partners' => 'Rakan anda',


    //Dashboard//
    'Your ID' => 'ID anda',
    'Investment Plan' => 'Pelan Pelaburan',
    'Parent ID' => 'ID Ibu Bapa',
    'Your Partners ' => 'Your Partners ',
    'DEPOSITED' => 'DEPOSITED',
    'PROFIT' => 'KEUNTUNGAN',
    'REF. BONUS' => 'REF. BONUS',
    'Withdrawals' => 'Pengeluaran',
    'Attracted Funds' => 'Dana Tertarik',
    'Last Profit' => 'Keuntungan Terakhir',
    'Last Bonus' => 'Last Bonus',
    'Sold Balance' => 'Jual Baki',
    'LATEST DEPOSITS' => 'TERKINI DEPOSITS',
    'LATEST WITHDRAWALS' => 'TERKINI PENGELUARAN ',
    'View All Deposits' => 'Lihat Semua Deposits',
    'View All Withdrawals' => 'Lihat Semua Pengeluaran',
    'Amount' => 'Amaun',
    'Type' => 'Taipkan',
    'Date' => 'Tarikh',


    ////Deposits////
    'Your deposits' => 'Anda deposits',
    'New deposit' => 'Baru deposit',
    //table
    'Portfolio' => 'Portfolio',
    'Payment mode' => 'Pembayaran mod',
    'Rate' => 'Kadar',
    'Total' => 'Jumlah',
    'Status' => 'Status',
    'Date created' => 'Tarikh dicipta',
    'Action' => 'Tindakan',
    //popupon-newdeposit
    'Make deposit' => 'Membuat deposit',
    'Select Deposit Mode' => 'Memilih Deposit Mod',
    'Select' => 'Memilih',
    'New-Investment' => 'Baru-Pelaburan',
    'Re-Investment' => 'Pelaburan Semula',
    'Select Currency' => 'Memilih Mata wang',
    'Select Reinvest Type' => 'Memilih Semula Jenis',
    'Bonus' => 'Bonus',
    'Profit' => 'Keuntungan',
    'Sold Balance' => 'Jual Baki',
    'Continue' => 'Teruskan',


];
