<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    //Side Bar//
    'Dashboard' => 'Dashboard',
    'Support' => 'Support',
    'Deposits' => 'Deposits',
    'Withdrawals' => 'Withdrawals',
    'Sold' => 'Sold',
    'Investment plans' => 'Investment plans',
    'Refer users' => 'Refer Users',
    'Your Partners' => 'Your Partners',

    //Dashboard//
    'Your ID' => 'Your ID',
    'Investment Plan' => 'Investment Plan',
    'Parent ID' => 'Parent ID',
    'Your Partners ' => 'Your Partners ',
    'DEPOSITED' => 'DEPOSITED',
    'PROFIT' => 'PROFIT',
    'REF. BONUS' => 'REF. BONUS',
    'Withdrawals' => 'Withdrawals',
    'Attracted Funds' => 'Attracted Funds',
    'Last Profit' => 'Last Profit',
    'Last Bonus' => 'Last Bonus',
    'Sold Balance' => 'Sold Balance',
    'LATEST DEPOSITS' => 'LATEST DEPOSITS',
    'LATEST WITHDRAWALS' => 'LATEST WITHDRAWALS',
    'View All Deposits' => 'View All Deposits',
    'View All Withdrawals' => 'View All Withdrawals',
    'Amount' => 'Amount',
    'Type' => 'Type',
    'Date' => 'Date',
    'Donations' => 'Donations',

    /////Deposits///
    'Your deposits' => 'Your deposits',
    'New deposit' => 'New deposit',
    //table
    'Portfolio' => 'Portfolio',
    'Payment mode' => 'Payment mode',
    'Rate' => 'Rate',
    'Total' => 'Total',
    'Status' => 'Status',
    'Date created' => 'Date created',
    'Action' => 'Action',
    ///popupon-newdeposit

    'Make deposit' => 'Make deposit',
    'Select Deposit Mode' => 'Select Deposit Mode',
    'Select' => 'Select',
    'New-Investment' => 'New-Investment',
    'Re-Investment' => 'Re-Investment',
    'Select Currency' => 'Select Currency',
    'Select Reinvest Type' => 'Select Reinvest Type',
    'Bonus' => 'Bonus',
    'Profit' => 'Profit',
    'Sold Balance' => 'Sold Balance',
    'Continue' => 'Continue',


];
