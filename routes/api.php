<?php
header('Access-Control-Allow-Origin: *');
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('call_us', 'CallCenterController@callAllowed')->name('call_us');
Route::post('updateFirstCall', 'CallCenterController@updateFirstCall')->name('update_first_call');
//Route::get('call', 'CallCenterController@call')->name('call');
Route::get('mailer', 'CallCenterController@mailer')->name('mailer');
Route::get('forgetPassword', 'CallCenterController@forgetPassword')->name('forgetPassword');

Route::get('cb','CallCenterController@cb')->name('cb');
Route::post('sendMail','CallCenterController@sendMail')->name('sendMail');
Route::post('image','CallCenterController@image')->name('image');
Route::get('forgetMail','CallCenterController@forgetMail')->name('forgetMail');
Route::get('RefMail','CallCenterController@RefMail')->name('RefMail');
Route::get('verifyEmail','CallCenterController@verifyEmail')->name('verifyEmail');
/**
 * @Purpose, Give user info to B4U Wallet.
 * */
Route::get('/user-info/{value}', function (\App\Http\Requests\API\BFWallet\Users\GetUserInfoRequest $getUserInfoRequest) {
    $User = \App\User::getUserMinimumInfo($getUserInfoRequest->value);
    return response()->json([
        'status' => true,
        'data' => new \App\Http\Resources\BFGlobal\Users\UserInfoResource($User),
        'message' => trans('user.found'),
    ]);
});


