<?php

use App\Model\Old\Portfolio;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

Artisan::command('pr', function () {

    \DB::table('previous_reports')->orderBy('id','asc')->chunk(100,function($prs){
        foreach($prs as $pr){
            $user_account = \App\UserAccounts::where('user_id',$pr->user_id);
            if($user_account->count()){
                $user_account = $user_account->first();
                if($pr->type=='total_bonus'){
                    $user_account->update(['old_ref_bonus' => $pr->balance]);
                }

                if($pr->type=='total_profit_btc'){
                    $user_account->update(['old_profit_btc' => $pr->balance]);
                }

                if($pr->type=='total_profit_eth'){
                    $user_account->update(['old_profit_eth' => $pr->balance]);
                }

                if($pr->type=='total_profit_usd'){
                    $user_account->update(['old_profit_usd' => $pr->balance]);
                }

            }

        }
    });


})->describe('Display an inspiring quote');



Artisan::command('referral:import', function () {
    \App\Model\Referral::import();



})->describe('Display an inspiring quote');
Artisan::command('referral:update_rates', function () {
    \App\Model\Referral::updateToUsdAll();



})->describe('Display an inspiring quote');

Artisan::command('referral:to_usd', function () {
    \App\Model\Referral::updateToUsdAll();



})->describe('Display an inspiring quote');

Artisan::command('co', function () {

    foreach(\DB::select('SELECT * FROM `daily_profit_bonus` where date(created_at) = \'2019-01-06\'  ') as $tv){
        $user = \App\User::where('u_id',$tv->user_id)->first();
        $user_account = \App\UserAccounts::where('user_id',$user->id)->first();
        if($tv->currency == 'USD'){
            $user_account->profit_usd = $user_account->profit_usd - $tv->today_profit;
            $user_account->save();
            \App\daily_profit_bonus::where('id',$tv->id)->delete();
        }else{
            $c = "profit_".strtolower($tv->currency);
            $user_account->$c = $user_account->$c - $tv->crypto_profit;
            $user_account->save();
            \App\daily_profit_bonus::where('id',$tv->id)->delete();
        }


    }


})->describe('Display an inspiring quote');




Artisan::command('dib', function () {
    \App\UserAccounts::whereNull('ref_bonus2')->orderBy('id', 'asc')->chunk(1000, function ($user_accounts) {
    foreach($user_accounts as $user_account){
        try{
            $user = \App\User::findOrFail($user_account->id);
            $sum = \DB::table('daily_investment_bonus')->where('parent_id',$user->u_id)->sum('bonus');

            $user_account->ref_bonus2 = round($sum, 2);
            $user_account->save();
            if ($user_account->is_manual_verified == 0) {
                $user_account->reference_bonus = $user_account->ref_bonus2 + $user_account->old_ref_bonus;
                $user_account->save();

            }

        }catch(\Exception $ex){

        }
    }

});

})->describe('Display an inspiring quote');

Artisan::command('merge_rate', function () {
   foreach(\DB::table('old_currency')->get() as $c){
       \App\currency_rates::insert([
           'rate_usd' => $c->dollar_price,
           'rate_btc' => $c->bitcoin_price,
           'rate_eth' => $c->ethereum_price,
           'today_profit' => $c->daily_profit,
           'created_at' => $c->date_time
       ]);
   }

})->describe('Display an inspiring quote');

Artisan::command('Send:popupMessage', function () {
    foreach (\DB::table('users')->where('Country', 'Pakistan')->get() as $c) {
        \App\users::where('bank_name', 'Bank Al-Habib')->update([
            'msg' => 'Dear B4U Global member, please update your Bank Account/IBAN No to receive withdrawals timely.',
            'msg_bit' => '1',
        ]);
    }

})->describe('Sending a Message');

Artisan::command('percentage:import', function () {
        \App\Model\Percentage::import();
})->describe('Display an inspiring quote');



Artisan::command('deposit:sold_at', function () {
    $count = 0 ;
    \App\solds::where('trade_id','!=',0)->chunk(100000,function ($solds)use(&$count){
        foreach($solds as $sold){
            $deposit = \App\deposits::where('id',$sold->trade_id)->whereNull('sold_at');
            if($deposit->count()){
                $deposit = $deposit->first();
                $deposit->sold_at = $sold->created_at;
                $deposit->save();
                $count++;
            }

        }
    });

    \App\deposits::whereNull('sold_at')->where('status','sold')->chunk(100000,function($deposits) use(&$count){
        foreach ($deposits as $deposit){
            $a = $deposit->new_total;
            // dd(0.0124731 ==($a));
            $user_id = $deposit->user_id;
            $currency = $deposit->currency;
            $amount = $deposit->new_total;
            $solds = \DB::select("SELECT id,trade_id,user_id,currency,amount FROM solds WHERE amount LIKE $amount and currency = '$currency' and user_id = $user_id ORDER BY created_at ASC ");
            //dd($solds);
            if(count($solds) == 1){
                $sold = \App\solds::where('id',$solds[0]->id)->first();
                $sold->trade_id = $deposit->id;
                $sold->save();
                $deposit->sold_at = $sold->created_at;
                $deposit->save();
                $count ++ ;
            }
        }
    });

    echo "Total relation made: ".$count;

})->describe('Display an inspiring quote');




Artisan::command('accounts:new:update', function () {
    \App\Model\Percentage::import();
    \App\UserAccounts::updateNewForAll();
})->describe('Display2 an inspiring quote');

Artisan::command('a:s', function () {
   \App\Model\AttractiveFunds::syncForAllUser();
})->describe('Display2 an inspiring quote');


Artisan::command('test2', function () {
    print_r(\App\Model\AttractiveFunds::listCurrentTop50Users()) ;
})->describe('Display2 an inspiring quote');


Artisan::command('deposit:update:rate_and_total_amount', function () {
    \App\deposits::correctRate();

})->describe('Display2 an inspiring quote');



Artisan::command('user:update:max_af', function () {

    \App\Model\AttractiveFunds::syncForAllUserForSolds();
})->describe('Display2 an inspiring quote');


Artisan::command('old:sold_at', function () {
    $uids = \App\Model\Old\Portfolio::select(['user_id'])->distinct()->where('sold', 1)->orderBy('user_id', 'asc')->get();
    foreach ($uids as $u_id) {
        $u_id = $u_id->user_id;

        //   \App\Jobs\Temp\OldSoltAtHit::dispatch($u_id);

        Portfolio::sync($u_id);
    }
})->describe('Display2 an inspiring quote');

Artisan::command('plan:update', function () {

//  \App\Http\Controllers\Admin\Report\NewController::completeAllUser();
//  \App\Jobs\Temp\OldSoltAtHit::dispatch("B4U000314");
    \App\User::orderBy('id', 'desc')->chunk(50, function ($users) {
        foreach ($users as $user) {
            $preplan = $user->plan;
            \App\Console\Commands\PlansCron::process2($user);
            $user = \App\User::find($user->id);
            $updated = "";
            if ($preplan != $user->plan) {
                $updated = "updated";
            }
            echo "\n user(" . $user->id . ") plan(" . $user->plan . ") $updated";
        }
    });
})->describe('Display2 an inspiring quote');

Artisan::command('oldbonus:update', function () {

	//$allBonuses   			=  bonus_history::get();
	\App\bonus_history::orderBy('id', 'desc')->chunk(50, function ($allBonuses){
		
		foreach($allBonuses as $bonus)
		{
			$oldBonusID 		= $bonus->id;
			$userPlan 			= $bonus->plan_id;
			$unique_id 			= $bonus->unique_id;
					
			$bonusPercentage	= \App\ref_investment_bonus_rules::where('id',$userPlan)->first();

			$first_line  		= $bonusPercentage->first_line;
			$second_line		= $bonusPercentage->second_line;
			$third_line			= $bonusPercentage->third_line;
			$fourth_line		= $bonusPercentage->fourth_line;
			$fifth_line			= $bonusPercentage->fifth_line;

			$maxfirstTotal 		= $bonus->maxlevel1;
			$maxsecondTotal 	= $bonus->maxlevel2;
			$maxthirdTotal  	= $bonus->maxlevel3;
			$maxfourthTotal 	= $bonus->maxlevel4;
			$maxfifthTotal  	= $bonus->maxlevel5;

			$bonusFirst 		= (floatval($maxfirstTotal)  *  floatval($first_line) )/100;
			$bonusSecond 		= (floatval($maxsecondTotal) *  floatval($second_line))/100;
			$bonusThird 		= (floatval($maxthirdTotal)  *  floatval($third_line) )/100;
			$bonusFourth 		= (floatval($maxfourthTotal) *  floatval($fourth_line))/100;
			$bonusFifth 		= (floatval($maxfifthTotal)  *  floatval($fifth_line))/100;

			$bonusTotal 		=	$bonusFirst	+	$bonusSecond +	$bonusThird;

			if($userPlan >= '3'){
						
				$bonusTotal 	= 	$bonusTotal + $bonusFourth + $bonusFifth;
			}
			\App\bonus_history::where('id', $oldBonusID)->update(['bonus_total' => $bonusTotal]);
			 echo "\n user $unique_id Old plan updated";
		}	
		
   
    });
})->describe('OldBonus an inspiring quote');

Artisan::command('allRefferals:update', function () {
    \DB::table('users')->orderBy('id','asc')->chunk(10,function($users){
        foreach($users as $user){
            \App\Model\Referral::sync($user->id);
            echo "\n user $user->u_id Partners and plan updated.";
        }
    });

})->describe('Display an inspiring quote');

Artisan::command('topInvestors:update', function () {
		$from 			=  date("2019-02-25");
        $to 			=  date("2019-03-25");
		\DB::table('top_investors')->truncate();
        $totalAmount 	= 0;
        $totalChild 	= 0;
		$counter		= 0;
		$uniqueids 		= array();
		$downlineUsers  = "";
        \App\users::where('status','active')->chunk(100,function($allUsers ) use ($from,$to,&$totalAmount, &$totalChild, &$uniqueids, &$counter, &$downlineUsers){

            if(isset($allUsers)) 
			{
				$count = 0;
                foreach ($allUsers as $user) 
				{
                    $id = $user->id;
                    $u_id = $user->u_id;
					
                    $result 	= \DB::table('users')
						->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
						->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
						->where('users.parent_id', $u_id)
						->where('deposits.status', "Approved")
						->where('deposits.trans_type', "NewInvestment")
						->whereBetween('deposits.approved_at', [$from, $to])
						->groupBy('deposits.user_id')
						->get();
					$count		= count($result);
					$totalChild = $count;
                    if($count > 0) 
					{
                        $amount = 0;
						for($i=0; $i<$count; $i++)
						{	
							//echo $result[$i]->u_id."--".$result[$i]->parent_id."--".$result[$i]->total_amount."<br>";
							//exit;
								
							array_push($uniqueids, $result[$i]->u_id);
							$result2 = \DB::table('users')
									->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
									->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
									->where('users.u_id',$result[$i]->u_id)
									->where('deposits.status',"Approved")
									->where('deposits.trans_type',"NewInvestment")
									->whereBetween('deposits.approved_at',[$from, $to])
									->groupBy('deposits.user_id')
									->get(); 
							
							$count2	= count($result2);
								
								
							if($count2 > 1)
							{
								for($j=0; $j<$count2; $j++)
								{
										
										
									$result3 = \DB::table('users')
											->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
											->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
											->where('users.u_id',$result[$i]->u_id)
											->where('deposits.status',"Approved")
											->where('deposits.trans_type',"NewInvestment")
											->whereBetween('deposits.approved_at',[$from, $to])
											->groupBy('deposits.user_id')
											->sum('deposits.total_amount'); 
									
									$totalAmount = $totalAmount + $result3;
									$count2 ++;
								}// END INNER FOR LOOP
									
							}else{
									$result3 = \DB::table('users')
											->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
											->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
											->where('users.u_id',$result[$i]->u_id)
											->where('deposits.status',"Approved")
											->where('deposits.trans_type',"NewInvestment")
											->whereBetween('deposits.approved_at',[$from, $to])
											->groupBy('deposits.user_id')
											->sum('deposits.total_amount'); 
									$totalAmount = $totalAmount + $result3;
							}
							$counter++;
								
							$downlineUsers = implode(',',$uniqueids);
								
						}// END OUTER FOR LOOP
						
					}
					if($totalAmount >= 5000)
					{
						echo "\n usersID ($u_id) updated Amount, ($totalAmount) Total SUB users = $count , [ $downlineUsers ]<br>";
						$userExist = \App\top_investors::where('user_id',$id)->first();

						if(!isset($userExist))
						{
							$top1 				= new \App\top_investors();
							$top1->user_id 		= $id;
							$top1->user_uid 	= $u_id;
							$top1->level 		= 1;
							$top1->total_amount = $totalAmount;
							$top1->total_child 	= $totalChild;
							$top1->child_list 	= $downlineUsers;
							$top1->from_date 	= $from;
							$top1->to_date 		= $to;
							$top1->save();
			
						}
					}else{
						echo "\n usersID ($u_id) updated Amount, ($totalAmount) <br>";
					}
					
					$uniqueids = array();
					$totalAmount = 0;
				}// end of loop
				
			}
        
		});
})->describe('OldBonus an inspiring quote');

Artisan::command('topInvestors:update', function () {
		$from 			=  date("2019-02-25");
        $to 			=  date("2019-03-25");
		\DB::table('top_investors')->truncate();
        $totalAmount 	= 0;
        $totalChild 	= 0;
		$counter		= 0;
		$uniqueids 		= array();
		$downlineUsers  = "";
        \App\users::where('status','active')->chunk(100,function($allUsers ) use ($from,$to,&$totalAmount, &$totalChild, &$uniqueids, &$counter, &$downlineUsers){

            if(isset($allUsers)) 
			{
				$count = 0;
                foreach ($allUsers as $user) 
				{
                    $id = $user->id;
                    $u_id = $user->u_id;
					
                    $result 	= \DB::table('users')
						->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
						->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
						->where('users.parent_id', $u_id)
						->where('deposits.status', "Approved")
						->where('deposits.trans_type', "NewInvestment")
						->whereBetween('deposits.approved_at', [$from, $to])
						->groupBy('deposits.user_id')
						->get();
					$count		= count($result);
					$totalChild = $count;
                    if($count > 0) 
					{
                        $amount = 0;
						for($i=0; $i<$count; $i++)
						{	
							//echo $result[$i]->u_id."--".$result[$i]->parent_id."--".$result[$i]->total_amount."<br>";
							//exit;
								
							array_push($uniqueids, $result[$i]->u_id);
							$result2 = \DB::table('users')
									->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
									->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
									->where('users.u_id',$result[$i]->u_id)
									->where('deposits.status',"Approved")
									->where('deposits.trans_type',"NewInvestment")
									->whereBetween('deposits.approved_at',[$from, $to])
									->groupBy('deposits.user_id')
									->get(); 
							
							$count2	= count($result2);
								
								
							if($count2 > 1)
							{
								for($j=0; $j<$count2; $j++)
								{
										
										
									$result3 = \DB::table('users')
											->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
											->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
											->where('users.u_id',$result[$i]->u_id)
											->where('deposits.status',"Approved")
											->where('deposits.trans_type',"NewInvestment")
											->whereBetween('deposits.approved_at',[$from, $to])
											->groupBy('deposits.user_id')
											->sum('deposits.total_amount'); 
									
									$totalAmount = $totalAmount + $result3;
									$count2 ++;
								}// END INNER FOR LOOP
									
							}else{
									$result3 = \DB::table('users')
											->leftJoin('deposits', 'deposits.user_id', '=', 'users.id')
											->select('deposits.total_amount','users.u_id','users.id','users.parent_id')
											->where('users.u_id',$result[$i]->u_id)
											->where('deposits.status',"Approved")
											->where('deposits.trans_type',"NewInvestment")
											->whereBetween('deposits.approved_at',[$from, $to])
											->groupBy('deposits.user_id')
											->sum('deposits.total_amount'); 
									$totalAmount = $totalAmount + $result3;
							}
							$counter++;
								
							$downlineUsers = implode(',',$uniqueids);
								
						}// END OUTER FOR LOOP
						
					}
					if($totalAmount >= 5000)
					{
						echo "\n usersID ($u_id) updated Amount, ($totalAmount) Total SUB users = $count , [ $downlineUsers ]<br>";
						$userExist = \App\top_investors::where('user_id',$id)->first();

						if(!isset($userExist))
						{
							$top1 				= new \App\top_investors();
							$top1->user_id 		= $id;
							$top1->user_uid 	= $u_id;
							$top1->level 		= 1;
							$top1->total_amount = $totalAmount;
							$top1->total_child 	= $totalChild;
							$top1->child_list 	= $downlineUsers;
							$top1->from_date 	= $from;
							$top1->to_date 		= $to;
							$top1->save();
			
						}
					}else{
						echo "\n usersID ($u_id) updated Amount, ($totalAmount) <br>";
					}
					
					$uniqueids = array();
					$totalAmount = 0;
				}// end of loop
				
			}
        
		});
})->describe('topInvestors an inspiring quote');











/*
Artisan::command('bonus:update', function () {
	$count = 0 ;
	//$user_account = \App\UserAccounts::where('user_id','14855')->where('is_manual_verified','0')->get();
	\App\UserAccounts::where('is_manual_verified','0')->chunk(1000,function($user_account) use(&$count){
        foreach($user_account as $useracc)
		{
            $user_id 		= $useracc->user_id;
			//$old_bonus 		= $useracc->old_ref_bonus;

			$userDetails    = \App\users::where('id',$user_id)->first();
			if(isset($userDetails))
			{
				if(isset($userDetails->u_id))
				{
					$userUID		= $userDetails->u_id;
                } else {
					$userUID		= "Unknown";
				}
				if(isset($userDetails->plan))
				{
					$userPlan		= $userDetails->plan;
				}else{
					$userPlan		= 1;
				}

				$bonusPercentage	= \App\ref_investment_bonus_rules::where('id',$userPlan)->first();

				$first_line  		= $bonusPercentage->first_line;
				$second_line		= $bonusPercentage->second_line;
				$third_line			= $bonusPercentage->third_line;
				$fourth_line		= $bonusPercentage->fourth_line;
				$fifth_line			= $bonusPercentage->fifth_line;

				$maxfirstTotal 	=	\App\Model\AttractiveFunds::where('user_id',$user_id)->where('level',1)->pluck('max_amount');
				$maxsecondTotal = 	\App\Model\AttractiveFunds::where('user_id',$user_id)->where('level',2)->pluck('max_amount');
				$maxthirdTotal  = 	\App\Model\AttractiveFunds::where('user_id',$user_id)->where('level',3)->pluck('max_amount');

				$bonusFirst 		= (floatval($maxfirstTotal[0])  *  floatval($first_line) )/100;
				$bonusSecond 		= (floatval($maxsecondTotal[0]) *  floatval($second_line))/100;
				$bonusThird 		= (floatval($maxthirdTotal[0])  *  floatval($third_line) )/100;

				if($userPlan >= '3'){
					$maxfourthTotal = \App\Model\AttractiveFunds::where('user_id',$user_id)->where('level',4)->pluck('max_amount');
					$maxfifthTotal  = \App\Model\AttractiveFunds::where('user_id',$user_id)->where('level',5)->pluck('max_amount');
					$bonusFourth 	= (floatval($maxfourthTotal[0]) *  floatval($fourth_line))/100;
					$bonusFifth 	= (floatval($maxfifthTotal[0])  *  floatval($fifth_line))/100;
				}


                $maxTotal 			= 	$maxfirstTotal[0]	+	$maxsecondTotal[0] +	$maxthirdTotal[0];
				$bonusTotal 		=	$bonusFirst	+	$bonusSecond +	$bonusThird;

				if($userPlan >= '3'){
					$maxTotal 		= 	$maxTotal + $maxfourthTotal[0] + $maxfifthTotal[0];
					$bonusTotal 	= 	$bonusTotal + $bonusFourth + $bonusFifth;
				}


                $bonuswithdrawal =  \DB::table('withdrawals')
				->select('usd_amount')
				->where('user', $user_id)
				->where('payment_mode', "Bonus")
				->where(function ($query) {
					$query->where('status', "Approved")
						->orWhere('status', "Pending");
				})->sum('usd_amount');

				$bonusreinvestment =  \DB::table('deposits')
					->select('total_amount')
					->where('user_id', $user_id)
					->where('trans_type', "Reinvestment")
					->where('reinvest_type', "Bonus")
					->where(function ($query) {
						$query->where('status', "Approved")
							->orWhere('status', "Sold");
					})->sum('total_amount');


                $profitbonus    =  \DB::table('daily_investment_bonus')
					->where('parent_id', $userUID)
					->where('details','LIKE', "Profit Bonus")
					->sum('bonus');

				$finaltotalbonus    = $profitbonus + $bonusTotal - ($bonuswithdrawal + $bonusreinvestment) ;
				//echo $finaltotalbonus;
				//exit;
				//dd(\DB::getQueryLog());
				$useracc = \App\UserAccounts::where('user_id',$user_id)->first();
				$useracc->reference_bonus = $finaltotalbonus;
				$useracc->save();

				$this->info("$userUID Bonus Calculated Successfully! Final Bonus: ".$finaltotalbonus." Profit Bonus: ".$profitbonus." Avail Total Bonus: ".$bonusTotal." Bonus Withdrawal: ".$bonuswithdrawal."<ln> Bonus Reinvest: ".$bonusreinvestment);
				Log::info("$userUID Bonus Calculated Successfully! Final Bonus: ".$finaltotalbonus." Profit Bonus: ".$profitbonus." Avail Total Bonus: ".$bonusTotal." Bonus Withdrawal: ".$bonuswithdrawal."<ln> Bonus Reinvest: ".$bonusreinvestment);
			}
			$count ++ ;
		}
    });
})->describe('Update Reference Bonus');
*/
/*

Artisan::command('withdraw:usd', function () {
	$count = 0 ;
	\App\withdrawals::where('currency',"!=",'USD')->chunk(1000,function($withdrawals) use(&$count)
	{
        foreach($withdrawals as $withdraw){
            $c_amount 		= $withdraw->crypto_amount;
			$currency 		= $withdraw->currency;

			$currencySmall	= strtolower($currency);
			$createdDate 	= $withdraw->created_at;
			$date 			= strtotime($createdDate);
			$created_at 	= date("Y-m-d 00:00:00", $date);
			$created_at2 	= date("Y-m-d 23:59:00", $date);
			$rateVal		= "rate_".$currencySmall;

			//\DB::enableQueryLog();
            $rateQry 		= \App\currency_rates::whereBetween('created_at', array($created_at, $created_at2))->first();
			//$rateQry 		= \App\currency_rates::where('created_at','LIKE',$createdDate)->first();
			$rate			= $rateQry->$rateVal;

			//$rateQry 		= \DB::select("SELECT * FROM currency_rates where date(created_at) LIKE $created_at");
			//dd(\DB::getQueryLog());

			$rate			= $rateQry->$rateVal;
			$usdAmount		= $c_amount * $rate;

			$wd = \App\withdrawals::where('id',$withdraw->id)->first();
            $wd->usd_amount = $usdAmount;
            $wd->save();
            $count ++ ;
        }
    });
})->describe('Update Withdrawal table'); */
/*
 * Artisan::command('grc', function () {


        foreach(\DB::select("SELECT login_id,count(*) as c FROM `previous_reports` GROUP by user_id HAVING c < 5 ") as $user){
            \App\Jobs\PreviousReportImportor::dispatch($user->login_id);

        }

})->describe('Display an inspiring quote');

 *Artisan::command('pr', function () {

    \DB::table('previous_reports')->orderBy('id','asc')->chunk(100,function($prs){
        foreach($prs as $pr){
            $user_account = \App\UserAccounts::where('user_id',$pr->user_id);
            if($user_account->count()){
                $user_account = $user_account->first();
                if($pr->type=='total_bonus'){
                    $user_account->update(['old_ref_bonus' => $pr->balance]);
                }

                if($pr->type=='total_profit_btc'){
                    $user_account->update(['old_profit_btc' => $pr->balance]);
                }

                if($pr->type=='total_profit_eth'){
                    $user_account->update(['old_profit_eth' => $pr->balance]);
                }

                if($pr->type=='total_profit_usd'){
                    $user_account->update(['old_profit_usd' => $pr->balance]);
                }

            }

        }
    });


})->describe('Display an inspiring quote');

 * Artisan::command('gr', function () {


    \DB::table('users')->orderBy('id','asc')->chunk(10,function($users){
        foreach($users as $user){
            \App\Jobs\PreviousReportImportor::dispatch($user->u_id);

        }
    });

})->describe('Display an inspiring quote');
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
/*
Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('gr', function () {


 \DB::table('users')->orderBy('id','asc')->chunk(10,function($users){
         foreach($users as $user){
             file_get_contents("https://b4uglobal.com/report/index.php?user_id=".$user->u_id);
            file_get_contents("https://b4uglobal.com/report/btc.php?user_id=".$user->u_id);
            file_get_contents("https://b4uglobal.com/report/eth.php?user_id=".$user->u_id);
         file_get_contents("https://b4uglobal.com/report/usd.php?user_id=".$user->u_id);


         }
     });

     })->describe('Display an inspiring quote');


Artisan::command('pr', function () {

 \DB::table('previous_reports')->orderBy('id','asc')->chunk(100,function($prs){
                foreach($prs as $pr){
            $user_account = \App\UserAccounts::where('user_id',$pr->user_id);
            if($user_account->count()){
                $user_account = $user_account->first();
                if($pr->type=='total_bonus'){
                    $user_account->update(['old_ref_bonus' => $pr->balance]);
                }

                if($pr->type=='total_profit_btc'){
                    $user_account->update(['old_profit_btc' => $pr->balance]);
                }

                if($pr->type=='total_profit_eth'){
                    $user_account->update(['old_profit_eth' => $pr->balance]);
                }

                if($pr->type=='total_profit_usd'){
                    $user_account->update(['old_profit_usd' => $pr->balance]);
                }

            }

         }
     });


     })->describe('Display an inspiring quote');
*/
/*Artisan::command('test:mail', function () {
	
	\DB::table('users')->where('is_subscribe',1)->orderBy('id','asc')->chunk(10,function($users){
       foreach($users as $user){
			$email = $user->email;
			//if(isset($email) && ($email == "mudassar.mscit@gmail.com" ))
			if(isset($email))
			{
		    	\Mail::to($email)->send(new \App\Mail\TestMail('Mudassar'));
		    	 echo " Email Successfully Dileverd to $email </br>";
			}
		} 
        echo "Email Successfully Dileverd to all";
        exit;
    });
})->describe('Display an inspiring quote');
*/
Artisan::command('test:mail', function () {
    $user = \App\User::where('email','mubixhb4u@gmail.com');
    if($user->count()){
        $user = $user->first();
        \Mail::to($user)->send(new \App\Mail\TestMail('Mudassar'));
        echo "Email Successfully Dileverd";
        exit;
    }else{
        //$user = "mudassar_mscit@yahoo.com";
        echo "user not found";
    }
})->describe('Display an inspiring quote');

Artisan::command('test:queueMail', function () {
    $user = \App\User::where('email','mudassar_mscit@yahoo.com')->orWhere('email','toseefamir@gmail.com');
    if($user->count()){
        $user = $user->first();
		if(isset($user->name))
		{
		    $userName = $user->name;
		}else{
		    $userName = "Test Mail";
		}
		if(isset($user->u_id))
		{
            $userUID = $user->u_id;
		}else{
            $userUID ="TEST USER UID";
		}
		if(isset($user->id))
		{
            $userid = $user->id;
		}else{
            $userid = "User ID";
		}

		$lastProfitRate = ".30";
		$message = (new  \App\Mail\ProfitPercentageEmail($userName,$userUID,$userid,$lastProfitRate))->onQueue('emails');
        \Mail::to($user)->queue($message);
		 echo "Email Successfully Dileverd to $userUID";
		 exit;
    }else{
        //$user = "mudassar_mscit@yahoo.com";
        echo "user not found";
    }
})->describe('Display an inspiring quote');


Artisan::command('upload:gc_images', function () {
	$donedepostsids =  \App\Replace::pluck('deposit_id');
	
    \App\deposits::whereNotIn('id',$donedepostsids)->whereNotNull('proof')->orderby('id','desc')->chunk(100,function($deposits){
        //$total = 0;
		foreach ($deposits as $deposit){
			//$exists = Storage::disk('s3')->exists('file.jpg');
			//$path      	= \Illuminate\Support\Facades\Storage::disk('gcs')->putFile('/profile_Img',
			if(!empty($deposit->proof)){
					echo $deposit->proof."\n";
			
				$path = "storage/app/uploads/".$deposit->proof;
				
				if(  File::exists($path)){
					$gcspath = Storage::disk('gcs')->putFile('/uploads',new \Illuminate\Http\File($path),\Illuminate\Contracts\Filesystem\Filesystem::VISIBILITY_PUBLIC);
					echo $gcspath."\n";
					\App\Replace::create(['deposit_id'=>$deposit->id,'proof'=>str_replace('uploads/','',$gcspath)]);
					//\App\deposits::where('id', $deposit->id)->update(['proof' => $gcspath]);
				}
			}
		}
	});
})->describe('Display an inspiring quote');


Artisan::command('update:profile_image', function () {
	$doneuserids =  \App\Profileimage::whereNotNull('photo');
    //whereNotIn('id',$doneuserids)->
	//exit();
    \App\users::whereNotNull('photo')->orderby('id','desc')->chunk(100,function($allUsers){
        $total = 0;
		foreach ($allUsers as $user){
			
			if(!empty($user->photo)){
				
				echo $user->photo."\n";
			
				$path = "storage/app/profile_Img/".$user->photo;
				
				if(File::exists($path)){
					
					$gcspath = Storage::disk('gcs')->putFile('/profile_Img',new \Illuminate\Http\File($path),\Illuminate\Contracts\Filesystem\Filesystem::VISIBILITY_PUBLIC);
					
					echo $gcspath."\n";
					
					\App\Profileimage::create(['user_id'=>$user->id,'photo'=>str_replace('profile_Img/','',$gcspath)]);
					
					//\App\users::where('id', $deposit->id)->update(['photo' => $gcspath]);
				}
			}
		}
	});
})->describe('Display an inspiring quote');

Artisan::command('clearRate:cache', function () {
    Cache::pull('lastCurrencyRate');
    Cache::pull('lastCurrentRate');
});


Artisan::command('clear:cache:banks', function () {
    Cache::pull('bankAccountsCache');
    Cache::pull('pakBankAccountsCache');
});
