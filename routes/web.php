<?php

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

/*
 * =========================== Customer site Routes
 * */
Route::get('/', 'UsersController@index')->name('home');
Route::get('/LoginFromAdmin', 'UsersController@LoginFromAdmin')->name('LoginFromAdmin');
Route::get('terms', 'UsersController@terms')->name('terms');
Route::get('privacy', 'UsersController@privacy')->name('privacy');
Route::get('about', 'UsersController@about')->name('about');
Route::get('contact', 'UsersController@contact')->name('contact');
Route::get('contactus', 'UsersController@contactus')->name('contactus');
Route::get('faq', 'UsersController@faq')->name('faq');
Route::get('pcalculator', 'UsersController@pcalculator')->name('pcalculator');
Route::get('terms', 'UsersController@terms')->name('terms');
Route::get('privacy', 'UsersController@privacy')->name('privacy');
Route::get(
    'partnership_agreement',
    'UsersController@partnership_agreement'
)->name('partnership_agreement');


/*
 * =========================== Panel Routes
 * */


## validate the user added parent while registration
Route::post('validate/parentid', 'Auth\RegisterController@validateParentId');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');


$globalMiddleWare = [
    'auth',
    'wrong_attempts',
//    'verified'
];

//Route::middleware(['auth', 'verified'])->group(function () {
Route::middleware($globalMiddleWare)->group(function () {

//AdminController Routes
    Route::prefix('dashboard')->group(
        function () {

            Route::post(
                '/saveuser',
                'AdminController@saveuser'
            );
            Route::get('/signed-url-upload', 'SignedUrlUploadController@getPostObjectUrl');
            Route::get('/get-object-url/{objectKey}', 'SignedUrlUploadController@getObjectUrl');
            Route::get(
                '/', 'AdminController@dashboard'
            )->name('dashboard');
            Route::post('/viewUserDetails', 'AdminController@viewUserDetails');
            Route::get(
                '/withdrawal/valid',
                'WithdrawalController@withdrawal'
            )->name("valid.withdraw");

            Route::get(
                '/partners_after2',
                [
                    'middleware' => ['auth', 'can:user-sections'],
                    'uses' => 'User\PartnerController@partnersAfter2'
                ]
            )->name('partners_after2');
            Route::get(
                '/refferal_partners_after2',
                [
                    'middleware' => ['auth', 'can:user-sections'],
                    'uses' => 'User\PartnerController@RefferalPartnersAfter2'
                ]
            )->name('refferal_partners_after2');
            Route::post('/refferal_partner_level_1/json', ['middleware' => 'auth',
                'uses' => 'User\PartnerController@refferal_partner_level_1_json', 'as' => 'refferal_partner_level_1_json']);


            Route::post('/refferal_partner_level_2/json', ['middleware' => 'auth',
                'uses' => 'User\PartnerController@refferal_partner_level_2_json', 'as' => 'refferal_partner_level_2_json']);
            Route::post('/refferal_partner_level_3/json', ['middleware' => 'auth',
                'uses' => 'User\PartnerController@refferal_partner_level_3_json', 'as' => 'refferal_partner_level_3_json']);
            Route::post('/refferal_partner_level_4/json', ['middleware' => 'auth',
                'uses' => 'User\PartnerController@refferal_partner_level_4_json', 'as' => 'refferal_partner_level_4_json']);
            Route::post('/refferal_partner_level_5/json', ['middleware' => 'auth',
                'uses' => 'User\PartnerController@refferal_partner_level_5_json', 'as' => 'refferal_partner_level_5_json']);
            Route::get(
                '/partners2',
                [
                    'middleware' => 'auth',
                    'uses' => 'User\PartnerController@partners2'
                ]
            )->name('partners2');
            Route::get(
                '/partners',
                [
                    'middleware' => ['auth', 'can:user-sections'],
                    'uses' => 'User\PartnerController@RefferalPartners',
                    'as' => 'partners'
                ]
            );
            Route::get(
                '/RefferalPartners',
                [
                    'middleware' => ['auth', 'can:user-sections'],
                    'uses' => 'User\PartnerController@RefferalPartners',
                    'as' => 'RefferalPartners'
                ]
            );
            Route::get(
                '/plan_json',
                [
                    'middleware' => 'auth',
                    'uses' => 'User\PlanController@plan_json'
                ]
            )->name('plan_json');
            Route::get(
                '/plan_json2',
                [
                    'middleware' => 'auth',
                    'uses' => 'User\PlanController@plan_json2'
                ]
            )->name('plan_json2');

            /*
             * ======= Start:: Deposit Routes List
             * */
            Route::get('/payment', 'PaymentController@payment')->name('payment');
            Route::post('/deposit', 'PaymentController@deposit');
            Route::post('/savedeposit', 'PaymentController@savedeposit')->name('save_deposit');
            Route::post('/reinvest-deposit', 'PaymentController@savedeposit')->name('reinvest_deposit')->middleware(['2faAuth']);
            Route::get(
                '/deposits',
                [
                    'middleware' => ['auth'],
                    'uses' => 'PaymentController@deposits'
                ]
            )->name('deposits');
            Route::post('/deposits/json', ['middleware' => 'auth' ,
                'uses' => 'PaymentController@deposits_json', 'as' => 'deposits_json']);

            /*
           * ======= End:: Deposit Routes List
           * */


            /*
             * ======= Start:: Withdrawal Routes List
             * */

            Route::get(
                '/withdrawal',
                [
                    'middleware' => 'auth',
                    'uses' => 'WithdrawalController@withdrawal',
                    'as' => 'withdrawal'
                ]
            );
            Route::post('/view-partner-details', 'AdminController@viewPartnerDetail');
            Route::post('/withdrawal', 'WithdrawalController@withdrawal')->middleware(['2faAuth']);
            Route::get('/withdrawals', ['middleware' => ['auth'], 'uses' => 'WithdrawalController@withdrawals'])->name('withdrawals');
            Route::post('/withdrawals/json', ['middleware' => ['auth'], 'uses' => 'WithdrawalController@withdrawals_json'])->name('withdrawals_json');
            Route::post('/viewWithdrawDetailsPost', 'WithdrawalController@viewWithdrawDetailsPost')->name('view.withdrawal.details');
            Route::post('/subscribe_call', ['middleware' => 'auth', 'uses' => 'HomeController@subcribeCallPckg'])->name('subcribeCallPckg');

            /*
            * ======= End:: Withdrawal Routes List
            * */


            Route::post('/saleTradePost', 'PaymentController@saleTradePost');
            Route::post('/saleTradeSave', 'PaymentController@saleTradeSave');
            Route::post('/viewDetailsPost', 'PaymentController@viewDetailsPost');
            Route::post('/viewProofPost', 'PaymentController@viewProofPost');
            Route::post('/getAccountInfoPost', 'PaymentController@getAccountInfoPost');
            Route::post(
                '/getAccountInfoPostDonation',
                'PaymentController@getAccountInfoPostDonation'
            );

            Route::post(
                '/viewUserDetailsParent',
                'AdminController@viewUserDetailsParent'
            );
            Route::post(
                '/currenconvert',
                'PaymentController@currencyconversion'
            );
            Route::post(
                '/depositCheck',
                'PaymentController@depositCheck'
            );
            Route::post('/amountLimitCheck', 'PaymentController@amountLimitCheck');

            // Sold Routes
            Route::get(
                '/sold', 'SoldController@sold'
            )->name('sold');

            Route::get('/donations', ['middleware' => ['auth'], 'uses' => 'DonationController@donations'])->name('donations');
            Route::post('/donations/json', ['middleware' => ['auth'], 'uses' => 'DonationController@donations_json'])->name('donations_json');
            Route::post('/donations', ['middleware' => 'auth', 'uses' => 'DonationController@createDonation'])->name('donations.add');
            Route::post('/donate-one-day-profit', ['middleware' => 'auth', 'uses' => 'DonationController@oneday_profitDonation'])->name('donations.donate_odp');
            Route::post('/cancel-donate-one-day-profit', ['middleware' => 'auth', 'uses' => 'DonationController@canceldonateOdp'])->name('donations.donate_odp.cancel');
            Route::post('/subscribe_alpha', ['middleware' => 'auth', 'uses' => 'HomeController@subscribeAlpha'])->name('subscribe_alpha');


            ////ProfitBonusController from ProfitCalculateController

            Route::get('/daily_bonus', ['middleware' => 'auth', 'uses' => 'ProfitBonusController@daily_bonus', 'as' => 'daily_bonus']);
            Route::get('/daily_bonus/json', ['middleware' => 'auth', 'uses' => 'ProfitBonusController@daily_bonus_json', 'as' => 'daily_bonus_json']);
            Route::get('/daily_profit', ['middleware' => 'auth', 'uses' => 'ProfitBonusController@daily_profit', 'as' => 'daily_profit']);
            Route::match(['GET', 'POST'], '/daily_profit/json', ['middleware' => 'auth', 'uses' => 'ProfitBonusController@daily_profit_json', 'as' => 'daily_profit_json']);

            Route::post('/calculateProfit', 'ProfitCalculateController@calculateProfit');

            Route::get('/profitCalculator', ['middleware' => ['auth', 'can:user-sections'], 'uses' => 'AdminController@profitCalculator', 'as' => 'profitCalculator']);
            Route::post('/findProfit', 'AdminController@findProfit');

            Route::get('dashboard/kyc', ['middleware' => 'auth', 'uses' => 'UsersController@getKyc', 'as' => 'user.kyc.get']);
            Route::get('dashboard/kyc/{user}', ['middleware' => 'auth', 'uses' => 'UsersController@showKyc', 'as' => 'user.kyc.show']);
            Route::post('dashboard/kyc', ['middleware' => 'auth', 'uses' => 'UsersController@postKyc', 'as' => 'user.kyc.update']);
            Route::post(
                'dashboard/kyc-nok',
                [
                    'uses' => 'UsersController@postKyc',
                    'as' => 'user.kyc.update'
                ]
            );

            Route::get('dashboard/changepassword', ['middleware' => 'auth', 'uses' => 'UsersController@changepassword', 'as' => 'changepassword']);

            Route::post('dashboard/withdrawRules', 'UsersController@withdrawRules');

            Route::get('/accountdetails', ['middleware' => 'auth', 'uses' => 'UsersController@accountdetails', 'as' => 'acountdetails']);
            Route::get('/accountdetailsPhone', ['middleware' => 'auth', 'uses' => 'UsersController@accountdetailsPhone', 'as' => 'accountdetailsPhone']);
            Route::get('/update_profile_phone', ['middleware' => 'auth', 'uses' => 'UsersController@accountdetailsPhone'])->name('update_profile_phone');
            Route::get('/kyc', ['middleware' => 'auth', 'uses' => 'UsersController@getKyc', 'as' => 'user.kyc.get']);
            Route::get('/kyc/{user}', ['middleware' => 'auth', 'uses' => 'UsersController@showKyc', 'as' => 'user.kyc.show']);
            Route::post('/kyc', ['middleware' => 'auth', 'uses' => 'UsersController@postKyc', 'as' => 'user.kyc.update']);
            Route::get('/changepassword', ['middleware' => 'auth', 'uses' => 'UsersController@changepassword', 'as' => 'changepassword']);
            Route::get('/dnpagent', ['middleware' => 'auth', 'uses' => 'UsersController@dnpagent', 'as' => 'dnpagent']);
            Route::get('/referuser', ['middleware' => ['auth', 'can:user-sections'], 'uses' => 'UsersController@referuser', 'as' => 'referuser']);
            Route::post('/viewUserDetails', 'AdminController@viewUserDetails')->name('view_user_details');

            Route::post('/updatephoto', 'UsersController@updatephoto');
            Route::post('/updateacct', 'UsersController@updateacct')->name('updateacct')->middleware(['2faAuth']);
            Route::match(['GET', 'POST'], '/updateprofile', 'UsersController@updateprofile')->middleware(['2faAuth']);
            Route::match(['GET', 'POST'], '/updateprofilePhone', 'UsersController@updateprofilePhone')->middleware(['2faAuth']);
            Route::post('/updatepass', 'UsersController@updatepass')->middleware(['2faAuth']);
            Route::post('/dnate', 'UsersController@dnate');
            Route::post('/saveagent', 'UsersController@saveagent');

            Route::get('/awarded_partners/{level?}', 'User\AwardedPartnerController@awarded_partners')->name('awarded_partners');
            Route::get('/awarded_partners/cache/clear', 'User\AwardedPartnerController@clear_cache')->name('clear_cache_awarded_partners');

            Route::post(
                '/banksDetails',
                'UsersController@banksDetails'
            )->name('banksDetails');
            Route::post('/getBonusHistory', 'User\PartnerController@getBonusHistory')->name('getBonusHistory');

            Route::post(
                '/showmsg', 'AdminController@showmsg'

            );

            //Upload picture of deposit
            Route::post('/viewProofPost1', 'WithdrawalController@viewProofPost1');
            Route::get('/viewibanimage', 'UsersController@viewibanimage');

            #forDev
            /*   Route::get('/oneMonthPromo', 'User\TopInvestorsController@oneMonthPromo');
               Route::get('/oneMonthPromoExt', 'User\TopInvestorsController@oneMonthPromoExt'); */
               Route::get('/sixMonthPromo', 'User\TopInvestorsController@sixMonthPromo');
            #forUser
            Route::get('/promo-investors/{parmas?}', 'User\TopInvestorsController@promoInvestors')->name('promo_investor');
            Route::get('/oneMonthPromo', 'User\TopInvestorsController@oneMonthPromo2021')->name('oneMonthPromo');

            #notUsed
            //   Route::get('/topinvestors', 'User\TopInvestorsController@topinvestors');
            //  Route::post('/topMemberCalculator', 'User\TopInvestorsController@topMemberCalculator');

            Route::post(
                '/beneficiaryDetails',
                'WithdrawalController@beneficiaryDetails'
            );


            Route::get(
                '/PF_success',
                function () {
                    return view('dashboard')
                        ->with('msg', "You deposit has been successfully submitted");
                }
            );

            Route::get(
                '/PF_failed',
                function () {
                    return view('dashboard')
                        ->with('errormsg', "Your deposit has been cancelled");
                }
            );

        }
    );
    Route::get(
        'withdrawals/ajax', 'AjaxController@getWithDrawals'
    )->name("withdrawals.ajax");
// get total bonus
    Route::get(
        'totalbonus/ajax', 'AjaxController@getTotalBonus'

    )->name("totalbonus.ajax");
// user account history
    Route::get(
        'account/history/{id}', 'AdminController@userHistory'

    );
    Route::get(
        'ref/{id}',
        [
            'uses' => 'Controller@ref',
            'as' => 'ref'
        ]
    );
    Route::get(
        'partners_history/store', 'User\PartnerController@partnersHistory'

    );
    Route::get('deposit/valid', 'PaymentController@deposit'); ///otp validation
    Route::post('updateacct/valid', 'UsersController@updateacct')->name('updateacct_valid')->middleware(['2faAuth']);
    Route::get('ref/{id}', ['uses' => 'UsersController@ref', 'as' => 'ref']);
//Promotions
    Route::post('promotions/json', 'Admin\PromotionsController@index')->name('promotions.post.json');
    Route::post('promotions/history/json', 'Admin\UserPromotionsController@index')->name('promotions.history.post.json');
    Route::resources(
        [
            'promotions' => 'Admin\PromotionsController',
            'promotion_history' => 'Admin\UserPromotionsController'
        ]
    );

    Route::get(
        'user/logout',
        function () {
            Auth::logout();
            Session::flush();
            return Redirect::route('login');
        }
    );
    Route::post(
        'dashboard/depositprofdiv', 'WithdrawalController@depositprofdiv');
    /*Route::get('version/{contentType?}', function ($content_type = 'html') {
        $versionDesc = [
            'version_no' => 1.1,
            'major_release' => 'No',
            'release_notes' => [
                'Remove Deposit Resize feature',
                'Give access to naeem of hold withdrawals',
                'Implement True random number with deposit file name',
            ],
            'Developer Involve' => [
                'Mubasir'
            ]
        ];


        switch ($content_type) {
            case 'html':
                $response = view('version-details', ['versionDesc' => $versionDesc]);
                break;
            case 'json':
            default:
                $response = response()->json($versionDesc);
                break;
        }

        return $response;
    });*/

    Route::get('version/{contentType?}', function ($content_type = 'html') {
        $gitPath = base_path() . '/.git/HEAD' ;
        $gitBasePath = base_path().'/.git';

        $gitString = file_get_contents($gitPath);
        $gitBranchName = rtrim(preg_replace("/(.*?\/){2}/", '', $gitString));
        $gitPathBranch = $gitBasePath .'/refs/heads/'.$gitBranchName;
        $gitHash = file_get_contents($gitPathBranch,false,null,0,7);
        $gitDate = date('d-m-Y H:i', filemtime($gitPathBranch));
        $gitCommitMessage =  exec("git log -1");
        $gitCommitMessageServer =  exec("sudo git log -1");
        $gitCommitMessage = $gitCommitMessage . $gitCommitMessageServer;
        /*
            dump($gitCommitMessage);
            dump($gitBranchName);
            dump($gitPathBranch);
            dump($gitDate);
            dump($gitHash);*/

        $versionDesc = [
            'version_no' => 1.1,
            'major_release' => 'No',
            'release_notes' => [
                'Crypto Addresses Validation'
            ],
            'Git Details' => [
                'Commit Message' =>   $gitCommitMessage,
                'Branch Name' => $gitBranchName,
                'Commit Date' => $gitDate,
                'Commit No' => $gitHash,
            ],

        ];
        switch ($content_type) {
            case 'html':
                $response = view('version-details', ['versionDesc' => $versionDesc]);
                break;
            case 'json':
            default:
                $response = response()->json($versionDesc);
                break;
        }
        return $response;

        dump($imageName);
        dump('https://storage.googleapis.com/b4ufiles/' . $path);
    });

    /**
     * @Purpose Cash Box rotues.
     * */
    Route::resource('cash-box', 'CashBox\CashBoxController');
    Route::post('cash-box/credits/json', 'CashBox\CashBoxCreditController@index');
    Route::post('cash-box/debits/json', 'CashBox\CashBoxDebitController@index');
    Route::group(['middleware' => 'restrictUserToCb'], function() {
    Route::resource('cash-box-credits', 'CashBox\CashBoxCreditController');
    Route::resource('cash-box-debits', 'CashBox\CashBoxDebitController');
    Route::post('cash-box/transfer', 'CashBox\CashBoxController@fundTransfer')->name('cash_box_transfer')->middleware(['2faAuth']);
    Route::post('cash-box/top-down', 'CashBox\CashBoxDebitController@topdown')->name('cash_box_topUp')->middleware(['2faAuth']);
        ##user cashbox action
        Route::get('cash-box/cbTransfer/approve/{id}','CashBox\CashBoxController@approveCbTransfer')->name('approveCbTransfer');
        Route::post('cash-box/deposit', 'CashBox\CashBoxDebitController@store')->name('cash_box_deposit')->middleware(['2faAuth']);
    });
    Route::get('cash-box/cash-out/allowed', 'CashBox\CashBoxDebitController@isAllowedTopDown')->name('is_allowed_top_down');
    Route::get('cash-box/get-balance/{id}', 'CashBox\CashBoxController@getCurrentBalance')->name('get_cash_box_current_balance');
    ##user cashbox action
    Route::post('cash-box/cbRatingUser','CashBox\CashBoxController@cbRatingUser')->name('cb_rating_user');
   Route::get('userRatingInfo','CashBox\CashBoxController@userRatingInfo')->name('get_user_rating_info');
   Route::get('ratingDetails/{id}','CashBox\CashBoxController@reviewDetails')->name('review_details');
    /**
     * @Purpose Update User 2FA
     * */
    Route::match(['GET', 'POST'], '/update/user/2fa', 'UsersController@change2FaAuthentication')->name('update_google_2fa')->middleware(['2faAuth']);

    /**
     * @Purpose Send Otp to Current Logged In User
     * */
    Route::get('cash-box/send-otp/{mod}', function ($mod) {
        /** @var User $loggedInUser */
        $loggedInUser = \Illuminate\Support\Facades\Auth::user();
        return \App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade::sendOtp($loggedInUser->two_fa_auth_type, $loggedInUser, $mod);
    });

    /**
     * @Purpose Send Otp to Current Logged In User
     * */
    Route::get('dashboard/send-otp/{mod}', function ($mod) {
        /** @var User $loggedInUser */
        $loggedInUser = \Illuminate\Support\Facades\Auth::user();
        return \App\TwoFAAuth\Authentication\TwoFAAuthenticateFacade::sendOtp($loggedInUser->two_fa_auth_type, $loggedInUser, $mod);
    });

    /**
     * @Purpose Setup Google Authenticator or enable Google Authenticator.
     * */
    Route::post('setup/google/auth', 'UsersController@setupGoogle2FA')->name('setup_google_auth');
    Route::post('setup/save-pin', 'UsersController@setupSavedPin')->name('setup_saved_pin_auth');

    /**
     * Send email verification route
     * */
    Route::get('send-email-verification', function () {
        /** @var User $User */
        $User = Auth::user();
        $User->sendEmailVerificationNotification();
        return back()->with('successmsg', 'Verification Email has sent to your registered Email Address');
    })->name('send_verification_email_');


});

/**
 * Show Account Blocked page, if account is not blocked then redirect user to dashboard either redirect user to Blocked Page
 * */
Route::get('account-blocked', 'UsersController@accountBlock')->name('account_blocked');
//Route::get('check', 'HomeController@check')->name('check');
Route::get('autoProfit', function (){\App\current_rate::profitMonthlyLimit();});

//Route::get('send-reason-to-unblock-wrong-attempt-account', function () {
//
//    $RemoteEndPoint = 'https://support.b4uglobal.com/api/http.php/tickets.json';
//    $loggedInUser = Auth::user();
//    $message = null;
//    $ticketData = [
//        'name' => $loggedInUser->name,
//        'email' => $loggedInUser->email,
//        'subject' => 'Un-Block Wrong Attempt Account of User #' . $loggedInUser->u_id,
//        'program' => 'wrongAttempt',
//        'status' => 'open',
//        'quest' => 'NA',
//        'topicId' => 'NA',
//        'ip' => '52.77.183.244',
//        'message' => $message
//    ];
//
//    $HttpResponse = \Illuminate\Support\Facades\Http::
//    withHeaders([
//        'Expect:' . 'X-API-Key: 574209BF8E0F08DF2F89C9FBE82DF851'
//    ])->post($RemoteEndPoint, $ticketData);
//
//    dd($HttpResponse);
//
//
//})->name('send_reason_to_unblock_wrong_attempt_account');

Route::get('test', function () {
    $response = \App\WebHook\IntimateCashBoxTransferToCashBoxWebHook::send([
        'member' => 'admin@barong.io',
        ## receiver
        'rid' => 'john@barong.io',
        ## currency
        'currency' => 'usd',
        ## amount.
        'amount' => 100
    ]);
    dd($response);
});

Route::post('/hide_popup', ['middleware' => 'auth', 'uses' => 'DonationController@canceldonateOdp'])->name('hidePopUp');

Route::post('/mailer', 'CallCenterController@mailer')->name('mailer');